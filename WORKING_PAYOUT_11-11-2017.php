<?public function payout_work($start,$end){
      $em = $this->doctrine->em;
      
      $new_time = date("Y-m-d H:i:s", strtotime($end.'-1 hours'));
      $dt = new DateTime($new_time);

      $utill_obj = new Common_utill();
      $db_obj_ar = array();
      $a = $utill_obj->get_all_up_ids();
      $pck_ar = $utill_obj->get_all_active_packages_promary_key();
      rsort($pck_ar);
      $time_start = microtime(true);
      for($j=0;$j<count($a);$j++){
        $full_total_left = 0;
        $full_total_right = 0;
        self::$ft_rt = 0;
        self::$ft_lt = 0;
        $user_purchase_id = $a[$j];
         // $user_purchase_id = 8; //demo
        $exist = $utill_obj->get_single_row_using_array_value('User_package_purchase','user_purchase_id',$user_purchase_id);

        $rep_qry = $em->createQuery("SELECT up.user_purchase_id FROM Entity\User_package_purchase as up INNER JOIN Entity\Pins as p WITH (up.user_purchase_pin_id = p.pin_id) WHERE p.pin_request_for = 1 AND up.user_purchase_id =".$user_purchase_id);
        $rep_obj = $rep_qry->getResult();

        $rep_cnt = 0;
        if(count($rep_obj) > 0){
          $rep_cnt = count($rep_obj);
        }else{
          $created_dt_objy = $exist->getUserId()->getCreatedAt();
          $created_aty = $created_dt_objy->format('Y-m-d');
          $nowxy = date("Y-m-d");
          $endxy = date("Y-m-d", strtotime($created_aty."+30 days"));
          $startxy = strtotime($nowxy);
          $endxy = strtotime($endxy);
          $timeDiffy = abs($endxy - $startxy);
          $numberDaysy = $timeDiffy/86400;
          if($numberDaysy == 0 || !is_integer($numberDaysy)){
              $rep_cnt = 1;
          }else{
              $rep_cnt = 1;
          }
        }

          $created_dt_obj = $exist->getUserId()->getCreatedAt();
          $created_at = $created_dt_obj->format('Y-m-d');
          $nowx = date("Y-m-d");

          $endx = date("Y-m-d", strtotime($created_at."+120 days"));
          $startx = strtotime($nowx);
          $endx = strtotime($endx);

          $timeDiff = abs($endx - $startx);
          $numberDays = $timeDiff/86400;
        // echo $exist->getPairAchieve();exit();
        if($exist->getPairAchieve() == 1){
          $tree_var = 0;

          self::$tree_left_var=array();
          self::$tree_right_var=array();
          self::$tree_up_var_left=array();
          self::$tree_up_var_right=array();
          $obj = $this->get_tree_obj($user_purchase_id,$end);

         //  echo $user_purchase_id;echo"<br>";

         //  $tree = new Tree_class();
         //  $xx = $tree->get_L_count($obj->getLeftObj());
         //  $yy = $tree->get_R_count($obj->getRightObj());
         //  // $v = $t->xx();
         //  print_r($xx);echo"<br>";
         //  print_r($yy);
         //  echo"<pre>";
         // // print_r($obj);
         //  echo"</pre>";

         //  exit();

          // for($i=0;$i<count($pck_ar);$i++){
          for($i=0;$i<2;$i++){
            // $pck_ar[$i] = 18;$pck_ar[$i]
             // $pck_ar[$i] = 20; //demo
            self::$hunbvi_left=0;
            self::$hunbvi_right=0;
            $pack = $utill_obj->get_single_row_using_primary_key('Packages',$pck_ar[$i]);

            // $cnt = $this->get_left_right_package_count($obj,$pck_ar[$i],$start,$end);

            if(!is_integer($obj->getLeftObj())){
              if(count($obj->getLeftObj() > 0)){
               $left_array  =  $obj->getLeftObj();
               $counts = array_count_values($left_array);
               if(isset($counts[$pck_ar[$i]])){
                $cnt['cnt_left'] = $counts[$pck_ar[$i]];
                $pair_ach_pck_id = 0;
                if($obj->getPairAchievePackageId() != 0 ||$obj->getPairAchievePackageId() != null ){
                  $pair_ach_pck_id = $obj->getPairAchievePackageId();
                  if(in_array($obj->getPairAchieveUserPurchaseId(), self::$tree_up_var_left)){
                    if(self::$ft_lt == 0){
                      $cnt['cnt_left'] = $cnt['cnt_left']-1;
                    }
                    self::$ft_lt++;
                  }
                }
               }else{$cnt['cnt_left'] = 0;}
              }else{ $cnt['cnt_left'] = 0; }
            }else{ $cnt['cnt_left'] = 0; }

            if(!is_integer($obj->getRightObj())){
              if(count($obj->getRightObj() > 0)){
               $right_array  =  $obj->getRightObj();
               $counts = array_count_values($right_array);
               if(isset($counts[$pck_ar[$i]])){
                $cnt['cnt_right'] = $counts[$pck_ar[$i]];
                $pair_ach_pck_id = 0;
                if($obj->getPairAchievePackageId() != 0 ||$obj->getPairAchievePackageId() != null ){
                  $pair_ach_pck_id = $obj->getPairAchievePackageId();
                  if(in_array($obj->getPairAchieveUserPurchaseId(), self::$tree_up_var_right)){
                    if(self::$ft_rt == 0){
                      $cnt['cnt_right'] = $cnt['cnt_right']-1;
                    }
                    self::$ft_rt++;
                  }
                }
               }else{$cnt['cnt_right'] = 0;}
              }else{ $cnt['cnt_right'] = 0; }
            }else{ $cnt['cnt_right'] = 0; }

            $res = $em->getRepository('Entity\Payout')->findOneBy(array('user_purchase_id' => $user_purchase_id,'package_id'=>$pck_ar[$i]));

            $db_obj = new Entity\Payout();
            $db_res_obj = new Entity\Payout_result();

            $db_obj->setUserPurchaseId($user_purchase_id);
            $db_obj->setPackageId($utill_obj->get_reference_obj('Packages',$pack->getPackageId()));
            $db_obj->setPersonalLeft(0);
            $db_obj->setPersonalRight(0);
           
            $tot_cutoff_right = 0;
            $tot_cutoff_left = 0;

            //$fin_res = $em->getRepository('Entity\Final_payout')->findOneBy(array('user_purchase_id' => $user_purchase_id,'package_id'=>$pack->getPackageId()));

            //$query = $em->createQuery("SELECT MAX(b.payout_result_id),b.payout_id,b.payout_result_left,b.total_left_pv,b.total_right_pv,b.carry_forward_pv,b.carry_forward_pv_side,b.net_payout,b.payout_result_type,b.created_at FROM Entity\Payout_result as b WHERE b.payout_id=".$payout_id);
            $query = $em->createQuery("SELECT MAX(b.payout_id) FROM Entity\Payout as b WHERE b.package_id=".$pack->getPackageId()." AND b.user_purchase_id=".$user_purchase_id);
            $single_res = $query->getResult();

            $exist_carry_left = 0;
            $exist_carry_right = 0;

            $exist_cf_amount = 0;
            $exist_cf_amount_side = 0;

            if($single_res[0][1] != null){
              $max_payout_id = $single_res[0][1];
              $fin_res = $em->getRepository('Entity\Payout')->find($max_payout_id);
              if($fin_res != null){
                //echo "<pre>";
                //print_r($fin_res);
                //echo "</pre>";exit();

                if($fin_res->getCarryForwardSide() == 0){
                  $exist_carry_left = $fin_res->getCarryForward();
                  $exist_cf_amount_side = 0;
                }else{
                  $exist_carry_right = $fin_res->getCarryForward();
                  $exist_cf_amount_side = 1;
                }

                //$pay_res = $em->getRepository('Entity\Payout_result')->findOneBy(array('payout_result_user_id'=>$max_payout_id));

                $qry = $em->createQuery("SELECT MAX(b.payout_result_id) FROM Entity\Payout_result as b WHERE b.payout_result_user_id=".$user_purchase_id);
                $s_res = $qry->getResult();
                // print_r($s_res);exit(); 
                if($s_res[0][1] != null){
                  $max_payout_res_id = $s_res[0][1];
                  $pay_resx = $em->getRepository('Entity\Payout_result')->find($max_payout_res_id);
                  if($pay_resx != null){
                    $exist_cf_amount = $pay_resx->getCarryForwardPv();
                    $exist_cf_amount_sidex = $pay_resx->getCarryForwardPvSide();
                  }
                }
              }
            }
            
            $tot_cutoff_right = $exist_carry_right+$cnt['cnt_right'];
            $tot_cutoff_left = $exist_carry_left+$cnt['cnt_left'];

            $db_obj->setCurrentLeft($cnt['cnt_left']);
            $db_obj->setCurrentRight($cnt['cnt_right']);
            
            $db_obj->setPreviousLeft($exist_carry_left);
            $db_obj->setPreviousRight($exist_carry_right);

            $db_obj->setTotalLeft($tot_cutoff_left);
            $db_obj->setTotalRight($tot_cutoff_right);
            $db_obj->onPrePersist($new_time);
            
            $carry_left = 0;
            $carry_right = 0;
            $pair_details_total = 0;
            $pair_details_flushed = 0;

            $limit_per_cut_off = $pack->getPackageLimitedPairCutoff();
            // $limit_per_cut_off = 10; demo

            $package_pv = $pack->getPackagePointValue();

            if($tot_cutoff_left > 0 || $tot_cutoff_right > 0){
              if($tot_cutoff_left >= $limit_per_cut_off && $tot_cutoff_right >= $limit_per_cut_off){
                if($tot_cutoff_left > $tot_cutoff_right){
                  $carry_left = $tot_cutoff_left-$tot_cutoff_right;
                  $pair_details_total = $limit_per_cut_off;
                  $pair_details_flushed = $tot_cutoff_right-$limit_per_cut_off;
                  $db_obj->setCarryForward($carry_left);
                  $db_obj->setCarryForwardSide(0);

                  $db_obj->setPairDetailsTotal($pair_details_total);
                  $db_obj->setPairDetailsFlushed($pair_details_flushed);
                }elseif($tot_cutoff_right > $tot_cutoff_left){
                  $carry_right = $tot_cutoff_right-$tot_cutoff_left;
                  $pair_details_total = $limit_per_cut_off;
                  $pair_details_flushed = $tot_cutoff_left-$limit_per_cut_off;
                  $db_obj->setCarryForward($carry_right);
                  $db_obj->setCarryForwardSide(1);

                  $db_obj->setPairDetailsTotal($pair_details_total);
                  $db_obj->setPairDetailsFlushed($pair_details_flushed);
                }else{
                  $pair_details_total = $limit_per_cut_off;
                  $db_obj->setCarryForward(0);
                  $db_obj->setCarryForwardSide(0);
                  $db_obj->setPairDetailsTotal(0);
                  $db_obj->setPairDetailsFlushed(0);
                }

                $db_obj->setTotalPvLeft($limit_per_cut_off*$package_pv);
                $db_obj->setTotalPvRight($limit_per_cut_off*$package_pv);

                $full_total_left += $limit_per_cut_off*$package_pv;
                $full_total_right += $limit_per_cut_off*$package_pv;
                
              }else{
                if($tot_cutoff_left >= $limit_per_cut_off){
                  $current_cf = $tot_cutoff_left-$limit_per_cut_off;
                  $current_cf_side = 0;
                  $current_cf_left_pv = $limit_per_cut_off*$package_pv;
                  $current_cf_right_pv = $tot_cutoff_right*$package_pv;

                  $db_obj->setCarryForward($current_cf);
                  $db_obj->setCarryForwardSide($current_cf_side);

                  $db_obj->setPairDetailsTotal(0);
                  $db_obj->setPairDetailsFlushed(0);

                  $db_obj->setTotalPvLeft($limit_per_cut_off*$package_pv);
                  $db_obj->setTotalPvRight($tot_cutoff_right*$package_pv);

                  $full_total_left += $limit_per_cut_off*$package_pv;
                  $full_total_right += $tot_cutoff_right*$package_pv;
                  
                }elseif($tot_cutoff_right >= $limit_per_cut_off){
                  $current_cf = $tot_cutoff_right-$limit_per_cut_off;

                  $current_cf_side = 1;
                  $current_cf_left_pv = $tot_cutoff_left*$package_pv;
                  $current_cf_right_pv = $limit_per_cut_off*$package_pv;

                  $db_obj->setCarryForward($current_cf);
                  $db_obj->setCarryForwardSide($current_cf_side);

                  $db_obj->setPairDetailsTotal(0);
                  $db_obj->setPairDetailsFlushed(0);

                  $db_obj->setTotalPvLeft($tot_cutoff_left*$package_pv);
                  $db_obj->setTotalPvRight($limit_per_cut_off*$package_pv);

                  $full_total_left += $tot_cutoff_left*$package_pv;
                  $full_total_right += $limit_per_cut_off*$package_pv;

                }else{
                  $current_cf = 0;
                  $current_cf_left_pv = $tot_cutoff_left*$package_pv;
                  $current_cf_right_pv = $tot_cutoff_right*$package_pv;

                  $db_obj->setCarryForward($current_cf);
                  $db_obj->setCarryForwardSide(0);
                  $db_obj->setPairDetailsTotal(0);
                  $db_obj->setPairDetailsFlushed(0);

                  $db_obj->setTotalPvLeft($tot_cutoff_left*$package_pv);
                  $db_obj->setTotalPvRight($tot_cutoff_right*$package_pv);

                  $full_total_left += $tot_cutoff_left*$package_pv;
                  $full_total_right += $tot_cutoff_right*$package_pv;
                }
              }
            }else{
              $db_obj->setCarryForward(0);
              $db_obj->setCarryForwardSide(0);
              $db_obj->setPairDetailsTotal(0);
              $db_obj->setPairDetailsFlushed(0);
              $db_obj->setTotalPvLeft(0);
              $db_obj->setTotalPvRight(0);
            }
            // echo"<pre>";
            // print_r($obj);
            // echo"</pre>";
            $em->persist($db_obj);
            $em->flush();

          //1
          }// Package Array End

          if($exist_cf_amount_sidex == 1){
            $full_total_right += $exist_cf_amount;
          }else{
            $full_total_left += $exist_cf_amount;
          }

          $pan_verified = 0;$aadhar_verified = 0;$pass_verified = 0;
          $tot_bal = 0;$p_sts = 0;

          $pan_verified = $exist->getUserId()->getPanVerified();
          $aadhar_verified = $exist->getUserId()->getAadharVerified();
          $pass_verified = $exist->getUserId()->getPassVerified();

          $repurchase_bal = 0;
          $pay_res = new Entity\Payout_result();
          $pay_res->setPayoutResultUserId($a[$j]);
          $pay_res->setPayoutResultLeft(0);
          $pay_res->setTotalLeftPv($full_total_left);
          $pay_res->setTotalRightPv($full_total_right);
          if($full_total_left>$full_total_right){
            $pay_res->setCarryForwardPv($full_total_left-$full_total_right);
            $pay_res->setCarryForwardPvSide(0);
            $f_t = 2*$full_total_right;
            $td = 0;
            if($f_t != 0){
              $td = 2*($f_t*5/100);//service 5% + tds 5%
              $repurchase_bal = $f_t*10/100;//repurchase 5%
              $td = $td+$repurchase_bal;
            }

            // echo 'full_total_left  -  '.$full_total_left;echo"<br>";
            // echo 'full_total_right  -  '.$full_total_right;echo"<br>";
            // echo 'ft  -  '.$f_t;echo"<br>";
            // echo 'repurchase_bal  -  '.$repurchase_bal;echo"<br>";
            // echo 'td  -  '.$td;echo"<br>";
            // echo 'Net Payout  -  '.($f_t-$td);
            // exit();
         
            if($pass_verified != 0 && $aadhar_verified != 0){
              if($pan_verified != 0){

                if($numberDays == 0 || !is_integer($numberDays)){
                  // success
                  $tot_bal = $f_t-$td;
                }else{
                  // Fail
                  if($rep_cnt > 0){
                    $tot_bal = $f_t-$td;
                  }else{
                    $tot_bal = $f_t-$td;
                    $tot_bal = $tot_bal/2;
                  }
                }
                $p_sts = 1;
              }else{
                $td += $f_t*15/100;

                if($numberDays == 0 || !is_integer($numberDays)){
                  // success
                  $tot_bal = $f_t-$td;
                }else{
                  // Fail
                  if($rep_cnt > 0){
                    $tot_bal = $f_t-$td;
                  }else{
                    $tot_bal = $f_t-$td;
                    $tot_bal = floor($tot_bal/2);
                    // echo "td===yy".$tot_bal;exit();
                  }
                }
                $p_sts = 0;
              }
            }else{
              $td = $td+($f_t*15/100);
              if($numberDays == 0 || !is_integer($numberDays)){
                  // success
                  $tot_bal = $f_t-$td;
                }else{
                  // Fail
                  if($rep_cnt > 0){
                    $tot_bal = $f_t-$td;
                  }else{
                    $tot_bal = $f_t-$td;
                    $tot_bal = floor($tot_bal/2);
                  }
                }
              $p_sts = 0;
            }
            if($exist != null){
              $pay_res->setNetPayout($tot_bal);
              $pay_res->setPaymentStatus($p_sts);
            }
          }else{
            $pay_res->setCarryForwardPv($full_total_right-$full_total_left);
            $pay_res->setCarryForwardPvSide(1);
            $f_t = $full_total_left+$full_total_left;
            $td = 0;
            if($f_t != 0){
              $td = 2*($f_t*5/100);
              $repurchase_bal = $f_t*10/100;
              $td = $td+$repurchase_bal;

              if($pass_verified != 0 && $aadhar_verified != 0){
                if($pan_verified != 0){
                  // $tot_bal = $f_t-$td;
                  if($numberDays == 0 || !is_integer($numberDays)){
                    // success
                    $tot_bal = $f_t-$td;
                  }else{
                    // Fail
                    if($rep_cnt != 0){
                      $tot_bal = $f_t-$td;
                    }else{
                      $tot_bal = $f_t-$td;
                      $tot_bal = floor($tot_bal/2);
                    }
                  }
                  $p_sts = 1;
                }else{
                  $td = $td+($f_t*15/100);
                  // $tot_bal = $f_t-$td;/
                  if($numberDays == 0 || !is_integer($numberDays)){
                    // success
                    $tot_bal = $f_t-$td;
                  }else{
                    // Fail
                    if($rep_cnt != 0){
                      $tot_bal = $f_t-$td;
                    }else{
                      $tot_bal = $f_t-$td;
                      $tot_bal = floor($tot_bal/2);
                    }
                  }
                  $p_sts = 0;
                }
              }else{
                $td = $td+($f_t*15/100);
                $tot_bal = $f_t-$td;
                $p_sts = 0;
              }

            // echo 'full_total_left  -  '.$full_total_left;echo"<br>";
            // echo 'full_total_right  -  '.$full_total_right;echo"<br>";
            // echo 'ft  -  '.$f_t;echo"<br>";
            // echo 'repurchase_bal  -  '.$repurchase_bal;echo"<br>";
            // echo 'td  -  '.$td;echo"<br>";
            // echo 'Net Payout  -  '.($f_t-$td);echo"<br><br><br><br>";

            }
            if($exist != null){
              $pay_res->setNetPayout($tot_bal);
              $pay_res->setPaymentStatus($p_sts);
            }
            $pay_res->setNetPayout($f_t-$td);
          }

          $res_up = $em->getRepository('Entity\User_package_purchase')->find($user_purchase_id);
          if($res_up != null){
            $u_obj = $res_up->getUserId();
            if($u_obj != null){
              $u_id = $u_obj->getUserId();
              $ua_res = $em->getRepository('Entity\User_account')->findOneBy(array('user_id'=>$u_id));
              if($ua_res != null){
                $prev_up_amnt = $ua_res->getUserAccountRepurchaseAmount();
                $new_bal = $repurchase_bal+$prev_up_amnt;
                $prev_up_amnt = $ua_res->setUserAccountRepurchaseAmount($new_bal);
                $em->persist($ua_res);
                $em->flush();
              }
            }
          }

          $pay_res->setPayoutResultType(0);
          $pay_res->onPrePersist($new_time);
          $em->persist($pay_res);
          $em->flush();

          // exit();
          //2  
           $exist = $utill_obj->get_single_row_using_array_value('User_package_purchase','user_purchase_id',$user_purchase_id);      
          if($exist != null){
            $exist->setPayoutReleased(1);
            $exist->setUpdatedAt($new_time);
            $em->persist($exist);
            $em->flush();
            
          }
        }
        
      }
      // $time_end = microtime(true);
      // echo $time = $time_end - $time_start;
      // exit();
      return true;
    }