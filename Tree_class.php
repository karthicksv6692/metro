<?php
  defined('BASEPATH') OR exit('No direct script address allowed');
  class Tree_class{
    public $pck_ar_l=[];
    public $pck_ar_r=[];

    public function __construct() {
      $utill_obj = new Common_utill();
      
      $pck_arrs = $utill_obj->get_all_active_packages_promary_key();

      foreach ($pck_arrs as $key=>$value) {
        $this->pck_ar_l[$value]=0;
        $this->pck_ar_r[$value]=0;
      }
      //$this->pck_ar_r=$this->pck_ar_l;
    }

    public function get_L_count($obj){
      $this->getLCountByID($obj);
      return $this->pck_ar_l;
    }
    public function get_R_count($obj){
      $this->getRCountByID($obj);
      return $this->pck_ar_r;
    }

    public function getLCountByID($obj){
      if($obj->getLeftObj() != null){
          $this->getLPackageCount($obj->getLeftObj());
      }
    }

    public function getRCountByID($obj){
      if($obj->getRightObj() != null){
        $this->getRPackageCount($obj->getRightObj());
      }
    }

    public function getLPackageCount($obj){
      if($obj != null){   
        for($i=0;$i<count($obj);$i++){
          $this->pck_ar_l[$obj[$i]]++;
        }
      }
    }
    // public function getRPackageCount($obj,$end,$prid){
    public function getRPackageCount($obj){
      if($obj != null){
        for($i=0;$i<count($obj);$i++){
          $this->pck_ar_r[$obj[$i]]++;
        }
      }
    }

    
  }
?>