<?php
defined('BASEPATH') OR exit('No direct script address allowed');
  use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
  use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
  use Doctrine\ORM\Query\ResultSetMapping;
  require_once 'ssp.customized.class.php';
  require_once 'Tree_class.php';
  class Payout_demo extends CI_Controller {

    protected static $cnt=0;
    protected static $ar=array();
    protected static $obj_ar=array();

    protected static $hunbvi_left=0;
    protected static $hunbvi_right=0;

    protected static $tree_var=0;

    protected static $tree_left_var=array();
    protected static $tree_right_var=array();
    protected static $tree_up_var_left = array();
    protected static $tree_up_var_right = array();
    protected static $ft_rt = 0;
    protected static $ft_lt = 0;


    public function __construct(){
      parent::__construct();
      is_user_logged_in();
    }
    public function index(){
      if($this->input->post('start') !=null && $this->input->post('end') != null){
        $start = $this->input->post('start');
        $end = $this->input->post('end');
        $em = $this->doctrine->em;
        // $em->getConnection()->beginTransaction();
        $ret = $this->payout_work($start,$end);
        if($ret == 1){
          // $em->getConnection()->commit();
          echo 'Success';
        }else{
          // $em->getConnection()->rollBack();
          echo 'Fail';
        }
      }else{
        $this->load->view('header');
        $this->load->view('payout/Payout_demo');
        $this->load->view('footer');  
      }
    }
    public function payout_work($start,$end){
      $em = $this->doctrine->em;
      
      $new_time = date("Y-m-d H:i:s", strtotime($end.'-1 hours'));
      $dt = new DateTime($new_time);

      $utill_obj = new Common_utill();
      $db_obj_ar = array();
      $a = $utill_obj->get_all_up_ids();
      $pck_ar = $utill_obj->get_all_active_packages_promary_key();
      rsort($pck_ar);
      $time_start = microtime(true);
      for($j=0;$j<count($a);$j++){
        $full_total_left = 0;
        $full_total_right = 0;
        self::$ft_rt = 0;
        self::$ft_lt = 0;
        $user_purchase_id = $a[$j];
         // $user_purchase_id = 8; //demo
        $exist = $utill_obj->get_single_row_using_array_value('User_package_purchase','user_purchase_id',$user_purchase_id);

        $rep_qry = $em->createQuery("SELECT up.user_purchase_id FROM Entity\User_package_purchase as up INNER JOIN Entity\Pins as p WITH (up.user_purchase_pin_id = p.pin_id) WHERE p.pin_request_for = 1 AND up.user_purchase_id =".$user_purchase_id);
        $rep_obj = $rep_qry->getResult();

        $rep_cnt = 0;
        if(count($rep_obj) > 0){
          $rep_cnt = count($rep_obj);
        }else{
          $created_dt_objy = $exist->getUserId()->getCreatedAt();
          $created_aty = $created_dt_objy->format('Y-m-d');
          $nowxy = date("Y-m-d");
          $endxy = date("Y-m-d", strtotime($created_aty."+30 days"));
          $startxy = strtotime($nowxy);
          $endxy = strtotime($endxy);
          $timeDiffy = abs($endxy - $startxy);
          $numberDaysy = $timeDiffy/86400;
          if($numberDaysy == 0 || !is_integer($numberDaysy)){
              $rep_cnt = 1;
          }else{
              $rep_cnt = 1;
          }
        }

          $created_dt_obj = $exist->getUserId()->getCreatedAt();
          $created_at = $created_dt_obj->format('Y-m-d');
          $nowx = date("Y-m-d");

          $endx = date("Y-m-d", strtotime($created_at."+120 days"));
          $startx = strtotime($nowx);
          $endx = strtotime($endx);

          $timeDiff = abs($endx - $startx);
          $numberDays = $timeDiff/86400;
        // echo $exist->getPairAchieve();exit();
        if($exist->getPairAchieve() == 1){
          $tree_var = 0;

          self::$tree_left_var=array();
          self::$tree_right_var=array();
          self::$tree_up_var_left=array();
          self::$tree_up_var_right=array();
          $obj = $this->get_only_tree_obj($user_purchase_id,$end);

         //  echo $user_purchase_id;echo"<br>";

          $tree = new Tree_class();
          $xx = $tree->get_L_count($obj);
          $yy = $tree->get_R_count($obj);
          // $v = $t->xx();
          print_r($xx);echo"<br>";
          print_r($yy);echo"<br>";

          echo array_sum ( $xx );echo"<br>";
          echo array_sum ( $yy );echo"<br>";
          echo"<pre>";
         // print_r($obj);
          echo"</pre>";

          exit();

          for($i=0;$i<count($pck_ar);$i++){
            // $pck_ar[$i] = 18;$pck_ar[$i]
             // $pck_ar[$i] = 20; //demo
            self::$hunbvi_left=0;
            self::$hunbvi_right=0;
            $pack = $utill_obj->get_single_row_using_primary_key('Packages',$pck_ar[$i]);

            // $cnt = $this->get_left_right_package_count($obj,$pck_ar[$i],$start,$end);

            if(!is_integer($obj->getLeftObj())){
              if(count($obj->getLeftObj() > 0)){
               $left_array  =  $obj->getLeftObj();
               $counts = array_count_values($left_array);
               if(isset($counts[$pck_ar[$i]])){
                $cnt['cnt_left'] = $counts[$pck_ar[$i]];
                $pair_ach_pck_id = 0;
                if($obj->getPairAchievePackageId() != 0 ||$obj->getPairAchievePackageId() != null ){
                  $pair_ach_pck_id = $obj->getPairAchievePackageId();
                  
                  if(in_array($obj->getPairAchieveUserPurchaseId(), self::$tree_up_var_left)){
                    if(self::$ft_lt == 0){
                      $cnt['cnt_left'] = $cnt['cnt_left']-1;
                    }
                    self::$ft_lt++;
                  }
                }
               }else{$cnt['cnt_left'] = 0;}
              }else{ $cnt['cnt_left'] = 0; }
            }else{ $cnt['cnt_left'] = 0; }

            if(!is_integer($obj->getRightObj())){
              if(count($obj->getRightObj() > 0)){
               $right_array  =  $obj->getRightObj();
               $counts = array_count_values($right_array);
               if(isset($counts[$pck_ar[$i]])){
                $cnt['cnt_right'] = $counts[$pck_ar[$i]];
                $pair_ach_pck_id = 0;
                if($obj->getPairAchievePackageId() != 0 ||$obj->getPairAchievePackageId() != null ){
                  $pair_ach_pck_id = $obj->getPairAchievePackageId();
                  if(in_array($obj->getPairAchieveUserPurchaseId(), self::$tree_up_var_right)){
                    if(self::$ft_rt == 0){
                      $cnt['cnt_right'] = $cnt['cnt_right']-1;
                    }
                    self::$ft_rt++;
                  }
                }
               }else{$cnt['cnt_right'] = 0;}
              }else{ $cnt['cnt_right'] = 0; }
            }else{ $cnt['cnt_right'] = 0; }

            $res = $em->getRepository('Entity\Payout')->findOneBy(array('user_purchase_id' => $user_purchase_id,'package_id'=>$pck_ar[$i]));

            $db_obj = new Entity\Payout();
            $db_res_obj = new Entity\Payout_result();

            $db_obj->setUserPurchaseId($user_purchase_id);
            $db_obj->setPackageId($utill_obj->get_reference_obj('Packages',$pack->getPackageId()));
            $db_obj->setPersonalLeft(0);
            $db_obj->setPersonalRight(0);
           
            $tot_cutoff_right = 0;
            $tot_cutoff_left = 0;

            //$fin_res = $em->getRepository('Entity\Final_payout')->findOneBy(array('user_purchase_id' => $user_purchase_id,'package_id'=>$pack->getPackageId()));

            //$query = $em->createQuery("SELECT MAX(b.payout_result_id),b.payout_id,b.payout_result_left,b.total_left_pv,b.total_right_pv,b.carry_forward_pv,b.carry_forward_pv_side,b.net_payout,b.payout_result_type,b.created_at FROM Entity\Payout_result as b WHERE b.payout_id=".$payout_id);
            $query = $em->createQuery("SELECT MAX(b.payout_id) FROM Entity\Payout as b WHERE b.package_id=".$pack->getPackageId()." AND b.user_purchase_id=".$user_purchase_id);
            $single_res = $query->getResult();

            $exist_carry_left = 0;
            $exist_carry_right = 0;

            $exist_cf_amount = 0;
            $exist_cf_amount_side = 0;

            if($single_res[0][1] != null){
              $max_payout_id = $single_res[0][1];
              $fin_res = $em->getRepository('Entity\Payout')->find($max_payout_id);
              if($fin_res != null){
                //echo "<pre>";
                //print_r($fin_res);
                //echo "</pre>";exit();

                if($fin_res->getCarryForwardSide() == 0){
                  $exist_carry_left = $fin_res->getCarryForward();
                  $exist_cf_amount_side = 0;
                }else{
                  $exist_carry_right = $fin_res->getCarryForward();
                  $exist_cf_amount_side = 1;
                }

                //$pay_res = $em->getRepository('Entity\Payout_result')->findOneBy(array('payout_result_user_id'=>$max_payout_id));

                $qry = $em->createQuery("SELECT MAX(b.payout_result_id) FROM Entity\Payout_result as b WHERE b.payout_result_user_id=".$user_purchase_id);
                $s_res = $qry->getResult();
                // print_r($s_res);exit(); 
                if($s_res[0][1] != null){
                  $max_payout_res_id = $s_res[0][1];
                  $pay_resx = $em->getRepository('Entity\Payout_result')->find($max_payout_res_id);
                  if($pay_resx != null){
                    $exist_cf_amount = $pay_resx->getCarryForwardPv();
                    $exist_cf_amount_sidex = $pay_resx->getCarryForwardPvSide();
                  }
                }
              }
            }
            
            $tot_cutoff_right = $exist_carry_right+$cnt['cnt_right'];
            $tot_cutoff_left = $exist_carry_left+$cnt['cnt_left'];

            $db_obj->setCurrentLeft($cnt['cnt_left']);
            $db_obj->setCurrentRight($cnt['cnt_right']);
            
            $db_obj->setPreviousLeft($exist_carry_left);
            $db_obj->setPreviousRight($exist_carry_right);

            $db_obj->setTotalLeft($tot_cutoff_left);
            $db_obj->setTotalRight($tot_cutoff_right);
            $db_obj->onPrePersist($new_time);
            
            $carry_left = 0;
            $carry_right = 0;
            $pair_details_total = 0;
            $pair_details_flushed = 0;

            $limit_per_cut_off = $pack->getPackageLimitedPairCutoff();
            // $limit_per_cut_off = 10; demo

            $package_pv = $pack->getPackagePointValue();

            if($tot_cutoff_left > 0 || $tot_cutoff_right > 0){
              if($tot_cutoff_left >= $limit_per_cut_off && $tot_cutoff_right >= $limit_per_cut_off){
                if($tot_cutoff_left > $tot_cutoff_right){
                  $carry_left = $tot_cutoff_left-$tot_cutoff_right;
                  $pair_details_total = $limit_per_cut_off;
                  $pair_details_flushed = $tot_cutoff_right-$limit_per_cut_off;
                  $db_obj->setCarryForward($carry_left);
                  $db_obj->setCarryForwardSide(0);

                  $db_obj->setPairDetailsTotal($pair_details_total);
                  $db_obj->setPairDetailsFlushed($pair_details_flushed);
                }elseif($tot_cutoff_right > $tot_cutoff_left){
                  $carry_right = $tot_cutoff_right-$tot_cutoff_left;
                  $pair_details_total = $limit_per_cut_off;
                  $pair_details_flushed = $tot_cutoff_left-$limit_per_cut_off;
                  $db_obj->setCarryForward($carry_right);
                  $db_obj->setCarryForwardSide(1);

                  $db_obj->setPairDetailsTotal($pair_details_total);
                  $db_obj->setPairDetailsFlushed($pair_details_flushed);
                }else{
                  $pair_details_total = $limit_per_cut_off;
                  $db_obj->setCarryForward(0);
                  $db_obj->setCarryForwardSide(0);
                  $db_obj->setPairDetailsTotal(0);
                  $db_obj->setPairDetailsFlushed(0);
                }

                $db_obj->setTotalPvLeft($limit_per_cut_off*$package_pv);
                $db_obj->setTotalPvRight($limit_per_cut_off*$package_pv);

                $full_total_left += $limit_per_cut_off*$package_pv;
                $full_total_right += $limit_per_cut_off*$package_pv;
                
              }else{
                if($tot_cutoff_left >= $limit_per_cut_off){
                  $current_cf = $tot_cutoff_left-$limit_per_cut_off;
                  $current_cf_side = 0;
                  $current_cf_left_pv = $limit_per_cut_off*$package_pv;
                  $current_cf_right_pv = $tot_cutoff_right*$package_pv;

                  $db_obj->setCarryForward($current_cf);
                  $db_obj->setCarryForwardSide($current_cf_side);

                  $db_obj->setPairDetailsTotal(0);
                  $db_obj->setPairDetailsFlushed(0);

                  $db_obj->setTotalPvLeft($limit_per_cut_off*$package_pv);
                  $db_obj->setTotalPvRight($tot_cutoff_right*$package_pv);

                  $full_total_left += $limit_per_cut_off*$package_pv;
                  $full_total_right += $tot_cutoff_right*$package_pv;
                  
                }elseif($tot_cutoff_right >= $limit_per_cut_off){
                  $current_cf = $tot_cutoff_right-$limit_per_cut_off;

                  $current_cf_side = 1;
                  $current_cf_left_pv = $tot_cutoff_left*$package_pv;
                  $current_cf_right_pv = $limit_per_cut_off*$package_pv;

                  $db_obj->setCarryForward($current_cf);
                  $db_obj->setCarryForwardSide($current_cf_side);

                  $db_obj->setPairDetailsTotal(0);
                  $db_obj->setPairDetailsFlushed(0);

                  $db_obj->setTotalPvLeft($tot_cutoff_left*$package_pv);
                  $db_obj->setTotalPvRight($limit_per_cut_off*$package_pv);

                  $full_total_left += $tot_cutoff_left*$package_pv;
                  $full_total_right += $limit_per_cut_off*$package_pv;

                }else{
                  $current_cf = 0;
                  $current_cf_left_pv = $tot_cutoff_left*$package_pv;
                  $current_cf_right_pv = $tot_cutoff_right*$package_pv;

                  $db_obj->setCarryForward($current_cf);
                  $db_obj->setCarryForwardSide(0);
                  $db_obj->setPairDetailsTotal(0);
                  $db_obj->setPairDetailsFlushed(0);

                  $db_obj->setTotalPvLeft($tot_cutoff_left*$package_pv);
                  $db_obj->setTotalPvRight($tot_cutoff_right*$package_pv);

                  $full_total_left += $tot_cutoff_left*$package_pv;
                  $full_total_right += $tot_cutoff_right*$package_pv;
                }
              }
            }else{
              $db_obj->setCarryForward(0);
              $db_obj->setCarryForwardSide(0);
              $db_obj->setPairDetailsTotal(0);
              $db_obj->setPairDetailsFlushed(0);
              $db_obj->setTotalPvLeft(0);
              $db_obj->setTotalPvRight(0);
            }
            // echo"<pre>";
            // print_r($obj);
            // echo"</pre>";
            $em->persist($db_obj);
            $em->flush();

          //1
          }// Package Array End

          if($exist_cf_amount_sidex == 1){
            $full_total_right += $exist_cf_amount;
          }else{
            $full_total_left += $exist_cf_amount;
          }

          $pan_verified = 0;$aadhar_verified = 0;$pass_verified = 0;
          $tot_bal = 0;$p_sts = 0;

          $pan_verified = $exist->getUserId()->getPanVerified();
          $aadhar_verified = $exist->getUserId()->getAadharVerified();
          $pass_verified = $exist->getUserId()->getPassVerified();

          $repurchase_bal = 0;
          $pay_res = new Entity\Payout_result();
          $pay_res->setPayoutResultUserId($a[$j]);
          $pay_res->setPayoutResultLeft(0);
          $pay_res->setTotalLeftPv($full_total_left);
          $pay_res->setTotalRightPv($full_total_right);
          if($full_total_left>$full_total_right){
            $pay_res->setCarryForwardPv($full_total_left-$full_total_right);
            $pay_res->setCarryForwardPvSide(0);
            $f_t = 2*$full_total_right;
            $td = 0;
            if($f_t != 0){
              $td = 2*($f_t*5/100);//service 5% + tds 5%
              $repurchase_bal = $f_t*10/100;//repurchase 5%
              $td = $td+$repurchase_bal;
            }

            // echo 'full_total_left  -  '.$full_total_left;echo"<br>";
            // echo 'full_total_right  -  '.$full_total_right;echo"<br>";
            // echo 'ft  -  '.$f_t;echo"<br>";
            // echo 'repurchase_bal  -  '.$repurchase_bal;echo"<br>";
            // echo 'td  -  '.$td;echo"<br>";
            // echo 'Net Payout  -  '.($f_t-$td);
            // exit();
         
            if($pass_verified != 0 && $aadhar_verified != 0){
              if($pan_verified != 0){

                if($numberDays == 0 || !is_integer($numberDays)){
                  // success
                  $tot_bal = $f_t-$td;
                }else{
                  // Fail
                  if($rep_cnt > 0){
                    $tot_bal = $f_t-$td;
                  }else{
                    $tot_bal = $f_t-$td;
                    $tot_bal = $tot_bal/2;
                  }
                }
                $p_sts = 1;
              }else{
                $td += $f_t*15/100;

                if($numberDays == 0 || !is_integer($numberDays)){
                  // success
                  $tot_bal = $f_t-$td;
                }else{
                  // Fail
                  if($rep_cnt > 0){
                    $tot_bal = $f_t-$td;
                  }else{
                    $tot_bal = $f_t-$td;
                    $tot_bal = floor($tot_bal/2);
                    // echo "td===yy".$tot_bal;exit();
                  }
                }
                $p_sts = 0;
              }
            }else{
              $td = $td+($f_t*15/100);
              if($numberDays == 0 || !is_integer($numberDays)){
                  // success
                  $tot_bal = $f_t-$td;
                }else{
                  // Fail
                  if($rep_cnt > 0){
                    $tot_bal = $f_t-$td;
                  }else{
                    $tot_bal = $f_t-$td;
                    $tot_bal = floor($tot_bal/2);
                  }
                }
              $p_sts = 0;
            }
            if($exist != null){
              $pay_res->setNetPayout($tot_bal);
              $pay_res->setPaymentStatus($p_sts);
            }
          }else{
            $pay_res->setCarryForwardPv($full_total_right-$full_total_left);
            $pay_res->setCarryForwardPvSide(1);
            $f_t = $full_total_left+$full_total_left;
            $td = 0;
            if($f_t != 0){
              $td = 2*($f_t*5/100);
              $repurchase_bal = $f_t*10/100;
              $td = $td+$repurchase_bal;

              if($pass_verified != 0 && $aadhar_verified != 0){
                if($pan_verified != 0){
                  // $tot_bal = $f_t-$td;
                  if($numberDays == 0 || !is_integer($numberDays)){
                    // success
                    $tot_bal = $f_t-$td;
                  }else{
                    // Fail
                    if($rep_cnt != 0){
                      $tot_bal = $f_t-$td;
                    }else{
                      $tot_bal = $f_t-$td;
                      $tot_bal = floor($tot_bal/2);
                    }
                  }
                  $p_sts = 1;
                }else{
                  $td = $td+($f_t*15/100);
                  // $tot_bal = $f_t-$td;/
                  if($numberDays == 0 || !is_integer($numberDays)){
                    // success
                    $tot_bal = $f_t-$td;
                  }else{
                    // Fail
                    if($rep_cnt != 0){
                      $tot_bal = $f_t-$td;
                    }else{
                      $tot_bal = $f_t-$td;
                      $tot_bal = floor($tot_bal/2);
                    }
                  }
                  $p_sts = 0;
                }
              }else{
                $td = $td+($f_t*15/100);
                $tot_bal = $f_t-$td;
                $p_sts = 0;
              }

            // echo 'full_total_left  -  '.$full_total_left;echo"<br>";
            // echo 'full_total_right  -  '.$full_total_right;echo"<br>";
            // echo 'ft  -  '.$f_t;echo"<br>";
            // echo 'repurchase_bal  -  '.$repurchase_bal;echo"<br>";
            // echo 'td  -  '.$td;echo"<br>";
            // echo 'Net Payout  -  '.($f_t-$td);echo"<br><br><br><br>";

            }
            if($exist != null){
              $pay_res->setNetPayout($tot_bal);
              $pay_res->setPaymentStatus($p_sts);
            }
            $pay_res->setNetPayout($f_t-$td);
          }

          $res_up = $em->getRepository('Entity\User_package_purchase')->find($user_purchase_id);
          if($res_up != null){
            $u_obj = $res_up->getUserId();
            if($u_obj != null){
              $u_id = $u_obj->getUserId();
              $ua_res = $em->getRepository('Entity\User_account')->findOneBy(array('user_id'=>$u_id));
              if($ua_res != null){
                $prev_up_amnt = $ua_res->getUserAccountRepurchaseAmount();
                $new_bal = $repurchase_bal+$prev_up_amnt;
                $prev_up_amnt = $ua_res->setUserAccountRepurchaseAmount($new_bal);
                $em->persist($ua_res);
                $em->flush();
              }
            }
          }

          $pay_res->setPayoutResultType(0);
          $pay_res->onPrePersist($new_time);
          $em->persist($pay_res);
          $em->flush();

          // exit();
          //2  
           $exist = $utill_obj->get_single_row_using_array_value('User_package_purchase','user_purchase_id',$user_purchase_id);      
          if($exist != null){
            $exist->setPayoutReleased(1);
            $exist->setUpdatedAt($new_time);
            $em->persist($exist);
            $em->flush();
            
          }
        }
        
      }
      // $time_end = microtime(true);
      // echo $time = $time_end - $time_start;
      // exit();
      return true;
    }
    public function get_left_right_package_count($obj,$id,$start,$end){
     
      // $now_time = new \DateTime("now");
      $now_time = new \DateTime($end);

      if(!is_numeric($obj->getLeftObj())){
        if($obj->getPayoutReleased() == 1){
          if($now_time->format('H') >= 13){
              $dt = $obj->getLeftObj()->getCreatedAt()->format('Y-m-d H:i:s');
              $check = $this->second_cutoffx($start,$end);
              if($check == 1){
                if( ! is_integer($obj->getLeftObj())){  
                  if($obj->getLeftObj()->getPackageId() == $id && $obj->getPairAchieveUserPurchaseId() != $obj->getLeftObj()->getPurchaseId()){
                    self::$hunbvi_left=1;
                  }
                }else{
                  if($obj->getLeftObj()->getPackageId() == $id){
                    self::$hunbvi_left=1;
                  }
                }
              }
          }else{
            $dt = $obj->getLeftObj()->getCreatedAt()->format('Y-m-d H:i:s');
              $check = $this->first_cutoffx($start,$end);
              if($check == 1){
                if( ! is_integer($obj->getLeftObj())){  
                  if($obj->getLeftObj()->getPackageId() == $id  && $obj->getPairAchieveUserPurchaseId() != $obj->getLeftObj()->getPurchaseId()){
                    self::$hunbvi_left=1;
                  }
                }else{
                  if($obj->getLeftObj()->getPackageId() == $id){
                    self::$hunbvi_left=1;
                  }
                }
              }
          }
        }else{
          if( ! is_integer($obj->getLeftObj())){  
            if($obj->getLeftObj()->getPackageId() == $id && $obj->getPairAchieveUserPurchaseId() != $obj->getLeftObj()->getPurchaseId()){
              self::$hunbvi_left=1;
            }
          }else{
            if($obj->getLeftObj()->getPackageId() == $id){
              self::$hunbvi_left++;
            }
          }
        }
        $this->printTreeL($obj->getLeftObj(),$id,$obj,$start,$end);
      }
      if(!is_numeric($obj->getRightObj())){
        if($obj->getPayoutReleased() == 1){
          if($now_time->format('H') >= 13){
              $dt = $obj->getRightObj()->getCreatedAt()->format('Y-m-d H:i:s');
              $check = $this->second_cutoff($start,$end);
              if($check == 1){
                if( ! is_integer($obj->getRightObj())){  
                  if($obj->getRightObj()->getPackageId() == $id  && $obj->getPairAchieveUserPurchaseId() != $obj->getRightObj()->getPurchaseId()){
                    self::$hunbvi_right=1;
                  }
                }else{
                  if($obj->getRightObj()->getPackageId() == $id){
                    self::$hunbvi_left=1;
                  }
                }
              }
          }else{
            $dt = $obj->getRightObj()->getCreatedAt()->format('Y-m-d H:i:s');
              $check = $this->first_cutoffx($start,$end);
              if($check == 1){
                if( ! is_integer($obj->getRightObj())){  
                  if($obj->getRightObj()->getPackageId() == $id && $obj->getPairAchieveUserPurchaseId() != $obj->getRightObj()->getPurchaseId() ){
                    self::$hunbvi_right=1;
                  }
                }else{
                  if($obj->getRightObj()->getPackageId() == $id){
                    self::$hunbvi_right=1;
                  }
                }
                
              }
          }
        }else{
          if( ! is_integer($obj->getRightObj())){  
            if($obj->getRightObj()->getPackageId() == $id && $obj->getPairAchieveUserPurchaseId()  != $obj->getRightObj()->getPurchaseId() ){
              self::$hunbvi_right=1;
            }
          }else{
            if($obj->getRightObj()->getPackageId() == $id){
              self::$hunbvi_right=1;
            }
          }
        }
          
        }
        $this->printTreeR($obj->getRightObj(),$id,$obj,$start,$end);
      return ['cnt_left'=>self::$hunbvi_left,'cnt_right'=>self::$hunbvi_right];
    }
    public function printTreeL($obj,$id,$robj,$start,$end){
      $now_time = new \DateTime("now");
      // $now_time = new \DateTime($end);
      // echo $obj->getUserId();echo"<br>";
      if(!is_numeric($obj->getLeftObj())){
        if($robj->getPayoutReleased() == 1){
          if($now_time->format('H') >= 13){ 
              $dt = $obj->getLeftObj()->getCreatedAt()->format('Y-m-d H:i:s');
              $check = $this->second_cutoffx($start,$end);
              if($check == 1){
                if($obj->getLeftObj()->getPackageId() == $id && $robj->getPairAchieveUserPurchaseId()  != $obj->getLeftObj()->getPurchaseId() ){
                  self::$hunbvi_left++;
                }
              }
          }else{
            $dt = $obj->getLeftObj()->getCreatedAt()->format('Y-m-d H:i:s');
              $check = $this->first_cutoffx($start,$end);
              if($check == 1){
                if($obj->getLeftObj()->getPackageId() == $id && $robj->getPairAchieveUserPurchaseId()  != $obj->getLeftObj()->getPurchaseId() ){
                  self::$hunbvi_left++;
                }
              }
          }
        }else{
          if( ! is_integer($obj->getLeftObj())){  
            if($obj->getLeftObj()->getPackageId() == $id && $robj->getPairAchieveUserPurchaseId()  != $obj->getLeftObj()->getPurchaseId()){
              self::$hunbvi_left++;
            }
          }else{
            if($obj->getLeftObj()->getPackageId() == $id && $robj->getPairAchieveUserPurchaseId()  != $obj->getLeftObj()->getPurchaseId()){
              self::$hunbvi_left++;
            }
          }
          
        }
        $this->printTreeL($obj->getLeftObj(),$id,$robj,$start,$end);
      }
      if(!is_numeric($obj->getRightObj())){
        if($robj->getPayoutReleased() == 1){
          if($now_time->format('H') >= 13){
              $dt = $obj->getRightObj()->getCreatedAt()->format('Y-m-d H:i:s');
              $check = $this->second_cutoffx($start,$end);
              if($check == 1){
                if($obj->getRightObj()->getPackageId() == $id  && $robj->getPairAchieveUserPurchaseId()  != $obj->getRightObj()->getPurchaseId()){
                  self::$hunbvi_left++;
                }
              }
          }else{
            $dt = $obj->getRightObj()->getCreatedAt()->format('Y-m-d H:i:s');
              $check = $this->first_cutoffx($start,$end);
              if($check == 1){
                if($obj->getRightObj()->getPackageId() == $id && $robj->getPairAchieveUserPurchaseId()  != $obj->getRightObj()->getPurchaseId() ){
                  self::$hunbvi_left++;
                }
              }
          }
        }else{
          if($obj->getRightObj()->getPackageId() == $id  && $robj->getPairAchieveUserPurchaseId()  != $obj->getRightObj()->getPurchaseId()){
            self::$hunbvi_left++;
          }
        }
        $this->printTreeL($obj->getRightObj(),$id,$robj,$start,$end);
      }
    }
    public function printTreeR($obj,$id,$robj,$start,$end){
      $now_time = new \DateTime("now");
      // $now_time = new \DateTime($end);
       //echo $obj->getUserId();echo"<br>";
      // echo $robj->getUserId();echo"<br>";
      if(!is_numeric($obj->getLeftObj())){
        if($robj->getPayoutReleased() == 1){
          if($now_time->format('H') >= 13){
              $dt = $obj->getLeftObj()->getCreatedAt()->format('Y-m-d H:i:s');
              $check = $this->second_cutoffx($start,$end);
              if($check == 1){
                if($obj->getLeftObj()->getPackageId() == $id  && $robj->getPairAchieveUserPurchaseId()  != $obj->getLeftObj()->getPurchaseId()){
                  self::$hunbvi_right++;
                }
              }
          }else{
            $dt = $obj->getLeftObj()->getCreatedAt()->format('Y-m-d H:i:s');
              $check = $this->first_cutoffx($start,$end);
              if($check == 1){
                if($obj->getLeftObj()->getPackageId() == $id  && $robj->getPairAchieveUserPurchaseId()  != $obj->getLeftObj()->getPurchaseId()){
                  self::$hunbvi_right++;
                }
              }
          }
        }else{
          if($obj->getLeftObj()->getPackageId() == $id  && $robj->getPairAchieveUserPurchaseId()  != $obj->getLeftObj()->getPurchaseId()){
            self::$hunbvi_right++;
          }
        }
        $this->printTreeR($obj->getLeftObj(),$id,$robj,$start,$end);
      }
      if(!is_numeric($obj->getRightObj())){
        if($robj->getPayoutReleased() == 1){
          if($now_time->format('H') >= 13){
              $dt = $obj->getRightObj()->getCreatedAt()->format('Y-m-d H:i:s');
              $check = $this->second_cutoffx($start,$end);
              if($check == 1){
                if($obj->getRightObj()->getPackageId() == $id  && $robj->getPairAchieveUserPurchaseId()  != $obj->getRightObj()->getPurchaseId()){
                  self::$hunbvi_right++;
                }
              }
          }else{
            $dt = $obj->getRightObj()->getCreatedAt()->format('Y-m-d H:i:s');
              $check = $this->first_cutoffx($start,$end);
              if($check == 1){
                if($obj->getRightObj()->getPackageId() == $id  && $robj->getPairAchieveUserPurchaseId()  != $obj->getRightObj()->getPurchaseId() ){
                  self::$hunbvi_right++;
                }
              }
          }
        }else{
          if($obj->getRightObj()->getPackageId() == $id  && $robj->getPairAchieveUserPurchaseId()  != $obj->getRightObj()->getPurchaseId() ){
              self::$hunbvi_right++;
            }
        }
        $this->printTreeR($obj->getRightObj(),$id,$robj,$start,$end);
      }
    }
    public function dt(){
      $d = "2017-10-05 02:01:00";
      $this->time_check($d);
    }
    public function first_cutoff($dt){
      // Timing Same Day 02:00 To Same Day 14:00
      $now_time = new \DateTime("now");
      $first_time = $now_time->format('Y-m-d H:i:s');
      // echo $first_time;echo"&nbsp;&nbsp;&nbsp;";
      // echo $dt;echo"<br><br>";
      // exit();
      $first_time = new DateTime($first_time);
      $common_time = new DateTime($dt);
      $first_interval = $common_time->diff($first_time);
      if($first_interval->d == 0 && $first_interval->y == 0 && $first_interval->m == 0){
        if($first_interval->h < 12){
          if($first_interval->i <= 59 && $first_interval->s <= 59){
            return 1;
          }else{return fasle;}
        }else{return false;}
      }else{ return false; }
    }
    public function first_cutoffx($from,$to){
      $start = new DateTime($from);
      $end = new DateTime($to);
      $first_interval = $start->diff($end);
      if($first_interval->d == 0 && $first_interval->y == 0 && $first_interval->m == 0){
        if($first_interval->h < 12){
          if($first_interval->i <= 59 && $first_interval->s <= 59){
            return 1;
          }else{return fasle;}
        }else{return false;}
      }else{ return false; }
    }
    public function second_cutoff($dt){
      // Timing Same Day 14:01 To Next Day 01:59
      $now_time = new \DateTime("now");
      // $second_time = $now_time->format('Y-m-d 02:00:00');
      $second_time = $now_time->format('Y-m-d H:i:s');
      // echo"---------------------------------------";echo"<br>";
      // echo $second_time;echo"&nbsp;&nbsp;&nbsp;";
      // echo $dt;echo"<br>";
      $second_time = new DateTime($second_time);
      //$second_time = $second_time->modify('+1 day');
      // print_r($second_time);
      $common_time = new DateTime($dt);
      $first_interval = $common_time->diff($second_time);
      // echo"<pre>";print_r($first_interval);echo"</pre>";
      // echo $first_interval->d;echo"||";echo $first_interval->y;echo"||";echo $first_interval->m;echo"||";
      // echo $first_interval->h; echo"||";echo $first_interval->i;echo"||";echo $first_interval->s;echo"<br>";
      // echo"---------------------------------------";echo"<br>";
      // exit();
      if($first_interval->d == 0 && $first_interval->y == 0 && $first_interval->m == 0){
        // echo"heare";
        if($first_interval->h <= 11){
          if($first_interval->i <= 59 && $first_interval->s <= 59){
            return 1;
          }else{return fasle;}
        }else{return false;}
      }else{ return false; }
    }
    public function second_cutoffx($from,$to){
      $start = new DateTime($from);
      $end = new DateTime($to);
      $first_interval = $start->diff($end);

      if($first_interval->d == 0 && $first_interval->y == 0 && $first_interval->m == 0){
        if($first_interval->h <= 11){
          if($first_interval->i <= 59 && $first_interval->s <= 59){
            return 1;
          }else{return fasle;}
        }else{return false;}
      }else{ return false; }
    }
    public function time_check($dt){
      $now_time = new \DateTime("now");
      // $first_time = $now_time->format('Y-m-d 14:00:00');
      $first_time= '2017-10-05 14:00:00';
      $first_time = new DateTime($first_time);
      $common_time = new DateTime($dt);
      $first_interval = $common_time->diff($first_time);
      // print_r($first_time) ;
      // echo"<pre>";
      // print_r($first_interval);
      // echo"</pre>";
      // print_r($common_time) ;

      if($first_interval->d == 0 && $first_interval->y == 0 && $first_interval->m == 0){
        if($first_interval->h <= 11){
          if($first_interval->i <= 59 && $first_interval->s <= 59){
            return true;
          }else{return fasle;}
        }else{return false;}
      }else{ return false; }
    }
    public function get_only_tree_obj($id){
      $utill_obj = new Common_utill();
      $em = $this->doctrine->em;
      $res = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$id);
      $uid = $res->getUserId()->getUserId();
      $un = $res->getUserId()->getUserName();
      $fn = $res->getUserId()->getFirstName();
      $attachment  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $uid, 'attachment_referer_type'=>1));
      $img = 0;
      if($attachment != null){
        if($attachment[0]->getAttachmentImageUrl() != null || $attachment[0]->getAttachmentImageUrl() != -1){
          $img = $attachment[0]->getAttachmentImageUrl();
        }else{ $img = 0; }
      }else{ $img = 0; }
      $pid = $res->getUserPurchaseId();
      $ct = $res->getUserId()->getCreatedAt();
      $pin_obj = $res->getUserPurchasePinId();
      $package_obj = $pin_obj->getPinRequestPackageId();
      if($package_obj != null){
        $package_id = $package_obj->getPackageId();
        $package_name = $package_obj->getPackageName();
        $package_price = $package_obj->getPackagePrice();
      }else{$package_id=0;$package_name=0;$package_price=0;}
      $obj = new Entity\Geanology();
      $obj->setPurchaseId($pid);
      $obj->setUserId($uid);
      $obj->setUserName($un);
      $obj->setFirstName($fn);
      $obj->setImage($img);
      $obj->setDate($ct);
      $obj->setPackageId($package_id);
      $obj->setPackageName($package_name);
      $obj->setPackagePrice($package_price);
      $obj->setRootId($res->getUserPurchaseActualGeneUserPurchaseId());
      if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
        $obj->setLeftObj($this->get_only_tree_obj($res->getUserPurchaseGeneLeftPurchaseId()));
      }
      if($res->getUserPurchaseGeneRightPurchaseId() != 0){
        $obj->setRightObj($this->get_only_tree_obj($res->getUserPurchaseGeneRightPurchaseId()));
      }
      return $obj;
    }
    public function get_tree_obj($id,$end){

      $utill_obj = new Common_utill();
      $em = $this->doctrine->em;
      // $obj = 0;
      // echo self::$tree_var;echo"<br>";
      
        $res = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$id);
        $resx = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$res->getPairAchieveUserPurchaseId());
        $package_idx = 0;
        if($resx != null){
          $pinx_obj = $resx->getUserPurchasePinId();
          $packagex_obj = $pinx_obj->getPinRequestPackageId();
          if($packagex_obj != null){
            $package_idx = $packagex_obj->getPackageId();
          }else{
            $package_idx = 0;
          }
        }
        $pin_obj = $res->getUserPurchasePinId();
        $package_obj = $pin_obj->getPinRequestPackageId();
        if($package_obj != null){
          $package_id = $package_obj->getPackageId();
          $package_name = $package_obj->getPackageName();
          $package_price = $package_obj->getPackagePointValue();
          $limit_per_cut_off = $package_obj->getPackageLimitedPairCutoff();
        }else{$package_id=0;$package_name=0;$package_price=0;}

        $uid = $res->getUserId()->getUserId();
        $un = $res->getUserId()->getUserName();

        $obj = new Entity\Sample_payout();
        $obj->setPurchaseId($res->getUserPurchaseId());
        $obj->setUserName($un);
        $obj->setUserId($uid);
        $obj->setPackageId($package_id);
        $obj->setPackageName($package_name);
        $obj->setPackagePrice($package_price);
        $obj->setPackageLimitedPairCutoff($limit_per_cut_off);

        $obj->setCreatedAt($res->getCreatedAt());

        $obj->setPayoutReleased($res->getPayoutReleased());
        $obj->setPairAchieve($res->getPairAchieve());
        $obj->setPairAchieveSide($res->getPairAchieveSide());
        $obj->setPairAchieveUserPurchaseId($res->getPairAchieveUserPurchaseId());

        $obj->setPairAchievePackageId($package_idx);

        $obj->setRootId($res->getUserPurchaseActualGeneUserPurchaseId());
        self::$tree_var++;

        if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
          $obj->setLeftObj($this->get_lf($res->getUserPurchaseGeneLeftPurchaseId(),$end,$res->getPayoutReleased()));
        }else{$obj->setLeftObj(0);}
        if($res->getUserPurchaseGeneRightPurchaseId() != 0){
          $obj->setRightObj($this->get_rf($res->getUserPurchaseGeneRightPurchaseId(),$end,$res->getPayoutReleased()));
        }else{$obj->setRightObj(0);}
      
      return $obj;
    }
    public function get_lf($id,$end,$pay){
      $utill_obj = new Common_utill();
      $em = $this->doctrine->em;
      // $obj = 0;
      $res = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$id);
      if($pay == 1){
        $start = new DateTime($end);
        $first_interval = $res->getCreatedAt()->diff($start);
        
        if($first_interval->d == 0 && $first_interval->y == 0 && $first_interval->m == 0){
          if($first_interval->h <= 11){

            if($first_interval->i <= 59 && $first_interval->s <= 59){
              // echo $id;echo"<br>";

              $res = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$id);
              $resx = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$res->getPairAchieveUserPurchaseId());
                  $package_idx = 0;
                  if($resx != null){
                    $pinx_obj = $resx->getUserPurchasePinId();
                    $packagex_obj = $pinx_obj->getPinRequestPackageId();
                    if($packagex_obj != null){
                      $package_idx = $packagex_obj->getPackageId();
                    }else{
                      $package_idx = 0;
                    }
                  }
                  $pin_obj = $res->getUserPurchasePinId();
                  $package_obj = $pin_obj->getPinRequestPackageId();
                  if($package_obj != null){
                    $package_id = $package_obj->getPackageId();
                    $package_name = $package_obj->getPackageName();
                    $package_price = $package_obj->getPackagePointValue();
                    $limit_per_cut_off = $package_obj->getPackageLimitedPairCutoff();
                  }else{$package_id=0;$package_name=0;$package_price=0;}

                  $uid = $res->getUserId()->getUserId();
                  $un = $res->getUserId()->getUserName();

                  array_push(self::$tree_left_var, $package_id);
                  array_push(self::$tree_up_var_left, $id);

                  if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
                    $this->get_lf($res->getUserPurchaseGeneLeftPurchaseId(),$end,$pay);
                  }
                  if($res->getUserPurchaseGeneRightPurchaseId() != 0){
                   $this->get_lf($res->getUserPurchaseGeneRightPurchaseId(),$end,$pay);
                  }

            }else{
              if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
                $this->get_lf($res->getUserPurchaseGeneLeftPurchaseId(),$end,$pay);
              }
              if($res->getUserPurchaseGeneRightPurchaseId() != 0){
                $this->get_lf($res->getUserPurchaseGeneRightPurchaseId(),$end,$pay);
              }
            }
          }else{
            if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
              $this->get_lf($res->getUserPurchaseGeneLeftPurchaseId(),$end,$pay);
            }
            if($res->getUserPurchaseGeneRightPurchaseId() != 0){
              $this->get_lf($res->getUserPurchaseGeneRightPurchaseId(),$end,$pay);
            }
          }
        }else{
            if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
              $this->get_lf($res->getUserPurchaseGeneLeftPurchaseId(),$end,$pay);
            }
            if($res->getUserPurchaseGeneRightPurchaseId() != 0){
              $this->get_lf($res->getUserPurchaseGeneRightPurchaseId(),$end,$pay);
            }
        }
      }else{
        $pin_obj = $res->getUserPurchasePinId();
        $package_obj = $pin_obj->getPinRequestPackageId();
        if($package_obj != null){
          $package_id = $package_obj->getPackageId();
          $package_name = $package_obj->getPackageName();
          $package_price = $package_obj->getPackagePointValue();
          $limit_per_cut_off = $package_obj->getPackageLimitedPairCutoff();
        }else{$package_id=0;$package_name=0;$package_price=0;}

        $uid = $res->getUserId()->getUserId();
        $un = $res->getUserId()->getUserName();

        array_push(self::$tree_left_var, $package_id);
        array_push(self::$tree_up_var_left, $id);

        if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
          $this->get_lf($res->getUserPurchaseGeneLeftPurchaseId(),$end,$pay);
        }
        if($res->getUserPurchaseGeneRightPurchaseId() != 0){
         $this->get_lf($res->getUserPurchaseGeneRightPurchaseId(),$end,$pay);
        }
      }

      return self::$tree_left_var;
    }
    public function get_rf($id,$end,$pay){
      $utill_obj = new Common_utill();
      $em = $this->doctrine->em;
      // $obj = 0;
      $res = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$id);
      $start = new DateTime($end);
      if($pay == 1){
        $first_interval = $res->getCreatedAt()->diff($start);
        if($first_interval->d == 0 && $first_interval->y == 0 && $first_interval->m == 0){
          if($first_interval->h <= 11){

            if($first_interval->i <= 59 && $first_interval->s <= 59){
               // echo $id;echo"---r--";echo"<br>";
              $res = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$id);
              $resx = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$res->getPairAchieveUserPurchaseId());
                  $package_idx = 0;
                  if($resx != null){
                    $pinx_obj = $resx->getUserPurchasePinId();
                    $packagex_obj = $pinx_obj->getPinRequestPackageId();
                    if($packagex_obj != null){
                      $package_idx = $packagex_obj->getPackageId();
                    }else{
                      $package_idx = 0;
                    }
                  }
                  $pin_obj = $res->getUserPurchasePinId();
                  $package_obj = $pin_obj->getPinRequestPackageId();
                  if($package_obj != null){
                    $package_id = $package_obj->getPackageId();
                    $package_name = $package_obj->getPackageName();
                    $package_price = $package_obj->getPackagePointValue();
                    $limit_per_cut_off = $package_obj->getPackageLimitedPairCutoff();
                  }else{$package_id=0;$package_name=0;$package_price=0;}

                  $uid = $res->getUserId()->getUserId();
                  $un = $res->getUserId()->getUserName();

                  array_push(self::$tree_right_var, $package_id);
                  array_push(self::$tree_up_var_right, $id);

                  if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
                    $this->get_rf($res->getUserPurchaseGeneLeftPurchaseId(),$end,$pay);
                  }
                  if($res->getUserPurchaseGeneRightPurchaseId() != 0){
                    $this->get_rf($res->getUserPurchaseGeneRightPurchaseId(),$end,$pay);
                  }
                  
            }else{
              if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
                $this->get_rf($res->getUserPurchaseGeneLeftPurchaseId(),$end,$pay);
              }
              if($res->getUserPurchaseGeneRightPurchaseId() != 0){
                $this->get_rf($res->getUserPurchaseGeneRightPurchaseId(),$end,$pay);
              }
            }
          }else{
            if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
              $this->get_rf($res->getUserPurchaseGeneLeftPurchaseId(),$end,$pay);
            }
            if($res->getUserPurchaseGeneRightPurchaseId() != 0){
              $this->get_rf($res->getUserPurchaseGeneRightPurchaseId(),$end,$pay);
            }
          }
        }else{
            if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
              $this->get_rf($res->getUserPurchaseGeneLeftPurchaseId(),$end,$pay);
            }
            if($res->getUserPurchaseGeneRightPurchaseId() != 0){
              $this->get_rf($res->getUserPurchaseGeneRightPurchaseId(),$end,$pay);
            }
        }
      }else{
        $pin_obj = $res->getUserPurchasePinId();
        $package_obj = $pin_obj->getPinRequestPackageId();
        if($package_obj != null){
          $package_id = $package_obj->getPackageId();
          $package_name = $package_obj->getPackageName();
          $package_price = $package_obj->getPackagePointValue();
          $limit_per_cut_off = $package_obj->getPackageLimitedPairCutoff();
        }else{$package_id=0;$package_name=0;$package_price=0;}

        $uid = $res->getUserId()->getUserId();
        $un = $res->getUserId()->getUserName();

        array_push(self::$tree_right_var, $package_id);
        array_push(self::$tree_up_var_right, $id);

        if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
          $this->get_rf($res->getUserPurchaseGeneLeftPurchaseId(),$end,$pay);
        }
        if($res->getUserPurchaseGeneRightPurchaseId() != 0){
          $this->get_rf($res->getUserPurchaseGeneRightPurchaseId(),$end,$pay);
        }
      }

      return self::$tree_right_var;
    }
    public function payout_view(){
      $em = $this->doctrine->em;
      $utill_obj = new Common_utill();
      //$final_obj_ar = array();
      $db_obj_ar = array();
      $pck_ar = $utill_obj->get_all_active_packages_promary_key();
      rsort($pck_ar);
      
      $user_id = $_SESSION['user_id'];
      $up_obj = $utill_obj->get_single_row_using_array_value('User_package_purchase','user_id',$user_id);
      
      $user_purchase_id = $up_obj->getUserPurchaseId();
      $obj_ar = array();
      for($i=0;$i<count($pck_ar);$i++){
        $query = $em->createQuery("SELECT MAX(b.payout_id) FROM Entity\Payout as b WHERE (b.total_pv_left != 0 OR b.total_pv_right != 0) AND b.package_id=".$pck_ar[$i]." AND b.user_purchase_id=".$user_purchase_id);
        $single_res = $query->getResult();
        if($single_res[0][1] != null){
          $max_payout_id = $single_res[0][1];
          $fin_res = $em->getRepository('Entity\Payout')->find($max_payout_id);
          // print_r($fin_res);exit();
          if($fin_res != null){
            $obj = new Entity\Payout_details_view();
            $obj->setPackageName($fin_res->getPackageId()->getPackageName());
            $obj->setPersonalSalesCount(0);
            $obj->setPersonalSalesSide(0);
            $obj->setPreviousLeft($fin_res->getPreviousLeft());
            $obj->setPreviousRight($fin_res->getPreviousRight());
            $obj->setCurrentLeft($fin_res->getCurrentLeft());
            $obj->setCurrentRight($fin_res->getCurrentRight());
            $obj->setTotalLeft($fin_res->getTotalLeft());
            $obj->setTotalRight($fin_res->getTotalRight());

            if($fin_res->getCarryForwardSide() == 0){
              $obj->setCarryLeft($fin_res->getCarryForward());
              $obj->setCarryRight(0);
            }else{
              $obj->setCarryLeft(0);
              $obj->setCarryRight($fin_res->getCarryForward());
            }
            $obj->setPairDetailsTotal($fin_res->getPairDetailsTotal());
            $obj->setPairDetailsFlushed($fin_res->getPairDetailsFlushed());

            $obj->setTotalPvLeft($fin_res->getTotalPvLeft());
            $obj->setTotalPvRight($fin_res->getTotalPvRight());
            array_push($obj_ar, $obj);
          }
        }
      }
      $qry = $em->createQuery("SELECT MAX(b.payout_result_id) FROM Entity\Payout_result as b WHERE b.payout_result_user_id=".$user_purchase_id);
      $s_res = $qry->getResult();
      $bbj = new Entity\Payout_details_result_view();
      if($s_res[0][1] != null){
        $max_payout_res_id = $s_res[0][1];
        $pay_resx = $em->getRepository('Entity\Payout_result')->find($max_payout_res_id);
        if($pay_resx != null){
          
          $bbj->setTotalLeftPv($pay_resx->getTotalLeftPv());
          $bbj->setTotalRightPv($pay_resx->getTotalRightPv());
          $bbj->setCarryForwardPv($pay_resx->getCarryForwardPv());
          $bbj->setCarryForwardPvSide($pay_resx->getCarryForwardPvSide());
          $bbj->setNetPayout($pay_resx->getNetPayout());
        }
      }

      $data['final_obj_ar'] = $obj_ar;
      $data['final_obj_res'] = $bbj;
      
      $this->load->view('header');
      $this->load->view('payout/Payout',$data);
      $this->load->view('footer');
    }
    public function payout_date_wise(){
      $this->load->view('header');
      $this->load->view('payout/Payout_view_date_wise');
      $this->load->view('footer');
    }
    public function datatable_payout(){ 
      //date($_GET['dt'], strtotime(' +1 day'))
      $dt1 = '00:00:00 02:00:00';
      $dt2 = '00:00:00 02:00:00';

      $cc = "";
      if($_SESSION['role_id'] != 7){
        $cc = " AND us.user_id = ".$_SESSION['user_id'];
      }

      if($_GET['pos1'] == 0){
        $dt1 = $_GET['dt1'].' 02:00:00';
      }else{
        $dt1 = $_GET['dt1'].' 13:59:59';
      }

      if($_GET['pos2'] == 0){
        $dt2 = $_GET['dt2'].' 02:00:00';
      }else{
        $dt2 = $_GET['dt2'].' 13:59:59';
      }

      // if($_GET['pos'] == 0){
      //   $dt1 = $_GET['dt'].' 02:00:00';
      //   $dt2 = $_GET['dt'].' 14:00:00';
      // }elseif($_GET['pos'] == 1){
      //   $nxt = date('Y-m-d', strtotime('+1 day', strtotime($_GET['dt'])));
      //   $dt1 = $_GET['dt'].' 14:00:00';
      //   $dt2 = $nxt.' 02:00:00';
      // }

      $dt = date('Y-m-d', strtotime('+1 day', strtotime('2017-10-09')));

      $CI = &get_instance();
      $CI->load->database();
      $sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
      $table = 'Payout';
      $primaryKey = 'payout_id';
      $columns = array(
        array( 'db' => '`p`.`package_name`', 'dt' => 0,'field' => 'package_name'),
        
        array( 'db' => '`a`.`personal_left`', 'dt' => 1,'field' => 'personal_left'),
        array( 'db' => '`a`.`personal_right`', 'dt' => 2,'field' => 'personal_right'),

        array( 'db' => '`a`.`previous_left`', 'dt' => 3,'field' => 'previous_left'),
        array( 'db' => '`a`.`previous_right`', 'dt' => 4,'field' => 'previous_right'),

        array( 'db' => '`a`.`current_left`', 'dt' => 5,'field' => 'current_left'),
        array( 'db' => '`a`.`current_right`', 'dt' => 6,'field' => 'current_right'),

        array( 'db' => '`a`.`total_left`', 'dt' => 7,'field' => 'total_left'),
        array( 'db' => '`a`.`total_right`', 'dt' => 8,'field' => 'total_right'),

        array( 'db' => '`a`.`carry_forward`',
           'dt' => 9,
           'field' => 'carry_forward',
           'formatter' => function( $d, $row ) {
              if($row['carry_forward_side'] == 0){
                return $row['carry_forward'];
              }else{return 0;}
           }

        ),
        array( 'db' => '`a`.`carry_forward`',
           'dt' => 10,
           'field' => 'carry_forward',
           'formatter' => function( $d, $row ) {
              if($row['carry_forward_side'] == 1){
                return $row['carry_forward'];
              }else{return 0;}
           }

        ),

        array( 'db' => '`a`.`pair_details_total`', 'dt' => 11,'field' => 'pair_details_total'),
        array( 'db' => '`a`.`pair_details_flushed`', 'dt' => 12,'field' => 'pair_details_flushed'),

        array( 'db' => '`a`.`total_pv_left`', 'dt' => 13,'field' => 'total_pv_left'),
        array( 'db' => '`a`.`total_pv_right`', 'dt' => 14,'field' => 'total_pv_right'),

        array( 'db' => '`a`.`created_at`', 'dt' => 15,'field' => 'created_at'),
        array( 'db' => '`a`.`carry_forward_side`', 'dt' => 16,'field' => 'carry_forward_side'),
      );
      $joinQuery = "FROM `Payout` AS `a` INNER JOIN `Packages` AS `p` ON (`a`.`package_id` = `p`.`package_id`) INNER JOIN `User_package_purchase` as up ON (`a`.`user_purchase_id` = `up`.`user_purchase_id`) INNER JOIN `User` as us ON (`us`.`user_id` = `up`.`user_id`)";
      // $where = "a.created_at BETWEEN '".$dt1."' AND '2017-10-14 02:00:00'";
      $where = "(a.total_pv_left != 0 OR a.total_pv_right != 0) AND a.created_at BETWEEN '".$dt1."' AND '".$dt2."' $cc";
      // $where = "a.created_at BETWEEN '".$dt1."' AND '".$dt2."' AND `us`.`user_id` = 108";
      echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery,$where));
    }
    public function payout_result_date_wise(){
      $this->load->view('header');
      $this->load->view('payout/Payout_result_view_date_wise');
      $this->load->view('footer');
    }
    public function datatable_payout_result(){
      $dt1 = '00:00:00 02:00:00';
      $dt2 = '00:00:00 02:00:00';

      // if($_GET['pos'] == 0){
      //   $dt1 = $_GET['dt'].' 02:00:00';
      //   $dt2 = $_GET['dt'].' 14:00:00';
      // }elseif($_GET['pos'] == 1){
      //   $nxt = date('Y-m-d', strtotime('+1 day', strtotime($_GET['dt'])));
      //   $dt1 = $_GET['dt'].' 14:00:00';
      //   $dt2 = $nxt.' 02:00:00';
      // }
      if($_GET['pos1'] == 0){
        $dt1 = $_GET['dt1'].' 02:00:00';
      }else{
        $dt1 = $_GET['dt1'].' 13:59:59';
      }

      if($_GET['pos2'] == 0){
        $dt2 = $_GET['dt2'].' 02:00:00';
      }else{
        $dt2 = $_GET['dt2'].' 13:59:59';
      }
      $cc = "";
      if($_SESSION['role_id'] != 7){
        $cc = " AND us.user_id = ".$_SESSION['user_id'];
      }

      // $dt = date('Y-m-d', strtotime('+1 day', strtotime('2017-10-09')));

      $CI = &get_instance();
      $CI->load->database();
      $sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
      $table = 'Payout_result';
      $primaryKey = 'payout_result_id';

      $columns = array(
        array( 'db' => '`us`.`user_name`', 'dt' => 0,'field' => 'user_name'),
        array( 'db' => '`a`.`total_left_pv`', 'dt' => 1,'field' => 'total_left_pv'),
        array( 'db' => '`a`.`total_right_pv`', 'dt' => 2,'field' => 'total_right_pv'),

        array( 'db' => '`a`.`carry_forward_pv`',
           'dt' => 3,
           'field' => 'carry_forward_pv',
           'formatter' => function( $d, $row ) {
              if($row['carry_forward_pv_side'] == 0){
                return $row['carry_forward_pv'];
              }else{return 0;}
           }

        ),
        array( 'db' => '`a`.`carry_forward_pv`',
           'dt' => 4,
           'field' => 'carry_forward_pv',
           'formatter' => function( $d, $row ) {
              if($row['carry_forward_pv_side'] == 1){
                return $row['carry_forward_pv'];
              }else{return 0;}
           }

        ),

        array( 'db' => '`a`.`net_payout`',
           'dt' => 5,
           'field' => 'net_payout',
           'formatter' => function( $d, $row ) {
             if($row['total_left_pv'] >= $row['total_right_pv'] ){
              if($row['total_left_pv'] != 0){
                if($row['total_right_pv'] != 0){
                  return 2*($row['total_right_pv']*5/100);
                }else{return 0;}
              }else{return 0;}
             }else{
                if($row['total_right_pv'] != 0){
                  if($row['total_left_pv'] != 0){
                    return 2*($row['total_left_pv']*5/100);
                  }else{return 0;}
                }else{return 0;}
             }
           }

        ),
        array( 'db' => '`a`.`net_payout`',
           'dt' => 6,
           'field' => 'net_payout',
           'formatter' => function( $d, $row ) {
               if($row['total_left_pv'] >= $row['total_right_pv'] ){
                  if($row['total_left_pv'] != 0){
                    if($row['total_right_pv'] != 0){
                      return 2*($row['total_right_pv']*5/100);
                    }else{return 0;}
                  }else{return 0;}
                 }else{
                    if($row['total_right_pv'] != 0){
                      if($row['total_left_pv'] != 0){
                        return 2*($row['total_left_pv']*5/100);
                      }else{return 0;}
                    }else{return 0;}
                 }
           }

        ),

        array( 'db' => '`a`.`net_payout`', 'dt' => 7,'field' => 'net_payout'),
        array( 'db' => '`a`.`created_at`', 'dt' => 8,'field' => 'created_at'),
        array( 'db' => '`a`.`carry_forward_pv_side`', 'dt' => 9,'field' => 'carry_forward_pv_side'),
      );

      $joinQuery = "FROM `Payout_result` AS `a`  INNER JOIN `User_package_purchase` as up ON (`a`.`payout_result_user_id` = `up`.`user_purchase_id`) INNER JOIN `User` as us ON (`us`.`user_id` = `up`.`user_id`)";
       $where = "a.created_at BETWEEN '".$dt1."' AND '".$dt2."' $cc";
       echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery,$where));
    }
    public function final_abstract(){
      $this->load->view('header');
      $this->load->view('payout/Final_abstract');
      $this->load->view('footer');
    }
    public function datatable_payout_abstract(){
      $dt1 = '00:00:00 02:00:00';
      $dt2 = '00:00:00 02:00:00';
      
      $con = '';
      if($_GET['stat'] == 0){
        $con = '';
        // $status = 1;
      }elseif($_GET['stat'] == 1){
        $con =  ' AND `a`.`payment_status` = 1';
      }else{
        $con =  ' AND `a`.`payment_status` = 0';
      }

      if($_GET['pos1'] == 0){
        $dt1 = $_GET['dt1'].' 02:00:00';
      }else{
        $dt1 = $_GET['dt1'].' 13:59:59';
      }

      if($_GET['pos2'] == 0){
        $dt2 = $_GET['dt2'].' 02:00:00';
      }else{
        $dt2 = $_GET['dt2'].' 13:59:59';
      }

      // if($_GET['pos'] == 0){
      //   $dt1 = $_GET['dt'].' 02:00:00';
      //   $dt2 = $_GET['dt'].' 14:00:00';
      // }elseif($_GET['pos'] == 1){
      //   $nxt = date('Y-m-d', strtotime('+1 day', strtotime($_GET['dt'])));
      //   $dt1 = $_GET['dt'].' 14:00:00';
      //   $dt2 = $nxt.' 02:00:00';
      // }

      // $dt = date('Y-m-d', strtotime('+1 day', strtotime('2017-10-09')));

      $CI = &get_instance();
      $CI->load->database();
      $sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
      $tot_tds = 0;
      $table = 'Payout_result';
      $primaryKey = 'payout_result_id';

      $columns = array(
        array( 'db' => '`us`.`user_id`', 'dt' => 0,'field' => 'user_id'),
        array( 'db' => '`us`.`user_id`', 'dt' => 1,'field' => 'user_id'),
        array( 'db' => '`us`.`user_name`', 'dt' => 2,'field' => 'user_name'),
        array( 'db' => '`us`.`pan_no`', 'dt' => 3,'field' => 'pan_no'),
        array( 'db' => '`a`.`created_at`', 'dt' => 4,'field' => 'pan_no',
          'formatter' => function( $d, $row ) {
            return $row['created_at'];
          }
        ),
        array( 'db' => '`bk`.`bank_name`', 'dt' => 5,'field' => 'bank_name'),
        array( 'db' => '`bk`.`bank_branch`', 'dt' => 6,'field' => 'bank_branch'),
        array( 'db' => '`ad`.`account_no`', 'dt' => 7,'field' => 'account_no'),
        array( 'db' => '`bk`.`bank_ifsc`', 'dt' => 8,'field' => 'bank_ifsc'),

        array( 'db' => '`a`.`net_payout`',
           'dt' => 9,
           'field' => 'net_payout',
           'formatter' => function( $d, $row ) {
              if($row['total_left_pv'] >= $row['total_right_pv'] ){
                return 2*$row['total_right_pv'];
              }else{
                return 2*$row['total_left_pv'];
              }
           }
        ),
         array( 'db' => '`a`.`net_payout`',
           'dt' => 10,
           'field' => 'net_payout',
           'formatter' => function( $d, $row ) {
             if($row['total_left_pv'] >= $row['total_right_pv'] ){
              if($row['total_left_pv'] != 0){
                if($row['total_right_pv'] != 0){
                  if($row['kyc_status'] == 1){
                    return 2*($row['total_right_pv']*5/100);
                  }else{
                    return 2*($row['total_right_pv']*20/100);
                  }
                }else{return 0;}
              }else{return 0;}
             }else{
                if($row['total_right_pv'] != 0){
                  if($row['total_left_pv'] != 0){
                    
                    if($row['kyc_status'] == 1){
                      return 2*($row['total_left_pv']*5/100);
                    }else{
                      return 2*($row['total_left_pv']*20/100);
                    }
                  }else{return 0;}
                }else{return 0;}
             }
           }
        ),
        array( 'db' => '`a`.`net_payout`',
           'dt' => 11,
           'field' => 'net_payout',
           'formatter' => function( $d, $row ) {
               if($row['total_left_pv'] >= $row['total_right_pv'] ){
                  if($row['total_left_pv'] != 0){
                    if($row['total_right_pv'] != 0){
                      return (2*$row['total_right_pv'])*5/100;
                    }else{return 0;}
                  }else{return 0;}
                 }else{
                    if($row['total_right_pv'] != 0){
                      if($row['total_left_pv'] != 0){
                        return (2*$row['total_left_pv'])*5/100;
                      }else{return 0;}
                    }else{return 0;}
                 }
           }

        ),
        array( 'db' => '`a`.`net_payout`',
           'dt' => 12,
           'field' => 'net_payout',
           'formatter' => function( $d, $row ) {
              if($row['total_left_pv'] >= $row['total_right_pv'] ){
                if($row['total_left_pv'] != 0){
                  if($row['total_right_pv'] != 0){
                    return 2*($row['total_right_pv']*10/100);
                  }else{return 0;}
                }else{return 0;}
              }else{
                if($row['total_right_pv'] != 0){
                  if($row['total_left_pv'] != 0){
                    return 2*($row['total_left_pv']*10/100);
                  }else{return 0;}
                }else{return 0;}
              }
           }

        ),

        
        array( 'db' => '`a`.`net_payout`', 'dt' => 13,'field' => 'net_payout'),
        array( 'db' => '`up`.`user_purchase_id`',
             'dt' => 14,
             'field' => 'user_purchase_id',
             'formatter' => function( $d, $row ) {
                return '<i class="fa fa-search-plus float_left" aria-hidden="true" onclick="get_payout_res('.$d.',`'.$row['created_at'].'`)"></i>
                        ';
            }
        ),

        array( 'db' => '`a`.`total_left_pv`', 'dt' => 15,'field' => 'total_left_pv'),
        array( 'db' => '`a`.`total_right_pv`', 'dt' => 16,'field' => 'total_right_pv'),

        array( 'db' => '`a`.`created_at`', 'dt' => 17,'field' => 'created_at'),
        array( 'db' => '`us`.`kyc_status`', 'dt' => 18,'field' => 'kyc_status'),

      );

      $joinQuery = "FROM `Payout_result` AS `a`  INNER JOIN `User_package_purchase` as up ON (`a`.`payout_result_user_id` = `up`.`user_purchase_id`) INNER JOIN `User` as us ON (`us`.`user_id` = `up`.`user_id`) INNER JOIN `Account_details` as ad ON (`us`.`user_id` = `ad`.`account_refer_id`) INNER JOIN `Bank_details` as bk ON (`bk`.`bank_id` = `ad`.`bank_id`)";
      $where = "a.created_at BETWEEN '".$dt1."' AND '".$dt2."' $con";

      $result = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery,$where);
      $start=$_REQUEST['start']+1;
      $idx=0;
      foreach($result['data'] as &$res){
          $res[0]=(string)$start;
          $start++;
          $idx++;
      }
      echo json_encode($result);
    }
    public function pin_abstract(){
      $this->load->view('header');
      $this->load->view('payout/Pin_abstract');
      $this->load->view('footer');
    }
    public function datatable_pin_abstract(){
      $dt1 = '00:00:00 02:00:00';
      $dt2 = '00:00:00 02:00:00';
      
      if($_GET['pos'] == 0){
        $dt1 = $_GET['dt'].' 02:00:00';
        $dt2 = $_GET['dt'].' 13:59:59';
      }elseif($_GET['pos'] == 1){
        $nxt = date('Y-m-d', strtotime('+1 day', strtotime($_GET['dt'])));
        $dt1 = $_GET['dt'].' 14:00:00';
        $dt2 = $nxt.' 01:59:59';
      }

      $CI = &get_instance();
      $CI->load->database();
      $sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
      $table = 'Payout_result';
      $primaryKey = 'payout_result_id';

      $columns = array(
        array( 'db' => '`pin`.`pin_no`', 'dt' => 0,'field' => 'pin_no'),
        array( 'db' => '`pin`.`pin_no`', 'dt' => 1,'field' => 'pin_no'),
        array( 'db' => '`u`.`user_name`', 'dt' => 2,'field' => 'user_name'),
        array( 'db' => '`p`.`package_name`', 'dt' => 3,'field' => 'package_name'),
        array( 'db' => '`pack`.`package_product_name`', 'dt' => 4,'field' => 'package_product_name'),
        array( 'db' => '`up`.`created_at`', 'dt' => 5,'field' => 'created_at'),
       
      );
      $joinQuery = "FROM User_package_purchase AS up INNER JOIN Pins as pin ON (up.user_purchase_pin_id = pin.pin_id) INNER JOIN Package_products as pack ON (pack.package_product_id = pin.pin_request_package_product_id) INNER JOIN Packages as p ON (pin.pin_request_package_id = p.package_id) INNER JOIN User as u ON (up.user_id = u.user_id)";
       $where = "up.created_at BETWEEN '".$dt1."' AND '".$dt2."'";
      //echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery,$where));
      $result=SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery,$where);
      $start=$_REQUEST['start']+1;
      $idx=0;
      foreach($result['data'] as &$res){
          $res[0]=(string)$start;
          $start++;
          $idx++;
      }
      echo json_encode($result);
    }

    public function final_abstract_without_kyc(){
      $this->load->view('header');
      $this->load->view('payout/Final_abstract_without_kyc');
      $this->load->view('footer');
    }
    public function datatable_payout_abstract_without_kyc(){
      $dt1 = '00:00:00 02:00:00';
      $dt2 = '00:00:00 02:00:00';

      if($_GET['pos'] == 0){
        $dt1 = $_GET['dt'].' 02:00:00';
        $dt2 = $_GET['dt'].' 14:00:00';
      }elseif($_GET['pos'] == 1){
        $nxt = date('Y-m-d', strtotime('+1 day', strtotime($_GET['dt'])));
        $dt1 = $_GET['dt'].' 14:00:00';
        $dt2 = $nxt.' 02:00:00';
      }

      // $dt = date('Y-m-d', strtotime('+1 day', strtotime('2017-10-09')));

      $CI = &get_instance();
      $CI->load->database();
      $sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
      $table = 'Payout_result';
      $primaryKey = 'payout_result_id';

      $columns = array(
        array( 'db' => '`us`.`user_id`', 'dt' => 0,'field' => 'user_id'),
        array( 'db' => '`us`.`user_name`', 'dt' => 1,'field' => 'user_name'),
        array( 'db' => '`us`.`pan_no`', 'dt' => 2,'field' => 'pan_no'),
        array( 'db' => '`a`.`created_at`', 'dt' => 3,'field' => 'pan_no',
          'formatter' => function( $d, $row ) {
            return $row['created_at'];
          }
        ),
        array( 'db' => '`bk`.`bank_name`', 'dt' => 4,'field' => 'bank_name'),
        array( 'db' => '`bk`.`bank_branch`', 'dt' => 5,'field' => 'bank_branch'),
        array( 'db' => '`ad`.`account_no`', 'dt' => 6,'field' => 'account_no'),
        array( 'db' => '`bk`.`bank_ifsc`', 'dt' => 7,'field' => 'bank_ifsc'),



        array( 'db' => '`a`.`net_payout`',
           'dt' => 8,
           'field' => 'net_payout',
           'formatter' => function( $d, $row ) {
              if($row['total_left_pv'] >= $row['total_right_pv'] ){
                return 2*$row['total_right_pv'];
              }else{
                return 2*$row['total_left_pv'];
              }
           }

        ),
         array( 'db' => '`a`.`net_payout`',
           'dt' => 9,
           'field' => 'net_payout',
           'formatter' => function( $d, $row ) {
             if($row['total_left_pv'] >= $row['total_right_pv'] ){
              if($row['total_left_pv'] != 0){
                if($row['total_right_pv'] != 0){
                  if($row['kyc_status'] == 1){
                    return 2*$row['total_right_pv']*5/100;
                  }else{
                    return 2*$row['total_right_pv']*20/100;
                  }
                }else{return 0;}
              }else{return 0;}
             }else{
                if($row['total_right_pv'] != 0){
                  if($row['total_left_pv'] != 0){
                    
                    if($row['kyc_status'] == 1){
                      return 2*$row['total_left_pv']*5/100;
                    }else{
                      return 2*$row['total_left_pv']*20/100;
                    }
                  }else{return 0;}
                }else{return 0;}
             }
           }

        ),
        array( 'db' => '`a`.`net_payout`',
           'dt' => 10,
           'field' => 'net_payout',
           'formatter' => function( $d, $row ) {
               if($row['total_left_pv'] >= $row['total_right_pv'] ){
                  if($row['total_left_pv'] != 0){
                    if($row['total_right_pv'] != 0){
                      return 2*($row['total_right_pv']*5/100);
                    }else{return 0;}
                  }else{return 0;}
                 }else{
                    if($row['total_right_pv'] != 0){
                      if($row['total_left_pv'] != 0){
                        return 2*($row['total_left_pv']*5/100);
                      }else{return 0;}
                    }else{return 0;}
                 }
           }

        ),
        array( 'db' => '`a`.`net_payout`',
           'dt' => 11,
           'field' => 'net_payout',
           'formatter' => function( $d, $row ) {
               if($row['total_left_pv'] >= $row['total_right_pv'] ){
                  if($row['total_left_pv'] != 0){
                    if($row['total_right_pv'] != 0){
                      return 2*($row['total_right_pv']*5/100);
                    }else{return 0;}
                  }else{return 0;}
                 }else{
                    if($row['total_right_pv'] != 0){
                      if($row['total_left_pv'] != 0){
                        return 2*($row['total_left_pv']*5/100);
                      }else{return 0;}
                    }else{return 0;}
                 }
           }

        ),
        array( 'db' => '`a`.`net_payout`', 'dt' => 12,'field' => 'net_payout'),
        array( 'db' => '`a`.`total_left_pv`', 'dt' => 13,'field' => 'total_left_pv'),
        array( 'db' => '`a`.`total_right_pv`', 'dt' => 14,'field' => 'total_right_pv'),

        array( 'db' => '`a`.`created_at`', 'dt' => 15,'field' => 'created_at'),
        array( 'db' => '`us`.`kyc_status`', 'dt' => 16,'field' => 'kyc_status'),
      );

      $joinQuery = "FROM `Payout_result` AS `a`  INNER JOIN `User_package_purchase` as up ON (`a`.`payout_result_user_id` = `up`.`user_purchase_id`) INNER JOIN `User` as us ON (`us`.`user_id` = `up`.`user_id`) INNER JOIN `Account_details` as ad ON (`us`.`user_id` = `ad`.`account_refer_id`) INNER JOIN `Bank_details` as bk ON (`bk`.`bank_id` = `ad`.`bank_id`)";
       $where = "a.created_at BETWEEN '".$dt1."' AND '".$dt2."' AND `a`.`payment_status` = 0";
       echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery,$where));
    }
    public function payout_viewxxxxxx(){
      $em = $this->doctrine->em;
      $utill_obj = new Common_utill();
      //$final_obj_ar = array();
      $db_obj_ar = array();
      
      $user_id = $_SESSION['user_id'];
      $up_obj = $utill_obj->get_single_row_using_array_value('User_package_purchase','user_id',$user_id);
      
      $user_purchase_id = $up_obj->getUserPurchaseId();

      $fin_res = $em->getRepository('Entity\Final_payout')->findBy(array('user_purchase_id' => $user_purchase_id));
      $data['final_obj_ar'] = $fin_res;
      
      $this->load->view('header');
      $this->load->view('payout/Payout',$data);
      $this->load->view('footer');
    }
    public function payout_work_xxx($start,$end){
      $em = $this->doctrine->em;
      $utill_obj = new Common_utill();
      //$final_obj_ar = array();
      $db_obj_ar = array();
      $a = $utill_obj->get_all_up_ids();
      $pck_ar = $utill_obj->get_all_active_packages_promary_key();
      rsort($pck_ar);
      for($j=0;$j<count($a);$j++){
        $user_purchase_id = $a[$j];
        // $user_purchase_id = 1;
        $exist = $utill_obj->get_single_row_using_array_value('User_package_purchase','user_purchase_id',$user_purchase_id);
        // echo $exist->getPairAchieve();exit();
        if($exist->getPairAchieve() == 1){
          $obj = $this->get_tree_obj($user_purchase_id);
          for($i=0;$i<count($pck_ar);$i++){
            // $pck_ar[$i] = 18;$pck_ar[$i]
            // $pck_ar[$i] = 18;

            self::$hunbvi_left=0;
            self::$hunbvi_right=0;
          // $pck_ar[$i] = 18;
            $pack = $utill_obj->get_single_row_using_primary_key('Packages',$pck_ar[$i]);
                
            $cnt = $this->get_left_right_package_count($obj,$pck_ar[$i],$start,$end);//$pck_ar[$i]
            
            //echo"<pre>";echo $pck_ar[$i];echo"xx";echo $user_purchase_id;echo"yy";print_r($cnt);echo"</pre>";
            // if($exist->getPayoutReleased() == 0){
            //   if($obj->getPairAchieveSide() == 0){
            //     if($cnt['cnt_left'] != 0){
            //       $cnt['cnt_left'] = $cnt['cnt_left']-1;
            //     }
            //   }else{
            //     if($cnt['cnt_right'] != 0){
            //       $cnt['cnt_right'] = $cnt['cnt_right']-1;
            //     }
            //   }
            // }

            $res = $em->getRepository('Entity\Payout')->findOneBy(array('user_purchase_id' => $user_purchase_id,'package_id'=>$pck_ar[$i]));

            $carry_forward_sale = 0;
            $carry_forward_sale_side = 0;
            if($res != null && $obj->getPackageId() == $pck_ar[$i]){
              $carry_forward_sale = $res->getCarryForwardSale();
              $carry_forward_sale_side = $res->getCarryForwardSaleSide();
            }

            //$final_obj = new Entity\Payout_details_view();
            $db_obj = new Entity\Payout();
            $f_obj = new Entity\Final_payout();

            //$final_obj->setPackageName($pack->getPackageName());
            //$final_obj->setPersonalSalesCount(0);
            //$final_obj->setPersonalSalesSide(0);

            $f_obj->setPackageId($pack->getPackageId());
            $f_obj->setPackageName($pack->getPackageName());
            $f_obj->setUserPurchaseId($user_purchase_id);
            $f_obj->setPersonalSalesCount(0);
            $f_obj->setPersonalSalesSide(0);

            $db_obj->setUserPurchaseId($user_purchase_id);
            $db_obj->setPackageId($utill_obj->get_reference_obj('Packages',$pack->getPackageId()));
            $db_obj->setPersonalLeft(0);
            $db_obj->setPersonalRight(0);
           
            $tot_cutoff_right = 0;
            $tot_cutoff_left = 0;

            $fin_res = $em->getRepository('Entity\Final_payout')->findOneBy(array('user_purchase_id' => $user_purchase_id,'package_id'=>$pack->getPackageId()));
            $exist_carry_left = 0;
            $exist_carry_right = 0;
            $exist_carry_pv = 0;
            $exist_carry_pv_side = 0;
            if($fin_res != null){
              $exist_carry_left = $fin_res->getCarryLeft();
              $exist_carry_right = $fin_res->getCarryRight();

              $exist_carry_pv = $fin_res->getCarryPv();
              $exist_carry_pv_side = $fin_res->getCarryPvSide();
            }

            //$final_obj->setPreviousLeft($exist_carry_left);
            //$final_obj->setPreviousRight($exist_carry_right);

            $f_obj->setPreviousLeft($exist_carry_left);
            $f_obj->setPreviousRight($exist_carry_right);

            $tot_cutoff_right = $exist_carry_right+$cnt['cnt_right'];
            $tot_cutoff_left = $exist_carry_left+$cnt['cnt_left'];

            //$final_obj->setCurrentLeft($cnt['cnt_left']);
            //$final_obj->setCurrentRight($cnt['cnt_right']);
            //$final_obj->setTotalLeft($tot_cutoff_left);
            //$final_obj->setTotalRight($tot_cutoff_right);

            $f_obj->setCurrentLeft($cnt['cnt_left']);
            $f_obj->setCurrentRight($cnt['cnt_right']);
            $f_obj->setTotalLeft($tot_cutoff_left);
            $f_obj->setTotalRight($tot_cutoff_right);

            $db_obj->setCurrentLeft($cnt['cnt_left']);
            $db_obj->setCurrentRight($cnt['cnt_right']);
            $db_obj->setTotalLeft($tot_cutoff_left);
            $db_obj->setTotalRight($tot_cutoff_right);
            $db_obj->onPrePersist();
            
            $carry_left = 0;
            $carry_right = 0;
            $pair_details_total = 0;
            $pair_details_flushed = 0;

            $limit_per_cut_off = $pack->getPackageLimitedPairCutoff();
            $package_pv = $pack->getPackagePointValue();

            if($tot_cutoff_left > 0 || $tot_cutoff_right > 0){
              if($tot_cutoff_left >= $limit_per_cut_off && $tot_cutoff_right >= $limit_per_cut_off){
                if($tot_cutoff_left > $tot_cutoff_right){
                  $carry_left = $tot_cutoff_left-$tot_cutoff_right;
                  $pair_details_total = $limit_per_cut_off;
                  $pair_details_flushed = $tot_cutoff_left-$limit_per_cut_off;
                  $db_obj->setCarryForward($carry_left);
                  $db_obj->setCarryForwardSide(0);
                  $db_obj->setCarryForwardSale($tot_cutoff_left-$tot_cutoff_right);
                  $db_obj->setCarryForwardSaleSide(0);
                  
                }elseif($tot_cutoff_right > $tot_cutoff_left){
                  $carry_right = $tot_cutoff_right-$tot_cutoff_left;
                  $pair_details_total = $limit_per_cut_off;
                  $pair_details_flushed = $tot_cutoff_right-$limit_per_cut_off;
                  $db_obj->setCarryForward($carry_right);
                  $db_obj->setCarryForwardSide(1);
                  $db_obj->setCarryForwardSale($tot_cutoff_right-$tot_cutoff_left);
                  $db_obj->setCarryForwardSaleSide(1);
                  // $db_obj->setCarryForwardPointValue(($tot_cutoff_right-$tot_cutoff_left)*$package_pv);
                  // $db_obj->setCarryForwardPointValueSide(1);
                }else{
                  $pair_details_total = $limit_per_cut_off;
                  $db_obj->setCarryForward(0);
                  $db_obj->setCarryForwardSide(0);
                  $db_obj->setCarryForwardSale(0);
                  $db_obj->setCarryForwardSaleSide(0);
                  // $db_obj->setCarryForwardPointValue(0);
                  // $db_obj->setCarryForwardPointValueSide(0);
                }
                $db_obj->setCarryForwardPointValue($exist_carry_pv);
                $f_obj->setCarryPv($exist_carry_pv);
                if($exist_carry_pv_side == 0){
                  $db_obj->setCarryForwardPointValueSide(0);
                  $f_obj->setCarryPvSide(0);
                }else{
                  $db_obj->setCarryForwardPointValueSide(1);
                  $f_obj->setCarryPvSide(1);
                }
              }else{
                if($tot_cutoff_left > $tot_cutoff_right){
                  $pair_details_total = $tot_cutoff_right;
                }elseif($tot_cutoff_right > $tot_cutoff_left){
                  $pair_details_total = $tot_cutoff_left;
                }else{
                  $pair_details_total = $tot_cutoff_left;
                }
                $db_obj->setCarryForward(0);
                $db_obj->setCarryForwardSide(0);

                $f_obj->setCarryPv($exist_carry_pv);
                $f_obj->setCarryPvSide($exist_carry_pv_side);

                $db_obj->setCarryForwardSale(0);
                $db_obj->setCarryForwardSaleSide(0);
                if($tot_cutoff_left > $tot_cutoff_right){
                  if($exist_carry_pv_side == 0){
                    $db_obj->setCarryForwardPointValue((($tot_cutoff_left-$tot_cutoff_right)*$package_pv)+$exist_carry_pv);
                    $db_obj->setCarryForwardPointValueSide(0);

                    $f_obj->setCarryPv((($tot_cutoff_left-$tot_cutoff_right)*$package_pv)+$exist_carry_pv);
                    $f_obj->setCarryPvSide(0);
                  }else{
                    $db_obj->setCarryForwardPointValue(($tot_cutoff_left-$tot_cutoff_right)*$package_pv);
                    $db_obj->setCarryForwardPointValueSide(0);

                    $f_obj->setCarryPv(($tot_cutoff_left-$tot_cutoff_right)*$package_pv);
                    $f_obj->setCarryPvSide(0);
                  }
                  
                }else{
                  if($exist_carry_pv_side == 1){
                    $db_obj->setCarryForwardPointValue((($tot_cutoff_right-$tot_cutoff_left)*$package_pv)+$exist_carry_pv);
                    $db_obj->setCarryForwardPointValueSide(1);

                    $f_obj->setCarryPv((($tot_cutoff_right-$tot_cutoff_left)*$package_pv)+$exist_carry_pv);
                    $f_obj->setCarryPvSide(1);
                  }else{
                    $db_obj->setCarryForwardPointValue(($tot_cutoff_right-$tot_cutoff_left)*$package_pv);
                    $db_obj->setCarryForwardPointValueSide(1);

                    $f_obj->setCarryPv(($tot_cutoff_right-$tot_cutoff_left)*$package_pv);
                    $f_obj->setCarryPvSide(1);
                  }
                  
                }
              }
            }else{
              $db_obj->setCarryForward(0);
              $db_obj->setCarryForwardSide(0);
              $db_obj->setCarryForwardPointValue($exist_carry_pv);
              $f_obj->setCarryPv($exist_carry_pv);
              if($exist_carry_pv_side == 0){
                $db_obj->setCarryForwardPointValueSide(0);
                $f_obj->setCarryPvSide(0);
              }else{
                $db_obj->setCarryForwardPointValueSide(1);
                $f_obj->setCarryPvSide(1);
              }
              $db_obj->setCarryForwardSale(0);
              $db_obj->setCarryForwardSaleSide(0);
            }

            //$final_obj->setCarryLeft($carry_left);
            //$final_obj->setCarryRight($carry_right);
            //$final_obj->setPairDetailsTotal($pair_details_total);

            if($exist_carry_pv_side == 0){
              //$final_obj->setTotalPvLeft(($pair_details_total*$package_pv)+$exist_carry_pv);
              $f_obj->setTotalPvLeft(($pair_details_total*$package_pv)+$exist_carry_pv);
            }else{
              //$final_obj->setTotalPvLeft($pair_details_total*$package_pv);
              $f_obj->setTotalPvLeft($pair_details_total*$package_pv);
            }
            if($exist_carry_pv_side == 1){
              //$final_obj->setTotalPvRight(($pair_details_total*$package_pv)+$exist_carry_pv);
              $f_obj->setTotalPvRight(($pair_details_total*$package_pv)+$exist_carry_pv);
            }else{
              //$final_obj->setTotalPvRight($pair_details_total*$package_pv);
              $f_obj->setTotalPvRight($pair_details_total*$package_pv);
            }

            //$final_obj->setPairDetailsFlushed($pair_details_flushed);

            $f_obj->setCarryLeft($carry_left);
            $f_obj->setCarryRight($carry_right);
            $f_obj->setPairTotal($pair_details_total);
            
            $f_obj->setFlushed($pair_details_flushed);

            if($fin_res != null){
              $em->remove($fin_res);
              $em->flush();
            }

            $em->persist($f_obj);
            $em->flush();

            //array_push(//$final_obj_ar, //$final_obj);
            array_push($db_obj_ar, $db_obj);


            // echo"<pre>";
            // echo 'Package '.$pck_ar[$i];echo"<br>";
            // echo 'user '.$a[$j];echo"<br>";
            // print_r($cnt);
            // echo"</pre>";

          }
          
          foreach ($db_obj_ar as $db) {
            $em->persist($db);
            $em->flush();  
          }
          
          if($exist != null){
            $exist->setPayoutReleased(1);
            $em->persist($exist);
            $em->flush();
          }
        }

      }
          // echo"--------------------------------------------------------------";

          //echo"<pre>";
          //print_r($db_obj_ar);
          //echo"</pre>";
          //exit();

      // echo"Hai";exit();
      echo"Payout Success";
    }

  }
?>
