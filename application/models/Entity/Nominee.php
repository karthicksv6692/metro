<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Nominee")
* @HasLifecycleCallbacks
*/
class Nominee{

	/**
	*
	* @Id
	* @Column(name="nominee_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $nominee_id;

	/**
	*@Column(type="string",length=255,nullable=false)
	****/
	protected $nominee_name;

     /**
    *@Column(type="datetime")
    ****/
    protected $nominee_dob;

    /**
    *@Column(type="string",length=255)
    ****/
    protected $nominee_relationship;

    /**
    *@Column(type="string",length=255)
    ****/
    protected $nominee_phone_no;

    /**
    *@Column(type="integer",nullable=true)
    ****/
    protected $nominee_gender;

     /**
     * @var datetime $created_at
     *
     * @Column(type="datetime",nullable = true)
     */
    protected $created_at;

    /**
     * @var datetime $updated_at
     * 
     * @Column(type="datetime", nullable = true)
     */
    protected $updated_at;

    


    /**
     * Gets the value of nominee_id.
     *
     * @return mixed
     */
    public function getNomineeId()
    {
        return $this->nominee_id;
    }

    /**
     * Sets the value of nominee_id.
     *
     * @param mixed $nominee_id the nominee id
     *
     * @return self
     */
    public function setNomineeId($nominee_id)
    {
        $this->nominee_id = $nominee_id;

        return $this;
    }

    /**
     * Gets the value of nominee_name.
     *
     * @return mixed
     */
    public function getNomineeName()
    {
        return $this->nominee_name;
    }

    /**
     * Sets the value of nominee_name.
     *
     * @param mixed $nominee_name the nominee name
     *
     * @return self
     */
    public function setNomineeName($nominee_name)
    {
        $this->nominee_name = $nominee_name;

        return $this;
    }

    /**
     * Gets the value of nominee_dob.
     *
     * @return mixed
     */
    public function getNomineeDob()
    {
        return $this->nominee_dob;
    }

    /**
     * Sets the value of nominee_dob.
     *
     * @param mixed $nominee_dob the nominee dob
     *
     * @return self
     */
    public function setNomineeDob($nominee_dob)
    {
        $this->nominee_dob = $nominee_dob;

        return $this;
    }

    /**
     * Gets the value of nominee_relationship.
     *
     * @return mixed
     */
    public function getNomineeRelationship()
    {
        return $this->nominee_relationship;
    }

    /**
     * Sets the value of nominee_relationship.
     *
     * @param mixed $nominee_relationship the nominee relationship
     *
     * @return self
     */
    public function setNomineeRelationship($nominee_relationship)
    {
        $this->nominee_relationship = $nominee_relationship;

        return $this;
    }

    /**
     * Gets the value of nominee_phone_no.
     *
     * @return mixed
     */
    public function getNomineePhoneNo()
    {
        return $this->nominee_phone_no;
    }

    /**
     * Sets the value of nominee_phone_no.
     *
     * @param mixed $nominee_phone_no the nominee phone no
     *
     * @return self
     */
    public function setNomineePhoneNo($nominee_phone_no)
    {
        $this->nominee_phone_no = $nominee_phone_no;

        return $this;
    }

    /**
     * Gets the value of nominee_gender.
     *
     * @return mixed
     */
    public function getNomineeGender()
    {
        return $this->nominee_gender;
    }

    /**
     * Sets the value of nominee_gender.
     *
     * @param mixed $nominee_gender the nominee gender
     *
     * @return self
     */
    public function setNomineeGender($nominee_gender)
    {
        $this->nominee_gender = $nominee_gender;

        return $this;
    }
    
    /**
     * Gets triggered only on insert

     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_at = new \DateTime("now");
    }


    public function getCreatedAt()
    {
        return $this->created_at;
    }



    /**
     * Gets triggered every time on update

     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_at = new \DateTime("now");
    }
    

    
}
?>