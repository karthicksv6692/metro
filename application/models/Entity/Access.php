<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Access")
*/
class Access{

	/**
	*
	* @Id
	* @Column(name="access_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $access_id;

	/**
	*@Column(type="string",length=255,nullable=false)
	****/
	protected $access_name;

	/**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Module", fetch="EAGER")
     * @JoinColumn(name="module_id", referencedColumnName="module_id")
     */
	protected $module_id;

	/**
	*@Column(type="string",length=255,nullable=false,unique=true)
	****/
	protected $access_url;



    /**
     * Gets the value of access_id.
     *
     * @return mixed
     */
    public function getAccessId()
    {
        return $this->access_id;
    }

    /**
     * Sets the value of access_id.
     *
     * @param mixed $access_id the access id
     *
     * @return self
     */
    public function setAccessId($access_id)
    {
        $this->access_id = $access_id;

        return $this;
    }

    /**
     * Gets the value of access_name.
     *
     * @return mixed
     */
    public function getAccessName()
    {
        return $this->access_name;
    }

    /**
     * Sets the value of access_name.
     *
     * @param mixed $access_name the access name
     *
     * @return self
     */
    public function setAccessName($access_name)
    {
        $this->access_name = $access_name;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getModuleId()
    {
        return $this->module_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $module_id the module id
     *
     * @return self
     */
    public function setModuleId($module_id)
    {
        $this->module_id = $module_id;

        return $this;
    }

    /**
     * Gets the value of access_url.
     *
     * @return mixed
     */
    public function getAccessUrl()
    {
        return $this->access_url;
    }

    /**
     * Sets the value of access_url.
     *
     * @param mixed $access_url the access url
     *
     * @return self
     */
    public function setAccessUrl($access_url)
    {
        $this->access_url = $access_url;

        return $this;
    }

    

    /**
     * Gets the value of user_access_access_id.
     *
     * @return mixed
     */
    public function getUserAccessAccessId()
    {
        return $this->user_access_access_id;
    }

    /**
     * Sets the value of user_access_access_id.
     *
     * @param mixed $user_access_access_id the user access access id
     *
     * @return self
     */
    private function _setUserAccessAccessId($user_access_access_id)
    {
        $this->user_access_access_id = $user_access_access_id;

        return $this;
    }
}
?>