<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Final_payout")
*/
class Final_payout{
    /**
    *
    * @Id
    * @Column(name="payout_id",type="integer",nullable=false)
    * @GeneratedValue(strategy="AUTO")
    */
    protected $payout_id;
    /**
    *@Column(type="integer")
    ****/
    protected $user_purchase_id;
    /**
    *@Column(type="integer")
    ****/
    protected $package_id;

    /**
    *@Column(type="string",length=255)
    ****/
    protected $package_name;

    /**
    *@Column(type="integer")
    ****/
    protected $personal_sales_count;
    /**
    *@Column(type="integer")
    ****/
    protected $personal_sales_side;
    /**
    *@Column(type="integer")
    ****/
    protected $previous_left;
    /**
    *@Column(type="integer")
    ****/
    protected $previous_right;
    /**
    *@Column(type="integer")
    ****/
    protected $current_left;
    /**
    *@Column(type="integer")
    ****/
    protected $current_right;
    /**
    *@Column(type="integer")
    ****/
    protected $total_left;
    /**
    *@Column(type="integer")
    ****/
    protected $total_right;
    /**
    *@Column(type="integer")
    ****/
    protected $carry_left;
    /**
    *@Column(type="integer")
    ****/
    protected $carry_right;

    /**
    *@Column(type="integer")
    ****/
    protected $pair_total;
    /**
    *@Column(type="integer")
    ****/
    protected $flushed;
    /**
    *@Column(type="integer")
    ****/
    protected $total_pv_left;
    /**
    *@Column(type="integer")
    ****/
    protected $total_pv_right;

    /**
    *@Column(type="integer")
    ****/
    protected $carry_pv;

    /**
    *@Column(type="integer")
    ****/
    protected $carry_pv_side;

    /**
     * @return mixed
     */
    public function getPayoutId()
    {
        return $this->payout_id;
    }

    /**
     * @param mixed $payout_id
     *
     * @return self
     */
    public function setPayoutId($payout_id)
    {
        $this->payout_id = $payout_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserPurchaseId()
    {
        return $this->user_purchase_id;
    }

    /**
     * @param mixed $user_purchase_id
     *
     * @return self
     */
    public function setUserPurchaseId($user_purchase_id)
    {
        $this->user_purchase_id = $user_purchase_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPackageId()
    {
        return $this->package_id;
    }

    /**
     * @param mixed $package_id
     *
     * @return self
     */
    public function setPackageId($package_id)
    {
        $this->package_id = $package_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPersonalSalesCount()
    {
        return $this->personal_sales_count;
    }

    /**
     * @param mixed $personal_sales_count
     *
     * @return self
     */
    public function setPersonalSalesCount($personal_sales_count)
    {
        $this->personal_sales_count = $personal_sales_count;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPersonalSalesSide()
    {
        return $this->personal_sales_side;
    }

    /**
     * @param mixed $personal_sales_side
     *
     * @return self
     */
    public function setPersonalSalesSide($personal_sales_side)
    {
        $this->personal_sales_side = $personal_sales_side;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPreviousLeft()
    {
        return $this->previous_left;
    }

    /**
     * @param mixed $previous_left
     *
     * @return self
     */
    public function setPreviousLeft($previous_left)
    {
        $this->previous_left = $previous_left;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPreviousRight()
    {
        return $this->previous_right;
    }

    /**
     * @param mixed $previous_right
     *
     * @return self
     */
    public function setPreviousRight($previous_right)
    {
        $this->previous_right = $previous_right;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrentLeft()
    {
        return $this->current_left;
    }

    /**
     * @param mixed $current_left
     *
     * @return self
     */
    public function setCurrentLeft($current_left)
    {
        $this->current_left = $current_left;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrentRight()
    {
        return $this->current_right;
    }

    /**
     * @param mixed $current_right
     *
     * @return self
     */
    public function setCurrentRight($current_right)
    {
        $this->current_right = $current_right;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalLeft()
    {
        return $this->total_left;
    }

    /**
     * @param mixed $total_left
     *
     * @return self
     */
    public function setTotalLeft($total_left)
    {
        $this->total_left = $total_left;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalRight()
    {
        return $this->total_right;
    }

    /**
     * @param mixed $total_right
     *
     * @return self
     */
    public function setTotalRight($total_right)
    {
        $this->total_right = $total_right;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCarryLeft()
    {
        return $this->carry_left;
    }

    /**
     * @param mixed $carry_left
     *
     * @return self
     */
    public function setCarryLeft($carry_left)
    {
        $this->carry_left = $carry_left;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCarryRight()
    {
        return $this->carry_right;
    }

    /**
     * @param mixed $carry_right
     *
     * @return self
     */
    public function setCarryRight($carry_right)
    {
        $this->carry_right = $carry_right;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPairTotal()
    {
        return $this->pair_total;
    }

    /**
     * @param mixed $pair_total
     *
     * @return self
     */
    public function setPairTotal($pair_total)
    {
        $this->pair_total = $pair_total;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFlushed()
    {
        return $this->flushed;
    }

    /**
     * @param mixed $flushed
     *
     * @return self
     */
    public function setFlushed($flushed)
    {
        $this->flushed = $flushed;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalPvLeft()
    {
        return $this->total_pv_left;
    }

    /**
     * @param mixed $total_pv_left
     *
     * @return self
     */
    public function setTotalPvLeft($total_pv_left)
    {
        $this->total_pv_left = $total_pv_left;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalPvRight()
    {
        return $this->total_pv_right;
    }

    /**
     * @param mixed $total_pv_right
     *
     * @return self
     */
    public function setTotalPvRight($total_pv_right)
    {
        $this->total_pv_right = $total_pv_right;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPackageName()
    {
        return $this->package_name;
    }

    /**
     * @param mixed $package_name
     *
     * @return self
     */
    public function setPackageName($package_name)
    {
        $this->package_name = $package_name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCarryPv()
    {
        return $this->carry_pv;
    }

    /**
     * @param mixed $carry_pv
     *
     * @return self
     */
    public function setCarryPv($carry_pv)
    {
        $this->carry_pv = $carry_pv;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCarryPvSide()
    {
        return $this->carry_pv_side;
    }

    /**
     * @param mixed $carry_pv_side
     *
     * @return self
     */
    public function setCarryPvSide($carry_pv_side)
    {
        $this->carry_pv_side = $carry_pv_side;

        return $this;
    }
}

?>