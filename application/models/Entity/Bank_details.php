<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Bank_details")
*/
class Bank_details{

	/**
	*
	* @Id
	* @Column(name="bank_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $bank_id;

	/**
	*@Column(type="string",length=255,nullable=false)
	****/
	protected $bank_name;
	/**
	*@Column(type="string",length=255,nullable=false,unique=true)
	****/
	protected $bank_ifsc;
	/**
	*@Column(type="string",length=255,nullable=false)
	****/
	protected $bank_branch;

	/**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Address", fetch="EAGER", cascade="persist")
     * @JoinColumn(name="bank_address_id", referencedColumnName="address_id", onDelete="SET NULL",nullable=true)
     */
	protected $bank_address_id;

	/**
	*@Column(type="string",length=255)
	****/
	protected $bank_address;
	/**
	*@Column(type="string",length=255)
	****/
	protected $bank_city;
	/**
	*@Column(type="string",length=255)
	****/
	protected $bank_district;
	/**
	*@Column(type="string",length=255)
	****/
	protected $bank_state;

	/**
     *
     * @ORM OneToMany(targetEntity="Entity\Account_details" mappedBy="bank_id")
     */
    protected $account_bank_id;


    /**
     * Gets the value of bank_id.
     *
     * @return mixed
     */
    public function getBankId()
    {
        return $this->bank_id;
    }

    /**
     * Sets the value of bank_id.
     *
     * @param mixed $bank_id the bank id
     *
     * @return self
     */
    public function setBankId($bank_id)
    {
        $this->bank_id = $bank_id;

        return $this;
    }

    /**
     * Gets the value of bank_name.
     *
     * @return mixed
     */
    public function getBankName()
    {
        return $this->bank_name;
    }

    /**
     * Sets the value of bank_name.
     *
     * @param mixed $bank_name the bank name
     *
     * @return self
     */
    public function setBankName($bank_name)
    {
        $this->bank_name = $bank_name;

        return $this;
    }

    /**
     * Gets the value of bank_ifsc.
     *
     * @return mixed
     */
    public function getBankIfsc()
    {
        return $this->bank_ifsc;
    }

    /**
     * Sets the value of bank_ifsc.
     *
     * @param mixed $bank_ifsc the bank ifsc
     *
     * @return self
     */
    public function setBankIfsc($bank_ifsc)
    {
        $this->bank_ifsc = $bank_ifsc;

        return $this;
    }

    /**
     * Gets the value of bank_branch.
     *
     * @return mixed
     */
    public function getBankBranch()
    {
        return $this->bank_branch;
    }

    /**
     * Sets the value of bank_branch.
     *
     * @param mixed $bank_branch the bank branch
     *
     * @return self
     */
    public function setBankBranch($bank_branch)
    {
        $this->bank_branch = $bank_branch;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getBankAddressId()
    {
        return $this->bank_address_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $bank_address_id the bank address id
     *
     * @return self
     */
    public function setBankAddressId($bank_address_id)
    {
        $this->bank_address_id = $bank_address_id;

        return $this;
    }

    /**
     * Gets the value of bank_address.
     *
     * @return mixed
     */
    public function getBankAddress()
    {
        return $this->bank_address;
    }

    /**
     * Sets the value of bank_address.
     *
     * @param mixed $bank_address the bank address
     *
     * @return self
     */
    public function setBankAddress($bank_address)
    {
        $this->bank_address = $bank_address;

        return $this;
    }

    /**
     * Gets the value of bank_city.
     *
     * @return mixed
     */
    public function getBankCity()
    {
        return $this->bank_city;
    }

    /**
     * Sets the value of bank_city.
     *
     * @param mixed $bank_city the bank city
     *
     * @return self
     */
    public function setBankCity($bank_city)
    {
        $this->bank_city = $bank_city;

        return $this;
    }

    /**
     * Gets the value of bank_district.
     *
     * @return mixed
     */
    public function getBankDistrict()
    {
        return $this->bank_district;
    }

    /**
     * Sets the value of bank_district.
     *
     * @param mixed $bank_district the bank district
     *
     * @return self
     */
    public function setBankDistrict($bank_district)
    {
        $this->bank_district = $bank_district;

        return $this;
    }

    /**
     * Gets the value of bank_state.
     *
     * @return mixed
     */
    public function getBankState()
    {
        return $this->bank_state;
    }

    /**
     * Sets the value of bank_state.
     *
     * @param mixed $bank_state the bank state
     *
     * @return self
     */
    public function setBankState($bank_state)
    {
        $this->bank_state = $bank_state;

        return $this;
    }

    /**
     * Gets the value of account_bank_id.
     *
     * @return mixed
     */
    public function getAccountBankId()
    {
        return $this->account_bank_id;
    }

    /**
     * Sets the value of account_bank_id.
     *
     * @param mixed $account_bank_id the account bank id
     *
     * @return self
     */
    private function _setAccountBankId($account_bank_id)
    {
        $this->account_bank_id = $account_bank_id;

        return $this;
    }
}


?>