<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Contact")
*/
class Contact{

	/**
	*
	* @Id
	* @Column(name="contact_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $contact_id;

	/**
	*@Column(type="string",length=255,nullable=false)
	****/
	protected $contact_name;

	/**
	*@Column(type="string",length=255,nullable=true)
	****/
	protected $contact_designation;

	/**
	*@Column(type="string",length=255)
	****/
	protected $contact_mobile_primary;

    /**
    *@Column(type="string",length=255,nullable=true)
    ****/
    protected $contact_mobile_secondary;

    /**
    *@Column(type="string",length=255,nullable=true)
    ****/
    protected $contact_email_id;

    /**
    *@Column(type="string",length=255,nullable=true)
    ****/
    protected $contact_landline;

   /**
    *@Column(type="integer")
    ****/
    protected $contact_address_id;

	/**
	*@Column(type="integer",nullable=true)
	****/
	protected $contact_refer_id;

	/**
	*@Column(type="integer")
	****/
	protected $contact_refer_type;


	

    /**
     * Gets the value of contact_id.
     *
     * @return mixed
     */
    public function getContactId()
    {
        return $this->contact_id;
    }

    /**
     * Sets the value of contact_id.
     *
     * @param mixed $contact_id the contact id
     *
     * @return self
     */
    public function setContactId($contact_id)
    {
        $this->contact_id = $contact_id;

        return $this;
    }

    /**
     * Gets the value of contact_name.
     *
     * @return mixed
     */
    public function getContactName()
    {
        return $this->contact_name;
    }

    /**
     * Sets the value of contact_name.
     *
     * @param mixed $contact_name the contact name
     *
     * @return self
     */
    public function setContactName($contact_name)
    {
        $this->contact_name = $contact_name;

        return $this;
    }

    /**
     * Gets the value of contact_type.
     *
     * @return mixed
     */
    public function getContactType()
    {
        return $this->contact_type;
    }

    /**
     * Sets the value of contact_type.
     *
     * @param mixed $contact_type the contact type
     *
     * @return self
     */
    public function setContactType($contact_type)
    {
        $this->contact_type = $contact_type;

        return $this;
    }

    /**
     * Gets the value of contact_no.
     *
     * @return mixed
     */
    public function getContactNo()
    {
        return $this->contact_no;
    }

    /**
     * Sets the value of contact_no.
     *
     * @param mixed $contact_no the contact no
     *
     * @return self
     */
    public function setContactNo($contact_no)
    {
        $this->contact_no = $contact_no;

        return $this;
    }

    /**
     * Gets the value of contact_remarks.
     *
     * @return mixed
     */
    public function getContactRemarks()
    {
        return $this->contact_remarks;
    }

    /**
     * Sets the value of contact_remarks.
     *
     * @param mixed $contact_remarks the contact remarks
     *
     * @return self
     */
    public function setContactRemarks($contact_remarks)
    {
        $this->contact_remarks = $contact_remarks;

        return $this;
    }

    /**
     * Gets the value of contact_refere_id.
     *
     * @return mixed
     */
    public function getContactRefereId()
    {
        return $this->contact_refere_id;
    }

    /**
     * Sets the value of contact_refere_id.
     *
     * @param mixed $contact_refere_id the contact refere id
     *
     * @return self
     */
    public function setContactRefereId($contact_refere_id)
    {
        $this->contact_refere_id = $contact_refere_id;

        return $this;
    }

    /**
     * Gets the value of contact_refere_type.
     *
     * @return mixed
     */
    public function getContactRefereType()
    {
        return $this->contact_refere_type;
    }

    /**
     * Sets the value of contact_refere_type.
     *
     * @param mixed $contact_refere_type the contact refere type
     *
     * @return self
     */
    public function setContactRefereType($contact_refere_type)
    {
        $this->contact_refere_type = $contact_refere_type;

        return $this;
    }

    /**
     * Gets the value of user_contact_id.
     *
     * @return mixed
     */
    public function getUserContactId()
    {
        return $this->user_contact_id;
    }

    /**
     * Sets the value of user_contact_id.
     *
     * @param mixed $user_contact_id the user contact id
     *
     * @return self
     */
    public function setUserContactId($user_contact_id)
    {
        $this->user_contact_id = $user_contact_id;

        return $this;
    }

    /**
     * Gets the value of contact_designation.
     *
     * @return mixed
     */
    public function getContactDesignation()
    {
        return $this->contact_designation;
    }

    /**
     * Sets the value of contact_designation.
     *
     * @param mixed $contact_designation the contact designation
     *
     * @return self
     */
    public function setContactDesignation($contact_designation)
    {
        $this->contact_designation = $contact_designation;

        return $this;
    }

    /**
     * Gets the value of contact_mobile_primary.
     *
     * @return mixed
     */
    public function getContactMobilePrimary()
    {
        return $this->contact_mobile_primary;
    }

    /**
     * Sets the value of contact_mobile_primary.
     *
     * @param mixed $contact_mobile_primary the contact mobile primary
     *
     * @return self
     */
    public function setContactMobilePrimary($contact_mobile_primary)
    {
        $this->contact_mobile_primary = $contact_mobile_primary;

        return $this;
    }

    /**
     * Gets the value of contact_mobile_secondary.
     *
     * @return mixed
     */
    public function getContactMobileSecondary()
    {
        return $this->contact_mobile_secondary;
    }

    /**
     * Sets the value of contact_mobile_secondary.
     *
     * @param mixed $contact_mobile_secondary the contact mobile secondary
     *
     * @return self
     */
    public function setContactMobileSecondary($contact_mobile_secondary)
    {
        $this->contact_mobile_secondary = $contact_mobile_secondary;

        return $this;
    }

    /**
     * Gets the value of contact_email_id.
     *
     * @return mixed
     */
    public function getContactEmailId()
    {
        return $this->contact_email_id;
    }

    /**
     * Sets the value of contact_email_id.
     *
     * @param mixed $contact_email_id the contact email id
     *
     * @return self
     */
    public function setContactEmailId($contact_email_id)
    {
        $this->contact_email_id = $contact_email_id;

        return $this;
    }

    /**
     * Gets the value of contact_landline.
     *
     * @return mixed
     */
    public function getContactLandline()
    {
        return $this->contact_landline;
    }

    /**
     * Sets the value of contact_landline.
     *
     * @param mixed $contact_landline the contact landline
     *
     * @return self
     */
    public function setContactLandline($contact_landline)
    {
        $this->contact_landline = $contact_landline;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getContactAddressId()
    {
        return $this->contact_address_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $contact_address_id the contact address id
     *
     * @return self
     */
    public function setContactAddressId($contact_address_id)
    {
        $this->contact_address_id = $contact_address_id;

        return $this;
    }

    /**
     * Gets the value of contact_refer_id.
     *
     * @return mixed
     */
    public function getContactReferId()
    {
        return $this->contact_refer_id;
    }

    /**
     * Sets the value of contact_refer_id.
     *
     * @param mixed $contact_refer_id the contact refer id
     *
     * @return self
     */
    public function setContactReferId($contact_refer_id)
    {
        $this->contact_refer_id = $contact_refer_id;

        return $this;
    }

    /**
     * Gets the value of contact_refer_type.
     *
     * @return mixed
     */
    public function getContactReferType()
    {
        return $this->contact_refer_type;
    }

    /**
     * Sets the value of contact_refer_type.
     *
     * @param mixed $contact_refer_type the contact refer type
     *
     * @return self
     */
    public function setContactReferType($contact_refer_type)
    {
        $this->contact_refer_type = $contact_refer_type;

        return $this;
    }
}
?>