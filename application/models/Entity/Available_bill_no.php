<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Available_bill_no")
*/
class Available_bill_no{

	/**
	*
	* @Id
	* @Column(name="avail_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	public $avail_id;

	/**
	*@Column(type="string",length=255,nullable=false)
	****/
	public $avail_no;

    /**
     * @return mixed
     */
    public function getAvailId()
    {
        return $this->avail_id;
    }

    /**
     * @param mixed $avail_id
     *
     * @return self
     */
    public function setAvailId($avail_id)
    {
        $this->avail_id = $avail_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAvailNo()
    {
        return $this->avail_no;
    }

    /**
     * @param mixed $avail_no
     *
     * @return self
     */
    public function setAvailNo($avail_no)
    {
        $this->avail_no = $avail_no;

        return $this;
    }
}
