<?php
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @Entity
* @Table(name="Team")
*/
class Team
{
	
	/**
	*
	* @Id
	* @Column(name="team_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
    private $team_id;


    /**
     * @var string
     *
     * @Column(name="team_name", type="string", length=255, nullable=false,unique=true)
     */
    private $team_name;

    /**
     *
     * @ORM OneToMany(targetEntity="Entity\Sample" mappedBy="fav_team")
     */
    private $fans;


    /**
     * Gets the value of team_id.
     *
     * @return mixed
     */
    public function getTeamId()
    {
        return $this->team_id;
    }

    /**
     * Sets the value of team_id.
     *
     * @param mixed $team_id the team id
     *
     * @return self
     */
    public function setTeamId($team_id)
    {
        $this->team_id = $team_id;

        return $this;
    }

    /**
     * Gets the value of team_name.
     *
     * @return string
     */
    public function getTeamName()
    {
        return $this->team_name;
    }

    /**
     * Sets the value of team_name.
     *
     * @param string $team_name the team name
     *
     * @return self
     */
    public function setTeamName($team_name)
    {
        $this->team_name = $team_name;

        return $this;
    }

    /**
     * Gets the value of fans.
     *
     * @return mixed
     */
    public function getFans()
    {
        return $this->fans;
    }

    /**
     * Sets the value of fans.
     *
     * @param mixed $fans the fans
     *
     * @return self
     */
    private function _setFans($fans)
    {
        $this->fans = $fans;

        return $this;
    }
}

?>