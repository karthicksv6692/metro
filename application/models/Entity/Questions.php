<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Questions")
*/
class Questions{

	/**
	*
	* @Id
	* @Column(name="question_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $question_id;

	/**
	*@Column(type="string",length=255,nullable=false)
	****/
	protected $question_name;

	


    /**
     * Gets the value of question_id.
     *
     * @return mixed
     */
    public function getQuestionId()
    {
        return $this->question_id;
    }

    /**
     * Sets the value of question_id.
     *
     * @param mixed $question_id the question id
     *
     * @return self
     */
    public function setQuestionId($question_id)
    {
        $this->question_id = $question_id;

        return $this;
    }

    /**
     * Gets the value of question_name.
     *
     * @return mixed
     */
    public function getQuestionName()
    {
        return $this->question_name;
    }

    /**
     * Sets the value of question_name.
     *
     * @param mixed $question_name the question name
     *
     * @return self
     */
    public function setQuestionName($question_name)
    {
        $this->question_name = $question_name;

        return $this;
    }
}
?>