<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Transactions")
*/
class Transactions{
	/**
	*
	* @Id
	* @Column(name="transaction_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $transaction_id;

	/**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\User", fetch="EAGER")
     * @JoinColumn(name="transaction_from_user_id", referencedColumnName="user_id",onDelete="cascade")
     */
	protected $transaction_from_user_id;
	/**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\User", fetch="EAGER")
     * @JoinColumn(name="transaction_to_user_id", referencedColumnName="user_id",onDelete="cascade")
     */
	protected $transaction_to_user_id;

	/**
	*@Column(type="integer")
	****/
	protected $transaction_type;

	/**
	*@Column(type="integer")
	****/
	protected $transaction_amount;


	/**
	*@Column(type="integer")
	****/
	protected $transaction_status;

	/**
	*@Column(type="string",length=255)
	****/
	protected $transaction_remarks;


    /**
     * @var datetime $created_at
     *
     * @Column(type="datetime")
     */
    protected $created_at;

    /**
     * @var datetime $updated_at
     * 
     * @Column(type="datetime", nullable = true)
     */
    protected $updated_at;



    /**
     * Gets the value of transaction_id.
     *
     * @return mixed
     */
    public function getTransactionId()
    {
        return $this->transaction_id;
    }

    /**
     * Sets the value of transaction_id.
     *
     * @param mixed $transaction_id the transaction id
     *
     * @return self
     */
    public function setTransactionId($transaction_id)
    {
        $this->transaction_id = $transaction_id;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getTransactionFromUserId()
    {
        return $this->transaction_from_user_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $transaction_from_user_id the transaction from user id
     *
     * @return self
     */
    public function setTransactionFromUserId($transaction_from_user_id)
    {
        $this->transaction_from_user_id = $transaction_from_user_id;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getTransactionToUserId()
    {
        return $this->transaction_to_user_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $transaction_to_user_id the transaction to user id
     *
     * @return self
     */
    public function setTransactionToUserId($transaction_to_user_id)
    {
        $this->transaction_to_user_id = $transaction_to_user_id;

        return $this;
    }

    /**
     * Gets the value of transaction_type.
     *
     * @return mixed
     */
    public function getTransactionType()
    {
        return $this->transaction_type;
    }

    /**
     * Sets the value of transaction_type.
     *
     * @param mixed $transaction_type the transaction type
     *
     * @return self
     */
    public function setTransactionType($transaction_type)
    {
        $this->transaction_type = $transaction_type;

        return $this;
    }

    /**
     * Gets the value of transaction_amount.
     *
     * @return mixed
     */
    public function getTransactionAmount()
    {
        return $this->transaction_amount;
    }

    /**
     * Sets the value of transaction_amount.
     *
     * @param mixed $transaction_amount the transaction amount
     *
     * @return self
     */
    public function setTransactionAmount($transaction_amount)
    {
        $this->transaction_amount = $transaction_amount;

        return $this;
    }

    /**
     * Gets the value of transaction_status.
     *
     * @return mixed
     */
    public function getTransactionStatus()
    {
        return $this->transaction_status;
    }

    /**
     * Sets the value of transaction_status.
     *
     * @param mixed $transaction_status the transaction status
     *
     * @return self
     */
    public function setTransactionStatus($transaction_status)
    {
        $this->transaction_status = $transaction_status;

        return $this;
    }

    /**
     * Gets the value of transaction_remarks.
     *
     * @return mixed
     */
    public function getTransactionRemarks()
    {
        return $this->transaction_remarks;
    }

    /**
     * Sets the value of transaction_remarks.
     *
     * @param mixed $transaction_remarks the transaction remarks
     *
     * @return self
     */
    public function setTransactionRemarks($transaction_remarks)
    {
        $this->transaction_remarks = $transaction_remarks;

        return $this;
    }

    /**
     * Gets the value of user_purchase_transaction_id.
     *
     * @return mixed
     */
    public function getUserPurchaseTransactionId()
    {
        return $this->user_purchase_transaction_id;
    }

    /**
     * Sets the value of user_purchase_transaction_id.
     *
     * @param mixed $user_purchase_transaction_id the user purchase transaction id
     *
     * @return self
     */
    private function _setUserPurchaseTransactionId($user_purchase_transaction_id)
    {
        $this->user_purchase_transaction_id = $user_purchase_transaction_id;

        return $this;
    }
    /**
     * Gets triggered only on insert

     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_at = new \DateTime("now");
    }


    public function getCreatedAt()
    {
        return $this->created_at;
    }



    /**
     * Gets triggered every time on update

     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_at = new \DateTime("now");
    }
}

?>