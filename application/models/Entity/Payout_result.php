<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Payout_result")
*/
class Payout_result{

  /**
  *
  * @Id
  * @Column(name="payout_result_id",type="integer",nullable=false)
  * @GeneratedValue(strategy="AUTO")
  */
  protected $payout_result_id;

  /**
  *@Column(type="integer")
  ****/
  protected $payout_result_user_id;

  /**
  *@Column(type="string",length=255)
  ****/
  protected $payout_result_left;

  /**
  *@Column(type="integer")
  ****/
  protected $total_left_pv;

  /**
  *@Column(type="integer")
  ****/
  protected $total_right_pv;

  /**
  *@Column(type="integer")
  ****/
  protected $carry_forward_pv;

  /**
  *@Column(type="integer")
  ****/
  protected $carry_forward_pv_side;

  /**
  *@Column(type="integer")
  ****/
  protected $net_payout;
  /**
  *@Column(type="string",length=255)
  ****/
  protected $payout_result_type;
  /**
  *@Column(type="integer")
  ****/
  protected $payment_status;
  /**
  *@Column(type="integer",nullable=true)
  ****/
  // protected $total_gross;

  /**
   * @var datetime $created_at
   *
   * @Column(type="datetime")
   */
  protected $created_at;

  /**
   * @var datetime $updated_at
   * 
   * @Column(type="datetime", nullable = true)
   */
  protected $updated_at;


    /**
     * @return mixed
     */
    public function getPayoutResultId()
    {
        return $this->payout_result_id;
    }

    /**
     * @param mixed $payout_result_id
     *
     * @return self
     */
    public function setPayoutResultId($payout_result_id)
    {
        $this->payout_result_id = $payout_result_id;

        return $this;
    }

    

    /**
     * @return mixed
     */
    public function getPayoutResultLeft()
    {
        return $this->payout_result_left;
    }

    /**
     * @param mixed $payout_result_left
     *
     * @return self
     */
    public function setPayoutResultLeft($payout_result_left)
    {
        $this->payout_result_left = $payout_result_left;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalLeftPv()
    {
        return $this->total_left_pv;
    }

    /**
     * @param mixed $total_left_pv
     *
     * @return self
     */
    public function setTotalLeftPv($total_left_pv)
    {
        $this->total_left_pv = $total_left_pv;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalRightPv()
    {
        return $this->total_right_pv;
    }

    /**
     * @param mixed $total_right_pv
     *
     * @return self
     */
    public function setTotalRightPv($total_right_pv)
    {
        $this->total_right_pv = $total_right_pv;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCarryForwardPv()
    {
        return $this->carry_forward_pv;
    }

    /**
     * @param mixed $carry_forward_pv
     *
     * @return self
     */
    public function setCarryForwardPv($carry_forward_pv)
    {
        $this->carry_forward_pv = $carry_forward_pv;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCarryForwardPvSide()
    {
        return $this->carry_forward_pv_side;
    }

    /**
     * @param mixed $carry_forward_pv_side
     *
     * @return self
     */
    public function setCarryForwardPvSide($carry_forward_pv_side)
    {
        $this->carry_forward_pv_side = $carry_forward_pv_side;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNetPayout()
    {
        return $this->net_payout;
    }

    /**
     * @param mixed $net_payout
     *
     * @return self
     */
    public function setNetPayout($net_payout)
    {
        $this->net_payout = $net_payout;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPayoutResultType()
    {
        return $this->payout_result_type;
    }

    /**
     * @param mixed $payout_result_type
     *
     * @return self
     */
    public function setPayoutResultType($payout_result_type)
    {
        $this->payout_result_type = $payout_result_type;

        return $this;
    }
    /**
     * Gets triggered only on insert

     * @ORM\PrePersist
     */
      public function onPrePersist($d)
      {
          $this->created_at = new \DateTime($d);
      }


      public function getCreatedAt()
      {
          return $this->created_at;
      }

      /**
       * Gets triggered every time on update

       * @ORM\PreUpdate
       */
      public function onPreUpdate()
      {
          $this->updated_at = new \DateTime("now");
      }

    /**
     * @return mixed
     */
    public function getPayoutResultUserId()
    {
        return $this->payout_result_user_id;
    }

    /**
     * @param mixed $payout_result_user_id
     *
     * @return self
     */
    public function setPayoutResultUserId($payout_result_user_id)
    {
        $this->payout_result_user_id = $payout_result_user_id;

        return $this;
    }
     /**
     * @return mixed
     */
    public function getPaymentStatus()
    {
        return $this->payment_status;
    }

    /**
     * @param mixed $payout_result_user_id
     *
     * @return self
     */
    public function setPaymentStatus($payment_status)
    {
        $this->payment_status= $payment_status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalGross()
    {
        return $this->total_gross;
    }

    /**
     * @param mixed $total_gross
     *
     * @return self
     */
    public function setTotalGross($total_gross)
    {
        $this->total_gross = $total_gross;

        return $this;
    }
}
?>