<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Sample")
*/
class Sample{
	
	/**
	*
	* @Id
	* @Column(name="sample_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
    private $node_id;


    /**
     * @var string
     *
     * @Column(name="node_name", type="string", length=255, nullable=false)
     */
    private $node_name;


  /**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\User", fetch="EAGER")
     * @JoinColumn(name="user_id", referencedColumnName="user_id")
     */
  protected $user_id;

    

    /**
     * @return mixed
     */
    public function getNodeId()
    {
        return $this->node_id;
    }

    /**
     * @param mixed $node_id
     *
     * @return self
     */
    public function setNodeId($node_id)
    {
        $this->node_id = $node_id;

        return $this;
    }

    /**
     * @return string
     */
    public function getNodeName()
    {
        return $this->node_name;
    }

    /**
     * @param string $node_name
     *
     * @return self
     */
    public function setNodeName($node_name)
    {
        $this->node_name = $node_name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     *
     * @return self
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }
}

?>