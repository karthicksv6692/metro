<?php
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Product_reviews")
*/
class Product_reviews
{
	/**
	*
	* @Id
	* @Column(name="product_review_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	private $product_review_id;

    /**
     * Gets the value of product_review_id.
     *
     * @return mixed
     */
    public function getProductReviewId()
    {
        return $this->product_review_id;
    }

    /**
     * Sets the value of product_review_id.
     *
     * @param mixed $product_review_id the product review id
     *
     * @return self
     */
    private function _setProductReviewId($product_review_id)
    {
        $this->product_review_id = $product_review_id;

        return $this;
    }
}

?>