<?php
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

class Package_prd_detail
{
	private $user_available_amount;
	private $pack_prd_price;
	private $product_list_array;


    /**
     * Gets the value of user_available_amount.
     *
     * @return mixed
     */
    public function getUserAvailableAmount()
    {
        return $this->user_available_amount;
    }

    /**
     * Sets the value of user_available_amount.
     *
     * @param mixed $user_available_amount the user available amount
     *
     * @return self
     */
    public function setUserAvailableAmount($user_available_amount)
    {
        $this->user_available_amount = $user_available_amount;

        return $this;
    }

    /**
     * Gets the value of pack_prd_price.
     *
     * @return mixed
     */
    public function getPackPrdPrice()
    {
        return $this->pack_prd_price;
    }

    /**
     * Sets the value of pack_prd_price.
     *
     * @param mixed $pack_prd_price the pack prd price
     *
     * @return self
     */
    public function setPackPrdPrice($pack_prd_price)
    {
        $this->pack_prd_price = $pack_prd_price;

        return $this;
    }

    /**
     * Gets the value of product_list_array.
     *
     * @return mixed
     */
    public function getProductListArray()
    {
        return $this->product_list_array;
    }

    /**
     * Sets the value of product_list_array.
     *
     * @param mixed $product_list_array the product list array
     *
     * @return self
     */
    public function setProductListArray($product_list_array)
    {
        $this->product_list_array = $product_list_array;

        return $this;
    }
}

?>