<?php
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Packages")
*/
class Packages
{

	/**
	*
	* @Id
	* @Column(name="package_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
    private $package_id;

     /**
     * @var string
     *
     * @Column(name="package_name", type="string", length=255, nullable=false,unique=true)
     */
    private $package_name;

    /**
     * @var string
     *
     * @Column(name="package_desc", type="string", length=255, nullable=false)
     */
    private $package_desc;

     /**
     * @var string
     *
     * @Column(name="package_price", type="integer")
     */
    private $package_price;

     /**
     * @var string
     *
     * @Column(name="package_point_value", type="integer")
     */
    private $package_point_value;

    /**
     * @var string
     *
     * @Column(name="package_short_desc", type="string", length=255, nullable=false)
     */
    private $package_short_desc;

    /**
     * @var string
     *
     * @Column(name="package_limited_pair_cutoff", type="integer")
     */
    private $package_limited_pair_cutoff;

    /**
     * @var string
     *
     * @Column(name="package_pair_income", type="integer")
     */
    private $package_pair_income;

    /**
     * @var string
     *
     * @Column(name="package_ceiling_per_month", type="integer")
     */
    private $package_ceiling_per_month;

    /**
     * @var string
     *
     * @Column(name="package_ceiling_price", type="integer")
     */
    private $package_ceiling_price;

    /**
     * @var string
     *
     * @Column(name="package_is_visible", type="integer")
     */
    private $package_is_visible;


     /**
     *  One Package have Many Products.
     * @ORM OneToMany(targetEntity="Entity\Product" mappedBy="product_package_id")
     */
    private $product_package_id; 

     /**
     *  One Package have Many Products.
     * @ORM OneToMany(targetEntity="Entity\Offer" mappedBy="offer_item_id")
     */
    private $offer_package_id;

     /**
     *  One Package have Many Products.
     * @ORM OneToMany(targetEntity="Entity\Package_product" mappedBy="package_id")
     */
    private $package_product_id;

     /**
     *
     * @ORM OneToMany(targetEntity="Entity\Pin_request" mappedBy="pin_request_package_id")
     */
    private $pin_request_package_id;

    /**
     * Gets the value of package_id.
     *
     * @return mixed
     */
    /**
     * @var string
     *
     * @Column(name="level_income_amount", type="integer",nullable=true)
     */
    private $level_income_amount;

    public function getPackageId()
    {
        return $this->package_id;
    }

    /**
     * Sets the value of package_id.
     *
     * @param mixed $package_id the package id
     *
     * @return self
     */
    public function setPackageId($package_id)
    {
        $this->package_id = $package_id;

        return $this;
    }

    /**
     * Gets the value of package_name.
     *
     * @return string
     */
    public function getPackageName()
    {
        return $this->package_name;
    }

    /**
     * Sets the value of package_name.
     *
     * @param string $package_name the package name
     *
     * @return self
     */
    public function setPackageName($package_name)
    {
        $this->package_name = $package_name;

        return $this;
    }

    /**
     * Gets the value of package_desc.
     *
     * @return string
     */
    public function getPackageDesc()
    {
        return $this->package_desc;
    }

    /**
     * Sets the value of package_desc.
     *
     * @param string $package_desc the package desc
     *
     * @return self
     */
    public function setPackageDesc($package_desc)
    {
        $this->package_desc = $package_desc;

        return $this;
    }

    /**
     * Gets the value of package_price.
     *
     * @return string
     */
    public function getPackagePrice()
    {
        return $this->package_price;
    }

    /**
     * Sets the value of package_price.
     *
     * @param string $package_price the package price
     *
     * @return self
     */
    public function setPackagePrice($package_price)
    {
        $this->package_price = $package_price;

        return $this;
    }

    /**
     * Gets the value of package_short_desc.
     *
     * @return string
     */
    public function getPackageShortDesc()
    {
        return $this->package_short_desc;
    }

    /**
     * Sets the value of package_short_desc.
     *
     * @param string $package_short_desc the package short desc
     *
     * @return self
     */
    public function setPackageShortDesc($package_short_desc)
    {
        $this->package_short_desc = $package_short_desc;

        return $this;
    }

    /**
     * Gets the value of package_limited_pair_cutoff.
     *
     * @return string
     */
    public function getPackageLimitedPairCutoff()
    {
        return $this->package_limited_pair_cutoff;
    }

    /**
     * Sets the value of package_limited_pair_cutoff.
     *
     * @param string $package_limited_pair_cutoff the package limited pair cutoff
     *
     * @return self
     */
    public function setPackageLimitedPairCutoff($package_limited_pair_cutoff)
    {
        $this->package_limited_pair_cutoff = $package_limited_pair_cutoff;

        return $this;
    }

    /**
     * Gets the value of package_pair_income.
     *
     * @return string
     */
    public function getPackagePairIncome()
    {
        return $this->package_pair_income;
    }

    /**
     * Sets the value of package_pair_income.
     *
     * @param string $package_pair_income the package pair income
     *
     * @return self
     */
    public function setPackagePairIncome($package_pair_income)
    {
        $this->package_pair_income = $package_pair_income;

        return $this;
    }

    /**
     * Gets the value of package_ceiling_per_month.
     *
     * @return string
     */
    public function getPackageCeilingPerMonth()
    {
        return $this->package_ceiling_per_month;
    }

    /**
     * Sets the value of package_ceiling_per_month.
     *
     * @param string $package_ceiling_per_month the package ceiling per month
     *
     * @return self
     */
    public function setPackageCeilingPerMonth($package_ceiling_per_month)
    {
        $this->package_ceiling_per_month = $package_ceiling_per_month;

        return $this;
    }

    /**
     * Gets the value of package_ceiling_price.
     *
     * @return string
     */
    public function getPackageCeilingPrice()
    {
        return $this->package_ceiling_price;
    }

    /**
     * Sets the value of package_ceiling_price.
     *
     * @param string $package_ceiling_price the package ceiling price
     *
     * @return self
     */
    public function setPackageCeilingPrice($package_ceiling_price)
    {
        $this->package_ceiling_price = $package_ceiling_price;

        return $this;
    }

    /**
     * Gets the value of package_is_visible.
     *
     * @return string
     */
    public function getPackageIsVisible()
    {
        return $this->package_is_visible;
    }

    /**
     * Sets the value of package_is_visible.
     *
     * @param string $package_is_visible the package is visible
     *
     * @return self
     */
    public function setPackageIsVisible($package_is_visible)
    {
        $this->package_is_visible = $package_is_visible;

        return $this;
    }

    /**
     * Gets the One Package have Many Products.
     *
     * @return mixed
     */
    public function getProductPackageId()
    {
        return $this->product_package_id;
    }

    /**
     * Sets the One Package have Many Products.
     *
     * @param mixed $product_package_id the product package id
     *
     * @return self
     */
    public function setProductPackageId($product_package_id)
    {
        $this->product_package_id = $product_package_id;

        return $this;
    }

    /**
     * Gets the One Package have Many Products.
     *
     * @return mixed
     */
    public function getOfferPackageId()
    {
        return $this->offer_package_id;
    }

    /**
     * Sets the One Package have Many Products.
     *
     * @param mixed $offer_package_id the offer package id
     *
     * @return self
     */
    public function setOfferPackageId($offer_package_id)
    {
        $this->offer_package_id = $offer_package_id;

        return $this;
    }

    /**
     * Gets the One Package have Many Products.
     *
     * @return mixed
     */
    public function getPackageProductId()
    {
        return $this->package_product_id;
    }

    /**
     * Sets the One Package have Many Products.
     *
     * @param mixed $package_product_id the package product id
     *
     * @return self
     */
    public function setPackageProductId($package_product_id)
    {
        $this->package_product_id = $package_product_id;

        return $this;
    }

    /**
     * Gets the value of pin_request_package_id.
     *
     * @return mixed
     */
    public function getPinRequestPackageId()
    {
        return $this->pin_request_package_id;
    }

    /**
     * Sets the value of pin_request_package_id.
     *
     * @param mixed $pin_request_package_id the pin request package id
     *
     * @return self
     */
    public function setPinRequestPackageId($pin_request_package_id)
    {
        $this->pin_request_package_id = $pin_request_package_id;

        return $this;
    }

    /**
     * @param string $package_point_value
     *
     * @return self
     */
    public function setPackagePointValue($package_point_value)
    {
        $this->package_point_value = $package_point_value;

        return $this;
    }

    /**
     * @return string
     */
    public function getPackagePointValue()
    {
        return $this->package_point_value;
    }

    /**
     * @return string
     */
    public function getLevelIncomeAmount()
    {
        return $this->level_income_amount;
    }

    /**
     * @param string $level_income_amount
     *
     * @return self
     */
    public function setLevelIncomeAmount($level_income_amount)
    {
        $this->level_income_amount = $level_income_amount;

        return $this;
    }
}
?>