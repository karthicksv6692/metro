<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="User_account")
*/
class User_account{	

	/**
	*
	* @Id
	* @Column(name="user_account_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $user_account_id;

	/**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\User", fetch="EAGER")
     * @JoinColumn(name="user_id", referencedColumnName="user_id",onDelete="cascade")
     */
	protected $user_id;

	/**
	*@Column(type="integer",nullable=true)
	****/
	protected $user_account_available_amount;
	/**
	*@Column(type="integer",nullable=true)
	****/
	protected $user_account_repurchase_amount;

	/**
	*@Column(type="integer",nullable=true)
	****/
	protected $user_account_transfer_per_day_limit_amount;

	/**
	*@Column(type="integer",nullable=true)
	****/
	protected $user_account_transfer_per_day_limit_transfer;

	/**
	*@Column(type="integer",nullable=true)
	****/
	protected $user_account_transfer_per_month_limit_amount;

	/**
	*@Column(type="integer",nullable=true)
	****/
	protected $user_account_transfer_per_month_limit_transfer;

	/**
	*@Column(type="integer",nullable=true)
	****/
	protected $user_account_per_day_transfer_enable;

    /**
    *@Column(type="integer",nullable=true)
    ****/
    protected $user_account_per_month_transfer_enable;


	/**
	*@Column(type="integer",nullable=true)
	****/
	protected $user_account_hold_amount;
	/**
	*@Column(type="integer",nullable=true)
	****/
	protected $user_account_hold_enabled;

	/**
	*@Column(type="integer",nullable=true)
	****/
	protected $user_account_is_active;

    /**
    *@Column(type="integer",nullable=true)
    ****/
    protected $user_account_transfer_enabled;


	/**
	*@Column(type="string",length=255,nullable=true)
	****/
	protected $user_account_locked_reason;


    /**
     * @var datetime $created_at
     *
     * @Column(type="datetime")
     */
    protected $created_at;

    /**
     * @var datetime $updated_at
     * 
     * @Column(type="datetime", nullable = true)
     */
    protected $updated_at;


    /**
     * Gets the value of user_account_id.
     *
     * @return mixed
     */
    public function getUserAccountId()
    {
        return $this->user_account_id;
    }

    /**
     * Sets the value of user_account_id.
     *
     * @param mixed $user_account_id the user account id
     *
     * @return self
     */
    public function setUserAccountId($user_account_id)
    {
        $this->user_account_id = $user_account_id;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $user_id the user id
     *
     * @return self
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Gets the value of user_account_available_amount.
     *
     * @return mixed
     */
    public function getUserAccountAvailableAmount()
    {
        return $this->user_account_available_amount;
    }

    /**
     * Sets the value of user_account_available_amount.
     *
     * @param mixed $user_account_available_amount the user account available amount
     *
     * @return self
     */
    public function setUserAccountAvailableAmount($user_account_available_amount)
    {
        $this->user_account_available_amount = $user_account_available_amount;

        return $this;
    }

    /**
     * Gets the value of user_account_repurchase_amount.
     *
     * @return mixed
     */
    public function getUserAccountRepurchaseAmount()
    {
        return $this->user_account_repurchase_amount;
    }

    /**
     * Sets the value of user_account_repurchase_amount.
     *
     * @param mixed $user_account_repurchase_amount the user account repurchase amount
     *
     * @return self
     */
    public function setUserAccountRepurchaseAmount($user_account_repurchase_amount)
    {
        $this->user_account_repurchase_amount = $user_account_repurchase_amount;

        return $this;
    }

    /**
     * Gets the value of user_account_transfer_per_day_limit_amount.
     *
     * @return mixed
     */
    public function getUserAccountTransferPerDayLimitAmount()
    {
        return $this->user_account_transfer_per_day_limit_amount;
    }

    /**
     * Sets the value of user_account_transfer_per_day_limit_amount.
     *
     * @param mixed $user_account_transfer_per_day_limit_amount the user account transfer per day limit amount
     *
     * @return self
     */
    public function setUserAccountTransferPerDayLimitAmount($user_account_transfer_per_day_limit_amount)
    {
        $this->user_account_transfer_per_day_limit_amount = $user_account_transfer_per_day_limit_amount;

        return $this;
    }

    /**
     * Gets the value of user_account_transfer_per_day_limit_transfer.
     *
     * @return mixed
     */
    public function getUserAccountTransferPerDayLimitTransfer()
    {
        return $this->user_account_transfer_per_day_limit_transfer;
    }

    /**
     * Sets the value of user_account_transfer_per_day_limit_transfer.
     *
     * @param mixed $user_account_transfer_per_day_limit_transfer the user account transfer per day limit transfer
     *
     * @return self
     */
    public function setUserAccountTransferPerDayLimitTransfer($user_account_transfer_per_day_limit_transfer)
    {
        $this->user_account_transfer_per_day_limit_transfer = $user_account_transfer_per_day_limit_transfer;

        return $this;
    }

    /**
     * Gets the value of user_account_transfer_per_month_limit_amount.
     *
     * @return mixed
     */
    public function getUserAccountTransferPerMonthLimitAmount()
    {
        return $this->user_account_transfer_per_month_limit_amount;
    }

    /**
     * Sets the value of user_account_transfer_per_month_limit_amount.
     *
     * @param mixed $user_account_transfer_per_month_limit_amount the user account transfer per month limit amount
     *
     * @return self
     */
    public function setUserAccountTransferPerMonthLimitAmount($user_account_transfer_per_month_limit_amount)
    {
        $this->user_account_transfer_per_month_limit_amount = $user_account_transfer_per_month_limit_amount;

        return $this;
    }

    /**
     * Gets the value of user_account_transfer_per_month_limit_transfer.
     *
     * @return mixed
     */
    public function getUserAccountTransferPerMonthLimitTransfer()
    {
        return $this->user_account_transfer_per_month_limit_transfer;
    }

    /**
     * Sets the value of user_account_transfer_per_month_limit_transfer.
     *
     * @param mixed $user_account_transfer_per_month_limit_transfer the user account transfer per month limit transfer
     *
     * @return self
     */
    public function setUserAccountTransferPerMonthLimitTransfer($user_account_transfer_per_month_limit_transfer)
    {
        $this->user_account_transfer_per_month_limit_transfer = $user_account_transfer_per_month_limit_transfer;

        return $this;
    }

    /**
     * Gets the value of user_account_per_day_transfer_enable.
     *
     * @return mixed
     */
    public function getUserAccountPerDayTransferEnable()
    {
        return $this->user_account_per_day_transfer_enable;
    }

    /**
     * Sets the value of user_account_per_day_transfer_enable.
     *
     * @param mixed $user_account_per_day_transfer_enable the user account per day transfer enable
     *
     * @return self
     */
    public function setUserAccountPerDayTransferEnable($user_account_per_day_transfer_enable)
    {
        $this->user_account_per_day_transfer_enable = $user_account_per_day_transfer_enable;

        return $this;
    }

     /**
     * Gets the value of user_account_per_day_transfer_enable.
     *
     * @return mixed
     */
    public function getUserAccountPerMonthTransferEnable()
    {
        return $this->user_account_per_month_transfer_enable;
    }

    /**
     * Sets the value of user_account_per_day_transfer_enable.
     *
     * @param mixed $user_account_per_day_transfer_enable the user account per day transfer enable
     *
     * @return self
     */
    public function setUserAccountPerMonthTransferEnable($user_account_per_month_transfer_enable)
    {
        $this->user_account_per_month_transfer_enable = $user_account_per_month_transfer_enable;

        return $this;
    }

    /**
     * Gets the value of user_account_hold_amount.
     *
     * @return mixed
     */
    public function getUserAccountHoldAmount()
    {
        return $this->user_account_hold_amount;
    }

    /**
     * Sets the value of user_account_hold_amount.
     *
     * @param mixed $user_account_hold_amount the user account hold amount
     *
     * @return self
     */
    public function setUserAccountHoldAmount($user_account_hold_amount)
    {
        $this->user_account_hold_amount = $user_account_hold_amount;

        return $this;
    }

    /**
     * Gets the value of user_account_hold_enabled.
     *
     * @return mixed
     */
    public function getUserAccountHoldEnabled()
    {
        return $this->user_account_hold_enabled;
    }

    /**
     * Sets the value of user_account_hold_enabled.
     *
     * @param mixed $user_account_hold_enabled the user account hold enabled
     *
     * @return self
     */
    public function setUserAccountHoldEnabled($user_account_hold_enabled)
    {
        $this->user_account_hold_enabled = $user_account_hold_enabled;

        return $this;
    }

    /**
     * Gets the value of user_account_is_active.
     *
     * @return mixed
     */
    public function getUserAccountIsActive()
    {
        return $this->user_account_is_active;
    }

    /**
     * Sets the value of user_account_is_active.
     *
     * @param mixed $user_account_is_active the user account is active
     *
     * @return self
     */
    public function setUserAccountIsActive($user_account_is_active)
    {
        $this->user_account_is_active = $user_account_is_active;

        return $this;
    }

    /**
     * Gets the value of user_account_locked_reason.
     *
     * @return mixed
     */
    public function getUserAccountLockedReason()
    {
        return $this->user_account_locked_reason;
    }

    /**
     * Sets the value of user_account_locked_reason.
     *
     * @param mixed $user_account_locked_reason the user account locked reason
     *
     * @return self
     */
    public function setUserAccountLockedReason($user_account_locked_reason)
    {
        $this->user_account_locked_reason = $user_account_locked_reason;

        return $this;
    }

    /**
     * Gets the value of user_account_is_active.
     *
     * @return mixed
     */
    public function getUserAccountTransferEnable()
    {
        return $this->user_account_transfer_enabled;
    }

    /**
     * Sets the value of user_account_is_active.
     *
     * @param mixed $user_account_is_active the user account is active
     *
     * @return self
     */
    public function setUserAccountTransferEnable($user_account_transfer_enabled)
    {
        $this->user_account_transfer_enabled = $user_account_transfer_enabled;

        return $this;
    }
     /**
     * Gets triggered only on insert

     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_at = new \DateTime("now");
    }


    public function getCreatedAt()
    {
        return $this->created_at;
    }



    /**
     * Gets triggered every time on update

     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_at = new \DateTime("now");
    }
}

?>