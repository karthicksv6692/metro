<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Password")
*/
class Password{
	/**
	*
	* @Id
	* @Column(name="password_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $password_id;

	/**
	*@Column(type="string",length=255,nullable=false)
	****/
	protected $password;

	/**
	*@Column(type="string",length=255,nullable=false)
	****/
	protected $salt;

	/**
	*@Column(type="string",length=255,nullable=true)
	****/
	protected $password_prev;

	/**
	*@Column(type="string",length=255,nullable=true)
	****/
	protected $password_req_reset_password;

    /**
    *@Column(type="datetime",length=255,nullable=true)
    ****/
    protected $password_req_date;

	/**
	*@Column(type="integer",nullable=true)
	****/
	protected $forget_password_request_or_not;

	/**
	*@Column(type="string",length=255,nullable=true)
	****/
	protected $forget_password_url;
	/**
	*@Column(type="integer",nullable=true)
	****/
	protected $forget_password_validity;
	/**
	*@Column(type="string",length=255,nullable=true)
	****/
	protected $question1;
	/**
	*@Column(type="string",length=255,nullable=true)
	****/
	protected $question2;
	/**
	*@Column(type="string",length=255,nullable=true)
	****/
	protected $question3;
	/**
	*@Column(type="string",length=255,nullable=true)
	****/
	protected $answer1;
	/**
	*@Column(type="string",length=255,nullable=true)
	****/
	protected $answer2;
	/**
	*@Column(type="string",length=255,nullable=true)
	****/
	protected $answer3;


	/**
     *
     * @ORM OneToMany(targetEntity="Entity\User" mappedBy="password_id")
     */
    private $user_contact_id;

    /**
     * Gets the value of password_id.
     *
     * @return mixed
     */
    public function getPasswordId()
    {
        return $this->password_id;
    }

    /**
     * Sets the value of password_id.
     *
     * @param mixed $password_id the password id
     *
     * @return self
     */
    public function setPasswordId($password_id)
    {
        $this->password_id = $password_id;

        return $this;
    }

    /**
     * Gets the value of password.
     *
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Sets the value of password.
     *
     * @param mixed $password the password
     *
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Gets the value of salt.
     *
     * @return mixed
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Sets the value of salt.
     *
     * @param mixed $salt the salt
     *
     * @return self
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Gets the value of password_prev.
     *
     * @return mixed
     */
    public function getPasswordPrev()
    {
        return $this->password_prev;
    }

    /**
     * Sets the value of password_prev.
     *
     * @param mixed $password_prev the password prev
     *
     * @return self
     */
    public function setPasswordPrev($password_prev)
    {
        $this->password_prev = $password_prev;

        return $this;
    }

    /**
     * Gets the value of password_req_reset_password.
     *
     * @return mixed
     */
    public function getPasswordReqResetPassword()
    {
        return $this->password_req_reset_password;
    }

    /**
     * Sets the value of password_req_reset_password.
     *
     * @param mixed $password_req_reset_password the password req reset password
     *
     * @return self
     */
    public function setPasswordReqResetPassword($password_req_reset_password)
    {
        $this->password_req_reset_password = $password_req_reset_password;

        return $this;
    }

  /**
     * Gets the value of password_req_reset_password.
     *
     * @return mixed
     */
    public function getPasswordRequestOrNot()
    {
        return $this->forget_password_request_or_not;
    }

    /**
     * Sets the value of password_req_reset_password.
     *
     * @param mixed $password_req_reset_password the password req reset password
     *
     * @return self
     */
    public function setPasswordRequestOrNot($forget_password_request_or_not)
    {
        $this->forget_password_request_or_not = $forget_password_request_or_not;

        return $this;
    }

    /**
     * Gets the value of password_req_date.
     *
     * @return mixed
     */
    public function getPasswordReqDate()
    {
        return $this->password_req_date;
    }

    /**
     * Sets the value of password_req_date.
     *
     * @param mixed $password_req_date the password req date
     *
     * @return self
     */
    public function setPasswordReqDate($password_req_date)
    {
        $this->password_req_date = $password_req_date;

        return $this;
    }

    /**
     * Gets the value of forget_password_url.
     *
     * @return mixed
     */
    public function getForgetPasswordUrl()
    {
        return $this->forget_password_url;
    }

    /**
     * Sets the value of forget_password_url.
     *
     * @param mixed $forget_password_url the forget password url
     *
     * @return self
     */
    public function setForgetPasswordUrl($forget_password_url)
    {
        $this->forget_password_url = $forget_password_url;

        return $this;
    }

    /**
     * Gets the value of forget_password_validity.
     *
     * @return mixed
     */
    public function getForgetPasswordValidity()
    {
        return $this->forget_password_validity;
    }

    /**
     * Sets the value of forget_password_validity.
     *
     * @param mixed $forget_password_validity the forget password validity
     *
     * @return self
     */
    public function setForgetPasswordValidity($forget_password_validity)
    {
        $this->forget_password_validity = $forget_password_validity;

        return $this;
    }

    /**
     * Gets the value of question1.
     *
     * @return mixed
     */
    public function getQuestion1()
    {
        return $this->question1;
    }

    /**
     * Sets the value of question1.
     *
     * @param mixed $question1 the question1
     *
     * @return self
     */
    public function setQuestion1($question1)
    {
        $this->question1 = $question1;

        return $this;
    }

    /**
     * Gets the value of question2.
     *
     * @return mixed
     */
    public function getQuestion2()
    {
        return $this->question2;
    }

    /**
     * Sets the value of question2.
     *
     * @param mixed $question2 the question2
     *
     * @return self
     */
    public function setQuestion2($question2)
    {
        $this->question2 = $question2;

        return $this;
    }

    /**
     * Gets the value of question3.
     *
     * @return mixed
     */
    public function getQuestion3()
    {
        return $this->question3;
    }

    /**
     * Sets the value of question3.
     *
     * @param mixed $question3 the question3
     *
     * @return self
     */
    public function setQuestion3($question3)
    {
        $this->question3 = $question3;

        return $this;
    }

    /**
     * Gets the value of answer1.
     *
     * @return mixed
     */
    public function getAnswer1()
    {
        return $this->answer1;
    }

    /**
     * Sets the value of answer1.
     *
     * @param mixed $answer1 the answer1
     *
     * @return self
     */
    public function setAnswer1($answer1)
    {
        $this->answer1 = $answer1;

        return $this;
    }

    /**
     * Gets the value of answer2.
     *
     * @return mixed
     */
    public function getAnswer2()
    {
        return $this->answer2;
    }

    /**
     * Sets the value of answer2.
     *
     * @param mixed $answer2 the answer2
     *
     * @return self
     */
    public function setAnswer2($answer2)
    {
        $this->answer2 = $answer2;

        return $this;
    }

    /**
     * Gets the value of answer3.
     *
     * @return mixed
     */
    public function getAnswer3()
    {
        return $this->answer3;
    }

    /**
     * Sets the value of answer3.
     *
     * @param mixed $answer3 the answer3
     *
     * @return self
     */
    public function setAnswer3($answer3)
    {
        $this->answer3 = $answer3;

        return $this;
    }

    /**
     * Gets the value of user_contact_id.
     *
     * @return mixed
     */
    public function getUserContactId()
    {
        return $this->user_contact_id;
    }

    /**
     * Sets the value of user_contact_id.
     *
     * @param mixed $user_contact_id the user contact id
     *
     * @return self
     */
    private function _setUserContactId($user_contact_id)
    {
        $this->user_contact_id = $user_contact_id;

        return $this;
    }
}

?>