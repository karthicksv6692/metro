<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="User_messages")
*/
class User_messages{

	/**
	*
	* @Id
	* @Column(name="user_messages_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $user_messages_id;

	/**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\User", fetch="EAGER")
     * @JoinColumn(name="user_messages_from_id", referencedColumnName="user_id",onDelete="cascade")
     */
	protected $user_messages_from_id;
	/**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\User", fetch="EAGER")
     * @JoinColumn(name="user_messages_to_id", referencedColumnName="user_id",onDelete="cascade")
     */
	protected $user_messages_to_id;

	/**
	*@Column(type="string",length=255,nullable=false)
	****/
	protected $user_messages_subject;


	/**
	*@Column(type="string",length=255,nullable=false)
	****/
	protected $user_messages_detail;

	/**
	*@Column(type="datetime")
	****/
	protected $user_messages_received_date;

	/**
	*@Column(type="integer")
	****/
	protected $user_messages_has_read;


	/**
	*@Column(type="string",length=255,nullable=false)
	****/
	protected $user_messages_attachment_ids;


	/**
	*@Column(type="integer")
	****/
	protected $user_messages_is_delete;
	/**
	*@Column(type="integer")
	****/
	protected $user_messages_is_important;


    /**
     * Gets the value of user_messages_id.
     *
     * @return mixed
     */
    public function getUserMessagesId()
    {
        return $this->user_messages_id;
    }

    /**
     * Sets the value of user_messages_id.
     *
     * @param mixed $user_messages_id the user messages id
     *
     * @return self
     */
    public function setUserMessagesId($user_messages_id)
    {
        $this->user_messages_id = $user_messages_id;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getUserMessagesFromId()
    {
        return $this->user_messages_from_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $user_messages_from_id the user messages from id
     *
     * @return self
     */
    public function setUserMessagesFromId($user_messages_from_id)
    {
        $this->user_messages_from_id = $user_messages_from_id;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getUserMessagesToId()
    {
        return $this->user_messages_to_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $user_messages_to_id the user messages to id
     *
     * @return self
     */
    public function setUserMessagesToId($user_messages_to_id)
    {
        $this->user_messages_to_id = $user_messages_to_id;

        return $this;
    }

    /**
     * Gets the value of user_messages_subject.
     *
     * @return mixed
     */
    public function getUserMessagesSubject()
    {
        return $this->user_messages_subject;
    }

    /**
     * Sets the value of user_messages_subject.
     *
     * @param mixed $user_messages_subject the user messages subject
     *
     * @return self
     */
    public function setUserMessagesSubject($user_messages_subject)
    {
        $this->user_messages_subject = $user_messages_subject;

        return $this;
    }

    /**
     * Gets the value of user_messages_detail.
     *
     * @return mixed
     */
    public function getUserMessagesDetail()
    {
        return $this->user_messages_detail;
    }

    /**
     * Sets the value of user_messages_detail.
     *
     * @param mixed $user_messages_detail the user messages detail
     *
     * @return self
     */
    public function setUserMessagesDetail($user_messages_detail)
    {
        $this->user_messages_detail = $user_messages_detail;

        return $this;
    }

    /**
     * Gets the value of user_messages_received_date.
     *
     * @return mixed
     */
    public function getUserMessagesReceivedDate()
    {
        return $this->user_messages_received_date;
    }

    /**
     * Sets the value of user_messages_received_date.
     *
     * @param mixed $user_messages_received_date the user messages received date
     *
     * @return self
     */
    public function setUserMessagesReceivedDate($user_messages_received_date)
    {
        $this->user_messages_received_date = $user_messages_received_date;

        return $this;
    }

    /**
     * Gets the value of user_messages_has_read.
     *
     * @return mixed
     */
    public function getUserMessagesHasRead()
    {
        return $this->user_messages_has_read;
    }

    /**
     * Sets the value of user_messages_has_read.
     *
     * @param mixed $user_messages_has_read the user messages has read
     *
     * @return self
     */
    public function setUserMessagesHasRead($user_messages_has_read)
    {
        $this->user_messages_has_read = $user_messages_has_read;

        return $this;
    }

    /**
     * Gets the value of user_messages_attachment_ids.
     *
     * @return mixed
     */
    public function getUserMessagesAttachmentIds()
    {
        return $this->user_messages_attachment_ids;
    }

    /**
     * Sets the value of user_messages_attachment_ids.
     *
     * @param mixed $user_messages_attachment_ids the user messages attachment ids
     *
     * @return self
     */
    public function setUserMessagesAttachmentIds($user_messages_attachment_ids)
    {
        $this->user_messages_attachment_ids = $user_messages_attachment_ids;

        return $this;
    }

    /**
     * Gets the value of user_messages_is_delete.
     *
     * @return mixed
     */
    public function getUserMessagesIsDelete()
    {
        return $this->user_messages_is_delete;
    }

    /**
     * Sets the value of user_messages_is_delete.
     *
     * @param mixed $user_messages_is_delete the user messages is delete
     *
     * @return self
     */
    public function setUserMessagesIsDelete($user_messages_is_delete)
    {
        $this->user_messages_is_delete = $user_messages_is_delete;

        return $this;
    }

    /**
     * Gets the value of user_messages_is_important.
     *
     * @return mixed
     */
    public function getUserMessagesIsImportant()
    {
        return $this->user_messages_is_important;
    }

    /**
     * Sets the value of user_messages_is_important.
     *
     * @param mixed $user_messages_is_important the user messages is important
     *
     * @return self
     */
    public function setUserMessagesIsImportant($user_messages_is_important)
    {
        $this->user_messages_is_important = $user_messages_is_important;

        return $this;
    }
}

?>