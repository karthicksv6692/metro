<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

class Single_mail{

	protected $from_user;
	protected $to_user;
	protected $obj;


    /**
     * Gets the value of from_user.
     *
     * @return mixed
     */
    public function getFromUser()
    {
        return $this->from_user;
    }

    /**
     * Sets the value of from_user.
     *
     * @param mixed $from_user the from user
     *
     * @return self
     */
    public function setFromUser($from_user)
    {
        $this->from_user = $from_user;

        return $this;
    }

    /**
     * Gets the value of to_user.
     *
     * @return mixed
     */
    public function getToUser()
    {
        return $this->to_user;
    }

    /**
     * Sets the value of to_user.
     *
     * @param mixed $to_user the to user
     *
     * @return self
     */
    public function setToUser($to_user)
    {
        $this->to_user = $to_user;

        return $this;
    }

    /**
     * Gets the value of obj.
     *
     * @return mixed
     */
    public function getObj()
    {
        return $this->obj;
    }

    /**
     * Sets the value of obj.
     *
     * @param mixed $obj the obj
     *
     * @return self
     */
    public function setObj($obj)
    {
        $this->obj = $obj;

        return $this;
    }
}

?>