<?php 
  namespace Entity;
  use Doctrine\ORM\Mapping as ORM;
  use Doctrine\Common\Collections\Collection;
  use Doctrine\Common\Collections\ArrayCollection;
  use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
  /**
  * @Entity
  * @Table(name="Payout")
  */
  class Payout{
    /**
    *
    * @Id
    * @Column(name="payout_id",type="integer",nullable=false)
    * @GeneratedValue(strategy="AUTO")
    */
    protected $payout_id;

    /**
    *@Column(type="integer")
    ****/
    protected $user_purchase_id;

    /**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Packages", fetch="EAGER")
     * @JoinColumn(name="package_id", referencedColumnName="package_id")
     */
    protected $package_id;


    /**
    *@Column(type="integer")
    ****/
    protected $personal_left;

    /**
    *@Column(type="integer")
    ****/
    protected $personal_right;
    /**
    *@Column(type="integer")
    ****/
    protected $current_left;

    /**
    *@Column(type="integer")
    ****/
    protected $current_right;

     /**
    *@Column(type="integer")
    ****/
    protected $previous_left;
      /**
    *@Column(type="integer")
    ****/
    protected $previous_right;


    /**
    *@Column(type="integer")
    ****/
    protected $total_left;

    /**
    *@Column(type="integer")
    ****/
    protected $total_right;
    /**
    *@Column(type="integer")
    ****/
    protected $carry_forward;

    /**
    *@Column(type="integer")
    ****/
    protected $pair_details_total;

    /**
    *@Column(type="integer")
    ****/
    protected $pair_details_flushed;


    /**
    *@Column(type="integer")
    ****/
    protected $total_pv_left;

    /**
    *@Column(type="integer")
    ****/
    protected $total_pv_right;


    /**
    *@Column(type="integer")
    ****/
    protected $carry_forward_side;

     /**
     * @var datetime $created_at
     *
     * @Column(type="datetime")
     */
    protected $created_at;

    /**
     * @var datetime $updated_at
     * 
     * @Column(type="datetime", nullable = true)
     */
    protected $updated_at;
  
   

    /**
     * Gets triggered only on insert

     * @ORM\PrePersist
     */
      public function onPrePersist($d)
      {
          $this->created_at = new \DateTime($d);
      }


      public function getCreatedAt()
      {
          return $this->created_at;
      }

      /**
       * Gets triggered every time on update

       * @ORM\PreUpdate
       */
      public function onPreUpdate()
      {
          $this->updated_at = new \DateTime("now");
      }
   

    /**
     * @return mixed
     */
    public function getPayoutId()
    {
        return $this->payout_id;
    }

    /**
     * @param mixed $payout_id
     *
     * @return self
     */
    public function setPayoutId($payout_id)
    {
        $this->payout_id = $payout_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserPurchaseId()
    {
        return $this->user_purchase_id;
    }

    /**
     * @param mixed $user_purchase_id
     *
     * @return self
     */
    public function setUserPurchaseId($user_purchase_id)
    {
        $this->user_purchase_id = $user_purchase_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPackageId()
    {
        return $this->package_id;
    }

    /**
     * @param mixed $package_id
     *
     * @return self
     */
    public function setPackageId($package_id)
    {
        $this->package_id = $package_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPersonalLeft()
    {
        return $this->personal_left;
    }

    /**
     * @param mixed $personal_left
     *
     * @return self
     */
    public function setPersonalLeft($personal_left)
    {
        $this->personal_left = $personal_left;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPersonalRight()
    {
        return $this->personal_right;
    }

    /**
     * @param mixed $personal_right
     *
     * @return self
     */
    public function setPersonalRight($personal_right)
    {
        $this->personal_right = $personal_right;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrentLeft()
    {
        return $this->current_left;
    }

    /**
     * @param mixed $current_left
     *
     * @return self
     */
    public function setCurrentLeft($current_left)
    {
        $this->current_left = $current_left;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrentRight()
    {
        return $this->current_right;
    }

    /**
     * @param mixed $current_right
     *
     * @return self
     */
    public function setCurrentRight($current_right)
    {
        $this->current_right = $current_right;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalLeft()
    {
        return $this->total_left;
    }

    /**
     * @param mixed $total_left
     *
     * @return self
     */
    public function setTotalLeft($total_left)
    {
        $this->total_left = $total_left;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalRight()
    {
        return $this->total_right;
    }

    /**
     * @param mixed $total_right
     *
     * @return self
     */
    public function setTotalRight($total_right)
    {
        $this->total_right = $total_right;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCarryForward()
    {
        return $this->carry_forward;
    }

    /**
     * @param mixed $carry_forward
     *
     * @return self
     */
    public function setCarryForward($carry_forward)
    {
        $this->carry_forward = $carry_forward;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCarryForwardSide()
    {
        return $this->carry_forward_side;
    }

    /**
     * @param mixed $carry_forward_side
     *
     * @return self
     */
    public function setCarryForwardSide($carry_forward_side)
    {
        $this->carry_forward_side = $carry_forward_side;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalPvLeft()
    {
        return $this->total_pv_left;
    }

    /**
     * @param mixed $total_pv_left
     *
     * @return self
     */
    public function setTotalPvLeft($total_pv_left)
    {
        $this->total_pv_left = $total_pv_left;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalPvRight()
    {
        return $this->total_pv_right;
    }

    /**
     * @param mixed $total_pv_right
     *
     * @return self
     */
    public function setTotalPvRight($total_pv_right)
    {
        $this->total_pv_right = $total_pv_right;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPreviousLeft()
    {
        return $this->previous_left;
    }

    /**
     * @param mixed $previous_left
     *
     * @return self
     */
    public function setPreviousLeft($previous_left)
    {
        $this->previous_left = $previous_left;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPreviousRight()
    {
        return $this->previous_right;
    }

    /**
     * @param mixed $previous_right
     *
     * @return self
     */
    public function setPreviousRight($previous_right)
    {
        $this->previous_right = $previous_right;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPairDetailsTotal()
    {
        return $this->pair_details_total;
    }

    /**
     * @param mixed $pair_details_total
     *
     * @return self
     */
    public function setPairDetailsTotal($pair_details_total)
    {
        $this->pair_details_total = $pair_details_total;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPairDetailsFlushed()
    {
        return $this->pair_details_flushed;
    }

    /**
     * @param mixed $pair_details_flushed
     *
     * @return self
     */
    public function setPairDetailsFlushed($pair_details_flushed)
    {
        $this->pair_details_flushed = $pair_details_flushed;

        return $this;
    }
}

?>