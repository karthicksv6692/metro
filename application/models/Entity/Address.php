<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
// use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Address")
* 
*/
class Address{

    /**
    *
    * @Id
    * @Column(name="address_id",type="integer",nullable=false)
    * @GeneratedValue(strategy="AUTO")
    */
    protected $address_id;

    /**
    *@Column(type="string",length=255,nullable=false)
    ****/
    protected $address_full_address;

    /**
     * Many address have One pincode.
     * @ManyToOne(targetEntity="Entity\Pincode",fetch="EAGER", cascade="persist")
     * @JoinColumn(name="address_pincode_id", referencedColumnName="pincode_id", onDelete="SET NULL")
     */
    protected $address_pincode_id;

     /**
     *
     * @ORM ManyToOne(targetEntity="Entity\Bank_details" mappedBy="bank_id")
     */
    private $bank_address_id;


    /**
     *
     * @ORM ManyToOne(targetEntity="Entity\Vendor" mappedBy="vendor_address_id")
     */
    private $vendor_address_id;



    public function get_Address_id()
    {
        return $this->address_id;
    }
    public function set_Address_id($address_id)
    {
        $this->address_id = $address_id;
        return $this;
    }
    public function set_Address_full_address($address_full_address)
    {
        $this->address_full_address = $address_full_address;
        return $this;
    }
    public function get_Address_full_address()
    {
        return $this->address_full_address;
    }
    public function set_Address_pincode_id($address_pincode_id)
    {
        $this->address_pincode_id = $address_pincode_id;
        return $this;
    }
    public function get_Address_pincode_id()
    {
        return $this->address_pincode_id;
    }
}

 ?>