<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* Pincode Model
* @Entity
* @Table(name="Pincode")
*
*/
class Pincode{

	/**
	*
	* @Id
	* @Column(name="pincode_id",type="integer",nullable=false,unique=true)
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $pincode_id;

	/**
	*@Column(type="integer",length=6,unique=true)
	*
	*/
	protected $pincode_no;

	/**
	*@Column(type="string",length=255)
	****/
	protected $pincode_place;

	/**
	*@Column(type="string",length=255)
	****/
	protected $pincode_district;

	/**
	*@Column(type="string",length=255)
	****/
	protected $pincode_state;

	/**
     * Many pincode have One country.
     * @ManyToOne(targetEntity="Entity\Country",fetch="EAGER", cascade="persist")
     * @JoinColumn(name="pincode_country_id", referencedColumnName="country_id", onDelete="SET NULL")
     */
    protected $pincode_country_id;

    // public function __construct()
    // {
    //     //$this->contact = new ArrayCollection;
    // }
    public function getPincodeId()
    {
        return $this->pincode_id;
    }
    public function setPincodeNo($pincode_no)
    {
        $this->pincode_no = $pincode_no;
        return $this;
    }
    public function getPincodeNo()
    {
        return $this->pincode_no;
    }
    public function setPincodePlace($pincode_place)
    {
        $this->pincode_place = $pincode_place;
        return $this;
    }
    public function getPincodePlace()
    {
        return $this->pincode_place;
    }
    public function setPincodeDistrict($pincode_district)
    {
        $this->pincode_district = $pincode_district;
        return $this;
    }
    public function getPincodeDistrict()
    {
        return $this->pincode_district;
    }
    public function setPincodeState($pincode_state)
    {
        $this->pincode_state = $pincode_state;
        return $this;
    }
    public function getPincodeState()
    {
        return $this->pincode_state;
    }
   
    /**
     * Sets the value of pincode_country_id.
     *
     * @param mixed $pincode_country_id the country id
     *
     * @return self
     */
    public function setPincodeCountryId($pincode_country_id)
    {
        $this->pincode_country_id = $pincode_country_id;
        return $this;
    }
    public function getPincodeCountryId()
    {
        return $this->pincode_country_id;
    }
    
}



 ?>