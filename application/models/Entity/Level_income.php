<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Level_income")
*/
class Level_income{

  /**
  *
  * @Id
  * @Column(name="income_id",type="integer",nullable=false)
  * @GeneratedValue(strategy="AUTO")
  */
  protected $income_id;
  /**
   * Many Features have One Product.
   * @ManyToOne(targetEntity="Entity\User", fetch="EAGER")
   * @JoinColumn(name="user_id", referencedColumnName="user_id")
   */
  protected $user_id;

  /**
  *@Column(type="integer",nullable=true)
  ****/
  protected $amount;
  /**
   * Many Features have One Product.
   * @ManyToOne(targetEntity="Entity\User", fetch="EAGER")
   * @JoinColumn(name="achieve_user_id", referencedColumnName="user_id")
   */
  protected $achieve_user_id;
  /**
   * @var datetime $created_at
   *
   * @Column(type="datetime")
   */
  protected $created_at;

  /**
   * @var datetime $updated_at
   * 
   * @Column(type="datetime", nullable = true)
   */
  protected $updated_at;


    /**
     * @return mixed
     */
    public function getIncomeId()
    {
        return $this->income_id;
    }

    /**
     * @param mixed $income_id
     *
     * @return self
     */
    public function setIncomeId($income_id)
    {
        $this->income_id = $income_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     *
     * @return self
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     *
     * @return self
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAchieveUserId()
    {
        return $this->achieve_user_id;
    }

    /**
     * @param mixed $achieve_user_id
     *
     * @return self
     */
    public function setAchieveUserId($achieve_user_id)
    {
        $this->achieve_user_id = $achieve_user_id;

        return $this;
    }
    /**
     * Gets triggered only on insert

     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_at = new \DateTime("now");
    }
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    /**
     * Gets triggered every time on update

     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_at = new \DateTime("now");
    }
}
?>