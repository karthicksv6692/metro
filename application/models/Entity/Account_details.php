<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Account_details")
*/
class Account_details{

	/**
	*
	* @Id
	* @Column(name="account_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $account_id;

	/**
	*@Column(type="string",nullable=false,unique=true)
	****/
	protected $account_no;

	/**
	*@Column(type="string",length=255)
	****/
	protected $account_type	;

	/**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Bank_details", fetch="EAGER")
     * @JoinColumn(name="bank_id", referencedColumnName="bank_id")
     */
	protected $bank_id;

	/**
	*@Column(type="string",length=255,nullable=false)
	****/
	protected $account_holder_name;

    /**
    *@Column(type="integer",nullable=false)
    ****/
    protected $account_refer_id;

	/**
	*@Column(type="integer",nullable=false)
	****/
	protected $account_refer_type;

    


    /**
     * Gets the value of account_id.
     *
     * @return mixed
     */
    public function getAccountId()
    {
        return $this->account_id;
    }

    /**
     * Sets the value of account_id.
     *
     * @param mixed $account_id the account id
     *
     * @return self
     */
    public function setAccountId($account_id)
    {
        $this->account_id = $account_id;

        return $this;
    }

    /**
     * Gets the value of account_no.
     *
     * @return mixed
     */
    public function getAccountNo()
    {
        return $this->account_no;
    }

    /**
     * Sets the value of account_no.
     *
     * @param mixed $account_no the account no
     *
     * @return self
     */
    public function setAccountNo($account_no)
    {
        $this->account_no = $account_no;

        return $this;
    }

    /**
     * Gets the value of account_type	.
     *
     * @return mixed
     */
    public function getAccountType()
    {
        return $this->account_type	;
    }

    /**
     * Sets the value of account_type	.
     *
     * @param mixed $account_type	 the account type	
     *
     * @return self
     */
    public function setAccountType($account_type	)
    {
        $this->account_type	 = $account_type	;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getBankId()
    {
        return $this->bank_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $bank_id the bank id
     *
     * @return self
     */
    public function setBankId($bank_id)
    {
        $this->bank_id = $bank_id;

        return $this;
    }

    /**
     * Gets the value of account_holder_name.
     *
     * @return mixed
     */
    public function getAccountHolderName()
    {
        return $this->account_holder_name;
    }

    /**
     * Sets the value of account_holder_name.
     *
     * @param mixed $account_holder_name the account holder name
     *
     * @return self
     */
    public function setAccountHolderName($account_holder_name)
    {
        $this->account_holder_name = $account_holder_name;

        return $this;
    }

    /**
     * Gets the value of account_refer_id.
     *
     * @return mixed
     */
    public function getAccountReferId()
    {
        return $this->account_refer_id;
    }

    /**
     * Sets the value of account_refer_id.
     *
     * @param mixed $account_refer_id the account refer id
     *
     * @return self
     */
    public function setAccountReferId($account_refer_id)
    {
        $this->account_refer_id = $account_refer_id;

        return $this;
    }

    /**
     * Gets the value of account_refer_type.
     *
     * @return mixed
     */
    public function getAccountReferType()
    {
        return $this->account_refer_type;
    }

    /**
     * Sets the value of account_refer_type.
     *
     * @param mixed $account_refer_type the account refer type
     *
     * @return self
     */
    public function setAccountReferType($account_refer_type)
    {
        $this->account_refer_type = $account_refer_type;

        return $this;
    }

   
}
?>