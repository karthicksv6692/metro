<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Pins")
*/
class Pins{

	/**
	*
	* @Id
	* @Column(name="pin_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $pin_id;

	/**
	*@Column(type="string",length=255,nullable=false)
	****/
	protected $pin_no;


	/**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\User", fetch="EAGER")
     * @JoinColumn(name="pin_requested_user_id", referencedColumnName="user_id",onDelete="cascade")
     */
    private $pin_requested_user_id;

  /**
  *@Column(type="integer",nullable=true)
  ****/
  protected $pin_used_user_id;
	/**
	*@Column(type="integer",nullable=true)
	****/
	protected $pin_payment_type=0;


  /**
  *@Column(type="integer",nullable=true)
  ****/
  protected $pin_transfer_user_id;

	/**
	*@Column(type="integer",nullable=false)
	****/
	protected $pin_status;

	/**
	*@Column(type="datetime",nullable=true)
	****/
	protected $pin_validity_time;


	/**
	*@Column(type="string",length=255,nullable=true)
	****/
	protected $pin_remarks;


  /**
  *@Column(type="datetime",nullable=true)
  ****/
  protected $pin_used_time;
	/**
	*@Column(type="datetime",nullable=true)
	****/
	protected $pin_transfer_date;


    /**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Packages", fetch="EAGER")
     * @JoinColumn(name="pin_request_package_id", referencedColumnName="package_id")
     */
    private $pin_request_package_id;

    /**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Package_products", fetch="EAGER")
     * @JoinColumn(name="pin_request_package_product_id", referencedColumnName="package_product_id")
     */
    private $pin_request_package_product_id;


    /**
    *@Column(type="integer",nullable=false)
    ****/
    protected $pin_request_for;
    /**
    *@Column(type="integer",options={"default" : 0})
    ****/
    protected $repurchase_type; 

	/**
     * Gets the value of pin_id.
     *
     * @return mixed
     */
    
    /**
     * @var datetime $created_at
     *
     * @Column(type="datetime")
     */
    protected $created_at;

    /**
     * @var datetime $updated_at
     * 
     * @Column(type="datetime", nullable = true)
     */
    protected $updated_at;

    public function getPinId()
    {
        return $this->pin_id;
    }

    /**
     * Sets the value of pin_id.
     *
     * @param mixed $pin_id the pin id
     *
     * @return self
     */
    public function setPinId($pin_id)
    {
        $this->pin_id = $pin_id;

        return $this;
    }

    /**
     * Gets the value of pin_no.
     *
     * @return mixed
     */
    public function getPinNo()
    {
        return $this->pin_no;
    }

    /**
     * Sets the value of pin_no.
     *
     * @param mixed $pin_no the pin no
     *
     * @return self
     */
    public function setPinNo($pin_no)
    {
        $this->pin_no = $pin_no;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getPinRequestedUserId()
    {
        return $this->pin_requested_user_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $pin_requested_user_id the pin requested user id
     *
     * @return self
     */
    public function setPinRequestedUserId($pin_requested_user_id)
    {
        $this->pin_requested_user_id = $pin_requested_user_id;

        return $this;
    }

    /**
     * Gets the value of pin_used_user_id.
     *
     * @return mixed
     */
    public function getPinUsedUserId()
    {
        return $this->pin_used_user_id;
    }

    /**
     * Sets the value of pin_used_user_id.
     *
     * @param mixed $pin_used_user_id the pin used user id
     *
     * @return self
     */
    public function setPinUsedUserId($pin_used_user_id)
    {
        $this->pin_used_user_id = $pin_used_user_id;

        return $this;
    }

    /**
     * Gets the value of pin_status.
     *
     * @return mixed
     */
    public function getPinStatus()
    {
        return $this->pin_status;
    }

    /**
     * Sets the value of pin_status.
     *
     * @param mixed $pin_status the pin status
     *
     * @return self
     */
    public function setPinStatus($pin_status)
    {
        $this->pin_status = $pin_status;

        return $this;
    }

    /**
     * Gets the value of pin_validity_time.
     *
     * @return mixed
     */
    public function getPinValidityTime()
    {
        return $this->pin_validity_time;
    }

    /**
     * Sets the value of pin_validity_time.
     *
     * @param mixed $pin_validity_time the pin validity time
     *
     * @return self
     */
    public function setPinValidityTime($pin_validity_time)
    {
        $this->pin_validity_time = $pin_validity_time;

        return $this;
    }

    /**
     * Gets the value of pin_remarks.
     *
     * @return mixed
     */
    public function getPinRemarks()
    {
        return $this->pin_remarks;
    }

    /**
     * Sets the value of pin_remarks.
     *
     * @param mixed $pin_remarks the pin remarks
     *
     * @return self
     */
    public function setPinRemarks($pin_remarks)
    {
        $this->pin_remarks = $pin_remarks;

        return $this;
    }

    /**
     * Gets the value of pin_used_time.
     *
     * @return mixed
     */
    public function getPinUsedTime()
    {
        return $this->pin_used_time;
    }

    /**
     * Sets the value of pin_used_time.
     *
     * @param mixed $pin_used_time the pin used time
     *
     * @return self
     */
    public function setPinUsedTime($pin_used_time)
    {
        $this->pin_used_time = $pin_used_time;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getPinRequestPackageId()
    {
        return $this->pin_request_package_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $pin_request_package_id the pin request package id
     *
     * @return self
     */
    public function setPinRequestPackageId($pin_request_package_id)
    {
        $this->pin_request_package_id = $pin_request_package_id;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getPinRequestPackageProductId()
    {
        return $this->pin_request_package_product_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $pin_request_package_product_id the pin request package product id
     *
     * @return self
     */
    public function setPinRequestPackageProductId($pin_request_package_product_id)
    {
        $this->pin_request_package_product_id = $pin_request_package_product_id;

        return $this;
    }

    /**
     * Gets the value of pin_request_for.
     *
     * @return mixed
     */
    public function getPinRequestFor()
    {
        return $this->pin_request_for;
    }

    /**
     * Sets the value of pin_request_for.
     *
     * @param mixed $pin_request_for the pin request for
     *
     * @return self
     */
    public function setPinRequestFor($pin_request_for)
    {
        $this->pin_request_for = $pin_request_for;

        return $this;
    }
    /**
     * Gets triggered only on insert

     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_at = new \DateTime("now");
    }


    public function getCreatedAt()
    {
        return $this->created_at;
    }



    /**
     * Gets triggered every time on update

     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_at = new \DateTime("now");
    }

    /**
     * @return mixed
     */
    public function getPinTransferUserId()
    {
        return $this->pin_transfer_user_id;
    }

    /**
     * @param mixed $pin_transfer_user_id
     *
     * @return self
     */
    public function setPinTransferUserId($pin_transfer_user_id)
    {
        $this->pin_transfer_user_id = $pin_transfer_user_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPinPaymentType()
    {
        return $this->pin_payment_type;
    }

    /**
     * @param mixed $pin_payment_type
     *
     * @return self
     */
    public function setPinPaymentType($pin_payment_type)
    {
        $this->pin_payment_type = $pin_payment_type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPinTransferDate()
    {
        return $this->pin_transfer_date;
    }

    /**
     * @param mixed $pin_transfer_date
     *
     * @return self
     */
    public function setPinTransferDate()
    {
        $this->pin_transfer_date = new \DateTime("now");

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRepurchaseType()
    {
        return $this->repurchase_type;
    }

    /**
     * @param mixed $repurchase_type
     *
     * @return self
     */
    public function setRepurchaseType($repurchase_type)
    {
        $this->repurchase_type = $repurchase_type;

        return $this;
    }
}

?>