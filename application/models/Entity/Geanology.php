<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
class Geanology{
	
    protected $purchase_id;
    protected $user_id;
    protected $user_name;
    protected $first_name;
    protected $image;
    protected $date;

    protected $package_id;
    protected $package_name;
    protected $package_price;
    protected $root_id;

    protected $h_left;
    protected $h_right;
    protected $payout_released;

    protected $left_obj;
    protected $right_obj;

    protected $created_at;
    protected $table_obj;

    protected $pair_achieve_id;
    protected $pair_achieve_side;

    protected $pin_type;

    /**
     * @return mixed
     */
    public function getPurchaseId()
    {
        return $this->purchase_id;
    }

    /**
     * @param mixed $purchase_id
     *
     * @return self
     */
    public function setPurchaseId($purchase_id)
    {
        $this->purchase_id = $purchase_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     *
     * @return self
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->user_name;
    }

    /**
     * @param mixed $user_name
     *
     * @return self
     */
    public function setUserName($user_name)
    {
        $this->user_name = $user_name;

        return $this;
    }

     /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param mixed $first_name
     *
     * @return self
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;

        return $this;
    }

    

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     *
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     *
     * @return self
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    

    /**
     * @return mixed
     */
    public function getLeftObj()
    {
        return $this->left_obj;
    }

    /**
     * @param mixed $left_obj
     *
     * @return self
     */
    public function setLeftObj($left_obj)
    {
        $this->left_obj = $left_obj;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRightObj()
    {
        return $this->right_obj;
    }

    /**
     * @param mixed $right_obj
     *
     * @return self
     */
    public function setRightObj($right_obj)
    {
        $this->right_obj = $right_obj;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPackageId()
    {
        return $this->package_id;
    }

    /**
     * @param mixed $package_id
     *
     * @return self
     */
    public function setPackageId($package_id)
    {
        $this->package_id = $package_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPackageName()
    {
        return $this->package_name;
    }

    /**
     * @param mixed $package_name
     *
     * @return self
     */
    public function setPackageName($package_name)
    {
        $this->package_name = $package_name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPackagePrice()
    {
        return $this->package_price;
    }

    /**
     * @param mixed $package_price
     *
     * @return self
     */
    public function setPackagePrice($package_price)
    {
        $this->package_price = $package_price;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRootId()
    {
        return $this->root_id;
    }

    /**
     * @param mixed $root_id
     *
     * @return self
     */
    public function setRootId($root_id)
    {
        $this->root_id = $root_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHLeft()
    {
        return $this->h_left;
    }

    /**
     * @param mixed $h_left
     *
     * @return self
     */
    public function setHLeft($h_left)
    {
        $this->h_left = $h_left;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHRight()
    {
        return $this->h_right;
    }

    /**
     * @param mixed $h_right
     *
     * @return self
     */
    public function setHRight($h_right)
    {
        $this->h_right = $h_right;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTableObj()
    {
        return $this->table_obj;
    }

    /**
     * @param mixed $table_obj
     *
     * @return self
     */
    public function setTableObj($table_obj)
    {
        $this->table_obj = $table_obj;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPayoutReleased()
    {
        return $this->payout_released;
    }

    /**
     * @param mixed $payout_released
     *
     * @return self
     */
    public function setPayoutReleased($payout_released)
    {
        $this->payout_released = $payout_released;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     *
     * @return self
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPairAchieveId()
    {
        return $this->pair_achieve_id;
    }

    /**
     * @param mixed $pair_achieve_id
     *
     * @return self
     */
    public function setPairAchieveId($pair_achieve_id)
    {
        $this->pair_achieve_id = $pair_achieve_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPairAchieveSide()
    {
        return $this->pair_achieve_side;
    }

    /**
     * @param mixed $pair_achieve_side
     *
     * @return self
     */
    public function setPairAchieveSide($pair_achieve_side)
    {
        $this->pair_achieve_side = $pair_achieve_side;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPinType()
    {
        return $this->pin_type;
    }

    /**
     * @param mixed $pin_type
     *
     * @return self
     */
    public function setPinType($pin_type)
    {
        $this->pin_type = $pin_type;

        return $this;
    }
}

?>