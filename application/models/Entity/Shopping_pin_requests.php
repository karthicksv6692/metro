<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Shopping_pin_requests")
*/
class Shopping_pin_requests{

  /**
  *
  * @Id
  * @Column(name="request_id",type="integer",nullable=false)
  * @GeneratedValue(strategy="AUTO")
  */
  protected $request_id;

  /**
   * Many Features have One Product.
   * @ManyToOne(targetEntity="Entity\User", fetch="EAGER")
   * @JoinColumn(name="request_user_id", referencedColumnName="user_id")
   */
  protected $request_user_id;

   /**
   * Many Features have One Product.
   * @ManyToOne(targetEntity="Entity\User", fetch="EAGER")
   * @JoinColumn(name="requested_to", referencedColumnName="user_id")
   */
  protected $requested_to;
  /**
   * Many Features have One Product.
   * @ManyToOne(targetEntity="Entity\Packages", fetch="EAGER")
   * @JoinColumn(name="request_package_id", referencedColumnName="package_id")
   */
  protected $request_package_id;

  /**
  *@Column(type="string",length=2000)
  ****/
  protected $product_details;

  /**
  *@Column(type="integer",nullable = true)
  ****/
  protected $request_type;

  /**
  *@Column(type="integer",nullable = true)
  ****/
  protected $no_of_pin;
  /**
  *@Column(type="integer",nullable = true)
  ****/
  protected $payment_mode;
  /**
  *@Column(type="integer",nullable = true)
  ****/
  protected $request_status;

  /**
  *@Column(type="string",length=200,nullable = true)
  ****/
  protected $notes;
  /**
   * @var datetime $created_at
   * @Column(type="datetime")
   */
  protected $created_at;
  /**
   * @var datetime $updated_at
   * @Column(type="datetime", nullable = true)
   */
  protected $updated_at;
  /**
   * @return mixed
   */
  public function getRequestId()
  {
      return $this->request_id;
  }

  /**
   * @param mixed $request_id
   *
   * @return self
   */
  public function setRequestId($request_id)
  {
      $this->request_id = $request_id;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getRequestUserId()
  {
      return $this->request_user_id;
  }

  /**
   * @param mixed $request_user_id
   *
   * @return self
   */
  public function setRequestUserId($request_user_id)
  {
      $this->request_user_id = $request_user_id;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getRequestPackageId()
  {
      return $this->request_package_id;
  }

  /**
   * @param mixed $request_package_id
   *
   * @return self
   */
  public function setRequestPackageId($request_package_id)
  {
      $this->request_package_id = $request_package_id;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getRequestType()
  {
      return $this->request_type;
  }

  /**
   * @param mixed $request_type
   *
   * @return self
   */
  public function setRequestType($request_type)
  {
      $this->request_type = $request_type;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getNoOfPin()
  {
      return $this->no_of_pin;
  }

  /**
   * @param mixed $no_of_pin
   *
   * @return self
   */
  public function setNoOfPin($no_of_pin)
  {
      $this->no_of_pin = $no_of_pin;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getRequestStatus()
  {
      return $this->request_status;
  }

  /**
   * @param mixed $request_status
   *
   * @return self
   */
  public function setRequestStatus($request_status)
  {
      $this->request_status = $request_status;

      return $this;
  }
  /**
   * @return mixed
   */
  public function getProductDetails()
  {
      return $this->product_details;
  }

  /**
   * @param mixed $product_details
   *
   * @return self
   */
  public function setProductDetails($product_details)
  {
      $this->product_details = $product_details;

      return $this;
  }

  /**
   * @param mixed $payment_mode
   *
   * @return self
   */
  public function setPaymentMode($payment_mode)
  {
      $this->payment_mode = $payment_mode;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getNotes()
  {
      return $this->notes;
  }

  /**
   * @param mixed $notes
   *
   * @return self
   */
  public function setNotes($notes)
  {
      $this->notes = $notes;

      return $this;
  }
  /**
   * Gets triggered only on insert

   * @ORM\PrePersist
   */
  public function onPrePersist()
  {
      $this->created_at = new \DateTime('now');
  }
  public function getCreatedAt()
  {
      return $this->created_at;
  }
  /**
   * Gets triggered every time on update
   * @ORM\PreUpdate
   */
  public function onPreUpdate()
  {
      $this->updated_at = new \DateTime("now");
  }
  public function getUpdatedAt()
  {
      return $this->updated_at;
  }

    /**
     * @return mixed
     */
    public function getRequestedTo()
    {
        return $this->requested_to;
    }

    /**
     * @param mixed $requested_to
     *
     * @return self
     */
    public function setRequestedTo($requested_to)
    {
        $this->requested_to = $requested_to;

        return $this;
    }
}
?>