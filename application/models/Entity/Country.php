<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* Country Model
* @Entity
* @Table(name="Country")
* 
*/
class Country{

	/**
	*
	* @Id
	* @Column(name="country_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $country_id;
    
    /**
    *@Column(type="string",length=255,nullable=false, unique=true)
    ****/
    protected $country_name;	
	/**
	*@Column(type="string", unique=true)
	****/
	protected $country_code;

	/**
     *
     * @ORM OneToMany(targetEntity="Entity\Pincode" mappedBy="pincode_country_id" )
     */
	protected $c_code;

    // public function __construct()
    // {
    //     //$this->contact = new ArrayCollection;
    // }
    public function getCountryId()
    {
        return $this->country_id;
    }
    public function setCountryName($country_name)
    {
        $this->country_name = $country_name;
        return $this;
    }
    public function getCountryName()
    {
        return $this->country_name;
    }
    public function setCountryCode($country_code)
    {
        $this->country_code = $country_code;
        return $this;
    }
    public function getCountryCode()
    {
        return $this->country_code;
    }
    
    public function add_Country(\Entity\Country $Country)
    {
        $this->Country[] = $Country;
        return $this;
    }
    public function getCountry()
    {
        return $this->Country;
    }

    /**
     * Sets the value of country_id.
     *
     * @param mixed $country_id the country id
     *
     * @return self
     */
    public function setCountryId($country_id)
    {
        $this->country_id = $country_id;

        return $this;
    }

    /**
     * Gets the value of c_code.
     *
     * @return mixed
     */
    public function getCCode()
    {
        return $this->c_code;
    }

    /**
     * Sets the value of c_code.
     *
     * @param mixed $c_code the c code
     *
     * @return self
     */
    public function setCCode($c_code)
    {
        $this->c_code = $c_code;

        return $this;
    }
}



 ?>