<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Setting")
*/
class Setting{

	/**
	*
	* @Id
	* @Column(name="setting_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $setting_id;
	/**
	*@Column(type="datetime")
	****/
	protected $pin_default_validity_time;

	/**
	*@Column(type="datetime")
	****/
	protected $account_inactivity_time;


	/**
	*@Column(type="integer")
	****/
	protected $default_access;

    /**
     * @return mixed
     */
    public function getSettingId()
    {
        return $this->setting_id;
    }

    /**
     * @param mixed $setting_id
     *
     * @return self
     */
    public function setSettingId($setting_id)
    {
        $this->setting_id = $setting_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPinDefaultValidityTime()
    {
        return $this->pin_default_validity_time;
    }

    /**
     * @param mixed $pin_default_validity_time
     *
     * @return self
     */
    public function setPinDefaultValidityTime($pin_default_validity_time)
    {
        $this->pin_default_validity_time = $pin_default_validity_time;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAccountInactivityTime()
    {
        return $this->account_inactivity_time;
    }

    /**
     * @param mixed $account_inactivity_time
     *
     * @return self
     */
    public function setAccountInactivityTime($account_inactivity_time)
    {
        $this->account_inactivity_time = $account_inactivity_time;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDefaultAccess()
    {
        return $this->default_access;
    }

    /**
     * @param mixed $default_access
     *
     * @return self
     */
    public function setDefaultAccess($default_access)
    {
        $this->default_access = $default_access;

        return $this;
    }
}