<?php 
namespace Entity;


class User_sview{


    protected $user_id;
    protected $is_active;
    protected $user_name;
    protected $first_name;
    protected $last_name;

    protected $address_id;
    protected $contact_id;
    protected $password_id;

    protected $referal_id;
    protected $sponcer_name;
    protected $placement_id;
    protected $placement_name;
    protected $position;


    protected $pin_no;
    protected $package_name;
    protected $products;
    protected $products_count;



    protected $uu_first_name;
    protected $dob;
    protected $gender;
    protected $phone_no;
    protected $email;


    protected $nominee_name;
    protected $relationship;
    protected $n_dob;
    protected $n_gender;
    protected $n_phone_no;


    protected $full_address;
    protected $pincode;
    protected $district;
    protected $state;
    protected $country;
    protected $country_name;


    protected $account_holder_name;
    protected $account_number;
    protected $account_type;

    protected $ifsc;
    protected $bank_name;
    protected $bank_branch;


    protected $aadhar_no;
    protected $pan_no;
    protected $pass_no;

    protected $aadhar_front;
    protected $aadhar_back;
    protected $pan_front;
    protected $pan_back;
    protected $pass_front;
    protected $pass_back;


    protected $secondary_phone;
    protected $country_id;
    protected $role_id;
    protected $role_name;
    protected $access_ids;

    protected $kyc_status;
    protected $pan_verified;
    protected $aadhar_verified;
    protected $pass_verified;

    protected $password;
    protected $pin_store_password;





  

    /**
     * Gets the value of referal_id.
     *
     * @return mixed
     */
    public function getReferalId()
    {
        return $this->referal_id;
    }

    /**
     * Sets the value of referal_id.
     *
     * @param mixed $referal_id the referal id
     *
     * @return self
     */
   public function setReferalId($referal_id)
    {
        $this->referal_id = $referal_id;

        return $this;
    }

    /**
     * Gets the value of is_active.
     *
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * Sets the value of is_active.
     *
     * @param mixed $is_active the is active
     *
     * @return self
     */
    public function setIsActive($is_active)
    {
        $this->is_active = $is_active;

        return $this;
    }

    /**
     * Gets the value of sponcer_name.
     *
     * @return mixed
     */
    public function getSponcerName()
    {
        return $this->sponcer_name;
    }

    /**
     * Sets the value of sponcer_name.
     *
     * @param mixed $sponcer_name the sponcer name
     *
     * @return self
     */
   public function setSponcerName($sponcer_name)
    {
        $this->sponcer_name = $sponcer_name;

        return $this;
    }

    /**
     * Gets the value of placement_id.
     *
     * @return mixed
     */
    public function getPlacementId()
    {
        return $this->placement_id;
    }

    /**
     * Sets the value of placement_id.
     *
     * @param mixed $placement_id the placement id
     *
     * @return self
     */
   public function setPlacementId($placement_id)
    {
        $this->placement_id = $placement_id;

        return $this;
    }

    /**
     * Gets the value of placement_name.
     *
     * @return mixed
     */
    public function getPlacementName()
    {
        return $this->placement_name;
    }

    /**
     * Sets the value of placement_name.
     *
     * @param mixed $placement_name the placement name
     *
     * @return self
     */
   public function setPlacementName($placement_name)
    {
        $this->placement_name = $placement_name;

        return $this;
    }

    /**
     * Gets the value of position.
     *
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Sets the value of position.
     *
     * @param mixed $position the position
     *
     * @return self
     */
   public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

   
    /**
     * Gets the value of pin_no.
     *
     * @return mixed
     */
    public function getPinNo()
    {
        return $this->pin_no;
    }

    /**
     * Sets the value of pin_no.
     *
     * @param mixed $pin_no the pin no
     *
     * @return self
     */
   public function setPinNo($pin_no)
    {
        $this->pin_no = $pin_no;

        return $this;
    }

    /**
     * Gets the value of package_name.
     *
     * @return mixed
     */
    public function getPackageName()
    {
        return $this->package_name;
    }

    /**
     * Sets the value of package_name.
     *
     * @param mixed $package_name the package name
     *
     * @return self
     */
   public function setPackageName($package_name)
    {
        $this->package_name = $package_name;

        return $this;
    }

    /**
     * Gets the value of products.
     *
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Sets the value of products.
     *
     * @param mixed $products the products
     *
     * @return self
     */
   public function setProducts($products)
    {
        $this->products = $products;

        return $this;
    }

    /**
     * Gets the value of products_count.
     *
     * @return mixed
     */
    public function getProductsCount()
    {
        return $this->products_count;
    }

    /**
     * Sets the value of products_count.
     *
     * @param mixed $products_count the products count
     *
     * @return self
     */
   public function setProductsCount($products_count)
    {
        $this->products_count = $products_count;

        return $this;
    }

    /**
     * Gets the value of uu_first_name.
     *
     * @return mixed
     */
    public function getUuFirstName()
    {
        return $this->uu_first_name;
    }

    /**
     * Sets the value of uu_first_name.
     *
     * @param mixed $uu_first_name the uu first name
     *
     * @return self
     */
   public function setUuFirstName($uu_first_name)
    {
        $this->uu_first_name = $uu_first_name;

        return $this;
    }

    /**
     * Gets the value of dob.
     *
     * @return mixed
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * Sets the value of dob.
     *
     * @param mixed $dob the dob
     *
     * @return self
     */
   public function setDob($dob)
    {
        $this->dob = $dob;

        return $this;
    }

    /**
     * Gets the value of gender.
     *
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Sets the value of gender.
     *
     * @param mixed $gender the gender
     *
     * @return self
     */
   public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Gets the value of phone_no.
     *
     * @return mixed
     */
    public function getPhoneNo()
    {
        return $this->phone_no;
    }

    /**
     * Sets the value of phone_no.
     *
     * @param mixed $phone_no the phone no
     *
     * @return self
     */
   public function setPhoneNo($phone_no)
    {
        $this->phone_no = $phone_no;

        return $this;
    }

    /**
     * Gets the value of email.
     *
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the value of email.
     *
     * @param mixed $email the email
     *
     * @return self
     */
   public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Gets the value of nominee_name.
     *
     * @return mixed
     */
    public function getNomineeName()
    {
        return $this->nominee_name;
    }

    /**
     * Sets the value of nominee_name.
     *
     * @param mixed $nominee_name the nominee name
     *
     * @return self
     */
   public function setNomineeName($nominee_name)
    {
        $this->nominee_name = $nominee_name;

        return $this;
    }

    /**
     * Gets the value of relationship.
     *
     * @return mixed
     */
    public function getRelationship()
    {
        return $this->relationship;
    }

    /**
     * Sets the value of relationship.
     *
     * @param mixed $relationship the relationship
     *
     * @return self
     */
   public function setRelationship($relationship)
    {
        $this->relationship = $relationship;

        return $this;
    }

    /**
     * Gets the value of n_dob.
     *
     * @return mixed
     */
    public function getNDob()
    {
        return $this->n_dob;
    }

    /**
     * Sets the value of n_dob.
     *
     * @param mixed $n_dob the n dob
     *
     * @return self
     */
   public function setNDob($n_dob)
    {
        $this->n_dob = $n_dob;

        return $this;
    }

    /**
     * Gets the value of n_gender.
     *
     * @return mixed
     */
    public function getNGender()
    {
        return $this->n_gender;
    }

    /**
     * Sets the value of n_gender.
     *
     * @param mixed $n_gender the n gender
     *
     * @return self
     */
   public function setNGender($n_gender)
    {
        $this->n_gender = $n_gender;

        return $this;
    }

    /**
     * Gets the value of n_phone_no.
     *
     * @return mixed
     */
    public function getNPhoneNo()
    {
        return $this->n_phone_no;
    }

    /**
     * Sets the value of n_phone_no.
     *
     * @param mixed $n_phone_no the n phone no
     *
     * @return self
     */
   public function setNPhoneNo($n_phone_no)
    {
        $this->n_phone_no = $n_phone_no;

        return $this;
    }

    /**
     * Gets the value of full_address.
     *
     * @return mixed
     */
    public function getFullAddress()
    {
        return $this->full_address;
    }

    /**
     * Sets the value of full_address.
     *
     * @param mixed $full_address the full address
     *
     * @return self
     */
   public function setFullAddress($full_address)
    {
        $this->full_address = $full_address;

        return $this;
    }

    /**
     * Gets the value of pincode.
     *
     * @return mixed
     */
    public function getPincode()
    {
        return $this->pincode;
    }

    /**
     * Sets the value of pincode.
     *
     * @param mixed $pincode the pincode
     *
     * @return self
     */
   public function setPincode($pincode)
    {
        $this->pincode = $pincode;

        return $this;
    }

    /**
     * Gets the value of district.
     *
     * @return mixed
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * Sets the value of district.
     *
     * @param mixed $district the district
     *
     * @return self
     */
   public function setDistrict($district)
    {
        $this->district = $district;

        return $this;
    }

    /**
     * Gets the value of state.
     *
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Sets the value of state.
     *
     * @param mixed $state the state
     *
     * @return self
     */
   public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Gets the value of country.
     *
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Sets the value of country.
     *
     * @param mixed $country the country
     *
     * @return self
     */
   public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Gets the value of account_holder_name.
     *
     * @return mixed
     */
    public function getAccountHolderName()
    {
        return $this->account_holder_name;
    }

    /**
     * Sets the value of account_holder_name.
     *
     * @param mixed $account_holder_name the account holder name
     *
     * @return self
     */
   public function setAccountHolderName($account_holder_name)
    {
        $this->account_holder_name = $account_holder_name;

        return $this;
    }

    /**
     * Gets the value of account_number.
     *
     * @return mixed
     */
    public function getAccountNumber()
    {
        return $this->account_number;
    }

    /**
     * Sets the value of account_number.
     *
     * @param mixed $account_number the account number
     *
     * @return self
     */
   public function setAccountNumber($account_number)
    {
        $this->account_number = $account_number;

        return $this;
    }

    /**
     * Gets the value of account_type.
     *
     * @return mixed
     */
    public function getAccountType()
    {
        return $this->account_type;
    }

    /**
     * Sets the value of account_type.
     *
     * @param mixed $account_type the account type
     *
     * @return self
     */
   public function setAccountType($account_type)
    {
        $this->account_type = $account_type;

        return $this;
    }

    /**
     * Gets the value of ifsc.
     *
     * @return mixed
     */
    public function getIfsc()
    {
        return $this->ifsc;
    }

    /**
     * Sets the value of ifsc.
     *
     * @param mixed $ifsc the ifsc
     *
     * @return self
     */
   public function setIfsc($ifsc)
    {
        $this->ifsc = $ifsc;

        return $this;
    }

    /**
     * Gets the value of bank_name.
     *
     * @return mixed
     */
    public function getBankName()
    {
        return $this->bank_name;
    }

    /**
     * Sets the value of bank_name.
     *
     * @param mixed $bank_name the bank name
     *
     * @return self
     */
   public function setBankName($bank_name)
    {
        $this->bank_name = $bank_name;

        return $this;
    }

    /**
     * Gets the value of bank_branch.
     *
     * @return mixed
     */
    public function getBankBranch()
    {
        return $this->bank_branch;
    }

    /**
     * Sets the value of bank_branch.
     *
     * @param mixed $bank_branch the bank branch
     *
     * @return self
     */
   public function setBankBranch($bank_branch)
    {
        $this->bank_branch = $bank_branch;

        return $this;
    }

    /**
     * Gets the value of aadhar_no.
     *
     * @return mixed
     */
    public function getAadharNo()
    {
        return $this->aadhar_no;
    }

    /**
     * Sets the value of aadhar_no.
     *
     * @param mixed $aadhar_no the aadhar no
     *
     * @return self
     */
   public function setAadharNo($aadhar_no)
    {
        $this->aadhar_no = $aadhar_no;

        return $this;
    }

    /**
     * Gets the value of pan_no.
     *
     * @return mixed
     */
    public function getPanNo()
    {
        return $this->pan_no;
    }

    /**
     * Sets the value of pan_no.
     *
     * @param mixed $pan_no the pan no
     *
     * @return self
     */
   public function setPanNo($pan_no)
    {
        $this->pan_no = $pan_no;

        return $this;
    }

    /**
     * Gets the value of pass_no.
     *
     * @return mixed
     */
    public function getPassNo()
    {
        return $this->pass_no;
    }

    /**
     * Sets the value of pass_no.
     *
     * @param mixed $pass_no the pass no
     *
     * @return self
     */
   public function setPassNo($pass_no)
    {
        $this->pass_no = $pass_no;

        return $this;
    }

    /**
     * Gets the value of aadhar_front.
     *
     * @return mixed
     */
    public function getAadharFront()
    {
        return $this->aadhar_front;
    }

    /**
     * Sets the value of aadhar_front.
     *
     * @param mixed $aadhar_front the aadhar front
     *
     * @return self
     */
   public function setAadharFront($aadhar_front)
    {
        $this->aadhar_front = $aadhar_front;

        return $this;
    }

    /**
     * Gets the value of aadhar_back.
     *
     * @return mixed
     */
    public function getAadharBack()
    {
        return $this->aadhar_back;
    }

    /**
     * Sets the value of aadhar_back.
     *
     * @param mixed $aadhar_back the aadhar back
     *
     * @return self
     */
   public function setAadharBack($aadhar_back)
    {
        $this->aadhar_back = $aadhar_back;

        return $this;
    }

    /**
     * Gets the value of pan_front.
     *
     * @return mixed
     */
    public function getPanFront()
    {
        return $this->pan_front;
    }

    /**
     * Sets the value of pan_front.
     *
     * @param mixed $pan_front the pan front
     *
     * @return self
     */
   public function setPanFront($pan_front)
    {
        $this->pan_front = $pan_front;

        return $this;
    }

    /**
     * Gets the value of pan_back.
     *
     * @return mixed
     */
    public function getPanBack()
    {
        return $this->pan_back;
    }

    /**
     * Sets the value of pan_back.
     *
     * @param mixed $pan_back the pan back
     *
     * @return self
     */
   public function setPanBack($pan_back)
    {
        $this->pan_back = $pan_back;

        return $this;
    }

    /**
     * Gets the value of pass_front.
     *
     * @return mixed
     */
    public function getPassFront()
    {
        return $this->pass_front;
    }

    /**
     * Sets the value of pass_front.
     *
     * @param mixed $pass_front the pass front
     *
     * @return self
     */
   public function setPassFront($pass_front)
    {
        $this->pass_front = $pass_front;

        return $this;
    }

    /**
     * Gets the value of pass_back.
     *
     * @return mixed
     */
    public function getPassBack()
    {
        return $this->pass_back;
    }

    /**
     * Sets the value of pass_back.
     *
     * @param mixed $pass_back the pass back
     *
     * @return self
     */
   public function setPassBack($pass_back)
    {
        $this->pass_back = $pass_back;

        return $this;
    }

    /**
     * Gets the value of secondary_phone.
     *
     * @return mixed
     */
    public function getSecondaryPhone()
    {
        return $this->secondary_phone;
    }

    /**
     * Sets the value of secondary_phone.
     *
     * @param mixed $secondary_phone the secondary phone
     *
     * @return self
     */
    public function setSecondaryPhone($secondary_phone)
    {
        $this->secondary_phone = $secondary_phone;

        return $this;
    }

    /**
     * Gets the value of country_id.
     *
     * @return mixed
     */
    public function getCountryId()
    {
        return $this->country_id;
    }

    /**
     * Sets the value of country_id.
     *
     * @param mixed $country_id the country id
     *
     * @return self
     */
    public function setCountryId($country_id)
    {
        $this->country_id = $country_id;

        return $this;
    }

    /**
     * Gets the value of role_id.
     *
     * @return mixed
     */
    public function getRoleId()
    {
        return $this->role_id;
    }

    /**
     * Sets the value of role_id.
     *
     * @param mixed $role_id the role id
     *
     * @return self
     */
    public function setRoleId($role_id)
    {
        $this->role_id = $role_id;

        return $this;
    }

    /**
     * Gets the value of access_ids.
     *
     * @return mixed
     */
    public function getAccessIds()
    {
        return $this->access_ids;
    }

    /**
     * Sets the value of access_ids.
     *
     * @param mixed $access_ids the access ids
     *
     * @return self
     */
    public function setAccessIds($access_ids)
    {
        $this->access_ids = $access_ids;

        return $this;
    }

    /**
     * Gets the value of user_id.
     *
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Sets the value of user_id.
     *
     * @param mixed $user_id the user id
     *
     * @return self
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Gets the value of user_name.
     *
     * @return mixed
     */
    public function getUserName()
    {
        return $this->user_name;
    }

    /**
     * Sets the value of user_name.
     *
     * @param mixed $user_name the user name
     *
     * @return self
     */
    public function setUserName($user_name)
    {
        $this->user_name = $user_name;

        return $this;
    }

    /**
     * Gets the value of last_name.
     *
     * @return mixed
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Sets the value of last_name.
     *
     * @param mixed $last_name the last name
     *
     * @return self
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;

        return $this;
    }

    /**
     * Gets the value of address_id.
     *
     * @return mixed
     */
    public function getAddressId()
    {
        return $this->address_id;
    }

    /**
     * Sets the value of address_id.
     *
     * @param mixed $address_id the address id
     *
     * @return self
     */
    public function setAddressId($address_id)
    {
        $this->address_id = $address_id;

        return $this;
    }

    /**
     * Gets the value of contact_id.
     *
     * @return mixed
     */
    public function getContactId()
    {
        return $this->contact_id;
    }

    /**
     * Sets the value of contact_id.
     *
     * @param mixed $contact_id the contact id
     *
     * @return self
     */
    public function setContactId($contact_id)
    {
        $this->contact_id = $contact_id;

        return $this;
    }

    /**
     * Gets the value of password_id.
     *
     * @return mixed
     */
    public function getPasswordId()
    {
        return $this->password_id;
    }

    /**
     * Sets the value of password_id.
     *
     * @param mixed $password_id the password id
     *
     * @return self
     */
    public function setPasswordId($password_id)
    {
        $this->password_id = $password_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountryName()
    {
        return $this->country_name;
    }

    /**
     * @param mixed $country_name
     *
     * @return self
     */
    public function setCountryName($country_name)
    {
        $this->country_name = $country_name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRoleName()
    {
        return $this->role_name;
    }

    /**
     * @param mixed $role_name
     *
     * @return self
     */
    public function setRoleName($role_name)
    {
        $this->role_name = $role_name;

        return $this;
    }
    /**
     * @return mixed
     */
    public function getKycStatus()
    {
        return $this->kyc_status;
    }

    /**
     * @param mixed $pin_store_password
     *
     * @return self
     */
    public function setKycStatus($kyc_status)
    {
        $this->kyc_status = $kyc_status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassVerified()
    {
        return $this->pass_verified;
    }

    /**
     * @param mixed $pass_verified
     *
     * @return self
     */
    public function setPassVerified($pass_verified)
    {
        $this->pass_verified = $pass_verified;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAadharVerified()
    {
        return $this->aadhar_verified;
    }

    /**
     * @param mixed $aadhar_verified
     *
     * @return self
     */
    public function setAadharVerified($aadhar_verified)
    {
        $this->aadhar_verified = $aadhar_verified;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPanVerified()
    {
        return $this->pan_verified;
    }

    /**
     * @param mixed $pan_verified
     *
     * @return self
     */
    public function setPanVerified($pan_verified)
    {
        $this->pan_verified = $pan_verified;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     *
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPinStorePassword()
    {
        return $this->pin_store_password;
    }

    /**
     * @param mixed $pin_store_password
     *
     * @return self
     */
    public function setPinStorePassword($pin_store_password)
    {
        $this->pin_store_password = $pin_store_password;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param mixed $first_name
     *
     * @return self
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;

        return $this;
    }
}
?>