<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
  class Sample_payout{
  
    protected $purchase_id;
    protected $user_name;
    protected $user_id;
    protected $package_id;
    protected $package_name;
    protected $package_price;
    protected $package_limited_pair_cutoff;
    protected $root_id;

    protected $pair_achieve;
    protected $pair_achieve_side;
    protected $pair_achieve_user_purchase_id;

    protected $payout_released;

    protected $pair_achieve_package_id;

    protected $created_at;

    protected $left_obj;
    protected $right_obj;

    

  
    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->user_name;
    }

    /**
     * @param mixed $user_name
     *
     * @return self
     */
    public function setUserName($user_name)
    {
        $this->user_name = $user_name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     *
     * @return self
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPackageName()
    {
        return $this->package_name;
    }

    /**
     * @param mixed $package_name
     *
     * @return self
     */
    public function setPackageName($package_name)
    {
        $this->package_name = $package_name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPackagePrice()
    {
        return $this->package_price;
    }

    /**
     * @param mixed $package_price
     *
     * @return self
     */
    public function setPackagePrice($package_price)
    {
        $this->package_price = $package_price;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLeftObj()
    {
        return $this->left_obj;
    }

    /**
     * @param mixed $left_obj
     *
     * @return self
     */
    public function setLeftObj($left_obj)
    {
        $this->left_obj = $left_obj;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRightObj()
    {
        return $this->right_obj;
    }

    /**
     * @param mixed $right_obj
     *
     * @return self
     */
    public function setRightObj($right_obj)
    {
        $this->right_obj = $right_obj;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPackageId()
    {
        return $this->package_id;
    }

    /**
     * @param mixed $package_id
     *
     * @return self
     */
    public function setPackageId($package_id)
    {
        $this->package_id = $package_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRootId()
    {
        return $this->root_id;
    }

    /**
     * @param mixed $root_id
     *
     * @return self
     */
    public function setRootId($root_id)
    {
        $this->root_id = $root_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPurchaseId()
    {
        return $this->purchase_id;
    }

    /**
     * @param mixed $purchase_id
     *
     * @return self
     */
    public function setPurchaseId($purchase_id)
    {
        $this->purchase_id = $purchase_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPairAchieve()
    {
        return $this->pair_achieve;
    }

    /**
     * @param mixed $pair_achieve
     *
     * @return self
     */
    public function setPairAchieve($pair_achieve)
    {
        $this->pair_achieve = $pair_achieve;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPairAchieveSide()
    {
        return $this->pair_achieve_side;
    }

    /**
     * @param mixed $pair_achieve_side
     *
     * @return self
     */
    public function setPairAchieveSide($pair_achieve_side)
    {
        $this->pair_achieve_side = $pair_achieve_side;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPairAchieveUserPurchaseId()
    {
        return $this->pair_achieve_user_purchase_id;
    }

    /**
     * @param mixed $pair_achieve_user_purchase_id
     *
     * @return self
     */
    public function setPairAchieveUserPurchaseId($pair_achieve_user_purchase_id)
    {
        $this->pair_achieve_user_purchase_id = $pair_achieve_user_purchase_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPairAchievePackageId()
    {
        return $this->pair_achieve_package_id;
    }

    /**
     * @param mixed $pair_achieve_package_id
     *
     * @return self
     */
    public function setPairAchievePackageId($pair_achieve_package_id)
    {
        $this->pair_achieve_package_id = $pair_achieve_package_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPackageLimitedPairCutoff()
    {
        return $this->package_limited_pair_cutoff;
    }

    /**
     * @param mixed $package_limited_pair_cutoff
     *
     * @return self
     */
    public function setPackageLimitedPairCutoff($package_limited_pair_cutoff)
    {
        $this->package_limited_pair_cutoff = $package_limited_pair_cutoff;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     *
     * @return self
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPayoutReleased()
    {
        return $this->payout_released;
    }

    /**
     * @param mixed $payout_released
     *
     * @return self
     */
    public function setPayoutReleased($payout_released)
    {
        $this->payout_released = $payout_released;

        return $this;
    }
}


?>