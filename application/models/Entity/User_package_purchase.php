<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="User_package_purchase")
*/
class User_package_purchase{

	/**
	*
	* @Id
	* @Column(name="user_purchase_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $user_purchase_id;

	/**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\User", fetch="EAGER")
     * @JoinColumn(name="user_id", referencedColumnName="user_id",onDelete="cascade")
     */
	protected $user_id;

	/**
    *@Column(type="integer")
    ****/
	protected $user_purchase_sponser_user_purchase_id;


   /**
    *@Column(type="integer")
    ****/
    protected $user_purchase_actual_gene_user_purchase_id;


  /**
  *@Column(type="integer")
  ****/
  protected $user_purchase_sponser_gene_side;
  
	/**
	*@Column(type="integer")
	****/
	protected $payout_ach;

	/**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Pins", fetch="EAGER")
     * @JoinColumn(name="user_purchase_pin_id", referencedColumnName="pin_id",onDelete="cascade")
     */
	protected $user_purchase_pin_id;

	/**
	*@Column(type="integer")
	****/
	protected $user_purchase_actual_gene_side;

    /**
    *@Column(type="integer")
    ****/
    protected $user_purchase_gene_left_purchase_id;
	/**
	*@Column(type="integer",nullable=true)
	****/
	protected $user_pruchase_pair_user_purchase_id;
	/**
	*@Column(type="integer")
	****/
	protected $user_purchase_gene_right_purchase_id;

	/**
	*@Column(type="integer")
	****/
	protected $user_purchase_money_alloted;

	/**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Transactions", fetch="EAGER")
     * @JoinColumn(name="user_purchase_transaction_id", referencedColumnName="transaction_id")
     */
	protected $user_purchase_transaction_id;

  /**
  *@Column(type="integer")
  ****/
  protected $pair_achieve;


  /**
  *@Column(type="integer")
  ****/
  protected $pair_achieve_user_purchase_id;

   /**
  *@Column(type="integer")
  ****/
  protected $pair_achieve_side;


   /**
  *@Column(type="integer")
  ****/
  protected $payout_released;
  
   /**
  *@Column(type="integer",nullable=true)
  ****/
  protected $rep_count;

   /**
  *@Column(type="integer",nullable=true)
  ****/
  protected $level_income_achieve = 0;

   

    /**
     * @var datetime $created_at
     *
     * @Column(type="datetime")
     */
    protected $created_at;

    /**
     * @var datetime $updated_at
     * 
     * @Column(type="datetime", nullable = true)
     */
    protected $updated_at;




    /**
     * Gets the value of user_purchase_id.
     *
     * @return mixed
     */
    public function getUserPurchaseId()
    {
        return $this->user_purchase_id;
    }

    /**
     * Sets the value of user_purchase_id.
     *
     * @param mixed $user_purchase_id the user purchase id
     *
     * @return self
     */
    public function setUserPurchaseId($user_purchase_id)
    {
        $this->user_purchase_id = $user_purchase_id;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $user_id the user id
     *
     * @return self
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getUserPurchaseSponserUserPurchaseId()
    {
        return $this->user_purchase_sponser_user_purchase_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $user_purchase_sponser_user_purchase_id the user purchase sponser user purchase id
     *
     * @return self
     */
    public function setUserPurchaseSponserUserPurchaseId($user_purchase_sponser_user_purchase_id)
    {
        $this->user_purchase_sponser_user_purchase_id = $user_purchase_sponser_user_purchase_id;

        return $this;
    }

    /**
     * Gets the value of user_purchase_sponser_gene_side.
     *
     * @return mixed
     */
    public function getUserPurchaseSponserGeneSide()
    {
        return $this->user_purchase_sponser_gene_side;
    }

    /**
     * Sets the value of user_purchase_sponser_gene_side.
     *
     * @param mixed $user_purchase_sponser_gene_side the user purchase sponser gene side
     *
     * @return self
     */
    public function setUserPurchaseSponserGeneSide($user_purchase_sponser_gene_side)
    {
        $this->user_purchase_sponser_gene_side = $user_purchase_sponser_gene_side;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getUserPurchasePinId()
    {
        return $this->user_purchase_pin_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $user_purchase_pin_id the user purchase pin id
     *
     * @return self
     */
    public function setUserPurchasePinId($user_purchase_pin_id)
    {
        $this->user_purchase_pin_id = $user_purchase_pin_id;

        return $this;
    }

    /**
     * Gets the value of user_purchase_actual_gene_side.
     *
     * @return mixed
     */
    public function getUserPurchaseActualGeneSide()
    {
        return $this->user_purchase_actual_gene_side;
    }

    /**
     * Sets the value of user_purchase_actual_gene_side.
     *
     * @param mixed $user_purchase_actual_gene_side the user purchase actual gene side
     *
     * @return self
     */
    public function setUserPurchaseActualGeneSide($user_purchase_actual_gene_side)
    {
        $this->user_purchase_actual_gene_side = $user_purchase_actual_gene_side;

        return $this;
    }

    /**
     * Gets the value of user_purchase_gene_left_purchase_id.
     *
     * @return mixed
     */
    public function getUserPurchaseGeneLeftPurchaseId()
    {
        return $this->user_purchase_gene_left_purchase_id;
    }

    /**
     * Sets the value of user_purchase_gene_left_purchase_id.
     *
     * @param mixed $user_purchase_gene_left_purchase_id the user purchase gene left purchase id
     *
     * @return self
     */
    public function setUserPurchaseGeneLeftPurchaseId($user_purchase_gene_left_purchase_id)
    {
        $this->user_purchase_gene_left_purchase_id = $user_purchase_gene_left_purchase_id;

        return $this;
    }

    /**
     * Gets the value of user_purchase_gene_right_purchase_id.
     *
     * @return mixed
     */
    public function getUserPurchaseGeneRightPurchaseId()
    {
        return $this->user_purchase_gene_right_purchase_id;
    }

    /**
     * Sets the value of user_purchase_gene_right_purchase_id.
     *
     * @param mixed $user_purchase_gene_right_purchase_id the user purchase gene right purchase id
     *
     * @return self
     */
    public function setUserPurchaseGeneRightPurchaseId($user_purchase_gene_right_purchase_id)
    {
        $this->user_purchase_gene_right_purchase_id = $user_purchase_gene_right_purchase_id;

        return $this;
    }

    /**
     * Gets the value of user_purchase_money_alloted.
     *
     * @return mixed
     */
    public function getUserPurchaseMoneyAlloted()
    {
        return $this->user_purchase_money_alloted;
    }

    /**
     * Sets the value of user_purchase_money_alloted.
     *
     * @param mixed $user_purchase_money_alloted the user purchase money alloted
     *
     * @return self
     */
    public function setUserPurchaseMoneyAlloted($user_purchase_money_alloted)
    {
        $this->user_purchase_money_alloted = $user_purchase_money_alloted;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getUserPurchaseTransactionId()
    {
        return $this->user_purchase_transaction_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $user_purchase_transaction_id the user purchase transaction id
     *
     * @return self
     */
    public function setUserPurchaseTransactionId($user_purchase_transaction_id)
    {
        $this->user_purchase_transaction_id = $user_purchase_transaction_id;

        return $this;
    }

    /**
     * Gets the value of user_pruchase_pair_user_purchase_id.
     *
     * @return mixed
     */
    public function getUserPruchasePairUserPurchaseId()
    {
        return $this->user_pruchase_pair_user_purchase_id;
    }

    /**
     * Sets the value of user_pruchase_pair_user_purchase_id.
     *
     * @param mixed $user_pruchase_pair_user_purchase_id the user pruchase pair user purchase id
     *
     * @return self
     */
    public function setUserPruchasePairUserPurchaseId($user_pruchase_pair_user_purchase_id)
    {
        $this->user_pruchase_pair_user_purchase_id = $user_pruchase_pair_user_purchase_id;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getUserPurchaseActualGeneUserPurchaseId()
    {
        return $this->user_purchase_actual_gene_user_purchase_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $user_purchase_actual_gene_user_purchase_id the user purchase actual gene user purchase id
     *
     * @return self
     */
    public function setUserPurchaseActualGeneUserPurchaseId($user_purchase_actual_gene_user_purchase_id)
    {
        $this->user_purchase_actual_gene_user_purchase_id = $user_purchase_actual_gene_user_purchase_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPairAchieve()
    {
        return $this->pair_achieve;
    }

    /**
     * @param mixed $pair_achieve
     *
     * @return self
     */
    public function setPairAchieve($pair_achieve)
    {
        $this->pair_achieve = $pair_achieve;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPairAchieveSide()
    {
        return $this->pair_achieve_side;
    }

    /**
     * @param mixed $pair_achieve_side
     *
     * @return self
     */
    public function setPairAchieveSide($pair_achieve_side)
    {
        $this->pair_achieve_side = $pair_achieve_side;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPairAchieveUserPurchaseId()
    {
        return $this->pair_achieve_user_purchase_id;
    }

    /**
     * @param mixed $pair_achieve_user_purchase_id
     *
     * @return self
     */
    public function setPairAchieveUserPurchaseId($pair_achieve_user_purchase_id)
    {
        $this->pair_achieve_user_purchase_id = $pair_achieve_user_purchase_id;

        return $this;
    }
    /**
     * Gets triggered only on insert

     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_at = new \DateTime("now");
        // $this->created_at = $created_at;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }
    /**
     * Gets triggered every time on update

     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_at = new \DateTime("now");
    }
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = new \DateTime($updated_at);
        return $this;
    }
     public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @return mixed
     */
    public function getPayoutReleased()
    {
        return $this->payout_released;
    }

    /**
     * @param mixed $payout_released
     *
     * @return self
     */
    public function setPayoutReleased($payout_released)
    {
        $this->payout_released = $payout_released;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRepCount()
    {
        return $this->rep_count;
    }

    /**
     * @param mixed $rep_count
     *
     * @return self
     */
    public function setRepCount($rep_count)
    {
        $this->rep_count = $rep_count;

        return $this;
    }

    

    /**
     * @return mixed
     */
    public function getLevelIncomeAchieve()
    {
        return $this->level_income_achieve;
    }

    /**
     * @param mixed $level_income_achieve
     *
     * @return self
     */
    public function setLevelIncomeAchieve($level_income_achieve)
    {
        $this->level_income_achieve = $level_income_achieve;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPayoutAch()
    {
        return $this->payout_ach;
    }

    /**
     * @param mixed $payout_ach
     *
     * @return self
     */
    public function setPayoutAch($payout_ach)
    {
        $this->payout_ach = $payout_ach;

        return $this;
    }
}

?>