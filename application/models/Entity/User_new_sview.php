<?php 
namespace Entity;


class User_new_sview{

  protected $user_id;
  protected $user_name;
  protected $first_name;
  protected $last_name;
  protected $user_email;
  protected $dob;

  protected $primary_mobile;
  protected $secondary_mobile;

  protected $gender;
  protected $aadhar_no;
  protected $pan_no;
  protected $aadhar_verified;
  protected $pan_verified;
  protected $pass_verified;
  protected $kyc_status;
  protected $created_at;
  protected $updated_at;
  protected $profile_picture_url;

  protected $user_role_id;
  protected $user_role_name;
  protected $user_access_ids;
  protected $unlocked_reason;
  protected $is_active;

  protected $account_no;
  protected $account_type;
  protected $bank_ifsc;
  protected $bank_name;
  protected $bank_branch;

  protected $full_address;
  protected $pincode;
  protected $pincode_place;
  protected $district;
  protected $state;
  protected $country_id;
  protected $country_name;

  protected $n_name;
  protected $n_mobile;
  protected $n_relation;
  protected $n_dob;
  protected $n_gender;

  protected $password;
  protected $pin_password;

  protected $aadhar_image_url;
  protected $pan_image_url;
  protected $pass_image_url;



  /**
   * @return mixed
   */
  public function getUserId()
  {
      return $this->user_id;
  }

  /**
   * @param mixed $user_id
   *
   * @return self
   */
  public function setUserId($user_id)
  {
      $this->user_id = $user_id;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getUserName()
  {
      return $this->user_name;
  }

  /**
   * @param mixed $user_name
   *
   * @return self
   */
  public function setUserName($user_name)
  {
      $this->user_name = $user_name;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getFirstName()
  {
      return $this->first_name;
  }

  /**
   * @param mixed $first_name
   *
   * @return self
   */
  public function setFirstName($first_name)
  {
      $this->first_name = $first_name;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getLastName()
  {
      return $this->last_name;
  }

  /**
   * @param mixed $last_name
   *
   * @return self
   */
  public function setLastName($last_name)
  {
      $this->last_name = $last_name;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getUserEmail()
  {
      return $this->user_email;
  }

  /**
   * @param mixed $user_email
   *
   * @return self
   */
  public function setUserEmail($user_email)
  {
      $this->user_email = $user_email;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getDob()
  {
      return $this->dob;
  }

  /**
   * @param mixed $dob
   *
   * @return self
   */
  public function setDob($dob)
  {
      $this->dob = $dob;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getPrimaryMobile()
  {
      return $this->primary_mobile;
  }

  /**
   * @param mixed $primary_mobile
   *
   * @return self
   */
  public function setPrimaryMobile($primary_mobile)
  {
      $this->primary_mobile = $primary_mobile;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getSecondaryMobile()
  {
      return $this->secondary_mobile;
  }

  /**
   * @param mixed $secondary_mobile
   *
   * @return self
   */
  public function setSecondaryMobile($secondary_mobile)
  {
      $this->secondary_mobile = $secondary_mobile;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getGender()
  {
      return $this->gender;
  }

  /**
   * @param mixed $gender
   *
   * @return self
   */
  public function setGender($gender)
  {
      $this->gender = $gender;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getAadharNo()
  {
      return $this->aadhar_no;
  }

  /**
   * @param mixed $aadhar_no
   *
   * @return self
   */
  public function setAadharNo($aadhar_no)
  {
      $this->aadhar_no = $aadhar_no;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getPanNo()
  {
      return $this->pan_no;
  }

  /**
   * @param mixed $pan_no
   *
   * @return self
   */
  public function setPanNo($pan_no)
  {
      $this->pan_no = $pan_no;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getAadharVerified()
  {
      return $this->aadhar_verified;
  }

  /**
   * @param mixed $aadhar_verified
   *
   * @return self
   */
  public function setAadharVerified($aadhar_verified)
  {
      $this->aadhar_verified = $aadhar_verified;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getPanVerified()
  {
      return $this->pan_verified;
  }

  /**
   * @param mixed $pan_verified
   *
   * @return self
   */
  public function setPanVerified($pan_verified)
  {
      $this->pan_verified = $pan_verified;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getPassVerified()
  {
      return $this->pass_verified;
  }

  /**
   * @param mixed $pass_verified
   *
   * @return self
   */
  public function setPassVerified($pass_verified)
  {
      $this->pass_verified = $pass_verified;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getKycStatus()
  {
      return $this->kyc_status;
  }

  /**
   * @param mixed $kyc_status
   *
   * @return self
   */
  public function setKycStatus($kyc_status)
  {
      $this->kyc_status = $kyc_status;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getCreatedAt()
  {
      return $this->created_at;
  }

  /**
   * @param mixed $created_at
   *
   * @return self
   */
  public function setCreatedAt($created_at)
  {
      $this->created_at = $created_at;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getUpdatedAt()
  {
      return $this->updated_at;
  }

  /**
   * @param mixed $updated_at
   *
   * @return self
   */
  public function setUpdatedAt($updated_at)
  {
      $this->updated_at = $updated_at;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getProfilePictureUrl()
  {
      return $this->profile_picture_url;
  }

  /**
   * @param mixed $profile_picture_url
   *
   * @return self
   */
  public function setProfilePictureUrl($profile_picture_url)
  {
      $this->profile_picture_url = $profile_picture_url;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getUserRoleId()
  {
      return $this->user_role_id;
  }

  /**
   * @param mixed $user_role_id
   *
   * @return self
   */
  public function setUserRoleId($user_role_id)
  {
      $this->user_role_id = $user_role_id;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getUserAccessIds()
  {
      return $this->user_access_ids;
  }

  /**
   * @param mixed $user_access_ids
   *
   * @return self
   */
  public function setUserAccessIds($user_access_ids)
  {
      $this->user_access_ids = $user_access_ids;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getUnlockedReason()
  {
      return $this->unlocked_reason;
  }

  /**
   * @param mixed $unlocked_reason
   *
   * @return self
   */
  public function setUnlockedReason($unlocked_reason)
  {
      $this->unlocked_reason = $unlocked_reason;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getIsActive()
  {
      return $this->is_active;
  }

  /**
   * @param mixed $is_active
   *
   * @return self
   */
  public function setIsActive($is_active)
  {
      $this->is_active = $is_active;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getAccountNo()
  {
      return $this->account_no;
  }

  /**
   * @param mixed $account_no
   *
   * @return self
   */
  public function setAccountNo($account_no)
  {
      $this->account_no = $account_no;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getBankIfsc()
  {
      return $this->bank_ifsc;
  }

  /**
   * @param mixed $bank_ifsc
   *
   * @return self
   */
  public function setBankIfsc($bank_ifsc)
  {
      $this->bank_ifsc = $bank_ifsc;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getBankName()
  {
      return $this->bank_name;
  }

  /**
   * @param mixed $bank_name
   *
   * @return self
   */
  public function setBankName($bank_name)
  {
      $this->bank_name = $bank_name;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getBankBranch()
  {
      return $this->bank_branch;
  }

  /**
   * @param mixed $bank_branch
   *
   * @return self
   */
  public function setBankBranch($bank_branch)
  {
      $this->bank_branch = $bank_branch;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getFullAddress()
  {
      return $this->full_address;
  }

  /**
   * @param mixed $full_address
   *
   * @return self
   */
  public function setFullAddress($full_address)
  {
      $this->full_address = $full_address;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getPincode()
  {
      return $this->pincode;
  }

  /**
   * @param mixed $pincode
   *
   * @return self
   */
  public function setPincode($pincode)
  {
      $this->pincode = $pincode;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getDistrict()
  {
      return $this->district;
  }

  /**
   * @param mixed $district
   *
   * @return self
   */
  public function setDistrict($district)
  {
      $this->district = $district;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getState()
  {
      return $this->state;
  }

  /**
   * @param mixed $state
   *
   * @return self
   */
  public function setState($state)
  {
      $this->state = $state;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getCountryId()
  {
      return $this->country_id;
  }

  /**
   * @param mixed $country_id
   *
   * @return self
   */
  public function setCountryId($country_id)
  {
      $this->country_id = $country_id;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getNName()
  {
      return $this->n_name;
  }

  /**
   * @param mixed $n_name
   *
   * @return self
   */
  public function setNName($n_name)
  {
      $this->n_name = $n_name;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getNMobile()
  {
      return $this->n_mobile;
  }

  /**
   * @param mixed $n_mobile
   *
   * @return self
   */
  public function setNMobile($n_mobile)
  {
      $this->n_mobile = $n_mobile;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getNRelation()
  {
      return $this->n_relation;
  }

  /**
   * @param mixed $n_relation
   *
   * @return self
   */
  public function setNRelation($n_relation)
  {
      $this->n_relation = $n_relation;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getNDob()
  {
      return $this->n_dob;
  }

  /**
   * @param mixed $n_dob
   *
   * @return self
   */
  public function setNDob($n_dob)
  {
      $this->n_dob = $n_dob;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getNGender()
  {
      return $this->n_gender;
  }

  /**
   * @param mixed $n_gender
   *
   * @return self
   */
  public function setNGender($n_gender)
  {
      $this->n_gender = $n_gender;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getPassword()
  {
      return $this->password;
  }

  /**
   * @param mixed $password
   *
   * @return self
   */
  public function setPassword($password)
  {
      $this->password = $password;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getPinPassword()
  {
      return $this->pin_password;
  }

  /**
   * @param mixed $pin_password
   *
   * @return self
   */
  public function setPinPassword($pin_password)
  {
      $this->pin_password = $pin_password;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getAadharImageUrl()
  {
      return $this->aadhar_image_url;
  }

  /**
   * @param mixed $aadhar_image_url
   *
   * @return self
   */
  public function setAadharImageUrl($aadhar_image_url)
  {
      $this->aadhar_image_url = $aadhar_image_url;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getPanImageUrl()
  {
      return $this->pan_image_url;
  }

  /**
   * @param mixed $pan_image_url
   *
   * @return self
   */
  public function setPanImageUrl($pan_image_url)
  {
      $this->pan_image_url = $pan_image_url;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getPassImageUrl()
  {
      return $this->pass_image_url;
  }

  /**
   * @param mixed $pass_image_url
   *
   * @return self
   */
  public function setPassImageUrl($pass_image_url)
  {
      $this->pass_image_url = $pass_image_url;

      return $this;
  }

    /**
     * @return mixed
     */
    public function getUserRoleName()
    {
        return $this->user_role_name;
    }

    /**
     * @param mixed $user_role_name
     *
     * @return self
     */
    public function setUserRoleName($user_role_name)
    {
        $this->user_role_name = $user_role_name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAccountType()
    {
        return $this->account_type;
    }

    /**
     * @param mixed $account_type
     *
     * @return self
     */
    public function setAccountType($account_type)
    {
        $this->account_type = $account_type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPincodePlace()
    {
        return $this->pincode_place;
    }

    /**
     * @param mixed $pincode_place
     *
     * @return self
     */
    public function setPincodePlace($pincode_place)
    {
        $this->pincode_place = $pincode_place;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountryName()
    {
        return $this->country_name;
    }

    /**
     * @param mixed $country_name
     *
     * @return self
     */
    public function setCountryName($country_name)
    {
        $this->country_name = $country_name;

        return $this;
    }
}