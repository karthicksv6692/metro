<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Purchase_bill")
*/
class Purchase_bill{

	/**
	*
	* @Id
	* @Column(name="purchase_bill_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $purchase_bill_id;

	/**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Purchase_request", fetch="EAGER")
     * @JoinColumn(name="purchase_request_id", referencedColumnName="purchase_request_id")
     */
    private $purchase_request_id;

    /**
	*@Column(type="integer")
	****/
	protected $purchase_bill_no;

	/**
	*@Column(type="datetime")
	****/
	protected $purchase_bill_delivered_date;

	/**
	*@Column(type="integer")
	****/
	protected $purchase_bill_status;


    /**
     * Gets the value of purchase_bill_id.
     *
     * @return mixed
     */
    public function getPurchaseBillId()
    {
        return $this->purchase_bill_id;
    }

    /**
     * Sets the value of purchase_bill_id.
     *
     * @param mixed $purchase_bill_id the purchase bill id
     *
     * @return self
     */
    public function setPurchaseBillId($purchase_bill_id)
    {
        $this->purchase_bill_id = $purchase_bill_id;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getPurchaseRequestId()
    {
        return $this->purchase_request_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $purchase_request_id the purchase request id
     *
     * @return self
     */
    public function setPurchaseRequestId($purchase_request_id)
    {
        $this->purchase_request_id = $purchase_request_id;

        return $this;
    }

    /**
     * Gets the value of purchase_bill_no.
     *
     * @return mixed
     */
    public function getPurchaseBillNo()
    {
        return $this->purchase_bill_no;
    }

    /**
     * Sets the value of purchase_bill_no.
     *
     * @param mixed $purchase_bill_no the purchase bill no
     *
     * @return self
     */
    public function setPurchaseBillNo($purchase_bill_no)
    {
        $this->purchase_bill_no = $purchase_bill_no;

        return $this;
    }

    /**
     * Gets the value of purchase_bill_delivered_date.
     *
     * @return mixed
     */
    public function getPurchaseBillDeliveredDate()
    {
        return $this->purchase_bill_delivered_date;
    }

    /**
     * Sets the value of purchase_bill_delivered_date.
     *
     * @param mixed $purchase_bill_delivered_date the purchase bill delivered date
     *
     * @return self
     */
    public function setPurchaseBillDeliveredDate($purchase_bill_delivered_date)
    {
        $this->purchase_bill_delivered_date = $purchase_bill_delivered_date;

        return $this;
    }

    /**
     * Gets the value of purchase_bill_status.
     *
     * @return mixed
     */
    public function getPurchaseBillStatus()
    {
        return $this->purchase_bill_status;
    }

    /**
     * Sets the value of purchase_bill_status.
     *
     * @param mixed $purchase_bill_status the purchase bill status
     *
     * @return self
     */
    public function setPurchaseBillStatus($purchase_bill_status)
    {
        $this->purchase_bill_status = $purchase_bill_status;

        return $this;
    }
}

?>