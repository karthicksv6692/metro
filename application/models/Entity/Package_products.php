<?php
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Package_products")
*/
class Package_products
{
	/**
	*
	* @Id
	* @Column(name="package_product_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
    private $package_product_id;


    /**
	*@Column(type="string",length=255,nullable=false)
	*
	*
	**/
	protected $package_product_name;

	/**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Packages", fetch="EAGER")
     * @JoinColumn(name="package_id", referencedColumnName="package_id")
     */
    private $package_id;

     /**
	*
	* @Column(name="package_product_quantity",type="integer",nullable=false)
	*/
    private $package_product_quantity;

     /**
    *
    * @Column(name="package_product_price",type="integer",nullable=false)
    */
    private $package_product_price;

     /**
	*
	* @Column(name="package_product_display_price",type="integer",nullable=false)
	*/
    private $package_product_display_price;


	/**
     * @var string
     *
     * @Column(name="package_product_desc", type="string", length=255, nullable=false)
     */
    private $package_product_desc;
	/**
     * @var string
     *
     * @Column(name="package_product_short_desc", type="string", length=255, nullable=false)
     */
    private $package_product_short_desc;


	/**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Offer", fetch="EAGER")
     * @JoinColumn(name="packages_product_offer_id", referencedColumnName="offer_id")
     */
    private $packages_product_offer_id;

     /**
	*
	* @Column(name="package_product_is_visible",type="integer")
	*/
    private $package_product_is_visible;


    /**
     *
     * @ORM OneToMany(targetEntity="Entity\Package_product_items" mappedBy="package_product_id")
     */
    private $pack_product_id;
  


    /**
     * Gets the value of package_product_id.
     *
     * @return mixed
     */
    public function getPackageProductId()
    {
        return $this->package_product_id;
    }

    /**
     * Sets the value of package_product_id.
     *
     * @param mixed $package_product_id the package product id
     *
     * @return self
     */
    public function setPackageProductId($package_product_id)
    {
        $this->package_product_id = $package_product_id;

        return $this;
    }

    /**
     * Gets the value of package_product_name.
     *
     * @return mixed
     */
    public function getPackageProductName()
    {
        return $this->package_product_name;
    }

    /**
     * Sets the value of package_product_name.
     *
     * @param mixed $package_product_name the package product name
     *
     * @return self
     */
    public function setPackageProductName($package_product_name)
    {
        $this->package_product_name = $package_product_name;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $product_id the product id
     *
     * @return self
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;

        return $this;
    }

    /**
     * Gets the value of package_product_quantity.
     *
     * @return mixed
     */
    public function getPackageProductQuantity()
    {
        return $this->package_product_quantity;
    }

    /**
     * Sets the value of package_product_quantity.
     *
     * @param mixed $package_product_quantity the package product quantity
     *
     * @return self
     */
    public function setPackageProductQuantity($package_product_quantity)
    {
        $this->package_product_quantity = $package_product_quantity;

        return $this;
    }

    /**
     * Gets the value of package_product_display_price.
     *
     * @return mixed
     */
    public function getPackageProductDisplayPrice()
    {
        return $this->package_product_display_price;
    }

    /**
     * Sets the value of package_product_display_price.
     *
     * @param mixed $package_product_display_price the package product display price
     *
     * @return self
     */
    public function setPackageProductDisplayPrice($package_product_display_price)
    {
        $this->package_product_display_price = $package_product_display_price;

        return $this;
    }

    /**
     * Gets the value of package_product_desc.
     *
     * @return string
     */
    public function getPackageProductDesc()
    {
        return $this->package_product_desc;
    }

    /**
     * Sets the value of package_product_desc.
     *
     * @param string $package_product_desc the package product desc
     *
     * @return self
     */
    public function setPackageProductDesc($package_product_desc)
    {
        $this->package_product_desc = $package_product_desc;

        return $this;
    }

    /**
     * Gets the value of package_product_short_desc.
     *
     * @return string
     */
    public function getPackageProductShortDesc()
    {
        return $this->package_product_short_desc;
    }

    /**
     * Sets the value of package_product_short_desc.
     *
     * @param string $package_product_short_desc the package product short desc
     *
     * @return self
     */
    public function setPackageProductShortDesc($package_product_short_desc)
    {
        $this->package_product_short_desc = $package_product_short_desc;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getPackagesProductOfferId()
    {
        return $this->packages_product_offer_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $packages_product_offer_id the packages product offer id
     *
     * @return self
     */
    public function setPackagesProductOfferId($packages_product_offer_id)
    {
        $this->packages_product_offer_id = $packages_product_offer_id;

        return $this;
    }

    /**
     * Gets the value of package_product_is_visible.
     *
     * @return mixed
     */
    public function getPackageProductIsVisible()
    {
        return $this->package_product_is_visible;
    }

    /**
     * Sets the value of package_product_is_visible.
     *
     * @param mixed $package_product_is_visible the package product is visible
     *
     * @return self
     */
    public function setPackageProductIsVisible($package_product_is_visible)
    {
        $this->package_product_is_visible = $package_product_is_visible;

        return $this;
    }

    /**
     * Gets the value of pack_product_id.
     *
     * @return mixed
     */
    public function getPackProductId()
    {
        return $this->pack_product_id;
    }

    /**
     * Sets the value of pack_product_id.
     *
     * @param mixed $pack_product_id the pack product id
     *
     * @return self
     */
    public function setPackProductId($pack_product_id)
    {
        $this->pack_product_id = $pack_product_id;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getPackageId()
    {
        return $this->package_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $package_id the package id
     *
     * @return self
     */
    public function setPackageId($package_id)
    {
        $this->package_id = $package_id;

        return $this;
    }

    /**
     * Gets the value of package_product_price.
     *
     * @return mixed
     */
    public function getPackageProductPrice()
    {
        return $this->package_product_price;
    }

    /**
     * Sets the value of package_product_price.
     *
     * @param mixed $package_product_price the package product price
     *
     * @return self
     */
    public function setPackageProductPrice($package_product_price)
    {
        $this->package_product_price = $package_product_price;

        return $this;
    }

  
}
?>