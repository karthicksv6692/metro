<?php 
  namespace Entity;
  use Doctrine\ORM\Mapping as ORM;
  use Doctrine\Common\Collections\Collection;
  use Doctrine\Common\Collections\ArrayCollection;
  use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
  
  class Payout_details_result_view{
    
    protected $total_left_pv;
    protected $total_right_pv;
    protected $carry_forward_pv;
    protected $carry_forward_pv_side;
    protected $net_payout;
    
  
    /**
     * @return mixed
     */
    public function getTotalLeftPv()
    {
        return $this->total_left_pv;
    }

    /**
     * @param mixed $total_left_pv
     *
     * @return self
     */
    public function setTotalLeftPv($total_left_pv)
    {
        $this->total_left_pv = $total_left_pv;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalRightPv()
    {
        return $this->total_right_pv;
    }

    /**
     * @param mixed $total_right_pv
     *
     * @return self
     */
    public function setTotalRightPv($total_right_pv)
    {
        $this->total_right_pv = $total_right_pv;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCarryForwardPv()
    {
        return $this->carry_forward_pv;
    }

    /**
     * @param mixed $carry_forward_pv
     *
     * @return self
     */
    public function setCarryForwardPv($carry_forward_pv)
    {
        $this->carry_forward_pv = $carry_forward_pv;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCarryForwardPvSide()
    {
        return $this->carry_forward_pv_side;
    }

    /**
     * @param mixed $carry_forward_pv_side
     *
     * @return self
     */
    public function setCarryForwardPvSide($carry_forward_pv_side)
    {
        $this->carry_forward_pv_side = $carry_forward_pv_side;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNetPayout()
    {
        return $this->net_payout;
    }

    /**
     * @param mixed $net_payout
     *
     * @return self
     */
    public function setNetPayout($net_payout)
    {
        $this->net_payout = $net_payout;

        return $this;
    }
}

?>