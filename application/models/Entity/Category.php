<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Category")
*/
class Category{

  /**
  *
  * @Id
  * @Column(name="category_id",type="integer")
  * @GeneratedValue(strategy="AUTO")
  */
  protected $category_id;


  /**
  *@Column(type="string",length=255,nullable=false,unique=true)
  ****/
  protected $category_name;

  /**
  *@Column(type="string",length=255,nullable=true)
  ****/
  protected $category_desc;

  /**
  *@Column(type="integer",nullable=true)
  ****/
  protected $parent_id;

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }

    /**
     * @param mixed $category_id
     *
     * @return self
     */
    public function setCategoryId($category_id)
    {
        $this->category_id = $category_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategoryName()
    {
        return $this->category_name;
    }

    /**
     * @param mixed $category_name
     *
     * @return self
     */
    public function setCategoryName($category_name)
    {
        $this->category_name = $category_name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategoryDesc()
    {
        return $this->category_desc;
    }

    /**
     * @param mixed $category_desc
     *
     * @return self
     */
    public function setCategoryDesc($category_desc)
    {
        $this->category_desc = $category_desc;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * @param mixed $parent_id
     *
     * @return self
     */
    public function setParentId($parent_id)
    {
        $this->parent_id = $parent_id;

        return $this;
    }
}
?>