<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Sample_upp")
*/
class Sample_upp{

	/**
	*
	* @Id
	* @Column(name="user_purchase_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $user_purchase_id;

	/**
	*@Column(type="integer")
	****/
	protected $user_purchase_sponser_gene_side;

	/**
    *@Column(type="integer")
    ****/
	protected $user_purchase_sponser_user_purchase_id;

	/**
	*@Column(type="integer")
	****/
	protected $user_purchase_actual_gene_side;

   /**
    *@Column(type="integer")
    ****/
    protected $user_purchase_actual_gene_user_purchase_id;
     /**
    *@Column(type="integer")
    ****/
    protected $user_purchase_gene_left_purchase_id;
	
	/**
	*@Column(type="integer")
	****/
	protected $user_purchase_gene_right_purchase_id;

	/**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Pins", fetch="EAGER")
     * @JoinColumn(name="user_purchase_pin_id", referencedColumnName="pin_id",onDelete="cascade")
     */
	protected $user_purchase_pin_id;
	/**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\User", fetch="EAGER")
     * @JoinColumn(name="user_purchase_pin_used_user_id", referencedColumnName="user_id",onDelete="cascade")
     */
	protected $user_purchase_pin_used_user_id;

	/**
    *@Column(type="integer")
    ****/
    protected $left_pair_count;

    /**
    *@Column(type="integer")
    ****/
    protected $right_pair_count;

     /**
    *@Column(type="integer")
    ****/
    protected $total_count;



    /**
     * @return mixed
     */
    public function getUserPurchaseId()
    {
        return $this->user_purchase_id;
    }

    /**
     * @param mixed $user_purchase_id
     *
     * @return self
     */
    public function setUserPurchaseId($user_purchase_id)
    {
        $this->user_purchase_id = $user_purchase_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserPurchaseSponserGeneSide()
    {
        return $this->user_purchase_sponser_gene_side;
    }

    /**
     * @param mixed $user_purchase_sponser_gene_side
     *
     * @return self
     */
    public function setUserPurchaseSponserGeneSide($user_purchase_sponser_gene_side)
    {
        $this->user_purchase_sponser_gene_side = $user_purchase_sponser_gene_side;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserPurchaseSponserUserPurchaseId()
    {
        return $this->user_purchase_sponser_user_purchase_id;
    }

    /**
     * @param mixed $user_purchase_sponser_user_purchase_id
     *
     * @return self
     */
    public function setUserPurchaseSponserUserPurchaseId($user_purchase_sponser_user_purchase_id)
    {
        $this->user_purchase_sponser_user_purchase_id = $user_purchase_sponser_user_purchase_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserPurchaseActualGeneSide()
    {
        return $this->user_purchase_actual_gene_side;
    }

    /**
     * @param mixed $user_purchase_actual_gene_side
     *
     * @return self
     */
    public function setUserPurchaseActualGeneSide($user_purchase_actual_gene_side)
    {
        $this->user_purchase_actual_gene_side = $user_purchase_actual_gene_side;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserPurchaseActualGeneUserPurchaseId()
    {
        return $this->user_purchase_actual_gene_user_purchase_id;
    }

    /**
     * @param mixed $user_purchase_actual_gene_user_purchase_id
     *
     * @return self
     */
    public function setUserPurchaseActualGeneUserPurchaseId($user_purchase_actual_gene_user_purchase_id)
    {
        $this->user_purchase_actual_gene_user_purchase_id = $user_purchase_actual_gene_user_purchase_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserPurchaseGeneLeftPurchaseId()
    {
        return $this->user_purchase_gene_left_purchase_id;
    }

    /**
     * @param mixed $user_purchase_gene_left_purchase_id
     *
     * @return self
     */
    public function setUserPurchaseGeneLeftPurchaseId($user_purchase_gene_left_purchase_id)
    {
        $this->user_purchase_gene_left_purchase_id = $user_purchase_gene_left_purchase_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserPurchaseGeneRightPurchaseId()
    {
        return $this->user_purchase_gene_right_purchase_id;
    }

    /**
     * @param mixed $user_purchase_gene_right_purchase_id
     *
     * @return self
     */
    public function setUserPurchaseGeneRightPurchaseId($user_purchase_gene_right_purchase_id)
    {
        $this->user_purchase_gene_right_purchase_id = $user_purchase_gene_right_purchase_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserPurchasePinId()
    {
        return $this->user_purchase_pin_id;
    }

    /**
     * @param mixed $user_purchase_pin_id
     *
     * @return self
     */
    public function setUserPurchasePinId($user_purchase_pin_id)
    {
        $this->user_purchase_pin_id = $user_purchase_pin_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserPurchasePinUsedUserId()
    {
        return $this->user_purchase_pin_used_user_id;
    }

    /**
     * @param mixed $user_purchase_pin_used_user_id
     *
     * @return self
     */
    public function setUserPurchasePinUsedUserId($user_purchase_pin_used_user_id)
    {
        $this->user_purchase_pin_used_user_id = $user_purchase_pin_used_user_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLeftPairCount()
    {
        return $this->left_pair_count;
    }

    /**
     * @param mixed $left_pair_count
     *
     * @return self
     */
    public function setLeftPairCount($left_pair_count)
    {
        $this->left_pair_count = $left_pair_count;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRightPairCount()
    {
        return $this->right_pair_count;
    }

    /**
     * @param mixed $right_pair_count
     *
     * @return self
     */
    public function setRightPairCount($right_pair_count)
    {
        $this->right_pair_count = $right_pair_count;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalCount()
    {
        return $this->total_count;
    }

    /**
     * @param mixed $total_count
     *
     * @return self
     */
    public function setTotalCount($total_count)
    {
        $this->total_count = $total_count;

        return $this;
    }
}

?>