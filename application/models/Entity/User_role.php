<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="User_role")
*/
class User_role{

	/**
	*
	* @Id
	* @Column(name="user_role_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $user_role_id;

	/**
	*@Column(type="string",length=255,nullable=false,unique=true)
	****/
	protected $user_role_name;

	/**
	*@Column(type="string",length=255)
	****/
	protected $user_role_desc;

	/**
	*@Column(type="string",length=255)
	****/
    protected $user_role_access_ids;



	protected $access_obj;


	/**
     *
     * @ORM OneToMany(targetEntity="Entity\User" mappedBy="user_role_id")
     */
    private $us_role_id;


    /**
     * Gets the value of user_role_id.
     *
     * @return mixed
     */
    public function getUserRoleId()
    {
        return $this->user_role_id;
    }

    /**
     * Sets the value of user_role_id.
     *
     * @param mixed $user_role_id the user role id
     *
     * @return self
     */
    public function setUserRoleId($user_role_id)
    {
        $this->user_role_id = $user_role_id;

        return $this;
    }

    /**
     * Gets the value of user_role_name.
     *
     * @return mixed
     */
    public function getUserRoleName()
    {
        return $this->user_role_name;
    }

    /**
     * Sets the value of user_role_name.
     *
     * @param mixed $user_role_name the user role name
     *
     * @return self
     */
    public function setUserRoleName($user_role_name)
    {
        $this->user_role_name = $user_role_name;

        return $this;
    }

    /**
     * Gets the value of user_role_desc.
     *
     * @return mixed
     */
    public function getUserRoleDesc()
    {
        return $this->user_role_desc;
    }

    /**
     * Sets the value of user_role_desc.
     *
     * @param mixed $user_role_desc the user role desc
     *
     * @return self
     */
    public function setUserRoleDesc($user_role_desc)
    {
        $this->user_role_desc = $user_role_desc;

        return $this;
    }

    /**
     * Gets the value of user_role_access_ids.
     *
     * @return mixed
     */
    public function getUserRoleAccessIds()
    {
        return $this->user_role_access_ids;
    }

    /**
     * Sets the value of user_role_access_ids.
     *
     * @param mixed $user_role_access_ids the user role access ids
     *
     * @return self
     */
    public function setUserRoleAccessIds($user_role_access_ids)
    {
        $this->user_role_access_ids = $user_role_access_ids;

        return $this;
    }

    /**
     * Gets the value of us_role_id.
     *
     * @return mixed
     */
    public function getUsRoleId()
    {
        return $this->us_role_id;
    }

    /**
     * Sets the value of us_role_id.
     *
     * @param mixed $us_role_id the us role id
     *
     * @return self
     */
    private function _setUsRoleId($us_role_id)
    {
        $this->us_role_id = $us_role_id;

        return $this;
    }

    /**
     * Gets the value of access_obj.
     *
     * @return mixed
     */
   
}
?>