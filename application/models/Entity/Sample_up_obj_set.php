<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
class Sample_up_obj_set{
	
	protected $root_name;
	protected $left_obj;
	protected $right_obj;

    /**
     * @return mixed
     */
    public function getRootName()
    {
        return $this->root_name;
    }

    /**
     * @param mixed $root_name
     *
     * @return self
     */
    public function setRootName($root_name)
    {
        $this->root_name = $root_name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLeftObj()
    {
        return $this->left_obj;
    }

    /**
     * @param mixed $left_obj
     *
     * @return self
     */
    public function setLeftObj($left_obj)
    {
        $this->left_obj = $left_obj;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRightObj()
    {
        return $this->right_obj;
    }

    /**
     * @param mixed $right_obj
     *
     * @return self
     */
    public function setRightObj($right_obj)
    {
        $this->right_obj = $right_obj;

        return $this;
    }
}

?>