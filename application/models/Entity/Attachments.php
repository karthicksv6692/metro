<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Attachments")
*/
class Attachments{

	/**
	*
	* @Id
	* @Column(name="attachment_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $attachment_id;

	/**
	*@Column(type="string",length=255)
	****/
	protected $attachment_name;

	/**
	*@Column(type="string",length=255)
	****/
	protected $attachment_image_url;

    
    /**
    *@Column(type="integer")
    ****/
	protected $attachment_referer_id;

	/**
	*@Column(type="integer")
	****/
	protected $attachment_referer_type;


	/**
	*@Column(type="string",length=255)
	****/
	protected $attachment_remark;



    /**
     * Gets the value of attachment_id.
     *
     * @return mixed
     */
    public function getAttachmentId()
    {
        return $this->attachment_id;
    }

    /**
     * Sets the value of attachment_id.
     *
     * @param mixed $attachment_id the attachment id
     *
     * @return self
     */
    public function setAttachmentId($attachment_id)
    {
        $this->attachment_id = $attachment_id;

        return $this;
    }

    /**
     * Gets the value of attachment_name.
     *
     * @return mixed
     */
    public function getAttachmentName()
    {
        return $this->attachment_name;
    }

    /**
     * Sets the value of attachment_name.
     *
     * @param mixed $attachment_name the attachment name
     *
     * @return self
     */
    public function setAttachmentName($attachment_name)
    {
        $this->attachment_name = $attachment_name;

        return $this;
    }

    /**
     * Gets the value of attachment_image_url.
     *
     * @return mixed
     */
    public function getAttachmentImageUrl()
    {
        return $this->attachment_image_url;
    }

    /**
     * Sets the value of attachment_image_url.
     *
     * @param mixed $attachment_image_url the attachment image url
     *
     * @return self
     */
    public function setAttachmentImageUrl($attachment_image_url)
    {
        $this->attachment_image_url = $attachment_image_url;

        return $this;
    }

    /**
     * Gets the value of attachment_referer_id.
     *
     * @return mixed
     */
    public function getAttachmentRefererId()
    {
        return $this->attachment_referer_id;
    }

    /**
     * Sets the value of attachment_referer_id.
     *
     * @param mixed $attachment_referer_id the attachment referer id
     *
     * @return self
     */
    public function setAttachmentRefererId($attachment_referer_id)
    {
        $this->attachment_referer_id = $attachment_referer_id;

        return $this;
    }

    /**
     * Gets the value of attachment_referer_type.
     *
     * @return mixed
     */
    public function getAttachmentRefererType()
    {
        return $this->attachment_referer_type;
    }

    /**
     * Sets the value of attachment_referer_type.
     *
     * @param mixed $attachment_referer_type the attachment referer type
     *
     * @return self
     */
    public function setAttachmentRefererType($attachment_referer_type)
    {
        $this->attachment_referer_type = $attachment_referer_type;

        return $this;
    }

    /**
     * Gets the value of attachment_remark.
     *
     * @return mixed
     */
    public function getAttachmentRemark()
    {
        return $this->attachment_remark;
    }

    /**
     * Sets the value of attachment_remark.
     *
     * @param mixed $attachment_remark the attachment remark
     *
     * @return self
     */
    public function setAttachmentRemark($attachment_remark)
    {
        $this->attachment_remark = $attachment_remark;

        return $this;
    }
   // /**
    // * Many address have One pincode.
    // * @ManyToOne(targetEntity="Entity\Product",fetch="EAGER")
    // * @JoinColumn(name="attachment_referer_id", referencedColumnName="product_id")
    // */
}
?>