<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
// use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Reviews")
* 
*/
class Reviews{

    /**
    *
    * @Id
    * @Column(name="review_id",type="integer",nullable=false)
    * @GeneratedValue(strategy="AUTO")
    */
    protected $review_id;

    /**
	*@Column(type="integer")
	****/
	protected $review_star;

	/**
	*@Column(type="string",length=255)
	****/
	protected $review_comment;

	///**
    // * Many Features have One Product.
     //* @ManyToOne(targetEntity="Entity\User", fetch="EAGER")
    // * @JoinColumn(name="review_user_id", referencedColumnName="user_id")
    // */

	/**
	*@Column(type="integer")
	****/
	protected $review_user_id;

	/**
	*@Column(type="integer")
	****/
	protected $review_item_id;

	/**
	*@Column(type="string",length=255)
	****/
	protected $review_item_type;
	/**
	*@Column(type="string",length=255)
	****/
	protected $review_status;


    /**
     * Gets the value of review_id.
     *
     * @return mixed
     */
    public function getReviewId()
    {
        return $this->review_id;
    }

    /**
     * Sets the value of review_id.
     *
     * @param mixed $review_id the review id
     *
     * @return self
     */
    public function setReviewId($review_id)
    {
        $this->review_id = $review_id;

        return $this;
    }

    /**
     * Gets the value of review_star.
     *
     * @return mixed
     */
    public function getReviewStar()
    {
        return $this->review_star;
    }

    /**
     * Sets the value of review_star.
     *
     * @param mixed $review_star the review star
     *
     * @return self
     */
    public function setReviewStar($review_star)
    {
        $this->review_star = $review_star;

        return $this;
    }

    /**
     * Gets the value of review_comment.
     *
     * @return mixed
     */
    public function getReviewComment()
    {
        return $this->review_comment;
    }

    /**
     * Sets the value of review_comment.
     *
     * @param mixed $review_comment the review comment
     *
     * @return self
     */
    public function setReviewComment($review_comment)
    {
        $this->review_comment = $review_comment;

        return $this;
    }

    /**
     * Gets the value of review_user_id.
     *
     * @return mixed
     */
    public function getReviewUserId()
    {
        return $this->review_user_id;
    }

    /**
     * Sets the value of review_user_id.
     *
     * @param mixed $review_user_id the review user id
     *
     * @return self
     */
    public function setReviewUserId($review_user_id)
    {
        $this->review_user_id = $review_user_id;

        return $this;
    }

    /**
     * Gets the value of review_item_id.
     *
     * @return mixed
     */
    public function getReviewItemId()
    {
        return $this->review_item_id;
    }

    /**
     * Sets the value of review_item_id.
     *
     * @param mixed $review_item_id the review item id
     *
     * @return self
     */
    public function setReviewItemId($review_item_id)
    {
        $this->review_item_id = $review_item_id;

        return $this;
    }

    /**
     * Gets the value of review_item_type.
     *
     * @return mixed
     */
    public function getReviewItemType()
    {
        return $this->review_item_type;
    }

    /**
     * Sets the value of review_item_type.
     *
     * @param mixed $review_item_type the review item type
     *
     * @return self
     */
    public function setReviewItemType($review_item_type)
    {
        $this->review_item_type = $review_item_type;

        return $this;
    }

    /**
     * Gets the value of review_status.
     *
     * @return mixed
     */
    public function getReviewStatus()
    {
        return $this->review_status;
    }

    /**
     * Sets the value of review_status.
     *
     * @param mixed $review_status the review status
     *
     * @return self
     */
    public function setReviewStatus($review_status)
    {
        $this->review_status = $review_status;

        return $this;
    }
}
?>