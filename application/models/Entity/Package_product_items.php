<?php
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Package_product_items")
*/
class Package_product_items
{
	/**
	*
	* @Id
	* @Column(name="package_product_item_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	private $package_product_item_id;


	/**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Package_products", fetch="EAGER")
     * @JoinColumn(name="package_product_id", referencedColumnName="package_product_id")
     */
    private $package_product_id;

    /**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Product", fetch="EAGER")
     * @JoinColumn(name="product_id", referencedColumnName="product_id")
     */
    private $package_product_item_product_id;

    /**
	*
	* @Column(name="package_product_quantity",type="integer",nullable=false)
	*/
    private $package_product_quantity;

   
    /**
     * Gets the value of package_product_item_id.
     *
     * @return mixed
     */
    public function getPackageProductItemId()
    {
        return $this->package_product_item_id;
    }

    /**
     * Sets the value of package_product_item_id.
     *
     * @param mixed $package_product_item_id the package product item id
     *
     * @return self
     */
    public function setPackageProductItemId($package_product_item_id)
    {
        $this->package_product_item_id = $package_product_item_id;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getPackageProductId()
    {
        return $this->package_product_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $package_product_id the package product id
     *
     * @return self
     */
    public function setPackageProductId($package_product_id)
    {
        $this->package_product_id = $package_product_id;

        return $this;
    }

    /**
     * Gets the value of package_product_quantity.
     *
     * @return mixed
     */
    public function getPackageProductQuantity()
    {
        return $this->package_product_quantity;
    }

    /**
     * Sets the value of package_product_quantity.
     *
     * @param mixed $package_product_quantity the package product quantity
     *
     * @return self
     */
    public function setPackageProductQuantity($package_product_quantity)
    {
        $this->package_product_quantity = $package_product_quantity;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getPackageProductItemProductId()
    {
        return $this->package_product_item_product_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $package_product_item_product_id the package product item product id
     *
     * @return self
     */
    public function setPackageProductItemProductId($package_product_item_product_id)
    {
        $this->package_product_item_product_id = $package_product_item_product_id;

        return $this;
    }

    /**
     * Gets the value of pin_request_package_product_id.
     *
     * @return mixed
     */
    public function getPinRequestPackageProductId()
    {
        return $this->pin_request_package_product_id;
    }

    /**
     * Sets the value of pin_request_package_product_id.
     *
     * @param mixed $pin_request_package_product_id the pin request package product id
     *
     * @return self
     */
    public function setPinRequestPackageProductId($pin_request_package_product_id)
    {
        $this->pin_request_package_product_id = $pin_request_package_product_id;

        return $this;
    }
}

?>