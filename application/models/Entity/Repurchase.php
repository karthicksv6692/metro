<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Repurchase")
*/
class Repurchase{

  /**
  *
  * @Id
  * @Column(name="repurchase_id",type="integer",nullable=false)
  * @GeneratedValue(strategy="AUTO")
  */
  protected $repurchase_id;

   /**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\User_package_purchase", fetch="EAGER")
     * @JoinColumn(name="repurchase_up_id", referencedColumnName="user_purchase_id")
     */
  protected $repurchase_up_id;

  /**
  *@Column(type="integer",nullable=true)
  ****/
  protected $repurchase_side;

   /**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Pins", fetch="EAGER")
     * @JoinColumn(name="repurchase_pin_id", referencedColumnName="pin_id")
     */
  protected $repurchase_pin_id;
  /**
  *@Column(type="integer",nullable=true)
  ****/
  protected $repurchase_type;
  /**
   * @var datetime $created_at
   *
   * @Column(type="datetime")
   */
  protected $created_at;

  /**
   * @var datetime $updated_at
   * 
   * @Column(type="datetime", nullable = true)
   */
  protected $updated_at;


    /**
     * @return mixed
     */
    public function getRepurchaseId()
    {
        return $this->repurchase_id;
    }

    /**
     * @param mixed $repurchase_id
     *
     * @return self
     */
    public function setRepurchaseId($repurchase_id)
    {
        $this->repurchase_id = $repurchase_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRepurchaseUpId()
    {
        return $this->repurchase_up_id;
    }

    /**
     * @param mixed $repurchase_up_id
     *
     * @return self
     */
    public function setRepurchaseUpId($repurchase_up_id)
    {
        $this->repurchase_up_id = $repurchase_up_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRepurchaseSide()
    {
        return $this->repurchase_side;
    }

    /**
     * @param mixed $repurchase_side
     *
     * @return self
     */
    public function setRepurchaseSide($repurchase_side)
    {
        $this->repurchase_side = $repurchase_side;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRepurchasePinId()
    {
        return $this->repurchase_pin_id;
    }

    /**
     * @param mixed $repurchase_pin_id
     *
     * @return self
     */
    public function setRepurchasePinId($repurchase_pin_id)
    {
        $this->repurchase_pin_id = $repurchase_pin_id;

        return $this;
    }
    /**
     * Gets triggered only on insert

     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_at = new \DateTime("now");
    }
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    /**
     * Gets triggered every time on update

     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_at = new \DateTime("now");
    }

    /**
     * @return mixed
     */
    public function getRepurchaseType()
    {
        return $this->repurchase_type;
    }

    /**
     * @param mixed $repurchase_type
     *
     * @return self
     */
    public function setRepurchaseType($repurchase_type)
    {
        $this->repurchase_type = $repurchase_type;

        return $this;
    }
}
?>