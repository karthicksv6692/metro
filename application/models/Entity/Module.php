<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Module")
*/
class Module{

	/**
	*
	* @Id
	* @Column(name="module_id",type="integer")
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $module_id;

	/**
	*@Column(type="string",length=255,nullable=false,unique=true)
	****/
	protected $module_name;

	/**
	*@Column(type="string",length=255)
	****/
	protected $module_desc;


    /**
     * Gets the value of module_id.
     *
     * @return mixed
     */
    public function getModuleId()
    {
        return $this->module_id;
    }

    /**
     * Sets the value of module_id.
     *
     * @param mixed $module_id the module id
     *
     * @return self
     */
    public function setModuleId($module_id)
    {
        $this->module_id = $module_id;

        return $this;
    }

    /**
     * Gets the value of module_name.
     *
     * @return mixed
     */
    public function getModuleName()
    {
        return $this->module_name;
    }

    /**
     * Sets the value of module_name.
     *
     * @param mixed $module_name the module name
     *
     * @return self
     */
    public function setModuleName($module_name)
    {
        $this->module_name = $module_name;

        return $this;
    }

    /**
     * Gets the value of module_desc.
     *
     * @return mixed
     */
    public function getModuleDesc()
    {
        return $this->module_desc;
    }

    /**
     * Sets the value of module_desc.
     *
     * @param mixed $module_desc the module desc
     *
     * @return self
     */
    public function setModuleDesc($module_desc)
    {
        $this->module_desc = $module_desc;

        return $this;
    }

}
?>