<?php
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Offer")
*/
class Offer
{
	/**
	*
	* @Id
	* @Column(name="offer_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
    private $offer_id;

    /**
    *
    * @Column(name="offer_name",type="string",nullable=false,unique=true)
    */
    private $offer_name;

    /**
    *
    * @Column(name="offer_code",unique=true,type="string",nullable=false)
    */
    private $offer_code;

    /**
    *
    * @Column(type="integer")
    */
    private $offer_type;
    /**
    *
    * @Column(type="integer",nullable=true)
    */
    private $offer_value;


     /**
    *
    * @Column(type="integer",nullable=true)
    */  
    private $offer_item_id;


	/**
	*
	* @Column(name="offer_item_type",type="integer")
	*/
    private $offer_item_type;


    /**
    *
    * @Column(name="offer_usage_count",type="integer",nullable=false)
    */
    private $offer_usage_count;


	/**
	*
	* @Column(name="offer_used_count",type="integer")
	*/
    private $offer_used_count;

    /**
    *@Column(type="string")
    ****/
    protected $offer_validity_start_date;
    /**
    *@Column(type="string")
    ****/
    protected $offer_validity_end_date;

    /**
     *
     * @ORM OneToMany(targetEntity="Entity\Package_product" mappedBy="package_product_offer_id")
     */
    private $package_product_offer_id;


    /**
     * Gets the value of offer_id.
     *
     * @return mixed
     */
    public function getOfferId()
    {
        return $this->offer_id;
    }

    /**
     * Sets the value of offer_id.
     *
     * @param mixed $offer_id the offer id
     *
     * @return self
     */
    public function setOfferId($offer_id)
    {
        $this->offer_id = $offer_id;

        return $this;
    }

    /**
     * Gets the value of offer_type.
     *
     * @return mixed
     */
    public function getOfferType()
    {
        return $this->offer_type;
    }

    /**
     * Sets the value of offer_type.
     *
     * @param mixed $offer_type the offer type
     *
     * @return self
     */
    public function setOfferType($offer_type)
    {
        $this->offer_type = $offer_type;

        return $this;
    }

    /**
     * Gets the value of offer_value.
     *
     * @return mixed
     */
    public function getOfferValue()
    {
        return $this->offer_value;
    }

    /**
     * Sets the value of offer_value.
     *
     * @param mixed $offer_value the offer value
     *
     * @return self
     */
    public function setOfferValue($offer_value)
    {
        $this->offer_value = $offer_value;

        return $this;
    }

    /**
     * Gets the value of offer_package_id.
     *
     * @return mixed
     */
    public function getOfferPackageId()
    {
        return $this->offer_package_id;
    }

    /**
     * Sets the value of offer_package_id.
     *
     * @param mixed $offer_package_id the offer package id
     *
     * @return self
     */
    public function setOfferPackageId($offer_package_id)
    {
        $this->offer_package_id = $offer_package_id;

        return $this;
    }

    /**
     * Gets the value of package_product_offer_id.
     *
     * @return mixed
     */
    public function getPackageProductOfferId()
    {
        return $this->package_product_offer_id;
    }

    /**
     * Sets the value of package_product_offer_id.
     *
     * @param mixed $package_product_offer_id the package product offer id
     *
     * @return self
     */
    public function setPackageProductOfferId($package_product_offer_id)
    {
        $this->package_product_offer_id = $package_product_offer_id;

        return $this;
    }

    /**
     * Gets the value of offer_name.
     *
     * @return mixed
     */
    public function getOfferName()
    {
        return $this->offer_name;
    }

    /**
     * Sets the value of offer_name.
     *
     * @param mixed $offer_name the offer name
     *
     * @return self
     */
    public function setOfferName($offer_name)
    {
        $this->offer_name = $offer_name;

        return $this;
    }

    /**
     * Gets the value of offer_code.
     *
     * @return mixed
     */
    public function getOfferCode()
    {
        return $this->offer_code;
    }

    /**
     * Sets the value of offer_code.
     *
     * @param mixed $offer_code the offer code
     *
     * @return self
     */
    public function setOfferCode($offer_code)
    {
        $this->offer_code = $offer_code;

        return $this;
    }

    /**
     * Gets the value of offer_item_id.
     *
     * @return mixed
     */
    public function getOfferItemId()
    {
        return $this->offer_item_id;
    }

    /**
     * Sets the value of offer_item_id.
     *
     * @param mixed $offer_item_id the offer item id
     *
     * @return self
     */
    public function setOfferItemId($offer_item_id)
    {
        $this->offer_item_id = $offer_item_id;

        return $this;
    }

    /**
     * Gets the value of offer_item_type.
     *
     * @return mixed
     */
    public function getOfferItemType()
    {
        return $this->offer_item_type;
    }

    /**
     * Sets the value of offer_item_type.
     *
     * @param mixed $offer_item_type the offer item type
     *
     * @return self
     */
    public function setOfferItemType($offer_item_type)
    {
        $this->offer_item_type = $offer_item_type;

        return $this;
    }

    /**
     * Gets the value of offer_usage_count.
     *
     * @return mixed
     */
    public function getOfferUsageCount()
    {
        return $this->offer_usage_count;
    }

    /**
     * Sets the value of offer_usage_count.
     *
     * @param mixed $offer_usage_count the offer usage count
     *
     * @return self
     */
    public function setOfferUsageCount($offer_usage_count)
    {
        $this->offer_usage_count = $offer_usage_count;

        return $this;
    }

    /**
     * Gets the value of offer_used_count.
     *
     * @return mixed
     */
    public function getOfferUsedCount()
    {
        return $this->offer_used_count;
    }

    /**
     * Sets the value of offer_used_count.
     *
     * @param mixed $offer_used_count the offer used count
     *
     * @return self
     */
    public function setOfferUsedCount($offer_used_count)
    {
        $this->offer_used_count = $offer_used_count;

        return $this;
    }

    /**
     * Gets the value of offer_validity_end_date.
     *
     * @return mixed
     */
    public function getOfferValidityEndDate()
    {
        return $this->offer_validity_end_date;
    }

    /**
     * Sets the value of offer_validity_end_date.
     *
     * @param mixed $offer_validity_end_date the offer validity end date
     *
     * @return self
     */
    public function setOfferValidityEndDate($offer_validity_end_date)
    {
        $this->offer_validity_end_date = $offer_validity_end_date;

        return $this;
    }

    

    // /**
    //  * Sets the value of offer_name.
    //  *
    //  * @param mixed $offer_name the offer name
    //  *
    //  * @return self
    //  */
    // public function setOfferName($offer_name)
    // {
    //     $this->offer_name = $offer_name;

    //     return $this;
    // }

    // /**
    //  * Sets the value of offer_code.
    //  *
    //  * @param mixed $offer_code the offer code
    //  *
    //  * @return self
    //  */
    // public function setOfferCode($offer_code)
    // {
    //     $this->offer_code = $offer_code;

    //     return $this;
    // }

    // /**
    //  * Sets the value of offer_type.
    //  *
    //  * @param mixed $offer_type the offer type
    //  *
    //  * @return self
    //  */
    // public function setOfferType($offer_type)
    // {
    //     $this->offer_type = $offer_type;

    //     return $this;
    // }

    // /**
    //  * Sets the value of offer_value.
    //  *
    //  * @param mixed $offer_value the offer value
    //  *
    //  * @return self
    //  */
    // public function setOfferValue($offer_value)
    // {
    //     $this->offer_value = $offer_value;

    //     return $this;
    // }

    // /**
    //  * Sets the value of offer_item_id.
    //  *
    //  * @param mixed $offer_item_id the offer item id
    //  *
    //  * @return self
    //  */
    // public function setOfferItemId($offer_item_id)
    // {
    //     $this->offer_item_id = $offer_item_id;

    //     return $this;
    // }

    // /**
    //  * Sets the value of offer_item_type.
    //  *
    //  * @param mixed $offer_item_type the offer item type
    //  *
    //  * @return self
    //  */
    // public function setOfferItemType($offer_item_type)
    // {
    //     $this->offer_item_type = $offer_item_type;

    //     return $this;
    // }

    // /**
    //  * Sets the value of offer_usage_count.
    //  *
    //  * @param mixed $offer_usage_count the offer usage count
    //  *
    //  * @return self
    //  */
    // public function setOfferUsageCount($offer_usage_count)
    // {
    //     $this->offer_usage_count = $offer_usage_count;

    //     return $this;
    // }

    // /**
    //  * Sets the value of offer_used_count.
    //  *
    //  * @param mixed $offer_used_count the offer used count
    //  *
    //  * @return self
    //  */
    // public function setOfferUsedCount($offer_used_count)
    // {
    //     $this->offer_used_count = $offer_used_count;

    //     return $this;
    // }

    /**
     * Gets the value of offer_validity_start_date.
     *
     * @return mixed
     */
    public function getOfferValidityStartDate()
    {
        return $this->offer_validity_start_date;
    }

    /**
     * Sets the value of offer_validity_start_date.
     *
     * @param mixed $offer_validity_start_date the offer validity start date
     *
     * @return self
     */
    public function setOfferValidityStartDate($offer_validity_start_date)
    {
        $this->offer_validity_start_date = $offer_validity_start_date;

        return $this;
    }

    /**
     * Sets the value of package_product_offer_id.
     *
     * @param mixed $package_product_offer_id the package product offer id
     *
     * @return self
     */
    // public function setPackageProductOfferId($package_product_offer_id)
    // {
    //     $this->package_product_offer_id = $package_product_offer_id;

    //     return $this;
    // }
}

?>