<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

/**
* @Entity
* @Table(name="User")
*/
class User{

	/**
	*
	* @Id
	* @Column(name="user_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $user_id;

	/**
	*@Column(type="string",length=255,nullable=false,unique=true)
	****/
	protected $user_name;
	/**
	*@Column(type="string",length=255,nullable=false,unique=false)
	****/
	protected $user_email;

	/**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Contact", fetch="EAGER")
     * @JoinColumn(name="contact_id", referencedColumnName="contact_id")
     */
	protected $contact_id;


    /**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Nominee", fetch="EAGER")
     * @JoinColumn(name="nominee_id", referencedColumnName="nominee_id",onDelete="cascade")
     */
    protected $nominee_id;

	/**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Password", fetch="EAGER")
     * @JoinColumn(name="password_id", referencedColumnName="password_id")
     */
	protected $password_id;

	/**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Address", fetch="EAGER", cascade="persist")
     * @JoinColumn(name="address_id", referencedColumnName="address_id", onDelete="SET NULL")
     */
	protected $address_id;

	/**
	*@Column(type="string",length=255,nullable=false)
	****/
	protected $first_name;

	/**
	*@Column(type="string",length=255)
	****/
	protected $last_name;
	/**
	*@Column(type="integer",nullable=true)
	****/
	protected $gender;

	/**
	*@Column(type="datetime")
	****/
	protected $dob;


	/**
	*@Column(type="string",length=255)
	****/
	protected $profile_picture_url;


	/**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\User_role", fetch="EAGER",cascade={"persist"})
     * @JoinColumn(name="user_role_id", referencedColumnName="user_role_id",nullable=true,onDelete="SET NULL")
     */
	protected $user_role_id;

	/**
	*@Column(type="string",length=255)
	****/
	protected $user_verification_url;

	/**
	*@Column(type="integer")
	****/
	protected $user_email_verified;
	/**
	*@Column(type="integer")
	****/
	protected $is_active;


	/**
	*@Column(type="datetime",nullable=true)
	****/
	protected $last_login_time;


	/**
	*@Column(type="string",length=255,nullable=true)
	****/
	protected $last_login_ip_address;

    /**
    *@Column(type="datetime",nullable=true)
    ****/
    protected $account_validity_time;

     /**
    *@Column(type="integer",nullable=true)
    ****/
    protected $invalid_login_attempts;

    /**
    *@Column(type="datetime",nullable=true)
    ****/
    protected $last_invalid_login_time;

    /**
    *@Column(type="string",length=255,nullable=true)
    ****/
    protected $last_invalid_login_ip_address;


	/**
	*@Column(type="string",length=255,nullable=true)
	****/
	protected $unlocked_reason;

    /**
    *@Column(type="string",length=255,nullable=false)
    ****/
    protected $aadhar_no;

    /**
    *@Column(type="string",length=255,nullable=true)
    ****/
    protected $pan_no;


    /**
    *@Column(type="integer")
    ****/
    protected $kyc_status = 0;


    /**
    *@Column(type="string",length=255,nullable=true)
    ****/
    protected $pin_store_password;

    /**
     *
     * @ORM OneToMany(targetEntity="Entity\Pin_request" mappedBy="pin_request_user_id")
     */
    private $pin_request_user_id;
    /**
     *
     * @ORM OneToMany(targetEntity="Entity\Pins" mappedBy="pin_created_user_id")
     */
    private $pin_created_user_id;

    /**
     *
     * @ORM OneToMany(targetEntity="Entity\Transactions" mappedBy="transaction_from_user_id")
     */
    private $transaction_from_user_id;
    /**
     *
     * @ORM OneToMany(targetEntity="Entity\Transactions" mappedBy="transaction_to_user_id")
     */
    private $transaction_to_user_id;
     /**
     *
     * @ORM OneToMany(targetEntity="Entity\User_account" mappedBy="user_id")
     */
    private $user_account_user_id;


    /**
     *
     * @ORM OneToMany(targetEntity="Entity\User_package_purchase" mappedBy="user_id")
     */
    private $user_package_purchase_user_id; 

    /**
     *
     * @ORM OneToMany(targetEntity="Entity\User_package_purchase" mappedBy="user_purchase_sponser_user_purchase_id")
     */
    private $user_purchase_sponser_user_purchase_id;
    

    /**
     *
     * @ORM OneToMany(targetEntity="Entity\User_package_purchase" mappedBy="user_id")
     */
    private $user_pck_id;

    /**
     *
     * @ORM OneToMany(targetEntity="Entity\User_access" mappedBy="user_access_user_id")
     */
    private $user_access_user_id;

    /**
     * @var datetime $created_at
     *
     * @Column(type="datetime")
     */
    protected $created_at;

    /**
     * @var datetime $updated_at
     * 
     * @Column(type="datetime", nullable = true)
     */
    protected $updated_at;


    /**
    *@Column(type="integer")
    ****/
    protected $pan_verified = 0;

    /**
    *@Column(type="integer")
    ****/
    protected $aadhar_verified = 0;
    /**
    *@Column(type="integer")
    ****/
    protected $pass_verified = 0;







    /**
     *
     * @ORM OneToMany(targetEntity="Entity\User_messages" mappedBy="user_messages_from_id")
     */
    private $user_messages_from_id;
    /**
     *
     * @ORM OneToMany(targetEntity="Entity\User_messages" mappedBy="user_messages_from_id")
     */
    private $user_messages_to_id;

     /**
     *
     * @ORM OneToMany(targetEntity="Entity\Payout_result" mappedBy="payout_result_user_id")
     */
    private $payout_result_user_id;





    /**
     * Gets the value of user_id.
     *
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Sets the value of user_id.
     *
     * @param mixed $user_id the user id
     *
     * @return self
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Gets the value of user_name.
     *
     * @return mixed
     */
    public function getUserName()
    {
        return $this->user_name;
    }

    /**
     * Sets the value of user_name.
     *
     * @param mixed $user_name the user name
     *
     * @return self
     */
    public function setUserName($user_name)
    {
        $this->user_name = $user_name;

        return $this;
    }

    /**
     * Gets the value of user_email.
     *
     * @return mixed
     */
    public function getUserEmail()
    {
        return $this->user_email;
    }

    /**
     * Sets the value of user_email.
     *
     * @param mixed $user_email the user email
     *
     * @return self
     */
    public function setUserEmail($user_email)
    {
        $this->user_email = $user_email;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getContactId()
    {
        return $this->contact_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $contact_id the contact id
     *
     * @return self
     */
    public function setContactId($contact_id)
    {
        $this->contact_id = $contact_id;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getPasswordId()
    {
        return $this->password_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $password_id the password id
     *
     * @return self
     */
    public function setPasswordId($password_id)
    {
        $this->password_id = $password_id;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getAddressId()
    {
        return $this->address_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $address_id the address id
     *
     * @return self
     */
    public function setAddressId($address_id)
    {
        $this->address_id = $address_id;

        return $this;
    }

    /**
     * Gets the value of first_name.
     *
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Sets the value of first_name.
     *
     * @param mixed $first_name the first name
     *
     * @return self
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;

        return $this;
    }

    /**
     * Gets the value of last_name.
     *
     * @return mixed
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Sets the value of last_name.
     *
     * @param mixed $last_name the last name
     *
     * @return self
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;

        return $this;
    }

    /**
     * Gets the value of gender.
     *
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Sets the value of gender.
     *
     * @param mixed $gender the gender
     *
     * @return self
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Gets the value of dob.
     *
     * @return mixed
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * Sets the value of dob.
     *
     * @param mixed $dob the dob
     *
     * @return self
     */
    public function setDob($dob)
    {
        $this->dob = $dob;

        return $this;
    }

    /**
     * Gets the value of profile_picture_url.
     *
     * @return mixed
     */
    public function getProfilePictureUrl()
    {
        return $this->profile_picture_url;
    }

    /**
     * Sets the value of profile_picture_url.
     *
     * @param mixed $profile_picture_url the profile picture url
     *
     * @return self
     */
    public function setProfilePictureUrl($profile_picture_url)
    {
        $this->profile_picture_url = $profile_picture_url;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getUserRoleId()
    {
        return $this->user_role_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $user_role_id the user role id
     *
     * @return self
     */
    public function setUserRoleId($user_role_id)
    {
        $this->user_role_id = $user_role_id;

        return $this;
    }

    /**
     * Gets the value of user_verification_url.
     *
     * @return mixed
     */
    public function getUserVerificationUrl()
    {
        return $this->user_verification_url;
    }

    /**
     * Sets the value of user_verification_url.
     *
     * @param mixed $user_verification_url the user verification url
     *
     * @return self
     */
    public function setUserVerificationUrl($user_verification_url)
    {
        $this->user_verification_url = $user_verification_url;

        return $this;
    }

    /**
     * Gets the value of user_email_verified.
     *
     * @return mixed
     */
    public function getUserEmailVerified()
    {
        return $this->user_email_verified;
    }

    /**
     * Sets the value of user_email_verified.
     *
     * @param mixed $user_email_verified the user email verified
     *
     * @return self
     */
    public function setUserEmailVerified($user_email_verified)
    {
        $this->user_email_verified = $user_email_verified;

        return $this;
    }

    /**
     * Gets the value of is_active.
     *
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * Sets the value of is_active.
     *
     * @param mixed $is_active the is active
     *
     * @return self
     */
    public function setIsActive($is_active)
    {
        $this->is_active = $is_active;

        return $this;
    }

    /**
     * Gets the value of last_login_time.
     *
     * @return mixed
     */
    public function getLastLoginTime()
    {
        return $this->last_login_time;
    }

    /**
     * Sets the value of last_login_time.
     *
     * @param mixed $last_login_time the last login time
     *
     * @return self
     */
    public function setLastLoginTime($last_login_time)
    {
        $this->last_login_time = $last_login_time;

        return $this;
    }

    /**
     * Gets the value of last_login_ip_address.
     *
     * @return mixed
     */
    public function getLastLoginIpAddress()
    {
        return $this->last_login_ip_address;
    }

    /**
     * Sets the value of last_login_ip_address.
     *
     * @param mixed $last_login_ip_address the last login ip address
     *
     * @return self
     */
    public function setLastLoginIpAddress($last_login_ip_address)
    {
        $this->last_login_ip_address = $last_login_ip_address;

        return $this;
    }

    /**
     * Gets the value of unlocked_reason.
     *
     * @return mixed
     */
    public function getUnlockedReason()
    {
        return $this->unlocked_reason;
    }

    /**
     * Sets the value of unlocked_reason.
     *
     * @param mixed $unlocked_reason the unlocked reason
     *
     * @return self
     */
    public function setUnlockedReason($unlocked_reason)
    {
        $this->unlocked_reason = $unlocked_reason;

        return $this;
    }

    /**
     * Gets the value of pin_request_user_id.
     *
     * @return mixed
     */
    public function getPinRequestUserId()
    {
        return $this->pin_request_user_id;
    }

    /**
     * Sets the value of pin_request_user_id.
     *
     * @param mixed $pin_request_user_id the pin request user id
     *
     * @return self
     */
    private function setPinRequestUserId($pin_request_user_id)
    {
        $this->pin_request_user_id = $pin_request_user_id;

        return $this;
    }

    /**
     * Gets the value of pin_created_user_id.
     *
     * @return mixed
     */
    public function getPinCreatedUserId()
    {
        return $this->pin_created_user_id;
    }

    /**
     * Sets the value of pin_created_user_id.
     *
     * @param mixed $pin_created_user_id the pin created user id
     *
     * @return self
     */
    private function setPinCreatedUserId($pin_created_user_id)
    {
        $this->pin_created_user_id = $pin_created_user_id;

        return $this;
    }

    /**
     * Gets the value of transaction_from_user_id.
     *
     * @return mixed
     */
    public function getTransactionFromUserId()
    {
        return $this->transaction_from_user_id;
    }

    /**
     * Sets the value of transaction_from_user_id.
     *
     * @param mixed $transaction_from_user_id the transaction from user id
     *
     * @return self
     */
    private function setTransactionFromUserId($transaction_from_user_id)
    {
        $this->transaction_from_user_id = $transaction_from_user_id;

        return $this;
    }

    /**
     * Gets the value of transaction_to_user_id.
     *
     * @return mixed
     */
    public function getTransactionToUserId()
    {
        return $this->transaction_to_user_id;
    }

    /**
     * Sets the value of transaction_to_user_id.
     *
     * @param mixed $transaction_to_user_id the transaction to user id
     *
     * @return self
     */
    private function setTransactionToUserId($transaction_to_user_id)
    {
        $this->transaction_to_user_id = $transaction_to_user_id;

        return $this;
    }

    /**
     * Gets the value of user_account_user_id.
     *
     * @return mixed
     */
    public function getUserAccountUserId()
    {
        return $this->user_account_user_id;
    }

    /**
     * Sets the value of user_account_user_id.
     *
     * @param mixed $user_account_user_id the user account user id
     *
     * @return self
     */
    private function setUserAccountUserId($user_account_user_id)
    {
        $this->user_account_user_id = $user_account_user_id;

        return $this;
    }

    /**
     * Gets the value of user_package_purchase_user_id.
     *
     * @return mixed
     */
    public function getUserPackagePurchaseUserId()
    {
        return $this->user_package_purchase_user_id;
    }

    /**
     * Sets the value of user_package_purchase_user_id.
     *
     * @param mixed $user_package_purchase_user_id the user package purchase user id
     *
     * @return self
     */
    private function setUserPackagePurchaseUserId($user_package_purchase_user_id)
    {
        $this->user_package_purchase_user_id = $user_package_purchase_user_id;

        return $this;
    }

    /**
     * Gets the value of user_purchase_sponser_user_purchase_id.
     *
     * @return mixed
     */
    public function getUserPurchaseSponserUserPurchaseId()
    {
        return $this->user_purchase_sponser_user_purchase_id;
    }

    /**
     * Sets the value of user_purchase_sponser_user_purchase_id.
     *
     * @param mixed $user_purchase_sponser_user_purchase_id the user purchase sponser user purchase id
     *
     * @return self
     */
    private function setUserPurchaseSponserUserPurchaseId($user_purchase_sponser_user_purchase_id)
    {
        $this->user_purchase_sponser_user_purchase_id = $user_purchase_sponser_user_purchase_id;

        return $this;
    }

    /**
     * Gets the value of user_pck_id.
     *
     * @return mixed
     */
    public function getUserPckId()
    {
        return $this->user_pck_id;
    }

    /**
     * Sets the value of user_pck_id.
     *
     * @param mixed $user_pck_id the user pck id
     *
     * @return self
     */
    private function setUserPckId($user_pck_id)
    {
        $this->user_pck_id = $user_pck_id;

        return $this;
    }

    /**
     * Gets the value of user_messages_from_id.
     *
     * @return mixed
     */
    public function getUserMessagesFromId()
    {
        return $this->user_messages_from_id;
    }

    /**
     * Sets the value of user_messages_from_id.
     *
     * @param mixed $user_messages_from_id the user messages from id
     *
     * @return self
     */
    private function setUserMessagesFromId($user_messages_from_id)
    {
        $this->user_messages_from_id = $user_messages_from_id;

        return $this;
    }

    /**
     * Gets the value of user_messages_to_id.
     *
     * @return mixed
     */
    public function getUserMessagesToId()
    {
        return $this->user_messages_to_id;
    }

    /**
     * Sets the value of user_messages_to_id.
     *
     * @param mixed $user_messages_to_id the user messages to id
     *
     * @return self
     */
    private function setUserMessagesToId($user_messages_to_id)
    {
        $this->user_messages_to_id = $user_messages_to_id;

        return $this;
    }

    /**
     * Gets the value of payout_result_user_id.
     *
     * @return mixed
     */
    public function getPayoutResultUserId()
    {
        return $this->payout_result_user_id;
    }

    

    /**
     * Gets the value of user_access_user_id.
     *
     * @return mixed
     */
    public function getUserAccessUserId()
    {
        return $this->user_access_user_id;
    }

    /**
     * Sets the value of user_access_user_id.
     *
     * @param mixed $user_access_user_id the user access user id
     *
     * @return self
     */
    private function setUserAccessUserId($user_access_user_id)
    {
        $this->user_access_user_id = $user_access_user_id;

        return $this;
    }

    /**
     * Gets the value of account_validity_time.
     *
     * @return mixed
     */
    public function getAccountValidityTime()
    {
        return $this->account_validity_time;
    }

    /**
     * Sets the value of account_validity_time.
     *
     * @param mixed $account_validity_time the account validity time
     *
     * @return self
     */
    public function setAccountValidityTime($account_validity_time)
    {
        $this->account_validity_time = $account_validity_time;

        return $this;
    }

    /**
     * Gets the value of invalid_login_attempts.
     *
     * @return mixed
     */
    public function getInvalidLoginAttempts()
    {
        return $this->invalid_login_attempts;
    }

    /**
     * Sets the value of invalid_login_attempts.
     *
     * @param mixed $invalid_login_attempts the invalid login attempts
     *
     * @return self
     */
    public function setInvalidLoginAttempts($invalid_login_attempts)
    {
        $this->invalid_login_attempts = $invalid_login_attempts;

        return $this;
    }

    /**
     * Gets the value of last_invalid_login_time.
     *
     * @return mixed
     */
    public function getLastInvalidLoginTime()
    {
        return $this->last_invalid_login_time;
    }

    /**
     * Sets the value of last_invalid_login_time.
     *
     * @param mixed $last_invalid_login_time the last invalid login time
     *
     * @return self
     */
    public function setLastInvalidLoginTime($last_invalid_login_time)
    {
        $this->last_invalid_login_time = $last_invalid_login_time;

        return $this;
    }

    /**
     * Gets the value of last_invalid_login_ip_address.
     *
     * @return mixed
     */
    public function getLastInvalidLoginIpAddress()
    {
        return $this->last_invalid_login_ip_address;
    }

    /**
     * Sets the value of last_invalid_login_ip_address.
     *
     * @param mixed $last_invalid_login_ip_address the last invalid login ip address
     *
     * @return self
     */
    public function setLastInvalidLoginIpAddress($last_invalid_login_ip_address)
    {
        $this->last_invalid_login_ip_address = $last_invalid_login_ip_address;

        return $this;
    }

   

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getNomineeId()
    {
        return $this->nominee_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $nominee_id the nominee id
     *
     * @return self
     */
    public function setNomineeId($nominee_id)
    {
        $this->nominee_id = $nominee_id;

        return $this;
    }

    /**
     * Gets the value of aadhar_no.
     *
     * @return mixed
     */
    public function getAadharNo()
    {
        return $this->aadhar_no;
    }

    /**
     * Sets the value of aadhar_no.
     *
     * @param mixed $aadhar_no the aadhar no
     *
     * @return self
     */
    public function setAadharNo($aadhar_no)
    {
        $this->aadhar_no = $aadhar_no;

        return $this;
    }

    /**
     * Gets the value of pan_no.
     *
     * @return mixed
     */
    public function getPanNo()
    {
        return $this->pan_no;
    }

    /**
     * Sets the value of pan_no.
     *
     * @param mixed $pan_no the pan no
     *
     * @return self
     */
    public function setPanNo($pan_no)
    {
        $this->pan_no = $pan_no;

        return $this;
    }
      /**
     * Gets triggered only on insert

     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_at = new \DateTime("now");
    }


    public function getCreatedAt()
    {
        return $this->created_at;
    }



    /**
     * Gets triggered every time on update

     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_at = new \DateTime("now");
    }

     public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @return mixed
     */
    public function getPinStorePassword()
    {
        return $this->pin_store_password;
    }

    /**
     * @param mixed $pin_store_password
     *
     * @return self
     */
    public function setPinStorePassword($pin_store_password)
    {
        $this->pin_store_password = $pin_store_password;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getKycStatus()
    {
        return $this->kyc_status;
    }

    /**
     * @param mixed $pin_store_password
     *
     * @return self
     */
    public function setKycStatus($kyc_status)
    {
        $this->kyc_status = $kyc_status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassVerified()
    {
        return $this->pass_verified;
    }

    /**
     * @param mixed $pass_verified
     *
     * @return self
     */
    public function setPassVerified($pass_verified)
    {
        $this->pass_verified = $pass_verified;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAadharVerified()
    {
        return $this->aadhar_verified;
    }

    /**
     * @param mixed $aadhar_verified
     *
     * @return self
     */
    public function setAadharVerified($aadhar_verified)
    {
        $this->aadhar_verified = $aadhar_verified;

        return $this;
    }

    /**
     * @param mixed $payout_result_user_id
     *
     * @return self
     */
    public function setPayoutResultUserId($payout_result_user_id)
    {
        $this->payout_result_user_id = $payout_result_user_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPanVerified()
    {
        return $this->pan_verified;
    }

    /**
     * @param mixed $pan_verified
     *
     * @return self
     */
    public function setPanVerified($pan_verified)
    {
        $this->pan_verified = $pan_verified;

        return $this;
    }
}
?>