<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* Site_updation Model
* @Entity
* @Table(name="Site_updation")
* 
*/
class Site_updation{
  /**
  *
  * @Id
  * @Column(name="updation_id",type="integer",nullable=false)
  * @GeneratedValue(strategy="AUTO")
  */
  protected $updation_id;
  /**
  *@Column(type="string",length=255,nullable=false)
  ****/
  protected $message;  
  /**
  *@Column(type="integer",options={"default" : 0})
  ****/
  protected $is_active; 
  /**
   * @var datetime $created_at
   * @Column(type="datetime")
   */
  protected $created_at;

  /**
   * @var datetime $updated_at
   * @Column(type="datetime", nullable = true)
   */
  protected $updated_at;
  /**
   * @return mixed
   */
  public function getUpdationId()
  {
      return $this->updation_id;
  }

  /**
   * @param mixed $updation_id
   *
   * @return self
   */
  public function setUpdationId($updation_id)
  {
      $this->updation_id = $updation_id;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getMessage()
  {
      return $this->message;
  }

  /**
   * @param mixed $message
   *
   * @return self
   */
  public function setMessage($message)
  {
      $this->message = $message;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getIsActive()
  {
      return $this->is_active;
  }

  /**
   * @param mixed $is_active
   *
   * @return self
   */
  public function setIsActive($is_active)
  {
      $this->is_active = $is_active;

      return $this;
  }
  /**
   * Gets triggered only on insert

   * @ORM\PrePersist
   */
  public function onPrePersist()
  {
      $this->created_at = new \DateTime("now");
  }
  public function getCreatedAt()
  {
      return $this->created_at;
  }
  /**
   * Gets triggered every time on update

   * @ORM\PreUpdate
   */
  public function onPreUpdate()
  {
      $this->updated_at = new \DateTime("now");
  }
}
?>