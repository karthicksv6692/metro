<?php 
  namespace Entity;
  use Doctrine\ORM\Mapping as ORM;
  use Doctrine\Common\Collections\Collection;
  use Doctrine\Common\Collections\ArrayCollection;
  use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
  
  class Payout_details_view{
    
    protected $package_name;
    protected $personal_sales_count;
    protected $personal_sales_side;

    protected $previous_left;
    protected $previous_right;

    protected $current_left;
    protected $current_right;

    protected $total_left;
    protected $total_right;

    protected $carry_left;
    protected $carry_right;

    protected $pair_details_total;
    protected $pair_details_flushed;

    protected $total_pv_left;
    protected $total_pv_right;

    protected $whole_left;
    protected $whole_right;


    protected $carry_pv_left;
    protected $carry_pv_right;


  
    /**
     * @return mixed
     */
    public function getPackageName()
    {
        return $this->package_name;
    }

    /**
     * @param mixed $package_name
     *
     * @return self
     */
    public function setPackageName($package_name)
    {
        $this->package_name = $package_name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPersonalSalesCount()
    {
        return $this->personal_sales_count;
    }

    /**
     * @param mixed $personal_sales_count
     *
     * @return self
     */
    public function setPersonalSalesCount($personal_sales_count)
    {
        $this->personal_sales_count = $personal_sales_count;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPersonalSalesSide()
    {
        return $this->personal_sales_side;
    }

    /**
     * @param mixed $personal_sales_side
     *
     * @return self
     */
    public function setPersonalSalesSide($personal_sales_side)
    {
        $this->personal_sales_side = $personal_sales_side;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPreviousLeft()
    {
        return $this->previous_left;
    }

    /**
     * @param mixed $previous_left
     *
     * @return self
     */
    public function setPreviousLeft($previous_left)
    {
        $this->previous_left = $previous_left;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPreviousRight()
    {
        return $this->previous_right;
    }

    /**
     * @param mixed $previous_right
     *
     * @return self
     */
    public function setPreviousRight($previous_right)
    {
        $this->previous_right = $previous_right;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrentLeft()
    {
        return $this->current_left;
    }

    /**
     * @param mixed $current_left
     *
     * @return self
     */
    public function setCurrentLeft($current_left)
    {
        $this->current_left = $current_left;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrentRight()
    {
        return $this->current_right;
    }

    /**
     * @param mixed $current_right
     *
     * @return self
     */
    public function setCurrentRight($current_right)
    {
        $this->current_right = $current_right;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalLeft()
    {
        return $this->total_left;
    }

    /**
     * @param mixed $total_left
     *
     * @return self
     */
    public function setTotalLeft($total_left)
    {
        $this->total_left = $total_left;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalRight()
    {
        return $this->total_right;
    }

    /**
     * @param mixed $total_right
     *
     * @return self
     */
    public function setTotalRight($total_right)
    {
        $this->total_right = $total_right;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCarryLeft()
    {
        return $this->carry_left;
    }

    /**
     * @param mixed $carry_left
     *
     * @return self
     */
    public function setCarryLeft($carry_left)
    {
        $this->carry_left = $carry_left;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCarryRight()
    {
        return $this->carry_right;
    }

    /**
     * @param mixed $carry_right
     *
     * @return self
     */
    public function setCarryRight($carry_right)
    {
        $this->carry_right = $carry_right;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPairDetailsTotal()
    {
        return $this->pair_details_total;
    }

    /**
     * @param mixed $pair_details_total
     *
     * @return self
     */
    public function setPairDetailsTotal($pair_details_total)
    {
        $this->pair_details_total = $pair_details_total;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPairDetailsFlushed()
    {
        return $this->pair_details_flushed;
    }

    /**
     * @param mixed $pair_details_flushed
     *
     * @return self
     */
    public function setPairDetailsFlushed($pair_details_flushed)
    {
        $this->pair_details_flushed = $pair_details_flushed;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalPvLeft()
    {
        return $this->total_pv_left;
    }

    /**
     * @param mixed $total_pv_left
     *
     * @return self
     */
    public function setTotalPvLeft($total_pv_left)
    {
        $this->total_pv_left = $total_pv_left;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalPvRight()
    {
        return $this->total_pv_right;
    }

    /**
     * @param mixed $total_pv_right
     *
     * @return self
     */
    public function setTotalPvRight($total_pv_right)
    {
        $this->total_pv_right = $total_pv_right;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWholeLeft()
    {
        return $this->whole_left;
    }

    /**
     * @param mixed $whole_left
     *
     * @return self
     */
    public function setWholeLeft($whole_left)
    {
        $this->whole_left = $whole_left;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWholeRight()
    {
        return $this->whole_right;
    }

    /**
     * @param mixed $whole_right
     *
     * @return self
     */
    public function setWholeRight($whole_right)
    {
        $this->whole_right = $whole_right;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCarryPvLeft()
    {
        return $this->carry_pv_left;
    }

    /**
     * @param mixed $carry_pv_left
     *
     * @return self
     */
    public function setCarryPvLeft($carry_pv_left)
    {
        $this->carry_pv_left = $carry_pv_left;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCarryPvRight()
    {
        return $this->carry_pv_right;
    }

    /**
     * @param mixed $carry_pv_right
     *
     * @return self
     */
    public function setCarryPvRight($carry_pv_right)
    {
        $this->carry_pv_right = $carry_pv_right;

        return $this;
    }
}

?>