<?php 
  namespace Entity;
  use Doctrine\ORM\Mapping as ORM;
  use Doctrine\Common\Collections\Collection;
  use Doctrine\Common\Collections\ArrayCollection;
  use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
  /**
  * @Entity
  * @Table(name="Product_sales")
  */
  class Product_sales{
    /**
    *
    * @Id
    * @Column(name="product_sales_id",type="integer",nullable=false)
    * @GeneratedValue(strategy="AUTO")
    */
    protected $product_sales_id;

    /**
    *@Column(type="integer")
    ****/
    protected $user_id;
    /**
    *@Column(type="integer")
    ****/
    protected $pin_no;

    /**
    *@Column(type="string",length=255,nullable=false)
    ****/
    protected $product_ids;

    /**
    *@Column(type="integer")
    ****/
    protected $payment_mode;

    /**
    *@Column(type="string",length=255,nullable=true)
    ****/
    protected $payment_id;

    /**
    *@Column(type="integer")
    ****/
    protected $payment_amount;
  
    /**
     * @return mixed
     */
    public function getProductSalesId()
    {
        return $this->product_sales_id;
    }

    /**
     * @param mixed $product_sales_id
     *
     * @return self
     */
    public function setProductSalesId($product_sales_id)
    {
        $this->product_sales_id = $product_sales_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     *
     * @return self
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPinNo()
    {
        return $this->pin_no;
    }

    /**
     * @param mixed $pin_no
     *
     * @return self
     */
    public function setPinNo($pin_no)
    {
        $this->pin_no = $pin_no;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProductIds()
    {
        return $this->product_ids;
    }

    /**
     * @param mixed $product_ids
     *
     * @return self
     */
    public function setProductIds($product_ids)
    {
        $this->product_ids = $product_ids;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentMode()
    {
        return $this->payment_mode;
    }

    /**
     * @param mixed $payment_mode
     *
     * @return self
     */
    public function setPaymentMode($payment_mode)
    {
        $this->payment_mode = $payment_mode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentId()
    {
        return $this->payment_id;
    }

    /**
     * @param mixed $payment_id
     *
     * @return self
     */
    public function setPaymentId($payment_id)
    {
        $this->payment_id = $payment_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentAmount()
    {
        return $this->payment_amount;
    }

    /**
     * @param mixed $payment_amount
     *
     * @return self
     */
    public function setPaymentAmount($payment_amount)
    {
        $this->payment_amount = $payment_amount;

        return $this;
    }
}
?>