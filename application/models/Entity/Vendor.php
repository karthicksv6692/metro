<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Vendor")
*/
class Vendor{

	/**
	*
	* @Id
	* @Column(name="vendor_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $vendor_id;

	/**
	*@Column(type="string",length=255,nullable=false,unique=true)
	****/
	protected $vendor_name;

	/**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Address", fetch="EAGER", cascade="persist")
     * @JoinColumn(name="vendor_address_id", referencedColumnName="address_id", onDelete="SET NULL")
     */
	protected $vendor_address_id;

	/**
	*@Column(type="integer")
	****/
	protected $vendor_tin_no;

    /**
    *@Column(type="string",length=255,nullable=false)
    ****/
    protected $vendor_pan_no;

	/**
	*@Column(type="string",length=255,nullable=false)
	****/
	protected $vendor_gst_no;

    /**
     *
     * @ORM OneToMany(targetEntity="Entity\Purchase_request" mappedBy="purchase_request_vendor_id")
     */
    private $purchase_request_vendor_id;

    /**
     *
     * @ORM OneToMany(targetEntity="Entity\Product" mappedBy="product_vendor_id")
     */
    private $product_vendor_id;

     /**
     *
     * @ORM OneToMany(targetEntity="Entity\Product" mappedBy="product_vendor_id_id")
     */
    private $product_vendor_id_id;

	 /**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Contact")
     * @JoinColumn(name="vendor_contact_id", referencedColumnName="contact_id")
     */
    protected $vendor_contact_id;

    /**
     * Gets the value of vendor_id.
     *
     * @return mixed
     */
    public function getVendorId()
    {
        return $this->vendor_id;
    }

    /**
     * Sets the value of vendor_id.
     *
     * @param mixed $vendor_id the vendor id
     *
     * @return self
     */
    public function setVendorId($vendor_id)
    {
        $this->vendor_id = $vendor_id;

        return $this;
    }

    /**
     * Gets the value of vendor_name.
     *
     * @return mixed
     */
    public function getVendorName()
    {
        return $this->vendor_name;
    }

    /**
     * Sets the value of vendor_name.
     *
     * @param mixed $vendor_name the vendor name
     *
     * @return self
     */
    public function setVendorName($vendor_name)
    {
        $this->vendor_name = $vendor_name;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getVendorAddressId()
    {
        return $this->vendor_address_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $vendor_address_id the vendor address id
     *
     * @return self
     */
    public function setVendorAddressId($vendor_address_id)
    {
        $this->vendor_address_id = $vendor_address_id;

        return $this;
    }

    /**
     * Gets the value of vendor_tin_no.
     *
     * @return mixed
     */
    public function getVendorTinNo()
    {
        return $this->vendor_tin_no;
    }

    /**
     * Sets the value of vendor_tin_no.
     *
     * @param mixed $vendor_tin_no the vendor tin no
     *
     * @return self
     */
    public function setVendorTinNo($vendor_tin_no)
    {
        $this->vendor_tin_no = $vendor_tin_no;

        return $this;
    }

    /**
     * Gets the value of vendor_pan_no.
     *
     * @return mixed
     */
    public function getVendorPanNo()
    {
        return $this->vendor_pan_no;
    }

    /**
     * Sets the value of vendor_pan_no.
     *
     * @param mixed $vendor_pan_no the vendor pan no
     *
     * @return self
     */
    public function setVendorPanNo($vendor_pan_no)
    {
        $this->vendor_pan_no = $vendor_pan_no;

        return $this;
    }

    /**
     * Gets the value of purchase_request_vendor_id.
     *
     * @return mixed
     */
    public function getPurchaseRequestVendorId()
    {
        return $this->purchase_request_vendor_id;
    }

    /**
     * Sets the value of purchase_request_vendor_id.
     *
     * @param mixed $purchase_request_vendor_id the purchase request vendor id
     *
     * @return self
     */
    private function setPurchaseRequestVendorId($purchase_request_vendor_id)
    {
        $this->purchase_request_vendor_id = $purchase_request_vendor_id;

        return $this;
    }

    /**
     * Gets the value of product_vendor_id.
     *
     * @return mixed
     */
    public function getProductVendorId()
    {
        return $this->product_vendor_id;
    }

    /**
     * Sets the value of product_vendor_id.
     *
     * @param mixed $product_vendor_id the product vendor id
     *
     * @return self
     */
    private function setProductVendorId($product_vendor_id)
    {
        $this->product_vendor_id = $product_vendor_id;

        return $this;
    }

    /**
     * Gets the value of product_vendor_id_id.
     *
     * @return mixed
     */
    public function getProductVendorIdId()
    {
        return $this->product_vendor_id_id;
    }

    /**
     * Sets the value of product_vendor_id_id.
     *
     * @param mixed $product_vendor_id_id the product vendor id id
     *
     * @return self
     */
    public function setProductVendorIdId($product_vendor_id_id)
    {
        $this->product_vendor_id_id = $product_vendor_id_id;

        return $this;
    }

    /**
     * Sets the value of product_vendor_id_id.
     *
     * @param mixed $product_vendor_id_id the product vendor id id
     *
     * @return self
     */
   

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getVendorContactId()
    {
        return $this->vendor_contact_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $vendor_contact_id the vendor contact id
     *
     * @return self
     */
    public function setVendorContactId($vendor_contact_id)
    {
        $this->vendor_contact_id = $vendor_contact_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getVendorGstNo()
    {
        return $this->vendor_gst_no;
    }

    /**
     * @param mixed $vendor_gst_no
     *
     * @return self
     */
    public function setVendorGstNo($vendor_gst_no)
    {
        $this->vendor_gst_no = $vendor_gst_no;

        return $this;
    }
}

?>