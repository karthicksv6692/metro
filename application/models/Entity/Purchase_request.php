<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Purchase_request")
*/
class Purchase_request{

	/**
	*
	* @Id
	* @Column(name="purchase_request_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $purchase_request_id;

	/**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Vendor", fetch="EAGER")
     * @JoinColumn(name="purchase_request_vendor_id", referencedColumnName="vendor_id")
     */
	protected $purchase_request_vendor_id;


	/**
	*@Column(type="string",nullable=true)
	****/
	protected $purchase_request_no;

	/**
	*@Column(type="datetime",nullable=true)
	****/
	protected $purchase_request_estimated_delivery_date;

    /**
    *@Column(type="integer")
    ****/
    protected $purchase_request_status;	

    /**
    *@Column(type="integer")
    ****/
    protected $purchase_type;

    /**
    *@Column(type="integer")
    ****/
    protected $purchase_request_bill_or_request_id;
    /**
    *@Column(type="string",nullable=true)
    ****/
    protected $approved_user_id;

    /**
	*@Column(type="text",nullable=true)
	****/
	protected $reson_for;

    /**
     * @var datetime $created_at
     *
     * @Column(type="datetime")
     */
    protected $created_at;

    /**
     * @var datetime $updated_at
     * 
     * @Column(type="datetime", nullable = true)
     */
    protected $updated_at;

   

    /**
     * Gets triggered only on insert

     * @ORM\PrePersist
     */
        public function onPrePersist()
        {
            $this->created_at = new \DateTime("now");
        }


        public function getCreatedAt()
        {
            return $this->created_at;
        }

        /**
         * Gets triggered every time on update

         * @ORM\PreUpdate
         */
        public function onPreUpdate()
        {
            $this->updated_at = new \DateTime("now");
        }

   

    /**
     * @return mixed
     */
    public function getPurchaseRequestId()
    {
        return $this->purchase_request_id;
    }

    /**
     * @param mixed $purchase_request_id
     *
     * @return self
     */
    public function setPurchaseRequestId($purchase_request_id)
    {
        $this->purchase_request_id = $purchase_request_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPurchaseRequestVendorId()
    {
        return $this->purchase_request_vendor_id;
    }

    /**
     * @param mixed $purchase_request_vendor_id
     *
     * @return self
     */
    public function setPurchaseRequestVendorId($purchase_request_vendor_id)
    {
        $this->purchase_request_vendor_id = $purchase_request_vendor_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPurchaseRequestNo()
    {
        return $this->purchase_request_no;
    }

    /**
     * @param mixed $purchase_request_no
     *
     * @return self
     */
    public function setPurchaseRequestNo($purchase_request_no)
    {
        $this->purchase_request_no = $purchase_request_no;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPurchaseRequestEstimatedDeliveryDate()
    {
        return $this->purchase_request_estimated_delivery_date;
    }

    /**
     * @param mixed $purchase_request_estimated_delivery_date
     *
     * @return self
     */
    public function setPurchaseRequestEstimatedDeliveryDate($purchase_request_estimated_delivery_date)
    {
        $this->purchase_request_estimated_delivery_date = $purchase_request_estimated_delivery_date;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPurchaseRequestStatus()
    {
        return $this->purchase_request_status;
    }

    /**
     * @param mixed $purchase_request_status
     *
     * @return self
     */
    public function setPurchaseRequestStatus($purchase_request_status)
    {
        $this->purchase_request_status = $purchase_request_status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPurchaseType()
    {
        return $this->purchase_type;
    }

    /**
     * @param mixed $purchase_type
     *
     * @return self
     */
    public function setPurchaseType($purchase_type)
    {
        $this->purchase_type = $purchase_type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPurchaseRequestBillOrRequestId()
    {
        return $this->purchase_request_bill_or_request_id;
    }

    /**
     * @param mixed $purchase_request_bill_or_request_id
     *
     * @return self
     */
    public function setPurchaseRequestBillOrRequestId($purchase_request_bill_or_request_id)
    {
        $this->purchase_request_bill_or_request_id = $purchase_request_bill_or_request_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getApprovedUserId()
    {
        return $this->approved_user_id;
    }

    /**
     * @param mixed $approved_user_id
     *
     * @return self
     */
    public function setApprovedUserId($approved_user_id)
    {
        $this->approved_user_id = $approved_user_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getResonFor()
    {
        return $this->reson_for;
    }

    /**
     * @param mixed $reson_for
     *
     * @return self
     */
    public function setResonFor($reson_for)
    {
        $this->reson_for = $reson_for;

        return $this;
    }


    /**
     * @param datetime $created_at $created_at
     *
     * @return self
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return datetime $updated_at
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param datetime $updated_at $updated_at
     *
     * @return self
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }
}

?>