<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="User_attachments")
*/
class User_attachments{
	/**
	*
	* @Id
	* @Column(name="user_attachment_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $user_attachment_id;
	/**
	*@Column(type="string",length=255,nullable=false)
	****/
	protected $user_attachment_name;

	/**
	*@Column(type="string",length=255,nullable=false)
	****/
	protected $user_attachment_url;

}

?>