<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Stock_pointer")
*/
class Stock_pointer{

  /**
  *
  * @Id
  * @Column(name="sp_id",type="integer",nullable=false)
  * @GeneratedValue(strategy="AUTO")
  */
  protected $sp_id;

  /**
   * Many Features have One Product.
   * @ManyToOne(targetEntity="Entity\User", fetch="EAGER")
   * @JoinColumn(name="user_id", referencedColumnName="user_id")
   */
  protected $user_id;

  /**
  *@Column(type="integer",)
  ****/
  protected $sp_amount = 0;

 /**
   * @var datetime $created_at
   *
   * @Column(type="datetime",nullable = true)
   */
  protected $created_at;

  /**
   * @var datetime $updated_at
   * 
   * @Column(type="datetime", nullable = true)
   */
  protected $updated_at;


  /**
   * @return mixed
   */
  public function getSpId()
  {
      return $this->sp_id;
  }

  /**
   * @param mixed $sp_id
   *
   * @return self
   */
  public function setSpId($sp_id)
  {
      $this->sp_id = $sp_id;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getUserId()
  {
      return $this->user_id;
  }

  /**
   * @param mixed $user_id
   *
   * @return self
   */
  public function setUserId($user_id)
  {
      $this->user_id = $user_id;

      return $this;
  }

  /**
   * @return mixed
   */
  public function getSpAmount()
  {
      return $this->sp_amount;
  }

  /**
   * @param mixed $sp_amount
   *
   * @return self
   */
  public function setSpAmount($sp_amount)
  {
      $this->sp_amount = $sp_amount;

      return $this;
  }
  /**
   * Gets triggered only on insert

   * @ORM\PrePersist
   */
  public function onPrePersist()
  {
      $this->created_at = new \DateTime("now");
  }

  public function getCreatedAt()
  {
      return $this->created_at;
  }

  /**
   * Gets triggered every time on update

   * @ORM\PreUpdate
   */
  public function onPreUpdate()
  {
      $this->updated_at = new \DateTime("now");
  }
}

?>