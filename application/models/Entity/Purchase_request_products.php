<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Purchase_request_products")
*/
class Purchase_request_products{

	/**
	*
	* @Id
	* @Column(name="purchase_request_product_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $purchase_request_product_id;


      /**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Product", fetch="EAGER")
     * @JoinColumn(name="purchase_request_product_product_id", referencedColumnName="product_id")
     */
    private $purchase_request_product_product_id;

    /**
    *
    * @Column(type="integer")
    */
    private $purchase_request_product_product_quantity;


  	/**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Purchase_request", fetch="EAGER")
     * @JoinColumn(name="purchase_request_id", referencedColumnName="purchase_request_id")
     */
    private $purchase_request_id;

    /**
    *
    * @Column(type="integer")
    */
    private $ppq_price;


    /**
     * Gets the value of purchase_request_product_id.
     *
     * @return mixed
     */
    public function getPurchaseRequestProductId()
    {
        return $this->purchase_request_product_id;
    }

    /**
     * Sets the value of purchase_request_product_id.
     *
     * @param mixed $purchase_request_product_id the purchase request product id
     *
     * @return self
     */
    public function setPurchaseRequestProductId($purchase_request_product_id)
    {
        $this->purchase_request_product_id = $purchase_request_product_id;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getPurchaseRequestProductProductId()
    {
        return $this->purchase_request_product_product_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $purchase_request_product_product_id the purchase request product product id
     *
     * @return self
     */
    public function setPurchaseRequestProductProductId($purchase_request_product_product_id)
    {
        $this->purchase_request_product_product_id = $purchase_request_product_product_id;

        return $this;
    }

    /**
     * Gets the value of purchase_request_product_product_quantity.
     *
     * @return mixed
     */
    public function getPurchaseRequestProductProductQuantity()
    {
        return $this->purchase_request_product_product_quantity;
    }

    /**
     * Sets the value of purchase_request_product_product_quantity.
     *
     * @param mixed $purchase_request_product_product_quantity the purchase request product product quantity
     *
     * @return self
     */
    public function setPurchaseRequestProductProductQuantity($purchase_request_product_product_quantity)
    {
        $this->purchase_request_product_product_quantity = $purchase_request_product_product_quantity;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getPurchaseRequestId()
    {
        return $this->purchase_request_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $purchase_request_id the purchase request id
     *
     * @return self
     */
    public function setPurchaseRequestId($purchase_request_id)
    {
        $this->purchase_request_id = $purchase_request_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPpqPrice()
    {
        return $this->ppq_price;
    }

    /**
     * @param mixed $ppq_price
     *
     * @return self
     */
    public function setPpqPrice($ppq_price)
    {
        $this->ppq_price = $ppq_price;

        return $this;
    }
}

?>