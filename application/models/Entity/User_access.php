<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

/**
* @Entity
* @Table(name="User_access")
*/
class User_access{

	/**
	*
	* @Id
	* @Column(name="user_access_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $user_access_id;


	/**
    *@Column(type="integer")
    ****/
	protected $user_access_user_id;
	/**
    *@Column(type="string",length=255)
    ****/
	protected $user_access_access_id;


    /**
     * Gets the value of user_access_id.
     *
     * @return mixed
     */
    public function getUserAccessId()
    {
        return $this->user_access_id;
    }

    /**
     * Sets the value of user_access_id.
     *
     * @param mixed $user_access_id the user access id
     *
     * @return self
     */
    public function setUserAccessId($user_access_id)
    {
        $this->user_access_id = $user_access_id;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getUserAccessUserId()
    {
        return $this->user_access_user_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $user_access_user_id the user access user id
     *
     * @return self
     */
    public function setUserAccessUserId($user_access_user_id)
    {
        $this->user_access_user_id = $user_access_user_id;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getUserAccessAccessId()
    {
        return $this->user_access_access_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $user_access_access_id the user access access id
     *
     * @return self
     */
    public function setUserAccessAccessId($user_access_access_id)
    {
        $this->user_access_access_id = $user_access_access_id;

        return $this;
    }
}


?>