<?php 
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Purchase_bill_products")
*/
class Purchase_bill_products{

	/**
	*
	* @Id
	* @Column(name="purchase_bill_product_id",type="integer",nullable=false)
	* @GeneratedValue(strategy="AUTO")
	*/
	protected $purchase_bill_product_id;

	/**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Product", fetch="EAGER")
     * @JoinColumn(name="purchase_bill_product_product_id", referencedColumnName="product_id")
     */
    protected $purchase_bill_product_product_id;

    /**
	*@Column(type="integer")
	****/
	protected $purchase_bill_product_product_quantity;

}
?>