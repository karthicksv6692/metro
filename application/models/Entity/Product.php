<?php
namespace Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
/**
* @Entity
* @Table(name="Product")
*/
class Product
{
    /**
    *
    * @Id
    * @Column(name="product_id",type="integer",nullable=false)
    * @GeneratedValue(strategy="AUTO")
    */
    private $product_id;

    /**
    *
    * @Column(name="product_code",type="string",length=255,unique=true,nullable=false)
    */
    private $product_code;

    /**
    *@Column(type="string",length=255,unique=true,nullable=false)
    *
    *
    **/
    protected $product_name;

     /**
     * @var string
     *
     * @Column(name="product_short_desc", type="string", length=255)
     */
    private $product_short_desc;

     /**
     * @var string
     *
     * @Column(name="product_desc", type="string", length=255)
     */
    private $product_desc;
    /**
    *
    * @Column(name="product_quantity",type="integer")
    */
    private $product_quantity;

    
    /**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Vendor", fetch="EAGER")
     * @JoinColumn(name="product_vendor_id_id", referencedColumnName="vendor_id")
     */
    private $product_vendor_id_id;


    /**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Entity\Category", fetch="EAGER", cascade="persist")
     * @JoinColumn(name="category_id", referencedColumnName="category_id",onDelete="SET NULL",nullable=true)
     */
    private $category_id;

    /**
    *
    * @Column(name="product_price",type="integer",nullable=false)
    */
    private $product_price;

    /**
    *
    * @Column(name="product_display_price",type="integer",nullable=false)
    */
    private $product_display_price = 0;
    /**
    *
    * @Column(name="point_value",type="integer",nullable=false)
    */
    private $point_value;

    /**
    *
    * @Column(name="product_reorder_level",type="integer",nullable=false)
    */
    private $product_reorder_level;
    /**
    *
    * @Column(name="product_is_visible",type="integer",nullable=false)
    */
    private $product_is_visible; 
    /**
    *
    * @Column(name="product_is_deleted",type="integer",nullable=false)
    */
    private $product_is_deleted;

     
     
     /**
     *
     * @ORM OneToOne(targetEntity="Entity\Attachments" mappedBy="attachment_referer_id")
     */
    private $attachment_referer_id;

    private $category_name;
    private $vendor_name;


    /**
     * Gets the value of product_id.
     *
     * @return mixed
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * Sets the value of product_id.
     *
     * @param mixed $product_id the product id
     *
     * @return self
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;

        return $this;
    }

    /**
     * Gets the value of product_code.
     *
     * @return mixed
     */
    public function getProductCode()
    {
        return $this->product_code;
    }

    /**
     * Sets the value of product_code.
     *
     * @param mixed $product_code the product code
     *
     * @return self
     */
    public function setProductCode($product_code)
    {
        $this->product_code = $product_code;

        return $this;
    }

    /**
     * Gets the value of product_name.
     *
     * @return mixed
     */
    public function getProductName()
    {
        return $this->product_name;
    }

    /**
     * Sets the value of product_name.
     *
     * @param mixed $product_name the product name
     *
     * @return self
     */
    public function setProductName($product_name)
    {
        $this->product_name = $product_name;

        return $this;
    }

    /**
     * Gets the value of product_short_desc.
     *
     * @return string
     */
    public function getProductShortDesc()
    {
        return $this->product_short_desc;
    }

    /**
     * Sets the value of product_short_desc.
     *
     * @param string $product_short_desc the product short desc
     *
     * @return self
     */
    public function setProductShortDesc($product_short_desc)
    {
        $this->product_short_desc = $product_short_desc;

        return $this;
    }

    /**
     * Gets the value of product_desc.
     *
     * @return string
     */
    public function getProductDesc()
    {
        return $this->product_desc;
    }

    /**
     * Sets the value of product_desc.
     *
     * @param string $product_desc the product desc
     *
     * @return self
     */
    public function setProductDesc($product_desc)
    {
        $this->product_desc = $product_desc;

        return $this;
    }

    /**
     * Gets the value of product_quantity.
     *
     * @return mixed
     */
    public function getProductQuantity()
    {
        return $this->product_quantity;
    }

    /**
     * Sets the value of product_quantity.
     *
     * @param mixed $product_quantity the product quantity
     *
     * @return self
     */
    public function setProductQuantity($product_quantity)
    {
        $this->product_quantity = $product_quantity;

        return $this;
    }

    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getProductVendorId()
    {
        return $this->product_vendor_id_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $product_vendor_id the product vendor id
     *
     * @return self
     */
    public function setProductVendorId($product_vendor_id_id)
    {
        $this->product_vendor_id_id = $product_vendor_id_id;

        return $this;
    }

    /**
     * Gets the value of product_price.
     *
     * @return mixed
     */
    public function getProductPrice()
    {
        return $this->product_price;
    }

    /**
     * Sets the value of product_price.
     *
     * @param mixed $product_price the product price
     *
     * @return self
     */
    public function setProductPrice($product_price)
    {
        $this->product_price = $product_price;

        return $this;
    }

    /**
     * Gets the value of product_reorder_level.
     *
     * @return mixed
     */
    public function getProductReorderLevel()
    {
        return $this->product_reorder_level;
    }

    /**
     * Sets the value of product_reorder_level.
     *
     * @param mixed $product_reorder_level the product reorder level
     *
     * @return self
     */
    public function setProductReorderLevel($product_reorder_level)
    {
        $this->product_reorder_level = $product_reorder_level;

        return $this;
    }

    /**
     * Gets the value of product_is_visible.
     *
     * @return mixed
     */
    public function getProductIsVisible()
    {
        return $this->product_is_visible;
    }

    /**
     * Sets the value of product_is_visible.
     *
     * @param mixed $product_is_visible the product is visible
     *
     * @return self
     */
    public function setProductIsVisible($product_is_visible)
    {
        $this->product_is_visible = $product_is_visible;

        return $this;
    }

    /**
     * Gets the value of product_is_deleted.
     *
     * @return mixed
     */
    public function getProductIsDeleted()
    {
        return $this->product_is_deleted;
    }

    /**
     * Sets the value of product_is_deleted.
     *
     * @param mixed $product_is_deleted the product is deleted
     *
     * @return self
     */
    public function setProductIsDeleted($product_is_deleted)
    {
        $this->product_is_deleted = $product_is_deleted;

        return $this;
    }

  

  
    /**
     * Gets the Many Features have One Product.
     *
     * @return mixed
     */
    public function getProductVendorIdId()
    {
        return $this->product_vendor_id_id;
    }

    /**
     * Sets the Many Features have One Product.
     *
     * @param mixed $product_vendor_id_id the product vendor id id
     *
     * @return self
     */
    public function setProductVendorIdId($product_vendor_id_id)
    {
        $this->product_vendor_id_id = $product_vendor_id_id;

        return $this;
    }

    /**
     * Gets the value of attachment_referer_id.
     *
     * @return mixed
     */
    public function getAttachmentRefererId()
    {
        return $this->attachment_referer_id;
    }

    /**
     * Sets the value of attachment_referer_id.
     *
     * @param mixed $attachment_referer_id the attachment referer id
     *
     * @return self
     */
    public function setAttachmentRefererId($attachment_referer_id)
    {
        $this->attachment_referer_id = $attachment_referer_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }

    /**
     * @param mixed $category_id
     *
     * @return self
     */
    public function setCategoryId($category_id)
    {
        $this->category_id = $category_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPointValue()
    {
        return $this->point_value;
    }

    /**
     * @param mixed $point_value
     *
     * @return self
     */
    public function setPointValue($point_value)
    {
        $this->point_value = $point_value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProductDisplayPrice()
    {
        return $this->product_display_price;
    }

    /**
     * @param mixed $product_display_price
     *
     * @return self
     */
    public function setProductDisplayPrice($product_display_price)
    {
        $this->product_display_price = $product_display_price;

        return $this;
    }

    /**
     * @param mixed $category_name
     *
     * @return self
     */
    public function setCategoryName($category_name)
    {
        $this->category_name = $category_name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getVendorName()
    {
        return $this->vendor_name;
    }

    /**
     * @param mixed $vendor_name
     *
     * @return self
     */
    public function setVendorName($vendor_name)
    {
        $this->vendor_name = $vendor_name;

        return $this;
    }
}
?>