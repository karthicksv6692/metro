<?php
use Doctrine\ORM\Tools\Pagination\Paginator;
defined('BASEPATH') OR exit('No direct script access allowed');
class Common_utill extends CI_Model{

  protected static $tree_left_var=array();
  protected static $tree_left_var_name=array();
  protected static $tree_right_var=array();
  protected static $tree_right_var_name=array();
  protected static $tree_up_var_left = array();
  protected static $tree_up_var_right = array();
  protected static $toto = array();

  // Referer Start
    public function find_refer($refer,$refer_type){
      $em = $this->doctrine->em;
      $referer_table="";$referer_field='';$referer_function='';
      if($refer_type == 1){ $referer_table = 'User'; $referer_field='user_name'; $referer_function = 'getUserId'; }
      elseif($refer_type == 2){ $referer_table = 'Vendor'; $referer_field='vendor_name'; $referer_function = 'getVendorId'; }
      elseif($refer_type == 3){ $referer_table = 'Bank'; $referer_field='bank_ifsc'; $referer_function = 'getBankId'; }
      elseif($refer_type == 4){ $referer_table = 'Prd'; $referer_field='product_name'; $referer_function = 'getProductId'; }
      elseif($refer_type == 5){ $referer_table = 'Packages'; $referer_field='package_name'; $referer_function = 'getPackageId'; }
      elseif($refer_type == 6){ $referer_table = 'Package_products'; $referer_field='package_product_name'; $referer_function = 'getPackageProductId';  }
      $find_ref = $em->getRepository('Entity\\'.$referer_table)->findOneBy(array($referer_field=>$refer));
      if($find_ref != null){
        return $find_ref->$referer_function();
      }else{
        return false;
      }
    }
    public function get_refer_type($refer_type){
      $em = $this->doctrine->em;
      $ar = array();
      $referer_table="";$referer_field='';$referer_function='';
      if($refer_type == 1){ $referer_table = 'User';  $referer_function = 'getUserName'; }
      elseif($refer_type == 2){ $referer_table = 'Vendor';  $referer_function = 'getVendorName'; }
      elseif($refer_type == 3){ $referer_table = 'Bank';  $referer_function = 'getBankId'; }
      elseif($refer_type == 4){ $referer_table = 'Prd';  $referer_function = 'getProductName'; }
      elseif($refer_type == 5){ $referer_table = 'Packages';  $referer_function = 'getPackageName'; }
      elseif($refer_type == 6){ $referer_table = 'Package_products';  $referer_function = 'getPackageProductName';  }

      $results = $em->getRepository('Entity\\'.$referer_table)->findAll();
      for($i=0;$i<count($results);$i++){
        array_push($ar,$results[$i]->$referer_function());
      }
      return $ar;
      echo json_encode($ar);
    }
    public function get_refer_name($refer,$refer_type){
      $em = $this->doctrine->em;
      $referer_table="";$referer_field='';$referer_function='';
      if($refer_type == 1){ $referer_table = 'User'; $referer_function = 'getUserName'; }
      elseif($refer_type == 2){ $referer_table = 'Vendor'; $referer_function = 'getVendorName'; }
      elseif($refer_type == 3){ $referer_table = 'Bank'; $referer_function = 'getBankName'; }
      elseif($refer_type == 4){ $referer_table = 'Prd'; $referer_function = 'getProductName'; }
      elseif($refer_type == 5){ $referer_table = 'Packages';  $referer_function = 'getPackageName'; }
      elseif($refer_type == 6){ $referer_table = 'Package_products'; $referer_function = 'getPackageProductName';  }
      $find_ref = $em->getRepository('Entity\\'.$referer_table)->find($refer);
      if($find_ref != null){
        return $find_ref->$referer_function();
      }
    }
  // Referer End

  // Contact Start
    public function add_contact($contact_refer_type,$contact_refer,$contact_name,$contact_desig,$mobile_primary,$mobile_secondary,$email,$landline){
      $em = $this->doctrine->em;
      $contact = new Entity\Contact;
      $contact->setContactName($contact_name);
      $contact->setContactDesignation($contact_desig);
      $contact->setContactMobilePrimary($mobile_primary);
      $contact->setContactMobileSecondary($mobile_secondary);
      $contact->setContactEmailId($email);
      $contact->setContactLandline($landline);
      $contact->setContactAddressId(0);
      $contact->setContactReferId($contact_refer);
      $contact->setContactReferType($contact_refer_type);
      $em->persist($contact);
      $em->flush();
      return $em->getReference('Entity\Contact',$contact->getContactId());
      //return true;
    }
    public function add_nominee($n_name,$n_dob,$n_relationship,$n_phoneno,$n_gender){
      $em = $this->doctrine->em;
      $obj = new Entity\Nominee;
      $obj->setNomineeName($n_name);
      $obj->setNomineeDob($n_dob);
      $obj->setNomineeRelationship($n_relationship);
      $obj->setNomineePhoneNo($n_phoneno);
      $obj->setNomineeGender($n_gender);
      $em->persist($obj);
      $em->flush();
      return $em->getReference('Entity\Nominee',$obj->getNomineeId());
      //return true;
    }
    public function update_nominee($n_id,$n_name,$n_dob,$n_relationship,$n_phoneno,$n_gender){
      $em = $this->doctrine->em;
      $obj = $em->find('Entity\Nominee',$n_id);

      if($obj != null){
        $obj->setNomineeName($n_name);
        $obj->setNomineeDob($n_dob);
        $obj->setNomineeRelationship($n_relationship);
        $obj->setNomineePhoneNo($n_phoneno);
        $obj->setNomineeGender($n_gender);
        $em->persist($obj);
        $em->flush();
        return true;
      }else{
        return false;
      }
    }
    public function update_contact($contact_id,$contact_refer_type,$contact_refer,$find_refer,$contact_name,$contact_desig,$mobile_primary,$mobile_secondary,$email,$landline){
      $em = $this->doctrine->em;
      $find_contact = $em->find('Entity\Contact',$contact_id);

      if($find_contact != null){
        $find_contact->setContactName($contact_name);
        $find_contact->setContactDesignation($contact_desig);
        $find_contact->setContactMobilePrimary($mobile_primary);
        $find_contact->setContactMobileSecondary($mobile_secondary);
        $find_contact->setContactEmailId($email);
        $find_contact->setContactLandline($landline);
        $find_contact->setContactAddressId(0);
        $find_contact->setContactReferId($find_refer);
        $find_contact->setContactReferType($contact_refer_type);
        $em->persist($find_contact);
        $em->flush();
        return $em->getReference('Entity\Contact',$find_contact->getContactId());
      }else{
        return false;
      }
    }
    public function delete_contact($contact_id){
      $em = $this->doctrine->em;
      $find_contact = $em->getRepository('Entity\Contact')->find($contact_id);
      if($find_contact != null){
        $em->remove($find_contact);
        try {
          $em->flush();
          return true;
        }catch (ForeignKeyConstraintViolationException $e){ 
          $em->getConnection()->rollBack();
          return false;
        }
      }else{
        return -1;
      }
    }
  // Contact End
    
  // Attachment Start
    public function add_attachment($attachment_name,$url,$referer_id,$referer_type,$remark){
      $em = $this->doctrine->em;
      $attach = new Entity\Attachments;
      $attach->setAttachmentName($attachment_name);
      $attach->setAttachmentImageUrl($url);
      $attach->setAttachmentRefererId($referer_id);
      $attach->setAttachmentRefererType($referer_type);
      $attach->setAttachmentRemark($remark);
      $em->persist($attach);
      $em->flush();
      return true;
    }
    public function update_attachment($attachment_id,$attachment_name,$front_image_add,$find_refer,$attachment_refer_type,$remarks){
      $em = $this->doctrine->em;
      $find_attachment = $em->find('Entity\Attachments',$attachment_id);
      if($find_attachment != null){
        $find_attachment->setAttachmentName($attachment_name);
        $find_attachment->setAttachmentImageUrl($front_image_add);
        $find_attachment->setAttachmentRefererId($find_refer);
        $find_attachment->setAttachmentRefererType($attachment_refer_type);
        $find_attachment->setAttachmentRemark($remarks);
        $em->persist($find_attachment);
        $em->flush();
        return true;
      }else{
        return false;
      }
    }
  // Attachment End

  // Image Add Start
    public function svk_add_single_image($image,$i){ 
      $target_dir = "attachments/";
      if($image['tmp_name'][$i] != null){ 
        $tmpFilePath = $image['tmp_name'][$i];
        $newFilePath = $target_dir. $image['name'][$i];
        move_uploaded_file($tmpFilePath, $newFilePath);
        return $newFilePath;
    //     if(){
        // }else{ return -2; }
      }else{ return -1; }
    }
    public function add_single_image($image,$i){ 
      //print_r($image); 
      $target_dir = "attachments/";
      if($image['tmp_name'][$i] != null){ 
        $tmpFilePath = $image['tmp_name'];
        $newFilePath = $target_dir. $image['name'];
        move_uploaded_file($tmpFilePath, $newFilePath);
        return $newFilePath;
    //     if(){
        // }else{ return -2; }
      }else{ return -1; }
    }
    public function add_id_single_image($image){  //print_r($image);
      $target_dir = "attachments/";
      if($image['tmp_name'] != null){ 
        $tmpFilePath = $image['tmp_name'];
        $newFilePath = $target_dir. $image['name'];
        move_uploaded_file($tmpFilePath, $newFilePath);
        return $newFilePath;
    //     if(){
        // }else{return false;}
      }else{return false;}
    }
    public function add_image_with_name_only($tmpFilePath, $newFilePath){
      if(move_uploaded_file($tmpFilePath, $newFilePath)){
        return $newFilePath;
      }else{
        return false;
      }
    }
    public function add_multiple_image($images){
      $target_dir = "attachments/";
      $path=array();
      for($i=0;$i<count($images['name']);$i++){
        $tmpFilePath = $images['tmp_name'][$i];
        $newFilePath = $target_dir. $images['name'][$i];
        move_uploaded_file($tmpFilePath, $newFilePath);
        array_push($path, $newFilePath);
        if($i == count($images['name'])-1){
          return implode('~',$path);
        }
      }
    }
  // Image Add Start

  // Address Start
    public function add_address_with_pincode_country_id($country_id,$pincode_no,$place=null,$district,$state,$full_address){
      $em = $this->doctrine->em;
      $country_obj = $this->get_reference_obj("Country",$country_id);
      if($country_obj != null){
        $pincode_obj = $this->add_pincode($pincode_no,$place,$district,$state,$country_obj);
        if($pincode_obj != null){
          $ad_obj = $this->add_add($full_address,$pincode_obj);
          if($ad_obj != null){
            return $ad_obj;
          }else{ return false; }
        }else{ return false; }
      }else{ return false; }
    }
    public function add_address_with_pincode_country($country_id,$pincode_no,$place,$district,$state,$full_address){
      $em = $this->doctrine->em;
      $country_obj = $this->get_reference_obj("Country",$country_id);
      if($country_obj != null){
        $pincode_obj = $this->add_pincode($pincode_no,$place,$district,$state,$country_obj);
        if($pincode_obj != null){
          $ad_obj = $this->add_add($full_address,$pincode_obj);
          if($ad_obj != null){
            //$em->getConnection()->commit();
            return $ad_obj;
          }else{
            return false;
          }
        }else{return false;}
      }else{
        return false;
      }
    }
    public function update_address_with_pincode_country($country_id,$pincode_no,$place,$district,$state,$full_address,$address_id){
      //echo $pincode_no;exit();
      $em = $this->doctrine->em;
      $address_obj = $em->getReference('Entity\Address',$address_id);
      if($address_obj != null){
        $country_obj = $this->get_reference_obj("Country",$country_id);
        if($country_obj != null){
          $pincode_obj = $this->add_pincode($pincode_no,$place,$district,$state,$country_obj);
          if($pincode_obj != null){
            $address_obj->set_Address_full_address($full_address);
            $address_obj->set_Address_pincode_id($pincode_obj);
            $em->persist($address_obj);
            $em->flush();
            return $address_obj;
          }else{ return false; }
        }else{ return false; }
      }else{ return false; }
    }
    public function add_country($country_name,$country_code){
      $em = $this->doctrine->em;
      $em->getConnection()->beginTransaction(); 
      $find_country_code = $em->getRepository('Entity\Country')->findOneBy(array('country_code' => $country_code));
      $find_country_name = $em->getRepository('Entity\Country')->findOneBy(array('country_name' => $country_name));
      if($find_country_code == null && $find_country_name == null){
        $country = new Entity\Country;
        $country->setCountryName($country_name);
        $country->setCountryCode($country_code);
        $em->persist($country);
        try{
          $em->flush();
          return $em->getReference('Entity\Country',$country->getCountryId());
          
        }catch(UniqueConstraintViolationException $e){
          $em->getConnection()->rollBack();
          return false;
          }
      }elseif($find_country_code != null){
        return $em->getReference('Entity\Country',$find_country_code->getCountryId());
      }elseif($find_country_name != null){
        return $em->getReference('Entity\Country',$find_country_name->getCountryId());
      }else{
        return $em->getReference('Entity\Country',$find_country->getCountryId());
      }
    }
    public function add_pincode($pincode_no,$place,$district,$state,$country_obj){
      $em = $this->doctrine->em;
      $find_pincode = $em->getRepository('Entity\Pincode')->findOneBy(array('pincode_no' => $pincode_no));
      if($find_pincode == null){
        $pincode_obj = new Entity\Pincode;
        $pincode_obj->setPincodeNo($pincode_no);
        $pincode_obj->setPincodePlace($place);
        $pincode_obj->setPincodeDistrict($district);
        $pincode_obj->setPincodeState($state);
        $pincode_obj->setPincodeCountryId($country_obj);
        $em->persist($pincode_obj);
        try {
          $em->flush();
          return $em->getReference('Entity\Pincode',$pincode_obj->getPincodeId());
        }catch (UniqueConstraintViolationException $e){
          $em->getConnection()->rollBack();
          return false;
        }
      }else{
        return $em->getReference('Entity\Pincode',$find_pincode->getPincodeId());
      }
    }
    public function add_add($full_address,$find_pincode){
      $em = $this->doctrine->em;
      $address_obj = new Entity\Address;
      $address_obj->set_Address_full_address($full_address);
      $address_obj->set_Address_pincode_id($find_pincode);
      $em->persist($address_obj);
      $em->flush();
      if($address_obj->get_Address_id() > 0){
        return $em->getReference('Entity\Address',$address_obj->get_Address_id());
      }else{
        $em->getConnection()->rollBack();
        return false;
      }
    }
    public function delete_address($address_id){
      $em = $this->doctrine->em;
      $find_address = $em->getRepository('Entity\Address')->find($address_id);
      if($find_address != null){
        $em->remove($find_address);
        try {
          $em->flush();
          return true;
        }catch (ForeignKeyConstraintViolationException $e){ 
          $em->getConnection()->rollBack();
          return false;
        }
      }else{
        return -1;
      }
    }
    public function delete_nominee($nominee_id){
      $em = $this->doctrine->em;
      $find_nominee = $em->getRepository('Entity\Nominee')->find($nominee_id);
      if($find_nominee != null){
        $em->remove($find_nominee);
        try {
          $em->flush();
          return true;
        }catch (ForeignKeyConstraintViolationException $e){ 
          $em->getConnection()->rollBack();
          return false;
        }
      }else{
        return -1;
      }
    }
    public function delete_password($password_id){
      $em = $this->doctrine->em;
      $find_password = $em->getRepository('Entity\Password')->find($password_id);
      if($find_password != null){
        $em->remove($find_password);
        try {
          $em->flush();
          return true;
        }catch (ForeignKeyConstraintViolationException $e){ 
          $em->getConnection()->rollBack();
          return false;
        }
      }else{
        return -1;
      }
    }
  // Address End

  //Bank Start
    public function add_bank_with_address_contact_details($bank_name,$bank_ifsc,$bank_branch,$pincode_no,$place,$district,$state,$full_address,$country_id,$mobile,$email,$landline){
      $em = $this->doctrine->em;
      $em->getConnection()->beginTransaction();
      $country_obj = $this->get_reference_obj("Country",$country_id);
      if($country_obj != null){
        $pincode_obj = $this->add_pincode($pincode_no,$place,$district,$state,$country_obj);
        if($pincode_obj != null){
          $ad_obj = $this->add_add($full_address,$pincode_obj);
          if($ad_obj != null){
            $bank_obj = $this->add_bank($bank_name,$bank_ifsc,$bank_branch,$ad_obj);
            if($bank_obj != null){
              $contact_refer_type = 3;
              $contact_refer = $bank_obj->getBankId();
              $contact_name = "Bank";
              $contact_desig = "Bank_contact";
              $mobile_primary = $mobile;
              $mobile_secondary = 0000000000;
              $email = $email;
              $landline = $landline;
              $contact_obj = $this->add_contact($contact_refer_type,$contact_refer,$contact_name,$contact_desig,$mobile_primary,$mobile_secondary,$email,$landline);
              if($contact_obj != null){
                $em->getConnection()->commit();
                return $em->getReference('Entity\Bank_details',$bank_obj->getBankId()); 
              }else{  $em->getConnection()->rollBack();return false; }
            }else{  $em->getConnection()->rollBack();return false; }
          }else{  $em->getConnection()->rollBack();return -3; }
        }else{  $em->getConnection()->rollBack();return -2; }
      }else{ $em->getConnection()->rollBack();return -1;}
    }

    public function update_bank_with_address_contact_details($bank_id,$bank_name,$bank_ifsc,$bank_branch,$pincode_no,$place,$district,$state,$full_address,$country_id,$contact_id,$mobile,$email,$landline){
      $em = $this->doctrine->em;
      $em->getConnection()->beginTransaction();
      $country_obj = $this->get_reference_obj("Country",$country_id);
      if($country_obj != null){
        $pincode_obj = $this->add_pincode($pincode_no,$place,$district,$state,$country_obj);
        if($pincode_obj != null){
          $ad_obj = $this->add_add($full_address,$pincode_obj);
          if($ad_obj != null){
            $bank_obj = $this->update_bank($bank_id,$bank_name,$bank_ifsc,$bank_branch,$ad_obj);
            if($bank_obj->getBankId() != null ){

              if($contact_id != 0){
                $contact_refer_type = 3;
                $contact_refer = $bank_obj->getBankId();
                $find_refer = $bank_obj->getBankId();
                $contact_name = "Bank";
                $contact_desig = "Bank_contact";
                $mobile_primary = $mobile;
                $mobile_secondary = 0000000000;
                $email = $email;
                $landline = $landline;
                $contact = $this->update_contact($contact_id,$contact_refer_type,$contact_refer,$find_refer,$contact_name,$contact_desig,$mobile_primary,$mobile_secondary,$email,$landline);

                if($contact != null){
                  $em->getConnection()->commit();
                  return $em->getReference('Entity\Bank_details',$bank_obj->getBankId());
                }else{return false;}
              }else{
                $contact_refer_type = 3;
                $contact_refer = $bank_obj->getBankId();
                $find_refer = $bank_obj->getBankId();
                $contact_name = "Bank";
                $contact_desig = "Bank_contact";
                $mobile_primary = $mobile;
                $mobile_secondary = 0000000000;
                $email = $email;
                $landline = $landline;
                $contact = $this->add_contact($contact_refer_type,$contact_refer,$contact_name,$contact_desig,$mobile_primary,$mobile_secondary,$email,$landline);

                if($contact != null){
                  $em->getConnection()->commit();
                  return $em->getReference('Entity\Bank_details',$bank_obj->getBankId());
                }else{return false;}
              }

            }elseif($bank_obj != -1){
              return -4;
            }else{return false;}
          }else{
            return -3;
          }
        }else{
          return -2;
        }
      }else{
        return -1;
      }
    }
    public function add_bank_with_address_details($bank_name,$bank_ifsc,$bank_branch,$country_name,$country_code,$pincode_no,$place,$district,$state,$full_address){
      $em = $this->doctrine->em;
      $country_obj = $this->add_country($country_name,$country_code);
      if($country_obj != null){
        $pincode_obj = $this->add_pincode($pincode_no,$place,$district,$state,$country_obj);
        if($pincode_obj != null){
          $ad_obj = $this->add_add($full_address,$pincode_obj);
          if($ad_obj != null){
            $bank_obj = $this->add_bank($bank_name,$bank_ifsc,$bank_branch,$ad_obj);
            if($bank_obj != null){
              $em->getConnection()->commit();
              return $em->getReference('Entity\Bank_details',$bank_obj->getBankId());
            }else{return false;}
          }else{
            return -3;
          }
        }else{
          return -2;
        }
      }else{
        return -1;
      }
    }
    public function update_bank_with_address_details($bank_id,$bank_name,$bank_ifsc,$bank_branch,$country_name,$country_code,$pincode_no,$place,$district,$state,$full_address){
      $em = $this->doctrine->em;
      $country_obj = $this->add_country($country_name,$country_code);
      if($country_obj != null){
        $pincode_obj = $this->add_pincode($pincode_no,$place,$district,$state,$country_obj);
        if($pincode_obj != null){
          $ad_obj = $this->add_add($full_address,$pincode_obj);
          if($ad_obj != null){
            $bank_obj = $this->update_bank($bank_id,$bank_name,$bank_ifsc,$bank_branch,$ad_obj);
            if($bank_obj->getBankId() != null ){
              $em->getConnection()->commit();
              return $em->getReference('Entity\Bank_details',$bank_obj->getBankId());
            }elseif($bank_obj != -1){
              return -4;
            }else{return false;}
          }else{
            return -3;
          }
        }else{
          return -2;
        }
      }else{
        return -1;
      }
    }
    public function add_bank($bank_name,$bank_ifsc,$bank_branch,$address_obj){
      $em = $this->doctrine->em;
      $bank_obj = new Entity\Bank_details;
      $bank_obj->setBankName($bank_name);
      $bank_obj->setBankIfsc($bank_ifsc);
      $bank_obj->setBankBranch($bank_branch);
      $bank_obj->setBankAddress($address_obj->get_Address_full_address());
      $bank_obj->setBankCity($address_obj->get_Address_pincode_id()->getPincodePlace());
      $bank_obj->setBankDistrict($address_obj->get_Address_pincode_id()->getPincodeDistrict());
      $bank_obj->setBankState($address_obj->get_Address_pincode_id()->getPincodeState());
      $bank_obj->setBankAddressId($address_obj);
      $em->persist($bank_obj);
      try{
        $em->flush();
        return $em->getReference('Entity\Bank_details',$bank_obj->getBankId());
      }catch(UniqueConstraintViolationException $e){
        $em->getConnection()->rollBack();
        return false;
        }
    }
    public function get_bank_reference_not_exists_add($bank_name,$bank_ifsc,$bank_branch,$ad_obj){
      $em = $this->doctrine->em;
      $find_bank = $em->getRepository('Entity\Bank_details')->findOneBy(array('bank_ifsc' => $bank_ifsc));

      if($find_bank == null){ 
        $bank_obj = $this->add_bank($bank_name,$bank_ifsc,$bank_branch,$ad_obj);
        if($bank_obj != null){
          return $em->getReference('Entity\Bank_details',$bank_obj->getBankId());
        }else{return false;}
      }else{
        return $em->getReference('Entity\Bank_details',$find_bank->getBankId());
      }
    }
    public function update_bank($bank_id,$bank_name,$bank_ifsc,$bank_branch,$address_obj){
      $em = $this->doctrine->em;
      $bank_obj = $em->getRepository('Entity\Bank_details')->find($bank_id);
      if($bank_obj != null){
        $bank_obj->setBankName($bank_name);
        $bank_obj->setBankIfsc($bank_ifsc);
        $bank_obj->setBankBranch($bank_branch);
        $bank_obj->setBankAddress($address_obj->get_Address_full_address());
        $bank_obj->setBankCity($address_obj->get_Address_pincode_id()->getPincodePlace());
        $bank_obj->setBankDistrict($address_obj->get_Address_pincode_id()->getPincodeDistrict());
        $bank_obj->setBankState($address_obj->get_Address_pincode_id()->getPincodeState());
        $bank_obj->setBankAddressId($address_obj);
        $em->persist($bank_obj);
        try{
          $em->flush();
          return $em->getReference('Entity\Bank_details',$bank_obj->getBankId());
        }catch(UniqueConstraintViolationException $e){
          $em->getConnection()->rollBack();
          return false;
          }
       }else{
        return -1;
       }
    }
    public function get_bank_details_with_ifsc_code($ifsc){
      $pincode = new Entity\Pincode; 
      $em=$this->doctrine->em;
      $single_bank = $em->getRepository('Entity\Bank_details')->findOneBy(array('bank_ifsc' => $ifsc)); 
      $bank_id = $single_bank->getBankId();
      $bank_name = $single_bank->getBankName();
      $bank_ifsc = $single_bank->getBankIfsc();
      $bank_branch = $single_bank->getBankBranch();
      $bank_address_obj = $single_bank->getBankAddressId();
      
      $bank_address = $bank_address_obj->get_Address_full_address();
      $bank_pincode_obj = $bank_address_obj->get_Address_pincode_id();
      $pincode_code = $bank_pincode_obj->getPincodeNo();
      $pincode_place = $bank_pincode_obj->getPincodePlace();
      $district = $bank_pincode_obj->getPincodeDistrict();
      $state = $bank_pincode_obj->getPincodeState();
      $country = $bank_pincode_obj->getPincodeCountryId()->getCountryName();
      $country_code = $bank_pincode_obj->getPincodeCountryId()->getCountryCode();
      $country_id = $bank_pincode_obj->getPincodeCountryId()->getCountryId();

      $ifsc_data = array();
      array_push($ifsc_data, array('bank_name'=>$bank_name));
      array_push($ifsc_data, array('bank_branch'=>$bank_branch));
      array_push($ifsc_data, array('bank_address'=>$bank_address));

      array_push($ifsc_data, array('pincode_code'=>$pincode_code));
      array_push($ifsc_data, array('pincode_place'=>$pincode_place));
      array_push($ifsc_data, array('pincode_state'=>$state));
      array_push($ifsc_data, array('pincode_district'=>$district));
      array_push($ifsc_data, array('pincode_country'=>$country));
      array_push($ifsc_data, array('pincode_country_code'=>$country_code));
      array_push($ifsc_data, array('pincode_country_id'=>$country_id));
      return $ifsc_data;
    }
    public function get_ifsc_list($key){
      $em=$this->doctrine->em;
      $bnk_ar = array();

      $query = $em->createQuery("SELECT b.bank_id,b.bank_ifsc FROM Entity\Bank_details as b WHERE b.bank_ifsc LIKE '%$key%'" )->setMaxResults(5);

      $single_bank = $query->getResult();

      for ($i=0; $i < count($single_bank); $i++) { 
        $ar = array('data_id'=>$single_bank[$i]['bank_id'],'value'=>$single_bank[$i]['bank_ifsc']);
        array_push($bnk_ar, $ar);
      }
      return $bnk_ar;
    }
    public function get_pincode_list($key){
      $em=$this->doctrine->em;
      $bnk_ar = array();

      $query = $em->createQuery("SELECT b.pincode_id,b.pincode_no FROM Entity\Pincode as b WHERE b.pincode_no LIKE '%$key%'" )->setMaxResults(5);

      $single_bank = $query->getResult();

      for ($i=0; $i < count($single_bank); $i++) { 
        $ar = array('data_id'=>$single_bank[$i]['pincode_id'],'value'=>$single_bank[$i]['pincode_no']);
        array_push($bnk_ar, $ar);
      }
      return $bnk_ar;
    }
    public function get_packages($key){
      $em=$this->doctrine->em;
      
      if($key != 0){
        $pack = array();
        $query = $em->createQuery("SELECT b.package_id,b.package_name,b.package_price FROM Entity\Packages as b WHERE b.package_name LIKE '%$key%' AND b.package_is_visible =1" )->setMaxResults(8);
        $tot_pack = $query->getResult();
        if(count($tot_pack) > 0){
          for ($i=0; $i < count($tot_pack); $i++) { 
            $ar = array('data_id'=>$tot_pack[$i]['package_id'],'value'=>$tot_pack[$i]['package_name'],'pck_price'=>$tot_pack[$i]['package_price']);
            array_push($pack, $ar);
          }
          return $pack;
        }else{
          $pack_sub = array();
          $query_sub = $em->createQuery("SELECT b.package_id,b.package_name,b.package_price FROM Entity\Packages as b WHERE b.package_is_visible =1" )->setMaxResults(8);
          $tot_pack_sub = $query_sub->getResult();
          for ($i=0; $i < count($tot_pack_sub); $i++) { 
            $ar_sub = array('data_id'=>$tot_pack_sub[$i]['package_id'],'value'=>$tot_pack_sub[$i]['package_name'],'pck_price'=>$tot_pack_sub[$i]['package_price']);
            array_push($pack_sub, $ar_sub);
          }
          return $pack_sub;
        }
      }else{
        $pack_sub_sub = array();
        $query_sub_sub = $em->createQuery("SELECT b.package_id,b.package_name,b.package_price FROM Entity\Packages as b WHERE b.package_is_visible =1" )->setMaxResults(8);
        $tot_pack_sub_sub = $query_sub_sub->getResult();
        for ($i=0; $i < count($tot_pack_sub_sub); $i++) { 
          $ar_sub_sub = array('data_id'=>$tot_pack_sub_sub[$i]['package_id'],'value'=>$tot_pack_sub_sub[$i]['package_name'],'pck_price'=>$tot_pack_sub_sub[$i]['package_price']);
          array_push($pack_sub_sub, $ar_sub_sub);
        }
        return $pack_sub_sub;
      }
    }
    public function get_all_packages(){
      $em=$this->doctrine->em;
      $pack_sub_sub = array();
      $query_sub_sub = $em->createQuery("SELECT b.package_id,b.package_name,b.package_price FROM Entity\Packages as b WHERE b.package_is_visible =1" );
      $tot_pack_sub_sub = $query_sub_sub->getResult();
      for ($i=0; $i < count($tot_pack_sub_sub); $i++) { 

        $product_att_obj = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_id' => $tot_pack_sub_sub[$i]['package_id'],'attachment_referer_type'=>5));
        $image_url = 0;
        if($product_att_obj != null){
          if(! is_integer($product_att_obj->getAttachmentImageUrl())){
            $image_url = $product_att_obj->getAttachmentImageUrl();
          }
        }


        $ar_sub_sub = array('data_id'=>$tot_pack_sub_sub[$i]['package_id'],'value'=>$tot_pack_sub_sub[$i]['package_name'],'pck_price'=>$tot_pack_sub_sub[$i]['package_price'],'image_url'=>$image_url);
        array_push($pack_sub_sub, $ar_sub_sub);
      }
      return $pack_sub_sub;
    }
    public function get_all_active_packages_promary_key(){
      $em=$this->doctrine->em;
      $pack_ar = array();
      $find_pack = $em->getRepository('Entity\Packages')->findAll();
      if($find_pack != null){
        foreach ($find_pack as $pack) {
          array_push($pack_ar,$pack->getPackageId());
        }
        
        return $pack_ar;
      }else{return -5;}
    }
    public function get_all_active_packages_primary_key_name(){
      $em=$this->doctrine->em;
      $pack_ar = array();
      $find_pack = $em->getRepository('Entity\Packages')->findAll();
      if($find_pack != null){
        foreach ($find_pack as $pack) {
          array_push($pack_ar,array('package_id' => $pack->getPackageId(), 'package_name' => $pack->getPackageName()));
        }
        
        return $pack_ar;
      }else{return -5;}
    }
    public function get_all_up_ids(){
      $em=$this->doctrine->em;
      $pack_ar = array();
      // $find_pack = $em->getRepository('Entity\User_package_purchase')->findAll();
      $find_pack = $em->getRepository('Entity\User_package_purchase')->findBy(array('payout_ach' => 1));
      if($find_pack != null){
        foreach ($find_pack as $pack) {
          array_push($pack_ar,$pack->getUserPurchaseId());
        }
        
        return $pack_ar;
      }else{return -5;}
    }
    public function get_all_packages_name(){
      $em=$this->doctrine->em;
      $pack_ar = array();
      $find_pack = $em->getRepository('Entity\Packages')->findAll();
      if($find_pack != null){
        foreach ($find_pack as $pack) {
          array_push($pack_ar,$pack->getPackageName());
        }
        
        return $pack_ar;
      }else{return -5;}
    }
  //Bank End

  //Account Details Start
    public function add_account_with_address_details($account_holder_name,$account_no,$account_type,$bank_name,$bank_ifsc,$bank_branch,$country_code,$pincode_no,$place,$district,$state,$full_address,$referer_id,$referer_type){

      $em = $this->doctrine->em;
      $country_obj = $this->get_country_reference($country_code);

      if($country_obj != null){
        $pincode_obj = $this->add_pincode($pincode_no,$place,$district,$state,$country_obj);
        if($pincode_obj != null){
          $ad_obj = $this->add_add($full_address,$pincode_obj);
          if($ad_obj != null){
            $bank_obj = $this->get_bank_reference_not_exists_add($bank_name,$bank_ifsc,$bank_branch,$ad_obj);
            if($bank_obj != null){ 
              $account_obj = $this->add_account($account_holder_name,$account_no,$account_type,$bank_obj,$referer_id,$referer_type);
              if($account_obj == 1){
                $em->getConnection()->commit();
                return true;
              }else{
                return false;
              }
            }else{
              return -4;
            }
          }else{
            return -3;
          }
        }else{
          return -2;
        }
      }else{
        return -1;
      }
    }
    public function update_account_with_address_details($account_id,$account_holder_name,$account_no,$account_type,$bank_name,$bank_ifsc,$bank_branch,$country_code,$pincode_no,$place,$district,$state,$full_address,$referer_id,$referer_type){

      $em = $this->doctrine->em;
      $country_obj = $this->get_country_reference($country_code);

      if($country_obj != null){
        $pincode_obj = $this->add_pincode($pincode_no,$place,$district,$state,$country_obj);
        if($pincode_obj != null){
          $ad_obj = $this->add_add($full_address,$pincode_obj);
          if($ad_obj != null){
            $bank_obj = $this->get_bank_reference_not_exists_add($bank_name,$bank_ifsc,$bank_branch,$ad_obj);
            if($bank_obj != null){ 
              $account_obj = $this->update_account($account_id,$account_holder_name,$account_no,$account_type,$bank_obj,$referer_id,$referer_type);
              if($account_obj == 1){
                $em->getConnection()->commit();
                return true;
              }else{
                return false;
              }
            }else{
              return -4;
            }
          }else{
            return -3;
          }
        }else{
          return -2;
        }
      }else{
        return -1;
      }
    }
    public function add_account($account_holder_name,$account_no,$account_type,$bank_obj,$referer_id,$referer_type){
      $em = $this->doctrine->em;
      $find_account = $em->getRepository('Entity\Account_details')->findOneBy(array('account_no' => $account_no));
      if($find_account == null){
        $account = new Entity\Account_details;
        $account->setAccountNo($account_no);
        $account->setAccountType($account_type);
        if($bank_obj != null){
          $account->setBankId($bank_obj);
        }
        $account->setAccountHolderName($account_holder_name);
        $account->setAccountReferId($referer_id);
        $account->setAccountReferType($referer_type);
        $em->persist($account);
        try {
          $em->flush();
          return true;
        }catch (UniqueConstraintViolationException $e){
          return false;
        }
      }else{
        return -1;
      }
    }
    public function update_account($account_id,$account_holder_name,$account_no,$account_type,$bank_obj,$referer_id,$referer_type){
      $em = $this->doctrine->em;
      $find_account = $em->getRepository('Entity\Account_details')->find($account_id);
      if($find_account != null){
        $find_account->setAccountNo($account_no);
        $find_account->setAccountType($account_type);
        $find_account->setBankId($bank_obj);
        $find_account->setAccountHolderName($account_holder_name);
        $find_account->setAccountReferId($referer_id);
        $find_account->setAccountReferType($referer_type);
        $em->persist($find_account);
        try {
          $em->flush();
          return true;
        }catch (UniqueConstraintViolationException $e){
          $em->getConnection()->rollBack();
          return false;
        }
      }else{
        $em->getConnection()->rollBack();
        return -1;
      }
    }
    public function get_country_reference($country_id){
      $em = $this->doctrine->em;
      $em->getConnection()->beginTransaction(); 
      $find_country = $em->getRepository('Entity\Country')->find($country_id);
      if($find_country != null){
        return $em->getReference('Entity\Country',$country_id);
      }else{
        return false;
      }
    }
    public function get_last_record(){
      $em = $this->doctrine->em;
      $query = $em->createQuery("SELECT MAX(u.avail_no) FROM Entity\Available_bill_no as u" );
      $course = $query->getResult();
      if($course[0][1] != null){
        return $course[0][1];
      }else{
        $q = $em->createQuery("SELECT MAX(u.purchase_request_no) FROM Entity\Purchase_request as u" );
        $c = $q->getResult();
        if($c[0][1] != null){
          return $c[0][1];
        }else{
          return 0;
        }
      }
    }
    public function set_last_record($new_val){
      $em = $this->doctrine->em;
      $account = new Entity\Available_bill_no;
      $account->setAvailNo((string)$new_val);
      $em->persist($account);
      $em->flush();
      return true;
    }
    public function del_last_record($new_val){
      $em = $this->doctrine->em;
      $find_account = $em->getRepository('Entity\Available_bill_no')->findOneBy(array('avail_no'=> trim($new_val)));
      if($find_account != null){
        $em->remove($find_account);
        try {
          $em->flush();
          return true;
        }catch (UniqueConstraintViolationException $e){
          return false;
        }
      }else{
        return false;
      }
    }
  //Account Details End

  //Check Common Availability
    public function check_availability($table_name,$filed_name,$field_value){
      // echo "$field_value";
      $em=$this->doctrine->em;
      $check_avail = $em->getRepository('Entity\\'.$table_name)->findOneBy(array($filed_name => $field_value));

      if(count($check_avail) == 0){
        return 1;
      }else{return 0;}
    }
    public function check_availability_on_update($table_name,$field_name,$field_value,$primary_key,$key_value){
      $em=$this->doctrine->em;
      $q = $em->createQuery("SELECT u from Entity\\$table_name u WHERE u.$field_name='".$field_value."' AND u.$primary_key!=$key_value" );
      $users = $q->getResult();
      if(count($users)==0){
        return 1;
      }else{
        return 0;
      }
    }
  //Check Common Availability


  //Add Product STart
    public function add_product_with_attachment($is_active,$product_name,$product_code,$product_quantity,$product_price,$product_display_price,$prd_vendor_id,$product_reorder_level,$product_desc,$product_short_desc,$product_vendor_id,$images,$cat_obj,$point_value,$user_choice){ 
      $em=$this->doctrine->em;
      $product = new Entity\Product;
      $em->getConnection()->beginTransaction();
      $product->setProductName($product_name);
      $product->setProductCode($product_code);
      $product->setProductQuantity($product_quantity);
      $product->setProductPrice($product_price);
      $product->setProductDisplayPrice($product_display_price);
      $product->setPointValue($point_value);
      $product->setProductVendorId($product_vendor_id);

      if(!is_integer($cat_obj)){
        $product->setCategoryId($cat_obj);
      }

      $product->setProductReorderLevel($product_reorder_level);
      $product->setProductDesc($product_desc);
      $product->setProductShortDesc($product_short_desc);
      $product->setProductIsVisible($is_active);
      $product->setProductIsDeleted($user_choice);
      $em->persist($product);
      try {
        $em->flush();
        if(count($images['name']) > 0){
          $atch = $this->add_multiple_image($images);
          $atch_ar = explode('~',$atch);
          for($i=0;$i<count($atch_ar);$i++){
            $attach = $this->add_attachment($product_name,$atch_ar[$i],$product->getProductId(),4,"xx");
            if($i == count($atch_ar)-1){
              return true;
            }
          }
        }else{
          return true;
        }

      }catch (UniqueConstraintViolationException $e){ 
        $em->getConnection()->rollBack();
        return false;
      }
    }
    public function update_product_with_attachment($product_id,$is_active,$product_name,$product_code,$product_quantity,$product_price,$product_display_price,$product_vendor_id,$product_reorder_level,$product_desc,$product_short_desc,$images,$img_list,$nw_ad_img,$package_image,$cat_obj,$point_value,$user_choice){

      $em=$this->doctrine->em;
      $product_exists = $em->find('Entity\Product',$product_id);
      $product_exists->setProductName($product_name);
      $product_exists->setProductCode($product_code);
      $product_exists->setProductQuantity($product_quantity);
      $product_exists->setProductPrice($product_price);
      $product_exists->setProductDisplayPrice($product_display_price);
      $product_exists->setProductPrice($product_price);
      $product_exists->setPointValue($point_value);
      $product_exists->setProductVendorId($product_vendor_id);

      if(!is_integer($cat_obj)){
        $product_exists->setCategoryId($cat_obj);
      }

      $product_exists->setProductReorderLevel($product_reorder_level);
      $product_exists->setProductDesc($product_desc);
      $product_exists->setProductShortDesc($product_short_desc);
      $product_exists->setProductIsVisible($is_active);
      $product_exists->setProductIsDeleted($user_choice);
      $em->persist($product_exists);
      try {
        $em->flush();
        $product_id = $product_exists->getProductId();

        if($nw_ad_img == 0){
          return true;
        }else{
          $find_attach = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $product_id,'attachment_referer_type' => 4));
            if(count($find_attach) > 0){
              for($i=0;$i<count($find_attach);$i++){
                $em->remove($find_attach[$i]);
                $em->flush();
              // $em->getConnection()->commit();
              }
            }
            if($package_image != null){
              $usr_img = explode(',', $package_image);

              // print_r(count($usr_img)-1)

              if(count($usr_img) > 0){
                for($i=0;$i<count($usr_img);$i++){
                  $attach = $this->add_attachment($product_name,$usr_img[$i],$product_id,4,"xx");
                  $tc = count($usr_img)-1;
                  if($i == $tc){ 
                    if($images['name'][0] != null){
                      $atch = $this->add_multiple_image($images);
                      $atch_ar = explode('~',$atch);
                      for($j=0;$j<count($atch_ar);$j++){
                        $this->add_attachment($product_name,$atch_ar[$j],$product_id,4,"xx");
                        if($j == count($atch_ar)-1){
                          $em->flush();
                          return true;
                        }
                      }
                    }else{
                      $em->flush();
                      return true;
                    }
                  }
                
                }
              }
            }else if(count($images['name']) > 0){
              $atch = $this->add_multiple_image($images);
              $atch_ar = explode('~',$atch);
              for($i=0;$i<count($atch_ar);$i++){
                $attach = $this->add_attachment($product_name,$atch_ar[$i],$product_id,4,"xx");
                if($i == count($atch_ar)-1){
                  $em->flush();
                  return true;
                }
              }
            }else{
              $em->flush();
              return true;
            }
        }

      }catch (UniqueConstraintViolationException $e){
        return false;
      }
    }
  //Add Product End

  //packageProduct Start
    //Add Start
    public function add_package_product_with_attachment($is_active,$package_product_name,$package_product_quantity,$package_product_price,$package_product_display_price,$package_prd_package_id,$package_id,$package_product_desc,$package_product_short_desc,$images){
      $em=$this->doctrine->em;
      $package_product = new Entity\Package_products;
      $package_product->setPackageProductName($package_product_name);
      $package_product->setPackageProductQuantity($package_product_quantity);
      $package_product->setPackageProductPrice($package_product_price);
      $package_product->setPackageProductDisplayPrice($package_product_display_price);
      $package_product->setPackageId($package_id);
      $package_product->setPackageProductDesc($package_product_desc);
      $package_product->setPackageProductShortDesc($package_product_short_desc);
      $package_product->setPackageProductIsVisible($is_active);
      $em->persist($package_product);
      try {
        $em->flush();
        //return $package_product->getPackageProductId();
        if($images['error'][0] != 4){ 
          for($i=0;$i<count($images['name']);$i++){
            $url = $this->svk_add_single_image($images,$i); 
            $attach = $this->add_attachment($package_product_name,$url,$package_product->getPackageProductId(),6,$package_product_short_desc);
            if($attach == 1){ 
              if($i == count($images['name'])-1){
                return $package_product->getPackageProductId();
              }
            }else{return false;}
          }
        }else{
          return $package_product->getPackageProductId();
        }
      }catch (UniqueConstraintViolationException $e){ 
        $em->getConnection()->rollBack();
        return false;
      }
    }
    public function update_package_product_with_attachment($package_product_id,$is_active,$package_product_name,$package_product_quantity,$package_product_price,$package_product_display_price,$package_prd_package_id,$package_id,$package_product_desc,$package_product_short_desc,$images,$img_list,$nw_ad_img,$package_image){
      $em=$this->doctrine->em;
      $em->getConnection()->beginTransaction();
      $package_product_exists = $em->find('Entity\Package_products',$package_product_id);
      if($package_product_exists != null){
        $package_product_exists->setPackageProductName($package_product_name);
        $package_product_exists->setPackageProductQuantity($package_product_quantity);
        $package_product_exists->setPackageProductPrice($package_product_price);
        $package_product_exists->setPackageProductDisplayPrice($package_product_display_price);
        $package_product_exists->setPackageId($package_id);
        $package_product_exists->setPackageProductDesc($package_product_short_desc);
        $package_product_exists->setPackageProductShortDesc($package_product_short_desc);
        $package_product_exists->setPackageProductIsVisible($is_active);
        $em->persist($package_product_exists);
        try {
          $em->flush();
          $package_product_id = $package_product_exists->getPackageProductId();
          $attachment_object = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $package_product_id,'attachment_referer_type'=>6));
          

          if($nw_ad_img == 0){
            return true;
          }else{
            if(count($attachment_object)>0){ 
              for($i=0;$i<count($attachment_object);$i++){
                $attachment_exists = $em->find('Entity\Attachments',$attachment_object[$i]->getAttachmentId());
                $em->remove($attachment_exists);
                $em->flush();
              }
            }

            if($package_image != null){
                $usr_img = explode(',', $package_image);
                if(count($usr_img) > 0){
                  for($i=0;$i<count($usr_img);$i++){
                    $attach = $this->Common_utill->add_attachment($package_product_name,$usr_img[$i],$package_product_id,6,$package_product_short_desc);
                    if($i == count($usr_img)-1){ 

                      $em->flush();
                      if($images['error'][0]!= 4){ //print_r($_FILES);exit();
                          // return $images;
                        for($k=0;$k<count($images['name']);$k++){
                        $url = $this->Common_utill->svk_add_single_image($images,$k);
                        $product_short_desc="";
                        $attach = $this->Common_utill->add_attachment($package_product_name,$url,$package_product_id,6,$package_product_short_desc);

                        if($attach == 1){ 
                          if($k == count($images['name'])-1){
                            $em->flush();
                            return true;
                          }
                        }
                      }
                      }else{ //echo"xx";exit();
                        $em->flush();
                      return true;
                      }
                    }
                  
                  }
                }else if($images['error'][0]!= 4){
                  for($i=0;$i<count($images['name']);$i++){
                  $url = $this->Common_utill->svk_add_single_image($images,$i);
                  $product_short_desc='Package desc';
                  $attach = $this->Common_utill->add_attachment($package_product_name,$url,$package_product_id,6,$product_short_desc);
                  if($attach == 1){ 
                    if($i == count($images['name'])-1){
                      $em->flush();
                      return true;
                    }
                  }
                }
                }else{
                  $em->flush();
                  return true;
                }
              }else if($images['error'][0]!= 4){
                for($i=0;$i<count($images['name']);$i++){
                $url = $this->Common_utill->svk_add_single_image($images,$i);
                $product_short_desc='Package desc';
                $attach = $this->Common_utill->add_attachment($package_product_name,$url,$package_product_id,6,$product_short_desc);
                if($attach == 1){ 
                  if($i == count($images['name'])-1){
                    $em->flush();
                    return true;
                  }
                }
              }
              }





          }

          // if(count($attachment_object)>0){ 
          //  for($i=0;$i<count($attachment_object);$i++){
          //    $attachment_exists = $em->find('Entity\Attachments',$attachment_object[$i]->getAttachmentId());
          //    $em->remove($attachment_exists);
          //    $em->flush();
          //  }
          //  if($images['error'][0] != 4 && explode('~',$img_list)[0] == null){ 
          //    for($i=0;$i<count($images['name']);$i++){
          //      $url = $this->add_single_image($images,$i);
          //      $attach = $this->add_attachment($package_product_name,$url,$package_product_id,6,$package_product_short_desc);
          //      if($attach == 1){ 
          //        if($i == count($images['name'])-1){
          //          $em->flush();
          //          return true;
          //        }
          //      }else{
          //        return false;
          //      }
          //    }
          //  }elseif(explode('~',$img_list)[0] != null  && $images['error'][0] == 4){ 
          //    $img_array=explode('~',$img_list);
          //    for($i=0;$i<count($img_array);$i++){
          //      $attach = $this->add_attachment($package_product_name,$img_array[$i],$package_product_id,6,$package_product_short_desc);
          //      if($attach == 1){ 
          //        if($i == count($img_array)-1){
          //          $em->flush();
          //          return true;
          //        }
          //      }else{
          //        return false;
          //      }
          //    }
          //  }elseif(explode('~',$img_list)[0] != null  && $images['error'][0] != 4){
          //    for($i=0;$i<count($images['name']);$i++){ 
          //      $url = $this->add_single_image($images,$i);
          //      //$attach = $this->add_attachment($product_name,$url,$product_id,4,$product_short_desc);
          //      if($url != null){ 
          //        if($i == count($images['name'])-1){
          //          $img_array=explode('~',$img_list);
          //          if($img_array[0] != null){
          //            for($j=0;$j<count($img_array);$j++){
          //              $ar = explode('/',$img_array[$j]);
          //              $xx = "";
          //              if($ar[0] != 'attachments'){
          //                $xx = 'attachments/'.$img_array[$j];
          //              }else{
          //                $xx = $img_array[$j];
          //              }
          //              $attach = $this->add_attachment($package_product_name,$url,$package_product_id,6,$package_product_short_desc);
          //              if($attach == 1){ 
          //                if($j == count($img_array)-1){
          //                  $em->flush();
          //                  return true;
          //                }
          //              }else{
          //                return false;
          //              }
          //            }
          //          }else{
          //            return false;
          //          }
          //        }
          //      }else{
          //        return false;
          //      }
          //    }
          //  }else{
          //    return true;
          //  }
          // }else{ 
          //  if($images['error'][0] != 4 && explode('~',$img_list)[0] == null){ 
          //    if(count($images['name']) > 0) {
          //      for($i=0;$i<count($images['name']);$i++){
          //        $url = $this->add_single_image($images,$i);
          //        $attach = $this->add_attachment($package_product_name,$url,$package_product_id,6,$package_product_short_desc);
          //        if($attach == 1){ 
          //          if($i == count($images['name'])-1){
          //            return true;
          //          }
          //        }else{
          //          $em->flush();
          //          return false;
          //        }
          //      }
          //    }else{
          //      return true;
          //    }
              
          //  }elseif(explode('~',$img_list)[0] != null  && $images['error'][0] == 4){ 
          //    $img_array=explode('~',$img_list);
          //    for($i=0;$i<count($img_array);$i++){
          //      $attach = $this->add_attachment($package_product_name,$img_array[$i],$package_product_id,6,$package_product_short_desc);
          //      if($attach == 1){ 
          //        if($i == count($img_array)-1){
          //          $em->flush();
          //          return true;
          //        }
          //      }else{
          //        return false;
          //      }
          //    }
          //  }elseif(explode('~',$img_list)[0] != null  && $images['error'][0] != 4){
          //    for($i=0;$i<count($images['name']);$i++){ 
          //      $url = $this->add_single_image($images,$i);
          //      //$attach = $this->add_attachment($product_name,$url,$product_id,4,$product_short_desc);
          //      if($url != null){ 
          //        if($i == count($images['name'])-1){
          //          $img_array=explode('~',$img_list);
          //          if($img_array[0] != null){
          //            for($j=0;$j<count($img_array);$j++){
          //              $ar = explode('/',$img_array[$j]);
          //              $xx = "";
          //              if($ar[0] != 'attachments'){
          //                $xx = 'attachments/'.$img_array[$j];
          //              }else{
          //                $xx = $img_array[$j];
          //              }
          //              $attach = $this->add_attachment($package_product_name,$url,$package_product_id,6,$package_product_short_desc);
          //              if($attach == 1){ 
          //                if($j == count($img_array)-1){
          //                  $em->flush();
          //                  return true;
          //                }
          //              }else{
          //                return false;
          //              }
          //            }
          //          }else{
          //            return false;
          //          }
          //        }
          //      }else{
          //        return false;
          //      }
          //    }
          //  }else{
          //    $em->flush();
          //    return true;
          //  }
          // }
        }catch (UniqueConstraintViolationException $e){
          $em->getConnection()->rollBack();
          return false;
        }
      }else{
        return false;
      }
    }
    //Add End
  //packageProduct End

  //Reviews Start
    //Add Reviews Start
    public function add_review($review_star,$review_comment,$review_user_id,$review_item_id,$review_item_type,$review_status){
      $em = $this->doctrine->em;
      $review = new Entity\Reviews;
      $review->setReviewStar($review_star);
      $review->setReviewComment($review_comment);
      $review->setReviewUserId($review_user_id);
      $review->setReviewItemId($review_item_id);
      $review->setReviewItemType($review_item_type);
      $review->setReviewStatus($review_status);
      $em->persist($review);
      try {
        $em->flush();
        return true;
      }catch (UniqueConstraintViolationException $e){
        return false;
      }
    }
    //Add Reviews End

    //update Reviews Start
    public function update_review($review_id,$review_star,$review_comment,$review_user_id,$review_item_id,$review_item_type,$review_status){
      $em = $this->doctrine->em;
      $review = new Entity\Reviews;
      $find_review = $em->find('Entity\Reviews',$review_id);
      if($find_review != null){
        $find_review->setReviewStar($review_star);
        $find_review->setReviewComment($review_comment);
        $find_review->setReviewUserId($review_user_id);
        $find_review->setReviewItemId($review_item_id);
        $find_review->setReviewItemType($review_item_type);
        $find_review->setReviewStatus($review_status);
        $em->persist($find_review);
        try {
          $em->flush();
          return true;
        }catch (UniqueConstraintViolationException $e){
          return false;
        }
      }else{
        return false;
      }
    }
    //Update Reviews End
  //Reviews End
  //Access Start  
    public function create_access($access_name,$access_url,$find_module){
      $em = $this->doctrine->em;
      $access_obj = new Entity\Access;
      $access_obj->setAccessName($access_name);
      $access_obj->setAccessUrl($access_url);
      $access_obj->setModuleId($find_module);
      $em->persist($access_obj);
      try {
        $em->flush();
        return true;
      }catch (UniqueConstraintViolationException $e){
        return false;
      }
    }
    public function edit_access($access_id,$access_name,$access_url,$find_module){
      $em = $this->doctrine->em;
      $find_access = $em->getRepository('Entity\Access')->find($access_id); 
      // print_r($access_id);print_r($find_access);exit();
      if($find_access != null){
        $find_access->setAccessName($access_name);
        $find_access->setAccessUrl($access_url);
        $find_access->setModuleId($find_module);
        $em->persist($find_access);
        // print_r($find_access);exit();
        try {
          $em->flush();
          return true;
        }catch (UniqueConstraintViolationException $e){
          return false;
        }
      }else{
        return false;
      }
    }
    public function delete_access($access_id){
      $em = $this->doctrine->em;
      $find_access = $em->getRepository('Entity\Access')->find($access_id); 
      if($find_access != null){
        $em->remove($find_access);
        $data['access_obj'] = $find_access;
        try {
          $em->flush();
          return true;
        }catch (UniqueConstraintViolationException $e){
          return false;
        }
      }else{
        return false;
      }
    }
    public function edit_module($module_id,$module_name,$module_desc){
      $em = $this->doctrine->em;
      $find_module = $em->getRepository('Entity\Module')->find($module_id); 
      if($find_module != null){
        $find_module->setModuleName($module_name);
        $find_module->setModuleDesc($module_desc);
        $em->persist($find_module);
        try {
          $em->flush();
          return true;
        }catch (UniqueConstraintViolationException $e){
          return false;
        }
      }else{
        return false;
      }
    }
  //Access End  

  //String Operations Start
    public function remove_spaces($str){
      if(count($str)>0){
        return str_replace(' ', '', $str);
      }else{
        return false;
      }
    }
    public function remove_symbols($str){
      if(count($str)>0){
        return preg_replace("/[^A-Za-z0-9 ]/", '', $str);
      }else{
        return false;
      }
    }
  //String Operations End

  //User Module Funcitons Start
    //gets start
    public function get_profile_picture_url($user_id){
      $em = $this->doctrine->em;
      $find_user = $em->getRepository('Entity\User')->find($user_id); 
      if($find_user != null){
        $url = $find_user->getProfilePictureUrl();
        if($url != null){
          return $url;
        }else{return false;}
      }else{
        return -1;
      }
    }

    public function get_something_using_primary_key($table_name,$primary_key,$function_name){
      $em = $this->doctrine->em;
      $find = $em->getRepository('Entity\\'.$table_name)->find($primary_key); 
      if($find != null){
        $exist = $find->$function_name();
        if($exist != null){
          return $exist;
        }else{return false;}
      }else{
        return -1;
      }
    }
    public function get_single_row_using_primary_key($table_name,$primary_key){
      $em = $this->doctrine->em;
      $find = $em->getRepository('Entity\\'.$table_name)->find($primary_key); 
      if($find != null){
        return $find;
      }else{
        return false;
      }
    }
    public function get_single_row_using_array_value($table_name,$key_name,$key_value){
      $em = $this->doctrine->em;
      $find = $em->getRepository('Entity\\'.$table_name)->findOneBy(array($key_name => $key_value)); 
      if($find != null){
        return $find;
      }else{
        return false;
      }
    }
    public function get_multiple_rows_using_array_value($table_name,$key_name,$key_value){
      $em = $this->doctrine->em;
      $find = $em->getRepository('Entity\\'.$table_name)->findBy(array($key_name => $key_value)); 
      if($find != null){
        return $find;
      }else{
        return false;
      }
    }
    //gets start
    //sets start
    public function set_something_using_primary_key($table_name,$primary_key_value,$function_name,$value){
      $em = $this->doctrine->em;
      $find = $this->get_single_row_using_primary_key($table_name,$primary_key_value);
      if($find != null){
        $exist = $find->$function_name($value);
        $em->persist($exist);
        try{
          $em->flush();
          return true;
        }catch(UniqueConstraintViolationException $e){
          return false;
        }
      }else{
        return -1;
      }
    }
    public function set_something_using_array_value($table_name,$key_name,$key_value,$function_name,$value){
      $em = $this->doctrine->em;
      $find = $this->get_single_row_using_array_value($table_name,$key_name,$key_value); 
      if($find != null){
        $exist = $find->$function_name($value);
        $em->persist($exist);
        try{
          $em->flush();
          return true;
        }catch(UniqueConstraintViolationException $e){
          return false;
        }
      }else{
        return false;
      }
    }
    //sets end
    public function sent_mail($user_id,$to,$subject,$message,$headers){
      if( ! filter_var($user_id, FILTER_VALIDATE_INT) === false ){
        if(mail($to,$subject,$message,$headers)){
          return true;
        }else{
          return true;
        }
      }else{ return -1; }
    }
    public function create_verification_url(){
      $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));
        for ($i = 0; $i <= 15; $i++) {
            $key .= $keys[array_rand($keys)];
        }
        $url =base_url('index.php/login/user_verification?id='.$key);
        return $url;
    }
    public function send_verification_url($user_id){
      if( ! filter_var($user_id, FILTER_VALIDATE_INT) ){
        $exist = $this->get_something_using_primary_key('User',$user_id,'getUserId');
        if($exist != null){
          $url = $this->create_verification_url();
          $update = $this->set_something_using_primary_key('user',$user_id,'setUserVerificationUrl',$url);
          if($update == 1){
            $to = $exist->getUserEmail();
            $subject = "D-Login";
            $message = "Please click the verification url : ".$url;
            $headers = "From: webmaster@example.com" . "\r\n";
            return $url;
            $mail_status = $this->sent_mail($user_id,$to,$subject,$message,$headers);
            if($mail_status == 1){
              return true;
            }else{return false;}
          }else{return false;}
        }else{ return -1; }
      }else{ return -1; }
    }

    public function send_verification_url_with_url($user_id,$url){
      $em = $this->doctrine->em;
      if(  filter_var($user_id, FILTER_VALIDATE_INT) ){
        $user_obj = $em->getReference('Entity\User',$user_id);
        $exist = $this->get_something_using_primary_key('User',$user_id,'getUserId');
        if($exist != null){
          $update = $this->set_something_using_primary_key('user',$user_id,'setUserVerificationUrl',$url);
          if($update == 1){
            $to = $user_obj->getUserEmail();
            $subject = "D-Login";
            $message = "Please click the verification url : ".$url;
            $headers = "From: webmaster@example.com" . "\r\n";
            $mail_status = $this->sent_mail($user_id,$to,$subject,$message,$headers);
            if($mail_status){
              return true;
            }else{return false;}
          }else{return false;}
        }else{ return -1; }
      }else{ return -1; }
    }
  //User Module Funcitons End

  //Password Start
    public function create_password($password,$salt,$password_prev = null,$password_request_reset_password = 0,$password_req_date = null,$forget_password_url = null,$forget_password_validity = null,$question1,$answer1,$question2,$answer2,$question3,$answer3){
      $em = $this->doctrine->em;
      $password_obj = new Entity\Password;
      $password_obj->setPassword($password);
      $password_obj->setSalt($salt);
      $password_obj->setPasswordPrev($password_prev);
      $password_obj->setPasswordReqResetPassword($password_request_reset_password);
      $password_obj->setPasswordReqDate($password_req_date);
      $password_obj->setPasswordRequestOrNot(0);
      $password_obj->setForgetPasswordUrl($forget_password_url);
      $password_obj->setForgetPasswordValidity($forget_password_validity);
      $password_obj->setQuestion1($question1);
      $password_obj->setQuestion2($question2);
      $password_obj->setQuestion3($question3);
      $password_obj->setAnswer1($answer1);
      $password_obj->setAnswer2($answer2);
      $password_obj->setAnswer3($answer3);
      $em->persist($password_obj);
      try {
        $em->flush();
        //return $password_obj->getPasswordId();
        return $password_obj;
      }catch (UniqueConstraintViolationException $e){
        return false;
      }
    }
  //Password End
  //user add Start
    public function add_user($user_name,$user_email,$contact_id,$address_id,$nominee_obj,$password_id,$first_name,$last_name,$gender,$dob,$profile_picture_url,$user_role_id,$user_verification_url,$user_email_verified,$is_active,$last_login_time = null,$last_login_ip_address = null,$invalid_login_attempts = null,$last_invalid_login_time = null,$last_invalid_login_ip_address = null,$unlocked_reason = null,$images,$aadhar_no,$pan_no,$pin_store_password,$arr){
      //$exist_name = $this->get_single_row_using_array_value('User','user_name',$user_name);
      //$exist_id = $this->get_single_row_using_array_value('User','user_id',$user_name);
      // $exist_mail = $this->get_single_row_using_array_value('User','user_email',$user_email);
      $exist_name = null;
      $exist_id = null;
      $exist_mail = null;
      //print_r($exist_name);print_r($exist_mail);exit();
      if($exist_name == null && $exist_mail == null&& $exist_id == null){
        if($gender == null){$gender = 0;}
        $em = $this->doctrine->em;

        $query = $em->createQuery("SELECT MAX(b.user_id) FROM Entity\User as b" );
        $usr = $query->getResult();

        $user_obj = new Entity\User;
        $user_obj->setUserName($usr[0][1]+100000);
        $user_obj->setUserEmail($user_email);

        $user_obj->setContactId($contact_id);
        $user_obj->setNomineeId($nominee_obj);
        $user_obj->setPasswordId($password_id);
        $user_obj->setAddressId($address_id);
        
        $user_obj->setFirstName($first_name);
        $user_obj->setLastName($last_name);
        $user_obj->setGender($gender);
        if($dob != null){
          $user_obj->setDob($dob);
        }
        $user_obj->setProfilePictureUrl($profile_picture_url);
        $user_obj->setUserRoleId($user_role_id);
        $user_obj->setUserVerificationUrl($user_verification_url);
        $user_obj->setLastInvalidLoginTime($last_invalid_login_time);
        $user_obj->setLastInvalidLoginIpAddress($last_invalid_login_ip_address);
        $user_obj->setUnlockedReason($unlocked_reason);
        $user_obj->setUserEmailVerified($user_email_verified);
        $user_obj->setIsActive($is_active);
        $user_obj->setLastLoginTime($last_login_time);
        $user_obj->setLastLoginIpAddress($last_login_ip_address);
        $user_obj->setInvalidLoginAttempts($invalid_login_attempts);
        $user_obj->setPinStorePassword($pin_store_password);

        $user_obj->setPanVerified($arr['pan_verified']);
        $user_obj->setAadharVerified($arr['aadhar_verified']);
        $user_obj->setPassVerified($arr['pass_verified']);
        $user_obj->setKycStatus($arr['kyc_status']);

        $user_obj->onPrePersist();

        $user_obj->setAadharNo($aadhar_no);
        $user_obj->setPanNo($pan_no);
        $em->persist($user_obj);
        try {
          $em->flush();
          if($images['file']['error'] != 4){ 
            for($i=0;$i<count($images['file']['name']);$i++){

              $url = $this->add_id_single_image($images['file']);
            
              $product_short_desc='Primary';
              $product_name='primary';
              $attach = $this->add_attachment($product_name,$url,$user_obj->getUserId(),1,$product_short_desc);
              if($attach == 1){ 
                if($i == count($images['file']['name'])-1){
                  // print_r($user_obj->getUserName());echo"asdasd";exit();
                  return $user_obj->getUserId();
                }
              }else{
                return false;
              }
            }
          }else{
            // print_r($user_obj->getUserId());echo"xxxxxxxx";exit();
            return $user_obj->getUserId();
          }
        }catch (UniqueConstraintViolationException $e){
          return false;
        }
      }else{return -1;}
    }

    public function delete_user($user_id,$address_id,$contact_id,$password_id,$nominee_id){
      $em = $this->doctrine->em;
      $em->getConnection()->beginTransaction();
      $find_user = $em->getRepository('Entity\User')->find($user_id);
      if($find_user != null){
        $em->remove($find_user);
        try {
          //echo $user_id;
          $em->flush();
          //print_r($find_user->getContactId());exit();

          $contact = $this->delete_contact($contact_id);
          $address = $this->delete_address($address_id);
          $password = $this->delete_password($password_id);
          $password = $this->delete_nominee($nominee_id);
          $user_purchase = $this->user_purchase_delete_by_user_id($user_id);
          if($password == 1 && $address == 1 && $password == 1 && $user_purchase == 1){
            return true;
          }else{
            return false;
          }
        }catch (ForeignKeyConstraintViolationException $e){
          $em->getConnection()->rollBack(); 
          return false;
        }
      }else{
        return -1;
      }
    }
    public function user_purchase_delete_by_user_id($user_id){
      $em = $this->doctrine->em;
      $exist = $this->get_single_row_using_array_value('User_package_purchase','user_id',$user_id);
      if($exist != null){
        $up_id = $exist->getUserPurchaseId();
        $q = $em->createQuery("SELECT E FROM Entity\User_package_purchase E WHERE E.user_purchase_gene_left_purchase_id = $up_id OR E.user_purchase_gene_right_purchase_id = $up_id" );
        $users = $q->getResult();
        if($users != null){
          $oth_id = $users[0]->getUserPurchaseId();
          $function_name = "setUserPurchaseGeneRightPurchaseId";
          if($users[0]->getUserPurchaseGeneLeftPurchaseId() == $up_id ){
            $function_name = "setUserPurchaseGeneLeftPurchaseId";
          }
          $this->set_something_using_primary_key('User_package_purchase',$oth_id,$function_name,0);
          $find_prd = $em->getRepository('Entity\User_package_purchase')->find($up_id);
          if($find_prd != null){
            $em->remove($find_prd);
            $em->flush();
          }
        }
        $find_prd = $em->getRepository('Entity\User_package_purchase')->find($up_id);
        if($find_prd != null){
          $em->remove($find_prd);
          $em->flush();
        }
        
      }
      return true;
    }
  //user add End
  //user Access ids Start
    public function add_access_ids($user_id,$access_ids){
      $em = $this->doctrine->em;
      if(  filter_var($user_id, FILTER_VALIDATE_INT) ){
        $exist = $this->get_something_using_primary_key('User',$user_id,'getUserId');
        if($exist != -1 && $exist != null){
          $access_obj = new Entity\User_access;
          $access_obj->setUserAccessAccessId($access_ids);
          $access_obj->setUserAccessUserId($user_id);
          $em->persist($access_obj);
          try {
            $em->flush();
            return true;
          }catch (UniqueConstraintViolationException $e){
            return false;
          }
        }else{return false;}
      }else{
        return false;
      }
    }
    public function update_access_ids($user_id,$access_ids){

      $em = $this->doctrine->em;
      if(  filter_var($user_id, FILTER_VALIDATE_INT) ){
        $exist = $this->get_something_using_primary_key('User',$user_id,'getUserId');

        if($exist != -1 && $exist != null){
          $check = $this->get_single_row_using_array_value('User_access','user_access_user_id',$user_id);
          if($check != null){
            $update_access = $this->set_something_using_array_value('User_access','user_access_user_id',$user_id,'setUserAccessAccessId',$access_ids);
            if($update_access == 1){
              $em->flush();
              return true;
            }else{return false;}
          }else{
            $add_access = $this->add_access_ids($user_id,$access_ids);
            if($add_access == 1){
              $em->flush();
              return true;
            }else{return false;}
          }
        }else{return false;}
      }else{
        return false;
      }
    }
  //user Access ids End
  //Login Start
    public function login_check($user_name,$password){
      $em = $this->doctrine->em;
      if(strlen(trim($user_name))>=1 && strlen(trim($password))>=3){
        $exist = "";

        if($this->input->post('user_name') != null){
          $exist = $this->get_single_row_using_array_value('User','user_name',$user_name);
          if($exist == null){
             $exist = $this->get_single_row_using_primary_key('User',$user_name);
          }
        }else{
          $exist = $this->get_single_row_using_array_value('User','user_name',$user_name);
          if($exist == null){
             $exist = $this->get_single_row_using_primary_key('User',$user_name);
          }
        }

        if($exist != null){
          if($exist->getIsActive() == 1){
            $created_dt_obj = $exist->getCreatedAt();
            $created_at = $created_dt_obj->format('Y-m-d');
            $nowx = date("Y-m-d");
            $endx = date("Y-m-d", strtotime($created_at."+120 days"));
            $dd1=date_create($nowx);
            $dd2=date_create($created_at);
            $diffx=date_diff($dd1,$dd2);
            $notification = '';
            if($diffx->days <= 120){
              $pan_status = 1;
            }else{
              $pan_status = 0;
            }

            $rep_qry = $em->createQuery("SELECT p.pin_id FROM Entity\Pins as p  WHERE p.pin_request_for = 1 AND p.pin_status=0 AND p.pin_used_user_id =".$exist->getUserId());
            $rep_obj = $rep_qry->getResult();
            if(count($rep_obj) > 0){
              $rep_cnt = 1;
            }


            if($pan_status == 1){
              if($exist->getPasswordId()->getPassword() === $password){
                //$time = date("Y-m-d H:i:s");
                $access_ids = $this->get_single_row_using_array_value('User_access','user_access_user_id',$exist->getUserId());
                if($access_ids == null){
                  $access_ids = $this->get_single_row_using_primary_key('User_role',8);
                }else{
                  $access_ids = $access_ids->getUserAccessAccessId();
                }

                $attach  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $exist->getUserId(), 'attachment_referer_type'=>1));
                if($attach != null){
                  $attach_url = $attach[0]->getAttachmentImageUrl();
                }else{
                  $attach_url = "";
                }

                $time = null;
                $ipaddress = $_SERVER['REMOTE_ADDR'];
                $login_time = $this->set_something_using_primary_key('User',$exist->getUserId(),'setLastLoginTime',$time);
                $ip_address = $this->set_something_using_primary_key('User',$exist->getUserId(),'setLastLoginIpAddress',$ipaddress);
                $login_time = $this->set_something_using_primary_key('User',$exist->getUserId(),'setInvalidLoginAttempts',0);
                if($login_time == 1 && $ip_address == 1){
                  $login_data = array( 
                     'username'          =>   $exist->getUserName(), 
                     'user_id'           =>   $exist->getUserId(), 
                     'email'             =>   $exist->getUserEmail(), 
                               'profile_picture_url'     =>   $attach_url, 
                     'role_id'     =>   $exist->getUserRoleId()->getUserRoleId(), 
                     'access_ids'        =>   $access_ids,
                     'logged_in'         =>   1,
                     'unread_mail_count'     =>   null,
                  );
                  // print_r($login_data);exit();
                  $this->session->set_userdata($login_data);

                  if(isset($_SESSION['cart_items'])){
                    if(count($_SESSION['cart_items']) > 0){
                      $cart_items = $_SESSION['cart_items'];
                    }else{$cart_items = array(); }
                  }else{$cart_items = array(); }
                  $this->session->set_userdata('cart_items',$cart_items);
                  return true;
                }else{return false;}
              }else{
                $ipaddress = $_SERVER['REMOTE_ADDR'];
                $attmpts = $exist->getInvalidLoginAttempts();
                $login_time = $this->set_something_using_primary_key('User',$exist->getUserId(),'setInvalidLoginAttempts',$attmpts+1);
                $login_time = $this->set_something_using_primary_key('User',$exist->getUserId(),'setLastInvalidLoginTime',null);
                $ip_address = $this->set_something_using_primary_key('User',$exist->getUserId(),'setLastInvalidLoginIpAddress',$ipaddress);
                return -1;
              }
            }else{
              if($exist->getPanVerified() == 1 &&$exist->getAadharVerified() == 1 &&$exist->getPassVerified() == 1){
                if($exist->getPasswordId()->getPassword() === $password){
                  //$time = date("Y-m-d H:i:s");
                  $access_ids = $this->get_single_row_using_array_value('User_access','user_access_user_id',$exist->getUserId());
                  if($access_ids == null){
                    $access_ids = $this->get_single_row_using_primary_key('User_role',8);
                  }else{
                    $access_ids = $access_ids->getUserAccessAccessId();
                  }
                  $attach  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $exist->getUserId(), 'attachment_referer_type'=>1));
                  if($attach != null){
                    $attach_url = $attach[0]->getAttachmentImageUrl();
                  }else{
                    $attach_url = "";
                  }
                  $time = null;
                  $ipaddress = $_SERVER['REMOTE_ADDR'];
                  $login_time = $this->set_something_using_primary_key('User',$exist->getUserId(),'setLastLoginTime',$time);
                  $ip_address = $this->set_something_using_primary_key('User',$exist->getUserId(),'setLastLoginIpAddress',$ipaddress);
                  $login_time = $this->set_something_using_primary_key('User',$exist->getUserId(),'setInvalidLoginAttempts',0);
                  if($login_time == 1 && $ip_address == 1){
                    $login_data = array( 
                       'username'          =>   $exist->getUserName(), 
                       'user_id'           =>   $exist->getUserId(), 
                       'email'             =>   $exist->getUserEmail(), 
                                 'profile_picture_url'     =>   $attach_url, 
                       'role_id'     =>   $exist->getUserRoleId()->getUserRoleId(), 
                       'access_ids'        =>   $access_ids,
                       'logged_in'         =>   1,
                       'unread_mail_count'     =>   null,
                    );
                    $this->session->set_userdata($login_data);
                    if(isset($_SESSION['cart_items'])){
                      if(count($_SESSION['cart_items']) > 0){
                        $cart_items = $_SESSION['cart_items'];
                      }else{$cart_items = array(); }
                    }else{$cart_items = array(); }
                    $this->session->set_userdata('cart_items',$cart_items);
                    return true;
                  }else{return false;}
                }else{
                  $ipaddress = $_SERVER['REMOTE_ADDR'];
                  $attmpts = $exist->getInvalidLoginAttempts();
                  $login_time = $this->set_something_using_primary_key('User',$exist->getUserId(),'setInvalidLoginAttempts',$attmpts+1);
                  $login_time = $this->set_something_using_primary_key('User',$exist->getUserId(),'setLastInvalidLoginTime',null);
                  $ip_address = $this->set_something_using_primary_key('User',$exist->getUserId(),'setLastInvalidLoginIpAddress',$ipaddress);
                  return -1;
                }
              }else{
                $login_time = $this->set_something_using_primary_key('User',$exist->getUserId(),'setIsActive',0);
                return -4;
              }
            }
          }else{return 0;}
        }else{return -2;}
      }else{return -3;}
    }
    public function is_logged_in(){
      $user_name = $_SESSION['username'];
      $logged_in = $_SESSION['logged_in'];
      if($user_name != null && $logged_in == 1){
        return true;
      }else{return false;}
    }
    public function logout(){
      $user_name = $_SESSION['username'];
      $logged_in = $_SESSION['logged_in'];
      if($user_name != null && $logged_in == 1){
        $check = $this->get_single_row_using_array_value('User','user_name',$user_name);
        if($check != null){
          $login_time = $this->set_something_using_primary_key('User',$check->getUserId(),'setLastLoginTime',null);
          $login_data = array( 
             'username'          =>   "", 
             'user_id'           =>   "", 
             'email'             =>   "", 
             'profile_picture_url'     =>   null, 
             'access_ids'        =>   "",
             'logged_in'         =>   0,
             'unread_mail_count'     =>   null,
          );
          $this->session->set_userdata($login_data);
          if($login_time == 1){
            return true;
          }else{return false;}
        }else{return false;}
      }else{return false;}
    }
  //Login End

  //Reset Password Start
    public function create_reset_url(){
      $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));
        for ($i = 0; $i <= 15; $i++) {
            $key .= $keys[array_rand($keys)];
        }
        $url =base_url('index.php/login/reset?id='.$key);
        return $url;
    }
    public function send_reset_url($user_id){
      if( ! filter_var($user_id, FILTER_VALIDATE_INT) === false ){
        $exist = $this->get_single_row_using_primary_key('User',$user_id);
        if($exist != null){
          $url = $this->create_reset_url();
          $set_req_date = $this->set_something_using_primary_key('Password',$exist->getPasswordId()->getPasswordId(),'setPasswordReqDate',null);
          $set_req_or_not = $this->set_something_using_primary_key('Password',$exist->getPasswordId()->getPasswordId(),'setPasswordRequestOrNot',1);
          $update = $this->set_something_using_primary_key('Password',$exist->getPasswordId()->getPasswordId(),'setForgetPasswordUrl',$url);
          if($update == 1){
            return true;
            $to = $exist->getUserEmail();
            $subject = "D-Login";
            $message = "Please click the Reset url : ".$url;
            $headers = "From: webmaster@example.com" . "\r\n";
            $mail_status = $this->sent_mail($user_id,$to,$subject,$message,$headers);
            if($mail_status == 1){
              return true;
            }else{return false;}
          }else{return false;}
        }else{ return -1; }
      }else{ return -1; }
    }
  //Reset Password End

  //Update Reset-Password Start
    public function update_new_password($user_id,$password){
      if( ! filter_var($user_id, FILTER_VALIDATE_INT) === false ){
        $exist = $this->get_single_row_using_primary_key('User',$user_id);
        if($exist != null){
          $password_id = $exist->getPasswordId()->getPasswordId();
          $old_password = $exist->getPasswordId()->getPassword();

          $old_password_update = $this->set_something_using_primary_key('Password',$password_id,'setPasswordPrev',$old_password);
          $password_req_date_update = $this->set_something_using_primary_key('Password',$password_id,'setPasswordReqDate',null);
          $password_forget_url_update = $this->set_something_using_primary_key('Password',$password_id,'setForgetPasswordUrl',null);
          $password_forget_validity_update = $this->set_something_using_primary_key('Password',$password_id,'setForgetPasswordValidity',null);
          $password_forget_check_update = $this->set_something_using_primary_key('Password',$password_id,'setPasswordRequestOrNot',0);
          if($old_password_update == 1 && $password_req_date_update == 1 && $password_forget_url_update == 1 && $password_forget_check_update == 1 ){
            $password_update = $this->set_something_using_primary_key('Password',$password_id,'setPassword',md5($password));
            $salt_update = $this->set_something_using_primary_key('Password',$password_id,'setSalt',md5($password));
            if($password_update == 1 && $salt_update == 1){
              return true;
            }else{return false;}
          }else{return -1;}
        }else{return -2;}
      }else{ return -3; }
    }
  //Update Reset-Password End

  //Create Menu Start
    public function create_menu($user_id){

      $em = $this->doctrine->em;
      $access = $em->getRepository('Entity\User_access')->findBy(array('user_access_user_id'=>$user_id));
      $ar = explode(',',$access[0]->getUserAccessAccessId());
      // print_r($_SESSION);
      $module_ar = array();
      $url_ar = array();
      $abc = array();
      $access_ar = array();
      $access_tot_array = array();
      // print_r($ar);exit();
      foreach ($ar as $aa) { 
        $ac_obj = $em->getRepository('Entity\Access')->find($aa);

        if($ac_obj != null){
          if(!in_array($ac_obj->getModuleId()->getModuleName(),$module_ar)){
            $module_ar=array();
            $access_ar=array();
            array_push($module_ar, $ac_obj->getModuleId()->getModuleName());
            $md_obj = $em->getRepository('Entity\Access')->findBy(array('module_id' => $ac_obj->getModuleId()->getModuleId()));
            foreach ($md_obj as $obj) {
              if(in_array($obj->getAccessId(),$ar)){
                array_push($access_ar, implode('~',array('name'=> $obj->getAccessName(),'url'=>$obj->getAccessUrl())));
              }
            }
            array_push($abc,array_merge($module_ar,$access_ar));
          }
        }
      }
      return $abc;
      // foreach ($abc as $xx) {
      //  for($i=0;$i<count($xx);$i++){
      //    if($i == 0){
      //      echo "<h4>$xx[$i]</h4><br>";
      //    }else{
      //      echo $xx[$i];
      //    }
      //  }
      // }
      
    }
    public function create_menu_html($user_id){

      if( ! filter_var($user_id, FILTER_VALIDATE_INT) === false ){
        $exist = $this->get_single_row_using_primary_key('User',$user_id);

        if($exist != null){
          $menu = $this->create_menu($user_id);

          $mm = '<ul class="collapsible collapsible-accordion">
            <li class="bold">
            <a class="has-arrow ripple" href="'.base_url('index.php/login/dashboard').'">
                          <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                          <span class="sidebar-nav-item ripple">Dashboard</span>
                      </a>
          ';
  //print_r($menu);
          foreach ($menu as $xx) {
            for($i=0;$i<count($xx);$i++){
              
              if($i == 0){
                $mm.='<li class="bold '.strtolower($xx[$i]).'_crud"><a class="has-arrow ripple" href="#">
                                <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                                <span class="sidebar-nav-item ripple">'.$xx[$i].'</span>
                            </a><ul>';
              }else{
                $ar = explode('~',$xx[$i]);
                $mm.='<li class="wow fadeInLeft ripple '.strtolower($ar[0]).'" data-wow-duration="0.2s">
                                <a href="'.base_url().'index.php/'.$ar[1].'">'.$ar[0].'</a>
                            </li>';
              }
              if($i == count($xx)-1){
                $mm.='</ul>';
              }
            }
          }
          $mm.='</li></ul>';
          return $mm;
        }else{ return false; }
      }else{ return false; }
    }
  //Create Menu End

  //Create Search start

    public function search_field($table_name, $primary_key, $column_name, $controller_name, $function_name, $text_box_name){
      $em = $this->doctrine->em;
      $records = $em->getRepository("Entity\\".$table_name)->findAll();
      if(count($records)>0){
        $data = '<div class="page-content text-center"><section class="content bgcolor-1"><form method="POST" action="'.base_url(''.$controller_name.'/'.$function_name.'"').'>
        <select name="'.$text_box_name.'" id="combobox" class="floating-label disabled validate">';
        foreach ($records as $res) {
          $data .= ' <option value="'.$res->$primary_key().'">'.$res->$column_name().'</option>';
        }
        $data .= '</select>';
        $data .='<i class="fa fa-search select_search" onclick="$(this).closest(\'form\').submit();"></i></div></form></section></div>';
        return $data;
      }
    }

    public function search_field_two_column($table_name, $primary_key, $column_name1,$column_name2, $controller_name, $function_name, $text_box_name){
      $em = $this->doctrine->em;
      $records = $em->getRepository("Entity\\".$table_name)->findAll();
      if(count($records)>0){
        $data = '<div class="input-field col s4"><form method="POST" action="'.base_url(''.$controller_name.'/'.$function_name.'"').'>
        <select name="'.$text_box_name.'" id="combobox" class="floating-label disabled validate">';
        foreach ($records as $res) {
          $data .= ' <option value="'.$res->$primary_key().'">'.$res->$column_name1()->$column_name2().'</option>';
        }
        $data .= '</select>';
        $data .= '<i class="mdi-action-pageview pink_color s_but" onclick="$(this).closest(\'form\').submit();"></i></form></div>';
        return $data;
      }
    }
  //Create Search End

  //package product start
    public function get_products_by_package_id($package_id){
      $em = $this->doctrine->em;
      $find = $em->getRepository('Entity\Package_products')->findBy(array('package_id' => $package_id));
      if($find != null){
        return $find;
      }else{ return false; }
    }
    public function get_package_products_by_packages_id($package_id){
      $em = $this->doctrine->em;
      $ar = array();
      if(is_numeric($package_id)){
        $prd = $this->get_products_form_package_id($package_id);
        for($i=0;$i<count($prd);$i++){
          array_push($ar,$this->get_product_detail_from_product_id_for_cart($prd[$i]['product_id']));
        }

        return $ar;        
      }else{
        $val = implode(',', $package_id);
        $q = $em->createQuery("SELECT E FROM Entity\Package_product_items E WHERE E.package_product_id IN ($val)" );
        $users = $q->getResult();
        if($users != null){
          return $users;
        }else{ return false; }
      }
    }
    public function get_products_form_package_id($package_id){
      $em = $this->doctrine->em;
      $query = 'select prd.product_id,prd.product_name,prd.product_price,prd.point_value,prd.product_short_desc from Entity\Packages as p INNER JOIN Entity\Package_products as pp WITH p.package_id = pp.package_id INNER JOIN Entity\Package_product_items as ppt WITH ppt.package_product_id = pp.package_product_id INNER JOIN Entity\Product as prd WITH prd.product_id = ppt.package_product_item_id WHERE prd.product_is_visible = 1 AND pp.package_id ='.$package_id;
      $qry = $em->createQuery($query);
      return $qry->getResult();
    }
    public function get_user_choice_products(){
      $em = $this->doctrine->em;
      $ar = array();
      $query = 'SELECT prd.product_id,prd.product_name,prd.product_price,prd.point_value,prd.product_short_desc,prd.product_display_price FROM Entity\Product as prd WHERE prd.product_is_deleted = 2';
      $qry = $em->createQuery($query);
      $prd = $qry->getResult();
      for($i=0;$i<count($prd);$i++){
        array_push($ar,$this->get_product_detail_from_product_id_for_cart($prd[$i]['product_id']));
      }
      return $ar;
    }
    public function get_product_details_from_product_id($product_id){
      $em = $this->doctrine->em;
      $prd_ar['name'] = array();
      $prd_ar['code'] = array();
      $prd_ar['price'] = array();
      $prd_ar['short_desc'] = array();
      $prd_ar['attach_url'] = array();
      //$prd_ar = array();
      for($i=0;$i<count($product_id);$i++){
        $product_obj = $em->getRepository('Entity\Product')->find($product_id[$i]);
        $product_att_obj = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_id' => $product_id[$i],'attachment_referer_type'=>4));
        if($product_obj != null){
          array_push($prd_ar['code'], $product_obj->getProductCode());
          array_push($prd_ar['name'], $product_obj->getProductName());
          array_push($prd_ar['price'], $product_obj->getProductPrice());
          array_push($prd_ar['short_desc'], $product_obj->getProductShortDesc());
          if($product_att_obj != null){
            array_push($prd_ar['attach_url'], $product_att_obj->getAttachmentImageUrl());
          }else{
            array_push($prd_ar['attach_url'], 0);
          }
          
        }else{return 0;}
      }

      return $prd_ar;
    }
    public function get_product_detail_from_product_id_for_cart($product_id){
      $em = $this->doctrine->em;
      $product_obj = $em->getRepository('Entity\Product')->find($product_id);
      if( $product_obj != null ){
        $product_att_obj = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_id' => $product_id,'attachment_referer_type'=>4));
        if($product_att_obj != null){
          if(!is_numeric($product_att_obj->getAttachmentImageUrl())){
            $image_url = $product_att_obj->getAttachmentImageUrl();
          }else{$image_url = 0;}
        }else{$image_url = 0;}
        $cat_name = '';
        $ven_name = '';
        if($product_obj->getCategoryId() != null){
          $cat_name = $product_obj->getCategoryId()->getCategoryName();
        }
        if($product_obj->getProductVendorId() != null){
          $ven_name = $product_obj->getProductVendorId()->getVendorName();
        }
        $prd_ar = array(
          'product_id'      => $product_obj->getProductId(),
          'product_code'      => $product_obj->getProductCode(),
          'product_name'      => $product_obj->getProductName(),
          'product_image_url'   => $image_url,
          'product_price'     => $product_obj->getProductPrice(),
          'product_display_price'     => $product_obj->getProductDisplayPrice(),
          'point_value'     => $product_obj->getPointValue(),
          'product_quantity'    => $product_obj->getProductQuantity(),
          'product_short_desc'  => explode('^', $product_obj->getProductShortDesc()),
          'product_desc'      => explode('^', $product_obj->getProductDesc()),
          'product_is_visible'  => $product_obj->getProductIsVisible(),
          'category_name'  => $cat_name,
          'vendor_name'  => $ven_name,
        );
        return $prd_ar;
      }else{return 0;}
    }
    public function get_package_details_from_product_id($product_id){
      $em = $this->doctrine->em;
      $pack_product_obj = $em->getRepository('Entity\Package_product_items')->findOneBy(array('package_product_item_product_id'=>$product_id));
      if($pack_product_obj != null){
        $pack_product_id = $pack_product_obj->getPackageProductId();
        $packprd_obj = $em->getRepository('Entity\Package_products')->find($pack_product_id);
        if($packprd_obj != null){
          $pack_obj = $packprd_obj->getPackageId();
          if($pack_obj != null){
            $product_att_obj = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $pack_obj->getPackageId(),'attachment_referer_type'=>5));
            $image_sub = array();
            $image_main = 0;
            if($product_att_obj != null){
              if($product_att_obj[0]->getAttachmentImageUrl()!= 0 && $product_att_obj[0]->getAttachmentImageUrl()[0]!=-1){
                $image_main = $product_att_obj[0]->getAttachmentImageUrl();
              }
              for($i=1;$i<count($product_att_obj);$i++){
                array_push($image_sub, $product_att_obj[$i]->getAttachmentImageUrl());
              }
            }
            $pack_ar = array(
              'package_id'          =>  $pack_obj->getPackageId(),
              'package_name'          =>  $pack_obj->getPackageName(),
              'package_desc'          =>  $pack_obj->getPackageDesc(),
              'package_short_desc'      =>  $pack_obj->getPackageShortDesc(),
              'package_price'         =>  $pack_obj->getPackagePrice(),
              'package_limited_pair_cut_off'  =>  $pack_obj->getPackageLimitedPairCutoff(),
              'package_pair_income'     =>  $pack_obj->getPackagePairIncome(),
              'package_ceiling_per_month'   =>  $pack_obj->getPackageCeilingPerMonth(),
              'package_ceiling_price'     =>  $pack_obj->getPackageCeilingPrice(),
              'package_is_visible'      =>  $pack_obj->getPackageIsVisible(),
              'image_main'          =>  $image_main,
              'image_sub'           =>  $image_sub,
            );
            return $pack_ar;
          }else{return 0;}
        }else{return 0;}
      }else{return 0;}
    }
    public function get_package_and_product_object_form_product_id($product_id){
      return array('product_obj'=>$this->get_product_detail_from_product_id_for_cart($product_id),'package_obj'=>$this->get_package_details_from_product_id($product_id));
    }
  //package product end
                        
        //$em=$this->doctrine->em;
        //$q = $em->createQuery("SELECT E.user_name,V FROM Entity\Cf_user_messages V INNER JOIN Entity\Cf_user E WITH V.user_messages_to_id = E.user_id WHERE V.user_messages_to_id=$user_id AND V.user_messages_is_delete = 0 ORDER BY V.user_messages_received_date DESC" );
        //return $users = $q->getResult();
  //Pin request start
    public function add_pin_request($pin_request_user_obj,$package_obj,$package_product_obj,$pin_req_quantity,$pin_request_for,$pin_request_status,$pin_request_declianed_reason){

      $em = $this->doctrine->em;
      $pin = new Entity\Pin_request;
      $pin->setPinRequestUserId($pin_request_user_obj);
      $pin->setPinRequestPackageId($package_obj);
      $pin->setPinRequestPackageProductId($package_product_obj);
      $pin->setPinRequestQuantity($pin_req_quantity);
      $pin->setPinRequestRepurchase($pin_request_for);
      $pin->setPinRequestStatus($pin_request_status);
      $pin->setPinRequestDeclianedReson($pin_request_declianed_reason);
      $pin->onPrePersist();
      //$pin->onPreUpdate();

      $em->persist($pin);
      try {
        $em->flush();
        return $pin;
      }catch (ForeignKeyConstraintViolationException $e){ return false; }

    }
    public function add_pin($pin_no,$pin_requested_user_id,$pin_used_user_id,$pin_status,$pin_validity_time,$pin_remarks,$pin_used_time,$pin_request_package_id,$pin_request_package_product_id,$pin_request_for){

      $em = $this->doctrine->em;
      $pin = new Entity\Pins;
      $pin->setPinNo($pin_no);
      $pin->setPinRequestedUserId($pin_requested_user_id);
      $pin->setPinUsedUserId($pin_used_user_id);
      $pin->setPinStatus($pin_status);
      $pin->setPinValidityTime($pin_validity_time);
      $pin->setPinRemarks($pin_remarks);
      $pin->setPinUsedTime($pin_used_time);
      if($pin_request_package_id != null){
        $pin->setPinRequestPackageId($pin_request_package_id);
      }
      if($pin_request_package_product_id != null){
        $pin->setPinRequestPackageProductId($pin_request_package_product_id);
      }
      $pin->setPinRequestFor($pin_request_for);
      $pin->setPinTransferUserId(0);
      $pin->onPrePersist();
      //$pin->onPreUpdate();

      $em->persist($pin);
      try {
        $em->flush();
        // return $pin->getPinNo();
        return $pin;
        // return true;
      }catch (ForeignKeyConstraintViolationException $e){ return false; }

    }
    public function get_pin_number(){
      $length = rand(8,25);
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
      //return mt_rand(10000, mt_getrandmax());
    }
  //Pin request End
    public function get_reference_obj($table_name,$primary_key){
      $em = $this->doctrine->em;
      return $em->getReference('Entity\\'.$table_name,$primary_key);
    }

  //Purchase request Start
    public function add_purchase_request($vendor_obj,$purchase_request_number,$date,$request_status,$type,$bill_or_req_id,$approve_id,$reson){
      $dd = new DateTime($date);
      $em = $this->doctrine->em;
      $purchase_obj = new Entity\Purchase_request();
      $purchase_obj->setPurchaseRequestVendorId($vendor_obj);
      $purchase_obj->setPurchaseRequestNo($purchase_request_number);
      $purchase_obj->setPurchaseRequestEstimatedDeliveryDate($dd);
      $purchase_obj->setPurchaseRequestStatus($request_status);
      $purchase_obj->setPurchaseType($type);
      $purchase_obj->setPurchaseRequestBillOrRequestId($bill_or_req_id);
      $purchase_obj->setApprovedUserId($approve_id);
      $purchase_obj->setResonFor($reson);
      $purchase_obj->onPrePersist();
      $em->persist($purchase_obj);
      try {
        $em->flush();
        return $purchase_obj;
      }catch (UniqueConstraintViolationException $e){ return false; }
    }
    public function update_purchase_request($purchase_request_id,$date,$request_status){
      $em = $this->doctrine->em;
      $obj = $this->get_reference_obj('Purchase_request',$purchase_request_id);
      if($obj != null){
        $obj->setPurchaseRequestEstimatedDeliveryDate($date);
        $obj->setPurchaseRequestStatus($request_status);
        $obj->setPurchaseType(2);
        $obj->setApprovedUserId(2);
        $obj->onPrePersist();
        $em->persist($obj);
        try {
          $em->flush();
          return $obj;
        }catch (UniqueConstraintViolationException $e){ return false; }
      }else{
        return false;
      }
    }

    public function add_purchase_request_products($product_obj,$quantity,$pur_obj,$ppq_price){
      $em = $this->doctrine->em;
      $req_obj = new Entity\Purchase_request_products();
      $req_obj->setPurchaseRequestProductProductId($product_obj);
      $req_obj->setPurchaseRequestProductProductQuantity($quantity);
      $req_obj->setPurchaseRequestId($pur_obj);
      $req_obj->setPpqPrice($ppq_price);
      $em->persist($req_obj);
      try {
        $em->flush();
        return true;
      }catch (UniqueConstraintViolationException $e){ return false; }
    }
      public function delete_product_req_products($product_obj){

      $em = $this->doctrine->em;  
      if(count($product_obj)>0){
        for($i=0;$i<count($product_obj);){
          $id = $product_obj[$i]->getPurchaseRequestProductId();
          $find_prd = $em->getRepository('Entity\Purchase_request_products')->find($id);
          if($find_prd != null){
            $em->remove($find_prd);
            if($i == count($product_obj)-1){
              return true;
            }
            $i++;
          }else{
            return false;
          }
        }
      }else{
        return true;
      }
    }
  //Purchase request End


  //Tree Start
    public function get_empty_child_nodes(){
      $em = $this->doctrine->em;

      $q = $em->createQuery("SELECT E from Entity\User_package_purchase E WHERE E.user_purchase_gene_left_purchase_id =0 OR E.user_purchase_gene_right_purchase_id =0 AND E.user_purchase_sponser_user_purchase_id != E.user_id");
      //$q = $em->createQuery("SELECT E from Entity\User E ");

      $users = $q->getResult();

      return $users;
    }

    public function get_total_empty_nodes_sample_app(){
      $em = $this->doctrine->em;
      $q = $em->createQuery("SELECT E from Entity\Sample_upp E WHERE E.user_purchase_gene_left_purchase_id = 0 OR E.user_purchase_gene_right_purchase_id = 0");
      $users = $q->getResult();
      if(count($users) > 0){
        return $users;
      }else{return 0;}
    }
    public function get_node_has_children_sample_app($sponcer_user_puchase_id,$side){
      $em = $this->doctrine->em;
      $sample_obj = $em->getRepository('Entity\Sample_upp')->find($sponcer_user_puchase_id);
      if($sample_obj != null){
        $left_purchase_id = $sample_obj->getUserPurchaseGeneLeftPurchaseId();
        $right_purchase_id = $sample_obj->getUserPurchaseGeneRightPurchaseId();
        if($left_purchase_id == null && $right_purchase_id == null ){
          return 1;
        }elseif($left_purchase_id == null && $side == 0 ){
          return 1;
        }elseif($right_purchase_id == null && $side == 1){
          return 1;
        }else{
          return 2;
        }
      }else{ return -5; }
    }

    public function set_sub_node_sample_upp($actual_gene_up_id,$side, $purchase_id){

      $em = $this->doctrine->em;
      $sample_obj = $em->getRepository('Entity\Sample_upp')->find($actual_gene_up_id);
      
      if($sample_obj != null){
        if($side == 1){ 
          //echo"asdd";exit();
          $sample_obj->setUserPurchaseGeneRightPurchaseId($purchase_id);

          $pr_cnt = $sample_obj->getRightPairCount();
          $sample_obj->setRightPairCount($pr_cnt+1);
          $em->persist($sample_obj);
          $em->flush(); 
          $left = $sample_obj->getUserPurchaseGeneLeftPurchaseId();
          
            //echo $left;exit();
          if($left == 0){
            if($sample_obj->getUserPurchaseActualGeneUserPurchaseId() == 0){
              return true;
            }else{
              return $this->increase_count_sample_app($sample_obj->getUserPurchaseActualGeneUserPurchaseId(),$side);
            }
          }else{
            return $this->increase_count_sample_app($purchase_id,1);
          }
        }else{

          $sample_obj->setUserPurchaseGeneLeftPurchaseId($purchase_id);
          $prl_cnt = $sample_obj->getLeftPairCount();
          $sample_obj->setLeftPairCount($prl_cnt+1);
          $em->persist($sample_obj);
          $em->flush(); 
          $right = $sample_obj->getUserPurchaseGeneRightPurchaseId();
          // echo $right;echo"yy";exit();
                  
          if($right == 0){

            if($sample_obj->getUserPurchaseActualGeneUserPurchaseId() == 0){
              
              return true;
            }else{
              return $this->increase_count_sample_app($sample_obj->getUserPurchaseActualGeneUserPurchaseId(),0);
            }
          }else{
            return $this->increase_count_sample_app($purchase_id,0);
          }
        }
      }else{ return -5; }
    }

    public function increase_count_sample_app($actual_gene_purchase_id,$side){
      $em = $this->doctrine->em;
      
        if($side == 1){
          $sample_obj = $em->getRepository('Entity\Sample_upp')->find($actual_gene_purchase_id);
          if($sample_obj != null){
            $cnt = $sample_obj->getRightPairCount();
            $sample_obj->setRightPairCount($cnt+1);
            $em->persist($sample_obj);
            if($sample_obj->getUserPurchaseActualGeneUserPurchaseId() == 0){
              $em->flush(); 
              return true;
            }else{
              return $this->increase_count_sample_app($sample_obj->getUserPurchaseActualGeneUserPurchaseId(),1);
            }
          }else{return -5;}
        }else{
          $sample_obj = $em->getRepository('Entity\Sample_upp')->find($actual_gene_purchase_id);
          if($sample_obj != null){
            $cnt = $sample_obj->getLeftPairCount();
            $sample_obj->setLeftPairCount($cnt+1);
            $em->persist($sample_obj);
            if($sample_obj->getUserPurchaseActualGeneUserPurchaseId() == 0){
              $em->flush(); 
              return true;
            }else{
              return $this->increase_count_sample_app($sample_obj->getUserPurchaseActualGeneUserPurchaseId(),0);
            }
          }else{return -5;}
        }
      
    }

    public function increase_tot_count_sample_upp($sp_id){
      $em = $this->doctrine->em;
      if($sp_id != null){
        $sponcer_obj = $em->getRepository('Entity\Sample_upp')->find($sp_id);
        $tot = $sponcer_obj->getTotalCount();
        $sponcer_obj->setTotalCount($tot+1);
        $em->persist($sponcer_obj);
              $em->flush(); 
        return 1;
      }else{
        return -5;
      }
    }

    public function increase_node_sample_upp($actual_gene_up_id,$actual_gene_side){
    }
    public function get_left_empty_node_sample_upp($sponcer_user_puchase_id,$side){
      $em = $this->doctrine->em;
      $sample_obj = $em->getRepository('Entity\Sample_upp')->findOneBy(array('user_purchase_sponser_user_purchase_id'=>$sponcer_user_puchase_id));
      if($sample_obj != null){
        if($side == 0){
          if($sample_obj->getUserPurchaseGeneLeftPurchaseId() == 0){
            return $sample_obj->getUserPurchaseId();
          }else{
            return $this->get_empty_node_given_pur_id_sample_upp($sample_obj->getUserPurchaseId(),0);
          }
        }else{
          if($sample_obj->getUserPurchaseGeneRightPurchaseId() == 0){
            return $sample_obj->getUserPurchaseId();
          }else{
            return $this->get_empty_node_given_pur_id_sample_upp($sample_obj->getUserPurchaseId(),1);
          }
        }
        
      }else{
        return -5;
      }
    }

    public function get_left_node_sample_upp($sponcer_user_puchase_id,$side){
      $em = $this->doctrine->em;
      $sample_obj = $em->getRepository('Entity\Sample_upp')->findOneBy(array('user_purchase_id'=>$sponcer_user_puchase_id));
      if($sample_obj != null){
        if($sample_obj->getUserPurchaseGeneLeftPurchaseId() == 0){
          return $sample_obj->getUserPurchaseId();
        }else{
          return $this->get_left_node_sample_upp($sample_obj->getUserPurchaseGeneLeftPurchaseId(),0);
        }
      }else{
        return -5;
      }
    }

    public function get_right_node_sample_upp($sponcer_user_puchase_id,$side){
      $em = $this->doctrine->em;
      $sample_obj = $em->getRepository('Entity\Sample_upp')->findOneBy(array('user_purchase_id'=>$sponcer_user_puchase_id));
      if($sample_obj != null){
        if($sample_obj->getUserPurchaseGeneRightPurchaseId() == 0){
          return $sample_obj->getUserPurchaseId();
        }else{
          return $this->get_right_node_sample_upp($sample_obj->getUserPurchaseGeneRightPurchaseId(),1);
        }
      }else{
        return -5;
      }
    }


    public function get_empty_node_given_pur_id_sample_upp($sponcer_user_puchase_id,$side){
      $em = $this->doctrine->em;
      $sample_obj = $em->getRepository('Entity\Sample_upp')->findOneBy(array('user_purchase_id'=>$sponcer_user_puchase_id));
      if($sample_obj != null){
        $this->get_left_empty_node_sample_upp($sample_obj,$side);       
      }else{
        return -5;
      }
    }

    public function set_sub_node($actual_gene_up_id,$actual_gene_side, $purchase_id){
      $em = $this->doctrine->em;
      $sample_obj = $em->getRepository('Entity\User_package_purchase')->find($actual_gene_up_id);
      if($sample_obj != null){
        if($actual_gene_side == 1){
          $sample_obj->setUserPurchaseGeneRightPurchaseId($purchase_id);
          $em->persist($sample_obj);
          $em->flush(); 
          return true;
        }else{
          $sample_obj->setUserPurchaseGeneLeftPurchaseId($purchase_id);
          $em->persist($sample_obj);
          $em->flush(); 
          return true;
        }
      }else{ return -5; }
    }

    
    public function get_left_node($node){
      $em = $this->doctrine->em;
      //$sample_obj = $em->getRepository('Entity\Sample')->find($id); 
      $sample_obj = $em->getRepository('Entity\Sample')->findOneBy(array('node_name'=>$node));  
      if($sample_obj != null){
        return $sample_obj->getLef();
      }else{
        return false;
      }
    }
    // public function get_node_has_children($node){
    //  $em = $this->doctrine->em;
    //  //$sample_obj = $em->getRepository('Entity\Sample')->find($id); 
    //  $sample_obj = $em->getRepository('Entity\Sample')->findOneBy(array('node_name'=>$node));  
    //  if($sample_obj != null){
    //    $left = $sample_obj->getLef();
    //    $right = $sample_obj->getRit();
    //    if($left == null && $right == null){
    //      return true;
    //    }elseif($left != null && $right == null){
    //      return true;
    //    }elseif($left == null && $right != null){
    //      return true;
    //    }else{
    //      return false;
    //    }
    //  }else{
    //    return false;
    //  }
    // }
    public function get_node_has_children($sponcer_user_puchase_id){
      $em = $this->doctrine->em;
      $sample_obj = $em->getRepository('Entity\User_package_purchase')->find($sponcer_user_puchase_id);
      if($sample_obj != null){
        $left_purchase_id = $sample_obj->getUserPurchaseGeneLeftPurchaseId();
        $right_purchase_id = $sample_obj->getUserPurchaseGeneRightPurchaseId();
        if($left_purchase_id != null && $right_purchase_id != null){
          return 1;
        }elseif($left_purchase_id == null && $right_purchase_id != null){
          return 2;
        }elseif($left_purchase_id != null && $right_purchase_id == null){
          return 3;
        }else{
          return 4;
        }
      }else{ return -5; }
    }
    public function get_left_empty_node_xx($sponcer_user_puchase_id,$side){
      $em = $this->doctrine->em;
      $sample_obj = $em->getRepository('Entity\User_package_purchase')->findOneBy(array('user_purchase_sponser_user_purchase_id'=>$sponcer_user_puchase_id,'user_purchase_sponser_gene_side'=>$side));
      if($sample_obj != null){
        return $sample_obj->getUserPurchaseId();
      }else{
        return -5;
      }
    }
    public function get_right_node($node){
      $em = $this->doctrine->em;
      //$sample_obj = $em->getRepository('Entity\Sample')->find($id); 
      $sample_obj = $em->getRepository('Entity\Sample')->findOneBy(array('node_name'=>$node));  
      if($sample_obj != null){
        return $sample_obj->getRit();
      }else{
        return false;
      }
    }
    public function get_root_of_node($node){
      $em = $this->doctrine->em;
      //$sample_obj = $em->getRepository('Entity\Sample')->find($id); 
      $sample_obj = $em->getRepository('Entity\Sample')->findOneBy(array('node_name'=>$node));  
      if($sample_obj != null){
        return $sample_obj->getTop();
      }else{
        return false;
      }
    }
    public function get_position_of_node($node){
      $em = $this->doctrine->em;
      //$sample_obj = $em->getRepository('Entity\Sample')->find($id); 
      $sample_obj = $em->getRepository('Entity\Sample')->findOneBy(array('node_name'=>$node));  
      if($sample_obj != null){
        $top =  $sample_obj->getTop();
        $top_obj = $em->getRepository('Entity\Sample')->findOneBy(array('node_name'=>$top));  
        if($top_obj != null){
          $left = $top_obj->getLef();
          $right = $top_obj->getRit();
          if($left == $node){
            return 0;
          }elseif($right == $node){
            return 1;
          }else{return -1;}
        }else{return false;}
      }else{
        return false;
      }
    }

    public function get_empty_nodes($node){
      $em = $this->doctrine->em;
      $sample_obj = $em->getRepository('Entity\Sample')->findOneBy(array('node_name'=>$node));  
      if($sample_obj != null){
        $right =  $sample_obj->getRit();
        $main_obj = $em->getRepository('Entity\Sample')->findBy(array('top'=>$right));
        if($main_obj != null){
          $obj = $this->get_ref($main_obj);
          if($obj != null){

            $obj = $this->get_ref($obj);
            return $obj;
          }else{
            //return $obj;
          }
        }else{return $right;}
      }else{//return false;
      }
    }

    public function get_ref($obj){
      $em = $this->doctrine->em;
      $ar = array();
      for($i=0;$i<count($obj);$i++){
        $main_obj = $em->getRepository('Entity\Sample')->findBy(array('top'=>$obj[$i]->getNodeName()));
        if($main_obj == null){
          array_push($ar, $obj[$i]);
        }
      }
      print_r($ar);echo"<br><br><br>";
      return $ar;
    }

    public function get_ety_nod($node){
      $em = $this->doctrine->em;
      $sample_obj = $em->getRepository('Entity\Sample')->findOneBy(array('node_name'=>$node));
      if($sample_obj != null){
        $right =  $sample_obj->getRit();
        if($right == null){
          array_push($ar, $right);
          return $ar;
        }else{
          $right =  $sample_obj->getRit();
          while($right == null){
            
          }
        }
      }
    }
    
  //Tree End


  //svk metro Start
    public function check_pin_get_details($pin,$for){
        $obj = new Entity\Pins;
        $pin_obj = $this->get_single_row_using_array_value('Pins','pin_no',$pin);
        if($pin_obj != null){
          if($for == $pin_obj->getPinRequestFor()){
            if( $pin_obj->getPinStatus() == 1 ){
              if($pin_obj->getPinTransferUserId() == 0){
                if($pin_obj->getPinRequestedUserId()->getUserId() == $_SESSION['user_id']){
                  return $pin_obj;
                }else{return -5;}
              }else{
                if($pin_obj->getPinTransferUserId() == $_SESSION['user_id']){
                  return $pin_obj;
                }else{return -5;}
              }
            }elseif( $pin_obj->getPinStatus() == 0 ){ 
              return -3;
            }else{ return -2; }
          }else{ return -4; }
        }else{return -1;}
    }
    public function check_pin_is_valid($id){
      if (!filter_var($id, FILTER_VALIDATE_INT) === false) {
        $obj = new Entity\Pins;
        $pin_obj = $this->get_single_row_using_primary_key('Pins',$id);
        if($pin_obj != null){
          if($pin_obj->getPinStatus() == 1){
            return 1;
          }else{ return -4; }
        }else{return -1;}
      }else{return -1;}
    }
    public function update_pin($pin_id,$pin_used_user_id,$pin_status,$pin_used_time){
      $em = $this->doctrine->em;
      $obj = $this->get_reference_obj('Pins',$pin_id);
      if($obj != null){
        $obj->setPinUsedUserId($pin_used_user_id);
        $obj->setPinStatus($pin_status);
        $obj->setPinUsedTime($pin_used_time);
        $obj->onPreUpdate();

        if($obj->getPinRequestFor() == 1){
            $find = $em->getRepository('Entity\Pins')->findBy(array('pin_used_user_id' => $pin_used_user_id,'pin_request_for'=>1)); 
            if($find != null){
              if(count($find) <= 1){
                $obj->setRepurchaseType(1);
              }else{
                foreach ($find as $pin) {
                  array_push($ar, $pin->getPinId());
                }
                $min_val = min($ar);
                if($obj->getPinId() == $min_val){
                  $obj->setRepurchaseType(1);
                }else{$obj->setRepurchaseType(0);}
              }
            }
        }else{
          $obj->setRepurchaseType(0);
        }

        $em->persist($obj);
        try {
          $em->flush();
          return $obj;
        }catch (UniqueConstraintViolationException $e){ return false; }
      }else{
        return false;
      }
    }
    public function add_user_package_purchase($user_obj,$up_sp_up_id,$up_sp_gene_side,$up_pin_obj,$up_act_gene_up_id,$up_act_gene_side,$up_gene_left_pid,$up_gene_right_pid,$up_money_alloted,$up_transaction_obj,$up_pair_up_id,$achieve,$created_at){
      $em = $this->doctrine->em;
      $pack_obj = new Entity\User_package_purchase();

      $pack_obj->setUserId($user_obj);
      $pack_obj->setUserPurchaseSponserUserPurchaseId($up_sp_up_id);
      $pack_obj->setUserPurchaseSponserGeneSide($up_sp_gene_side);
      $pack_obj->setUserPurchasePinId($up_pin_obj);
      $pack_obj->setUserPurchaseActualGeneUserPurchaseId($up_act_gene_up_id);
      $pack_obj->setUserPurchaseActualGeneSide($up_act_gene_side);
      $pack_obj->setUserPurchaseGeneLeftPurchaseId($up_gene_left_pid);
      $pack_obj->setUserPurchaseGeneRightPurchaseId($up_gene_right_pid);
      $pack_obj->setUserPurchaseMoneyAlloted($up_money_alloted);
      $pack_obj->setUserPurchaseTransactionId($up_transaction_obj);
      $pack_obj->setUserPruchasePairUserPurchaseId($up_pair_up_id);
      $pack_obj->setPairAchieve(0);
      $pack_obj->setRepCount(0);
      $pack_obj->setPairAchieveSide(0);
      $pack_obj->setPairAchieveUserPurchaseId(0);
      $pack_obj->setPayoutReleased(0);
      $pack_obj->setPayoutAch(0);

      $pack_obj->onPrePersist();

      $avail = $this->check_node_is_available_pk($up_act_gene_up_id,$up_act_gene_side);
      if($avail == 1){
        $em->persist($pack_obj);
        $em->flush();
       
         $find = $em->getRepository('Entity\User_package_purchase')->find($up_act_gene_up_id); 
        // if($achieve['achive_id'] != null){
        //   $ach_find = $em->getRepository('Entity\User_package_purchase')->find($achieve['achive_id']);
        //   if($ach_find != null){
        //     // $ach_find->setPairAchieveUserPurchaseId($achieve['achive_id']); 
        //     $ach_find->setPairAchieveUserPurchaseId($pack_obj->getUserPurchaseId()); 
        //     $ach_find->setPairAchieve($achieve['achive_value']); 
        //     $ach_find->setPairAchieveSide($up_sp_gene_side); 
        //     $em->persist($ach_find);
        //     $em->flush();
        //   }
        // }
        if($up_act_gene_side == 0){
              $find->setUserPurchaseGeneLeftPurchaseId($pack_obj->getUserPurchaseId());
              $em->persist($find);
              $em->flush();
              return true;
        }else{
          $find->setUserPurchaseGeneRightPurchaseId($pack_obj->getUserPurchaseId());
          $em->persist($find);
          $em->flush();
          return true;
        }
      }else{return -5;}
        
    }
    public function get_products_from_pack_prd_id($id){
      $em = $this->doctrine->em;
      if (!filter_var($id, FILTER_VALIDATE_INT) === false) {
        $pck_ar = array();
        $pck_name = array();
        $pck_shrt_desc = array();
        $pck_desc = array();
        $pck_price = array();
        $pck_url = array();
        $pck_quantity = array();
        $avail_bal = array();
        $pack_price = array();
        $pack_prd_obj = $this->get_multiple_rows_using_array_value('Package_product_items','package_product_id',$id);
        //print_r($pack_prd_obj);exit();
        foreach ($pack_prd_obj as $pck) {

          array_push($pck_name, $pck->getPackageProductItemProductId()->getProductName());
          array_push($pck_shrt_desc, $pck->getPackageProductItemProductId()->getProductShortDesc());
          array_push($pck_desc, $pck->getPackageProductItemProductId()->getProductDesc());
          array_push($pck_price, $pck->getPackageProductItemProductId()->getProductPrice());
          array_push($pck_quantity, $pck->getPackageProductQuantity());

          $attachment_object = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_id' => $pck->getPackageProductItemProductId()->getProductId(),'attachment_referer_type'=>4));
          if($attachment_object != null){
            array_push($pck_url,$attachment_object->getAttachmentImageUrl());
          }else{array_push($pck_url,"");}
        }

        $pin_obj = unserialize($this->session->userdata('pin_obj'));
        $user_id = $pin_obj->getPinRequestedUserId()->getUserId();

        $amount = $this->get_available_balance($user_id);
        $pack_prd_price = $this->get_package_product_price($id);
        array_push($pack_price, $pack_prd_price);
        array_push($avail_bal, $amount);

        array_push($pck_ar, $pck_name);
        array_push($pck_ar, $pck_shrt_desc);
        array_push($pck_ar, $pck_desc);
        array_push($pck_ar, $pck_price);
        array_push($pck_ar, $pck_url);
        array_push($pck_ar, $pck_quantity);
        array_push($pck_ar, $avail_bal);
        array_push($pck_ar, $pack_price);
        return $pck_ar;
      }else{return false;}
    }
    public function get_product_names_from_pack_prd_id($id){
      $em = $this->doctrine->em;
      $pck_name = array();
      $pack_prd_obj = $this->get_multiple_rows_using_array_value('Package_product_items','package_product_id',$id);
      foreach ($pack_prd_obj as $pck) {
        array_push($pck_name, $pck->getPackageProductItemProductId()->getProductName());
      }
      return $pck_name;
    }
    public function get_referer_details($user_id){
      $em = $this->doctrine->em;
      
      $pck_obj = $em->getRepository('Entity\User_package_purchase')->findOneBy(array('user_id' => $user_id));
      if($pck_obj != null){
        $spu_pid = $pck_obj->getUserPurchaseSponserUserPurchaseId();
        $act_pid = $pck_obj->getUserPurchaseActualGeneUserPurchaseId();

        $act_user_put_obj = $this->get_single_row_using_primary_key('User_package_purchase',$act_pid);
        $act_user_obj = $act_user_put_obj->getUserId();

        $position = $pck_obj->getUserPurchaseActualGeneSide();

      $pin_obj = $pck_obj->getUserPurchasePinId();
      $pin_id = $pin_obj->getPinId();
      $sp_user_obj = $pin_obj->getPinRequestedUserId();
        $pin_no = $pin_obj->getPinNo();
        $package_name = $pin_obj->getPinRequestPackageId()->getPackageName();
        $package_product_id = $pin_obj->getPinRequestPackageProductId()->getPackageProductId();
        $product_names = $this->get_product_names_from_pack_prd_id($package_product_id);

      $used_user_id = $pin_obj->getPinUsedUserId();
      $used_user_obj = $this->get_single_row_using_primary_key('User',$used_user_id);

      $account_obj = $this->get_single_row_using_array_value('Account_details','account_refer_id',$used_user_id);

      $obj = new Entity\User_sview();

      //$dt = $account_obj->getBankId()->getBankIfsc();
      // print_r();
      //echo $dt;
      //echo $used_user_obj->getDob();

      $obj->setReferalId($sp_user_obj->getUserName());
      $obj->setSponcerName($sp_user_obj->getFirstName());
      $obj->setPlacementId('PID100');
      $obj->setPlacementName($act_user_obj->getFirstName());
      $obj->setPosition($position);
      $obj->setPinNo($pin_no);
      $obj->setPackageName($package_name);
      $obj->setProducts(implode(',', $product_names));
      $obj->setProductsCount(count($product_names));

      $obj->setUuFirstName($used_user_obj->getFirstName());
      $obj->setDob($used_user_obj->getDob()->format('Y-m-d'));
      $obj->setGender($used_user_obj->getGender());
      $obj->setPhoneNo($used_user_obj->getContactId()->getContactMobilePrimary());
      $obj->setEmail($used_user_obj->getuserEmail());

      $obj->setNomineeName($used_user_obj->getNomineeId()->getNomineeName());
      $obj->setRelationship($used_user_obj->getNomineeId()->getNomineeRelationship());
      $obj->setNDob($used_user_obj->getNomineeId()->getNomineeDob()->format('Y-m-d'));
      $obj->setNGender($used_user_obj->getNomineeId()->getNomineeGender());
      $obj->setNPhoneNo($used_user_obj->getNomineeId()->getNomineePhoneNo());

      $obj->setFullAddress($used_user_obj->getAddressId()->get_Address_full_address());
      $obj->setPincode($used_user_obj->getAddressId()->get_Address_pincode_id()->getPincodeNo());
      $obj->setDistrict($used_user_obj->getAddressId()->get_Address_pincode_id()->getPincodeDistrict());
      $obj->setState($used_user_obj->getAddressId()->get_Address_pincode_id()->getPincodeState());
      $obj->setCountry($used_user_obj->getAddressId()->get_Address_pincode_id()->getPincodeCountryId()->getCountryName());

      $obj->setAccountHolderName($account_obj->getAccountHolderName());
      $obj->setAccountNumber($account_obj->getAccountNo());
      $obj->setAccountType($account_obj->getAccountType());
      $obj->setIfsc($account_obj->getBankId()->getBankIfsc());
      $obj->setBankName($account_obj->getBankId()->getBankName());
      $obj->setBankBranch($account_obj->getBankId()->getBankBranch());

      $obj->setAadharNo("");
      $obj->setPanNo("");
      $obj->setPassNo($account_obj->getAccountNo());

      $obj->setAadharFront("");
      $obj->setAadharBack("");
      $obj->setPanFront("");
      $obj->setPanBack("");
      $obj->setPassFront("");
      $obj->setPassBack("");
      return $obj;
      }else{return false;}
    }
    public function get_user_details($user_id){
      $em = $this->doctrine->em;
      $used_user_obj = $this->get_single_row_using_primary_key('User',$user_id);
      $access_obj = $this->get_single_row_using_array_value('User_access','user_access_user_id',$user_id);

      $obj = new Entity\User_sview();

      $obj->setIsActive($used_user_obj->getIsActive());
      $obj->setUuFirstName($used_user_obj->getFirstName());
      if($used_user_obj->getDob() != null){
        $obj->setDob($used_user_obj->getDob()->format('Y-m-d'));
      }
      $obj->setGender($used_user_obj->getGender());
      $obj->setPhoneNo($used_user_obj->getContactId()->getContactMobilePrimary());
      $obj->setEmail($used_user_obj->getuserEmail());

      $obj->setKycStatus($used_user_obj->getKycStatus());
      $obj->setPanVerified($used_user_obj->getPanVerified());
      $obj->setAadharVerified($used_user_obj->getAadharVerified());
      $obj->setPassVerified($used_user_obj->getPassVerified());

      $obj->setNomineeName($used_user_obj->getNomineeId()->getNomineeName());
      $obj->setRelationship($used_user_obj->getNomineeId()->getNomineeRelationship());
      if($used_user_obj->getNomineeId()->getNomineeDob() != null){
         $obj->setNDob($used_user_obj->getNomineeId()->getNomineeDob()->format('Y-m-d'));
      }
     
      $obj->setNGender($used_user_obj->getNomineeId()->getNomineeGender());
      $obj->setNPhoneNo($used_user_obj->getNomineeId()->getNomineePhoneNo());

      $obj->setFullAddress($used_user_obj->getAddressId()->get_Address_full_address());
      
      $obj->setPassword($used_user_obj->getPasswordId()->getSalt());
      $obj->setPinStorePassword($used_user_obj->getPinStorePassword());

      if($used_user_obj->getAddressId() != null){
        $obj->setAddressId($used_user_obj->getAddressId()->get_Address_id());
        if($used_user_obj->getAddressId()->get_Address_pincode_id() != null){
          $obj->setPincode($used_user_obj->getAddressId()->get_Address_pincode_id()->getPincodeNo());
          $obj->setDistrict($used_user_obj->getAddressId()->get_Address_pincode_id()->getPincodeDistrict());
          $obj->setState($used_user_obj->getAddressId()->get_Address_pincode_id()->getPincodeState());
          if($used_user_obj->getAddressId()->get_Address_pincode_id()->getPincodeCountryId() != null){
            $obj->setCountry($used_user_obj->getAddressId()->get_Address_pincode_id()->getPincodeCountryId()->getCountryName());
            $obj->setCountryId($used_user_obj->getAddressId()->get_Address_pincode_id()->getPincodeCountryId());
          }
        }else{
          $query = $em->createQuery("SELECT u FROM Entity\Pincode as u " )->setMaxResults(1) ;
          $pin_obj = $query->getResult();
          if($pin_obj != null){
            $obj->setPincode($pin_obj[0]->getPincodeNo());
            $obj->setDistrict($pin_obj[0]->getPincodeDistrict());
            $obj->setState($pin_obj[0]->getPincodeState());
          }else{
            $obj->setPincode("");
            $obj->setDistrict("");
            $obj->setState("");
          }

          if($pin_obj[0]->getPincodeCountryId() != null){
            $obj->setCountry($pin_obj[0]->getPincodeCountryId()->getCountryName());
            $obj->setCountryId($pin_obj[0]->getPincodeCountryId());
          }else{
            $con = $em->createQuery("SELECT u FROM Entity\Country as u " )->setMaxResults(1) ;
            $con_obj = $con->getResult();
            if($con_obj != null){
              $obj->setCountry($con_obj[0]->getCountryName());
              $obj->setCountryId($pin_obj[0]);
            }else{
              $obj->setCountry("");
              $obj->setCountryId("");
            }
          }

        }
      }else{
        $obj->setPincode("");
        $obj->setDistrict("");
        $obj->setState("");
        $obj->setCountry("");
        $obj->setAddressId("");
      }
      
      $obj->setAadharNo($used_user_obj->getAadharNo());
      $obj->setPanNo($used_user_obj->getPanNo());

      $obj->setSecondaryPhone($used_user_obj->getContactId()->getContactMobileSecondary());
      
      $obj->setRoleId($used_user_obj->getUserRoleId()->getUserRoleId());

      $obj->setRoleName($used_user_obj->getUserRoleId()->getUserRoleName());
      if($access_obj != null){$ids = $access_obj->getUserAccessAccessId();}else{$ids = "";}
      $obj->setAccessIds($ids);

      $obj->setUserId($used_user_obj->getUserId());
      $obj->setUserName($used_user_obj->getUserName());
      $obj->setLastName($used_user_obj->getLastName());

      
      $obj->setContactId($used_user_obj->getContactId()->getContactId());
      $obj->setPasswordId($used_user_obj->getPasswordId()->getPasswordId());

      return $obj;
    }
    public function get_user_new_details($user_id){
      $em = $this->doctrine->em;
      $user_obj = $this->get_single_row_using_primary_key('User',$user_id);
      $access_obj = $this->get_single_row_using_array_value('User_access','user_access_user_id',$user_id);
      $account_obj= $em->getRepository('Entity\Account_details')->findOneBy(array('account_refer_id' => $user_id,'account_refer_type'=>1));
      $attachment_obj= $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $user_id,'attachment_referer_type'=>7));

      $obj = new Entity\User_new_sview();

      $obj->setUserId($user_obj->getUserId());
      $obj->setUserName($user_obj->getUserName());
      $obj->setFirstName($user_obj->getFirstName());
      $obj->setLastName($user_obj->getLastName());
      $obj->setUserEmail($user_obj->getUserEmail());
      if($user_obj->getDob() != null){
        $obj->setDob($user_obj->getDob()->format('Y-m-d'));
      }else{$obj->setDob('');}
      $obj->setGender($user_obj->getGender());
      $obj->setAadharNo($user_obj->getAadharNo());
      $obj->setPanNo($user_obj->getPanNo());
      $obj->setAadharVerified($user_obj->getAadharVerified());
      $obj->setPanVerified($user_obj->getPanVerified());
      $obj->setPassVerified($user_obj->getPassVerified());
      $obj->setKycStatus($user_obj->getKycStatus());
      $obj->setPinPassword($user_obj->getPinStorePassword());
      if($user_obj->getCreatedAt() != null){
        $obj->setCreatedAt($user_obj->getCreatedAt()->format('Y-m-d'));
      }else{$obj->setCreatedAt('');}
      if($user_obj->getUpdatedAt() != null){
        $obj->setUpdatedAt($user_obj->getUpdatedAt()->format('Y-m-d'));
      }else{$obj->setUpdatedAt('');}
      $obj->setProfilePictureUrl($user_obj->getProfilePictureUrl());

      if($user_obj->getContactId() != null){
        $obj->setPrimaryMobile($user_obj->getContactId()->getContactMobilePrimary());
        $obj->setSecondaryMobile($user_obj->getContactId()->getContactMobileSecondary());
      }

      if($user_obj->getUserRoleId() != null){
        $obj->setUserRoleId($user_obj->getUserRoleId()->getUserRoleId());
        $obj->setUserRoleName($user_obj->getUserRoleId()->getUserRoleName());
      }else{$obj->setUserRoleId(0);$obj->setUserRoleName('');}
      if($access_obj != null){$ids = $access_obj->getUserAccessAccessId();}else{$ids = "";}
      $obj->setUserAccessIds($ids);

      $obj->setUnlockedReason($user_obj->getUnlockedReason());
      $obj->setIsActive($user_obj->getIsActive());

      // Account Start
        if($account_obj != null){
          $obj->setAccountNo($account_obj->getAccountNo());
          $obj->setAccountType($account_obj->getAccountType());
          if($account_obj->getBankId() != null){
            $obj->setBankIfsc($account_obj->getBankId()->getBankIfsc());
            $obj->setBankName($account_obj->getBankId()->getBankName());
            $obj->setBankBranch($account_obj->getBankId()->getBankBranch());
          }else{
            $obj->setBankIfsc(""); $obj->setBankName(""); $obj->setBankBranch("");
          }
        }else{
          $obj->setAccountNo(""); $obj->setAccountType("");
          $obj->setBankIfsc(""); $obj->setBankName(""); $obj->setBankBranch("");
        }
      //Account End

      //Address Start
        if($user_obj->getAddressId() != null){
          $obj->setFullAddress($user_obj->getAddressId()->get_Address_full_address());
          if($user_obj->getAddressId()->get_Address_pincode_id() != null){
            $obj->setPincode($user_obj->getAddressId()->get_Address_pincode_id()->getPincodeNo());
            $obj->setPincodePlace($user_obj->getAddressId()->get_Address_pincode_id()->getPincodePlace());
            $obj->setDistrict($user_obj->getAddressId()->get_Address_pincode_id()->getPincodeDistrict());
            $obj->setState($user_obj->getAddressId()->get_Address_pincode_id()->getPincodeState());
            if($user_obj->getAddressId()->get_Address_pincode_id()->getPincodeCountryId() != null){
              $obj->setCountryId($user_obj->getAddressId()->get_Address_pincode_id()->getPincodeCountryId()->getCountryId());
              $obj->setCountryName($user_obj->getAddressId()->get_Address_pincode_id()->getPincodeCountryId()->getCountryName());

            }else{
              $obj->setCountryId(""); $obj->setCountryName("");
            }
          }else{
            $obj->setPincode(""); $obj->setPincodePlace(""); $obj->setDistrict(""); $obj->setState("");$obj->setCountryId(""); $obj->setCountryName("");
          }
        }else{
          $obj->setFullAddress("");$obj->setPincode(""); $obj->setPincodePlace(""); $obj->setDistrict(""); $obj->setState("");$obj->setCountryId(""); $obj->setCountryName("");
        }
      //Address End

      //Nominee Start
        
        if($user_obj->getNomineeId() != null){
          $obj->setNName($user_obj->getNomineeId()->getNomineeName());
          if($user_obj->getNomineeId()->getNomineeDob() != null){
             $obj->setNDob($user_obj->getNomineeId()->getNomineeDob()->format('Y-m-d'));
          }
          $obj->setNRelation($user_obj->getNomineeId()->getNomineeRelationship());
          $obj->setNMobile($user_obj->getNomineeId()->getNomineePhoneNo());
          $obj->setNGender($user_obj->getNomineeId()->getNomineeGender());
        }else{
          $obj->setNName('');
          $obj->setNDob('');
          $obj->setNRelation('');
          $obj->setNMobile('');
          $obj->setNGender('');
        }
      //Nominee End

      //Passsword Start
        if($user_obj->getPasswordId() != null){
          $obj->setPassword($user_obj->getPasswordId()->getPassword());
        }else{
          $obj->setPassword("");
        }
      //Passsword End

      //Attachment Start
        $find = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_id'=>$user_id,'attachment_referer_type'=>7,'attachment_name'=>'pan_front')); 
        if($find != null){
          $obj->setPanImageUrl($find->getAttachmentImageUrl());
        }else{
          $obj->setPanImageUrl('');
        }
        $pass_find = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_id'=>$user_id,'attachment_referer_type'=>7,'attachment_name'=>'pass_front'));
        if($pass_find != null){
          $obj->setPassImageUrl($pass_find->getAttachmentImageUrl());
        }else{
          $obj->setPassImageUrl('');
        }
        $aadhar_find = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_id'=>$user_id,'attachment_referer_type'=>7,'attachment_name'=>'aadhar_front'));
        if($aadhar_find != null){
          $obj->setAadharImageUrl($aadhar_find->getAttachmentImageUrl());
        }else{
          $obj->setAadharImageUrl('');
        }
      //Attachment End
      return $obj;
    }
    public function get_available_balance($user_id){
      if (!filter_var($user_id, FILTER_VALIDATE_INT) === false) {
        $user_account_obj = $this->get_single_row_using_array_value('User_account','user_id',$user_id);
        return $user_account_obj->getUserAccountRepurchaseAmount();
      }else{ return -1; }
    }
    public function get_package_product_price($pack_prd_id){
      if (!filter_var($pack_prd_id, FILTER_VALIDATE_INT) === false) {
        $pack_prd_obj = $this->get_single_row_using_primary_key('Package_products',$pack_prd_id);
        return $pack_prd_obj->getPackageProductDisplayPrice();
      }else{ return -1; }
    }
    public function purchase($user_id,$amount,$pack_prd_id){
      $em = $this->doctrine->em;
      if (!filter_var($user_id, FILTER_VALIDATE_INT) === false) {
        return true;
        // $user_account_obj = $this->get_single_row_using_array_value('User_account','user_id',$user_id);
        // if($user_account_obj != null){
        //  $exist_amount = $user_account_obj->getUserAccountRepurchaseAmount();
        //  $pack_price = $this->get_package_product_price($pack_prd_id);
        //  $remaining_bal = $exist_amount-$pack_price;
        //  $user_account_obj->setUserAccountRepurchaseAmount($remaining_bal);
        //  $user_account_obj->onPreUpdate();
        //  $em->persist($user_account_obj);
        //  $em->flush();
          
        // }else{return false;}
      }else{return false;}
    }
    public function add_money($rupee){
      $em = $this->doctrine->em;
      if (!filter_var($rupee, FILTER_VALIDATE_INT) === false) {
        $pin_obj = unserialize($this->session->userdata('pin_obj'));
        $user_id = $pin_obj->getPinRequestedUserId()->getUserId();
        $user_account_obj = $this->get_single_row_using_array_value('User_account','user_id',$user_id);
        $exists_amt = $user_account_obj->getUserAccountRepurchaseAmount($rupee);
        $new_amt = $exists_amt+$rupee;
        $user_account_obj->setUserAccountRepurchaseAmount($new_amt);
        $em->persist($user_account_obj);
        $em->flush();
        return true;
      }else{ return false; }
    }
    public function add_user_account($user_obj,$user_account_available_amount,$user_account_re_purchase_amount,$user_account_transfer_per_day_limit_amount,$user_account_transfer_per_day_limit_transfer,$user_account_transfer_per_month_limit_amount,$user_account_transfer_per_month_limit_transfer,$user_account_account_per_day_transfer_enable,$user_account_per_month_transfer_enable,$user_account_hold_amount,$user_account_hold_enabled,$user_account_transfer_enabled,$user_account_is_active,$user_account_locked_reason){
      $em = $this->doctrine->em;
      $account = new Entity\User_account;
      $account->setUserId($user_obj);
      $account->setUserAccountAvailableAmount($user_account_available_amount);
      $account->setUserAccountRepurchaseAmount($user_account_re_purchase_amount);
      $account->setUserAccountTransferPerDayLimitAmount($user_account_transfer_per_day_limit_amount);
      $account->setUserAccountTransferPerDayLimitTransfer($user_account_transfer_per_day_limit_transfer);
      $account->setUserAccountTransferPerMonthLimitAmount($user_account_transfer_per_month_limit_amount);
      $account->setUserAccountTransferPerMonthLimitTransfer($user_account_transfer_per_month_limit_transfer);
      $account->setUserAccountPerDayTransferEnable($user_account_account_per_day_transfer_enable);
      $account->setUserAccountPerMonthTransferEnable($user_account_per_month_transfer_enable);
      $account->setUserAccountHoldAmount($user_account_hold_amount);
      $account->setUserAccountHoldEnabled($user_account_hold_enabled);
      $account->setUserAccountTransferEnable($user_account_transfer_enabled);
      $account->setUserAccountIsActive($user_account_is_active);
      $account->setUserAccountLockedReason($user_account_locked_reason);
      $account->onPrePersist();
      $em->persist($account);
      try {
        $em->flush();
        return true;
      }catch (UniqueConstraintViolationException $e){
        return false;
      }
    }
  //svk metro End

  //has accsss funcions Start
    public function get_access_id_from_module_name($module_id,$access_name){
      $em = $this->doctrine->em;
      $find = $em->getRepository('Entity\Access')->findOneBy(array('module_id' => $module_id,'access_name'=>$access_name));
      if($find != null){
        return $find;
      }else{ return false; }
    }
    public function has_access($module_name,$access_name){

      $module_obj = $this->get_single_row_using_array_value('Module','module_name',$module_name);
      if($module_obj != null){

        $module_id = $module_obj->getModuleId();
        $access_obj = $this->get_access_id_from_module_name($module_id,$access_name);
        if($access_obj != null){
          $access_id = $access_obj->getAccessId();
          if(count(explode(',',$_SESSION['access_ids'])) > 0){
            if(in_array($access_id,explode(',',$_SESSION['access_ids'])) == 1){
              return true;
            }else{ return false; }
          }else{ return false; }
        }else{ return false; }
      }
    }
    public function create_payout_record($user_purchase_id,$package_id,$total_left,$total_right,$personal_left,$personal_right,$current_left,$current_right,$carry_forward,$carry_forward_side){
      $em = $this->doctrine->em;
      $obj = new Entity\Payout;
      $obj->setUserPurchaseId($user_purchase_id);
      $obj->setPackageId($package_id);
      $obj->setTotalLeft($total_left);
      $obj->setTotalRight($total_right);
      $obj->setPersonalLeft($personal_left);
      $obj->setPersonalRight($personal_right);
      $obj->setCurrentLeft($current_left);
      $obj->setCurrentRight($current_right);
      $obj->setCarryForward($carry_forward);
      $obj->setCarryForwardSide($carry_forward_side);
      $obj->onPrePersist();
      $obj->onPreUpdate();
      $em->persist($obj);
      try {
        $em->flush();
        return true;
      }catch (UniqueConstraintViolationException $e){
        return false;
      }
    }
  //has accsss funcions End
    public function test(){
      $utill_obj = new Common_utill();
      $dql = 'select p.product_id from Entity\Product as p';
      $page_no=1;$max_item=1;$no_of_links=5;$list_class='list_class';$active_class='active';$item_count=5;
        // $query = $em->createQuery($dql);
        // $paginator=$this->get_page_Data($query,1,1); 
      $max = 1;$page_no=1;
        $em=$this->doctrine->em;
        $query = $em->createQuery($dql)
                ->setFirstResult($max*$page_no-$max)
                ->setMaxResults($max);
        $page_data = new Paginator($query, $fetchJoinCollection = true);
        

         $last       = ceil($item_count/$max_item );
        //-----------------------
        if($page_no<=0 || $page_no>$last){
         $page_no=1;
        }
        if ($max_item== 'all' ){
         return '';
        }
        //-----------------------

        $start      = ( ( $page_no - $no_of_links ) > 0 ) ? $page_no - $no_of_links : 1;
        $end        = ( ( $page_no + $no_of_links ) < $last ) ? $page_no + $no_of_links : $last;
        $html       = '<ul class="' . $list_class . '">';
        $class      = ( $page_no == 1 ) ? "disabled" : "";
        $prev     =  $page_no-1;    
        $next     = $page_no+1;     
        $html       .= '<a onclick="myfuction('.$prev.')"><li class="' . $class . '">&laquo;</li></a>';
        if ( $start > 1 ) {
         $html   .= '<a href="?max=' . $max_item . '&page=1"><li>1</li></a>';
         $html   .= '<li class="disabled"><span>...</span></li>';
        }
        for ( $i = $start ; $i <= $end; $i++ ) {
         $class  = ( $page_no == $i ) ? $active_class : "";
         $html   .= '<a ><li class="' . $class . '"  onclick="myfuction('.$i.')">' . $i . '</li></a>';
        }
        if ( $end < $last ) {
         $html   .= '<li class="disabled"><span>...</span></li>';
         $html   .= '<a onclick="myfuction('.$i.')"><li>' . $last . '</li></a>';
        }
        $class      = ( $page_no == $last ) ? "disabled" : "";
        $html       .= '<a  onclick="myfuction('.$next.')"><li class="' . $class . '">&raquo;</li></a>';
        $html       .= '</ul>';
        echo '';
        print_r($html);
          // return $page_data;
            
    }


    public function get_pagination_data($query,$args=array('page_no'=>1,'max_item'=>1,'no_of_links'=>5,'list_class'=>'  list_class','active_class'=>'active')){
        $page_no=$args['page_no'];
        if($args['page_no']<=0){
         $page_no=1;
        }
        $max_item=$args['max_item'];
        $no_of_links=$args['no_of_links'];
        $list_class=$args['list_class'];
        $active_class=$args['active_class'];
        $paginator=$this->get_page_Data($query,$page_no,$max_item);
        $item_count=$paginator->count();
        $last       = ceil($item_count/$max_item );
        $p_links=$this->create_pagination_links($page_no,$max_item,$no_of_links,$list_class,$active_class,$item_count);
        if($page_no>$last){
         $page_no=1;
         $paginator=$this->get_page_Data($query,$page_no,$max_item);
         $item_count=$paginator->count();
         $p_links=$this->create_pagination_links($page_no,$max_item,$no_of_links,$list_class,$active_class,$item_count);
        }
        $p_data['p_records']=$paginator;
        $p_data['p_links']=$p_links;
        return $p_data;
    }
    public function get_page_Data($dql='',$page_no,$max){
      $em=$this->doctrine->em;
      $query = $em->createQuery($dql)
              ->setFirstResult($max*$page_no-$max)
              ->setMaxResults($max);
      $page_data = new Paginator($query, $fetchJoinCollection = true);
      return $page_data;
    }
    public function create_pagination_links($page_no=1,$max_item=1,$no_of_links=5,$list_class='list_class',$active_class='active',$item_count=5){
        //echo $no_of_links.'hahahaa';
        $last       = ceil($item_count/$max_item );
        //-----------------------
        if($page_no<=0 || $page_no>$last){
         $page_no=1;
        }
        if ($max_item== 'all' ){
         return '';
        }
        //-----------------------

        $start      = ( ( $page_no - $no_of_links ) > 0 ) ? $page_no - $no_of_links : 1;
        $end        = ( ( $page_no + $no_of_links ) < $last ) ? $page_no + $no_of_links : $last;
        $html       = '<ul class="' . $list_class . '">';
        $class      = ( $page_no == 1 ) ? "disabled" : "";
        $prev     =  $page_no-1;    
        $next     = $page_no+1;     
        $html       .= '<a onclick="myfuction('.$prev.')"><li class="' . $class . '">&laquo;</li></a>';
        if ( $start > 1 ) {
         $html   .= '<a href="?max=' . $max_item . '&page=1"><li>1</li></a>';
         $html   .= '<li class="disabled"><span>...</span></li>';
        }
        for ( $i = $start ; $i <= $end; $i++ ) {
         $class  = ( $page_no == $i ) ? $active_class : "";
         $html   .= '<a ><li class="' . $class . '"  onclick="myfuction('.$i.')">' . $i . '</li></a>';
        }
        if ( $end < $last ) {
         $html   .= '<li class="disabled"><span>...</span></li>';
         $html   .= '<a onclick="myfuction('.$i.')"><li>' . $last . '</li></a>';
        }
        $class      = ( $page_no == $last ) ? "disabled" : "";
        $html       .= '<a  onclick="myfuction('.$next.')"><li class="' . $class . '">&raquo;</li></a>';
        $html       .= '</ul>';
        echo '';

        return $html;
    }
    public function get_inbox_messages($user_id){
      if( ! filter_var($user_id, FILTER_VALIDATE_INT) === false ){
        $exist = $this->get_single_row_using_primary_key('User',$user_id);
        if($exist != null){
          $em = $this->doctrine->em;
          $query = $em->createQuery("SELECT E.user_email,V.user_messages_subject,V.user_messages_id,V.user_messages_received_date FROM Entity\User_messages as V INNER JOIN Entity\User as E WITH V.user_messages_from_id = E.user_id WHERE V.user_messages_to_id=$user_id AND V.user_messages_is_delete = 0 ORDER BY V.user_messages_received_date DESC" );
          $course = $query->getResult();
          
          return $course;
        }else{ return false; }
      }else{ return false; }
    }
    public function get_single_row_using_array_value_two($table_name,$key_name,$key_value){
      $em = $this->doctrine->em;
      $find = $em->getRepository('Entity\\'.$table_name)->findOneBy(array($key_name => $key_value));
      if($find != null){ return $find; }else{ return false; }
    }
    public function set_message_has_read_one($message_id){
      if( ! filter_var($message_id, FILTER_VALIDATE_INT) === false ){
        $em = $this->doctrine->em;
        $find = $em->getRepository('Entity\User_messages')->find($message_id);
        if($find != null){
          $find->setUserMessagesHasRead(1);
          $em->persist($find);
          $em->flush();
          return true;
        }else{ return false; }
      }else{ return false; }
    }
    public function get_all_active_users_mail($user_id){
      $em = $this->doctrine->em;
      $query = $em->createQuery("SELECT E.user_email,E.user_name FROM  Entity\User as E WHERE E.user_id != $user_id" );
      $course = $query->getResult();
      return $course;
    }
    public function add_message($from,$to,$subject,$full_message,$now,$images){
      if( $from != null && $to != null ){
        $em = $this->doctrine->em;
        
          $obj = new Entity\User_messages();
          $obj->setUserMessagesFromId($from);
          $obj->setUserMessagesToId($to);
          $obj->setUserMessagesSubject($subject);
          $obj->setUserMessagesDetail(base64_encode($full_message));
          $obj->setUserMessagesReceivedDate($now);
          $obj->setUserMessagesHasRead(0);
          $obj->setUserMessagesAttachmentIds(0);
          $obj->setUserMessagesIsDelete(0);
          $obj->setUserMessagesIsImportant(0);
          $em->persist($obj);
          try {
            $em->flush();
            if($images['error'] != 4){ 
              for($i=0;$i<count($images['name']);$i++){
                $url = $this->svk_add_single_image($images,$i);
                $product_short_desc='User_messages';
                $product_name='User_messages';
                $attach = $this->add_attachment($product_name,$url,$obj->getUserMessagesId(),RT::$REF['messages'],$product_short_desc);
                if($attach == 1){ 
                  if($i == count($images['name'])-1){
                    return true;
                  }
                }else{ return -4; }
              }
            }else{
              return true;
            }
          }catch (UniqueConstraintViolationException $e){
            return -3;
          }

      }else{return -1;}
    }

    public function get_sent_messages($user_id){
      if( ! filter_var($user_id, FILTER_VALIDATE_INT) === false ){
        $exist = $this->get_single_row_using_primary_key('User',$user_id);
        if($exist != null){
          $em = $this->doctrine->em;
          $query = $em->createQuery("SELECT E.user_email,V.user_messages_subject,V.user_messages_id,V.user_messages_received_date FROM Entity\User_messages as V INNER JOIN Entity\User as E WITH V.user_messages_to_id = E.user_id WHERE V.user_messages_from_id=$user_id AND V.user_messages_is_delete = 0 ORDER BY V.user_messages_received_date DESC" );
          $course = $query->getResult();
          
          return $course;
        }else{ return false; }
      }else{ return false; }
    }
    public function delete_message($id){
      if( ! filter_var($id, FILTER_VALIDATE_INT) === false ){
        $em = $this->doctrine->em;
        $find = $em->getRepository('Entity\User_messages')->find($id);
        if($find != null){
          $em->remove($find);
          try {
            $em->flush();
            return true;
          }catch (ForeignKeyConstraintViolationException $e){ 
            return false;
          }
        }else{return false;}
      }else{return false;}

    }

    public function get_available_product(){
      $em = $this->doctrine->em;
      $find = $em->getRepository('Entity\Product')->findAll();
      $prd_array['id'] = array();
      $prd_array['name'] = array();
      $prd_array['price'] = array();
      $prd_array['attachment'] = array();
      if($find != null){
        foreach ($find as $prd) {
          
          array_push($prd_array['id'], $prd->getProductId());
          array_push($prd_array['name'], $prd->getProductName());
          array_push($prd_array['price'], $prd->getProductPrice());
          $att = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_id' => $prd->getProductId(), 'attachment_referer_type' => RT::$REF['product']));
          if($att != null){
            array_push($prd_array['attachment'], $att[0]->getAttachmentImageUrl());
          }else{ array_push($prd_array['attachment'], "" ); }
          
        }
        return $prd_array;
      }else{ return false; }
    }
    public function get_available_product_spc_ven($id){
      $em = $this->doctrine->em;
      $find = $em->getRepository('Entity\Product')->findBy(array('product_vendor_id_id' => $id));
      $prd_array['id'] = array();
      $prd_array['name'] = array();
      $prd_array['price'] = array();
      $prd_array['attachment'] = array();
      if($find != null){
        foreach ($find as $prd) {
          
          array_push($prd_array['id'], $prd->getProductId());
          array_push($prd_array['name'], $prd->getProductName());
          array_push($prd_array['price'], $prd->getProductPrice());
          $att = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_id' => $prd->getProductId(), 'attachment_referer_type' => RT::$REF['product']));
          if($att != null){
            array_push($prd_array['attachment'], $att[0]->getAttachmentImageUrl());
          }else{ array_push($prd_array['attachment'], "" ); }
          
        }
        return $prd_array;
      }else{ return false; }
    }
    public function com_query(){
      $em = $this->doctrine->em;
      $query = $em->createQuery("SELECT E FROM Entity\User_messages as V INNER JOIN Entity\User as E WITH V.user_messages_to_id = E.user_id WHERE V.user_messages_from_id=$user_id AND V.user_messages_is_delete = 0 ORDER BY V.user_messages_received_date DESC" );
      $query = $em->createQuery("SELECT E.user_email,V.user_messages_subject,V.user_messages_id FROM Entity\User_messages as V INNER JOIN Entity\User as E WITH V.user_messages_to_id = E.user_id WHERE V.user_messages_from_id=$user_id AND V.user_messages_is_delete = 0 ORDER BY V.user_messages_received_date DESC" );
      $course = $query->getResult();
    }
    public function compress_image($source_url, $destination_url, $quality) { 
      $info = getimagesize($source_url);

      if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url); 
      elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url); 
      elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

      imagejpeg($image, $destination_url, $quality); 
      return $image; 
     } 

   public function upload_single_compressed_image($file,$upload_dir,$file_name){
      if ($file["error"] > 0) {
       $upload_status['error']=$file["error"];
      }
      else if (($file["type"] == "image/jpeg") || ($file["type"] == "image/jpg")) 
      { 
      
        $quality = 50;

        if($file['size']>3000000){
          $quality = 10;
        }else if($file['size']>2000000){
          $quality = 15;
        }else if($file['size']>1000000){
          $quality = 25;
        }else if($file['size']>1000000){
          $quality = 25;
        }else{
          $quality = 90;
        }

       $url = $upload_dir.'/'.$file_name.'.jpg'; 
       $filename =$this->compress_image($file["tmp_name"], $url,$quality);
   return $filename;
        header("Content-Type: application/octet-stream");
      header("Content-Transfer-Encoding: binary");
      header("Content-Length: " . strlen($buffer));
      header("Content-Disposition: attachment; filename=$url");

       $upload_status['location'] =$url;
      }
      else 
      { 
       $error = "Uploaded image should be jpg."; 
       $upload_status['error']=$error;
      } 
     
   }
  public function check_node_is_available($top_id,$pos){
    $top_obj = $this->get_single_row_using_array_value('User_package_purchase','user_id',$top_id);
    if($top_obj != null){
      if($this->input->post('pos') == 0){
        if($top_obj->getUserPurchaseGeneLeftPurchaseId() == 0){
          return 1;
        }else{return 0;}
      }else{
        if($top_obj->getUserPurchaseGeneRightPurchaseId() == 0){
          return 1;
        }else{return 0;}
      }
    }else{return -1;}
  }
  public function check_node_is_available_pk($top_id,$pos){
    $top_obj = $this->get_single_row_using_primary_key('User_package_purchase',$top_id);
    if($top_obj != null){
      if($pos == 0){
        if($top_obj->getUserPurchaseGeneLeftPurchaseId() == 0){
          return 1;
        }else{return 0;}
      }else{
        if($top_obj->getUserPurchaseGeneRightPurchaseId() == 0){
          return 1;
        }else{
          return 0;
        }
      }
    }else{
      return -1;
    }
  }
  public function get_category_sub_category(){
    $em = $this->doctrine->em;
    $pack_ar = array();
    $find_pack = $em->getRepository('Entity\Category')->findAll();
    if($find_pack != null){
      foreach ($find_pack as $pack) {
        array_push($pack_ar,array('cat_id'=>$pack->getCategoryId(),'cat_name'=>$pack->getCategoryName()));
      }
      return $pack_ar;
    }else{return -5;}
  }
  public function get_products_by_cat_id($id,$package_id){
    $em=$this->doctrine->em;
    $prd_ar = array();

    if($id == -1){
      $query = 'select prd.product_id,prd.product_name,prd.product_price,prd.point_value,prd.product_short_desc from Entity\Packages as p INNER JOIN Entity\Package_products as pp WITH p.package_id = pp.package_id INNER JOIN Entity\Package_product_items as ppt WITH ppt.package_product_id = pp.package_product_id INNER JOIN Entity\Product as prd WITH prd.product_id = ppt.package_product_item_id WHERE prd.product_is_visible = 1 AND pp.package_id ='.$package_id;
    }else{
      $query = 'select prd.product_id,prd.product_name,prd.product_price,prd.point_value,prd.product_short_desc from Entity\Packages as p INNER JOIN Entity\Package_products as pp WITH p.package_id = pp.package_id INNER JOIN Entity\Package_product_items as ppt WITH ppt.package_product_id = pp.package_product_id INNER JOIN Entity\Product as prd WITH prd.product_id = ppt.package_product_item_id WHERE prd.product_is_visible = 1 AND pp.package_id ='.$package_id.' AND prd.category_id = '.$id;
    }
    $qry = $em->createQuery($query);
    $pack = $qry->getResult();
    if(count($pack) > 0){
      return $pack;
    }else{return 0;}
  }
  public function get_products_by_cat_id_user_choice($id){
    $em=$this->doctrine->em;
    if($id == -1){
      $query = 'SELECT prd.product_id,prd.product_name,prd.product_price,prd.point_value,prd.product_short_desc from  Entity\Product as prd  WHERE prd.product_is_deleted = 2 ';
    }else{
      $query = 'SELECT prd.product_id,prd.product_name,prd.product_price,prd.point_value,prd.product_short_desc from  Entity\Product as prd  WHERE prd.product_is_deleted = 2 AND prd.category_id = '.$id;
    }
    $qry = $em->createQuery($query);
    $pack = $qry->getResult();
    if(count($pack) > 0){
      return $pack;
    }else{return 0;}
  }
  public function get_tree_obj($id){
    $utill_obj = new Common_utill();
    $em = $this->doctrine->em;
    $res = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$id);

    $uid = $res->getUserId()->getUserId();
    $un = $res->getUserId()->getUserName();
    $obj = new Entity\Sample_payout();

    if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
      $obj->setLeftObj($this->get_lf($res->getUserPurchaseGeneLeftPurchaseId()));
    }else{$obj->setLeftObj(0);}
    if($res->getUserPurchaseGeneRightPurchaseId() != 0){
      $obj->setRightObj($this->get_rf($res->getUserPurchaseGeneRightPurchaseId()));
    }else{$obj->setRightObj(0);}

    if($obj->getLeftObj() != 0 && $obj->getRightObj() != 0){
      $ar1 = array_merge($obj->getLeftObj()['name'],$obj->getRightObj()['name']);
      $ar2 = array_merge($obj->getLeftObj()['id'],$obj->getRightObj()['id']);
      return self::$toto;
      return array('id' => $ar2, 'name' => $ar1);
    }elseif($obj->getLeftObj() != 0 && $obj->getRightObj() == 0){
      return self::$toto;
      return array('id' => $obj->getLeftObj()['id'], 'name' => $obj->getLeftObj()['name']);
    }else{
      return self::$toto;
      return array('id' => $obj->getRightObj()['id'], 'name' => $obj->getRightObj()['name']);
    }
  }
  public function get_lf($id){
    $em = $this->doctrine->em;
    $res = $this->get_single_row_using_primary_key('User_package_purchase',$id);
    
    $package_idx = 0;

    $uid = $res->getUserId()->getUserId();
    $un = $res->getUserId()->getUserName();

    array_push(self::$tree_left_var, $uid);
    array_push(self::$tree_left_var_name, $un);
    array_push(self::$toto, array('id'=> $uid,'name' => $un));
    if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
      $this->get_lf($res->getUserPurchaseGeneLeftPurchaseId());
    }
    if($res->getUserPurchaseGeneRightPurchaseId() != 0){
     $this->get_lf($res->getUserPurchaseGeneRightPurchaseId());
    }
    return array('id'=>self::$tree_left_var,'name'=>self::$tree_left_var_name);
  }
  public function get_rf($id){
    $em = $this->doctrine->em;
    // $obj = 0;
    $res = $this->get_single_row_using_primary_key('User_package_purchase',$id);
   
    $uid = $res->getUserId()->getUserId();
    $un = $res->getUserId()->getUserName();

    array_push(self::$tree_right_var, $uid);
    array_push(self::$tree_right_var_name, $un);
    array_push(self::$toto, array('id'=> $uid,'name' => $un));
    if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
      $this->get_rf($res->getUserPurchaseGeneLeftPurchaseId());
    }
    if($res->getUserPurchaseGeneRightPurchaseId() != 0){
      $this->get_rf($res->getUserPurchaseGeneRightPurchaseId());
    }
    return array('id'=>self::$tree_right_var,'name'=>self::$tree_right_var_name);
  }
  public function payout_achieve(){
    $em = $this->doctrine->em;
    $query = $em->createQuery("SELECT up.user_purchase_id FROM Entity\User_package_purchase as up WHERE up.pair_achieve_user_purchase_id > 0 AND up.pair_achieve = 0");
    $res = $query->getResult();
    if($res != null){
      foreach ($res as $rr) {
        $up_obj = $this->get_single_row_using_primary_key('User_package_purchase',$rr['user_purchase_id']);
        if($up_obj != null){
          $up_obj->setPairAchieve(1);
          $em->persist($up_obj);
          $em->flush();
        }
        return 1;
      }
    }else{return 1;}
  }
  public function payout_left_right_manage(){
    $em = $this->doctrine->em;
    $query = $em->createQuery("SELECT up.user_purchase_id FROM Entity\User_package_purchase as up WHERE up.user_purchase_gene_left_purchase_id > 0 AND up.user_purchase_gene_right_purchase_id > 0 AND up.pair_achieve = 0");
    $res = $query->getResult();
    if($res != null){
      foreach ($res as $rr) {
        $ar = array();
        $up_obj = $this->get_single_row_using_primary_key('User_package_purchase',$rr['user_purchase_id']);
        if($up_obj != null){
          $left_obj_id = $up_obj->getUserPurchaseGeneLeftPurchaseId();
          $left_chk_obj = $this->get_single_row_using_primary_key('User_package_purchase',$left_obj_id);
          // array_push($ar, $left_obj_id);

          $right_obj_id = $up_obj->getUserPurchaseGeneRightPurchaseId();
          $right_chk_obj = $this->get_single_row_using_primary_key('User_package_purchase',$right_obj_id);
          // array_push($ar, $right_obj_id);

          if($left_chk_obj->getUserPurchaseGeneLeftPurchaseId()>0 || $left_chk_obj->getUserPurchaseGeneRightPurchaseId()>0 || $right_chk_obj->getUserPurchaseGeneLeftPurchaseId()>0 || $right_chk_obj->getUserPurchaseGeneRightPurchaseId() > 0){
            array_push($ar, $left_chk_obj->getUserPurchaseGeneLeftPurchaseId());
            array_push($ar, $left_chk_obj->getUserPurchaseGeneRightPurchaseId());
            array_push($ar, $right_chk_obj->getUserPurchaseGeneLeftPurchaseId());
            array_push($ar, $right_chk_obj->getUserPurchaseGeneRightPurchaseId());

            $key = array_search(max($ar), $ar);
            if($key == 0 ||$key == 1){
              $position = 0;
            }else{
              $position = 1;
            }


            $up_obj->setPairAchieveUserPurchaseId(max($ar));
            $up_obj->setPairAchieve(1);
            $up_obj->setPairAchieveSide($position);
            $em->persist($up_obj);
            $em->flush();
            return 1;
          }
        }

      }
      return 1;
    }else{return 1;}
  }
  public function get_all_users_mobile($id=null){
    $em = $this->doctrine->em;
    if($id == null){
      $con = '';
    }else{
      $con = ' AND u.user_id ='.$id;
    }
    $query = $em->createQuery("SELECT c.contact_mobile_primary FROM Entity\Contact as c INNER JOIN Entity\User as u WITH (u.contact_id = c.contact_id) WHERE u.user_id != 2 $con");
    $all_mobile = $query->getResult();
    if(count($all_mobile) > 0){
      $result = array(); 
      foreach ($all_mobile as $key => $value) { 
        $result[$key] = $value['contact_mobile_primary']; 
      } 
      return $result;
    }else{return 0;}
  }
  public function add_level_income($user_id,$amount,$achieve_user_id){
    $em = $this->doctrine->em;
    $obj = new Entity\Level_income();
    $obj->setUserId($user_id);
    $obj->setAmount($amount);
    $obj->setAchieveUserId($achieve_user_id);
    $obj->onPrePersist();
    $em->persist($obj);
    $em->flush();
    return true;
  }
  public function rank($rs){
    if($rs >= 25000 && $rs<50000){
      return "EXECUTIVE DISTRIBUTOR";
    }elseif($rs >= 50000 && $rs<100000){
      return "PREMIUM EXECUTIVE DISTRIBUTOR";
    }elseif($rs >= 100000 && $rs<200000){
      return "BRONZE DISTRIBUTOR";
    }elseif($rs >= 200000 && $rs<400000){
      return "SILVER DISTRIBUTOR";
    }elseif($rs >= 500000 && $rs<1000000){
      return "PEARL DISTRIBUTOR";
    }elseif($rs >= 1000000 && $rs<2500000){
      return "SAPHIRE DISTRIBUTOR";
    }elseif($rs >= 2500000 && $rs<5000000){
      return "NATIONAL DISTRIBUTOR";
    }elseif($rs >= 5000000 && $rs<10000000){
      return "GLOBAL DISTRIBUTOR";
    }elseif($rs >= 10000000 && $rs<50000000){
      return "DIAMOND DISTRIBUTOR";
    }elseif($rs >= 50000000 && $rs<100000000){
      return "DOUBLE DIAMOND DISTRIBUTOR";
    }elseif($rs >= 100000000 && $rs<2500000000){
      return "TRIPLE DIAMOND DISTRIBUTOR";
    }elseif($rs >= 250000000 && $rs<5000000000){
      return "CROWN DIAMOND DISTRIBUTOR";
    }else{
      return "DISTRIBUTOR";
    }
  }

  public function get_sum_level_income($user_id){
    $em = $this->doctrine->em;
    if($user_id != null){
      $query = $em->createQuery("SELECT SUM(l.amount) FROM Entity\Level_income as l WHERE l.user_id =".$user_id);
      $res = $query->getResult();
      return $res[0][1];
    }else{return 0;}
  }
  public function get_pin_detail_status_repurchase($pin_no){
    $em = $this->doctrine->em;
    $ar = array();
    $pin_obj = $this->get_single_row_using_array_value('Pins','pin_no',$pin_no);
    if(count($pin_obj)>0){
      if($pin_obj->getPinStatus() == 0){
        $user_id = $pin_obj->getPinUsedUserId();
        $find = $em->getRepository('Entity\Pins')->findBy(array('pin_used_user_id' => $user_id,'pin_request_for'=>1)); 
        if($find != null){
          if(count($find) <= 1){
            return "First Repurchase";
          }else{
            foreach ($find as $pin) {
              array_push($ar, $pin->getPinId());
            }
            $min_val = min($ar);
            if($pin_obj->getPinId() == $min_val){
              return "First Repurchase";
            }else{return "Repurchase";}
          }
        }
      }else{return "Repurchase";}
    }
  }
  public function get_pin_detail_status_repurchase_int($pin_no){
    $em = $this->doctrine->em;
    $ar = array();
    $pin_obj = $this->get_single_row_using_array_value('Pins','pin_no',$pin_no);
    if(count($pin_obj)>0){
      if($pin_obj->getPinStatus() == 0){
        $user_id = $pin_obj->getPinUsedUserId();
        $find = $em->getRepository('Entity\Pins')->findBy(array('pin_used_user_id' => $user_id,'pin_request_for'=>1)); 
        if($find != null){
          if(count($find) <= 1){
            return 1;
          }else{
            foreach ($find as $pin) {
              array_push($ar, $pin->getPinId());
            }
            $min_val = min($ar);
            if($pin_obj->getPinId() == $min_val){
              return 1;
            }else{return 2;}
          }
        }
      }else{return 2;}
    }
  }
  
}
?>  