<?php

namespace DoctrineProxies\__CG__\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Nominee extends \Entity\Nominee implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', 'nominee_id', 'nominee_name', 'nominee_dob', 'nominee_relationship', 'nominee_phone_no', 'nominee_gender', 'created_at', 'updated_at'];
        }

        return ['__isInitialized__', 'nominee_id', 'nominee_name', 'nominee_dob', 'nominee_relationship', 'nominee_phone_no', 'nominee_gender', 'created_at', 'updated_at'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Nominee $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getNomineeId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNomineeId', []);

        return parent::getNomineeId();
    }

    /**
     * {@inheritDoc}
     */
    public function setNomineeId($nominee_id)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNomineeId', [$nominee_id]);

        return parent::setNomineeId($nominee_id);
    }

    /**
     * {@inheritDoc}
     */
    public function getNomineeName()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNomineeName', []);

        return parent::getNomineeName();
    }

    /**
     * {@inheritDoc}
     */
    public function setNomineeName($nominee_name)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNomineeName', [$nominee_name]);

        return parent::setNomineeName($nominee_name);
    }

    /**
     * {@inheritDoc}
     */
    public function getNomineeDob()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNomineeDob', []);

        return parent::getNomineeDob();
    }

    /**
     * {@inheritDoc}
     */
    public function setNomineeDob($nominee_dob)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNomineeDob', [$nominee_dob]);

        return parent::setNomineeDob($nominee_dob);
    }

    /**
     * {@inheritDoc}
     */
    public function getNomineeRelationship()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNomineeRelationship', []);

        return parent::getNomineeRelationship();
    }

    /**
     * {@inheritDoc}
     */
    public function setNomineeRelationship($nominee_relationship)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNomineeRelationship', [$nominee_relationship]);

        return parent::setNomineeRelationship($nominee_relationship);
    }

    /**
     * {@inheritDoc}
     */
    public function getNomineePhoneNo()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNomineePhoneNo', []);

        return parent::getNomineePhoneNo();
    }

    /**
     * {@inheritDoc}
     */
    public function setNomineePhoneNo($nominee_phone_no)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNomineePhoneNo', [$nominee_phone_no]);

        return parent::setNomineePhoneNo($nominee_phone_no);
    }

    /**
     * {@inheritDoc}
     */
    public function getNomineeGender()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNomineeGender', []);

        return parent::getNomineeGender();
    }

    /**
     * {@inheritDoc}
     */
    public function setNomineeGender($nominee_gender)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNomineeGender', [$nominee_gender]);

        return parent::setNomineeGender($nominee_gender);
    }

    /**
     * {@inheritDoc}
     */
    public function onPrePersist()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'onPrePersist', []);

        return parent::onPrePersist();
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreatedAt', []);

        return parent::getCreatedAt();
    }

    /**
     * {@inheritDoc}
     */
    public function onPreUpdate()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'onPreUpdate', []);

        return parent::onPreUpdate();
    }

}
