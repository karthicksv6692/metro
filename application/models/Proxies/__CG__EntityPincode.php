<?php

namespace DoctrineProxies\__CG__\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Pincode extends \Entity\Pincode implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', 'pincode_id', 'pincode_no', 'pincode_place', 'pincode_district', 'pincode_state', 'pincode_country_id'];
        }

        return ['__isInitialized__', 'pincode_id', 'pincode_no', 'pincode_place', 'pincode_district', 'pincode_state', 'pincode_country_id'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Pincode $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getPincodeId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPincodeId', []);

        return parent::getPincodeId();
    }

    /**
     * {@inheritDoc}
     */
    public function setPincodeNo($pincode_no)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPincodeNo', [$pincode_no]);

        return parent::setPincodeNo($pincode_no);
    }

    /**
     * {@inheritDoc}
     */
    public function getPincodeNo()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPincodeNo', []);

        return parent::getPincodeNo();
    }

    /**
     * {@inheritDoc}
     */
    public function setPincodePlace($pincode_place)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPincodePlace', [$pincode_place]);

        return parent::setPincodePlace($pincode_place);
    }

    /**
     * {@inheritDoc}
     */
    public function getPincodePlace()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPincodePlace', []);

        return parent::getPincodePlace();
    }

    /**
     * {@inheritDoc}
     */
    public function setPincodeDistrict($pincode_district)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPincodeDistrict', [$pincode_district]);

        return parent::setPincodeDistrict($pincode_district);
    }

    /**
     * {@inheritDoc}
     */
    public function getPincodeDistrict()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPincodeDistrict', []);

        return parent::getPincodeDistrict();
    }

    /**
     * {@inheritDoc}
     */
    public function setPincodeState($pincode_state)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPincodeState', [$pincode_state]);

        return parent::setPincodeState($pincode_state);
    }

    /**
     * {@inheritDoc}
     */
    public function getPincodeState()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPincodeState', []);

        return parent::getPincodeState();
    }

    /**
     * {@inheritDoc}
     */
    public function setPincodeCountryId($pincode_country_id)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPincodeCountryId', [$pincode_country_id]);

        return parent::setPincodeCountryId($pincode_country_id);
    }

    /**
     * {@inheritDoc}
     */
    public function getPincodeCountryId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPincodeCountryId', []);

        return parent::getPincodeCountryId();
    }

}
