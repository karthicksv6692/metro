<?php

namespace DoctrineProxies\__CG__\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Payout_result extends \Entity\Payout_result implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', 'payout_result_id', 'payout_result_user_id', 'payout_result_left', 'payout_result_right', 'payout_result_type'];
        }

        return ['__isInitialized__', 'payout_result_id', 'payout_result_user_id', 'payout_result_left', 'payout_result_right', 'payout_result_type'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Payout_result $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getPayoutResultId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPayoutResultId', []);

        return parent::getPayoutResultId();
    }

    /**
     * {@inheritDoc}
     */
    public function setPayoutResultId($payout_result_id)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPayoutResultId', [$payout_result_id]);

        return parent::setPayoutResultId($payout_result_id);
    }

    /**
     * {@inheritDoc}
     */
    public function getPayoutResultUserId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPayoutResultUserId', []);

        return parent::getPayoutResultUserId();
    }

    /**
     * {@inheritDoc}
     */
    public function setPayoutResultUserId($payout_result_user_id)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPayoutResultUserId', [$payout_result_user_id]);

        return parent::setPayoutResultUserId($payout_result_user_id);
    }

    /**
     * {@inheritDoc}
     */
    public function getPayoutResultLeft()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPayoutResultLeft', []);

        return parent::getPayoutResultLeft();
    }

    /**
     * {@inheritDoc}
     */
    public function setPayoutResultLeft($payout_result_left)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPayoutResultLeft', [$payout_result_left]);

        return parent::setPayoutResultLeft($payout_result_left);
    }

    /**
     * {@inheritDoc}
     */
    public function getPayoutResultRight()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPayoutResultRight', []);

        return parent::getPayoutResultRight();
    }

    /**
     * {@inheritDoc}
     */
    public function setPayoutResultRight($payout_result_right)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPayoutResultRight', [$payout_result_right]);

        return parent::setPayoutResultRight($payout_result_right);
    }

    /**
     * {@inheritDoc}
     */
    public function getPayoutResultType()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPayoutResultType', []);

        return parent::getPayoutResultType();
    }

    /**
     * {@inheritDoc}
     */
    public function setPayoutResultType($payout_result_type)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPayoutResultType', [$payout_result_type]);

        return parent::setPayoutResultType($payout_result_type);
    }

}
