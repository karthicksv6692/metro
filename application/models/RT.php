<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RT extends CI_Model{
  static $root_user_id = 1;
	
	static $REF 	= array(
						'user'				=>	1,
						'vendor'			=>	2,
						'bank'				=>	3,
						'product'			=>	4,
						'package'			=>	5,
						'package_product'	=>	6,
						'user_id_proof'		=>	7,
						'other_id_proof'	=>	8,
						'messages'			=>	9,
					);

	static $user_account_transfer_per_day_limit_amount = 10000;
	static $user_account_transfer_per_day_limit_transfer = 5000;
	static $user_account_transfer_per_month_limit_amount = 50000;
  static $user_account_transfer_per_month_limit_transfer = 20000;


  static $_ec_pack = 200;
  static $_100bvi = 100;
  static $_200bvi = 150;
  static $_300bvi = 200;
  static $_400bvi = 250;
  static $_500bvi = 300;

  static $default_ec_pack_id = 18;

  static $default_stock_pointer_id = 13;
	

  static $_100bvi_ID = 12;
  static $_200bvi_ID = 13;
  static $_300bvi_ID = 21;
  static $_400bvi_ID = 15;
  static $_500bvi_ID = 16;
  static $_ec_ID = 18;


  static $deafault_user_role_id = 8;
  static $deafault_admin_role_id = 7;

  static $level_ec = 5;
  static $level_bundle = 10;
  static $level_sprinter = 10;

  static $level_100bv  = 20;
  static$level_200bv = 20;
  static$level_300bv = 20;
  static$level_400bv = 20;
  static$level_500bv = 20;

  static $level_600bv  = 30;
  static $level_700bv = 30;
  static $level_800bv = 30;
  static $level_900bv = 30;
  static $level_1000bv = 30;

  static $level_1100bv = 50;
  static $level_1200bv = 50;
  static $level_1300bv = 50;
  static $level_1400bv = 50;
  static $level_1500bv = 50;

  static $level_1600bv = 100;
  static $level_1700bv = 100;
  static $level_1800bv = 100;
  static $level_1900bv = 100;
  static $level_2000bv = 100;

  static $level_2100bv = 150;
  static $level_2200bv = 150;
  static $level_2300bv = 150;
  static $level_2400bv = 150;
  
  static $level_2500bv = 200;

	public function xx(){
		echo "Hai Welcome";
	}

}
?>