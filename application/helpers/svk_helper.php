<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('is_user_logged_in')){
  function is_user_logged_in(){
    if(isset($_SESSION['logged_in']) && $_SESSION['logged_in'] == 1){
    }else{
      redirect();
    }
  }
}
if ( ! function_exists('is_pinstore_logged_in')){
	function is_pinstore_logged_in(){
		if(isset($_SESSION['pinstore_logged_in']) && $_SESSION['pinstore_logged_in'] == 1){
		}else{
			redirect('index.php/pin/pin_store/pin_store_view');
		}
	}
}
if ( ! function_exists('is_enter')){
	function is_enter(){
		if(isset($_SESSION['role_id']) && $_SESSION['role_id'] == RT::$deafault_admin_role_id){
		}else{
			redirect('index.php/login/dashboard');
		}
	}
}
if ( ! function_exists('is_pin_enter')){
	function is_pin_enter(){
		if(isset($_SESSION['role_id']) && $_SESSION['role_id'] == RT::$default_stock_pointer_id || $_SESSION['role_id'] == RT::$deafault_admin_role_id){
		}else{
			redirect('index.php/login/dashboard');
		}
	}
}

?>