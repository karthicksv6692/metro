<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\Query\ResultSetMapping;
require_once 'ssp.customized.class.php';
class Login extends CI_Controller {
   protected static $hunbvi_left=0;
    protected static $hunbvi_right=0;
	public function index(){
		$this->load->view('includes/header');
		$this->load->view('login/Login');
		$this->load->view('includes/footer');
	}
	public function user_verification(){
		$key = $this->input->get('id');
		$url = base_url('index.php/login/user_verification')."?id=";
		$utill_obj = new Common_utill();
		$exist = $utill_obj->get_single_row_using_array_value('User','user_verification_url',$url.$key);
		if($exist != null){
			if($exist->getIsActive() == 0){
				$update_user_active = $utill_obj->set_something_using_array_value('User','user_verification_url',$url.$key,'setIsActive',1);
				$update_user_email_verified = $utill_obj->set_something_using_array_value('User','user_verification_url',$url.$key,'setUserEmailVerified',1);
				if($update_user_active == 1 && $update_user_email_verified == 1){
					$this->session->set_flashdata('success', '<p>User Activated Successfully</p>');
					redirect('index.php/login','refresh');
				}else{
					$this->session->set_flashdata('error', '<p>User Active Update Failed</p>');
					redirect('index.php/login','refresh');
				}
			}else{
				$this->session->set_flashdata('error', '<p>User Already Veridied</p>');
				redirect('index.php/login','refresh');
			}
		}else{

			$url = base_url('login/user_verification')."?id=";
			$utill_obj = new Common_utill();
			$exist = $utill_obj->get_single_row_using_array_value('User','user_verification_url',$url.$key);
			if($exist != null){
				if($exist->getIsActive() == 0){
					$update_user_active = $utill_obj->set_something_using_array_value('User','user_verification_url',$url.$key,'setIsActive',1);
					$update_user_email_verified = $utill_obj->set_something_using_array_value('User','user_verification_url',$url.$key,'setUserEmailVerified',1);
					if($update_user_active == 1 && $update_user_email_verified == 1){
						$this->session->set_flashdata('success', '<p>User Activated Successfully</p>');
						redirect('index.php/login','refresh');
					}else{
						$this->session->set_flashdata('error', '<p>User Active Update Failed</p>');
						redirect('index.php/login','refresh');
					}
				}else{
					$this->session->set_flashdata('error', '<p>User Already Veridied</p>');
					redirect('index.php/login','refresh');
				}
			}else{
				$this->session->set_flashdata('error', '<p>User Not Exists</p>');
				redirect('index.php/login','refresh');
			}
		}
	}
	public function do_login(){ 
		$utill_obj = new Common_utill();
		if($this->input->post('user_name') != null && $this->input->post('password') != null){
			$this->form_validation->set_rules('user_name', 'User Name',array('trim','required','min_length[1]'),array('required'=> 'Please fill %s .'));
			$this->form_validation->set_rules('password', 'Password',array('trim','required','min_length[3]'),array('required'=> 'Please fill %s .'));
			if ($this->form_validation->run() !== false){
				$em = $this->doctrine->em;
				$user_name = $this->input->post('user_name');
				$password = $this->input->post('password');
				
				$login = $utill_obj->login_check($user_name,$password);
				if($login == 1){
					$user_id = $_SESSION['user_id'];
					$data['attachment']  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $user_id, 'attachment_referer_type'=>1));
					$data['access_ids']  = $em->getRepository('Entity\User_access')->findBy(array('user_access_user_id' => $user_id));

					$data['obj'] = $utill_obj->get_user_details($user_id);
          $pck_ar = $utill_obj->get_all_active_packages_promary_key();
          rsort($pck_ar);
          if($user_id != 2){
            $package_obj = $this->all_payout($user_id);
            $data['pck'] = array();
            $data['lf'] = array();
            $data['rt'] = array();

            foreach ($package_obj as $pack) {
              array_push($data['pck'], $pack['package_name']);
              array_push($data['lf'], $pack['left']);
              array_push($data['rt'], $pack['right']);
            }
            $data['left_sum'] = array_sum($data['lf']);
            $data['right_sum'] = array_sum($data['rt']);

            $up_obj = $utill_obj->get_single_row_using_array_value('User_package_purchase','user_id',$user_id);

            if($up_obj->getUserPurchasePinId()->getPinRequestPackageId() != null){
              if($up_obj->getUserPurchasePinId()->getPinRequestPackageId()->getPackageId() == 18){
                $rep_qry = $em->createQuery("SELECT p.pin_id FROM Entity\Pins as p  WHERE p.pin_request_for = 1 AND p.pin_status=0 AND p.pin_used_user_id =".$user_id);
                $rep_res = $rep_qry->getResult();
                if(count($rep_res) == 0){
                  $us = $utill_obj->get_single_row_using_primary_key('User',$user_id);
                 $ddt = $us->getCreatedAt()->format('Y-m-d');
                  $end = date("Y-m-d", strtotime($ddt."+30 days"));
                  $_SESSION['notification'] = 'Your Last Repurchase Date is '.$end;
                }
              }
            }

            $user_purchase_id = $up_obj->getUserPurchaseId();
            $obj_ar = array();
            for($i=0;$i<count($pck_ar);$i++){
              $query = $em->createQuery("SELECT MAX(b.payout_id) FROM Entity\Payout as b WHERE (b.total_pv_left != 0 OR b.total_pv_right != 0) AND b.package_id=".$pck_ar[$i]." AND b.user_purchase_id=".$user_purchase_id);
              $single_res = $query->getResult();
              if($single_res[0][1] != null){
                $max_payout_id = $single_res[0][1];
                $fin_res = $em->getRepository('Entity\Payout')->find($max_payout_id);
                // print_r($fin_res);exit();
                if($fin_res != null){
                  $obj = new Entity\Payout_details_view();
                  $obj->setPackageName($fin_res->getPackageId()->getPackageName());
                  $obj->setPersonalSalesCount(0);
                  $obj->setPersonalSalesSide(0);
                  $obj->setPreviousLeft($fin_res->getPreviousLeft());
                  $obj->setPreviousRight($fin_res->getPreviousRight());
                  $obj->setCurrentLeft($fin_res->getCurrentLeft());
                  $obj->setCurrentRight($fin_res->getCurrentRight());
                  $obj->setTotalLeft($fin_res->getTotalLeft());
                  $obj->setTotalRight($fin_res->getTotalRight());

                  if($fin_res->getCarryForwardSide() == 0){
                    $obj->setCarryLeft($fin_res->getCarryForward());
                    $obj->setCarryRight(0);
                  }else{
                    $obj->setCarryLeft(0);
                    $obj->setCarryRight($fin_res->getCarryForward());
                  }
                  $obj->setPairDetailsTotal($fin_res->getPairDetailsTotal());
                  $obj->setPairDetailsFlushed($fin_res->getPairDetailsFlushed());

                  $obj->setTotalPvLeft($fin_res->getTotalPvLeft());
                  $obj->setTotalPvRight($fin_res->getTotalPvRight());
                  array_push($obj_ar, $obj);
                }
              }
            }
            $data['final_obj_ar'] = $obj_ar;
          }else{
            $data['pck'] = array();
            $data['lf'] = array();
            $data['rt'] = array();
            $data['left_sum'] = 0;
            $data['right_sum'] =0;
            $data['final_obj_ar'] = array();

            
          }
					$this->session->set_flashdata('success', '<p>Login Successfully</p>');
					$this->load->view('header');
					$this->load->view('user/user/Dashboard',$data);
					$this->load->view('footer');	
				}elseif($login == -4){
          $this->session->set_flashdata('error', '<p>Please  Update Your Pan & KYC</p>');
              redirect('index.php/login','refresh');
        }elseif($login == -3){
					$this->session->set_flashdata('error', '<p>Enter Valid UserName/Password</p>');
	         		redirect('index.php/login','refresh');
				}elseif($login == -2){
					$this->session->set_flashdata('error', '<p>User not Exists</p>');
	         		redirect('index.php/login','refresh');
				}elseif($login == -1){
					$this->session->set_flashdata('error', '<p>Enter Valid Password</p>');
	         		redirect('index.php/login','refresh');
				}elseif($login == 0){
					$this->session->set_flashdata('error', '<p>User is Locked. Contact Admin</p>');
	         		redirect('index.php/login','refresh');
				}else{
					$this->session->set_flashdata('error', '<p>Login Failed</p>');
	         		redirect('index.php/login','refresh');
				}
			}else{
				$this->session->set_flashdata('error', '<p>Enter Valid Login Details</p>');
	         	redirect('index.php/login','refresh');
			}
		}else{ $this->index(); }
	}
	public function dashboard(){ 
		is_user_logged_in();
		$em = $this->doctrine->em;
		$utill_obj = new Common_utill();
		$user_id = $_SESSION['user_id'];
		$data['attachment']  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $user_id, 'attachment_referer_type'=>1));
		$data['access_ids']  = $em->getRepository('Entity\User_access')->findBy(array('user_access_user_id' => $user_id));
		$pck_name = "";
		$bal = 0;
		
		
		    $bal_obj = $em->getRepository('Entity\User_account')->findOneBy(array('user_id' => $user_id));
		    if($bal_obj != null){
		      $bal = $bal_obj->getUserAccountRepurchaseAmount();
		    }else{$bal = 0;}
		   
		    $pin_obj =  $em->getRepository('Entity\Pins')->findOneBy(array('pin_used_user_id' => $user_id));
		    if($pin_obj != null){
		      $pck_name = $pin_obj->getPinRequestPackageId()->getPackageName();
		    }else{$pck_name = "";}
		
		    $data['repurchase_bal'] = $bal;
		    $data['pck_name'] = $pck_name;
		    
		    
		    $data['pin_obj'] = $pin_obj;
		     $data['bal_obj'] = $bal_obj;

		    
		$data['obj'] = $utill_obj->get_user_details($user_id);
    if($user_id != 2){
      $package_obj = $this->all_payout($user_id);
      $data['pck'] = array();
      $data['lf'] = array();
      $data['rt'] = array();

      foreach ($package_obj as $pack) {
        array_push($data['pck'], $pack['package_name']);
        array_push($data['lf'], $pack['left']);
        array_push($data['rt'], $pack['right']);
      }
      $data['left_sum'] = array_sum($data['lf']);
      $data['right_sum'] = array_sum($data['rt']);
    }else{
      $data['pck'] = array();
      $data['lf'] = array();
      $data['rt'] = array();
      $data['left_sum'] = 0;
      $data['right_sum'] =0;
      $q = $em->createQuery("SELECT pr.payout_result_user_id,SUM(LEAST(pr.`total_left_pv`,pr.`total_right_pv`)),u.user_id,u.user_name
       FROM Entity\Payout_result as pr INNER JOIN 
       Entity\User_package_purchase as up ON (up.user_purchase_id = pr.payout_result_user_id) 
       INNER JOIN  Entity\User as u ON (u.user_id = up.user_id) 
       where 
       (SELECT SUM(LEAST(pr2.total_left_pv,pr2.total_right_pv)) 
        FROM Entity\payout_result as pr2 where pr2.payout_result_user_id=pr.payout_result_user_id)>=10000 
       group by pr.payout_result_user_id ");
      $data['rank_res'] = $q->getResult();
    }
		$this->load->view('header');
		$this->load->view('user/user/Dashboard',$data);
		$this->load->view('footer');	
	}
	public function get_pck_name(){
            $em = $this->doctrine->em;
	    $pin_obj =  $em->getRepository('Entity\Pins')->findOneBy(array('pin_used_user_id' => $_SESSION['user_id']));
	    if($pin_obj != null){
	      echo $pck_name = $pin_obj->getPinRequestPackageId()->getPackageName();
	    }else{echo 0;}
	
	  }
	  public function get_rep_amt(){
	   $em = $this->doctrine->em;
	    $bal_obj = $em->getRepository('Entity\User_account')->findOneBy(array('user_id' => $_SESSION['user_id']));
	    if($bal_obj != null){
	      echo $bal = $bal_obj->getUserAccountRepurchaseAmount();
	    }else{echo 0;}
	
	  }
	public function dashboard_edit(){
		$main_folder_name='user';$sub_folder_name='user';$file_name='dashboard_edit';
		$em = $this->doctrine->em;
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('User', 'getUserId', 'getUserName', 'index.php/user/user_crud', 'Edit', 'user_id');
		$data['pincode_list'] = $em->getRepository("Entity\Pincode")->findAll();
		$data['country_list'] = $em->getRepository("Entity\Country")->findAll();
		if($this->input->post('user_id') != null){
			$this->form_validation->set_rules('user_id', 'ProductId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$user_id = $this->input->post('user_id');

				$current_data = array(  'cur_data' => $user_id);
				$this->session->set_userdata($current_data);

				$data['attachment']  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $user_id, 'attachment_referer_type'=>1));
				$data['obj'] = $utill_obj->get_user_details($user_id);
				$user_obj  = $em->find('Entity\User',$user_id);
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}else{
				redirect('index.php/login/dashboard','refresh');
			}
		}
	}
	public function login_user(){
		is_user_logged_in();
		$em = $this->doctrine->em;
		$utill_obj = new Common_utill();
		$user_id = $_SESSION['user_id'];
		$data['attachment']  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $user_id, 'attachment_referer_type'=>1));
		$data['access_ids']  = $em->getRepository('Entity\User_access')->findBy(array('user_access_user_id' => $user_id));
		$data['bank']  = $em->getRepository('Entity\Account_details')->findBy(array('account_refer_id' => $user_id, 'account_refer_type'=>1));
		$data['obj'] = $utill_obj->get_user_details($user_id);
		$this->load->view('header');
		$this->load->view('user/user/Login_user',$data);
		$this->load->view('footer');	
	}
	public function logout(){
		$utill_obj = new Common_utill();
		$login = $utill_obj->is_logged_in();
		if($login == 1){
			$logout = $utill_obj->logout();
			if($logout == 1){
				session_destroy();
			    unset($_SESSION);
			    session_start(); 
			    session_regenerate_id(true);
				$this->session->set_flashdata('success', '<p>Successfully Logout</p>');
         		redirect('index.php/login','refresh');
			}else{
				$this->session->set_flashdata('error', '<p>Logout Failed</p>');
         		redirect('index.php/login','refresh');
			}
		}else{
			$this->session->set_flashdata('error', '<p>User not LoggedIn</p>');
         	redirect('index.php/login','refresh');
		}
	}
	public function forget(){
		$utill_obj = new Common_utill();
		if($this->input->post('forget_name') != null || $this->input->post('forget') != null ){
      $this->form_validation->set_rules('forget_name', 'Forget','trim|required',array('required'=> 'Requierd'));
			$this->form_validation->set_rules('forget_mobile', 'Mobile','trim|required|regex_match[/^[0-9]{10,15}$/]',array('required'=> 'Requierd'));
			if ($this->form_validation->run() != false){
        $user_name = $this->input->post('forget_name');
				$forget_mobile = $this->input->post('forget_mobile');
        $check = null;
        $check = $utill_obj->get_single_row_using_array_value('User','user_name',$user_name);
        if($check == null){
			 	 $check = $utill_obj->get_single_row_using_array_value('User','user_email',$user_name);
        }
        if($check != null){
            if($check->getContactId() != null){
              if($check->getContactId()->getContactMobilePrimary() == $forget_mobile){
                $password = $check->getPasswordId()->getPassword();
                $first_name = $check->getFirstName();
                $last_name = $check->getLastName();
                // MEssage Start
                  $sent_message = "Dear ".$first_name.' '.$last_name.', Your Login Password is '.$password.'. Visit as www.metrokings.biz';
                  $mobile_number = $forget_mobile;
                  $var_message = urlencode($sent_message);
                  $var_destination = urlencode($mobile_number);
                  $var_message_type = 2;
                  $var_senderid = urlencode('METROK');

                  $ch = curl_init();
                  curl_setopt($ch, CURLOPT_URL, "http://sms.dhinatechnologies.com/api/sms_api.php?username=metrokings&api_password=swyvpq558x0&message=$var_message&destination=$var_destination&type=$var_message_type&sender=$var_senderid");
                  curl_setopt($ch, CURLOPT_HEADER, 0);  
                  curl_exec($ch);
                  curl_close($ch);
                // MEssage Start
                $this->session->set_flashdata('success', '<p>Password Sent Your Registered Mobile & Email</p>');
     					  redirect('index.php/login','refresh');
              }else{
                $data['error'] = 'Enter Registered Mobile';
                $this->load->view('includes/header');
                $this->load->view('login/forget',$data);
              }
            }else{
              $data['error'] = 'User Mobile Not Registered. Contact to Admin';
              $this->load->view('includes/header');
              $this->load->view('login/forget',$data);
            }
				}else{
          $id_check = $utill_obj->get_single_row_using_primary_key('User',$user_name);
          if($id_check != null){
            if($id_check->getContactId() != null){
              if($id_check->getContactId()->getContactMobilePrimary() == $forget_mobile){
                $password = $id_check->getPasswordId()->getPassword();
                $first_name = $id_check->getFirstName();
                $last_name = $id_check->getLastName();

                // MEssage Start
                    $sent_message = "Dear ".$first_name.' '.$last_name.', Your Login Password is '.$password.'.Visit as www.metrokings.biz';
                    $mobile_number = $forget_mobile;
                    $var_message = urlencode($sent_message);
                    $var_destination = urlencode($mobile_number);
                    $var_message_type = 2;
                    $var_senderid = urlencode('METROK');

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, "http://sms.dhinatechnologies.com/api/sms_api.php?username=metrokings&api_password=swyvpq558x0&message=$var_message&destination=$var_destination&type=$var_message_type&sender=$var_senderid");
                    curl_setopt($ch, CURLOPT_HEADER, 0);  
                    curl_exec($ch);
                    curl_close($ch);
                  // MEssage Start

                $this->session->set_flashdata('success', '<p>Password Sent Your Registered Mobile & Email</p>');
                redirect('index.php/login','refresh');
              }else{
                $data['error'] = 'Enter Registered Mobile';
                $this->load->view('includes/header');
                $this->load->view('login/forget',$data);
              }
            }else{
              $data['error'] = 'User Mobile Not Registered. Contact to Admin';
              $this->load->view('includes/header');
              $this->load->view('login/forget',$data);
            }
          }else{
            $data['error'] = 'Invalid User';
            $this->load->view('includes/header');
            $this->load->view('login/forget',$data);
          }
        }
				
			}else{
				$this->session->set_flashdata('error', '<p>Error in Fields/p>');
			 	$this->load->view('includes/header');
				$this->load->view('login/forget');
			}
		}else{
			$this->load->view('includes/header');
			$this->load->view('login/forget');			
		}
	}
	public function reset(){
		$utill_obj = new Common_utill();
		if($this->input->post('password')!= NULL || $this->input->post('user_id')!= NULL){
			$this->form_validation->set_rules('user_id', 'ProductId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			$this->form_validation->set_rules('password', 'Password','trim|required|callback_find_alphabets|min_length[5]|max_length[20]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]',array( 'required'=>'Password Required','min_length' => 'Minimum 5 Charecters Need','max_length' => 'Maximum 20 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			$this->form_validation->set_rules('repassword', 'Password','trim|required|matches[password]',array('matches'=>'Password Does not Match'));
			if ($this->form_validation->run() != false){
				$user_id = $this->input->post('user_id');
				$password = $this->input->post('password');

				$update_password = $utill_obj->update_new_password($user_id,$password);
				if($update_password == 1){
					$this->session->set_flashdata('success', '<p>Password Updated Successfully</p>');
 					redirect('index.php/login','refresh');
				}elseif($update_password == -3){
					$this->session->set_flashdata('success', '<p>UserId Wrong</p>');
 					redirect('index.php/login','refresh');
				}elseif($update_password == -2){
					$this->session->set_flashdata('success', '<p>User Not Exists</p>');
 					redirect('index.php/login','refresh');
				}else{
					$data['user_id'] = $this->input->post('user_id');
					$this->session->set_flashdata('error', '<p>Error in Fileds.Not Updated</p>');
					$this->load->view('includes/header');
					$this->load->view('login/Reset',$data);
				}
			}else{
				$data['user_id'] = $this->input->post('user_id');
				$this->session->set_flashdata('error', '<p>Password doesn\'t Match</p>');
				$this->load->view('includes/header');
				$this->load->view('login/Reset',$data);
			}
		}else{
			$key = $this->input->get('id');
			$url = base_url('index.php/login/Reset')."?id=";
			$exist = $utill_obj->get_single_row_using_array_value('Password','forget_password_url',$url.$key);
			if($exist != null){
				$user_obj = $utill_obj->get_single_row_using_array_value('User','password_id',$exist->getPasswordId());
				if($user_obj != null){
					$data['user_id'] = $user_obj->getUserId();
					$this->load->view('includes/header');
					$this->load->view('login/Reset',$data);
				}else{
					$this->session->set_flashdata('error', '<p>Wrong User</p>');
					redirect('index.php/login','refresh');
				}
			}else{
				$this->session->set_flashdata('error', '<p>Wrong Reset Url</p>');
				redirect('index.php/login','refresh');
			}
		}
	}
	public function checkDateFormat($date) {
    if (preg_match('/^\d{2}\/\d{2}\/\d{4}$/', $date)) {
        if(checkdate(substr($date, 3, 2), substr($date, 0, 2), substr($date, 6, 4)))
            return true;
        else
            return false;
    } else {
        return false;
    }
	}
	public function alphabets_and_number($str){
	   if (preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function find_alphabets($str){
	   if (preg_match('#[a-zA-Z]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
		$this->load->view('header');
		$this->load->view(''.$main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
		$this->load->view('footer');
	}
	public function change_password(){
		$em = $this->doctrine->em;
		$main_folder_name='user';$sub_folder_name='password';$file_name='Pass_change';
		$utill_obj = new Common_utill();
		if($this->input->post('pwd') != null && $this->input->post('n_pwd') != null){
			$old_pw = $this->input->post('pwd');
			$new_pw = $this->input->post('n_pwd');

      // print_r($_POST);exit();

			$user_name = $_SESSION['username'];
				
			$login = $utill_obj->login_check($user_name,$old_pw);
			if($login == 1){
				$this->form_validation->set_rules('n_pwd', 'Password','trim|required|callback_find_alphabets|min_length[5]|max_length[20]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]',array( 'required'=>'Password Required','min_length' => 'Minimum 5 Charecters Need','max_length' => 'Maximum 20 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
				$this->form_validation->set_rules('rn_pwd', 'Password','trim|required|matches[n_pwd]',array('matches'=>'Password Does not Match'));

				if ($this->form_validation->run() != false){
					$uobj = $utill_obj->get_single_row_using_primary_key('User',$_SESSION['user_id']);
					if($uobj != null){
						$uobj->getPasswordId()->setPassword($new_pw);
						$uobj->getPasswordId()->setSalt($new_pw);
						$uobj->getPasswordId()->setPasswordPrev($old_pw);
						$em->persist($uobj);
						$em->flush();
						$this->session->set_flashdata('success', '<p>Password updated Successfully</p>');
						redirect('index.php/login/dashboard','refresh');
					}else{
						$this->session->set_flashdata('error', '<p>User Not Exists</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name);	
					}
				}else{
					$this->session->set_flashdata('error', '<p>Password not Match</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name);
				}
			}else{
				$this->session->set_flashdata('error', '<p>Old Password Wrong</p>');
				$this->view_call($main_folder_name,$sub_folder_name,$file_name);
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name);
		}
	}

  public function ceiling(){
    $em=$this->doctrine->em;
    $ar = array();
    $hour = date('H');
    if($hour>=14){
      $d1 = date('Y-m-d').' 02:00:00';
      $d2 = date('Y-m-d').' 13:59:59';
    }else{
      $x = date('Y-m-d');
      $d1 = date("Y-m-d", strtotime($x.'-1 days')).' 14:00:00';
      $d2 = date('Y-m-d').' 01:59:59';
    }
    $query = $em->createQuery("SELECT DISTINCT(u.user_id),u.user_name FROM Entity\Payout_result as pr INNER JOIN Entity\User_package_purchase as upp WITH (pr.payout_result_user_id = upp.user_purchase_id) INNER JOIN Entity\User as u WITH (u.user_id = upp.user_id) WHERE  pr.total_left_pv >=500 AND pr.total_right_pv >=500 AND pr.created_at BETWEEN '".$d1."' AND '".$d2."'" );

    $xx = $query->getResult();

    if(count($xx) > 0){
      $achievers = $xx;
    }else{
      $achievers = $this->check_cel(0);
    }

    if(count($achievers) > 0){
      foreach ($achievers as $ach) {
        $find = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_id' => $ach[1],'attachment_referer_type' => 1)); 
        if($find != null){
          if($find->getAttachmentImageUrl() != -1 && $find->getAttachmentImageUrl() != null){
            array_push($ar,array('name' => $ach['user_name'] , 'img_url' => $find->getAttachmentImageUrl()));
          }else{
            array_push($ar,array('name' => $ach['user_name'] , 'img_url' => 0));
          }
        }else{
          array_push($ar,array('name' => $ach['user_name'] , 'img_url' => 0));
        }
      }
      $data['user'] = $ar;
    }else{$data['user'] = 0;}
    $this->load->view('includes/header');
    $this->load->view('common/Ceiling',$data);
    $this->load->view('includes/footer');  
  }
  public function check_cel($i){
    $em=$this->doctrine->em;
    $d = date('Y-m-d');
    $sec_d1 = date("Y-m-d", strtotime($d.'-'.$i.' days')).' 14:00:00';
    $sec_d2 = date("Y-m-d", strtotime($sec_d1.'+'.$i.' days')).' 01:59:59';

    $fir_d1 = date("Y-m-d", strtotime($d.'-'.$i.' days')).' 02:00:00';
    $fir_d2 = date("Y-m-d", strtotime($d.'-'.$i.' days')).' 13:59:59';

    $query = $em->createQuery("SELECT DISTINCT(u.user_id),u.user_name FROM Entity\Payout_result as pr INNER JOIN Entity\User_package_purchase as upp WITH (pr.payout_result_user_id = upp.user_purchase_id) INNER JOIN Entity\User as u WITH (u.user_id = upp.user_id) WHERE  pr.total_left_pv >=500 AND pr.total_right_pv >=500 AND pr.created_at BETWEEN '".$sec_d1."' AND '".$sec_d2."'" );
    $xx = $query->getResult();
    if(!empty($xx)){
      return $xx;
    }else{
      $query = $em->createQuery("SELECT DISTINCT(u.user_id),u.user_name FROM Entity\Payout_result as pr INNER JOIN Entity\User_package_purchase as upp WITH (pr.payout_result_user_id = upp.user_purchase_id) INNER JOIN Entity\User as u WITH (u.user_id = upp.user_id) WHERE  pr.total_left_pv >=500 AND pr.total_right_pv >=500 AND pr.created_at BETWEEN '".$fir_d1."' AND '".$fir_d2."'" );
      $xx = $query->getResult();
      if(!empty($xx)){
        return $xx;
      }else{
        return $this->check_cel($i+1);
      }
    }
  }

  public function home_products($id=null){
    $em=$this->doctrine->em;
    $utill_obj = new Common_utill();
    $con = '';
    if($id != null){
      $con = ' AND p.product_id ='.$id;
    }

    $query = $em->createQuery("SELECT p.product_id FROM Entity\Product as p WHERE  p.product_is_visible =1 $con order by p.product_id desc" )->setMaxResults(25);
    $result = $query->getResult();

    $product_ar = array();
    foreach ($result as $res) {
      $rs = $utill_obj->get_product_detail_from_product_id_for_cart($res['product_id']);
      array_push($product_ar, $rs);
    }
    $data['products']     = $product_ar;
    $this->load->view('includes/header');
    $this->load->view('common/Home_products',$data);
    $this->load->view('includes/footer');  
  }

  public function mail_func(){
    // $to = "svkarthicksv@gmail.com";
    // $subject = "My subject";
    // $txt = "Hello world! MEtro";
    // $headers = "From: webmaster@example.com" . "\r\n" .
    // "CC: somebodyelse@example.com";
    
    // mail($to,$subject,$txt,$headers);
    
    $ch = curl_init();

  // set URL and other appropriate options
  curl_setopt($ch, CURLOPT_URL, "http://metro.metrokings.biz/index.php/login/main_func/");
  curl_setopt($ch, CURLOPT_HEADER, 0);  

  // grab URL and pass it to the browser
  curl_exec($ch);

  // close cURL resource, and free up system resources
  curl_close($ch);
  }

  public function all_payout($user_id){
    $em = $this->doctrine->em;
    $utill_obj = new Common_utill();
    $pack_ar = array();
    $pck_ar = $utill_obj->get_all_active_packages_promary_key();
    rsort($pck_ar);
    $up_obj = $utill_obj->get_single_row_using_array_value('User_package_purchase','user_id',$user_id);
    
    $user_purchase_id = $up_obj->getUserPurchaseId();
    $tree_obj = $this->get_hole_tree_obj($user_purchase_id);
    for($i=0;$i<count($pck_ar);$i++){
      self::$hunbvi_right=0;
      self::$hunbvi_left=0;
      $cnt = $this->get_left_package_count_hole($tree_obj,$pck_ar[$i]);
      $query = $em->createQuery("SELECT MAX(b.payout_id) FROM Entity\Payout as b WHERE b.package_id=".$pck_ar[$i]." AND b.user_purchase_id=".$user_purchase_id);
      $single_res = $query->getResult();
      if($single_res[0][1] != null){
        $max_payout_id = $single_res[0][1];
        $fin_res = $em->getRepository('Entity\Payout')->find($max_payout_id);
        if($fin_res != null){
          $ar = array(
            'package_name' => $fin_res->getPackageId()->getPackageName(),
            'left' => $cnt['cnt_left'],
            'right' => $cnt['cnt_right'],
          );
          array_push($pack_ar,$ar);;
        }
      }
    }
    return $pack_ar;
  }

  public function get_hole_tree_obj($id){
    $utill_obj = new Common_utill();
    $em = $this->doctrine->em;

    $res = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$id);

    $pck_r = $utill_obj->get_all_active_packages_primary_key_name();

    $uid = $res->getUserId()->getUserId();

    $un = $res->getUserId()->getUserName();
    $fn = $res->getUserId()->getFirstName();

    $attachment  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $uid, 'attachment_referer_type'=>1));
    $img = 0;
    if($attachment != null){
      if($attachment[0]->getAttachmentImageUrl() != null || $attachment[0]->getAttachmentImageUrl() != -1){
        $img = $attachment[0]->getAttachmentImageUrl();
      }else{ $img = 0; }
    }else{ $img = 0; }

    $pid = $res->getUserPurchaseId();
    $ct = $res->getUserId()->getCreatedAt();

    $pin_obj = $res->getUserPurchasePinId();
    $package_obj = $pin_obj->getPinRequestPackageId();
    if($package_obj != null){
      $package_id = $package_obj->getPackageId();
      $package_name = $package_obj->getPackageName();
      $package_price = $package_obj->getPackagePrice();
    }else{$package_id=0;$package_name=0;$package_price=0;}

    $obj = new Entity\Geanology();
    $obj->setPurchaseId($pid);
    $obj->setPackageId($package_id);
    $obj->setPackageName($package_name);
    // Table Code Start
      $db_obj_ar = array();
      $pck_ar = $utill_obj->get_all_active_packages_promary_key();
      rsort($pck_ar);
      
      if($_SESSION['role_id'] == 7){
        $user_id=1;
      }else{
        $user_id = $_SESSION['user_id'];
      }
      $up_obj = $utill_obj->get_single_row_using_array_value('User_package_purchase','user_id',$user_id);
      
      $user_purchase_id = $up_obj->getUserPurchaseId();
      $obj_ar = array();

    // Table Code End
    if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
      $obj->setLeftObj($this->get_hole_tree_obj($res->getUserPurchaseGeneLeftPurchaseId()));
    }
    if($res->getUserPurchaseGeneRightPurchaseId() != 0){
   
      $obj->setRightObj($this->get_hole_tree_obj($res->getUserPurchaseGeneRightPurchaseId()));
    }
     
    return $obj;
  }
  public function get_left_package_count_hole($obj,$id){
    if( $obj->getLeftObj() != null){  
     if($obj->getLeftObj()->getPackageId() == $id){
        self::$hunbvi_left=1;
     }
      $this->printTreeL_hole($obj->getLeftObj(),$id);
    }

    if($obj->getRightObj() != null){  
     if($obj->getRightObj()->getPackageId() == $id){
        self::$hunbvi_right=1;
     }
      $this->printTreeR_hole($obj->getRightObj(),$id);
    }
   
    return ['cnt_left'=>self::$hunbvi_left,'cnt_right'=>self::$hunbvi_right];
  }
  public function printTreeL_hole($obj,$id){
      if( $obj->getLeftObj() != null){
        // echo $obj->getLeftObj()->getPurchaseId();echo"<br>";
          if($obj->getLeftObj()->getPackageId() == $id){
            self::$hunbvi_left++;
          }
        $this->printTreeL_hole($obj->getLeftObj(),$id);
      }
      if($obj->getRightObj() != null){
        if($obj->getRightObj()->getPackageId() == $id){
          self::$hunbvi_left++;
        }
      $this->printTreeL_hole($obj->getRightObj(),$id);
      }
  }
  public function printTreeR_hole($obj,$id){
    if( $obj->getLeftObj() != null){
      if($obj->getLeftObj()->getPackageId() == $id){
        self::$hunbvi_right++;
      }
      $this->printTreeR_hole($obj->getLeftObj(),$id);
    }
    if($obj->getRightObj() != null){
      if($obj->getRightObj()->getPackageId() == $id){
        self::$hunbvi_right++;
      }
    $this->printTreeR_hole($obj->getRightObj(),$id);
    }
  }
}
?>