<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\Query\ResultSetMapping;
require_once 'ssp.customized.class.php';
class Bank_crud extends CI_Controller {

	public function __construct(){
		parent::__construct(); 
		is_user_logged_in();
	}
	public function index(){

		$current_data = array(  'cur_data' => "");
		$this->session->set_userdata($current_data);

		$utill_obj = new Common_utill();
		//$data['add'] = $utill_obj->has_access('pincode','add');

		$this->view();

	}
	public function view(){
		$this->load->view('header');
		$this->load->view('account/bank/View');
		$this->load->view('footer');	
	}
	public function datatable_ajax(){
		$CI = &get_instance();
		$CI->load->database();
		$sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
		$table = 'Bank_details';
		$primaryKey = 'bank_id';
		$columns = array(
		    array( 'db' => 'bank_name', 'dt' => 0 ),
		    array( 'db' => 'bank_ifsc',  'dt' => 1 ),
		    array( 'db' => 'bank_branch',  'dt' => 2 ),
		    array(
			        'db'        => 'bank_id',
			        'dt'        => 3,
			        'formatter' => function( $d, $row ) {

			        		$utill_obj = new Common_utill();
							// $edit = $utill_obj->has_access('address','edit');
							// $delete = $utill_obj->has_access('address','delete');
							// $s_view = $utill_obj->has_access('address','s_view');
			        		$edit = "asd";
							$delete = "asd";
							$s_view = "asd";

							if($s_view != null){
				        		$ss = '<form method="POST" class="icon_edit_form" action="'.base_url().'index.php/account_details/Bank_crud/S_view">
                            <input type="hidden" name="bank_id" value="'.$d.'">
                            <i class="fa fa-search-plus float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
				        	}else{$ss = '';}

				        	if($edit != null){
				        		$ee = ' <form method="POST" class="icon_edit_form" action="'.base_url().'index.php/account_details/Bank_crud/Edit">
                            <input type="hidden" name="bank_id" value="'.$d.'">
                            <i class="fa fa-pencil float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
				        	}else{$ee='';}

				        	if($delete != null){
				        		$dd = ' <i class="fa fa-trash-o float_left btn_delete but_del" aria-hidden="true" data-value="'.$d.'" ></i>';
				        	}else{$dd='';}
		      
				        	return $ss.$ee.$dd;
        			}
		    	)
			);
		echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns));
	}
	public function add(){
		$em = $this->doctrine->em;
		$utill_obj = new Common_utill();
		$data['pincode_list'] = $em->getRepository("Entity\Pincode")->findAll();
		$data['country_list'] = $em->getRepository("Entity\Country")->findAll();
		$main_folder_name='account';$sub_folder_name='bank';$file_name='Add';
		if($this->input->post('ifsc') != null || $this->input->post('bank') != null){
			$this->form_validation->set_rules('bank_name', 'Bank Name','trim|callback_find_alphabets|required|min_length[3]|max_length[255]|regex_match[/^[[a-zA-Z0-9._  ,\-]+$/]',array('required'=> 'Bank Name Requierd','min_length' => 'Bank Name needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			$this->form_validation->set_rules('ifsc', 'Bank IFSC','trim|callback_alphabets_and_number|required|min_length[5]|max_length[255]|regex_match[/^[[a-zA-Z0-9]+$/]',array('required'=> 'Bank Name Requierd','min_length' => 'Bank Name needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','alphabets_and_number'=>'Enter Valid IFSC Code'));
			$this->form_validation->set_rules('branch', 'Bank Branch','trim|callback_find_alphabets|required|min_length[3]|max_length[255]|regex_match[/^[[a-zA-Z0-9._ ,\-]+$/]',array('required'=> 'Bank Name Requierd','min_length' => 'Bank Name needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			$this->form_validation->set_rules('full_address', 'Address','trim|required|callback_find_alphabets|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			$this->form_validation->set_rules('pincode_no', 'Pincode','trim|required|regex_match[/^[0-9]+$/]|min_length[4]|max_length[8]',array('regex_match' => 'Numbers Only Allowed','min_length'=>'Minimumm length 4','max_length'=>'Maximum Length 8'));
			if($this->form_validation->run() != false){  //print_r($_POST);exit();
				$bank_name = $this->input->post('bank_name');
				$bank_ifsc = $this->input->post('ifsc');
				$bank_branch = $this->input->post('branch');
				$full_address = $this->input->post('full_address');
				$pincode_no = $this->input->post('pincode_no');
				$place = $this->input->post('place');
				$district = $this->input->post('district');
				$state = $this->input->post('state');
				$country = $this->input->post('country');
				$mobile = $this->input->post('mobile');
				$email = $this->input->post('email');
				$landline = $this->input->post('landline');
				//$country_obj = $this->add_country($country,$country_code);
				$find_bank = $em->getRepository('Entity\Bank_details')->findOneBy(array('bank_ifsc' => $bank_ifsc));
				if($find_bank == null){
					$bank_obj = $utill_obj->add_bank_with_address_contact_details($bank_name,$bank_ifsc,$bank_branch,$pincode_no,$place,$district,$state,$full_address,$country,$mobile,$email,$landline);
					if( $bank_obj->getBankId()  != null){
						$this->session->set_flashdata('success', '<p>Successfully Created</p>');
						redirect('index.php/account_details/bank_crud','refresh');
					}elseif($address_obj == -1){
						$this->session->set_flashdata('error', '<p>Country Already Exists</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					}elseif($address_obj == -2){
						$this->session->set_flashdata('error', '<p>Pincode Already Exists</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					}elseif($address_obj == -3){
						$this->session->set_flashdata('error', '<p>Address Insert Exists</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					}else{
						$this->session->set_flashdata('error', '<p>Bank Insert Failed</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					}
				}else{
					$this->session->set_flashdata('error', '<p>Bank IFSC Already Exists</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}
			}else{
				$this->session->set_flashdata('error', '<p>Error in Fields</p>');
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function edit(){
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Bank_details', 'getBankId', 'getBankName', 'index.php/account_details/bank_crud', 'Edit', 'bank_id');
		$em = $this->doctrine->em;
		$data['pincode_list'] = $em->getRepository("Entity\Pincode")->findAll();
		$data['country_list'] = $em->getRepository("Entity\Country")->findAll();

		$main_folder_name='account';$sub_folder_name='bank';$file_name='Edit';
		if($this->input->post('bank_id') != null){
			$this->form_validation->set_rules('bank_id', 'Bank id','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$bank_id = $this->input->post('bank_id');
				$em = $this->doctrine->em;
				$bank_obj  = $em->find('Entity\Bank_details',$bank_id); 
				if($bank_obj != null){

					$current_data = array(  'cur_data' => $bank_id);
					$this->session->set_userdata($current_data);

					$contact = $em->getRepository('Entity\Contact')->findOneBy(array('contact_refer_id' => $bank_id,'contact_refer_type' => 3)); 
					$data['contact_obj'] = $contact;
					$data['bank_obj'] = $bank_obj;
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}else{
					$this->session->set_flashdata('error', '<p>Bank Not Exists</p>');
					redirect('index.php/account_details/bank_crud','refresh');
				}
			}else{
				redirect('index.php/account_details/bank_crud','refresh');
			}
		}elseif($this->input->post('edit_bank_id') != null){
			$data['bank_obj'] = $em->find('Entity\Bank_details',$this->session->userdata('cur_data'));
			if($this->session->userdata('cur_data') == $this->input->post('edit_bank_id')) {
				$this->form_validation->set_rules('edit_bank_id', 'Bank id','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
				if ($this->form_validation->run() != false){ 
					$this->form_validation->set_rules('bank_name', 'Bank Name','trim|callback_find_alphabets|required|min_length[3]|max_length[255]|regex_match[/^[[a-zA-Z0-9._  ,\-]+$/]',array('required'=> 'Bank Name Requierd','min_length' => 'Bank Name needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
					$this->form_validation->set_rules('ifsc', 'Bank IFSC','trim|callback_alphabets_and_number|required|min_length[5]|max_length[255]|regex_match[/^[[a-zA-Z0-9]+$/]',array('required'=> 'Bank Name Requierd','min_length' => 'Bank Name needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','alphabets_and_number'=>'Enter Valid IFSC Code'));
					$this->form_validation->set_rules('branch', 'Bank Branch','trim|callback_find_alphabets|required|min_length[3]|max_length[255]|regex_match[/^[[a-zA-Z0-9._ ,\-]+$/]',array('required'=> 'Bank Name Requierd','min_length' => 'Bank Name needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
					$this->form_validation->set_rules('full_address', 'Address','trim|required|callback_find_alphabets|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
					$this->form_validation->set_rules('pincode_no', 'Pincode','trim|required|regex_match[/^[0-9]+$/]|min_length[4]|max_length[8]',array('regex_match' => 'Numbers Only Allowed','min_length'=>'Minimumm length 4','max_length'=>'Maximum Length 8'));
					if($this->form_validation->run() != false){  

						$bank_id = $this->input->post('edit_bank_id');
						$bank_name = $this->input->post('bank_name');
						$bank_ifsc = $this->input->post('ifsc');
						$bank_branch = $this->input->post('branch');
						$full_address = $this->input->post('full_address');
						$pincode_no = $this->input->post('pincode_no');
						$place = $this->input->post('place');
						$district = $this->input->post('district');
						$state = $this->input->post('state');
						$country = $this->input->post('country');
						$mobile = $this->input->post('mobile');
						$email = $this->input->post('email');
						$landline = $this->input->post('landline');

						$contact = $em->getRepository('Entity\Contact')->findOneBy(array('contact_refer_id' => $bank_id,'contact_refer_type' => 3)); 
						if($contact != null){
							$contact_id = $contact->getContactId();
						}else{$contact_id = 0;}

						$bank_obj = $utill_obj->update_bank_with_address_contact_details($bank_id,$bank_name,$bank_ifsc,$bank_branch,$pincode_no,$place,$district,$state,$full_address,$country,$contact_id,$mobile,$email,$landline);
						if( $bank_obj->getBankId()  != null  ){
							$this->session->set_flashdata('success', '<p>Successfully Updated</p>');
							redirect('index.php/account_details/bank_crud','refresh');
						}elseif($address_obj == -1){
							$this->session->set_flashdata('error', '<p>Country Not Exists</p>');
							$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
						}elseif($address_obj == -2){
							$this->session->set_flashdata('error', '<p>Pincode Not Exists</p>');
							$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
						}elseif($address_obj == -3){
							$this->session->set_flashdata('error', '<p>Address Not Exists</p>');
							$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
						}elseif($address_obj == -4){
							$this->session->set_flashdata('error', '<p>Bank Not Exists</p>');
							$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
						}else{
							$this->session->set_flashdata('error', '<p>Bank Update Failed</p>');
							$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
						}
					}else{
						$this->session->set_flashdata('error', '<p>Error in Fields</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					}
				}else{
					$this->session->set_flashdata('error', '<p>Error in Fields</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}
			}else{
				$this->session->set_flashdata('error', '<p>Wrong ID</p>');
				redirect('index.php/account_details/bank_crud','refresh');
			}
		}else{
			$this->load->view('header');
			$this->load->view('account/bank/Edit',$data);
			$this->load->view('footer');
		}
	}
	public function s_view(){
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Bank_details', 'getBankId', 'getBankName', 'index.php/account_details/bank_crud', 'S_view', 'bank_id');
		if($this->input->post('bank_id') != null){
			$this->form_validation->set_rules('bank_id', 'Bank id','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$bank_id = $this->input->post('bank_id');
				$em = $this->doctrine->em;
				$bank_obj  = $em->find('Entity\Bank_details',$bank_id); 
				if($bank_obj != null){
					$data['bank_obj'] = $bank_obj;
					$this->load->view('header');
					$this->load->view('account/bank/S_view',$data);
					$this->load->view('footer');
				}else{
					$this->session->set_flashdata('error', '<p>Bank Not Exists</p>');
					redirect('index.php/account_details/bank_crud','refresh');
				}
			}else{
				redirect('index.php/account_details/bank_crud','refresh');
			}
		}else{
			$this->load->view('header');
			$this->load->view('account/bank/S_view',$data);
			$this->load->view('footer');
		}
	}
	public function delete(){
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Bank_details', 'getBankId', 'getBankName', 'index.php/account_details/bank_crud', 'Delete', 'bank_id');
		$em = $this->doctrine->em;
		if($this->input->post('bank_id') != null){
			$this->form_validation->set_rules('bank_id', 'Bank id','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$bank_id = $this->input->post('bank_id');
				$em = $this->doctrine->em;
				$bank_obj  = $em->find('Entity\Bank_details',$bank_id); 
				if($bank_obj != null){
					$data['bank_obj'] = $bank_obj;
					$this->load->view('header');
					$this->load->view('account/bank/Delete',$data);
					$this->load->view('footer');
				}else{
					$this->session->set_flashdata('error', '<p>Bank Not Exists</p>');
					redirect('index.php/account_details/bank_crud','refresh');
				}
			}else{
				redirect('index.php/account_details/bank_crud','refresh');
			}
		}elseif($this->input->post('delete_bank_id') != null){
			$this->form_validation->set_rules('delete_bank_id', 'Bank id','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$bank_id = $this->input->post('delete_bank_id');
				$find_bank = $em->getRepository('Entity\Bank_details')->find($bank_id);
				if($find_bank != null){
					$contact = $em->getRepository('Entity\Contact')->findOneBy(array('contact_refer_id' => $bank_id,'contact_refer_type' => 3)); 
					if($contact != null){
						$em->remove($find_bank);
						$em->remove($find_bank->getBankAddressId());
						$em->remove($contact);
						try{
							$em->flush();
							$this->session->set_flashdata('success', '<p>Successfully Deleted</p>');
							redirect('index.php/account_details/bank_crud','refresh');
						}catch(ForeignKeyConstraintViolationException $e){
							$this->session->set_flashdata('error', '<p>ForeignKeyConstraintViolationException</p>');
							redirect('index.php/account_details/bank_crud','refresh');
					   	}
					}else{
						$em->remove($find_bank);
						try{
							$em->flush();
							$this->session->set_flashdata('success', '<p>Successfully Deleted</p>');
							redirect('index.php/account_details/bank_crud','refresh');
						}catch(ForeignKeyConstraintViolationException $e){
							$this->session->set_flashdata('error', '<p>ForeignKeyConstraintViolationException</p>');
							redirect('index.php/account_details/bank_crud','refresh');
					   	}
					}

				}else{
					$this->session->set_flashdata('error', '<p>Bank Not Exists</p>');
					redirect('index.php/account_details/bank_crud','refresh');
				}
			}else{
				$this->session->set_flashdata('error', '<p>Bank Not Exists</p>');
				redirect('index.php/account_details/bank_crud','refresh');
			}
		}else{
			$this->load->view('header');
			$this->load->view('account/bank/Delete',$data);
			$this->load->view('footer');
		}
	}
	public function ajax_ifsc(){  
		$utill_obj = new Common_utill();
		echo json_encode($utill_obj->get_bank_details_with_ifsc_code($this->input->post('ifsc')));
	}
	public function ajax_delete(){
		$em = $this->doctrine->em;
		if($this->input->post('delete_bank_id') != null){
			$this->form_validation->set_rules('delete_bank_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$bank_id = $this->input->post('delete_bank_id');
				$find_bank = $em->getRepository('Entity\Bank_details')->find($bank_id); 
				$data['bank_obj'] = $find_bank;
				if($find_bank != null){

					$contact = $em->getRepository('Entity\Contact')->findOneBy(array('contact_refer_id' => $bank_id,'contact_refer_type' => 3)); 
					if($contact != null){
						$em->remove($find_bank);
						$em->remove($find_bank->getBankAddressId());
						$em->remove($contact);
						try{
							$em->flush();
							echo 1;
						}catch(ForeignKeyConstraintViolationException $e){
							echo -1;
					   	}
					}else{
						$em->remove($find_bank);
						try{
							$em->flush();
							echo 1;
						}catch(ForeignKeyConstraintViolationException $e){
							echo -1;
					   	}
					}
					
				}else{
					echo -1;
				}
			}else{
				echo 0;
			}
		}else{
			echo 0;
		}
	}
	public function alphabets_and_number($str){
	   if (preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function find_alphabets($str){
	   if (preg_match('#[a-zA-Z]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function find_numbers($str){
	   if (preg_match('#[0-9]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
		$this->load->view('header');
		$this->load->view($main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
		$this->load->view('footer');
	}

}

?>