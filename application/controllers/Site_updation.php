<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

require_once 'ssp.customized.class.php';

class Site_updation extends CI_Controller {

  public function __construct() {
    parent::__construct();
  }
  public function index(){
    $main_folder_name='common';$sub_folder_name='site_updation';$file_name='View';
    $this->view_call($main_folder_name,$sub_folder_name,$file_name);
  }
  public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
    $this->load->view('header');
    $this->load->view(''.$main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
    $this->load->view('footer');
  }

}

?>