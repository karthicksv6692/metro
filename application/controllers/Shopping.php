<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\Query\ResultSetMapping;

require_once 'ssp.customized.class.php';
class Shopping extends CI_Controller {

	public function __construct(){
		parent::__construct();
		// is_user_logged_in();
	}
	public function index(){
    // $main_folder_name='purchase';$sub_folder_name='shopping';$file_name='Packages_view.php';
		$main_folder_name='purchase';$sub_folder_name='shopping';$file_name='selection_view.php';
    $utill_obj = new Common_utill();
    $data['packages'] = $utill_obj->get_all_packages();
    // print_r($data['packages']);exit();
		$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
	}
  public function get_packages(){
    $main_folder_name='purchase';$sub_folder_name='shopping';$file_name='Packages_view.php';
    $utill_obj = new Common_utill();
    $data['packages'] = $utill_obj->get_all_packages();
    $this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
  }
	public function get_product(){
		$main_folder_name='purchase';$sub_folder_name='shopping';
    $file_name='New_products_view.php';
    // $file_name='Products_view.php';
    $file_name1='Packages_view.php';
		$utill_obj = new Common_utill();
    $data['packages'] = $utill_obj->get_all_packages();
		if($this->input->post('package_id') != null){

      $request_for = $this->input->post('request_for');
      $package_id = $this->input->post('package_id');

      $gen_data = array( 
         'request_for'          =>   $request_for, 
         'package_id'           =>   $package_id, 
      );
      $this->session->set_userdata('pin_generate',$gen_data);

			$this->form_validation->set_rules('package_id', 'Package','trim|required|min_length[1]|max_length[11]|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed','find_numbers'=>'Numbers Required'));
			if ($this->form_validation->run() != false){
				$data['products'] = $utill_obj->get_package_products_by_packages_id($this->input->post('package_id'));

        // print_r($data['products']);exit();

        $data['categories_list'] = $utill_obj->get_category_sub_category();
        $data['pck_id'] = $package_id;
        // print_r($data['categories_list']);exit();

        $this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}else{
				$this->session->set_flashdata('error', '<p>Error in Fields.</p>');
				$this->view_call($main_folder_name,$sub_folder_name,$file_name);
			}
		}else{
			$this->index();
		}
	}
	public function cart(){
		$main_folder_name='purchase';$sub_folder_name='shopping';$file_name='Cart.php';
		$this->view_call($main_folder_name,$sub_folder_name,$file_name);
	}
	public function s_view(){
    $em = $this->doctrine->em;
		$main_folder_name='purchase';$sub_folder_name='shopping';$file_name='S_view.php';$file_name1='Packages_view.php';
		$utill_obj = new Common_utill();
		if($this->input->post('product_id') != null){
			$s_prd_obj = $utill_obj->get_package_and_product_object_form_product_id($this->input->post('product_id'));
			if($s_prd_obj != 0){
        $data['attach'] = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $this->input->post('product_id'),'attachment_referer_type'=>4));
        // print_r($data['attach']);exit();
				$data['obj'] = $s_prd_obj;
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}else{
				$this->index();
			}
		}else{
			$this->index();
		}
	}
	public function go_to_payapl(){
    $main_folder_name='purchase';$sub_folder_name='shopping';$file_name='Paynow.php';
    if(isset($_SESSION['logged_in'])){
      if($_SESSION['logged_in'] == 1){
        $ct = 0;
        if( count($_SESSION['cart_items']) > 0){
          $ct = count($_SESSION['cart_items'])-1;
        }
        if(isset($_SESSION['cart_items'][$ct])){
          $tot_pv = $_SESSION['cart_items'][$ct]['cart_tot_pv'];
        }else{$tot_pv = 0;}
        if($tot_pv >= 100){
          $this->view_call($main_folder_name,$sub_folder_name,$file_name);
          // $paypal_email   =   'svkarthicksv@gmail.com';
          // $return_url   =   'http://localhost/metro/index.php/shopping/paypal_success';
          // $cancel_url   =   'http://localhost/metro/index.php/shopping/paypal_cancel';
          // $notify_url   =   'http://domain.com/payments.php';
          // $item_name    =   'Test Item';
          // $item_amount  =   1;
          // $querystring  =   '';
          // $querystring   .=   "?business=".urlencode($paypal_email)."&";
          // $querystring   .=   "item_name=".urlencode($item_name)."&";
          // $querystring   .=   "amount=".urlencode($item_amount)."&";
          // foreach($_POST as $key => $value){
          //   $value      = urlencode(stripslashes($value));
          //   $querystring .= "$key=$value&";
          // }
          // $querystring  .=  "return=".urlencode(stripslashes($return_url))."&";
          // $querystring  .=  "cancel_return=".urlencode(stripslashes($cancel_url))."&";
          // $querystring  .=  "notify_url=".urlencode($notify_url);
          // header('location:https://www.sandbox.paypal.com/cgi-bin/webscr'.$querystring);
        }else{
          $main_folder_name='purchase';$sub_folder_name='shopping';$file_name='Cart.php';
          $this->session->set_flashdata('error', '<p>Minimum 100 PV Required</p>');
          $this->view_call($main_folder_name,$sub_folder_name,$file_name);
        }
      }else{
        $this->do_login();
      }
    }else{
      $this->do_login();
    }
	}
  public function shop_pin_request(){
    $em = $this->doctrine->em;
    $utill_obj = new Common_utill();
    $main_folder_name='purchase';$sub_folder_name='shopping';$file_name='Paynow.php';
    if(isset($_SESSION['logged_in'])){
      if($_SESSION['logged_in'] == 1){
        if($this->input->post('request_for') != null && $this->input->post('payment_mode') != null ){
          $this->form_validation->set_rules('request_for', 'Request For','trim|required|regex_match[/^[0-9]+$/]',array('required'=> 'Requierd'));
          $this->form_validation->set_rules('payment_mode', 'Payment Mode','trim|required|regex_match[/^[0-9]+$/]',array('required'=> 'Requierd'));
          //$this->form_validation->set_rules('request_to', 'Request To','trim|required|regex_match[/^[0-9]+$/]',array('required'=> 'Requierd'));
          if($this->form_validation->run() != false){
            $cart_ar = $_SESSION['cart_items'];
            $tot_pv = 0;
            $tot_price = 0;
            $prd_ar = array();
            foreach ($cart_ar as $cc) {
              $tot_pv += $cc['user_pv'];
              $tot_price += $cc['user_quantity']*$cc['product_display_price'];
              // $tot_price += $cc['user_quantity']*$cc['product_display_price'];
              // $p = array(
              //   'product_id' => $cc['product_id'],
              //   'product_code'=> $cc['product_code'],
              //   'product_name'=> $cc['product_name'],
              //   'product_display_price'=> $cc['product_display_price'],
              //   'point_value'=> $cc['point_value'],
              //   'quantity'=> $cc['user_quantity'],
              //   'image_utl'=> $cc['product_image_url'],
              // );
              // array_push($prd_ar, $p);
            }
            if($tot_pv >= 100){


              if($this->input->post('payment_mode') == 4){
                $bal_obj = $em->getRepository('Entity\User_account')->findOneBy(array('user_id' => $_SESSION['user_id']));
                if($bal_obj != null){
                  $bal = $bal_obj->getUserAccountRepurchaseAmount();
                  if($bal >= $tot_price){
                    $amt  = $bal-$tot_price;
                    $bal_obj->setUserAccountRepurchaseAmount($amt);
                    $em->persist($bal_obj);
                    $em->flush();
                    $login_user_obj = $utill_obj->get_single_row_using_primary_key('User',$_SESSION['user_id']);
                    $pin_used_user_id = 0;
                    $pin_status = 1;
                    $pin_validity_time = null;
                    $pin_remarks = null;
                    $pin_used_time = null;
                    $pck_id=0;
                    if(isset($_SESSION['pin_generate'])){
                      if(isset($_SESSION['pin_generate']['package_id'])){
                        $request_package_id = $em->getReference('Entity\Packages',$_SESSION['pin_generate']['package_id']);
                        $pck_id = $_SESSION['pin_generate']['package_id'];
                      }else{
                        if($tot_pv >=100 && $tot_pv < 200){
                          $pck_id = RT::$_100bvi_ID;
                        }elseif($tot_pv >=200 && $tot_pv < 300){
                          $pck_id = RT::$_200bvi_ID;
                        }elseif($tot_pv >=300 && $tot_pv < 400){
                          $pck_id = RT::$_300bvi_ID;
                        }elseif($tot_pv >=400 && $tot_pv < 500){
                          $pck_id = RT::$_400bvi_ID;
                        }elseif($tot_pv >500){
                          $pck_id = RT::$_500bvi_ID;
                        }else{ $pck_id = RT::$_500bvi_ID;}
                        $request_package_id = $em->getReference('Entity\Packages',$pck_id);
                      }   
                    }else{
                      if($tot_pv >=100 && $tot_pv < 200){
                        $pck_id = RT::$_100bvi_ID;
                      }elseif($tot_pv >=200 && $tot_pv < 300){
                        $pck_id = RT::$_200bvi_ID;
                      }elseif($tot_pv >=300 && $tot_pv < 400){
                        $pck_id = RT::$_300bvi_ID;
                      }elseif($tot_pv >=400 && $tot_pv < 500){
                        $pck_id = RT::$_400bvi_ID;
                      }elseif($tot_pv >500){
                        $pck_id = RT::$_500bvi_ID;
                      }else{ $pck_id = RT::$_500bvi_ID; }
                      $request_package_id = $em->getReference('Entity\Packages',$pck_id);
                    }
                    $query = $em->createQuery("SELECT E.package_product_id FROM  Entity\Package_products as E WHERE E.package_id = $pck_id AND E.package_product_price <= $tot_price" );
                    $pp_obj = $query->getResult();
                    if($pp_obj != null){
                      $pin_request_package_product_id = $utill_obj->get_reference_obj('Package_products',$pp_obj[0]);
                      $login_user_obj = $utill_obj->get_single_row_using_primary_key('User',$_SESSION['user_id']);
                    }else{
                      $pin_request_package_product_id = null;
                    }

                    $pin = new Entity\Pins;
                    $pin->setPinNo($pin_no = $utill_obj->get_pin_number());
                    $pin->setPinRequestedUserId($login_user_obj);
                    $pin->setPinUsedUserId($pin_used_user_id);
                    $pin->setPinStatus($pin_status);
                    $pin->setPinValidityTime($pin_validity_time);
                    $pin->setPinRemarks($pin_remarks);
                    $pin->setPinUsedTime($pin_used_time);
                    $pin->setPinRequestPackageId($request_package_id);
                    $pin->setPinRequestPackageProductId($pin_request_package_product_id);
                    $pin->setPinRequestFor($this->input->post('request_for'));
                    $pin->setPinTransferUserId(0);
                    $pin->setPinPaymentType(1);
                    $pin->onPrePersist();
                    $em->persist($pin);
                    $em->flush();

                    if($this->input->post('request_for') == 1){
                      $req = 'Repurchase';
                    }else{
                      $req = 'Registration';
                    }

                    $html = '<!DOCTYPE html> <html> <head> 
                                <style> 
                                  table {
                                      font-family: arial, sans-serif;
                                      border-collapse: collapse;
                                      width: 100%;
                                  }
                                  td, th {
                                      border: 1px solid #dddddd;
                                      text-align: left;
                                      padding: 8px;
                                  }

                                  tr:nth-child(even) {
                                      background-color: #dddddd;
                                  }
                                  </style>
                                  </head>
                                  <body>
                                    <table>
                                      <tr>
                                        <th>User Name</th>
                                        <th>Pin No</th>
                                        <th>Package Name</th>
                                        <th>Package Price</th>
                                        <th>For</th>
                                        <th>Date</th>
                                      </tr>';
                                      
                                      $html .= '<tr><td>'.$login_user_obj->getUserName().'</td><td>'.$pin->getPinNo().'</td><td>'.$request_package_id->getPackageName().'</td><td>'.$pin_request_package_product_id->getPackageProductDisplayPrice().'</td><td>'.$req.'</td><td>'.$pin->getCreatedAt()->format('Y-m-d H:i:s').'</td></tr>';
                                      
                                      $html .= '</table> </body> </html>';
                    $to = "svkarthicksv@gmail.com";
                    $subject = "Pin Generate Report";
                    $txt = $html;
                    $headers  = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    $headers .= "From: webmaster@example.com";

                    mail($to,$subject,$txt,$headers);

                    $this->session->unset_userdata('cart_items');
                    $this->session->unset_userdata('pin_generate');
                    $this->session->set_flashdata('success', '<p>Pin Created Successfully</p>');
                    redirect('index.php/login/dashboard','refresh');
                  }else{
                    $this->session->set_flashdata('error', '<p>Insufficient Repurchase Balance</p>');
                    $this->view_call($main_folder_name,$sub_folder_name,$file_name);
                  }
                }else{
                  $this->session->set_flashdata('error', '<p>Repurchase Balance Not Exists</p>');
                  $this->view_call($main_folder_name,$sub_folder_name,$file_name);
                }
                
              }else{
                if($this->input->post('request_to') != null){
                  $request_user_id = $em->getReference('Entity\User',$_SESSION['user_id']);
                  if(isset($_SESSION['pin_generate'])){
                    if(isset($_SESSION['pin_generate']['package_id'])){
                      $request_package_id = $em->getReference('Entity\Packages',$_SESSION['pin_generate']['package_id']);
                    }else{$request_package_id = null;}   
                  }else{$request_package_id = null;}
                  $request_type = $this->input->post('request_for'); //0-REG  1-REP
                  $no_of_pin = 1;
                  $request_status = 0;
                  $payment_mode = $this->input->post('payment_mode');
                  $product_details = serialize($_SESSION['cart_items']);
                  $notes = '';

                  $obj = new Entity\Shopping_pin_requests();
                  $obj->setRequestUserId($request_user_id);
                  if($request_package_id != null){
                    $obj->setRequestPackageId($request_package_id);
                  }
                  $obj->setRequestType($request_type);
                  $obj->setRequestedTo($em->getReference('Entity\User',$this->input->post('request_to')));
                  $obj->setNoOfPin($no_of_pin);
                  $obj->setRequestStatus($request_status);
                  $obj->setProductDetails($product_details);
                  $obj->setPaymentMode($payment_mode);
                  $obj->setNotes($notes);
                  $obj->onPrePersist();
                  $em->persist($obj);
                  $em->flush();
                  $this->session->unset_userdata('cart_items');
                  $this->session->unset_userdata('pin_generate');
                  $this->session->set_flashdata('success', '<p>Pin Requsted Successfully</p>');
                  redirect('index.php/login/dashboard','refresh');
                }else{
                  $this->session->set_flashdata('error', '<p>Request To is Required</p>');
                  $this->view_call($main_folder_name,$sub_folder_name,$file_name);
                }
              }
            }else{
              $this->session->set_flashdata('error', '<p>Purchase Point Value must 100pv</p>');
              $this->view_call($main_folder_name,$sub_folder_name,$file_name);
            }
          }else{
            $this->session->set_flashdata('error', '<p>Error in Fields</p>');
            $this->view_call($main_folder_name,$sub_folder_name,$file_name);
          }
        }else{$this->go_to_payapl();}
      }else{ $this->do_login(); }
    }else{ $this->do_login(); }
  }
  public function do_login(){ 
    $utill_obj = new Common_utill();
    if($this->input->post('user_name') != null && $this->input->post('password') != null){
      $this->form_validation->set_rules('user_name', 'User Name',array('trim','required','min_length[1]'),array('required'=> 'Please fill %s .'));
      $this->form_validation->set_rules('password', 'Password',array('trim','required','min_length[3]'),array('required'=> 'Please fill %s .'));
      if ($this->form_validation->run() !== false){
        $em = $this->doctrine->em;
        $user_name = $this->input->post('user_name');
        $password = $this->input->post('password');
        
        $login = $utill_obj->login_check($user_name,$password);
        if($login == 1){
          $this->cart();           
        }elseif($login == -3){
          $this->session->set_flashdata('error', '<p>Enter Valid UserName/Password</p>');
              redirect('index.php/shopping/do_login','refresh');
        }elseif($login == -2){
          $this->session->set_flashdata('error', '<p>User not Exists</p>');
              redirect('index.php/shopping/do_login','refresh');
        }elseif($login == -1){
          $this->session->set_flashdata('error', '<p>Enter Valid Password</p>');
              redirect('index.php/shopping/do_login','refresh');
        }elseif($login == 0){
          $this->session->set_flashdata('error', '<p>User Not Activated</p>');
              redirect('index.php/shopping/do_login','refresh');
        }else{
          $this->session->set_flashdata('error', '<p>Login Failed</p>');
              redirect('index.php/shopping/do_login','refresh');
        }
      }else{
        $this->session->set_flashdata('error', '<p>Enter Valid Login Details</p>');
            redirect('index.php/shopping/do_login','refresh');
      }
    }else{ 
      $this->load->view('includes/header');
      $this->load->view('login/Shopping_login');
      $this->load->view('includes/footer');
    }
  }
	public function paypal_success(){
    $gen_ar = $this->session->userdata('pin_generate');
		$main_folder_name='purchase';$sub_folder_name='shopping';$file_name='Success.php';
		$utill_obj = new Common_utill();
		$user_obj 			  = $utill_obj->get_reference_obj('User',$_SESSION['user_id']);
		$package_obj 		  = $utill_obj->get_reference_obj('Packages',$gen_ar['package_id']);
		$package_product_obj  = $utill_obj->get_reference_obj('Package_products',1);

		$pin_request_user_id   				=   $user_obj;
		$pin_request_package_id            	=   $package_obj;
		$pin_request_package_product_id    	=   $package_product_obj;
		$pin_req_quantity      				=   1;
		$pin_request_for       				=   $gen_ar['request_for'];
		$pin_used_user_id   				=   0;
		$pin_status   						=   1;
		$pin_validity_time   				=   new \DateTime( Date('y-m-d h:i:s', strtotime("+5 days")));
		$pin_used_time   					=   new \DateTime("0000-00-00 00:00:00");
		$pin_remarks   						=   "";

		$pin_no	 =	$utill_obj->get_pin_number();

		$pin_obj = $utill_obj->add_pin($pin_no,$pin_request_user_id,$pin_used_user_id,$pin_status,$pin_validity_time,$pin_remarks,$pin_used_time,$pin_request_package_id,$pin_request_package_product_id,$pin_request_for);
		if($pin_obj != null){
			$this->session->unset_userdata('cart_items');
			$cart_items = array();
			$this->session->set_userdata('cart_items',$cart_items);

			$data['msg'] = 'Your payment is Success';
			$data['pin'] = $pin_obj;
			$this->session->set_flashdata('success', '<p>Pin Created Sussfully</p>');
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);

			echo "Pin Created Successfully. Your Pin is:".$pin_obj;
			// redirect('index.php/registration/','refresh');
		}
	}
	public function paypal_cancel(){
		echo"Hai Your payment is Cancelled. Low Balance";
	}
	public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
		$utill_obj = new Common_utill();
		
		$this->load->view('includes/header');
		$this->load->view('includes/Web_page_header');
		$this->load->view($main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
		$this->load->view('includes/footer');
		$this->load->view('includes/Web_page_footer');
	}
  public function get_user_choice_products(){
    $main_folder_name='purchase';$sub_folder_name='shopping';
    $file_name='New_products_view_user_choice.php';
    // $file_name='Products_view.php';
    $file_name1='Packages_view.php';
    $utill_obj = new Common_utill();
    $data['packages'] = $utill_obj->get_all_packages();
    
    $data['products'] = $utill_obj->get_user_choice_products();

    // print_r($data['products']);exit();

    $data['categories_list'] = $utill_obj->get_category_sub_category();
    // print_r($data['categories_list']);exit();

    $this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
  }
  public function shop_pin_requests(){
    $main_folder_name='purchase';$sub_folder_name='shopping';$file_name='Pin_req';
    if($_SESSION['user_id'] == 2){
      $this->load->view('header');
      $this->load->view('purchase/shopping/Pin_req');
      $this->load->view('footer');
    }else{
      $this->load->view('header');
      $this->load->view('purchase/shopping/User_pin_req');
      $this->load->view('footer');
    }
  }
  public function datatable_pin_requests(){
    $con = '';
    if(isset($_GET['all'])){
      $con = '';
    }elseif(isset($_GET['dt1']) &&isset($_GET['dt2']) ){
      if($_GET['dt1'] != null && $_GET['dt2'] != null){
        $d1 = $_GET['dt1'].' 00:00:00';
        $d2 = $_GET['dt2'].' 23:59:59';
        $con = ' AND `spr`.`created_at` between "'.str_replace("/","-",$d1).'" AND "'.str_replace("/","-",$d2).'"';
      }elseif($_GET['dt1'] != null && $_GET['dt2'] == null){
        $con = ' AND `spr`.`created_at` LIKE "'.str_replace("/","-",$_GET['dt1']).' %"';
      }elseif($_GET['dt1'] == null && $_GET['dt2'] != null){
        $con = ' AND `spr`.`created_at` LIKE "'.str_replace("/","-",$_GET['dt2']).' %"';
      }else{
        $d1 = date('Y-m-d');
        $con = ' AND `spr`.`created_at` LIKE "'.str_replace("/","-",$d1).'%"';
      }
    }else{
      $d1 = date('Y-m-d');
      $con = ' AND `spr`.`created_at` LIKE "'.str_replace("/","-",$d1).'%"';
    }
    
    $CI = &get_instance();
    $CI->load->database();
    $sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
    $tot_tds = 0;
    $table = 'Shopping_pin_requests';
    $primaryKey = 'request_id';
    $columns = array(
      array( 'db' => '`spr`.`request_id`', 'dt' => 0,'field' => 'request_id'),
      array( 'db' => '`us`.`user_id`', 'dt' => 1,'field' => 'user_id'),
      array( 'db' => '`us`.`user_name`', 'dt' => 2,'field' => 'user_name'),
      array( 'db' => '`us`.`first_name`', 'dt' => 3, 'field' => 'first_name',
       'formatter' => function( $d, $row ) {
            return $row['first_name'].' '.$row['last_name'];
       }
      ),
      array( 'db' => '`spr`.`request_package_id`', 'dt' => 4, 'field' => 'request_package_id',
       'formatter' => function( $d, $row ) {
          if($row['request_package_id'] != null){
            $utill_obj = new Common_utill();
            $pack = $utill_obj->get_single_row_using_primary_key('Packages',$row['request_package_id']);
            if($pack != null){
              return $pack->getPackageName();
            }
          }else{return "User Chioce Product";}
       }
      ),
      array( 'db' => '`spr`.`request_type`', 'dt' => 5, 'field' => 'request_type',
       'formatter' => function( $d, $row ) {
            if($row['request_type'] == 0){
              return "Registration";
            }else{ return "Repurchase";}
       }
      ),
      array( 'db' => '`spr`.`request_status`', 'dt' => 6, 'field' => 'request_status',
       'formatter' => function( $d, $row ) {
            if($row['request_status'] == 0){
              return "<span class='fai'>Requested</span>";
            }elseif($row['request_status'] == 1){
              return "<span class='suc'>Trasfered</span>";
            }elseif($row['request_status'] == 2){
              return "<span class='war'>Pending</span>";
            }elseif($row['request_status'] == 3){
              return "<span class='fai'>Deleted</span>";
            }else{return"---";}
       }
      ),
      array( 'db' => '`spr`.`created_at`', 'dt' => 7,'field' => 'created_at'),
      array( 'db' => '`spr`.`payment_mode`', 'dt' => 8, 'field' => 'payment_mode',
       'formatter' => function( $d, $row ) {
            if($row['payment_mode'] == 0){
              return "<span class='blu'>Cash</span>";
            }elseif($row['payment_mode'] == 1){
              return "<span class='blu'>Check</span>";
            }elseif($row['payment_mode'] == 2){
              return "<span class='blu'>DD</span>";
            }elseif($row['payment_mode'] == 3){
              return "<span class='blu'>Online</span>";
            }elseif($row['payment_mode'] == 4){
              return "<span class='blu'>Rep.Wallet</span>";
            }elseif($row['payment_mode'] == 5){
              return "<span class='blu'>Cash</span>";
            }elseif($row['payment_mode'] == 6){
              return "<span class='blu'>NEFT</span>";
            }else{return"---";}
       }
      ),
      array( 'db' => '`spr`.`request_id`',
       'dt' => 9,
       'field' => 'request_id',
       'formatter' => function( $d, $row ) {
          return '<div class="col-md-6"><i class="fa fa-search-plus float_left" aria-hidden="true" onclick="get_req_details('.$row['request_id'].')"></i></div>
                  <div class="col-md-6"><input type="button" onclick="view_bill('.$row['request_id'].')" value="Bill"></div>';
        }
      ),
      array( 'db' => '`us`.`last_name`', 'dt' => 10,'field' => 'last_name'),
    );
    $joinQuery = "FROM `Shopping_pin_requests` AS `spr`  INNER JOIN  `User` as us ON (`us`.`user_id` = `spr`.`request_user_id`)";
    $where = " 1=1 $con AND`spr`.`requested_to` = ".$_SESSION['user_id'];

    $result = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery,$where);
    $start=$_REQUEST['start']+1;
    $idx=0;
    foreach($result['data'] as &$res){
        $res[0]=(string)$start;
        $start++;
        $idx++;
    }
    echo json_encode($result);
  }
}
?>