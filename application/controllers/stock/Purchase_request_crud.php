<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\Query\ResultSetMapping;
require_once 'ssp.customized.class.php';
class Purchase_request_crud extends CI_Controller {

	public function __construct(){
		parent::__construct();
		is_user_logged_in();
	}
	public function index(){ 
		$this->view();
	}
	public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
		$this->load->view('header');
		$this->load->view(''.$main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
		$this->load->view('footer');
	}
	public function view(){
		$this->load->view('header');
		$this->load->view('stock/purchase_request/View');
		$this->load->view('footer');	
	}
	public function datatable_ajax(){
		$CI = &get_instance();
		$CI->load->database();
		$sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);

		$table = 'Purchase_request';
		$primaryKey = 'purchase_request_id';
		$columns = array(
		    array( 'db' => '`usr`.`vendor_name`', 'dt' => 0,'field' => 'vendor_name'),
		    array( 'db' => '`u`.`purchase_request_no`', 'dt' => 1,'field' => 'purchase_request_no'),
		    array( 'db' => '`u`.`created_at`', 'dt' => 2,'field' => 'created_at'),
		    array( 'db' => '`u`.`purchase_request_status`',  'dt' => 3 ,'field' => 'purchase_request_status','formatter' => function( $d, $row ){
				    	if($row['purchase_request_status'] == 0){
				    		return "Requested";
				    	}elseif($row['purchase_request_status'] == 1){
				    		return "Approved";
				    	}else{return "Rejected";}
				    }),
		    array( 'db' => '`u`.`purchase_request_id`',
			       'dt' => 4,
			       'field' => 'purchase_request_id',
			       'formatter' => function( $d, $row ) {
		           	$utill_obj = new Common_utill();
							// $edit = $utill_obj->has_access('pincode','edit');
							// $delete = $utill_obj->has_access('pincode','delete');
							// $s_view = $utill_obj->has_access('pincode','s_view');
							$edit = "asd";
							$delete = "asd";
							$s_view = "asd";

		            if($s_view != null){
		        		$ss = '<form method="POST" class="icon_edit_form" action="'.base_url().'index.php/stock/purchase_request_crud/S_view">
                            <input type="hidden" name="purchase_request_id" value="'.$d.'">
                            <i class="fa fa-search-plus float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
		        	}else{$ss = '';}

		        	if($edit != null){
		        		$ee = ' <form method="POST" class="icon_edit_form" action="'.base_url().'index.php/stock/purchase_request_crud/Edit">
                            <input type="hidden" name="purchase_request_id" value="'.$d.'">
                            <i class="fa fa-pencil float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
		        	}else{$ee='';}

		        	if($delete != null){
		        		$dd = ' <form method="POST" class="icon_edit_form" action="'.base_url().'index.php/stock/purchase_request_crud/Delete">
                            <input type="hidden" name="purchase_request_id" value="'.$d.'">
                            <i class="fa fa-trash-o" aria-hidden="true" onclick="$(this).closest(\'form\').submit();" ></i>
                        </form>';
		        		//$dd = '<i class="fa fa-trash-o float_left btn_delete but_del" aria-hidden="true" data-value="'.$d.'" ></i>';
		        	}else{$dd='';}

		        	return $ss.$ee.$dd;
	        	}
		    )
		);
		//$joinQuery = "FROM `Purchase_request` AS `u`  INNER JOIN `Purchase_request_products` AS `ps` ON (`ps`.`purchase_request_id` = `u`.`purchase_request_id`) INNER JOIN  `Vendor` AS `usr` ON ( `u`.`purchase_request_vendor_id` = `usr`.`vendor_id` )";
		$joinQuery = "FROM `Purchase_request` AS `u`  INNER JOIN  `Vendor` AS `usr` ON ( `u`.`purchase_request_vendor_id` = `usr`.`vendor_id` )";
		 
		echo json_encode(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery));
	}
	public function add(){//print_r($_POST);exit();
		$utill_obj = new Common_utill();
		$em = $this->doctrine->em;
		$main_folder_name = "stock";$sub_folder_name = "purchase_request";$file_name = "Add";

		$data['vendor_list'] = $em->getRepository("Entity\Vendor")->findAll();
		$data['product_list'] = $em->getRepository('Entity\Product')->findBy(array('product_is_visible' => 1));

		if($this->input->post('vendor_id') != null || $this->input->post('ad_pr') != null){ 
			
			$this->form_validation->set_rules('vendor_id', 'User','trim|required|min_length[1]|regex_match[/^[0-9]+$/]',array('required'=> 'Requierd','min_length' => 'needs Minimum 5 charecters'));
			//$this->form_validation->set_rules('estimate_date', 'User','trim|required|min_length[1]|regex_match[/^[0-9]+$/]',array('required'=> 'Requierd','min_length' => 'needs Minimum 5 charecters'));
			if(isset($_POST['prd'])){
				if (is_array($_POST['prd'])) {
				    foreach ($_POST['prd'] as $key => $value) {
				       $this->form_validation->set_rules('prd['.$key.']', 'Product','trim|required|required|regex_match[/^[0-9\-]+$/]',array('required'=> 'Required','regex_match' => 'Numbers Only Allowed','find_numbers'=>'Quantity is Numeric'));
				    }
				}
			}
			if(isset($_POST['quantity'])){
				if (is_array($_POST['quantity'])) {
				    foreach ($_POST['quantity'] as $key => $value) {
				       $this->form_validation->set_rules('quantity['.$key.']', 'Quantity','trim|required|required|regex_match[/^[0-9\-]+$/]',array('required'=> 'Required','regex_match' => 'Numbers Only Allowed','find_numbers'=>'Quantity is Numeric'));
				    }
				}
			}
			if ($this->form_validation->run() != false){//
				$em->getConnection()->beginTransaction();
				$vendor_id = $this->input->post('vendor_id');
				
				$vendor_obj = $utill_obj->get_reference_obj('Vendor',$vendor_id);
				$purchase_request_number = $this->input->post('gj_bill_no');
				$date = $this->input->post('estimate_date');
				$request_status = 0;
				$type = 1;
				$bill_or_req_id =  $this->input->post('gj_bill_no');
				$approve_id =  0;
				$reson =  "";

				if($vendor_obj != null){

					$pur_obj = $utill_obj->add_purchase_request($vendor_obj,$purchase_request_number,$date,$request_status,$type,$bill_or_req_id,$approve_id,$reson);
					if($pur_obj != null){


						$len = count($this->input->post('prd'));
						for($i=0;$i<$len;){
							$quantity = $this->input->post('quantity')[$i];
							$product_obj = $utill_obj->get_single_row_using_primary_key('Product',$this->input->post('prd')[$i]);
							if($product_obj != null){
								$req_obj = $utill_obj->add_purchase_request_products($product_obj,$quantity,$pur_obj,$product_obj->getProductPrice());
								if($req_obj != null){
									if($i == $len-1){
										$em->flush();
										echo $this->input->post('gj_bill_no');
										echo"<br><br>";
										$del_bill_no = $utill_obj->del_last_record($this->input->post('gj_bill_no'));
										if($del_bill_no == 1){
											$em->getConnection()->commit();
											$this->session->set_flashdata('success', '<p>Request Created Successfully</p>');
											redirect('index.php/stock/purchase_request_crud','refresh');
										}else{
											$em->getConnection()->rollBack();
											$this->session->set_flashdata('error', '<p>Bil Number Delete Failed</p>');
											$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
										}
									}
									$i++;
								}else{
									$em->getConnection()->rollBack();
									$this->session->set_flashdata('error', '<p>Failed</p>');
									$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
								}
							}

						}
					}
				}else{
					$em->getConnection()->rollBack();
					$this->session->set_flashdata('error', '<p>Vendor Not Exists</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}

			}else{ 
				$this->session->set_flashdata('error', '<p>Error in Fields</p>');
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}
		}else{$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);}
	}
	public function edit(){
		$utill_obj = new Common_utill();
		$em = $this->doctrine->em;
		$main_folder_name = "stock";$sub_folder_name = "purchase_request";$file_name = "Edit";
		$data['search'] = $utill_obj->search_field_two_column('Purchase_request', 'getPurchaseRequestId', 'getPurchaseRequestVendorId','getVendorName', 'stock/Purchase_request_crud', 'Edit', 'purchase_request_id');
		$data['vendor_list'] = $em->getRepository("Entity\Vendor")->findAll();
		if($this->input->post('purchase_request_id') != null){

			$this->form_validation->set_rules('purchase_request_id', 'User','trim|required|min_length[1]|regex_match[/^[0-9]+$/]',array('required'=> 'Requierd','min_length' => 'needs Minimum 5 charecters'));
			if ($this->form_validation->run() != false){
				$purchase_request_id = $this->input->post('purchase_request_id');
				$purchase_req_obj = $utill_obj->get_reference_obj('Purchase_request',$purchase_request_id);
				$product_obj = $utill_obj->get_multiple_rows_using_array_value('purchase_request_products','purchase_request_id',$purchase_request_id);
				if($purchase_req_obj != null){
					$data['purchase_req_obj'] = $purchase_req_obj;
					$data['product_obj'] = $product_obj;
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}else{
					$this->session->set_flashdata('error', '<p>Account Not Exists</p>');
					redirect('account/transaction_crud','refresh');
				}
			}else{ redirect('index.php/stock/purchase_request_crud','refresh'); }

		}elseif($this->input->post('edit_purchase_request_id') != null){ //print_r($_POST);exit();
			if($this->input->post('status') != 0){
				$this->form_validation->set_rules('edit_purchase_request_id', 'User','trim|required|min_length[1]|regex_match[/^[0-9]+$/]',array('required'=> 'Requierd','min_length' => 'needs Minimum 5 charecters'));

				$this->form_validation->set_rules('status', 'User','trim|required|min_length[1]|regex_match[/^[0-9]+$/]',array('required'=> 'Requierd','min_length' => 'needs Minimum 5 charecters'));
				$this->form_validation->set_rules('received_date', 'User','trim|required',array('required'=> 'Requierd'));

				if(isset($_POST['prd'])){
					if (is_array($_POST['prd'])) {
					    foreach ($_POST['prd'] as $key => $value) {
					       $this->form_validation->set_rules('prd['.$key.']', 'Product','trim|required|required|regex_match[/^[0-9\-]+$/]',array('required'=> 'Required','regex_match' => 'Numbers Only Allowed','find_numbers'=>'Quantity is Numeric'));
					    }
					}
				}
				if(isset($_POST['quantity'])){
					if (is_array($_POST['quantity'])) {
					    foreach ($_POST['quantity'] as $key => $value) {
					       $this->form_validation->set_rules('quantity['.$key.']', 'Quantity','trim|required|required|regex_match[/^[0-9\-]+$/]',array('required'=> 'Required','regex_match' => 'Numbers Only Allowed','find_numbers'=>'Quantity is Numeric'));
					    }
					}
				}

				
				$purchase_request_id = $this->input->post('edit_purchase_request_id');
				$req_obj = $utill_obj->get_reference_obj('Purchase_request',$purchase_request_id);
				$product_obj = $utill_obj->get_multiple_rows_using_array_value('purchase_request_products','purchase_request_id',$purchase_request_id);
				$data['purchase_req_obj'] = $req_obj;
				$data['product_obj'] = $product_obj;
				if ($this->form_validation->run() != false){
					$em->getConnection()->beginTransaction();
					//print_r($_POST);exit();

					$purchase_request_id = $this->input->post('edit_purchase_request_id');
					$request_status = $this->input->post('status');
					$date = new DateTime($this->input->post('received_date'));

					$req_obj = $utill_obj->get_reference_obj('Purchase_request',$purchase_request_id);

					if($req_obj != null){
						$pur_obj = $utill_obj->update_purchase_request($purchase_request_id,$date,$request_status);
						
						if($pur_obj != null){

							if($product_obj != null){
								$del_obj = $utill_obj->delete_product_req_products($product_obj);
								if($del_obj != null){
									$len = count($this->input->post('prd'));
									for($i=0;$i<$len;){
										$quantity = $this->input->post('quantity')[$i];
										$ppq_price = $this->input->post('tot_price')[$i];
										$product_obj = $utill_obj->get_single_row_using_primary_key('Product',$this->input->post('prd')[$i]);
										if($product_obj != null){
											$req_obj = $utill_obj->add_purchase_request_products($product_obj,$quantity,$pur_obj,$ppq_price);
											if($req_obj != null){
												if($i == $len-1){
													$em->flush();
													$em->getConnection()->commit();
													$this->session->set_flashdata('success', '<p>Request Updated Successfully</p>');
													redirect('index.php/stock/purchase_request_crud','refresh');	
												}
												$i++;
											}else{
												$em->getConnection()->rollBack();
												$this->session->set_flashdata('error', '<p>Failed</p>');
												$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
											}
										}

									}
								}
							}

							$len = count($this->input->post('product_name'));
							for($i=0;$i<$len;){
							}


						}else{
							$em->getConnection()->rollBack();
							$this->session->set_flashdata('error', '<p>Update Failed</p>');
							$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
						}
						
					}else{
						$em->getConnection()->rollBack();
						$this->session->set_flashdata('error', '<p>Request Not Exists</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					}

				}else{
					$this->session->set_flashdata('error', '<p>Error in Fields</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}
			}else{
				$this->session->set_flashdata('error', '<p>Status Not Changed</p>');
				redirect('index.php/stock/purchase_request_crud','refresh');	
			}
		}else{

		}
	}
	public function s_view(){
		$utill_obj = new Common_utill();
		$em = $this->doctrine->em;
		$main_folder_name = "stock";$sub_folder_name = "purchase_request";$file_name = "S_view";
		$data['search'] = $utill_obj->search_field_two_column('Purchase_request', 'getPurchaseRequestId', 'getPurchaseRequestVendorId','getVendorName', 'stock/Purchase_request_crud', 'S_view', 'purchase_request_id');
		// $data['vendor_list'] = $em->getRepository("Entity\Vendor")->findAll();
		if($this->input->post('purchase_request_id') != null){

			$this->form_validation->set_rules('purchase_request_id', 'User','trim|required|min_length[1]|regex_match[/^[0-9]+$/]',array('required'=> 'Requierd','min_length' => 'needs Minimum 5 charecters'));
			if ($this->form_validation->run() != false){
				$purchase_request_id = $this->input->post('purchase_request_id');
				$purchase_req_obj = $utill_obj->get_reference_obj('Purchase_request',$purchase_request_id);
				$product_obj = $utill_obj->get_multiple_rows_using_array_value('purchase_request_products','purchase_request_id',$purchase_request_id);
				if($purchase_req_obj != null){
					$data['purchase_req_obj'] = $purchase_req_obj;
					$data['product_obj'] = $product_obj;
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}else{
					$this->session->set_flashdata('error', '<p>Account Not Exists</p>');
					redirect('account/transaction_crud','refresh');
				}
			}else{ redirect('index.php/stock/purchase_request_crud','refresh'); }

		}else{$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);}
	}
	public function delete(){
		$utill_obj = new Common_utill();
		$em = $this->doctrine->em;
		$main_folder_name = "stock";$sub_folder_name = "purchase_request";$file_name = "Delete";
		$data['search'] = $utill_obj->search_field_two_column('Purchase_request', 'getPurchaseRequestId', 'getPurchaseRequestVendorId','getVendorName', 'stock/Purchase_request_crud', 'Delete', 'purchase_request_id');
		// $data['vendor_list'] = $em->getRepository("Entity\Vendor")->findAll();
		if($this->input->post('purchase_request_id') != null){

			$this->form_validation->set_rules('purchase_request_id', 'User','trim|required|min_length[1]|regex_match[/^[0-9]+$/]',array('required'=> 'Requierd','min_length' => 'needs Minimum 5 charecters'));
			if ($this->form_validation->run() != false){
				$purchase_request_id = $this->input->post('purchase_request_id');
				$purchase_req_obj = $utill_obj->get_reference_obj('Purchase_request',$purchase_request_id);
				$product_obj = $utill_obj->get_multiple_rows_using_array_value('purchase_request_products','purchase_request_id',$purchase_request_id);
				if($purchase_req_obj != null){
					$data['purchase_req_obj'] = $purchase_req_obj;
					$data['product_obj'] = $product_obj;
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}else{
					$this->session->set_flashdata('error', '<p>Account Not Exists</p>');
					redirect('account/transaction_crud','refresh');
				}
			}else{ redirect('index.php/stock/purchase_request_crud','refresh'); }

		}elseif($this->input->post('delete_purchase_request_id') != null){
			// print_r($_POST);
			$this->form_validation->set_rules('delete_purchase_request_id', 'User','trim|required|min_length[1]|regex_match[/^[0-9]+$/]',array('required'=> 'Requierd','min_length' => 'needs Minimum 5 charecters'));
			if ($this->form_validation->run() != false){
				$em->getConnection()->beginTransaction();

				$purchase_request_id = $this->input->post('delete_purchase_request_id');
				$req_obj = $utill_obj->get_reference_obj('Purchase_request',$purchase_request_id);
				if($req_obj != null){
					$product_obj = $utill_obj->get_multiple_rows_using_array_value('purchase_request_products','purchase_request_id',$purchase_request_id);
					if($product_obj != null){
						$del_obj = $utill_obj->delete_product_req_products($product_obj);
						if($del_obj != null){
							$em->remove($req_obj);
							$em->flush();
							$em->getConnection()->commit();
							$this->session->set_flashdata('success', '<p>Request Deleted Successfully</p>');
							redirect('index.php/stock/purchase_request_crud','refresh');
						}
					}
				}else{
					$em->getConnection()->rollBack();
					$this->session->set_flashdata('error', '<p>Request Not Exists</p>');
					redirect('index.php/stock/purchase_request_crud','refresh');
				}
			}else{
				$em->getConnection()->rollBack();
				$this->session->set_flashdata('error', '<p>Error in Fields</p>');
				redirect('index.php/stock/purchase_request_crud','refresh');
			}
		}else{$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);}
	}

}
?>