<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\Query\ResultSetMapping;

require_once 'ssp.customized.class.php';
class Purchase extends CI_Controller {

  public function index(){
    $this->session->sess_destroy();
    $this->load->view('header');
    $this->load->view('footer');
  }
  public function enter_pin(){ 
    $em = $this->doctrine->em;
    $main_folder_name = 'purchase';$sub_folder_name = 'pin';$file_name = 'Pin_enter_page';
    $file_name2 = 'Shopping_page';
    $utill_obj = new Common_utill();
    if( $this->input->post('pin_no') != null || $this->input->post('pin_page') != null ){
      $position = $this->input->post('position');
      
      $this->form_validation->set_rules('pin_no', 'Pin Number ','trim|required',array('required'=> 'Required','min_length' => 'Enter Valid Pin'));
      if ($this->form_validation->run() != false){
        $user_id = $_SESSION['user_id'];
        $pin_no = $this->input->post('pin_no');
        $for = 1;
        $obj = $utill_obj->check_pin_get_details($pin_no,$for);

        if( !is_numeric($obj) && $obj != null){
          $package_id = $obj->getPinRequestPackageId()->getPackageId();
          $up_obj = $utill_obj->get_single_row_using_array_value('User_package_purchase','user_id',$user_id);

          if($position == 1){
            $res = $this->get_right_empty_node($up_obj->getUserPurchaseId());
          }else{
            $res = $this->get_left_empty_node($up_obj->getUserPurchaseId());
          }
          if(! is_numeric($res)){
            if($up_obj != null){
              $package_id = $up_obj->getUserPurchasePinId()->getPinRequestPackageId()->getPackageId();
              $user_obj = $up_obj->getUserId();
              $up_sp_up_id = $up_obj->getUserPurchaseId();
              $up_sp_gene_side = $position;
              $pin_id = $obj;
              $up_act_gene_up_id = $res->getUserPurchaseId();
              $pin_id = $obj->getPinId();
              $pin_used_user_id = $up_obj->getUserId()->getUserId();
              $pin_status = 0;
              $pin_used_time = new \DateTime(date("Y-m-d H:i:s"));
              if($res->getUserPurchaseGeneLeftPurchaseId() == 0){
                $up_act_gene_side = 0;
              }else{
                $up_act_gene_side = 1;
              }
              $up_gene_left_pid=0;$up_gene_right_pid=0;$up_money_alloted=0;$up_pair_up_id=0;$achieve=0;
              $up_transaction_obj = $utill_obj->get_reference_obj('Transactions', 1);
              $created_at = null;
              if($package_id == 18){
                 $rep_qry = $em->createQuery("SELECT p.pin_id FROM Entity\Pins as p  WHERE p.pin_request_for = 1 AND p.pin_status=0 AND p.pin_used_user_id =".$up_obj->getUserId()->getUserId());
                 $rep_res = $rep_qry->getResult();
                 if(count($rep_res) > 0){
                    $update_pin_obj = $utill_obj->update_pin($pin_id,$pin_used_user_id,$pin_status,$pin_used_time);
 
                    $rep_obj = new Entity\Repurchase();
                    $rep_obj->setRepurchaseUpId($up_obj);
                    $rep_obj->setRepurchaseSide($position);
                    $rep_obj->setRepurchasePinId($obj);
                    $rep_obj->setRepurchaseType(1);
                    $rep_obj->onPrePersist();
                    $em->persist($rep_obj);
 
                    $up_obj->setRepCount(1);
                    $em->persist($up_obj);
                    $em->flush();
                    if($update_pin_obj != null){
                      $this->session->set_flashdata('success', '<p>Your Repurchase Successfully</p>');
                      redirect('index.php/login/dashboard','refresh');
                    }else{
                      $this->session->set_flashdata('error', '<p>Pin Update Failed</p>');
                      $this->view_call($main_folder_name,$sub_folder_name,$file_name);
                    }
                 //    $up_status = $utill_obj->add_user_package_purchase($user_obj, $up_sp_up_id, $up_sp_gene_side, $pin_id, $up_act_gene_up_id, $up_act_gene_side, $up_gene_left_pid, $up_gene_right_pid, $up_money_alloted, $up_transaction_obj, $up_pair_up_id, $achieve, $created_at);
                 //    if ($up_status != null) {
                 //      $res = $utill_obj->payout_achieve();
                 //      $res = $utill_obj->payout_left_right_manage();
                 //      $this->session->set_flashdata('success', '<p>Your Repurchase Successfully</p>');
                 //      redirect('index.php/login/dashboard','refresh');
                 //    }else{
                 //      $this->session->set_flashdata('error', '<p>Repurchase Failed</p>');
                 //      $this->view_call($main_folder_name,$sub_folder_name,$file_name);
                 //    }
                 }else{
                    $update_pin_obj = $utill_obj->update_pin($pin_id,$pin_used_user_id,$pin_status,$pin_used_time);
                    $rep_obj = new Entity\Repurchase();
                    $rep_obj->setRepurchaseUpId($up_obj);
                    $rep_obj->setRepurchaseSide($position);
                    $rep_obj->setRepurchasePinId($obj);
                    $rep_obj->setRepurchaseType(0);
                    $rep_obj->onPrePersist();
                    $em->persist($rep_obj);
                    
                    $up_obj->setRepCount(1);
                    $em->persist($up_obj);
                    $em->flush();
                    if($update_pin_obj != null){
                      $this->session->set_flashdata('success', '<p>Your First Repurchase Successfully</p>');
                      redirect('index.php/login/dashboard','refresh');
                    }else{
                      $this->session->set_flashdata('error', '<p>Pin Update Failed</p>');
                      $this->view_call($main_folder_name,$sub_folder_name,$file_name);
                    }
                 //    $pin_id = $obj->getPinId();
                 //    $pin_used_user_id = $up_obj->getUserId()->getUserId();
                 //    $pin_status = 0;
                 //    $pin_used_time = new \DateTime(date("Y-m-d h:i:s"));
                 //    $update_pin_obj = $utill_obj->update_pin($pin_id,$pin_used_user_id,$pin_status,$pin_used_time);
                 //    if($update_pin_obj != null){
                 //      $this->session->set_flashdata('success', '<p>Your First Repurchase Successfully</p>');
                 //      redirect('index.php/login/dashboard','refresh');
                 //    }else{
                 //      $this->session->set_flashdata('error', '<p>Pin Update Failed</p>');
                 //      $this->view_call($main_folder_name,$sub_folder_name,$file_name);
                 //    }
                }
              }else{
                $rep_obj = new Entity\Repurchase();
                $rep_obj->setRepurchaseUpId($up_obj);
                $rep_obj->setRepurchaseSide($position);
                $rep_obj->setRepurchasePinId($obj);
                $rep_obj->setRepurchaseType(1);
                $rep_obj->onPrePersist();
                $em->persist($rep_obj);
                $update_pin_obj = $utill_obj->update_pin($pin_id,$pin_used_user_id,$pin_status,$pin_used_time);
                $up_obj->setRepCount(1);
                $em->persist($up_obj);
                $em->flush();
                if($update_pin_obj != null){
                  $this->session->set_flashdata('success', '<p>Your Repurchase Successfully</p>');
                  redirect('index.php/login/dashboard','refresh');
                }else{
                  $this->session->set_flashdata('error', '<p>Pin Update Failed</p>');
                  $this->view_call($main_folder_name,$sub_folder_name,$file_name);
                }
                // $up_status = $utill_obj->add_user_package_purchase($user_obj, $up_sp_up_id, $up_sp_gene_side, $pin_id, $up_act_gene_up_id, $up_act_gene_side, $up_gene_left_pid, $up_gene_right_pid, $up_money_alloted, $up_transaction_obj, $up_pair_up_id, $achieve, $created_at);
                // if ($up_status != null) {
                //   $res = $utill_obj->payout_achieve();
                //   $res = $utill_obj->payout_left_right_manage();
                //   $this->session->set_flashdata('success', '<p>Your First Repurchase Successfully</p>');
                //   redirect('index.php/login/dashboard','refresh');
                // }else{
                //   $this->session->set_flashdata('error', '<p>Repurchase Failed</p>');
                //   $this->view_call($main_folder_name,$sub_folder_name,$file_name);
                // }
              }
            }else{
              $this->session->set_flashdata('error', '<p>User Not Valid</p>');
              $this->view_call($main_folder_name,$sub_folder_name,$file_name);
            }
          }else{
            $this->session->set_flashdata('error', '<p>Invalid Repurchase Position</p>');
            $this->view_call($main_folder_name,$sub_folder_name,$file_name);
          }
        }elseif( $obj == -2 ){
          $this->session->set_flashdata('error', '<p>Pin is Locked.</p>');
          $this->view_call($main_folder_name,$sub_folder_name,$file_name);
        }elseif( $obj == -3 ){
          $this->session->set_flashdata('error', '<p>Pin is Already Used.</p>');
          $this->view_call($main_folder_name,$sub_folder_name,$file_name);
        }elseif( $obj == -4 ){
          $this->session->set_flashdata('error', '<p>Pin is Created for Registration Only.</p>');
          $this->view_call($main_folder_name,$sub_folder_name,$file_name);
        }else{
          $this->session->set_flashdata('error', '<p>Enter The Valid Pin</p>');
          $this->view_call($main_folder_name,$sub_folder_name,$file_name);
        }
      }else{
        $this->session->set_flashdata('error', '<p>Error in Fields</p>');
        $this->view_call($main_folder_name,$sub_folder_name,$file_name);
      }
    }else{ $this->view_call($main_folder_name,$sub_folder_name,$file_name); }
  }
  public function get_left_empty_node($id){
    $em = $this->doctrine->em;
    $utill_obj = new Common_utill();
    $res = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$id);
    if($res->getUserPurchaseGeneLeftPurchaseId() > 0){
      return $this->left_right_check($res->getUserPurchaseGeneLeftPurchaseId());
    }else{
      return $res;
    }
  }
  public function get_right_empty_node($id){
    $em = $this->doctrine->em;
    $utill_obj = new Common_utill();
    $res = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$id);
    if($res->getUserPurchaseGeneRightPurchaseId() > 0){
      // echo $res->getUserPurchaseGeneRightPurchaseId();
      return $this->left_right_check($res->getUserPurchaseGeneRightPurchaseId());
    }else{
      return $res;
    }
  }
  public function left_right_check($id){

    $utill_obj = new Common_utill();
    $res = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$id);
    if($res != null){
      if($res->getUserPurchaseGeneLeftPurchaseId() > 0 && $res->getUserPurchaseGeneRightPurchaseId() > 0){
        return $this->left_right_check($res->getUserPurchaseGeneLeftPurchaseId());
        return $this->left_right_check($res->getUserPurchaseGeneRightPurchaseId());
      }elseif($res->getUserPurchaseGeneLeftPurchaseId() > 0 && $res->getUserPurchaseGeneRightPurchaseId() == 0){
        return $utill_obj->get_single_row_using_primary_key('User_package_purchase',$id);
      }elseif($res->getUserPurchaseGeneLeftPurchaseId() == 0 && $res->getUserPurchaseGeneRightPurchaseId() > 0){
        return $utill_obj->get_single_row_using_primary_key('User_package_purchase',$id);
      }elseif($res->getUserPurchaseGeneLeftPurchaseId() == 0 && $res->getUserPurchaseGeneRightPurchaseId() == 0){
        return $utill_obj->get_single_row_using_primary_key('User_package_purchase',$id);
      }else{
        return $this->left_right_check($res->getUserPurchaseGeneLeftPurchaseId());
        return $this->left_right_check($res->getUserPurchaseGeneRightPurchaseId());
      }
    }else{
      return 0;
    }
  }

  public function test(){
    $utill_obj = new Common_utill();
    
      $paginator=$utill_obj->test();  
  }

  public function get_products_form_package_id($page_no = null,$max = null){
    $ar['products'] = array();
    $ar['pagination'] = array();
    if($page_no == null){
      $page_no = $this->input->post('page_no');
    }
    if($max == null){
      $max = $this->input->post('max');
    }
    $em = $this->doctrine->em;
    $main_folder_name = 'purchase';$sub_folder_name = 'pin';$file_name = 'Products_page';
    $utill_obj = new Common_utill();
    $data['products'] = array('asdf','dsaf');
    $query = 'select prd.product_id,prd.product_name,prd.product_price from Entity\Packages as p INNER JOIN Entity\Package_products as pp WITH p.package_id = pp.package_id INNER JOIN Entity\Package_product_items as ppt WITH ppt.package_product_id = pp.package_product_id INNER JOIN Entity\Product as prd WITH prd.product_id = ppt.package_product_item_product_id WHERE pp.package_id = 11';
  
    $qry = $em->createQuery($query)
               ->setFirstResult($max*$page_no-$max)
               ->setMaxResults($max);

       $count_query = 'select COUNT(prd.product_id) from Entity\Packages as p INNER JOIN Entity\Package_products as pp WITH p.package_id = pp.package_id INNER JOIN Entity\Package_product_items as ppt WITH ppt.package_product_id = pp.package_product_id INNER JOIN Entity\Product as prd WITH prd.product_id = ppt.package_product_item_product_id WHERE pp.package_id = 11';

      $cnt_qry = $em->createQuery($count_query);

      $cnt_qry_count = $cnt_qry->getResult()[0][1];

    if($this->input->post('page_no') != null){
      echo json_encode($qry->getResult());
    }else{
      $products = $qry->getResult();
      array_push($ar['products'],$products);
      array_push($ar['pagination'],$this->create_svk_pagination($products));
      return $ar;
    }
    
    
  }

  public function create_svk_pagination($products){ //print_r($products);exit();
    $first_id = $products[0]['product_id'];
    $last_id = $products[count($products)-1]['product_id'];

    $page  = '<ul class="full_prd_pag">';
    $page  .= '<li class="full_prd_first" onclick="get_page("'.$first_id.'")"><a>First</a></li>';

    // $page  .= '<li class="full_prd_ele active" onclick="get_page("'.$first_id.'")"><a>1</a></li>';
    // for($i=1;$i<count($products);$i++){
    //  $page  .= '<li class="full_prd_ele" onclick="get_page("'.$products[$i]['product_id'].'")"><a>'.($i+1).'</a></li>';
    // }
    // $page  .= '<li>.</li><li>.</li>';
    $page  .= '<li class="full_prd_last" onclick="get_page("'.$last_id.'")"><a>Last</a></li>';
    $page  .= '</ul>';

    return $page;
  }

  public function products(){
    $em = $this->doctrine->em;
    $main_folder_name = 'purchase';$sub_folder_name = 'pin';$file_name = 'Products_page';

    $data['products'] = $this->get_products_form_package_id();
    //$page = $this->get_products_form_package_id(1,2);
    
    if( $this->input->post('prd_ids') != null || $this->input->post('prd_page') != null ){
      if($this->input->post('prd_ids') != 0){
        $this->form_validation->set_rules('prd_ids', 'Products','trim|required',array('required'=> 'Select atleast one product'));
        if ($this->form_validation->run() != false){
          $prd_ids = $this->input->post('prd_ids');
          $prd_id_ar = explode(',', $prd_ids);
          //print_r($prd_id_ar);
          $this->product_details($prd_id_ar);
        }else{
          $this->session->set_flashdata('error', '<p>Select atleast one product</p>');
          $this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
        }
      }else{
        $this->session->set_flashdata('error', '<p>Select atleast one product</p>');
        $this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
      }
    }else{  $this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);  }
  }

  public function Product_details($prd_ids = null){
    //print_r($_POST);
    $main_folder_name = 'purchase';$sub_folder_name = 'pin';$file_name = 'Prd_detail_page';$file_name1 = 'Package_prd_detail_page';
    $utill_obj = new Common_utill();
    $pack_prd_id = 115;
    // if($this->input->post('pack_prd_id') != null){
    if($pack_prd_id != null){
      // $this->form_validation->set_rules('pack_prd_id', 'Id ','trim|required|min_length[1]|regex_match[/^[[0-9 \-]+$/]',array('required'=> 'Required','min_length' => '','regex_match' => ''));
      // if ($this->form_validation->run() != false){

        $pin_obj = unserialize($this->session->userdata('pin_obj'));

        if($pin_obj != null){

          $user_id = $pin_obj->getPinRequestedUserId()->getUserId();

          $amount = $utill_obj->get_available_balance($user_id);
          $pack_prd_price = $utill_obj->get_package_product_price($pack_prd_id);
          $product_list_array = $utill_obj->get_products_from_pack_prd_id($pack_prd_id);

          $obj = new Entity\Package_prd_detail;
          $obj->setUserAvailableAmount($amount);
          $obj->setPackPrdPrice($pack_prd_price);
          $obj->setProductListArray($product_list_array);
          $data['obj'] = $obj;
          //$data['prd_ids'] = $prd_ids;

          $data['product_details'] = $utill_obj->get_product_details_from_product_id($prd_ids);
          // print_r($pp);exit();
          
          $this->session->set_userdata('user_id',$user_id);
          $this->session->set_userdata('amount',$amount);
          $this->session->set_userdata('pack_prd_id',$pack_prd_id);
          $this->view_call($main_folder_name,$sub_folder_name,$file_name1,$data);
        }
      // }else{
        // $this->session->set_flashdata('error', '<p>Error in Fields</p>');
        // $this->view_call($main_folder_name,$sub_folder_name,$file_name1);
      // }
    }else{$this->view_call($main_folder_name,$sub_folder_name,$file_name1);}
  }
  public function package_purchase(){
    $main_folder_name = 'purchase';$sub_folder_name = 'pin';$file_name = 'Purchase_success_page';$file_name = 'purchase_success_page';
    $utill_obj = new Common_utill();
    $user_id = $this->session->userdata('user_id');
    $amount = $this->session->userdata('amount');
    $pack_prd_id = $this->session->userdata('pack_prd_id');
    // exit();
    $purchase_obj = $utill_obj->purchase($user_id,$amount,$pack_prd_id);
    if($purchase_obj == true){
      $this->view_call($main_folder_name,$sub_folder_name,$file_name);

      $pin_obj = unserialize($_SESSION['pin_obj']);
      $pin_id = $pin_obj->getPinId();
      $pin_used_user_id = $user_id;
      $pin_status = 0;
      $pin_used_time = new \DateTime(date("Y-m-d H:i:s"));
      $update_pin_obj = $utill_obj->update_pin($pin_id,$pin_used_user_id,$pin_status,$pin_used_time);
      if($update_pin_obj != null){
        $this->session->set_flashdata('success', '<p>Purchase Success</p>');
        redirect('index.php/purchase/Success','refresh');
      }else{
        $this->session->set_flashdata('error', '<p>Money Transaction Failed</p>');
        $this->view_call($main_folder_name,$sub_folder_name,$file_name1);
      }
    }else{
      $this->view_call($main_folder_name,$sub_folder_name,$file_name1);
    }
  }
// asdf
  //  public function add_money(){
  //    $main_folder_name = 'purchase';$sub_folder_name = 'pin';$file_name = 'Add_money_page';$file_name1 = 'Package_prd_detail_page';
  //    $utill_obj = new Common_utill();
  //    if($this->input->post('rupee') != null && $this->input->post('money_add_page') != null){
  //      $this->form_validation->set_rules('rupee', 'Id ','trim|required|min_length[1]|regex_match[/^[[0-9 \-]+$/]',array('required'=> 'Required','min_length' => '','regex_match' => ''));
  //      if ($this->form_validation->run() != false){
  //        $rupee = $this->input->post('rupee');
  //        $add = $utill_obj->add_money($rupee);
  //        if($add == true){
  //          $pin_obj = unserialize($this->session->userdata('pin_obj'));

  //          $user_id = $pin_obj->getPinRequestedUserId()->getUserId();

  //          $pack_prd_id = $this->session->userdata('pack_prd_id');
  //          $amount = $utill_obj->get_available_balance($user_id);
  //          $pack_prd_price = $utill_obj->get_package_product_price($pack_prd_id);
            
  //          $product_list_array = $utill_obj->get_products_from_pack_prd_id($pack_prd_id);

  //          $obj = new Entity\Package_prd_detail;
  //          $obj->setUserAvailableAmount($amount);
  //          $obj->setPackPrdPrice($pack_prd_price);
  //          $obj->setProductListArray($product_list_array);
  //          $data['obj'] = $obj;

  //          $this->session->set_userdata('user_id',$user_id);
  //          $this->session->set_userdata('amount',$amount);
  //          $this->session->set_userdata('pack_prd_id',$pack_prd_id);
  //          $this->view_call($main_folder_name,$sub_folder_name,$file_name1,$data);
  //        }else{
  //          $this->session->set_flashdata('error', '<p>Money Added Failed</p>');
  //          $this->view_call($main_folder_name,$sub_folder_name,$file_name);
  //        }
  //      }else{
  //        $this->session->set_flashdata('error', '<p>Error in Fields</p>');
  //        $this->view_call($main_folder_name,$sub_folder_name,$file_name);
  //      }
  //    }else{
  //      $this->view_call($main_folder_name,$sub_folder_name,$file_name);
  //    }
  //  }
// asdf
  public function success(){
    $main_folder_name = 'purchase';$sub_folder_name = 'pin';$file_name = 'purchase_success_page';
    $this->view_call($main_folder_name,$sub_folder_name,$file_name);
  }
  public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
    $this->load->view('header');
    $this->load->view($main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
    $this->load->view('footer');
  }
}
?>