<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\ORM\Query\ResultSetMapping;

require_once 'ssp.customized.class.php';
class Registration extends CI_Controller {

  protected static $reverse_tree_ar = array();

  public function index() {
    // $this->session->sess_destroy();
    $this->load->view('header');
    $this->load->view('footer');
  }
  public function enter_pin() {
    $this->session->set_userdata('pin_obj', "");
    $this->session->set_userdata('user_purchase_obj', "");
    $this->session->set_userdata('user_personal_details', "");
    $this->session->set_userdata('user_id_details', "");
    $this->session->set_userdata('user_account_details', "");
    $this->session->set_userdata('user_password_details', "");
    $main_folder_name = 'registration';
    $sub_folder_name = 'pin';
    $file_name = 'Pin_enter_page';
    $utill_obj = new Common_utill();
    if ($this->input->post('pin_no') != null || $this->input->post('pin_page') != null) {

      $this->form_validation->set_rules('pin_no', 'Pin Number ', 'trim|required', array('required' => 'Required'));
      if ($this->form_validation->run() != false) {
        $pin_no = $this->input->post('pin_no');
        $for = 0;
        $obj = $utill_obj->check_pin_get_details($pin_no, $for);
        if (!is_numeric($obj) && $obj != null) {
          $this->session->set_userdata('pin_obj', serialize($obj));
          $this->ref_detail();
        } elseif ($obj == -2) {
          $this->session->set_flashdata('error', '<p>Pin is Locked.</p>');
          $this->view_call($main_folder_name, $sub_folder_name, $file_name);
        } elseif ($obj == -3) {
          $this->session->set_flashdata('error', '<p>Pin is Already Used.</p>');
          $this->view_call($main_folder_name, $sub_folder_name, $file_name);
        } elseif ($obj == -4) {
          $this->session->set_flashdata('error', '<p>Pin is Created for Purchase Only.</p>');
          $this->view_call($main_folder_name, $sub_folder_name, $file_name);
        } elseif ($obj == -5) {
          $this->session->set_flashdata('error', '<p>This is Not Your Pin</p>');
          $this->view_call($main_folder_name, $sub_folder_name, $file_name);
        } else {
          $this->session->set_flashdata('error', '<p>Enter The Valid Pin</p>');
          $this->view_call($main_folder_name, $sub_folder_name, $file_name);
        }
      } else {
        $this->session->set_flashdata('error', '<p>Error in Fields</p>');
        $this->view_call($main_folder_name, $sub_folder_name, $file_name);
      }
    } else {

      $this->view_call($main_folder_name, $sub_folder_name, $file_name);
    }
  }
  public function cus_select_enter_pin() {
    $this->session->set_userdata('pin_obj', "");
    $this->session->set_userdata('user_purchase_obj', "");
    $this->session->set_userdata('user_personal_details', "");
    $this->session->set_userdata('user_id_details', "");
    $this->session->set_userdata('user_account_details', "");
    $this->session->set_userdata('user_password_details', "");
    $data['actual_id'] = $this->input->post('top_id');
    $data['side'] = $this->input->post('pos');
    $utill_obj = new Common_utill();
    $avail = $utill_obj->check_node_is_available($this->input->post('top_id'), $this->input->post('pos'));

    if ($avail == 1) {
      $main_folder_name = 'registration';
      $sub_folder_name = 'pin';
      $file_name = 'Cus_pin_enter_page';
      $this->view_call($main_folder_name, $sub_folder_name, $file_name, $data);
    } elseif ($avail == 0) {
      $this->session->set_flashdata('error', '<p>Node is Not Avilable.Choose Another One</p>');
      redirect('index.php/package_purchase/tree', 'refresh');
    } else {
      $this->session->set_flashdata('error', '<p>User Not Exists</p>');
      redirect('index.php/package_purchase/tree', 'refresh');
    }
  }
  public function cs_pin_enr() {
    $this->session->set_userdata('pin_obj', "");
    $this->session->set_userdata('user_purchase_obj', "");
    $this->session->set_userdata('user_personal_details', "");
    $this->session->set_userdata('user_id_details', "");
    $this->session->set_userdata('user_account_details', "");
    $this->session->set_userdata('user_password_details', "");
    $data['actual_id'] = $this->input->post('top_id');
    $data['side'] = $this->input->post('pos');
    $utill_obj = new Common_utill();
    $main_folder_name = 'registration';
    $sub_folder_name = 'pin';
    $file_name = 'Cus_pin_enter_page';

    if ($this->input->post('pin_no') != null || $this->input->post('pin_page') != null) {

      $data['side'] = $this->input->post('side');
      $data['actual_id'] = $this->input->post('actual_id');
      $this->form_validation->set_rules('pin_no', 'Pin Number ', 'trim|required', array('required' => 'Required'));
      if ($this->form_validation->run() != false) {
        $pin_no = $this->input->post('pin_no');
        $for = 0;
        $obj = $utill_obj->check_pin_get_details($pin_no, $for);

        if (!is_numeric($obj)) {
          $this->session->set_userdata('pin_obj', serialize($obj));
          $this->ref_detail_cus_select($this->input->post('actual_id'), $this->input->post('side'));
        } elseif ($obj == -2) {
          $this->session->set_flashdata('error', '<p>Pin is Locked.</p>');
          $this->view_call($main_folder_name, $sub_folder_name, $file_name, $data);
        } elseif ($obj == -3) {
          $this->session->set_flashdata('error', '<p>Pin is Already Used.</p>');
          $this->view_call($main_folder_name, $sub_folder_name, $file_name, $data);
        } elseif ($obj == -4) {
          $this->session->set_flashdata('error', '<p>Pin is Created for Purchase Only.</p>');
          $this->view_call($main_folder_name, $sub_folder_name, $file_name, $data);
        } elseif ($obj == -5) {
          $this->session->set_flashdata('error', '<p>This is Not Your Pin</p>');
          $this->view_call($main_folder_name, $sub_folder_name, $file_name, $data);
        }else {
          $this->session->set_flashdata('error', '<p>Enter The Valid Pin</p>');
          $this->view_call($main_folder_name, $sub_folder_name, $file_name, $data);
        }
      } else {
        $this->session->set_flashdata('error', '<p>Error in Fields</p>');
        $this->view_call($main_folder_name, $sub_folder_name, $file_name, $data);
      }
    } else {
      $this->view_call($main_folder_name, $sub_folder_name, $file_name, $data);
    }
  }
  public function ref_detail_cus_select($actual_id, $side) {
    $em = $this->doctrine->em;
    $main_folder_name = 'registration';
    $sub_folder_name = 'pin';
    $file_name = 'Referral_enter_page';
    $utill_obj = new Common_utill();
    $pin_obj = unserialize($_SESSION['pin_obj']);
    $user_obj = $pin_obj->getPinRequestedUserId();
    $p_id = $pin_obj->getPinId();
    $data['user'] = $user_obj;

    $p_obj = $utill_obj->get_single_row_using_array_value('User_package_purchase', 'user_purchase_pin_id', $p_id);
    $pur_id = 0;
    if ($p_obj != null) {
      $pur_id = $p_obj->getUserPurchaseId();
    }

    $data['pur_id'] = $pur_id;
    // echo   $user_id = $actual_id;exit();
    $pin_obj = unserialize($this->session->userdata('pin_obj'));

    $user_name = $pin_obj->getPinRequestedUserId()->getUserName();

    $sponcer_user_id = $pin_obj->getPinRequestedUserId()->getUserId();
    $sponcer_user_purchase_obj = $utill_obj->get_single_row_using_array_value('User_package_purchase', 'user_id', $sponcer_user_id);
    if ($sponcer_user_purchase_obj == null) {
      $sponcer_user_purchase_obj = $utill_obj->get_single_row_using_array_value('User_package_purchase', 'user_id', RT::$root_user_id);
    }
    // echo $actual_id;exit();
    $actual_user_obj = $utill_obj->get_single_row_using_primary_key('User', $actual_id);
    $actual_user_id = $actual_user_obj->getUserId();
    $actual_user_purchase_obj = $utill_obj->get_single_row_using_array_value('User_package_purchase', 'user_id', $actual_user_id);
    $actual_gene_user_purchase_id = $actual_user_purchase_obj->getUserPurchaseId();

    // Achive Code Start
    $first_act_up_id = $actual_gene_user_purchase_id;
    $root_act_up_id = $actual_user_purchase_obj->getUserPurchaseActualGeneUserPurchaseId();
    $root_act_obj = $utill_obj->get_single_row_using_primary_key('User_package_purchase', $root_act_up_id);
    if ($root_act_obj != null) {
      if ($root_act_obj->getPairAchieve() == 0) {
        if ($root_act_obj->getUserPurchaseGeneLeftPurchaseId() == $first_act_up_id) {
          $root_rit_obj = $utill_obj->get_single_row_using_primary_key('User_package_purchase', $root_act_obj->getUserPurchaseId());
          if ($root_rit_obj != null) {
            $root_rit_id = $root_rit_obj->getUserPurchaseGeneRightPurchaseId();
            $root_rit_rit = $utill_obj->get_single_row_using_primary_key('User_package_purchase', $root_rit_id);
            if ($root_rit_rit != null) {
              if ($root_rit_rit->getUserPurchaseGeneRightPurchaseId() == 0 && $root_rit_rit->getUserPurchaseGeneLeftPurchaseId() == 0) {
                $achive_ar = array('achive_id' => $root_act_obj->getUserPurchaseId(), 'achive_value' => 1);
              } else { $achive_ar = array('achive_id' => '', 'achive_value' => 0);}
            } else { $achive_ar = array('achive_id' => '', 'achive_value' => 0);}
          } else {
            $achive_ar = array('achive_id' => '', 'achive_value' => 0);
          }

        } else {
          $root_rit_obj = $utill_obj->get_single_row_using_primary_key('User_package_purchase', $root_act_obj->getUserPurchaseId());
          if ($root_rit_obj != null) {
            $root_rit_id = $root_rit_obj->getUserPurchaseGeneLeftPurchaseId();
            $root_rit_rit = $utill_obj->get_single_row_using_primary_key('User_package_purchase', $root_rit_id);
            if ($root_rit_rit != null) {
              if ($root_rit_rit->getUserPurchaseGeneLeftPurchaseId() == 0 && $root_rit_rit->getUserPurchaseGeneRightPurchaseId() == 0) {
                $achive_ar = array('achive_id' => $root_act_obj->getUserPurchaseId(), 'achive_value' => 1);
              } else { $achive_ar = array('achive_id' => '', 'achive_value' => 0);}
            } else { $achive_ar = array('achive_id' => '', 'achive_value' => 0);}
          } else {
            $achive_ar = array('achive_id' => '', 'achive_value' => 0);
          }
        }
      } else {
        if ($side == 1) {
          $act_second_id = $actual_user_purchase_obj->getUserPurchaseGeneLeftPurchaseId();
          $act_second_obj = $utill_obj->get_single_row_using_primary_key('User_package_purchase', $act_second_id);

          if ($act_second_obj != null) {
            if ($act_second_obj->getUserPurchaseGeneLeftPurchaseId() > 0 || $act_second_obj->getUserPurchaseGeneRightPurchaseId() > 0) {

              $achive_ar = array('achive_id' => $actual_gene_user_purchase_id, 'achive_value' => 1);

            } else { $achive_ar = array('achive_id' => '', 'achive_value' => 0);}
          } else { $achive_ar = array('achive_id' => '', 'achive_value' => 0);}
        } else {
          $act_second_id = $actual_user_purchase_obj->getUserPurchaseGeneRightPurchaseId();
          $act_second_obj = $utill_obj->get_single_row_using_primary_key('User_package_purchase', $act_second_id);

          if ($act_second_obj != null) {
            if ($act_second_obj->getUserPurchaseGeneLeftPurchaseId() > 0 || $act_second_obj->getUserPurchaseGeneRightPurchaseId() > 0) {
              $achive_ar = array('achive_id' => $actual_gene_user_purchase_id, 'achive_value' => 1);
            } else { $achive_ar = array('achive_id' => '', 'achive_value' => 0);}
          } else { $achive_ar = array('achive_id' => '', 'achive_value' => 0);}
        }
      }
    } else {
      if ($root_act_up_id == 0) {
        if ($side == 1) {
          $act_second_id = $actual_user_purchase_obj->getUserPurchaseGeneLeftPurchaseId();
          $act_second_obj = $utill_obj->get_single_row_using_primary_key('User_package_purchase', $act_second_id);

          if ($act_second_obj != null) {
            if ($act_second_obj->getUserPurchaseGeneLeftPurchaseId() > 0 || $act_second_obj->getUserPurchaseGeneRightPurchaseId() > 0) {

              $achive_ar = array('achive_id' => $actual_gene_user_purchase_id, 'achive_value' => 1);

            } else { $achive_ar = array('achive_id' => '', 'achive_value' => 0);}
          } else { $achive_ar = array('achive_id' => '', 'achive_value' => 0);}
        } else {
          $act_second_id = $actual_user_purchase_obj->getUserPurchaseGeneRightPurchaseId();
          $act_second_obj = $utill_obj->get_single_row_using_primary_key('User_package_purchase', $act_second_id);

          if ($act_second_obj != null) {
            if ($act_second_obj->getUserPurchaseGeneRightPurchaseId() > 0 || $act_second_obj->getUserPurchaseGeneRightPurchaseId() > 0) {
              $achive_ar = array('achive_id' => $actual_gene_user_purchase_id, 'achive_value' => 1);
            } else { $achive_ar = array('achive_id' => '', 'achive_value' => 0);}
          } else { $achive_ar = array('achive_id' => '', 'achive_value' => 0);}
        }
      } else {
        $achive_ar = array('achive_id' => '', 'achive_value' => 0);
      }
    }

    // Achieve Code End

    $transaction_obj = $utill_obj->get_single_row_using_primary_key('Transactions', 2);

    if ($sponcer_user_purchase_obj != null) {

      // if($sponcer_user_purchase_obj->getUserPurchaseGeneLeftPurchaseId() == $actual_gene_user_purchase_id){
      //   $ac_side = 0;
      // }elseif($sponcer_user_purchase_obj->getUserPurchaseGeneRightPurchaseId() == $actual_gene_user_purchase_id){
      //   $ac_side = 1;
      // }else{
      //   $ac_side = 0;
      // }

      // echo $sponcer_user_purchase_obj->getUserPurchaseGeneRighttPurchaseId();
      // echo "--";echo $actual_gene_user_purchase_id;
      // exit();

      $user_purchase_array = array(
        'user_purchase_sponcer_user_purchase_id' => $sponcer_user_purchase_obj->getUserPurchaseId(),
        'user_purchase_sponcer_gene_side' => $side,
        'user_purcahse_pin_id' => $pin_obj,
        'user_purchase_actual_gene_user_purchase_id' => $actual_gene_user_purchase_id,
        'user_purchase_actual_gene_side' => $side,
        'user_purchase_gene_left_purchase_id' => 0,
        'user_purchase_gene_right_purchase_id' => 0,
        'user_purchase_money_alloted' => 0,
        'user_purchase_transaction_id' => serialize($transaction_obj),
        'user_pruchase_pair_user_purchase_id' => 0,
        'optradio' => $this->input->post('optradio'),
        'achieve' => $achive_ar,
      );

      $this->session->set_userdata('user_purchase_obj', $user_purchase_array);

      if($pin_obj->getPinRequestPackageId() != null){
        $package_id = $pin_obj->getPinRequestPackageId()->getPackageId();
        $dd['pack_prd_obj'] = $utill_obj->get_multiple_rows_using_array_value('Package_products', 'package_id', $package_id);
        if($pin_obj->getPinRequestPackageProductId() != null){
          $package_product_id = $pin_obj->getPinRequestPackageProductId()->getPackageProductId();
        }else{
          $package_product_id = 1;
        }
        $package_product_obj = $em->find('Entity\Package_products', $package_product_id);
        $package_id = $package_product_obj->getPackageId()->getPackageId();
        $qb = $em->createQueryBuilder();
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('Entity\Packages', 'u');
        $rsm->addFieldResult('u', 'package_id', 'package_id');
        $rsm->addFieldResult('u', 'package_name', 'package_name');
        $query = $em->createNativeQuery('SELECT V.* FROM Packages V LEFT JOIN Package_products E ON V.package_id!= ' . $package_id . ' AND V.package_id = E.package_id  WHERE E.package_id  IS NULL', $rsm);
        $dd['attachment'] = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $package_product_id, 'attachment_referer_type' => 6));
        $dd['package'] = $query->getResult();
        $dd['package_product_item'] = $em->getRepository('Entity\Package_product_items')->findBy(array('package_product_id' => $package_product_id));
        $dd['obj'] = $package_product_obj;
        $this->view_call($main_folder_name, $sub_folder_name, 'Shopping_cart_page', $dd);
      }else{
        $this->add_user();
      }
      
    }
  }
  public function ref_detail() {
    $em = $this->doctrine->em;
    $main_folder_name = 'registration';
    $sub_folder_name = 'pin';
    $file_name = 'Referral_enter_page';
    $utill_obj = new Common_utill();
    $pin_obj = unserialize($_SESSION['pin_obj']);
    $user_obj = $pin_obj->getPinRequestedUserId();
    $p_id = $pin_obj->getPinId();

    $data['user'] = $user_obj;
    $data['empty_obj'] = $utill_obj->get_empty_child_nodes();

    $p_obj = $utill_obj->get_single_row_using_array_value('User_package_purchase', 'user_purchase_pin_id', $p_id);

    $pur_id = 0;
    if ($p_obj != null) {
      $pur_id = $p_obj->getUserPurchaseId();
    }
    $data['pur_id'] = $pur_id;
    //print_r($utill_obj->get_empty_child_nodes());exit();
    if ($this->input->post('user_name') != null || $this->input->post('ref_page') != null) {

      $this->form_validation->set_rules('user_name', 'User Name', 'trim|required|callback_find_alphabets|min_length[5]|max_length[20]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]', array('required' => 'User Name Requierd', 'min_length' => 'User Name needs Minimum 5 charecters', 'max_length' => 'Maximum 20 Charecters are allowed', 'find_alphabets' => 'Albhabets Required'));

      if ($this->form_validation->run() != false) {
        //print_r($_POST);exit();
        $u_name = $this->input->post('user_name');
        $f_name = $this->input->post('f_name');
        $side = $this->input->post('optradio');

        if ($side == 2) {
          $side == 0;
        }

        $pin_obj = unserialize($this->session->userdata('pin_obj'));

        $user_name = $pin_obj->getPinRequestedUserId()->getUserName();

        $sponcer_user_id = $pin_obj->getPinRequestedUserId()->getUserId();
        $sponcer_user_purchase_obj = $utill_obj->get_single_row_using_array_value('User_package_purchase', 'user_id', $sponcer_user_id);

        $actual_user_obj = $utill_obj->get_single_row_using_array_value('User', 'user_name', $u_name);
        $actual_user_id = $actual_user_obj->getUserId();
        $actual_user_purchase_obj = $utill_obj->get_single_row_using_array_value('User_package_purchase', 'user_id', $actual_user_id);

        // Achive Code Start
        $actual_gene_user_purchase_id = $actual_user_purchase_obj->getUserPurchaseId();

        $actual_gene_parent_up_obj = $utill_obj->get_single_row_using_primary_key('User_package_purchase', $actual_gene_user_purchase_id);
        $act_left = $actual_gene_parent_up_obj->getUserPurchaseGeneLeftPurchaseId();
        $act_right = $actual_gene_parent_up_obj->getUserPurchaseGeneRightPurchaseId();
        $achive_ar = array();
        if ($act_left > 0 && $act_right > 0) {
          $achive_ar = array('achive_id' => $actual_gene_parent_up_obj->getUserPurchaseId(), 'achive_value' => 1);
        } else {
          $achive_ar = array('achive_id' => $actual_gene_parent_up_obj->getUserPurchaseId(), 'achive_value' => 0);
        }
        // Achieve Code End

        $transaction_obj = $utill_obj->get_single_row_using_primary_key('Transactions', 2);

        if ($sponcer_user_purchase_obj != null) {

          $user_purchase_array = array(

            'user_purchase_sponcer_user_purchase_id' => $sponcer_user_purchase_obj->getUserPurchaseId(),
            'user_purchase_sponcer_gene_side' => $side,
            'user_purcahse_pin_id' => $pin_obj,
            'user_purchase_actual_gene_user_purchase_id' => $actual_gene_user_purchase_id,
            'user_purchase_actual_gene_side' => $side,
            'user_purchase_gene_left_purchase_id' => 0,
            'user_purchase_gene_right_purchase_id' => 0,
            'user_purchase_money_alloted' => 0,
            'user_purchase_transaction_id' => serialize($transaction_obj),
            'user_pruchase_pair_user_purchase_id' => 0,
            'optradio' => $this->input->post('optradio'),
            'achieve' => $achive_ar,

          );

          $this->session->set_userdata('user_purchase_obj', $user_purchase_array);

          $package_id = $pin_obj->getPinRequestPackageId()->getPackageId();
          $dd['pack_prd_obj'] = $utill_obj->get_multiple_rows_using_array_value('Package_products', 'package_id', $package_id);
          $package_product_id = 1;
          $package_product_obj = $em->find('Entity\Package_products', $package_product_id);
          $package_id = $package_product_obj->getPackageId()->getPackageId();
          $qb = $em->createQueryBuilder();
          $rsm = new ResultSetMapping();
          $rsm->addEntityResult('Entity\Packages', 'u');
          $rsm->addFieldResult('u', 'package_id', 'package_id');
          $rsm->addFieldResult('u', 'package_name', 'package_name');
          $query = $em->createNativeQuery('SELECT V.* FROM Packages V LEFT JOIN Package_products E ON V.package_id!= ' . $package_id . ' AND V.package_id = E.package_id  WHERE E.package_id  IS NULL', $rsm);
          $dd['attachment'] = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $package_product_id, 'attachment_referer_type' => 6));
          $dd['package'] = $query->getResult();
          $dd['package_product_item'] = $em->getRepository('Entity\Package_product_items')->findBy(array('package_product_id' => $package_product_id));

          $dd['obj'] = $package_product_obj;

          $this->view_call($main_folder_name, $sub_folder_name, 'Shopping_cart_page', $dd);

        } else {
          $this->session->set_flashdata('error', '<p>User Not Exists</p>');
          $this->view_call($main_folder_name, $sub_folder_name, $file_name, $data);
        }
      } else {
        $this->session->set_flashdata('error', '<p>Error in Fields</p>');
        $this->view_call($main_folder_name, $sub_folder_name, $file_name, $data);
      }

    } else {
      // print_r($_SESSION);
      $this->view_call($main_folder_name, $sub_folder_name, $file_name, $data);
    }
  }
  public function add_user() {
    $em = $this->doctrine->em;
    $main_folder_name = 'registration';
    $sub_folder_name = 'user';
    $file_name = 'Add_user';
    $file_name2 = 'Account_details_page';
    $utill_obj = new Common_utill();
    $data['country_list'] = $em->getRepository("Entity\Country")->findAll();
    $data['pincode_list'] = $em->getRepository("Entity\Pincode")->findAll();
    if ($this->input->post('user_page') != null) {

      //$this->form_validation->set_rules('user_name', 'User Name', 'trim|required|min_length[1]|max_length[30]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]', array('required' => 'User Name Requierd', 'min_length' => 'User Name needs Minimum 1 charecters', 'max_length' => 'Maximum 20 Charecters are allowed'));
      //$this->form_validation->set_rules('user_email', 'User Mail', 'trim|required|valid_email', array('required' => 'Email Required', 'valid_email' => 'Enter Valid EmailId'));
      $this->form_validation->set_rules('first_name', 'First Name', 'trim|required|callback_find_alphabets|min_length[3]|max_length[20]|regex_match[/^[a-zA-Z._ ,%-]+$/]', array('required' => 'First Name Requierd', 'min_length' => 'First Name needs Minimum 3 charecters', 'max_length' => 'Maximum 20 Charecters are allowed', 'find_alphabets' => 'Albhabets Required'));
      //$this->form_validation->set_rules('last_name', 'Last Name', 'trim|min_length[0]|max_length[20]|regex_match[/^[a-zA-Z._ ,%-]+$/]', array('max_length' => 'Maximum 20 Charecters are allowed', 'find_alphabets' => 'Albhabets Required'));

      //$this->form_validation->set_rules('dob', 'DOB', 'trim|required|callback_is_start_date_valid', array('required' => 'Required', 'is_start_date_valid' => 'Enter Valid Date'));
      $this->form_validation->set_rules('gender', 'Gender', 'trim|required|regex_match[/^[0-9]+$/]', array('required' => 'Gender Requierd', 'regex_match' => 'Enter valid Gender'));

      //User Contact Validation Start
      $this->form_validation->set_rules('mobile', 'Mobile ', 'trim|required|min_length[10]|max_length[20]|regex_match[/^[[0-9-+  \-]+$/]', array('required' => 'Mobile Requierd', 'min_length' => 'Mobile needs Minimum 10 charecters', 'max_length' => 'Maximum 20 Charecters are allowed', 'find_alphabets' => 'Albhabets Required'));
      $this->form_validation->set_rules('alter_mobile', 'Alter Mobile ', 'trim|max_length[20]|regex_match[/^[[0-9-+  \-]+$/]', array('required' => 'Mobile Requierd', 'min_length' => 'Mobile needs Minimum 10 charecters', 'max_length' => 'Maximum 20 Charecters are allowed'));
      $this->form_validation->set_rules('land_line', 'Landline ', 'trim|max_length[20]|regex_match[/^[[0-9-+  \-]+$/]', array('required' => 'Landline Requierd', 'min_length' => 'Landline needs Minimum 10 charecters', 'max_length' => 'Maximum 20 Charecters are allowed', 'find_alphabets' => 'Albhabets Required'));
      //User Contact Validation End
      //Nominee Validation Start
      $this->form_validation->set_rules('n_name', 'Nominee Name', 'trim|required|callback_find_alphabets|min_length[3]|max_length[20]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]', array('required' => 'Nominee Name Requierd', 'min_length' => 'Nominee Name needs Minimum 5 charecters', 'max_length' => 'Maximum 20 Charecters are allowed', 'find_alphabets' => 'Albhabets Required'));

      $this->form_validation->set_rules('n_relationship', 'Nominee', 'trim|required|min_length[3]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]', array('required' => 'Nominee Name Requierd', 'min_length' => 'Required'));
      //$this->form_validation->set_rules('n_dob', 'DOB', 'trim|required|callback_is_start_date_valid', array('required' => 'Required', 'is_start_date_valid' => 'Enter Valid Date'));
      //$this->form_validation->set_rules('n_phoneno', 'Mobile ', 'trim|required|min_length[10]|max_length[20]|regex_match[/^[[0-9-+  \-]+$/]', array('required' => 'Mobile Requierd', 'min_length' => 'Mobile needs Minimum 10 charecters', 'max_length' => 'Maximum 20 Charecters are allowed', 'find_alphabets' => 'Albhabets Required'));
      $this->form_validation->set_rules('n_gender', 'Gender', 'trim|required|regex_match[/^[0-9]+$/]', array('required' => 'Gender Requierd', 'regex_match' => 'Enter valid Gender'));
      //Nominee Validation End

      $this->form_validation->set_rules('full_address', 'Address', 'trim|required|callback_find_alphabets|min_length[5]|max_length[255]', array('min_length' => 'Minimum 5 Char Needed', 'max_length' => 'Maximum 255 Charecters are allowed', 'find_alphabets' => 'Albhabets Required'));
      $this->form_validation->set_rules('pincode_no', 'Pincode', 'trim|required|regex_match[/^[0-9]+$/]|min_length[4]|max_length[8]', array('regex_match' => 'Numbers Only Allowed', 'min_length' => 'Minimumm length 4', 'max_length' => 'Maximum Length 8'));

      if ($this->form_validation->run() != false) {
        // variable declaratinos Start
        // $user_name = $this->input->post('user_name');
        $user_email = $this->input->post('user_email');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $gender = $this->input->post('gender');
        if($this->input->post('dob') != null){
          $dob = new DateTime($this->input->post('dob'));
        }else{$dob = null;}

        $n_name = $this->input->post('n_name');
        if($this->input->post('n_dob') !=null){
          $n_dob = new DateTime($this->input->post('n_dob'));
        }else{$n_dob = null;}
        $n_relationship = $this->input->post('n_relationship');
        $n_phoneno = $this->input->post('n_phoneno');
        $n_gender = $this->input->post('n_gender');

        $mobile = $this->input->post('mobile');
        $alter_mobile = $this->input->post('alter_mobile');

        $full_address = $this->input->post('full_address');
        $pincode_no = $this->input->post('pincode_no');

        $district = $this->input->post('district');
        $state = $this->input->post('state');
        $country_id = $this->input->post('country_id');

        $place = "asdasd";
        $pincode_obj = $utill_obj->get_single_row_using_array_value('Pincode', 'pincode_no', $pincode_no);
        if ($pincode_obj == null) {
          $country_obj = $utill_obj->get_reference_obj("Country", $country_id);
          $pincode_obj = $utill_obj->add_pincode($pincode_no, $place, $district, $state, $country_obj);
        }
        $image = $_FILES;

        // variable declaratinos End

        // $user_avail = $utill_obj->check_availability('User', 'user_name', $user_name);
        //$email_avail = $utill_obj->check_availability('User', 'user_email', $user_email);
        $user_avail = 1;
        $email_avail=1;
        // $al_obj = $utill_obj->get_single_row_using_primary_key('User', $user_name);
        $al_obj = null;
        if ($al_obj == null) {
          if ($user_avail == 1) {
            if ($email_avail == 1) {
              $ar = array(
                'user_name' => '',
                'user_email' => $user_email,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'gender' => $gender,
                'dob' => $dob,
                'n_name' => $n_name,
                'n_dob' => $n_dob,
                'n_relationship' => $n_relationship,
                'n_phoneno' => $n_phoneno,
                'n_gender' => $n_gender,
                'mobile' => $mobile,
                'alter_mobile' => $alter_mobile,
                'full_address' => $full_address,
                'pincode_no' => $pincode_no,
                'image' => $image,
                'pincode_obj' => serialize($pincode_obj),
              );
              $this->session->set_userdata('user_personal_details', $ar);
              $this->view_call($main_folder_name, $sub_folder_name, $file_name2);
            } else {
              $this->session->set_flashdata('error', '<p>User Email Already Registered</p>');
              $this->view_call($main_folder_name, $sub_folder_name, $file_name, $data);
            }
          } else {
            $this->session->set_flashdata('error', '<p>User Already Exists</p>');
            $this->view_call($main_folder_name, $sub_folder_name, $file_name, $data);
          }
        } else {
          $this->session->set_flashdata('error', '<p>UserName or User Id Already Exists</p>');
          $this->view_call($main_folder_name, $sub_folder_name, $file_name, $data);
        }
      } else {
        $this->session->set_flashdata('error', '<p>Error in Fields</p>');
        $this->view_call($main_folder_name, $sub_folder_name, $file_name, $data);
      }
    } else {
      $this->view_call($main_folder_name, $sub_folder_name, $file_name, $data);
    }
  }
  public function add_account_details() {
    $main_folder_name = 'registration';
    $sub_folder_name = 'user';
    $file_name = 'Account_details_page';
    $utill_obj = new Common_utill();
    if ($this->input->post('account_no') != null || $this->input->post('acc_page') != null) {

      $this->form_validation->set_rules('account_holder_name', 'Account Holder Name', 'trim|callback_find_alphabets|required|min_length[3]|max_length[255]|regex_match[/^[[a-zA-Z0-9._ ,\-]+$/]', array('required' => 'Bank Name Requierd', 'min_length' => 'Bank Name needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed', 'find_alphabets' => 'Albhabets Required'));
      $this->form_validation->set_rules('account_no', 'Account No', 'trim|required|min_length[5]|max_length[30]|regex_match[/^[0-9]+$/]', array('required' => 'PackageName Requierd', 'min_length' => 'AccountHolderName needs Minimum 5 charecters', 'max_length' => 'Maximum 255 Charecters are allowed', 'find_alphabets' => 'Albhabets Required'));
      $this->form_validation->set_rules('account_type', 'Account Type', 'trim|required|regex_match[/^[0-9\-]+$/]', array('regex_match' => 'Numbers Only Allowed'));
      // $this->form_validation->set_rules('bank_ifsc', 'Bank IFSC','trim|callback_alphabets_and_number|required|min_length[5]|max_length[255]|regex_match[/^[[a-zA-Z0-9]+$/]',array('required'=> 'Bank Name Requierd','min_length' => 'Bank Name needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','alphabets_and_number'=>'Enter Valid IFSC Code'));

      $this->form_validation->set_rules('bank_name', 'Bank Name', 'trim|required|min_length[3]|max_length[255]', array('required' => 'Bank Name Requierd', 'min_length' => 'Bank Name needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed'));
      $this->form_validation->set_rules('bank_branch', 'Bank Branch', 'trim|required|min_length[3]|max_length[255]', array('required' => 'Bank Branch Requierd', 'min_length' => 'Bank Name needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed'));

      if ($this->form_validation->run() != false) {
        $account_holder_name = $this->input->post('account_holder_name');
        $account_no = $this->input->post('account_no');
        $account_type = $this->input->post('account_type');

        $bank_ifsc = $this->input->post('bank_ifsc');
        $bank_obj = $utill_obj->get_single_row_using_array_value('Bank_details', 'bank_ifsc', $bank_ifsc);

        $account_avail = $utill_obj->check_availability('Account_details', 'account_no', $account_no);
        if ($account_avail == 1) {

          $bank_avail = $utill_obj->get_single_row_using_array_value('Bank_details', 'bank_ifsc', $bank_ifsc);

          if ($bank_avail != null) {

            $ar = array(
              'account_holder_name' => $_SESSION['user_personal_details']['first_name'],
              // 'account_holder_name'  =>      $account_holder_name,
              'account_no' => $account_no,
              'account_type' => $account_type,
              'bank_ifsc' => $bank_ifsc,
              'bank_obj' => serialize($bank_obj),
            );
            $this->session->set_userdata('user_account_details', $ar);
            //print_r($_SESSION['user_account_details']);
            $this->view_call($main_folder_name, $sub_folder_name, 'Identity_card_details');
          } else {
            $bank_name = $this->input->post('bank_name');
            $bank_branch = $this->input->post('bank_branch');

            $country_id = 149;
            $pincode_no = 629175;
            $place = 'xxxx';
            $district = 'xxxx';
            $state = 'xxxx';
            $full_address = 'xxxxxxxxxxxxxxx';

            $address_obj = $utill_obj->add_address_with_pincode_country($country_id, $pincode_no, $place, $district, $state, $full_address);
            if ($address_obj != null) {
              $bnk_obj = $utill_obj->add_bank($bank_name, $bank_ifsc, $bank_branch, $address_obj);

              $ar = array(
                'account_holder_name' => $_SESSION['user_personal_details']['first_name'],
                // 'account_holder_name'  =>      $account_holder_name,
                'account_no' => $account_no,
                'account_type' => $account_type,
                'bank_ifsc' => $bank_ifsc,
                'bank_obj' => serialize($bnk_obj),
              );
              $this->session->set_userdata('user_account_details', $ar);
              $this->view_call($main_folder_name, $sub_folder_name, 'Identity_card_details');
            }

          }

        } else {
          $this->session->set_flashdata('error', '<p>Account Already Exists</p>');
          $this->view_call($main_folder_name, $sub_folder_name, $file_name);
        }

      } else {
        //print_r($_POST);echo"yy";exit();
        $this->session->set_flashdata('error', '<p>Error in Fields</p>');
        $this->view_call($main_folder_name, $sub_folder_name, $file_name);
      }
    } else { $this->view_call($main_folder_name, $sub_folder_name, $file_name);}
  }
  public function add_identity_card_details() {
    $this->load->helper('string');
    // print_r($_POST);
    //print_r($_FILES);exit();
    $main_folder_name = 'registration';
    $sub_folder_name = 'user';
    $file_name = 'Identity_card_details';
    $utill_obj = new Common_utill();
    if ($this->input->post('pan_no') != null || $this->input->post('id_page') != null) {
      if ($this->input->post('skip') == null) {
        $this->form_validation->set_rules('pan_no', 'Pan', 'trim|required|regex_match[/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/]|min_length[10]|max_length[15]', array('regex_match' => 'Enter Valid Pan Number', 'min_length' => 'Minimumm length 4', 'max_length' => 'Maximum Length 8', 'required' => 'Required'));
        $this->form_validation->set_rules('aadhar_no', 'Aadhar', 'trim|required|regex_match[/^[0-9]+$/]|min_length[12]|max_length[12]', array('regex_match' => 'Numbers Only Allowed', 'min_length' => 'Enter Valid Aadhar', 'max_length' => 'Enter Valid Aadhar', 'required' => 'Required'));
        $this->form_validation->set_rules('pass_no', 'Passbook Number', 'trim|required|regex_match[/^[0-9]+$/]|min_length[5]|max_length[18]', array('regex_match' => 'Numbers Only Allowed', 'min_length' => 'Minimum 5 number', 'max_length' => 'Maximum 18 number', 'required' => 'Required'));
        if ($this->form_validation->run() != false) {

          $pan_no = $this->input->post('pan_no');
          $aadhar_no = $this->input->post('aadhar_no');
          $pass_no = $this->input->post('pass_no');

          if ($this->input->post('pan_verified') != null) {$pan_verified = 1;} else { $pan_verified = 0;}
          if ($this->input->post('aadhar_verified') != null) {$aadhar_verified = 1;} else { $aadhar_verified = 0;}
          if ($this->input->post('pass_verified') != null) {$pass_verified = 1;} else { $pass_verified = 0;}
          if ($this->input->post('kyc_status') != null) {$kyc_status = 1;} else { $kyc_status = 0;}

          $pan_avail = $utill_obj->check_availability('User', 'pan_no', $pan_no);
          $aadhar_avail = $utill_obj->check_availability('User', 'aadhar_no', $aadhar_no);
          $upload_dir = 'attachments';

          if ($pan_avail == 1) {
            if ($aadhar_avail == 1) {
              if ($_FILES['pan_front']['tmp_name'] != null) {
                if ($_FILES['pan_front']['type'] == 'image/png') {
                  $pan_front_url = $utill_obj->add_id_single_image($_FILES['pan_front']);
                } else {
                  $file_name = 'user_' . random_string('alnum', 8);
                  $pan_front_url = $utill_obj->upload_single_compressed_image($_FILES['pan_front'], $upload_dir, $file_name);
                }
              } else { $pan_front_url = "";}

              if ($_FILES['pan_back']['tmp_name'] != null) {
                if ($_FILES['pan_back']['type'] == 'image/png') {
                  $pan_back_url = $utill_obj->add_id_single_image($_FILES['pan_back']);
                } else {
                  $file_name = 'user_' . random_string('alnum', 8);
                  $pan_back_url = $utill_obj->upload_single_compressed_image($_FILES['pan_back'], $upload_dir, $file_name);
                }

              } else { $pan_back_url = "";}

              if ($_FILES['aadhar_front']['tmp_name'] != null) {
                if ($_FILES['aadhar_front']['type'] == 'image/png') {
                  $aadhar_front_url = $utill_obj->add_id_single_image($_FILES['aadhar_front']);
                } else {
                  $file_name = 'user_' . random_string('alnum', 8);
                  $aadhar_front_url = $utill_obj->upload_single_compressed_image($_FILES['aadhar_front'], $upload_dir, $file_name);
                }

              } else { $aadhar_front_url = "";}

              if ($_FILES['aadhar_back']['tmp_name'] != null) {
                if ($_FILES['aadhar_back']['type'] == 'image/png') {
                  $aadhar_back_url = $utill_obj->add_id_single_image($_FILES['aadhar_back']);
                } else {
                  $file_name = 'user_' . random_string('alnum', 8);
                  $aadhar_back_url = $utill_obj->upload_single_compressed_image($_FILES['aadhar_back'], $upload_dir, $file_name);
                }

              } else { $aadhar_back_url = "";}

              if ($_FILES['pass_front']['tmp_name'] != null) {
                if ($_FILES['pass_front']['type'] == 'image/png') {
                  $pass_front_url = $utill_obj->add_id_single_image($_FILES['pass_front']);
                } else {
                  $file_name = 'user_' . random_string('alnum', 8);
                  $pass_front_url = $utill_obj->upload_single_compressed_image($_FILES['pass_front'], $upload_dir, $file_name);
                }

              } else { $pass_front_url = "";}

              if ($_FILES['pass_back']['tmp_name'] != null) {
                if ($_FILES['pass_back']['type'] == 'image/png') {
                  $pass_back_url = $utill_obj->add_id_single_image($_FILES['pass_back']);
                } else {
                  $file_name = 'user_' . random_string('alnum', 8);
                  $pass_back_url = $utill_obj->upload_single_compressed_image($_FILES['pass_back'], $upload_dir, $file_name);
                }

              } else { $pass_back_url = "";}

              $ar = array(
                'pan_no' => $pan_no,
                'aadhar_no' => $aadhar_no,
                'pass_no' => $pass_no,
                'pan_front' => $pan_front_url,
                'pan_back' => $pan_back_url,
                'aadhar_front' => $aadhar_front_url,
                'aadhar_back' => $aadhar_back_url,
                'pass_front' => $pass_front_url,
                'pass_back' => $pass_back_url,
                'pass_verified' => $pass_verified,
                'pan_verified' => $pan_verified,
                'aadhar_verified' => $aadhar_verified,
                'kyc_status' => $kyc_status,
              );
              $this->session->set_userdata('user_id_details', $ar);
              //print_r($_SESSION['user_account_details']);
              $this->view_call($main_folder_name, $sub_folder_name, 'Password');

            } else {
              $this->session->set_flashdata('error', '<p>Aadhar Already Exists</p>');
              $this->view_call($main_folder_name, $sub_folder_name, $file_name);
            }
          } else {
            $this->session->set_flashdata('error', '<p>Pan Already Exists</p>');
            $this->view_call($main_folder_name, $sub_folder_name, $file_name);
          }
        } else {
          $this->session->set_flashdata('error', '<p>Error in Fields</p>');
          $this->view_call($main_folder_name, $sub_folder_name, $file_name);
        }
      } else {
        $ar = array(
          'pan_no' => "",
          'aadhar_no' => "",
          'pass_no' => "",
          'pan_front' => "",
          'pan_back' => "",
          'aadhar_front' => "",
          'aadhar_back' => "",
          'pass_front' => "",
          'pass_back' => "",
          'pass_verified' => "",
          'pan_verified' => "",
          'aadhar_verified' => "",
          'kyc_status' => "",
        );
        $this->session->set_userdata('user_id_details', $ar);
        $this->view_call($main_folder_name, $sub_folder_name, 'Password');
      }
    } else { $this->view_call($main_folder_name, $sub_folder_name, $file_name);}
  }
  public function add_password() {
    $main_folder_name = 'registration';
    $sub_folder_name = 'user';
    $file_name = 'Password';
    $utill_obj = new Common_utill();
    $this->form_validation->set_rules('pwd', 'Password', 'trim|required|callback_find_alphabets|min_length[5]|max_length[20]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]', array('required' => 'Password Required', 'min_length' => 'Minimum 5 Charecters Need', 'max_length' => 'Maximum 20 Charecters are allowed', 'find_alphabets' => 'Albhabets Required'));
    $this->form_validation->set_rules('r_password', 'Password', 'trim|required|matches[pwd]', array('matches' => 'Password Does not Match'));
    if ($this->form_validation->run() != false) {
      $password = $this->input->post('pwd');
      $pin_store_password = $this->input->post('pin_store_password');
      $r_password = $this->input->post('r_password');
      $created_at = $this->input->post('created_at');
      $ar = array(
        'password' => $password,
        'pin_store_password' => $pin_store_password,
        'created_at' => $created_at,
      );
      $this->session->set_userdata('user_password_details', $ar);

      $created_at = $this->input->post('created_at');
      $ar = $_SESSION['user_purchase_obj'];
      array_push($ar, array('created_at' => $created_at));
      $this->session->set_userdata('user_purchase_obj', $ar);

      $this->save_details();
    } else {
      $this->session->set_flashdata('error', '<p>Error in Fields</p>');
      $this->view_call($main_folder_name, $sub_folder_name, $file_name);
    }
  }
  public function save_details() {
    $em = $this->doctrine->em;
    $data['pincode_list'] = $em->getRepository("Entity\Pincode")->findAll();
    $main_folder_name = 'registration';
    $sub_folder_name = 'user';
    $utill_obj = new Common_utill();
    $em->getConnection()->beginTransaction();

    // Add Contact details Start
    $contact_refer_type = 1;
    $contact_refer = null;
    $contact_name = $_SESSION['user_personal_details']['first_name'];
    $contact_desig = null;
    $mobile_primary = $_SESSION['user_personal_details']['mobile'];
    $mobile_secondary = $_SESSION['user_personal_details']['alter_mobile'];
    $email = $_SESSION['user_personal_details']['user_email'];
    $landline = null;
    $contact_obj = $utill_obj->add_contact($contact_refer_type, $contact_refer, $contact_name, $contact_desig, $mobile_primary, $mobile_secondary, $email, $landline);

    if ($contact_obj != null) {
      $n_name = $_SESSION['user_personal_details']['n_name'];
      $n_dob = $_SESSION['user_personal_details']['n_dob'];
      $n_relationship = $_SESSION['user_personal_details']['n_relationship'];
      if ($n_relationship == null) {
        $n_relationship = 'brother';
      }
      $n_phoneno = $_SESSION['user_personal_details']['n_phoneno'];
      $n_gender = $_SESSION['user_personal_details']['n_gender'];

      $nominee_obj = $utill_obj->add_nominee($n_name, $n_dob, $n_relationship, $n_phoneno, $n_gender);

      if ($nominee_obj != null) {

        $password = $_SESSION['user_password_details']['password'];
        $pin_store_password = $_SESSION['user_password_details']['pin_store_password'];
        $created_at = $_SESSION['user_password_details']['created_at'];

        $salt = $password;
        $question1 = "";
        $answer1 = "";
        $question2 = "";
        $answer2 = "";
        $question3 = "";
        $answer3 = "";

        $password_obj = $utill_obj->create_password($password, $salt, $password_prev = null, $password_request_reset_password = 0, $password_req_date = null, $forget_password_url = null, $forget_password_validity = null, $question1, $answer1, $question2, $answer2, $question3, $answer3);

        if ($password_obj != null) {

          $full_address = $_SESSION['user_personal_details']['full_address'];
          $pincode = $_SESSION['user_personal_details']['pincode_no'];
          $find_pincode = $utill_obj->get_single_row_using_array_value('Pincode', 'pincode_no', $pincode);
          $address_obj = $utill_obj->add_add($full_address, $find_pincode);

          if ($address_obj != null) {

            $user_name = $_SESSION['user_personal_details']['user_name'];
            $user_email = $_SESSION['user_personal_details']['user_email'];
            $contact_id = $contact_obj;
            $password_id = $password_obj;
            $address_id = $address_obj;
            $first_name = $_SESSION['user_personal_details']['first_name'];
            $last_name = $_SESSION['user_personal_details']['last_name'];
            $gender = $_SESSION['user_personal_details']['gender'];
            $dob = $_SESSION['user_personal_details']['dob'];
            $profile_picture_url = "xxxx";
            $user_role_id = $em->getReference('Entity\User_role', 8);
            $user_verification_url = $utill_obj->create_verification_url();
            $user_email_verified = 0;
            $is_active = 1;
            $last_login_time = null;
            $last_login_ip_address = 0;
            $invalid_login_attempts = 0;
            $last_invalid_login_time = null;
            $unlocked_reason = "";
            $images = $_SESSION['user_personal_details']['image'];

            $aadhar_no = $_SESSION['user_id_details']['aadhar_no'];
            $pan_no = $_SESSION['user_id_details']['pan_no'];

            $pan_verified = 0;
            $aadhar_verified = 0;
            $pass_verified = 0;
            if ($_SESSION['user_id_details']['pan_verified'] != null) {
              $pan_verified = $_SESSION['user_id_details']['pan_verified'];
            }
            if ($_SESSION['user_id_details']['aadhar_verified'] != null) {
              $aadhar_verified = $_SESSION['user_id_details']['aadhar_verified'];
            }
            if ($_SESSION['user_id_details']['pass_verified'] != null) {
              $pass_verified = $_SESSION['user_id_details']['pass_verified'];
            }
            if ($_SESSION['user_id_details']['kyc_status'] != null) {
              $kyc_status = $_SESSION['user_id_details']['kyc_status'];
            } else { $kyc_status = 0;}
            $arr = array(
              'pan_verified' => $pan_verified,
              'aadhar_verified' => $aadhar_verified,
              'pass_verified' => $pass_verified,
              'kyc_status' => $kyc_status,
              'created_at' => $created_at,
            );

            $user_id = $utill_obj->add_user($user_name, $user_email, $contact_id, $address_id, $nominee_obj, $password_id, $first_name, $last_name, $gender, $dob, $profile_picture_url, $user_role_id, $user_verification_url, $user_email_verified, $is_active, $last_login_time = null, $last_login_ip_address = null, $invalid_login_attempts = null, $last_invalid_login_time = null, $last_invalid_login_ip_address = null, $unlocked_reason = null, $images, $aadhar_no, $pan_no, $pin_store_password, $arr);

            // print_r($user_id);exit();

            $user_obj = $em->getReference('Entity\User', $user_id);

            if ($user_id != -1 || $user_id != '') {
              //print_r($_SESSION['pin_obj']);exit();
              $pin_obj = unserialize($_SESSION['pin_obj']);
              $pin_id = $pin_obj->getPinId();
              $pin_used_user_id = $user_id;
              $pin_status = 0;
              $pin_used_time = new \DateTime(date("Y-m-d H:i:s"));
              $update_pin_obj = $utill_obj->update_pin($pin_id, $pin_used_user_id, $pin_status, $pin_used_time);
              if ($update_pin_obj != null) {

                $ifsc = $_SESSION['user_account_details']['bank_ifsc'];

                $account_holder_name = $_SESSION['user_account_details']['account_holder_name'];
                $account_no = $_SESSION['user_account_details']['account_no'];
                $account_type = $_SESSION['user_account_details']['account_type'];
                $bank_obj = $utill_obj->get_single_row_using_array_value('Bank_details', 'bank_ifsc', $ifsc);
                $referer_id = $user_id;
                $referer_type = 1;

                $account_obj = $utill_obj->add_account($account_holder_name, $account_no, $account_type, $bank_obj, $referer_id, $referer_type);
                if ($account_obj == 1) {

                  $user_account_available_amount = 0;
                  $user_account_re_purchase_amount = 0;
                  $user_account_transfer_per_day_limit_amount = RT::$user_account_transfer_per_day_limit_amount;
                  $user_account_transfer_per_day_limit_transfer = RT::$user_account_transfer_per_day_limit_transfer;
                  $user_account_transfer_per_month_limit_amount = RT::$user_account_transfer_per_month_limit_amount;
                  $user_account_transfer_per_month_limit_transfer = RT::$user_account_transfer_per_month_limit_transfer;
                  $user_account_account_per_day_transfer_enable = 0;
                  $user_account_per_month_transfer_enable = 0;
                  $user_account_is_active = 0;
                  $user_account_locked_reason = 0;
                  $user_account_hold_amount = 0;
                  $user_account_hold_enabled = 0;
                  $user_account_transfer_enabled = 0;

                  $user_account_obj = $utill_obj->add_user_account($user_obj, $user_account_available_amount, $user_account_re_purchase_amount, $user_account_transfer_per_day_limit_amount, $user_account_transfer_per_day_limit_transfer, $user_account_transfer_per_month_limit_amount, $user_account_transfer_per_month_limit_transfer, $user_account_account_per_day_transfer_enable, $user_account_per_month_transfer_enable, $user_account_hold_amount, $user_account_hold_enabled, $user_account_transfer_enabled, $user_account_is_active, $user_account_locked_reason);
                  if ($user_account_obj == true) {

                    // asdasd
                    $pin_id = $update_pin_obj;

                    $transaction_id = $utill_obj->get_reference_obj('Transactions', 1);

                    $up_sp_up_id = $_SESSION['user_purchase_obj']['user_purchase_sponcer_user_purchase_id'];
                    $up_sp_gene_side = $_SESSION['user_purchase_obj']['user_purchase_sponcer_gene_side'];

                    $up_act_gene_up_id = $_SESSION['user_purchase_obj']['user_purchase_actual_gene_user_purchase_id'];
                    $up_act_gene_side = $_SESSION['user_purchase_obj']['user_purchase_actual_gene_side'];
                    $up_gene_left_pid = $_SESSION['user_purchase_obj']['user_purchase_gene_left_purchase_id'];
                    $up_gene_right_pid = $_SESSION['user_purchase_obj']['user_purchase_gene_right_purchase_id'];
                    $up_money_alloted = $_SESSION['user_purchase_obj']['user_purchase_money_alloted'];
                    $up_transaction_obj = $transaction_id;
                    $up_pair_up_id = $_SESSION['user_purchase_obj']['user_pruchase_pair_user_purchase_id'];
                    $achieve = $_SESSION['user_purchase_obj']['achieve'];

                    $created_at = $_SESSION['user_purchase_obj'][0]['created_at'];

                    $up_status = $utill_obj->add_user_package_purchase($user_obj, $up_sp_up_id, $up_sp_gene_side, $pin_id, $up_act_gene_up_id, $up_act_gene_side, $up_gene_left_pid, $up_gene_right_pid, $up_money_alloted, $up_transaction_obj, $up_pair_up_id, $achieve, $created_at);

                    if ($up_status != null) {

                      

                      //print_r($_SESSION['user_id_details']);exit();

                      if ($_SESSION['user_id_details']['pan_front'] != null) {
                        $name = 'pan_front';
                        $utill_obj->add_attachment($name, $_SESSION['user_id_details']['pan_front'], $user_id, 7, $name);
                      }

                      if ($_SESSION['user_id_details']['pan_back'] != null) {
                        $name = 'pan_back';
                        $utill_obj->add_attachment($name, $_SESSION['user_id_details']['pan_back'], $user_id, 7, $name);
                      }

                      if ($_SESSION['user_id_details']['aadhar_front'] != null) {
                        $name = 'aadhar_front';
                        $utill_obj->add_attachment($name, $_SESSION['user_id_details']['aadhar_front'], $user_id, 7, $name);
                      }

                      if ($_SESSION['user_id_details']['aadhar_back'] != null) {
                        $name = 'aadhar_back';
                        $utill_obj->add_attachment($name, $_SESSION['user_id_details']['aadhar_back'], $user_id, 7, $name);
                      }

                      if ($_SESSION['user_id_details']['pass_front'] != null) {
                        $name = 'pass_front';
                        $utill_obj->add_attachment($name, $_SESSION['user_id_details']['pass_front'], $user_id, 7, $name);

                      }

                      if ($_SESSION['user_id_details']['pass_back'] != null) {
                        $name = 'pass_back';
                        $utill_obj->add_attachment($name, $_SESSION['user_id_details']['pass_back'], $user_id, 7, $name);
                      }

                      $em->getConnection()->commit();
                      
                      // MEssage Start
                        //           $sent_message = "Dear ".$first_name.' '.$last_name.' Metrokings Family Welcomes You.Login Id:'.$user_id.',Pwd:'.$password.',Pinstore Pwd:'.$pin_store_password.'.Wish you all success.Visit as www.metrokings.biz';
                        // $mobile_number = $mobile_primary;
                        // $var_message = urlencode($sent_message);
                        // $var_destination = urlencode($mobile_number);
                        // $var_message_type = 2;
                        // $var_senderid = urlencode('METROK');
          
                        //           $ch = curl_init();
                        //           curl_setopt($ch, CURLOPT_URL, "http://sms.dhinatechnologies.com/api/sms_api.php?username=metrokings&api_password=swyvpq558x0&message=$var_message&destination=$var_destination&type=$var_message_type&sender=$var_senderid");
                        //           curl_setopt($ch, CURLOPT_HEADER, 0);  
                        //           curl_exec($ch);
                        //           curl_close($ch);
                                // MEssage Start
                      $this->reverse_tree($user_id);
                      $this->reverse_tree_payout($user_id);
                      $res = $utill_obj->payout_achieve();
                      $res = $utill_obj->payout_left_right_manage();
                      $this->session->set_userdata('pin_obj', "");
                      $this->session->set_userdata('user_purchase_obj', "");
                      $this->session->set_userdata('user_personal_details', "");
                      $this->session->set_userdata('user_id_details', "");
                      $this->session->set_userdata('user_account_details', "");
                      $this->session->set_userdata('user_password_details', "");
                      $this->session->set_flashdata('success', '<p>User Created Successfully</p>');
                      redirect('index.php/package_purchase/tree', 'refresh');
                      // redirect('index.php/Registration','refresh');
                    } else {
                      $em->getConnection()->rollBack();
                      $this->session->set_flashdata('error', '<p>User-Purchase Created Failed</p>');
                      $this->view_call($main_folder_name, $sub_folder_name, 'Pin_enter_page');
                    }
                    // asas
                  } else {
                    $em->getConnection()->rollBack();
                    $this->session->set_flashdata('error', '<p>User Account Created Failed</p>');
                    $this->view_call($main_folder_name, $sub_folder_name, 'Pin_enter_page');
                  }
                } else {
                  $em->getConnection()->rollBack();
                  $this->session->set_flashdata('error', '<p>Account Created Failed</p>');
                  $this->view_call($main_folder_name, $sub_folder_name, 'Pin_enter_page');
                }
              } else {
                $em->getConnection()->rollBack();
                $this->session->set_flashdata('error', '<p>Pin update Failed</p>');
                $this->view_call($main_folder_name, $sub_folder_name, 'Pin_enter_page');
              }
            } else {
              $em->getConnection()->rollBack();
              $this->session->set_flashdata('error', '<p>User Created Failed</p>');
              $this->view_call($main_folder_name, $sub_folder_name, 'Add_user', $data);
            }
          } else {
            $em->getConnection()->rollBack();
            $this->session->set_flashdata('error', '<p>Address Created Failed</p>');
            $this->view_call($main_folder_name, $sub_folder_name, 'Add_user');
          }
        } else {
          $em->getConnection()->rollBack();
          $this->session->set_flashdata('error', '<p>Error in Fields</p>');
          $this->view_call($main_folder_name, $sub_folder_name, 'Password');
        }
      } else {
        $em->getConnection()->rollBack();
        $this->session->set_flashdata('error', '<p>Error in Fields</p>');
        $this->view_call($main_folder_name, $sub_folder_name, 'Add_user');
      }
    } else {
      $em->getConnection()->rollBack();
      $this->session->set_flashdata('error', '<p>Error in Fields</p>');
      $this->view_call($main_folder_name, $sub_folder_name, 'Add_user');
    }
    // Add Contact details End
  }
  public function reverse_tree_payout($id){
    
    $utill_obj = new Common_utill();
    $obj = $utill_obj->get_single_row_using_array_value('User_package_purchase','user_id',$id);
    // $obj = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$user_purchase_id);
    if($obj != null){
      if($obj->getUserPurchaseActualGeneUserPurchaseId() > 0 ){
        $tree_ar = $this->get_r_tree_for_payout($obj->getUserPurchaseActualGeneUserPurchaseId());
        return true;
      }else{return false;}
    }else{return false;}
  }
  public function get_r_tree_for_payout($id){
    $em=$this->doctrine->em;
    $utill_obj = new Common_utill();
    $obj = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$id);
    if($obj != null){
      if($obj->getUserPurchaseActualGeneUserPurchaseId() > 0 ){
        if($obj->getPairAchieve() == 1){
          $obj->setPayoutAch(1);
          $em->persist($obj);
          $em->flush();
          $this->get_r_tree_for_payout($obj->getUserPurchaseActualGeneUserPurchaseId());
          // array_push(self::$reverse_tree_ar,$obj->getUserPurchaseId());
        }else{
          $this->get_r_tree_for_payout($obj->getUserPurchaseActualGeneUserPurchaseId());
        }
      }else{
        if($obj->getPairAchieve() == 1){
          $obj->setPayoutAch(1);
          $em->persist($obj);
          $em->flush();
          // array_push(self::$reverse_tree_ar,$obj->getUserPurchaseId());
        }
      }
    }
    return true;
    // return self::$reverse_tree_ar;
  }
  public function reverse_tree($user_id){
    // $user_purchase_id = 1165;
    $utill_obj = new Common_utill();
    $obj = $utill_obj->get_single_row_using_array_value('User_package_purchase','user_id',$user_id);
    $tree_ar = $this->get_r_tree($obj->getUserPurchaseId(),$obj->getUserPurchaseId());
    return true;
  }
  public function get_r_tree($id,$or_up_id){
    $em=$this->doctrine->em;
    $utill_obj = new Common_utill();
    $obj = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$id);
    if($obj != null){
      if($obj->getLevelIncomeAchieve() == 0){
        $bbj = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$or_up_id);
        $user_id = $obj->getUserId();
        if($obj->getUserPurchasePinId()->getPinRequestPackageId() != null){
          $amount = $obj->getUserPurchasePinId()->getPinRequestPackageId()->getLevelIncomeAmount();
          $achieve_user_id = $bbj->getUserId();
          $u_id = $obj->getUserId()->getUserId();
          if($obj->getUserPurchaseActualGeneUserPurchaseId() == 0){
            $utill_obj->add_level_income($user_id,$amount,$achieve_user_id);

            $query = $em->createQuery("SELECT COUNT(c.income_id) FROM Entity\Level_income as c WHERE c.user_id = $u_id");
            $cnt = $query->getResult();
            if($cnt[0][1] == 10){
              $obj->setLevelIncomeAchieve(1);
              $em->persist($obj);
              $em->flush();
            }
            //array_push(self::$reverse_tree_ar,$obj->getUserPurchaseId());
          }else{
            $utill_obj->add_level_income($user_id,$amount,$achieve_user_id);

            $query = $em->createQuery("SELECT COUNT(c.income_id) FROM Entity\Level_income as c WHERE c.user_id = $u_id");
            $cnt = $query->getResult();
            if($cnt[0][1] == 10){
              $obj->setLevelIncomeAchieve(1);
              $em->persist($obj);
              $em->flush();
            }

            //array_push(self::$reverse_tree_ar,$obj->getUserPurchaseId());
            $this->get_r_tree($obj->getUserPurchaseActualGeneUserPurchaseId(),$or_up_id);
          }
        }
        
      }else{
        $this->get_r_tree($obj->getUserPurchaseActualGeneUserPurchaseId(),$or_up_id);
      }
      return self::$reverse_tree_ar;
    }
  }
  public function view_call($main_folder_name, $sub_folder_name, $file_name, $data = null) {
    $this->load->view('header');
    $this->load->view($main_folder_name . '/' . $sub_folder_name . '/' . $file_name, $data);
    $this->load->view('footer');
  }
  public function get_fname() {
    $uname = $this->input->post('uname');
    $utill_obj = new Common_utill();
    $obj = $utill_obj->get_single_row_using_array_value('User', 'user_name', $uname);
    if ($obj != null) {
      echo $obj->getFirstName();
    } else {echo 0;}
  }
  public function checkDateFormat($date) {
    if (preg_match('/^\d{2}\/\d{2}\/\d{4}$/', $date)) {
      if (checkdate(substr($date, 3, 2), substr($date, 0, 2), substr($date, 6, 4))) {
        return true;
      } else {
        return false;
      }

    } else {
      return false;
    }
  }
  public function alphabets_and_number($str) {
    if (preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str)) {
      return true;
    }
    return false;
  }
  public function find_alphabets($str) {
    if (preg_match('#[a-zA-Z]#', $str)) {
      return true;
    }
    return false;
  }
  public function view_single_user() {
    $utill_obj = new Common_utill();
    $main_folder_name = 'registration';
    $sub_folder_name = 'user';
    $file_name = 'S_view';

    $user_id = 84;
    $data['obj'] = $utill_obj->get_referer_details($user_id);
    $this->view_call($main_folder_name, $sub_folder_name, $file_name, $data);
  }
  public function get_products_from_pack_prd_id() {
    $utill_obj = new Common_utill();
    //$id = 63;
    $id = $this->input->post('id');
    $product_obj = $utill_obj->get_products_from_pack_prd_id($id);
    echo json_encode($product_obj);
  }
  public function ss() {
    //$utill_obj = new Common_utill();
    //$empty_obj = $utill_obj->get_empty_child_nodes();
    //print_r($empty_obj[0]->getUserId());

    echo RT::$user_account_transfer_per_day_limit_amount;

    //$this->load->view('header');
    //$this->load->view('registration/user/Sample');
    //$this->load->view('footer');
  }
  public function sample() {
    $em = $this->doctrine->em;
    $utill_obj = new Common_utill();
    print_r($_FILES);

    // $images = $_FILES['file'];
    // $contact_id = $em->getReference('Entity\Contact',16);
    // $password_id = $em->getReference('Entity\Password',129);
    // $address_id = $em->getReference('Entity\Address',192);

    // $user_name = 'xxxxyyyz123' ;
    // $user_email = 'xxxxyyyz123@mail.com' ;
    // $first_name = 'xxx' ;
    // $last_name = 'xxx' ;
    // $gender=1;
    // $dob =   new \DateTime(date("Y-m-d h:i:s"));
    // $profile_picture_url="xxx";
    // $user_role_id = $em->getReference('Entity\User_role',8);
    // $user_verification_url = "xxx";
    // $user_email_verified = 0;
    // $is_active = 0;

    // echo $utill_obj->add_id_single_image( $_FILES['file']);

    //echo $utill_obj->add_user($user_name,$user_email,$contact_id,$password_id,$address_id,$first_name,$last_name,$gender,$dob,$profile_picture_url,$user_role_id,$user_verification_url,$user_email_verified,$is_active,$last_login_time = null,$last_login_ip_address = null,$invalid_login_attempts = null,$last_invalid_login_time = null,$last_invalid_login_ip_address = null,$unlocked_reason = null,$images);
  }
  public function is_start_date_valid($date) {
    if (date('Y/m/d', strtotime($date)) == $date) {
      return TRUE;
    } else {
      $this->form_validation->set_message('dob', 'The %s Start date must be in format "yyyy-mm-dd"');
      return FALSE;
    }
  }
  public function list_view() {
    $em = $this->doctrine->em;
    $utill_obj = new Common_utill();
    $entity_url = 'Entity\Product';
    //------------------------------
    $page_no = (!isset($_GET['page'])) ? 1 : $_GET['page'];
    if ($page_no <= 0) {
      $page_no = 1;
    }
    $max_item = 3;
    $no_of_links = 1;
    //-----------------------List view contents-------
    $sql = 'Select a from Entity\Product a';
    $options = array('page_no' => $page_no, 'max_item' => $max_item, 'no_of_links' => $no_of_links, 'list_class' => 'list_class', 'active_class' => 'active');
    $page_data = $utill_obj->get_pagination_data($sql, $options);
    $prd_id = array();
    $prd_name = array();
    $prd_short = array();
    $prd_price = array();
    $pagination = array();
    $pck_url = array();
    $common = array();
    foreach ($page_data['p_records'] as $row) {
      array_push($prd_id, $row->getProductId());
      array_push($prd_name, $row->getProductName());
      array_push($prd_short, $row->getProductShortDesc());
      array_push($prd_price, $row->getProductPrice());

      $attachment_object = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_id' => $row->getProductId(), 'attachment_referer_type' => 4));
      if ($attachment_object != null) {
        array_push($pck_url, $attachment_object->getAttachmentImageUrl());
      } else {array_push($pck_url, "");}

    }

    array_push($common, $prd_id);
    array_push($common, $prd_name);
    array_push($common, $prd_short);
    array_push($common, $prd_price);
    array_push($pagination, $page_data['p_links']);
    array_push($common, $pck_url);
    array_push($common, $pagination);

    //print_r($common);
    echo json_encode($common);
  }
  

}

function checkDateFormat($date) {
  if (preg_match('/^\d{2}\/\d{2}\/\d{4}$/', $date)) {
    if (checkdate(substr($date, 3, 2), substr($date, 0, 2), substr($date, 6, 4))) {
      return true;
    } else {
      return false;
    }

  } else {
    return false;
  }
}
?>