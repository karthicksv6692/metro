<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\Query\ResultSetMapping;
require_once 'ssp.customized.class.php';
class User_package_purchase extends CI_Controller {


	public function __construct(){
		parent::__construct();
	}
	public function index(){
	
		$main_folder_name='purchase';$sub_folder_name='purchase';$file_name='Add';
		$this->view_call($main_folder_name,$sub_folder_name,$file_name);
	}
	
	public function add(){
		$main_folder_name='purchase';$sub_folder_name='purchase';$file_name='Add';

		if($this->input->post('pin') !== null){ //print_r($_POST);exit();
				$utill_obj = new Common_utill();
				$em=$this->doctrine->em;
				
				$pin_no = $this->input->post('pin');
				$em->getConnection()->beginTransaction();

				$for = 0;
				$obj = $utill_obj->check_pin_get_details($pin_no,$for);
				if( !is_numeric($obj) && $obj != null){
					$user_obj = $obj->getPinRequestedUserId()->getUserName();
					$req_user_id = $obj->getPinRequestedUserId()->getUserId();

					$req_user_purchase_obj = $utill_obj->get_single_row_using_array_value('Sample_upp','user_purchase_pin_used_user_id',$req_user_id);

					$used_user_id = $this->input->post('pin_used_user_id');

					$left_pair_count = 0;
					$right_pair_count = 0;
					$pin_used_user_obj = $utill_obj->get_reference_obj("User",$used_user_id);
					
					// $sponcer_gene_side = $this->input->post('sponcer_gene_side');
					// $actual_gene_side = $this->input->post('actual_gene_side');

					$side = $this->input->post('side');



					$sponcer_user_puchase_id = $req_user_purchase_obj->getUserPurchaseId();

					// Find Actual Gene Start
					$actual_gene_up_id = 0;
						$actual_obj = $utill_obj->get_node_has_children_sample_app($sponcer_user_puchase_id,$side);
						//echo $actual_obj;exit();
						if($actual_obj != -5){
							if($actual_obj == 1 ){
								$actual_gene_up_id = $sponcer_user_puchase_id;
							}else{
								if($side == 0){
									//Find Left Empty Nodes related to the root_id (sponcer_id)
									echo "left way------------------++";
									$left_ar = $utill_obj->get_left_node_sample_upp($sponcer_user_puchase_id,0);
								// echo $left_ar;exit();
									if($left_ar != -5){
										$actual_gene_up_id = $left_ar;
									}
								}else{
									//Find Right Empty Nodes related to the root_id (sponcer_id)
									echo "right way------------------++";
									// echo $sponcer_user_puchase_id;exit();
									$right_ar = $utill_obj->get_right_node_sample_upp($sponcer_user_puchase_id,1);
									//echo $right_ar;exit();
									if($right_ar != -5){
										$actual_gene_up_id = $right_ar;
									}
								}
							}



							
						}
						//echo $sponcer_user_puchase_id;exit();
					// Find Actual Gene End

					$pack_obj = new Entity\Sample_upp;
					$pack_obj->setUserPurchasePinId($obj);
					$pack_obj->setUserPurchasePinUsedUserId($pin_used_user_obj);
					$pack_obj->setUserPurchaseSponserGeneSide($side);
					$pack_obj->setUserPurchaseSponserUserPurchaseId($req_user_purchase_obj->getUserPurchaseId());
					$pack_obj->setUserPurchaseActualGeneSide($side);
					$pack_obj->setUserPurchaseActualGeneUserPurchaseId($actual_gene_up_id);
					$pack_obj->setUserPurchaseGeneLeftPurchaseId(0);
					$pack_obj->setUserPurchaseGeneRightPurchaseId(0);
					$pack_obj->setLeftPairCount(0);
					$pack_obj->setRightPairCount(0);
					$pack_obj->setTotalCount(0);

					$em->persist($pack_obj);
					try {

						$em->flush();
						$set_sub = $utill_obj->set_sub_node_sample_upp($actual_gene_up_id,$side,$pack_obj->getUserPurchaseId());
						 // echo  $set_sub;exit();
						if( $set_sub == 1){
							$em->getConnection()->commit();
							echo "SUccess";
						}else{
							$em->getConnection()->rollBack();
							echo "Error in Common_utill /set_sub_node_sample_upp/ Funtion";
						}
						
					}catch (UniqueConstraintViolationException $e){ 
						$em->getConnection()->rollBack();
						echo "Catch Errror";
					}
					
				}else{
					$em->getConnection()->rollBack();
					print_r($obj);echo"Errror";
				}
			  	
			
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name);
		}
	}
	public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
		$this->load->view('header');
		$this->load->view(''.$main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
		$this->load->view('footer');
	}

	public function get_left_right_nodes($id){
		$utill_obj = new Common_utill();

		$ar['root']['id'] = array();
		$ar['root']['name'] = array();
		$ar['root']['p_id'] = array();

		$ar['root']['left_count'] = array();
		$ar['root']['right_count'] = array();

		$ar['left']['id'] = array();
		$ar['left']['name'] = array();
		$ar['left']['p_id'] = array();

		$ar['left_left']['id'] = array();
		$ar['left_left']['name'] = array();
		$ar['left_left']['p_id'] = array();

		$ar['left_right']['id'] = array();
		$ar['left_right']['name'] = array();
		$ar['left_right']['p_id'] = array();

		$ar['right']['id'] = array();
		$ar['right']['name'] = array();
		$ar['right']['p_id'] = array();

		$ar['right_right']['id'] = array();
		$ar['right_right']['name'] = array();
		$ar['right_right']['p_id'] = array();

		$ar['right_left']['id'] = array();
		$ar['right_left']['name'] = array();
		$ar['right_left']['p_id'] = array();

		$ret_obj = $utill_obj->get_single_row_using_primary_key('Sample_upp',$id);

		
			$root_obj_user_name = $ret_obj->getUserPurchasePinUsedUserId()->getUserName();
			$root_obj_user_id = $ret_obj->getUserPurchasePinUsedUserId()->getUserId();
			$root_obj_up_id = $ret_obj->getUserPurchaseId();

			$root_left_count = $ret_obj->getLeftPairCount();
			$root_right_count = $ret_obj->getRightPairCount();
		

		$left_id = $ret_obj->getUserPurchaseGeneLeftPurchaseId();
		$right_id = $ret_obj->getUserPurchaseGeneRightPurchaseId();
		
		if($left_id != 0 ){
			$left_obj = $utill_obj->get_single_row_using_primary_key('Sample_upp',$left_id);
			$left_obj_user_name = $left_obj->getUserPurchasePinUsedUserId()->getUserName();
			$left_obj_user_id = $left_obj->getUserPurchasePinUsedUserId()->getUserId();
			$left_obj_up_id = $left_obj->getUserPurchaseId();

			$left_left_id = $left_obj->getUserPurchaseGeneLeftPurchaseId();
			$left_right_id = $left_obj->getUserPurchaseGeneRightPurchaseId();

			// echo $left_id;exit();

			if($left_left_id > 0 ){
				$left_left_obj = $utill_obj->get_single_row_using_primary_key('Sample_upp',$left_left_id);
				$left_left_obj_user_name = $left_left_obj->getUserPurchasePinUsedUserId()->getUserName();
				$left_left_obj_user_id = $left_left_obj->getUserPurchasePinUsedUserId()->getUserId();
				$left_left_obj_up_id = $left_left_obj->getUserPurchaseId();
			}else{
				$left_left_obj_user_name = 0;$left_left_obj_user_id = 0;$left_left_obj_up_id = 0;
			}

			if($left_right_id > 0 ){
				$left_right_obj = $utill_obj->get_single_row_using_primary_key('Sample_upp',$left_right_id);
				$left_right_obj_user_name = $left_right_obj->getUserPurchasePinUsedUserId()->getUserName();
				$left_right_obj_user_id = $left_right_obj->getUserPurchasePinUsedUserId()->getUserId();
				$left_right_obj_up_id = $left_right_obj->getUserPurchaseId();
			}else{
				$left_right_obj_user_name = 0;$left_right_obj_user_id = 0;$left_right_obj_up_id = 0;
			}
		}else{
			$left_obj_user_name = 0;$left_obj_user_id = 0;$left_obj_up_id = 0;
			$left_left_obj_user_name = 0;$left_left_obj_user_id = 0;$left_left_obj_up_id = 0;
			$left_right_obj_user_name = 0;$left_right_obj_user_id = 0;$left_right_obj_up_id = 0;
		}


		if($right_id != 0 ){
			$right_obj = $utill_obj->get_single_row_using_primary_key('Sample_upp',$right_id);
			$right_obj_user_name = $right_obj->getUserPurchasePinUsedUserId()->getUserName();
			$right_obj_user_id = $right_obj->getUserPurchasePinUsedUserId()->getUserId();
			$right_obj_up_id = $right_obj->getUserPurchaseId();

			$right_left_id = $right_obj->getUserPurchaseGeneLeftPurchaseId();
			$right_right_id = $right_obj->getUserPurchaseGeneRightPurchaseId();

			// echo $right_right_id;exit();

			if($right_left_id > 0 ){
				$right_left_obj = $utill_obj->get_single_row_using_primary_key('Sample_upp',$right_left_id);
				$right_left_obj_user_name = $right_left_obj->getUserPurchasePinUsedUserId()->getUserName();
				$right_left_obj_user_id = $right_left_obj->getUserPurchasePinUsedUserId()->getUserId();
				$right_left_obj_up_id = $right_left_obj->getUserPurchaseId();
			}else{
				$right_left_obj_user_name = 0;$right_left_obj_user_id = 0;$right_left_obj_up_id = 0;
			}

			if($right_right_id > 0 ){
				$right_right_obj = $utill_obj->get_single_row_using_primary_key('Sample_upp',$right_right_id);
				$right_right_obj_user_name = $right_right_obj->getUserPurchasePinUsedUserId()->getUserName();
				$right_right_obj_user_id = $right_right_obj->getUserPurchasePinUsedUserId()->getUserId();
				$right_right_obj_up_id = $right_right_obj->getUserPurchaseId();
			}else{
				$right_right_obj_user_name = 0;$right_right_obj_user_id = 0;$right_right_obj_up_id = 0;
			}
		}else{
			$right_obj_user_name = 0;$right_obj_user_id = 0;$right_obj_up_id = 0;
			$right_left_obj_user_name = 0;$right_left_obj_user_id = 0;$right_left_obj_up_id = 0;
			$right_right_obj_user_name = 0;$right_right_obj_user_id = 0;$right_right_obj_up_id = 0;
		}


		array_push($ar['root']['id'], $root_obj_user_id);
		array_push($ar['root']['name'], $root_obj_user_name);
		array_push($ar['root']['p_id'], $root_obj_up_id);

		array_push($ar['root']['left_count'], $root_left_count);
		array_push($ar['root']['right_count'], $root_right_count);

		array_push($ar['left']['id'], $left_obj_user_id);
		array_push($ar['left']['name'], $left_obj_user_name);
		array_push($ar['left']['p_id'], $left_obj_up_id);


		array_push($ar['left_left']['id'], $left_left_obj_user_id);
		array_push($ar['left_left']['name'], $left_left_obj_user_name);
		array_push($ar['left_left']['p_id'], $left_left_obj_up_id);

		array_push($ar['left_right']['id'], $left_right_obj_user_id);
		array_push($ar['left_right']['name'], $left_right_obj_user_name);
		array_push($ar['left_right']['p_id'], $left_right_obj_up_id);

		array_push($ar['right']['id'], $right_obj_user_id);
		array_push($ar['right']['name'], $right_obj_user_name);
		array_push($ar['right']['p_id'], $right_obj_up_id);

		array_push($ar['right_right']['id'], $right_left_obj_user_id);
		array_push($ar['right_right']['name'], $right_left_obj_user_name);
		array_push($ar['right_right']['p_id'], $right_left_obj_up_id);

		array_push($ar['right_left']['id'], $right_right_obj_user_id);
		array_push($ar['right_left']['name'], $right_right_obj_user_name);
		array_push($ar['right_left']['p_id'], $right_right_obj_up_id);

		return $ar;
	}
	public function tree(){
		$main_folder_name='purchase';$sub_folder_name='purchase';$file_name='Tree_view';
		$data['obj'] = $this->get_left_right_nodes(1);
		$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);

		// $ar = $this->get_left_right_nodes(1);
		// print_r($ar);
	}

	public function ajax_tree(){
		$main_folder_name='purchase';$sub_folder_name='purchase';$file_name='Tree_view';
		 echo json_encode($this->get_left_right_nodes($this->input->post('id')));
		//echo $this->input->post('id');
	}

}

?>