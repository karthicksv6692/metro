<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\Query\ResultSetMapping;
require_once 'ssp.customized.class.php';
class Common_controller extends CI_Controller {

	public function ajax_pincode(){
		$pincode = new Entity\Pincode; 
		$em=$this->doctrine->em;
		$single_pincode = $em->getRepository('Entity\Pincode')->findOneBy(array('pincode_no' => $this->input->post('pincode')));
		if($single_pincode != null){
			$pincode_name = $single_pincode->getPincodeNo();
			$pincode_place = $single_pincode->getPincodePlace();
			$state = $single_pincode->getPincodeState();
			$district = $single_pincode->getPincodeDistrict();
			$country = $single_pincode->getPincodeCountryId()->getCountryName();
			$country_code = $single_pincode->getPincodeCountryId()->getCountryCode();
			$country_id = $single_pincode->getPincodeCountryId()->getCountryId();
			$pincode_data = array();
			array_push($pincode_data, array('pincode_no'=>$pincode_name));
			array_push($pincode_data, array('pincode_place'=>$pincode_place));
			array_push($pincode_data, array('pincode_district'=>$district));
			array_push($pincode_data, array('pincode_state'=>$state));
			array_push($pincode_data, array('pincode_country'=>$country));
			array_push($pincode_data, array('pincode_country_code'=>$country_code));
			array_push($pincode_data, array('country_id'=>$country_id));
			echo json_encode($pincode_data);
		}else{echo 0;}
	}
	public function user_name_available(){
		$table_name = "User";
		$field_name = "user_name";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name,$field_name,$field_value);
	}	
	public function user_email_available(){
		$table_name = "User";
		$field_name = "user_email";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name,$field_name,$field_value);
	}
	public function user_mobile_available(){
		$table_name = "Contact";
		$field_name = "contact_mobile_primary";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name,$field_name,$field_value);
	}
	public function country_name_available(){
		$table_name = "Country";
		$field_name = "country_name";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name,$field_name,$field_value);
	}
	public function country_code_available(){
		$table_name = "Country";
		$field_name = "country_code";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name,$field_name,$field_value);
	}
	public function pincode_available(){
		$table_name = "Pincode";
		$field_name = "pincode_no";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name,$field_name,$field_value);
	}
	public function ifsc_available(){
		$table_name = "Bank_details";
		$field_name = "bank_ifsc";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name,$field_name,$field_value);
	}
	public function module_name_available(){
		$table_name = "Module";
		$field_name = "module_name";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name,$field_name,$field_value);
	}
	public function role_name_available(){
		$table_name = "User_role";
		$field_name = "user_role_name";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name,$field_name,$field_value);
	}
	public function vendor_name_available(){
		$table_name = "Vendor";
		$field_name = "vendor_name";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name,$field_name,$field_value);
	}
	public function vendor_tin_no_available(){
		$table_name = "Vendor";
		$field_name = "vendor_tin_no";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name,$field_name,$field_value);
	}
	public function vendor_pan_no_available(){
		$table_name = "Vendor";
		$field_name = "vendor_pan_no";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name,$field_name,$field_value);
	}
	public function vendor_gst_no_available(){
		$table_name = "Vendor";
		$field_name = "vendor_gst_no";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name,$field_name,$field_value);
	}
	public function product_code_available(){
		$table_name = "Product";
		$field_name = "product_code";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name,$field_name,$field_value);
	}
	public function package_product_name_available(){
		$table_name = "Package_products";
		$field_name = "package_product_name";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name,$field_name,$field_value);
	}

	public function get_available_bill_no(){
		$utill = new Common_utill();
		$res = $utill->get_last_record();
		if(!is_integer($res)){
			$val = preg_replace("/[^0-9]/", "", $res);
			$res = $val+1;	
			$rr =  "B".''.$res.'';
			$utill->set_last_record($rr);
			echo $rr;
		}else{
			$new_val =  "B".''.date("Ymdh").''."01";
			$utill->set_last_record($new_val);
			echo $new_val;
		}
	}

	public function country_name_available_on_update(){
		$utill = new Common_utill();
		$table_name = "Country";
		$field_name = "country_name";
		$field_value = $this->input->post('field_value');
		$primary_key = 'country_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name,$field_name,$field_value,$primary_key,$key_value);
	}
	public function country_code_available_on_update(){
		$utill = new Common_utill();
		$table_name = "Country";
		$field_name = "country_code";
		$field_value = $this->input->post('field_value');
		$primary_key = 'country_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name,$field_name,$field_value,$primary_key,$key_value);
	}
	public function pincode_available_on_update(){
		$utill = new Common_utill();
		$table_name = "Pincode";
		$field_name = "pincode_no";
		$field_value = $this->input->post('field_value');
		$primary_key = 'pincode_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name,$field_name,$field_value,$primary_key,$key_value);
	}
	public function ifsc_available_on_update(){
		$utill = new Common_utill();
		$table_name = "Bank_details";
		$field_name = "bank_ifsc";
		$field_value = $this->input->post('field_value');
		$primary_key = 'bank_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name,$field_name,$field_value,$primary_key,$key_value);
	}
	public function module_name_available_on_update(){
		$utill = new Common_utill();
		$table_name = "Module";
		$field_name = "module_name";
		$field_value = $this->input->post('field_value');
		$primary_key = 'module_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name,$field_name,$field_value,$primary_key,$key_value);
	}
	public function user_name_available_on_update(){
		$utill = new Common_utill();
		$table_name = "User";
		$field_name = "user_name";
		$field_value = $this->input->post('field_value');
		$primary_key = 'user_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name,$field_name,$field_value,$primary_key,$key_value);
	}
	public function user_email_available_on_update(){
		$utill = new Common_utill();
		$table_name = "User";
		$field_name = "user_email";
		$field_value = $this->input->post('field_value');
		$primary_key = 'user_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name,$field_name,$field_value,$primary_key,$key_value);
	}
	public function user_mobile_available_on_update(){
		$utill = new Common_utill();
		$table_name = "Contact";
		$field_name = "contact_mobile_primary";
		$field_value = $this->input->post('field_value');
		$primary_key = 'contact_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name,$field_name,$field_value,$primary_key,$key_value);
	}
	public function user_role_name_available_on_update(){
		$utill = new Common_utill();
		$table_name = "User_role";
		$field_name = "user_role_name";
		$field_value = $this->input->post('field_value');
		$primary_key = 'user_role_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name,$field_name,$field_value,$primary_key,$key_value);
	}
	public function vendor_name_available_on_update(){
		$utill = new Common_utill();
		$table_name = "Vendor";
		$field_name = "vendor_name";
		$field_value = $this->input->post('field_value');
		$primary_key = 'vendor_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name,$field_name,$field_value,$primary_key,$key_value);
	}

	public function vendor_tin_no_available_on_update(){
		$utill = new Common_utill();
		$table_name = "Vendor";
		$field_name = "vendor_tin_no";
		$field_value = $this->input->post('field_value');
		$primary_key = 'vendor_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name,$field_name,$field_value,$primary_key,$key_value);
	}

	public function vendor_pan_no_available_on_update(){
		$utill = new Common_utill();
		$table_name = "Vendor";
		$field_name = "vendor_pan_no";
		$field_value = $this->input->post('field_value');
		$primary_key = 'vendor_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name,$field_name,$field_value,$primary_key,$key_value);
	}

	public function vendor_gst_no_available_on_update(){
		$utill = new Common_utill();
		$table_name = "Vendor";
		$field_name = "vendor_gst_no";
		$field_value = $this->input->post('field_value');
		$primary_key = 'vendor_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name,$field_name,$field_value,$primary_key,$key_value);
	}
	public function product_code_available_on_update(){
		$utill = new Common_utill();
		$table_name = "Product";
		$field_name = "product_code";
		$field_value = $this->input->post('field_value');
		$primary_key = 'product_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name,$field_name,$field_value,$primary_key,$key_value);
	}
	public function package_product_name_available_on_update(){
		$utill = new Common_utill();
		$table_name = "Package_products";
		$field_name = "package_product_name";
		$field_value = $this->input->post('field_value');
		$primary_key = 'package_product_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name,$field_name,$field_value,$primary_key,$key_value);
	}


	/*-------------jalin added fns*/
	public function package_name_available(){
		$table_name = "Packages";
		$field_name = "package_name";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name,$field_name,$field_value);
	}
	public function package_name_available_on_update(){
		$utill = new Common_utill();
		$table_name = "Packages";
		$field_name = "package_name";
		$field_value = $this->input->post('field_value');
		$primary_key = 'package_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name,$field_name,$field_value,$primary_key,$key_value);
	}


	public function get_products(){
		$utill = new Common_utill();
		$prd = $utill->get_available_product();
		echo json_encode($prd);
	}
	public function get_ifsc(){

		$utill = new Common_utill();
		$bank_ar = $utill->get_ifsc_list($this->input->post('key'));
		
		echo json_encode($bank_ar);
	}

	public function get_all_packages_ajax(){
		$utill = new Common_utill();
		$val = 0;
		if($this->input->post('val') != null){
			$val = $this->input->post('val');
		}
		$pack = $utill->get_packages($val);
		
		echo json_encode($pack);
	}
	public function get_products_specific_vendor(){
		$utill = new Common_utill();
		if($this->input->post('id') != 0){
			$prd = $utill->get_available_product_spc_ven( $this->input->post('id') );
			echo json_encode($prd);
		}else{
			$this->get_products();
		}
	}
	public function get_products_with_price_range(){
		$utill = new Common_utill();
		if($this->input->post('range') != 0){

		}else{
			$this->get_products();
		}
	}
	public function get_bank_details_from_bank(){
		$id = $this->input->post('id');
		$utill = new Common_utill();
		$bank_ar = $utill->get_single_row_using_primary_key('Bank_details',$id);
		$ar = array('bank_name'=>$bank_ar->getBankName(),'bank_branch'=>$bank_ar->getBankBranch());
		echo json_encode($ar);
	}
	public function get_left_empty_nodes(){
		$utill_obj = new Common_utill();

		$id = $this->input->post('id');
		$side = $this->input->post('side');
		if($side == 0){
			$check = $utill_obj->get_left_empty_node_ajax($id);
			if($check != -5){
				echo json_encode($check);
			}else{echo"lEmpty";}
		}elseif($side == 1){
			$check = $utill_obj->get_right_empty_node_ajax($id);
			if($check != -5){
				echo json_encode($check);
			}else{echo"REmpty";}
		}else{
			$left = $utill_obj->get_left_empty_node_ajax($id);
			$right = $utill_obj->get_right_empty_node_ajax($id);
			$ar = array();
			array_push($ar,$left);
			array_push($ar,$right);
			echo json_encode($ar);
		}
		// $check = $utill_obj->get_node_has_children($id);
		// if($check != -5){
		// 	if($check == 2 || $check == 3 || $check == 4 ){
		// 		$left_ar = $utill_obj->get_left_empty_node_xx($id,0);
		// 		echo json_encode($left_ar);
		// 	}else{
		// 		$left_ar = $utill_obj->get_left_empty_node_xx($id,0);
		// 		echo json_encode($left_ar);
		// 		echo"Left Way";
		// 	}
		// }else{echo"asd";}
	}


	public function test_img_up(){
		$this->load->helper('string');
		$utill = new Common_utill();
		$file=$_FILES['profile_picture'];
	    $upload_url='attachments';
	    if ( ! is_dir($upload_url)) {
	     mkdir($upload_url);
	    }
	    $file_name='user_'.random_string('alnum',8);
	    $upload_data=$utill->upload_single_compressed_image($file,$upload_url,$file_name);

	    if($upload_data != null){
	      return $upload_data[0];
	    }
	}
	public function test_img_up_file(){
		$this->load->view('sample/sample/Img_upload');
	}

  
}?>
