<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

require_once 'ssp.customized.class.php';

class Level_income extends CI_Controller {

  public function __construct() {
    parent::__construct();
  }
  public function index(){
    $main_folder_name='common';$sub_folder_name='income';$file_name='Level_income';
    $this->view_call($main_folder_name,$sub_folder_name,$file_name);
  }
  public function datatable(){
    if($_GET['dt1'] != null || $_GET['user_name'] || $_GET['user_id']){
      if($_GET['dt1'] != null){
        $dt1 = $_GET['dt1'];
        if($_GET['dt2'] != null){
          $dt2 = $_GET['dt2'];
        }else{$dt2 = '';}
      }else{$dt1='';}


      if($_GET['user_id'] != null || $_GET['user_name'] != null){
        if($_GET['user_id'] != null && $_GET['user_name'] == null){
          if($dt1 != null){
            $u_con = " AND `l`.`user_id` = ".$_GET['user_id'];
          }else{
            $u_con = "  `l`.`user_id` = ".$_GET['user_id'];
          }
        }elseif($_GET['user_id'] == null && $_GET['user_name'] != null){
          if($dt1 != null){
            $u_con = " AND `l`.`user_id` = ".$_GET['user_name'];
          }else{
            $u_con = "  `l`.`user_id` = ".$_GET['user_name'];
          }
        }else{
          if($dt1 != null){
            $u_con = " AND `l`.`user_id` = ".$_GET['user_id'];
          }else{
            $u_con = "  `l`.`user_id` = ".$_GET['user_id'];
          }
        }
      }
      $CI = &get_instance();
      $CI->load->database();
      $sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
      $tot_tds = 0;
      $table = 'Level_income';
      $primaryKey = 'income_id';
      $columns = array(
        array( 'db' => '`us`.`user_id`', 'dt' => 0,'field' => 'user_id'),
        array( 'db' => '`us`.`user_id`', 'dt' => 1,'field' => 'user_id'),
        array( 'db' => '`us`.`user_name`', 'dt' => 2,'field' => 'user_name'),
        array( 'db' => '`us`.`first_name`', 'dt' => 3,'field' => 'first_name',
          'formatter' => function( $d, $row ) {
            return $row['first_name'].' '.$row['last_name'];
          }
        ),
        array( 'db' => '`l`.`amount`',
           'dt' => 4,
           'field' => 'amount',
           'formatter' => function( $d, $row ) {
              $utill_obj = new Common_utill();
              return $utill_obj->get_sum_level_income($row['user_id']);
           }
        ),
        array( 'db' => '`l`.`income_id`',
           'dt' => 5,
           'field' => 'income_id',
           'formatter' => function( $d, $row ) {
              return '<i class="fa fa-search-plus float_left" aria-hidden="true" onclick="get_income_details(`'.$row['user_id'].'`)"></i>
                      ';
          }
        ),
        array( 'db' => '`us`.`last_name`', 'dt' => 6,'field' => 'last_name'),
      );
      $joinQuery = "FROM `Level_income` AS `l`  INNER JOIN `User` as us ON (`us`.`user_id` = `l`.`user_id`)";
      if($dt1 != null && $dt2 != ''){
        $where = "l.created_at BETWEEN '".$dt1."' AND '".$dt2."' GROUP BY l.user_id";
      }elseif($dt1 != null && $dt2 == null){
        $where = "l.created_at LIKE '".$dt1."%'";
      }else{
        $where = " $u_con GROUP BY l.user_id";
      }
    }
    $result = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery,$where);
    $start=$_REQUEST['start']+1;
    $idx=0;
    foreach($result['data'] as &$res){
        $res[0]=(string)$start;
        $start++;
        $idx++;
    }
    echo json_encode($result);
  }
  public function level_income_details(){
    $em = $this->doctrine->em;
    if($this->input->post('id') != null){
      $query = $em->createQuery("SELECT u.user_id,u.user_name,u.first_name,u.last_name,l.amount,l.created_at FROM Entity\Level_income as l INNER JOIN Entity\User as u WITH (u.user_id = l.achieve_user_id) WHERE l.user_id =".$this->input->post('id'));
      $res = $query->getResult();

      if(count($res) > 0){
        $html = '';
          $i=1;
          foreach ($res as $rr) {
            $html .= '<tr>';
            $html .= '<td>'.$i.'</td>';
            $html .= '<td>'.$rr['user_id'].'</td>';
            $html .= '<td>'.$rr['user_name'].'</td>';
            $html .= '<td>'.$rr['first_name'].' '.$rr['last_name'].'</td>';
            $html .= '<td>'.$rr['amount'].'</td>';
            $html .= '<td>'.$rr['created_at']->format('d-m-Y H:i:s').'</td>';
            $i++;
            $html .= '</tr>';
          }
        echo json_encode($html);
      }else{echo 0;}
    }
  }
  public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
    $this->load->view('header');
    $this->load->view(''.$main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
    $this->load->view('footer');
  }
}

?>