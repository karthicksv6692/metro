<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\Query\ResultSetMapping;
require_once 'ssp.customized.class.php';
class User_crud extends CI_Controller {

	public function __construct(){
		parent::__construct();
		is_user_logged_in();
	}
	public function index(){ 
		$this->view();
	}
	public function view(){

		$current_data = array(  'cur_data' => "");
		$this->session->set_userdata($current_data);

		$this->load->view('header');
		$this->load->view('user/user/View');
		$this->load->view('footer');	
	}
	public function datatable_ajax(){
		$CI = &get_instance();
		$CI->load->database();
		$sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
		$table = 'User';
		$primaryKey = 'user_id';
		$columns = array(
        array( 'db' => 'user_id', 'dt' => 0 ),
		    array( 'db' => 'user_name', 'dt' => 1 ),
		    array( 'db' => 'first_name', 'dt' => 2 ),
		    array( 'db' => 'last_name', 'dt' => 3 ),
		    array( 'db' => 'user_email', 'dt' => 4 ),
		    array( 'db' => 'is_active',  'dt' => 5 ,'field' => 'is_active','formatter' => function( $d, $row ){
				    	if($row['is_active'] == 0){
				    		return "Not-Active";
				    	}else{return "Active";}
				    }),
		    array(
			        'db'        => 'user_id',
			        'dt'        => 6,
			        'formatter' => function( $d, $row ) {
		            $utill_obj = new Common_utill();
							// $edit = $utill_obj->has_access('pincode','edit');
							// $delete = $utill_obj->has_access('pincode','delete');
							// $s_view = $utill_obj->has_access('pincode','s_view');
							$edit = "asd";
							$delete = "asd";
							$s_view = "asd";

		            if($s_view != null){
		        		$ss = '<form method="POST" class="icon_edit_form" action="'.base_url().'index.php/user/User_crud/S_view">
                            <input type="hidden" name="user_id" value="'.$d.'">
                            <i class="fa fa-search-plus float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
		        	}else{$ss = '';}

		        	if($edit != null){
		        		$ee = ' <form method="POST" class="icon_edit_form" action="'.base_url().'index.php/user/User_crud/Edit">
                            <input type="hidden" name="user_id" value="'.$d.'">
                            <i class="fa fa-pencil float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
		        	}else{$ee='';}

		        	if($delete != null){
		        		$dd = '<i class="fa fa-trash-o float_left btn_delete but_del" aria-hidden="true" data-value="'.$d.'" ></i>';
		        	}else{$dd='';}

		        	return $ss.$ee.$dd;
	        	}
		    )
    );
    $id = 2;
    $joinQuery = '';
    $where = "user_id != ".$id;
		echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery,$where));
	}
	public function add(){
    // $main_folder_name='user';$sub_folder_name='user';$file_name='Add';
		$main_folder_name='registration';$sub_folder_name='user';$file_name='New_user';
		$em = $this->doctrine->em;
		$data['pincode_list'] = $em->getRepository("Entity\Pincode")->findAll();
		$data['module_list'] = $em->getRepository("Entity\Module")->findAll();
		$data['access_list'] = $em->getRepository("Entity\Access")->findAll();
		$data['role_lists'] = $em->getRepository("Entity\User_role")->findAll();
		$data['country_list'] = $em->getRepository("Entity\Country")->findAll();
		$utill_obj = new Common_utill();
		if($this->input->post('user_name') != null || $this->input->post('usr') != null ){
			//print_r($_POST);exit();
			//print_r($_FILES);exit();
			//User Detail Validation Start
			// $dob = $this->input->post('dob');
			// $dob = explode('-',$dob);
			// $dob = $dob[2].'/'.$dob[1].$dob[0];
			// $dob = implode('/',$dob);
			//echo $dob;
				$this->form_validation->set_rules('user_name', 'User Name','trim|required|callback_find_alphabets|min_length[1]|max_length[30]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]',array('required'=> 'User Name Requierd','min_length' => 'User Name needs Minimum 1 charecters', 'max_length' => 'Maximum 20 Charecters are allowed'));
				$this->form_validation->set_rules('user_email', 'User Mail','trim|required|valid_email',array('required'=>'Email Required','valid_email'=>'Enter Valid EmailId'));
				$this->form_validation->set_rules('first_name', 'First Name','trim|required|callback_find_alphabets|min_length[3]|max_length[20]|regex_match[/^[a-zA-Z._ ,%-]+$/]',array('required'=> 'First Name Requierd','min_length' => 'First Name needs Minimum 3 charecters', 'max_length' => 'Maximum 20 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
				$this->form_validation->set_rules('last_name', 'Last Name','trim|min_length[0]|max_length[20]|regex_match[/^[a-zA-Z._ ,%-]+$/]',array( 'max_length' => 'Maximum 20 Charecters are allowed','find_alphabets'=>'Albhabets Required'));

				$this->form_validation->set_rules('dob', 'DOB', 'trim|required|callback_is_start_date_valid',array('required'=>'Required','is_start_date_valid'=>'Enter Valid Date'));
				$this->form_validation->set_rules('gender', 'Gender','trim|required|regex_match[/^[0-9]+$/]',array('required'=> 'Gender Requierd','regex_match'=>'Enter valid Gender'));
			//User Deatail Validation End
			//User Password Validation Start
				$this->form_validation->set_rules('password', 'Password','trim|required|callback_find_alphabets|min_length[5]|max_length[20]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]',array( 'required'=>'Password Required','min_length' => 'Minimum 5 Charecters Need','max_length' => 'Maximum 20 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
				$this->form_validation->set_rules('r_password', 'Password','trim|required|matches[password]',array('matches'=>'Password Does not Match'));
			//User Password Validation Start
			//User Contact Validation Start
				$this->form_validation->set_rules('mobile', 'Mobile ','trim|required|min_length[10]|max_length[20]|regex_match[/^[[0-9-+  \-]+$/]',array('required'=> 'Mobile Requierd','min_length' => 'Mobile needs Minimum 10 charecters', 'max_length' => 'Maximum 20 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
				$this->form_validation->set_rules('alter_mobile', 'Alter Mobile ','trim|max_length[20]|regex_match[/^[[0-9-+  \-]+$/]',array('required'=> 'Mobile Requierd','min_length' => 'Mobile needs Minimum 10 charecters', 'max_length' => 'Maximum 20 Charecters are allowed'));
				$this->form_validation->set_rules('land_line', 'Landline ','trim|max_length[20]|regex_match[/^[[0-9-+  \-]+$/]',array('required'=> 'Landline Requierd','min_length' => 'Landline needs Minimum 10 charecters', 'max_length' => 'Maximum 20 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			//User Contact Validation End
			//User Address Pincode Validation Start
				$this->form_validation->set_rules('full_address', 'Address','trim|required|callback_find_alphabets|min_length[5]|max_length[255]',array('min_length'=>'Minimum 5 Char Needed','max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
				$this->form_validation->set_rules('pincode_no', 'Pincode','trim|required|regex_match[/^[0-9]+$/]|min_length[4]|max_length[8]',array('regex_match' => 'Numbers Only Allowed','min_length'=>'Minimumm length 4','max_length'=>'Maximum Length 8','required'=>'Required'));
				 $this->form_validation->set_rules('district', 'District','trim|required|callback_find_alphabets|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
				
				 $this->form_validation->set_rules('state', 'State','trim|required|callback_find_alphabets|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
				

			//Nominee Validation Start
				 $this->form_validation->set_rules('n_name', 'Nominee Name','trim|required|callback_find_alphabets|min_length[3]|max_length[20]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]',array('required'=> 'Nominee Name Requierd','min_length' => 'Nominee Name needs Minimum 5 charecters', 'max_length' => 'Maximum 20 Charecters are allowed','find_alphabets'=>'Albhabets Required'));

				 $this->form_validation->set_rules('n_relationship', 'Nominee','trim|required|min_length[3]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]',array('required'=> 'Nominee Name Requierd','min_length' => 'Required'));
				 $this->form_validation->set_rules('n_dob', 'DOB', 'trim|required|callback_is_start_date_valid',array('required'=>'Required','is_start_date_valid'=>'Enter Valid Date'));
				 $this->form_validation->set_rules('n_mob', 'Mobile ','trim|required|min_length[10]|max_length[20]|regex_match[/^[[0-9-+  \-]+$/]',array('required'=> 'Mobile Requierd','min_length' => 'Mobile needs Minimum 10 charecters', 'max_length' => 'Maximum 20 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
				 $this->form_validation->set_rules('n_gender', 'Gender','trim|required|regex_match[/^[0-9]+$/]',array('required'=> 'Gender Requierd','regex_match'=>'Enter valid Gender'));
			//Nominee Validation End

			
			if($this->input->post('skip') == null){

				$this->form_validation->set_rules('pan_no', 'Pan','trim|required|regex_match[/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/]|min_length[10]|max_length[15]',array('regex_match' => 'Enter Valid Pan Number','min_length'=>'Minimumm length 4','max_length'=>'Maximum Length 8','required'=>'Required'));
				$this->form_validation->set_rules('aadhar_no', 'Aadhar','trim|required|regex_match[/^[0-9]+$/]|min_length[12]|max_length[12]',array('regex_match' => 'Numbers Only Allowed','min_length'=>'Enter Valid Aadhar','max_length'=>'Enter Valid Aadhar','required'=>'Required'));
				$this->form_validation->set_rules('pass_no', 'Passbook Number','trim|required|regex_match[/^[0-9]+$/]|min_length[5]|max_length[18]',array('regex_match' => 'Numbers Only Allowed','min_length'=>'Minimum 5 number','max_length'=>'Maximum 18 number','required'=>'Required'));
			}



			if ($this->form_validation->run() != false){ //print_r($_POST);exit();
				if($this->input->post('is_active') != null){ $is_active = 1; }else{$is_active = 0;}

				if($this->input->post('pan_verified') != null){ $pan_verified = 1; }else{$pan_verified = 0;}
				if($this->input->post('aadhar_verified') != null){ $aadhar_verified = 1; }else{$aadhar_verified = 0;}
				if($this->input->post('bank_verified') != null){ $bank_verified = 1; }else{$bank_verified = 0;}


				$user_name = $this->input->post('user_name');
				$user_email = $this->input->post('user_email');
				$first_name = $this->input->post('first_name');
				$last_name = $this->input->post('last_name');
				$gender = $this->input->post('gender');
				$dob = new DateTime($this->input->post('dob'));

				$access_ids = $this->input->post('hid_access_id');
				$role_id = $this->input->post('hid_role_id');
				// if($role_id == 0){
				// 	$role_id = 8;
				// }

				$password = $this->input->post('password');
				$r_password = $this->input->post('r_password');

				$mobile = $this->input->post('mobile');
				$alter_mobile = $this->input->post('alter_mob');
				$land_line = $this->input->post('land_line');
				
				$full_address = $this->input->post('full_address');
				$pincode_no = $this->input->post('pincode_no');
				$district = $this->input->post('district');
				$country = $this->input->post('country_id');
				$place = $this->input->post('place');
				$state = $this->input->post('state');
				$aadhar_no = $this->input->post('aadhar_no');
				$pan_no = $this->input->post('pan_no');


				$n_name = $this->input->post('n_name');
				$n_dob = new DateTime($this->input->post('n_dob'));
				$n_relationship = $this->input->post('n_relationship');
				$n_phoneno = $this->input->post('n_mob');
				$n_gender = $this->input->post('n_gender');

				$em->getConnection()->beginTransaction();

				$address_obj = $utill_obj->add_address_with_pincode_country($country,$pincode_no,$place,$district,$state,$full_address);
				
				if($address_obj != null){
					$salt = md5($password);
					$password = md5($password);
					$password_obj = $utill_obj->create_password($password,$salt,$password_prev=null,$password_request_reset_password=null,$password_req_date=null,$forget_password_url=null,$forget_password_validity=null,$question1=null,$answer1=null,$question2=null,$answer2=null,$question3=null,$answer3=null);

					if($password_obj != null){
						$nominee_obj = $utill_obj->add_nominee($n_name,$n_dob,$n_relationship,$n_phoneno,$n_gender);
						
						if($nominee_obj != null){
							$contact_refer_type = 1;
							$contact_refer = null;
							$contact_name = $first_name;
							$contact_desig = null;
							$mobile_primary = $mobile;
							$mobile_secondary = $alter_mobile;
							$email = $user_email;
							$landline = $land_line;
							$contact_obj = $utill_obj->add_contact($contact_refer_type,$contact_refer,$contact_name,$contact_desig,$mobile_primary,$mobile_secondary,$email,$landline);

							if($contact_obj != null){
								$contact_id = $contact_obj;
								$password_id = $em->getReference('Entity\Password',$password_obj->getPasswordId());
								$address_id = $address_obj;
								$profile_picture_url = 'xxx';

								if($em->getReference('Entity\User_role',$role_id) != null){
									$user_role_id = $em->getReference('Entity\User_role',$role_id);
								}else{
									$user_role_id = null;
								}

								$user_verification_url = $utill_obj->create_verification_url();
								$user_email_verified = 0;
								$last_login_time = null;
								$last_login_ip_address = 0;
								$invalid_login_attempts = 0;
								$last_invalid_login_time = null;
								$last_invalid_login_ip_address = 0;
								$unlocked_reason = 0;

								$user_obj = $utill_obj->add_user($user_name,$user_email,$contact_id,$address_id,$nominee_obj,$password_id,$first_name,$last_name,$gender,$dob,$profile_picture_url,$user_role_id,$user_verification_url,$user_email_verified,$is_active,$last_login_time,$last_login_ip_address,$invalid_login_attempts,$last_invalid_login_time,$last_invalid_login_ip_address,$unlocked_reason,$_FILES['file'],$aadhar_no,$pan_no);

								if($user_obj != null && $user_obj != -1){
									$access = $utill_obj->add_access_ids($user_obj,$access_ids);
									
									if($access == 1){
										$user_org_obj = $em->getReference('Entity\User',$user_obj);
										$user_account_available_amount = 0;
										$user_account_re_purchase_amount = 0;
										$user_account_transfer_per_day_limit_amount = RT::$user_account_transfer_per_day_limit_amount;
										$user_account_transfer_per_day_limit_transfer = RT::$user_account_transfer_per_day_limit_transfer;
										$user_account_transfer_per_month_limit_amount = RT::$user_account_transfer_per_month_limit_amount;
										$user_account_transfer_per_month_limit_transfer = RT::$user_account_transfer_per_month_limit_transfer;
										$user_account_account_per_day_transfer_enable = 0;
										$user_account_per_month_transfer_enable = 0;
										$user_account_is_active = 0;
										$user_account_locked_reason = 0;
										$user_account_hold_amount = 0;
										$user_account_hold_enabled = 0;
										$user_account_transfer_enabled = 0;

										$user_account_obj = $utill_obj->add_user_account($user_org_obj,$user_account_available_amount,$user_account_re_purchase_amount,$user_account_transfer_per_day_limit_amount,$user_account_transfer_per_day_limit_transfer,$user_account_transfer_per_month_limit_amount,$user_account_transfer_per_month_limit_transfer,$user_account_account_per_day_transfer_enable,$user_account_per_month_transfer_enable,$user_account_hold_amount,$user_account_hold_enabled,$user_account_transfer_enabled,$user_account_is_active,$user_account_locked_reason);
										//print_r($_FILES);exit();
										
										if($user_account_obj == true){
											if($this->input->post('skip') == null){

												if($_FILES['pan_front']['error'] != 4){
													if($_FILES['pan_front']['tmp_name'] != null){
														$pan_front_url = $utill_obj->add_id_single_image($_FILES['pan_front']);
														if($pan_front_url != null){
															$utill_obj->add_attachment("pan_front",$pan_front_url,$user_obj,RT::$REF['user_id_proof'],$pan_verified);
														}
													}
												}
												if($_FILES['pan_back']['error'] != 4){
													if($_FILES['pan_back']['tmp_name'] != null){
														$pan_back_url = $utill_obj->add_id_single_image($_FILES['pan_back']);
														if($pan_back_url != null){
															$utill_obj->add_attachment("pan_back",$pan_back_url,$user_obj,RT::$REF['user_id_proof'],$pan_verified);
														}
													}
												}
												if($_FILES['aadhar_front']['error'] != 4){
													if($_FILES['aadhar_front']['tmp_name'] != null){
														$aadhar_front_url = $utill_obj->add_id_single_image($_FILES['aadhar_front']);
														if($aadhar_front_url != null){
															$utill_obj->add_attachment("aadhar_front",$aadhar_front_url,$user_obj,RT::$REF['user_id_proof'],$aadhar_verified);
														}
													}
												}
												if($_FILES['aadhar_back']['error'] != 4){
													if($_FILES['aadhar_back']['tmp_name'] != null){
														$aadhar_back_url = $utill_obj->add_id_single_image($_FILES['aadhar_back']);
														if($aadhar_back_url != null){
															$utill_obj->add_attachment("aadhar_back",$aadhar_back_url,$user_obj,RT::$REF['user_id_proof'],$aadhar_verified);
														}
													}
												}
												if($_FILES['pass_front']['error'] != 4){
													if($_FILES['pass_front']['tmp_name'] != null){
														$pass_front_url = $utill_obj->add_id_single_image($_FILES['pass_front']);
														if($pass_front_url != null){
															$utill_obj->add_attachment("pass_front",$pass_front_url,$user_obj,RT::$REF['user_id_proof'],$bank_verified);
														}
													}
												}
												if($_FILES['pass_front']['error'] != 4){
													if($_FILES['pass_back']['tmp_name'] != null){
														$pass_back_url = $utill_obj->add_id_single_image($_FILES['pass_back']);
														if($pass_back_url != null){
															$utill_obj->add_attachment("pass_back",$pass_back_url,$user_obj,RT::$REF['user_id_proof'],$bank_verified);
														}
													}
												}

												$sent_verification_mail = $utill_obj->send_verification_url_with_url($user_obj,$user_verification_url);
												if($sent_verification_mail == 1){
													$em->getConnection()->commit();
													$this->session->set_flashdata('success', '<p>User Created Successfully</p>');
													redirect('index.php/user/user_crud','refresh');
												}else{
													$this->session->set_flashdata('error', '<p>User Created Failed</p>');
													$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
												}

											}else{
												$sent_verification_mail = $utill_obj->send_verification_url_with_url($user_obj,$user_verification_url);
												if($sent_verification_mail == 1){
													//$em->getConnection()->commit();
													$this->session->set_flashdata('success', '<p>User Created Successfully</p>');
													redirect('index.php/user/user_crud','refresh');
												}else{
													$this->session->set_flashdata('error', '<p>User Created Failed</p>');
													$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
												}
											}
										}else{
											$this->session->set_flashdata('error', '<p>User Accout Created Failed</p>');
											$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
										}

									}else{
										$this->session->set_flashdata('error', '<p>Access Created Failed</p>');
										$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
									}
										
								}elseif($user_obj == -1){
									$this->session->set_flashdata('error', '<p>UserName Already Exists</p>');
									$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
								}else{
									$this->session->set_flashdata('error', '<p>User Created Failed</p>');
									$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
								}
							}else{
								$this->session->set_flashdata('error', '<p>Contact Created Failed</p>');
								$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
							}
						}else{
							$this->session->set_flashdata('error', '<p>Nominee Created Failed</p>');
							$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
						}
					}else{
						$this->session->set_flashdata('error', '<p>Password Created Failed</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					}
				}else{
					$this->session->set_flashdata('error', '<p>Address Created Failed</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}
			}else{
				$this->session->set_flashdata('error', '<p>Error in Fields</p>');
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}
		}else{ $this->view_call($main_folder_name,$sub_folder_name,$file_name,$data); }	
	}
	public function edit(){
		$main_folder_name='user';$sub_folder_name='user';$file_name='Edit';
		$em = $this->doctrine->em;
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('User', 'getUserId', 'getUserName', 'index.php/user/user_crud', 'Edit', 'user_id');
		//$data['pincode_list'] = $em->getRepository("Entity\Pincode")->findAll();
		$data['module_list'] = $em->getRepository("Entity\Module")->findAll();
		$data['access_list'] = $em->getRepository("Entity\Access")->findAll();
		$data['role_lists'] = $em->getRepository("Entity\User_role")->findAll();
		$data['country_list'] = $em->getRepository("Entity\Country")->findAll();
    $data['account_obj'] = $em->getRepository('Entity\Account_details')->findOneBy(array('account_refer_id' => $this->input->post('user_id'), 'account_refer_type'=>1));
		if($this->input->post('user_id') != null){
			$this->form_validation->set_rules('user_id', 'ProductId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$user_id = $this->input->post('user_id');

				$current_data = array(  'cur_data' => $user_id);
				$this->session->set_userdata($current_data);

				$data['attachment']  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $user_id, 'attachment_referer_type'=>1));
				$data['pan_front_attachment']  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $user_id, 'attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_name'=>'pan_front'));
				$data['pan_back_attachment']  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $user_id, 'attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_name'=>'pan_back'));
				$data['aadhar_front_attachment']  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $user_id, 'attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_name'=>'aadhar_front'));
				$data['aadhar_back_attachment']  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $user_id, 'attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_name'=>'aadhar_back'));
				$data['pass_front_attachment']  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $user_id, 'attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_name'=>'pass_front'));
				$data['pass_back_attachment']  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $user_id, 'attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_name'=>'pass_back'));
         $data['account_obj'] = $em->getRepository('Entity\Account_details')->findOneBy(array('account_refer_id' => $this->input->post('user_id'), 'account_refer_type'=>1));
       

				$data['access_ids']  = $em->getRepository('Entity\User_access')->findBy(array('user_access_user_id' => $user_id));

        // $data['obj'] = $utill_obj->get_user_details($user_id);
				$data['obj'] = $utill_obj->get_user_new_details($user_id);
				
				$user_obj  = $em->find('Entity\User',$user_id);
        // $this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				$this->view_call('registration','user','New_edit_user',$data);
			}else{
				redirect('index.php/user/user_crud','refresh');
			}
		}elseif($this->input->post('edit_user_id') != null){ // print_r($_FILES);exit();
			if($_SESSION['cur_data'] == $this->input->post('edit_user_id')){
				$data['obj'] = $utill_obj->get_user_details($this->input->post('edit_user_id'));
				$user_id = $this->input->post('edit_user_id');
				$user_obj  = $em->find('Entity\User',$user_id);
				$address_obj = $user_obj->getAddressId(); 
				if($address_obj != null){
					$pincode_obj = $address_obj->get_Address_pincode_id(); 
					if($pincode_obj != null){
						$country_obj = $pincode_obj->getPincodeCountryId();
					}else{
						$country_obj = null;
					}
				}else{ $pincode_obj = null; $country_obj = null; }
				$data['user_obj'] = $user_obj;
						
				//User Deatail Validation Start
					$this->form_validation->set_rules('edit_user_id', 'ProductId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
					$this->form_validation->set_rules('user_name', 'User Name','trim|required|min_length[1]|max_length[30]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]',array('required'=> 'User Name Requierd','min_length' => 'User Name needs Minimum 1 charecters', 'max_length' => 'Maximum 20 Charecters are allowed'));
					$this->form_validation->set_rules('user_email', 'User Mail','trim|required|valid_email',array('required'=>'Email Required','valid_email'=>'Enter Valid EmailId'));
					$this->form_validation->set_rules('first_name', 'First Name','trim|required|callback_find_alphabets|min_length[3]|max_length[20]|regex_match[/^[a-zA-Z._ ,%-]+$/]',array('required'=> 'First Name Requierd','min_length' => 'First Name needs Minimum 3 charecters', 'max_length' => 'Maximum 20 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
					$this->form_validation->set_rules('last_name', 'Last Name','trim|min_length[0]|max_length[20]|regex_match[/^[a-zA-Z._ ,%-]+$/]',array( 'max_length' => 'Maximum 20 Charecters are allowed','find_alphabets'=>'Albhabets Required'));

					$this->form_validation->set_rules('dob', 'DOB', 'trim|required|callback_is_start_date_valid',array('required'=>'Required','is_start_date_valid'=>'Enter Valid Date'));
					$this->form_validation->set_rules('gender', 'Gender','trim|required|regex_match[/^[0-9]+$/]',array('required'=> 'Gender Requierd','regex_match'=>'Enter valid Gender'));

          $this->form_validation->set_rules('password', 'Password','trim|required|callback_find_alphabets|min_length[5]|max_length[20]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]',array( 'required'=>'Password Required','min_length' => 'Minimum 5 Charecters Need','max_length' => 'Maximum 20 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
          $this->form_validation->set_rules('pin_password', 'Pin Password','trim|required|callback_find_alphabets|min_length[3]|max_length[20]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]',array( 'required'=>'Password Required','min_length' => 'Minimum 5 Charecters Need','max_length' => 'Maximum 20 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
				//User Deatail Validation End
				
				//User Contact Validation Start
					$this->form_validation->set_rules('mobile', 'Mobile ','trim|required|min_length[10]|max_length[20]|regex_match[/^[[0-9-+  \-]+$/]',array('required'=> 'Mobile Requierd','min_length' => 'Mobile needs Minimum 10 charecters', 'max_length' => 'Maximum 20 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
					$this->form_validation->set_rules('alter_mobile', 'Alter Mobile ','trim|max_length[20]|regex_match[/^[[0-9-+  \-]+$/]',array('required'=> 'Mobile Requierd','min_length' => 'Mobile needs Minimum 10 charecters', 'max_length' => 'Maximum 20 Charecters are allowed'));
					$this->form_validation->set_rules('land_line', 'Landline ','trim|max_length[20]|regex_match[/^[[0-9-+  \-]+$/]',array('required'=> 'Landline Requierd','min_length' => 'Landline needs Minimum 10 charecters', 'max_length' => 'Maximum 20 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
				//User Contact Validation End
				//User Address Pincode Validation Start
					$this->form_validation->set_rules('full_address', 'Address','trim|required|callback_find_alphabets|min_length[5]|max_length[255]',array('min_length'=>'Minimum 5 Char Needed','max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
					$this->form_validation->set_rules('pincode_no', 'Pincode','trim|required|regex_match[/^[0-9]+$/]|min_length[4]|max_length[8]',array('regex_match' => 'Numbers Only Allowed','min_length'=>'Minimumm length 4','max_length'=>'Maximum Length 8','required'=>'Required'));
					 $this->form_validation->set_rules('district', 'District','trim|required|callback_find_alphabets|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
					
					 $this->form_validation->set_rules('state', 'State','trim|required|callback_find_alphabets|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
					

				
					//Nominee Validation Start
						 $this->form_validation->set_rules('n_name', 'Nominee Name','trim|required|callback_find_alphabets|min_length[3]|max_length[20]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]',array('required'=> 'Nominee Name Requierd','min_length' => 'Nominee Name needs Minimum 5 charecters', 'max_length' => 'Maximum 20 Charecters are allowed','find_alphabets'=>'Albhabets Required'));

						 $this->form_validation->set_rules('n_relationship', 'Nominee','trim|required|min_length[3]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]',array('required'=> 'Nominee Name Requierd','min_length' => 'Required'));
						 $this->form_validation->set_rules('n_dob', 'DOB', 'trim|required|callback_is_start_date_valid',array('required'=>'Required','is_start_date_valid'=>'Enter Valid Date'));
						 $this->form_validation->set_rules('n_mob', 'Mobile ','trim|required|min_length[10]|max_length[20]|regex_match[/^[[0-9-+  \-]+$/]',array('required'=> 'Mobile Requierd','min_length' => 'Mobile needs Minimum 10 charecters', 'max_length' => 'Maximum 20 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
						 $this->form_validation->set_rules('n_gender', 'Gender','trim|required|regex_match[/^[0-9]+$/]',array('required'=> 'Gender Requierd','regex_match'=>'Enter valid Gender'));
             
             $this->form_validation->set_rules('account_no', 'Account No','trim|required|min_length[5]|max_length[30]|regex_match[/^[0-9]+$/]',array('required'=> 'PackageName Requierd','min_length' => 'AccountHolderName needs Minimum 5 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
					//Nominee Validation End

				
				if($this->input->post('skip') == null){

					$this->form_validation->set_rules('pan_no', 'Pan','trim|required|regex_match[/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/]|min_length[10]|max_length[15]',array('regex_match' => 'Enter Valid Pan Number','min_length'=>'Minimumm length 4','max_length'=>'Maximum Length 8','required'=>'Required'));
					$this->form_validation->set_rules('aadhar_no', 'Aadhar','trim|required|regex_match[/^[0-9]+$/]|min_length[12]|max_length[12]',array('regex_match' => 'Numbers Only Allowed','min_length'=>'Enter Valid Aadhar','max_length'=>'Enter Valid Aadhar','required'=>'Required'));
					$this->form_validation->set_rules('pass_no', 'Passbook Number','trim|required|regex_match[/^[0-9]+$/]|min_length[5]|max_length[18]',array('regex_match' => 'Numbers Only Allowed','min_length'=>'Minimum 5 number','max_length'=>'Maximum 18 number','required'=>'Required'));
				}
				//User Address Pincode Validation End

         $this->form_validation->set_rules('bank_name', 'Bank Name','trim|required|min_length[3]|max_length[255]',array('required'=> 'Bank Name Requierd','min_length' => 'Bank Name needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed'));
        $this->form_validation->set_rules('bank_branch', 'Bank Branch','trim|required|min_length[3]|max_length[255]',array('required'=> 'Bank Branch Requierd','min_length' => 'Bank Name needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed'));

				if ($this->form_validation->run() != false){ 
          $password = $this->input->post('password');
          $pin_password = $this->input->post('pin_password');

          $bank_ifsc = $this->input->post('bank_ifsc');
          $account_no = $this->input->post('account_no');
          $ac_bbj = $em->getRepository('Entity\Account_details')->findOneBy(array('account_refer_id' => $user_id, 'account_refer_type'=>1));
          $chk_ac = 1;
          if($ac_bbj != null){
             $bank_avail = $utill_obj->get_single_row_using_array_value('Bank_details','bank_ifsc',$bank_ifsc);
             if($bank_avail != null){
               $ac_ic = $ac_bbj->setBankId($bank_avail);
             }else{
              $bank_name = $this->input->post('bank_name');
              $bank_branch = $this->input->post('bank_branch');
              
              $country_id = 149;
              $pincode_no = 629175;
              $place = 'xxxx';
              $district = 'xxxx';
              $state = 'xxxx';
              $full_address = 'xxxxxxxxxxxxxxx';

              $address_obj =  $utill_obj->add_address_with_pincode_country($country_id,$pincode_no,$place,$district,$state,$full_address);
              if($address_obj != null){
                $bnk_obj = $utill_obj->add_bank($bank_name,$bank_ifsc,$bank_branch,$address_obj);
                $ac_ic = $ac_bbj->setBankId($bnk_obj);
              }
             }

            $ac_ic = $ac_bbj->getAccountId();
            $chk_ac = $utill_obj->check_availability_on_update('Account_details','account_no',$account_no,'account_id',$ac_ic);
          }
       
          if($chk_ac == 1){
          
  					$em->getConnection()->beginTransaction();
            if($ac_bbj != null){
              $ac_bbj->setAccountNo($account_no);
              $em->flush();
            }

  					if($this->input->post('is_active') != null){ $is_active = 1; }else{$is_active = 0;}
  					if($this->input->post('kyc_status') != null){ $kyc_status = 1; }else{$kyc_status = 0;}


  					if($this->input->post('pan_verified') != null){ $pan_verified = 1; }else{$pan_verified = 0;}
  					if($this->input->post('aadhar_verified') != null){ $aadhar_verified = 1; }else{$aadhar_verified = 0;}
  					if($this->input->post('bank_verified') != null){ $bank_verified = 1; }else{$bank_verified = 0;}
  					$user_id = $this->input->post('edit_user_id');

  					$data['attachment']  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $user_id, 'attachment_referer_type'=>1));
  					$data['access_ids']  = $em->getRepository('Entity\User_access')->findBy(array('user_access_user_id' => $user_id));
  					$data['obj'] = $utill_obj->get_user_details($user_id);

  					$user_name = $this->input->post('user_name');
  					$user_email = $this->input->post('user_email');
  					$first_name = $this->input->post('first_name');
  					$last_name = $this->input->post('last_name');
  					$gender = $this->input->post('gender');
  					$dob = new DateTime($this->input->post('dob'));

  					$access_ids = $this->input->post('hid_access_id');
  					
  					$role_id = $this->input->post('hid_role_id');

  					$question1 = $this->input->post('question1');
  					$question2 = $this->input->post('question2');
  					$question3 = $this->input->post('question3');
  					$answer1 = $this->input->post('answer1');
  					$answer2 = $this->input->post('answer2');
  					$answer3 = $this->input->post('answer3');
  					$img_list = $this->input->post('img_list');

  					$mobile = $this->input->post('mobile');
  					$alter_mobile = $this->input->post('alter_mobile');
  					$land_line = $this->input->post('land_line');
  					
  					$full_address = $this->input->post('full_address');
  					$address_id = $this->input->post('address_id');
  					$contact_id = $this->input->post('contact_id');
  					$password_id = $this->input->post('password_id');
  					$pincode_no = $this->input->post('pincode_no');
  					$district = $this->input->post('district');
  					$country = $this->input->post('country_id');
  					$place = $this->input->post('place');
  					$state = $this->input->post('state');


  					$n_name = $this->input->post('n_name');
  					$n_dob = new DateTime($this->input->post('n_dob'));
  					$n_relationship = $this->input->post('n_relationship');
  					$n_phoneno = $this->input->post('n_mob');
  					$n_gender = $this->input->post('n_gender');


  					$pan_no = $this->input->post('pan_no');
  					$aadhar_no = $this->input->post('aadhar_no');

  					if($user_obj->getNomineeId() != null){
  						$n_id = $user_obj->getNomineeId()->getNomineeId();
  						$utill_obj->update_nominee($n_id,$n_name,$n_dob,$n_relationship,$n_phoneno,$n_gender);
  					}

  					if($address_id != null){
  						$address_obj = $utill_obj->update_address_with_pincode_country($country,$pincode_no,$place,$district,$state,$full_address,$address_id);
  					}else{
  						$address_obj = $utill_obj->add_address_with_pincode_country($country,$pincode_no,$place,$district,$state,$full_address);
  					}
  					if($address_obj != null){
  						$contact_refer_type = 1;
  						$contact_refer = null;
  						$contact_name = $first_name;
  						$contact_desig = null;
  						$mobile_primary = $mobile;
  						$mobile_secondary = $alter_mobile;
  						$email = $user_email;
  						$landline = $land_line;
  						$contact_obj = $utill_obj->update_contact($contact_id,$contact_refer_type,$contact_refer,$contact_refer,$contact_name,$contact_desig,$mobile_primary,$mobile_secondary,$email,$landline);
  						$access_obj = $utill_obj->update_access_ids($user_id,$access_ids);
  						if($contact_obj != null && $access_obj == 1){
  							$contact_id = $contact_obj;
  							$password_id = $em->getReference('Entity\Password',$password_id);
  							$address_id = $address_obj;
  							$profile_picture_url = 'xxx';
  							$user_role_id = $em->getReference('Entity\User_role',$role_id);

  							$user_obj = $em->getReference('Entity\User',$user_id);
  							$user_obj->setIsActive($is_active);
  							$user_obj->setUserName($user_name);
  							$user_obj->setUserEmail($user_email);
  							$user_obj->setContactId($contact_id);
  							$user_obj->setAddressId($address_obj);
  							$user_obj->setPasswordId($password_id);
  							$user_obj->setFirstName($first_name);
  							$user_obj->setLastName($last_name);
  							$user_obj->setGender($gender);
  							$user_obj->setDob($dob);
  							$user_obj->setProfilePictureUrl($profile_picture_url);
  							$user_obj->setUserRoleId($user_role_id);
  							$user_obj->setPanNo($pan_no);
  							$user_obj->setAadharNo($aadhar_no);
  							$user_obj->setKycStatus($kyc_status);
                $user_obj->getPasswordId()->setSalt($password);
                $user_obj->getPasswordId()->setPasswordId($password);
  							$user_obj->setPinStorePassword($pin_password);
  							
    						$user_obj->setPanVerified($pan_verified);
    						$user_obj->setAadharVerified($aadhar_verified);
  							$user_obj->setPassVerified($bank_verified);
  							$em->persist($user_obj);
  							try{  
  								$images = $_FILES['file'] ;
  								$attachment_object = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $user_id, 'attachment_referer_type'=>1));
  								if(count($attachment_object)>0){

  									
  									if($images['error'] != 4){
  										for($i=0;$i<count($attachment_object);$i++){
  											$attachment_exists = $em->find('Entity\Attachments',$attachment_object[$i]->getAttachmentId());
  											$em->remove($attachment_exists);
  											$em->flush();
  										}
  											 //echo"aa";exit();
  										for($i=0;$i<count($images['name']);$i++){
  											$url = $utill_obj->add_single_image($images,$i);
  											$product_short_desc='Primary';
  											$product_name='primary';
  											$attach = $utill_obj->add_attachment($product_name,$url,$user_id,1,$product_short_desc);
  											if($attach == 1){ 

  												if($i == count($images['name'])-1){

  													if($this->input->post('skip') == null){
  														if($this->input->post('pan_front_status') == 1){
  															if($_FILES['pan_front']['error'] != 4){
  																$pan_frnt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pan_front'));

  																if($pan_frnt != null){
  																	$em->remove($pan_frnt);
  																	$em->flush();
  																}
  																if($_FILES['pan_front']['tmp_name'] != null){
  																	$pan_front_url = $utill_obj->add_id_single_image($_FILES['pan_front']);
  																	if($pan_front_url != null){
  																		$utill_obj->add_attachment("pan_front",$pan_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$pan_verified);
  																	}
  																}
  															}elseif($this->input->post('pan_front_xx') == 1){
  																$pan_frnt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pan_front'));

  																if($pan_frnt != null){
  																	$em->remove($pan_frnt);
  																	$em->flush();
  																}
  															}
  														}
  														if($this->input->post('pan_back_status') == 1){
  															if($_FILES['pan_back']['error'] != 4){
  																$pan_bck = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pan_back'));
  																if($pan_bck != null){
  																	$em->remove($pan_bck);
  																	$em->flush();
  																}
  																if($_FILES['pan_back']['tmp_name'] != null){
  																	$pan_front_url = $utill_obj->add_id_single_image($_FILES['pan_back']);
  																	if($pan_front_url != null){
  																		$utill_obj->add_attachment("pan_back",$pan_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$pan_verified);
  																	}
  																}
  															}elseif($this->input->post('pan_back_xx') == 1){
  																$pan_bck = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pan_back'));
  																if($pan_bck != null){
  																	$em->remove($pan_bck);
  																	$em->flush();
  																}
  															}
  														}

  														if($this->input->post('aadhar_front_status') == 1){
  															if($_FILES['aadhar_front']['error'] != 4){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'aadhar_front'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  																if($_FILES['aadhar_front']['tmp_name'] != null){
  																		
  																	$aadhar_front_url = $utill_obj->add_id_single_image($_FILES['aadhar_front']);
  																	if($aadhar_front_url != null){
  																		$utill_obj->add_attachment("aadhar_front",$aadhar_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$aadhar_verified);
  																	}
  																}
  															}elseif($this->input->post('aadhar_front_status_xx') == 1){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'aadhar_front'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  															}
  														}
  														if($this->input->post('aadhar_back_status') == 1){
  															if($_FILES['aadhar_back']['error'] != 4){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'aadhar_back'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  																if($_FILES['aadhar_back']['tmp_name'] != null){
  																	$aadhar_front_url = $utill_obj->add_id_single_image($_FILES['aadhar_back']);
  																	if($aadhar_front_url != null){
  																		$utill_obj->add_attachment("aadhar_back",$aadhar_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$aadhar_verified);
  																	}
  																}
  															}elseif($this->input->post('aadhar_back_status_xx') == 1){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'aadhar_back'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  															}
  														}

  														if($this->input->post('pass_front_status') == 1){
  															if($_FILES['pass_front']['error'] != 4){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pass_front'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  																if($_FILES['pass_front']['tmp_name'] != null){
  																		
  																	$aadhar_front_url = $utill_obj->add_id_single_image($_FILES['pass_front']);
  																	if($aadhar_front_url != null){
  																		$utill_obj->add_attachment("pass_front",$aadhar_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$aadhar_verified);
  																	}
  																}
  															}elseif($this->input->post('pass_front_xx') == 1){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pass_front'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  															}
  														}

  														if($this->input->post('pass_back_status') == 1){
  															if($_FILES['pass_back']['error'] != 4){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pass_back'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  																if($_FILES['pass_back']['tmp_name'] != null){
  																	$aadhar_front_url = $utill_obj->add_id_single_image($_FILES['pass_back']);
  																	if($aadhar_front_url != null){
  																		$utill_obj->add_attachment("pass_back",$aadhar_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$aadhar_verified);
  																	}
  																}
  															}elseif($this->input->post('pass_back_xx') == 1){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pass_back'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  															}
  														}

  														$em->flush();
  														$em->getConnection()->commit();
  														$this->session->set_flashdata('success', '<p>User Updated Successfully</p>');
  														redirect('index.php/user/user_crud','refresh');
  														
  													}else{
  														$em->flush();
  														$em->getConnection()->commit();
  														$this->session->set_flashdata('success', '<p>User Updated Successfully</p>');
  														redirect('index.php/user/user_crud','refresh');
  													}
  												}
  											}else{
  												return false;
  											}
  										}
  									}elseif(explode('~',$img_list)[0] != null){
  										for($i=0;$i<count($attachment_object);$i++){
  											$attachment_exists = $em->find('Entity\Attachments',$attachment_object[$i]->getAttachmentId());
  											$em->remove($attachment_exists);
  											$em->flush();
  										}	
  										// print_r(explode('~',$img_list));exit();
  										$img_array=explode('~',$img_list);
  										for($i=0;$i<count($img_array);$i++){
  											$product_short_desc='Primary';
  											$product_name='primary';
  											$attach = $utill_obj->add_attachment($product_name,$img_array[$i],$user_id,1,$product_short_desc);
  											if($attach == 1){ 
  												if($i == count($img_array)-1){
  													if($this->input->post('skip') == null){
  														if($this->input->post('pan_front_status') == 1){
  															if($_FILES['pan_front']['error'] != 4){
  																$pan_frnt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pan_front'));

  																if($pan_frnt != null){
  																	$em->remove($pan_frnt);
  																	$em->flush();
  																}
  																if($_FILES['pan_front']['tmp_name'] != null){
  																	$pan_front_url = $utill_obj->add_id_single_image($_FILES['pan_front']);
  																	if($pan_front_url != null){
  																		$utill_obj->add_attachment("pan_front",$pan_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$pan_verified);
  																	}
  																}
  															}elseif($this->input->post('pan_front_xx') == 1){
  																$pan_frnt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pan_front'));

  																if($pan_frnt != null){
  																	$em->remove($pan_frnt);
  																	$em->flush();
  																}
  															}
  														}
  														if($this->input->post('pan_back_status') == 1){
  															if($_FILES['pan_back']['error'] != 4){
  																$pan_bck = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pan_back'));
  																if($pan_bck != null){
  																	$em->remove($pan_bck);
  																	$em->flush();
  																}
  																if($_FILES['pan_back']['tmp_name'] != null){
  																	$pan_front_url = $utill_obj->add_id_single_image($_FILES['pan_back']);
  																	if($pan_front_url != null){
  																		$utill_obj->add_attachment("pan_back",$pan_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$pan_verified);
  																	}
  																}
  															}elseif($this->input->post('pan_back_xx') == 1){
  																$pan_bck = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pan_back'));
  																if($pan_bck != null){
  																	$em->remove($pan_bck);
  																	$em->flush();
  																}
  															}
  														}

  														if($this->input->post('aadhar_front_status') == 1){
  															if($_FILES['aadhar_front']['error'] != 4){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'aadhar_front'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  																if($_FILES['aadhar_front']['tmp_name'] != null){
  																		
  																	$aadhar_front_url = $utill_obj->add_id_single_image($_FILES['aadhar_front']);
  																	if($aadhar_front_url != null){
  																		$utill_obj->add_attachment("aadhar_front",$aadhar_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$aadhar_verified);
  																	}
  																}
  															}elseif($this->input->post('aadhar_front_status_xx') == 1){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'aadhar_front'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  															}
  														}
  														if($this->input->post('aadhar_back_status') == 1){
  															if($_FILES['aadhar_back']['error'] != 4){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'aadhar_back'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  																if($_FILES['aadhar_back']['tmp_name'] != null){
  																	$aadhar_front_url = $utill_obj->add_id_single_image($_FILES['aadhar_back']);
  																	if($aadhar_front_url != null){
  																		$utill_obj->add_attachment("aadhar_back",$aadhar_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$aadhar_verified);
  																	}
  																}
  															}elseif($this->input->post('aadhar_back_status_xx') == 1){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'aadhar_back'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  															}
  														}

  														if($this->input->post('pass_front_status') == 1){
  															if($_FILES['pass_front']['error'] != 4){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pass_front'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  																if($_FILES['pass_front']['tmp_name'] != null){
  																		
  																	$aadhar_front_url = $utill_obj->add_id_single_image($_FILES['pass_front']);
  																	if($aadhar_front_url != null){
  																		$utill_obj->add_attachment("pass_front",$aadhar_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$aadhar_verified);
  																	}
  																}
  															}elseif($this->input->post('pass_front_xx') == 1){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pass_front'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  															}
  														}

  														if($this->input->post('pass_back_status') == 1){
  															if($_FILES['pass_back']['error'] != 4){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pass_back'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  																if($_FILES['pass_back']['tmp_name'] != null){
  																	$aadhar_front_url = $utill_obj->add_id_single_image($_FILES['pass_back']);
  																	if($aadhar_front_url != null){
  																		$utill_obj->add_attachment("pass_back",$aadhar_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$aadhar_verified);
  																	}
  																}
  															}elseif($this->input->post('pass_back_xx') == 1){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pass_back'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  															}
  														}

  														$em->flush();
  														$em->getConnection()->commit();
  														$this->session->set_flashdata('success', '<p>User Updated Successfully</p>');
  														redirect('index.php/user/user_crud','refresh');
  													}else{
  														$em->flush();
  														$em->getConnection()->commit();
  														$this->session->set_flashdata('success', '<p>User Updated Successfully</p>');
  														redirect('index.php/user/user_crud','refresh');
  													}
  												}
  											}else{
  												return false;
  											}
  										}
  									}else{
  										// dyna add
  										if($this->input->post('skip') == null){
  														if($this->input->post('pan_front_status') == 1){
  															if($_FILES['pan_front']['error'] != 4){
  																$pan_frnt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pan_front'));

  																if($pan_frnt != null){
  																	$em->remove($pan_frnt);
  																	$em->flush();
  																}
  																if($_FILES['pan_front']['tmp_name'] != null){
  																	$pan_front_url = $utill_obj->add_id_single_image($_FILES['pan_front']);
  																	if($pan_front_url != null){
  																		$utill_obj->add_attachment("pan_front",$pan_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$pan_verified);
  																	}
  																}
  															}elseif($this->input->post('pan_front_xx') == 1){
  																$pan_frnt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pan_front'));

  																if($pan_frnt != null){
  																	$em->remove($pan_frnt);
  																	$em->flush();
  																}
  															}
  														}
  														if($this->input->post('pan_back_status') == 1){
  															if($_FILES['pan_back']['error'] != 4){
  																$pan_bck = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pan_back'));
  																if($pan_bck != null){
  																	$em->remove($pan_bck);
  																	$em->flush();
  																}
  																if($_FILES['pan_back']['tmp_name'] != null){
  																	$pan_front_url = $utill_obj->add_id_single_image($_FILES['pan_back']);
  																	if($pan_front_url != null){
  																		$utill_obj->add_attachment("pan_back",$pan_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$pan_verified);
  																	}
  																}
  															}elseif($this->input->post('pan_back_xx') == 1){
  																$pan_bck = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pan_back'));
  																if($pan_bck != null){
  																	$em->remove($pan_bck);
  																	$em->flush();
  																}
  															}
  														}

  														if($this->input->post('aadhar_front_status') == 1){
  															if($_FILES['aadhar_front']['error'] != 4){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'aadhar_front'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  																if($_FILES['aadhar_front']['tmp_name'] != null){
  																		
  																	$aadhar_front_url = $utill_obj->add_id_single_image($_FILES['aadhar_front']);
  																	if($aadhar_front_url != null){
  																		$utill_obj->add_attachment("aadhar_front",$aadhar_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$aadhar_verified);
  																	}
  																}
  															}elseif($this->input->post('aadhar_front_status_xx') == 1){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'aadhar_front'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  															}
  														}
  														if($this->input->post('aadhar_back_status') == 1){
  															if($_FILES['aadhar_back']['error'] != 4){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'aadhar_back'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  																if($_FILES['aadhar_back']['tmp_name'] != null){
  																	$aadhar_front_url = $utill_obj->add_id_single_image($_FILES['aadhar_back']);
  																	if($aadhar_front_url != null){
  																		$utill_obj->add_attachment("aadhar_back",$aadhar_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$aadhar_verified);
  																	}
  																}
  															}elseif($this->input->post('aadhar_back_status_xx') == 1){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'aadhar_back'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  															}
  														}

  														if($this->input->post('pass_front_status') == 1){
  															if($_FILES['pass_front']['error'] != 4){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pass_front'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  																if($_FILES['pass_front']['tmp_name'] != null){
  																		
  																	$aadhar_front_url = $utill_obj->add_id_single_image($_FILES['pass_front']);
  																	if($aadhar_front_url != null){
  																		$utill_obj->add_attachment("pass_front",$aadhar_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$aadhar_verified);
  																	}
  																}
  															}elseif($this->input->post('pass_front_xx') == 1){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pass_front'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  															}
  														}

  														if($this->input->post('pass_back_status') == 1){
  															if($_FILES['pass_back']['error'] != 4){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pass_back'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  																if($_FILES['pass_back']['tmp_name'] != null){
  																	$aadhar_front_url = $utill_obj->add_id_single_image($_FILES['pass_back']);
  																	if($aadhar_front_url != null){
  																		$utill_obj->add_attachment("pass_back",$aadhar_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$aadhar_verified);
  																	}
  																}
  															}elseif($this->input->post('pass_back_xx') == 1){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pass_back'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  															}
  														}

  														$em->flush();
  														$em->getConnection()->commit();
  														$this->session->set_flashdata('success', '<p>User Updated Successfully</p>');
  														redirect('index.php/user/user_crud','refresh');
  														
  													}else{
  														$em->flush();
  														$em->getConnection()->commit();
  														$this->session->set_flashdata('success', '<p>User Updated Successfully</p>');
  														redirect('index.php/user/user_crud','refresh');
  													}
  										// dyna End
  									}
  								}else{
  									if($images['error'] != 4){ 
  										for($i=0;$i<count($images['name']);$i++){
  											$url = $utill_obj->add_single_image($images,$i);
  											$product_short_desc='Primary';
  											$product_name='primary';
  											$attach = $utill_obj->add_attachment($product_name,$url,$user_id,1,$product_short_desc);
  											if($attach == 1){ 
  												if($i == count($images['name'])-1){
  													if($this->input->post('skip') == null){
  														if($this->input->post('pan_front_status') == 1){
  															if($_FILES['pan_front']['error'] != 4){
  																$pan_frnt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pan_front'));

  																if($pan_frnt != null){
  																	$em->remove($pan_frnt);
  																	$em->flush();
  																}
  																if($_FILES['pan_front']['tmp_name'] != null){
  																	$pan_front_url = $utill_obj->add_id_single_image($_FILES['pan_front']);
  																	if($pan_front_url != null){
  																		$utill_obj->add_attachment("pan_front",$pan_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$pan_verified);
  																	}
  																}
  															}elseif($this->input->post('pan_front_xx') == 1){
  																$pan_frnt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pan_front'));

  																if($pan_frnt != null){
  																	$em->remove($pan_frnt);
  																	$em->flush();
  																}
  															}
  														}
  														if($this->input->post('pan_back_status') == 1){
  															if($_FILES['pan_back']['error'] != 4){
  																$pan_bck = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pan_back'));
  																if($pan_bck != null){
  																	$em->remove($pan_bck);
  																	$em->flush();
  																}
  																if($_FILES['pan_back']['tmp_name'] != null){
  																	$pan_front_url = $utill_obj->add_id_single_image($_FILES['pan_back']);
  																	if($pan_front_url != null){
  																		$utill_obj->add_attachment("pan_back",$pan_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$pan_verified);
  																	}
  																}
  															}elseif($this->input->post('pan_back_xx') == 1){
  																$pan_bck = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pan_back'));
  																if($pan_bck != null){
  																	$em->remove($pan_bck);
  																	$em->flush();
  																}
  															}
  														}

  														if($this->input->post('aadhar_front_status') == 1){
  															if($_FILES['aadhar_front']['error'] != 4){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'aadhar_front'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  																if($_FILES['aadhar_front']['tmp_name'] != null){
  																		
  																	$aadhar_front_url = $utill_obj->add_id_single_image($_FILES['aadhar_front']);
  																	if($aadhar_front_url != null){
  																		$utill_obj->add_attachment("aadhar_front",$aadhar_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$aadhar_verified);
  																	}
  																}
  															}elseif($this->input->post('aadhar_front_status_xx') == 1){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'aadhar_front'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  															}
  														}
  														if($this->input->post('aadhar_back_status') == 1){
  															if($_FILES['aadhar_back']['error'] != 4){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'aadhar_back'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  																if($_FILES['aadhar_back']['tmp_name'] != null){
  																	$aadhar_front_url = $utill_obj->add_id_single_image($_FILES['aadhar_back']);
  																	if($aadhar_front_url != null){
  																		$utill_obj->add_attachment("aadhar_back",$aadhar_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$aadhar_verified);
  																	}
  																}
  															}elseif($this->input->post('aadhar_back_status_xx') == 1){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'aadhar_back'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  															}
  														}

  														if($this->input->post('pass_front_status') == 1){
  															if($_FILES['pass_front']['error'] != 4){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pass_front'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  																if($_FILES['pass_front']['tmp_name'] != null){
  																		
  																	$aadhar_front_url = $utill_obj->add_id_single_image($_FILES['pass_front']);
  																	if($aadhar_front_url != null){
  																		$utill_obj->add_attachment("pass_front",$aadhar_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$aadhar_verified);
  																	}
  																}
  															}elseif($this->input->post('pass_front_xx') == 1){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pass_front'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  															}
  														}

  														if($this->input->post('pass_back_status') == 1){
  															if($_FILES['pass_back']['error'] != 4){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pass_back'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  																if($_FILES['pass_back']['tmp_name'] != null){
  																	$aadhar_front_url = $utill_obj->add_id_single_image($_FILES['pass_back']);
  																	if($aadhar_front_url != null){
  																		$utill_obj->add_attachment("pass_back",$aadhar_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$aadhar_verified);
  																	}
  																}
  															}elseif($this->input->post('pass_back_xx') == 1){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pass_back'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  															}
  														}

  														$em->flush();
  														$em->getConnection()->commit();
  														$this->session->set_flashdata('success', '<p>User Updated Successfully</p>');
  														redirect('index.php/user/user_crud','refresh');
  														
  													}else{
  														$em->flush();
  														$em->getConnection()->commit();
  														$this->session->set_flashdata('success', '<p>User Updated Successfully</p>');
  														redirect('index.php/user/user_crud','refresh');
  													}
  												}
  											}else{
  												return false;
  											}
  										}
  									}elseif(explode('~',$img_list)[0] != null){//echo"ee";exit();
  										$img_array=explode('~',$img_list);
  										for($i=0;$i<count($img_array);$i++){
  											$product_short_desc='Primary';
  											$product_name='primary';
  											$attach = $utill_obj->add_attachment($product_name,$img_array[$i],$user_id,1,$product_short_desc);
  											if($attach == 1){ 
  												if($i == count($img_array)-1){
  													if($this->input->post('skip') == null){

  														if($this->input->post('pan_front_status') == 1){
  															if($_FILES['pan_front']['error'] != 4){
  																$pan_frnt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pan_front'));

  																if($pan_frnt != null){
  																	$em->remove($pan_frnt);
  																	$em->flush();
  																}
  																if($_FILES['pan_front']['tmp_name'] != null){
  																	$pan_front_url = $utill_obj->add_id_single_image($_FILES['pan_front']);
  																	if($pan_front_url != null){
  																		$utill_obj->add_attachment("pan_front",$pan_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$pan_verified);
  																	}
  																}
  															}elseif($this->input->post('pan_front_xx') == 1){
  																$pan_frnt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pan_front'));

  																if($pan_frnt != null){
  																	$em->remove($pan_frnt);
  																	$em->flush();
  																}
  															}
  														}
  														if($this->input->post('pan_back_status') == 1){
  															if($_FILES['pan_back']['error'] != 4){
  																$pan_bck = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pan_back'));
  																if($pan_bck != null){
  																	$em->remove($pan_bck);
  																	$em->flush();
  																}
  																if($_FILES['pan_back']['tmp_name'] != null){
  																	$pan_front_url = $utill_obj->add_id_single_image($_FILES['pan_back']);
  																	if($pan_front_url != null){
  																		$utill_obj->add_attachment("pan_back",$pan_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$pan_verified);
  																	}
  																}
  															}elseif($this->input->post('pan_back_xx') == 1){
  																$pan_bck = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pan_back'));
  																if($pan_bck != null){
  																	$em->remove($pan_bck);
  																	$em->flush();
  																}
  															}
  														}

  														if($this->input->post('aadhar_front_status') == 1){
  															if($_FILES['aadhar_front']['error'] != 4){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'aadhar_front'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  																if($_FILES['aadhar_front']['tmp_name'] != null){
  																		
  																	$aadhar_front_url = $utill_obj->add_id_single_image($_FILES['aadhar_front']);
  																	if($aadhar_front_url != null){
  																		$utill_obj->add_attachment("aadhar_front",$aadhar_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$aadhar_verified);
  																	}
  																}
  															}elseif($this->input->post('aadhar_front_status_xx') == 1){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'aadhar_front'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  															}
  														}
  														if($this->input->post('aadhar_back_status') == 1){
  															if($_FILES['aadhar_back']['error'] != 4){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'aadhar_back'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  																if($_FILES['aadhar_back']['tmp_name'] != null){
  																	$aadhar_front_url = $utill_obj->add_id_single_image($_FILES['aadhar_back']);
  																	if($aadhar_front_url != null){
  																		$utill_obj->add_attachment("aadhar_back",$aadhar_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$aadhar_verified);
  																	}
  																}
  															}elseif($this->input->post('aadhar_back_status_xx') == 1){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'aadhar_back'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  															}
  														}

  														if($this->input->post('pass_front_status') == 1){
  															if($_FILES['pass_front']['error'] != 4){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pass_front'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  																if($_FILES['pass_front']['tmp_name'] != null){
  																		
  																	$aadhar_front_url = $utill_obj->add_id_single_image($_FILES['pass_front']);
  																	if($aadhar_front_url != null){
  																		$utill_obj->add_attachment("pass_front",$aadhar_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$aadhar_verified);
  																	}
  																}
  															}elseif($this->input->post('pass_front_xx') == 1){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pass_front'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  															}
  														}

  														if($this->input->post('pass_back_status') == 1){
  															if($_FILES['pass_back']['error'] != 4){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pass_back'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  																if($_FILES['pass_back']['tmp_name'] != null){
  																	$aadhar_front_url = $utill_obj->add_id_single_image($_FILES['pass_back']);
  																	if($aadhar_front_url != null){
  																		$utill_obj->add_attachment("pass_back",$aadhar_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$aadhar_verified);
  																	}
  																}
  															}elseif($this->input->post('pass_back_xx') == 1){
  																$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pass_back'));
  																if($aadhar_frt != null){
  																	$em->remove($aadhar_frt);
  																	$em->flush();
  																}
  															}
  														}

  														$em->flush();
  														$em->getConnection()->commit();
  														$this->session->set_flashdata('success', '<p>User Updated Successfully</p>');
  														redirect('index.php/user/user_crud','refresh');
  														
  													}else{
  														$em->flush();
  														$em->getConnection()->commit();
  														$this->session->set_flashdata('success', '<p>User Updated Successfully</p>');
  														redirect('index.php/user/user_crud','refresh');
  													}
  												}
  											}else{
  												return false;
  											}
  										}
  									}else{//echo"ff";exit();
  										if($this->input->post('skip') == null){
  											if($_FILES['pan_front']['error'] != 4){
  													$pan_frnt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pan_front'));

  													if($pan_frnt != null){
  														$em->remove($pan_frnt);
  														$em->flush();
  													}
  													if($_FILES['pan_front']['tmp_name'] != null){
  														$pan_front_url = $utill_obj->add_id_single_image($_FILES['pan_front']);
  														if($pan_front_url != null){
  															$utill_obj->add_attachment("pan_front",$pan_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$pan_verified);
  														}
  													}
  												}elseif($this->input->post('pan_front_xx') == 1){
  													$pan_frnt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pan_front'));

  													if($pan_frnt != null){
  														$em->remove($pan_frnt);
  														$em->flush();
  													}
  												}
  											}
  											if($this->input->post('pan_back_status') == 1){
  												if($_FILES['pan_back']['error'] != 4){
  													$pan_bck = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pan_back'));
  													if($pan_bck != null){
  														$em->remove($pan_bck);
  														$em->flush();
  													}
  													if($_FILES['pan_back']['tmp_name'] != null){
  														$pan_front_url = $utill_obj->add_id_single_image($_FILES['pan_back']);
  														if($pan_front_url != null){
  															$utill_obj->add_attachment("pan_back",$pan_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$pan_verified);
  														}
  													}
  												}elseif($this->input->post('pan_back_xx') == 1){
  													$pan_bck = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pan_back'));
  													if($pan_bck != null){
  														$em->remove($pan_bck);
  														$em->flush();
  													}
  												}
  											}

  											if($this->input->post('aadhar_front_status') == 1){
  												if($_FILES['aadhar_front']['error'] != 4){
  													$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'aadhar_front'));
  													if($aadhar_frt != null){
  														$em->remove($aadhar_frt);
  														$em->flush();
  													}
  													if($_FILES['aadhar_front']['tmp_name'] != null){
  															
  														$aadhar_front_url = $utill_obj->add_id_single_image($_FILES['aadhar_front']);
  														if($aadhar_front_url != null){
  															$utill_obj->add_attachment("aadhar_front",$aadhar_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$aadhar_verified);
  														}
  													}
  												}elseif($this->input->post('aadhar_front_status_xx') == 1){
  													$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'aadhar_front'));
  													if($aadhar_frt != null){
  														$em->remove($aadhar_frt);
  														$em->flush();
  													}
  												}
  											}
  											if($this->input->post('aadhar_back_status') == 1){
  												if($_FILES['aadhar_back']['error'] != 4){
  													$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'aadhar_back'));
  													if($aadhar_frt != null){
  														$em->remove($aadhar_frt);
  														$em->flush();
  													}
  													if($_FILES['aadhar_back']['tmp_name'] != null){
  														$aadhar_front_url = $utill_obj->add_id_single_image($_FILES['aadhar_back']);
  														if($aadhar_front_url != null){
  															$utill_obj->add_attachment("aadhar_back",$aadhar_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$aadhar_verified);
  														}
  													}
  												}elseif($this->input->post('aadhar_back_status_xx') == 1){
  													$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'aadhar_back'));
  													if($aadhar_frt != null){
  														$em->remove($aadhar_frt);
  														$em->flush();
  													}
  												}
  											}

  											if($this->input->post('pass_front_status') == 1){
  												if($_FILES['pass_front']['error'] != 4){
  													$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pass_front'));
  													if($aadhar_frt != null){
  														$em->remove($aadhar_frt);
  														$em->flush();
  													}
  													if($_FILES['pass_front']['tmp_name'] != null){
  															
  														$aadhar_front_url = $utill_obj->add_id_single_image($_FILES['pass_front']);
  														if($aadhar_front_url != null){
  															$utill_obj->add_attachment("pass_front",$aadhar_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$aadhar_verified);
  														}
  													}
  												}elseif($this->input->post('pass_front_xx') == 1){
  													$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pass_front'));
  													if($aadhar_frt != null){
  														$em->remove($aadhar_frt);
  														$em->flush();
  													}
  												}
  											}

  											if($this->input->post('pass_back_status') == 1){
  												if($_FILES['pass_back']['error'] != 4){
  													$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pass_back'));
  													if($aadhar_frt != null){
  														$em->remove($aadhar_frt);
  														$em->flush();
  													}
  													if($_FILES['pass_back']['tmp_name'] != null){
  														$aadhar_front_url = $utill_obj->add_id_single_image($_FILES['pass_back']);
  														if($aadhar_front_url != null){
  															$utill_obj->add_attachment("pass_back",$aadhar_front_url,$user_obj->getUserId(),RT::$REF['user_id_proof'],$aadhar_verified);
  														}
  													}
  												}elseif($this->input->post('pass_back_xx') == 1){
  													$aadhar_frt = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_type'=>RT::$REF['user_id_proof'],'attachment_referer_id'=>$user_obj->getUserId(),'attachment_name'=>'pass_back'));
  													if($aadhar_frt != null){
  														$em->remove($aadhar_frt);
  														$em->flush();
  													}
  												}
  											}

  											$em->flush();
  											$em->getConnection()->commit();
  											$this->session->set_flashdata('success', '<p>User Updated Successfully</p>');
  											redirect('index.php/user/user_crud','refresh');
  														
  									}
  								}
  							}catch (UniqueConstraintViolationException $e){
  								$this->session->set_flashdata('error', '<p>User Already Exists</p>');
  								$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
  							}
  						}else{
  							$this->session->set_flashdata('error', '<p>Contact Update Failed</p>');
  							$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
  						}
  					}else{
  						$this->session->set_flashdata('error', '<p>Address Update Failed</p>');
  						$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
  					}
          }else{
            $this->session->set_flashdata('error', '<p>AccountNo Already Exists</p>');
            $this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
          }
				}else{
					$this->session->set_flashdata('error', '<p>Error in Fields</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}
			}else{
				$this->session->set_flashdata('error', '<p>Wrong ID</p>');
				redirect('index.php/user/user_crud','refresh');
				
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function s_view(){
		$main_folder_name='user';$sub_folder_name='user';$file_name='S_view';
		$em = $this->doctrine->em;
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('User', 'getUserId', 'getUserName', 'index.php/user/user_crud', 'S_view', 'user_id');
		$data['module_list'] = $em->getRepository("Entity\Module")->findAll();
		$data['access_list'] = $em->getRepository("Entity\Access")->findAll();
    $data['account_obj'] = $em->getRepository('Entity\Account_details')->findOneBy(array('account_refer_id' => $this->input->post('user_id'), 'account_refer_type'=>1));
		if($this->input->post('user_id') != null){
			$this->form_validation->set_rules('user_id', 'ProductId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$user_id = $this->input->post('user_id');
				$data['attachment']  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $user_id, 'attachment_referer_type'=>1));
				$data['access_ids']  = $em->getRepository('Entity\User_access')->findBy(array('user_access_user_id' => $user_id));
				$data['obj'] = $utill_obj->get_user_details($user_id);
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}else{
				redirect('index.php/user/user_crud','refresh');
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function delete(){
		$main_folder_name='user';$sub_folder_name='user';$file_name='Delete';
		$em = $this->doctrine->em;
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('User', 'getUserId', 'getUserName', 'index.php/user/user_crud', 'Delete', 'user_id');
	
		if($this->input->post('user_id') != null){
			$this->form_validation->set_rules('user_id', 'ProductId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$user_id = $this->input->post('user_id');

				$current_data = array(  'cur_data' => $user_id);
				$this->session->set_userdata($current_data);

				$data['attachment']  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $user_id, 'attachment_referer_type'=>1));
				$data['access_ids']  = $em->getRepository('Entity\User_access')->findBy(array('user_access_user_id' => $user_id));
				$data['obj'] = $utill_obj->get_user_details($user_id);
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}else{
				redirect('index.php/user/user_crud','refresh');
			}
		}elseif($this->input->post('delete_user_id') != null){
			if($_SESSION['cur_data'] == $this->input->post('delete_user_id')){
				$this->form_validation->set_rules('delete_user_id', 'ProductId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
				if ($this->form_validation->run() != false){
					$user_id = $this->input->post('delete_user_id');
					$user_obj = $em->getReference('Entity\User',$user_id);
					if($user_obj != null){
						$attachment_object = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $user_id, 'attachment_referer_type'=>1));
						if(count($attachment_object)>0){ 
							for($i=0;$i<count($attachment_object);$i++){
								$attachment_exists = $em->find('Entity\Attachments',$attachment_object[$i]->getAttachmentId());
								$em->remove($attachment_exists);
								$em->flush();
							}
						}
						$password_id = $user_obj->getPasswordId()->getPasswordId();
						$address_id = $user_obj->getAddressId()->get_Address_id();
						$contact_id = $user_obj->getContactId();
						$nominee_id = $user_obj->getNomineeId();
						$delete_user = $utill_obj->delete_user($user_id,$address_id,$contact_id,$password_id,$nominee_id);
						


						if($delete_user == 1){
							//$em->getConnection()->commit();
							$this->session->set_flashdata('success', '<p>User Deleted Successfully</p>');
							redirect('index.php/user/user_crud','refresh');
						}else{
							$this->session->set_flashdata('error', '<p>User Deleted Failed</p>');
							$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
						}
					}else{
						$this->session->set_flashdata('error', '<p>User not Exists</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					}
				}else{
					redirect('index.php/user/user_crud','refresh');
				}
			}else{
				$this->session->set_flashdata('error', '<p>Wrong Id</p>');
				redirect('index.php/user/user_crud','refresh');
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function ajax_delete(){
		$em = $this->doctrine->em;
		$utill_obj = new Common_utill();
		$this->form_validation->set_rules('delete_user_id', 'ProductId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
		if ($this->form_validation->run() != false){
			//$user_id =83;
			$user_id = $this->input->post('delete_user_id');

			$user_obj = $em->getReference('Entity\User',$user_id);
			if($user_obj != null){
				$attachment_object = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $user_id, 'attachment_referer_type'=>1));

				$id_attachment_object = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $user_id, 'attachment_referer_type'=>RT::$REF['user_id_proof']));

				if(count($attachment_object)>0){ 
					for($i=0;$i<count($attachment_object);$i++){
						$attachment_exists = $em->find('Entity\Attachments',$attachment_object[$i]->getAttachmentId());
						$em->remove($attachment_exists);
						$em->flush();
					}
				}

				if(count($id_attachment_object)>0){ 
					for($i=0;$i<count($id_attachment_object);$i++){
						$attachment_exists = $em->find('Entity\Attachments',$id_attachment_object[$i]->getAttachmentId());
						$em->remove($attachment_exists);
						$em->flush();
					}
				}
				$password_id = $user_obj->getPasswordId()->getPasswordId();
				$address_id = $user_obj->getAddressId()->get_Address_id();
				$contact_id = $user_obj->getContactId();
				$nominee_id = $user_obj->getNomineeId();
				$delete_user = $utill_obj->delete_user($user_id,$address_id,$contact_id,$password_id,$nominee_id);
				if($delete_user == 1){
					$em->getConnection()->commit();
					echo 1;
				}else{
					echo 0;
				}
			}else{
				echo -1;
			}
		}else{
			$this->session->set_flashdata('error', '<p>Wrong Id</p>');
			redirect('index.php/user/user_crud','refresh');
		}
	}
	public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
		$this->load->view('header');
		$this->load->view(''.$main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
		$this->load->view('footer');
	}
	public function checkDateFormat($date) {
	    if (preg_match('/^\d{4}\/\d{2}\/\d{2}$/', $date)) {
	        if(checkdate(substr($date, 3, 2), substr($date, 0, 2), substr($date, 6, 4)))
	            return true;
	        else
	            return false;
	    } else {
	        return false;
	    }
	}
	public function is_start_date_valid($date) {
	    if (date('Y/m/d', strtotime($date)) == $date) {
	        return TRUE;
	    } else {
	        $this->form_validation->set_message('dob', 'The %s Start date must be in format "yyyy-mm-dd"');
	            return FALSE;
	    }
	}
	public function alphabets_and_number($str){
	   if (preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function find_alphabets($str){
	   if (preg_match('#[a-zA-Z]#', $str)) {
	     return true;
	   }
	   return false;
	}
  public function user_ajax_edit_view(){
    $utill_obj =  new Common_utill();
    $user_id = $this->input->post('user_id');
    $data['obj'] = $utill_obj->get_user_new_details($user_id);
    echo json_encode($this->load->view('registration/user/New_edit_user',$data));
  }


}
?>