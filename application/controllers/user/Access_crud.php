<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\Query\ResultSetMapping;
require_once 'ssp.customized.class.php';
class Access_crud extends CI_Controller {

	public function __construct(){
		parent::__construct();
		is_user_logged_in();
	}
	public function index(){ 
		$this->view();
	}
	public function view(){
		$this->load->view('header');
		$this->load->view('user/access/View');
		$this->load->view('footer');	
	}
	public function datatable_ajax(){
		$CI = &get_instance();
		$CI->load->database();
		$sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
		$table = 'Access';
		$primaryKey = 'access_id';
		$columns = array(
		    array( 'db' => '`a`.`access_name`', 'dt' => 0,'field' => 'access_name'),
		    array( 'db' => '`a`.`access_url`', 'dt' => 1,'field' => 'access_url'),
		    array( 'db' => '`p`.`module_name`', 'dt' => 2,'field' => 'module_name'),
		    array( 'db' => '`a`.`access_id`',
			       'dt' => 3,
			       'field' => 'access_id',
			       'formatter' => function( $d, $row ) {
		           $utill_obj = new Common_utill();
							// $edit = $utill_obj->has_access('pincode','edit');
							// $delete = $utill_obj->has_access('pincode','delete');
							// $s_view = $utill_obj->has_access('pincode','s_view');
							$edit = "asd";
							$delete = "asd";
							$s_view = "asd";

		            if($s_view != null){
		        		$ss = '<form method="POST" class="icon_edit_form" action="'.base_url().'index.php/user/Access_crud/S_view">
                            <input type="hidden" name="access_id" value="'.$d.'">
                            <i class="fa fa-search-plus float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
		        	}else{$ss = '';}

		        	if($edit != null){
		        		$ee = ' <form method="POST" class="icon_edit_form" action="'.base_url().'index.php/user/Access_crud/Edit">
                            <input type="hidden" name="access_id" value="'.$d.'">
                            <i class="fa fa-pencil float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
		        	}else{$ee='';}

		        	if($delete != null){
		        		$dd = '<i class="fa fa-trash-o float_left btn_delete but_del" aria-hidden="true" data-value="'.$d.'" ></i>';
		        	}else{$dd='';}

		        	return $ss.$ee.$dd;
	        	}
		    )
		);
		$joinQuery = "FROM `Access` AS `a` INNER JOIN `Module` AS `p` ON (`a`.`module_id` = `p`.`module_id`)";
		echo json_encode(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery));
	}
	public function add(){
		$main_folder_name='user';$sub_folder_name='access';$file_name='Add';
		$em = $this->doctrine->em;
		$data['module_list'] = $em->getRepository("Entity\Module")->findAll();
		if($this->input->post('access_name') != null || $this->input->post('acc') != null){
			// print_r($_POST);
			$this->form_validation->set_rules('access_name', 'Access Name','trim|callback_find_alphabets|required|min_length[3]|max_length[255]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]',array('required'=> 'AccessName Requierd','min_length' => 'AccessName needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			$this->form_validation->set_rules('access_url', 'AccessUrl','trim|callback_find_alphabets|required|min_length[3]|max_length[255]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]',array('required'=> 'AccessUrl Requierd','min_length' => 'AccessUrl needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			$this->form_validation->set_rules('module_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$access_name = $this->input->post('access_name');
				$access_url = $this->input->post('access_url');
				$module_id = $this->input->post('module_id');
				$find_access = $em->getRepository('Entity\Access')->findBy(array('access_name'=>$access_name));
				$find_access_url = $em->getRepository('Entity\Access')->findBy(array('access_url'=>$access_url));
				$find_module = $em->getRepository('Entity\Module')->find($module_id);
				if($find_access == null && $find_access_url == null){
					if($find_module != null){
						$access_obj = new Entity\Access;
						$access_obj->setAccessName($access_name);
						$access_obj->setAccessUrl($access_url);
						$access_obj->setModuleId($find_module);
						$em->persist($access_obj);
						try {
							$em->flush();
							$this->session->set_flashdata('success', '<p>Access Created Successfully</p>');
							redirect('index.php/user/access_crud','refresh');
						}catch (UniqueConstraintViolationException $e){
							$this->session->set_flashdata('error', '<p>Access Already Exists</p>');
							$this->view_call($main_folder_name,$sub_folder_name,$file_name);
						}
					}else{
						$this->session->set_flashdata('error', '<p>Module Not Exists</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					}
				}else{
					$this->session->set_flashdata('error', '<p>AccessName/Url Already Exists</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}
			}else{
				$this->session->set_flashdata('error', '<p>Error in Fields</p>');
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function edit(){
		$main_folder_name='user';$sub_folder_name='access';$file_name='Edit';
		$em = $this->doctrine->em;
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Access', 'getAccessId', 'getAccessName', 'index.php/user/access_crud', 'Edit', 'access_id');
		$data['module_list'] = $em->getRepository("Entity\Module")->findAll();
		if($this->input->post('access_id')){
			$this->form_validation->set_rules('access_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$access_id = $this->input->post('access_id');
				$find_access = $em->getRepository('Entity\Access')->find($access_id); 
				if($find_access != null){
					$data['access_obj'] = $find_access;
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}else{
					$this->session->set_flashdata('error', '<p>Wrong Access</p>');
					redirect('index.php/user/access_crud','refresh');
				}
			}else{
				$this->view_call($main_folder_name,$sub_folder_name,$file_name);
			}
		}elseif($this->input->post('edit_access_id')){
			$this->form_validation->set_rules('edit_access_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			$this->form_validation->set_rules('access_name', 'Access Name','trim|callback_find_alphabets|required|min_length[3]|max_length[255]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]',array('required'=> 'AccessName Requierd','min_length' => 'AccessName needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			$this->form_validation->set_rules('access_url', 'AccessUrl','trim|callback_find_alphabets|required|min_length[3]|max_length[255]|regex_match[/^[a-zA-Z0-9.\/_ ,%-]+$/]',array('required'=> 'AccessUrl Requierd','min_length' => 'AccessUrl needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			$this->form_validation->set_rules('module_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			$data['access_obj'] = $em->getRepository('Entity\Access')->find($this->input->post('edit_access_id')); 
			if ($this->form_validation->run() != false){
				$access_id = $this->input->post('edit_access_id');
				$access_name = $this->input->post('access_name');
				$access_url = $this->input->post('access_url');
				$module_id = $this->input->post('module_id');
				$find_module = $em->getRepository('Entity\Module')->find($module_id);
				$find_access = $em->getRepository('Entity\Access')->find($access_id);
				if($find_module != null){
					$find_access->setAccessName($access_name);
					$find_access->setAccessUrl($access_url);
					$find_access->setModuleId($find_module);
					$em->persist($find_access);
					$data['access_obj'] = $find_access;
					try {
						$em->flush();
						$this->session->set_flashdata('success', '<p>Access Updated Successfully</p>');
						redirect('index.php/user/access_crud','refresh');
					}catch (UniqueConstraintViolationException $e){
						$this->session->set_flashdata('error', '<p>Access Already Exists</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					}
				}else{

				}
			}else{
				$this->session->set_flashdata('error', '<p>Error in Fields</p>');
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function s_view(){
		$main_folder_name='user';$sub_folder_name='access';$file_name='S_view';
		$em = $this->doctrine->em;
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Access', 'getAccessId', 'getAccessName', 'index.php/user/access_crud', 'S_view', 'access_id');
		if($this->input->post('access_id')){
			$this->form_validation->set_rules('access_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$access_id = $this->input->post('access_id');
				$find_access = $em->getRepository('Entity\Access')->find($access_id); 
				if($find_access != null){
					$data['access_obj'] = $find_access;
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}else{
					$this->session->set_flashdata('error', '<p>Wrong Access</p>');
					redirect('index.php/user/access_crud','refresh');
				}
			}else{
				$this->view_call($main_folder_name,$sub_folder_name,$file_name);
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function delete(){
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Access', 'getAccessId', 'getAccessName', 'index.php/user/access_crud', 'Delete', 'access_id');
		$main_folder_name='user';$sub_folder_name='access';$file_name='Delete';
		$em = $this->doctrine->em;
		if($this->input->post('access_id')){
			$this->form_validation->set_rules('access_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$access_id = $this->input->post('access_id');
				$find_access = $em->getRepository('Entity\Access')->find($access_id); 
				if($find_access != null){
					$data['access_obj'] = $find_access;
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}else{
					$this->session->set_flashdata('error', '<p>Wrong Access</p>');
					redirect('index.php/user/access_crud','refresh');
				}
			}else{
				$this->view_call($main_folder_name,$sub_folder_name,$file_name);
			}
		}elseif($this->input->post('delete_access_id')){
			$this->form_validation->set_rules('delete_access_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$access_id = $this->input->post('delete_access_id');
				$find_access = $em->getRepository('Entity\Access')->find($access_id); 
				if($find_access != null){
					$em->remove($find_access);
					$data['access_obj'] = $find_access;
					try {
						$em->flush();
						$this->session->set_flashdata('success', '<p>Access Deleted Successfully</p>');
						redirect('index.php/user/access_crud','refresh');
					}catch (UniqueConstraintViolationException $e){
						$this->session->set_flashdata('error', '<p>Access Not Exists</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					}
				}else{
					$this->session->set_flashdata('error', '<p>Access Not Exists</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name);
				}
			}else{
				$this->session->set_flashdata('error', '<p>Error in Fields</p>');
				$this->view_call($main_folder_name,$sub_folder_name,$file_name);
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function ajax_delete(){
		$em = $this->doctrine->em;
		if($this->input->post('delete_access_id') != null){
			$this->form_validation->set_rules('delete_access_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$access_id = $this->input->post('delete_access_id');
				$find_access = $em->getRepository('Entity\Access')->find($access_id);
				if($find_access != null){
					$em->remove($find_access);
					$em->flush();
					echo 1;
				}else{
					echo -1;
				}
			}else{
				echo 0;
			}
		}else{
			echo 0;
		}
	}
	public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
		$this->load->view('header');
		$this->load->view(''.$main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
		$this->load->view('footer');
	}
	public function alphabets_and_number($str){
	   if (preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function find_alphabets($str){
	   if (preg_match('#[a-zA-Z]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function find_numbers($str){
	   if (preg_match('#[0-9]#', $str)) {
	     return true;
	   }
	   return false;
	}
}
?>