<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\Query\ResultSetMapping;
require_once 'ssp.customized.class.php';
class Module_crud extends CI_Controller {

	public function __construct(){
		parent::__construct();
		is_user_logged_in();
	}
	public function index(){ 
		$this->view();
	}
	public function view(){
		$this->load->view('header');
		$this->load->view('user/module/View');
		$this->load->view('footer');	
	}
	public function datatable_ajax(){
		$CI = &get_instance();
		$CI->load->database();
		$sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
		$table = 'Module';
		$primaryKey = 'module_id';
		$columns = array(
		    array( 'db' => 'module_name', 'dt' => 0 ),
		    array( 'db' => 'module_desc',  'dt' => 1 ),
		    array(
			        'db'        => 'module_id',
			        'dt'        => 2,
			        'formatter' => function( $d, $row ) {

			        		$utill_obj = new Common_utill();
							// $edit = $utill_obj->has_access('pincode','edit');
							// $delete = $utill_obj->has_access('pincode','delete');
							// $s_view = $utill_obj->has_access('pincode','s_view');
							$edit = "asd";
							$delete = "asd";
							$s_view = "asd";

		            if($s_view != null){
		        		$ss = '<form method="POST" class="icon_edit_form" action="'.base_url().'index.php/user/Module_crud/S_view">
                            <input type="hidden" name="module_id" value="'.$d.'">
                            <i class="fa fa-search-plus float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
		        	}else{$ss = '';}

		        	if($edit != null){
		        		$ee = ' <form method="POST" class="icon_edit_form" action="'.base_url().'index.php/user/Module_crud/Edit">
                            <input type="hidden" name="module_id" value="'.$d.'">
                            <i class="fa fa-pencil float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
		        	}else{$ee='';}

		        	if($delete != null){
		        		$dd = '<i class="fa fa-trash-o float_left btn_delete but_del" aria-hidden="true" data-value="'.$d.'" ></i>';
		        	}else{$dd='';}

		        	return $ss.$ee.$dd;
	        	}
		    )
		);
		echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns));
	}
	public function add(){ 
		$main_folder_name='user';$sub_folder_name='module';$file_name='Add';
		$em = $this->doctrine->em;
		$utill_obj = new Common_utill();
		if($this->input->post('module_name') != null || $this->input->post('mod') != null){
			$this->form_validation->set_rules('module_name', 'Module Name','trim|callback_find_alphabets|required|min_length[3]|max_length[255]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]',array('required'=> 'ModuleName Requierd','min_length' => 'PackageName needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			$this->form_validation->set_rules('module_desc', 'Description','trim|callback_find_alphabets|max_length[255]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]',array('required'=> 'ModuleDesc Requierd', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			if ($this->form_validation->run() != false){
				$module_name = $this->input->post('module_name');
				$module_desc = $this->input->post('module_desc');
				$find_module = $em->getRepository('Entity\Module')->findBy(array('module_name'=>$module_name));
				if($find_module == null){ 
					$module_obj = new Entity\Module;
					$module_obj->setModuleName($module_name);
					$module_obj->setModuleDesc($module_desc);
					$em->persist($module_obj);
					try {
						$em->flush();
						if($this->input->post('check') == 1){
							$utill_obj->create_access('Add',$module_name.'/Add',$module_obj);
							$utill_obj->create_access('Edit',$module_name.'/Edit',$module_obj);
							$utill_obj->create_access('Delete',$module_name.'/Delete',$module_obj);
							$utill_obj->create_access('S_view',$module_name.'/S_view',$module_obj);
							$utill_obj->create_access('View',$module_name.'/View',$module_obj);
							$this->session->set_flashdata('success', '<p>Module Created Successfully</p>');
							redirect('index.php/user/module_crud','refresh');
						}else{
							$this->session->set_flashdata('success', '<p>Module Created Successfully</p>');
							redirect('index.php/user/module_crud','refresh');
						}
						
					}catch (UniqueConstraintViolationException $e){
						// $em->getConnection()->rollBack();
						$this->session->set_flashdata('error', '<p>Module Already Exists</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name);
					}
				}else{
					$this->session->set_flashdata('error', '<p>Module Already Exists</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name);
				}
			}else{
				$this->session->set_flashdata('error', '<p>Error in Fields</p>');
				$this->view_call($main_folder_name,$sub_folder_name,$file_name);
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name);
		}
	}
	public function edit(){
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Module', 'getModuleId', 'getModuleName', 'index.php/user/module_crud', 'Edit', 'module_id');
		$main_folder_name='user';$sub_folder_name='module';$file_name='Edit';
		$em = $this->doctrine->em;
		if($this->input->post('module_id') != null){
			$this->form_validation->set_rules('module_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$module_id = $this->input->post('module_id');
				$find_module = $em->getRepository('Entity\Module')->find($module_id); 
				if($find_module != null){
					$data['module_obj'] = $find_module;
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}else{
					$this->session->set_flashdata('error', '<p>Wrong Module</p>');
					redirect('index.php/user/module_crud','refresh');
				}
			}else{
				$this->view_call($main_folder_name,$sub_folder_name,$file_name);
			}
		}elseif($this->input->post('edit_module_id') != null){ 
			$module_id = $this->input->post('edit_module_id');
			$data['module_obj'] = $em->getRepository('Entity\Module')->find($module_id); 
			$this->form_validation->set_rules('edit_module_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			$this->form_validation->set_rules('module_name', 'Module Name','trim|callback_find_alphabets|required|min_length[3]|max_length[255]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]',array('required'=> 'ModuleName Requierd','min_length' => 'PackageName needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			$this->form_validation->set_rules('module_desc', 'Description','trim|callback_find_alphabets|max_length[255]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]',array('required'=> 'ModuleDesc Requierd', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			if ($this->form_validation->run() != false){
				$module_id = $this->input->post('edit_module_id');
				$module_name = $this->input->post('module_name');
				$module_desc = $this->input->post('module_desc');
				$find_module = $em->getRepository('Entity\Module')->find($module_id); 
				$find_access = $em->getRepository('Entity\Access')->findBy(array('module_id'=>$module_id));
				if($find_module != null){
					if(count($find_access)>0){
						$update_module = $utill_obj->edit_module($module_id,$module_name,$module_desc);
						$data['module_obj'] = $find_module;
						if($update_module == 1){
							for($i=0;$i<count($find_access);$i++){
								$utill_obj->edit_access($find_access[$i]->getAccessId(),$find_access[$i]->getAccessName(),$module_name.$find_access[$i]->getAccessName(),$find_module);
								if($i == count($find_access)-1){
									$this->session->set_flashdata('success', '<p>Module Updated Successfully</p>');
									redirect('index.php/user/module_crud','refresh');
								}
							}
						}else{
							$this->session->set_flashdata('error', '<p>Module Already Exists</p>');
							redirect('index.php/user/module_crud','refresh');
						}
					}else{
						$update_module = $utill_obj->edit_module($module_id,$module_name,$module_desc);
						if($update_module == 1){
							$this->session->set_flashdata('success', '<p>Module Updated Successfully</p>');
							redirect('index.php/user/module_crud','refresh');
						}else{
							$this->session->set_flashdata('error', '<p>Module Already Exists</p>');
							$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
						}
					}
				}else{
					$this->session->set_flashdata('error', '<p>Wrong Module</p>');
					redirect('index.php/user/module_crud','refresh');
				}
			}else{
				$this->session->set_flashdata('error', '<p>Error in Fields</p>');
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}
		}else{ $this->view_call($main_folder_name,$sub_folder_name,$file_name,$data); }
	}
	public function s_view(){
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Module', 'getModuleId', 'getModuleName', 'index.php/user/module_crud', 'S_view', 'module_id');
		$main_folder_name='user';$sub_folder_name='module';$file_name='S_view';
		$em = $this->doctrine->em;
		if($this->input->post('module_id') != null){
			$this->form_validation->set_rules('module_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$module_id = $this->input->post('module_id');
				$find_module = $em->getRepository('Entity\Module')->find($module_id); 
				if($find_module != null){
					$data['module_obj'] = $find_module;
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}else{
					$this->session->set_flashdata('error', '<p>Wrong Module</p>');
					redirect('index.php/user/module_crud','refresh');
				}
			}else{
				$this->view_call($main_folder_name,$sub_folder_name,$file_name);
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function delete(){
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Module', 'getModuleId', 'getModuleName', 'index.php/user/module_crud', 'Delete', 'module_id');
		$main_folder_name='user';$sub_folder_name='module';$file_name='Delete';
		$em = $this->doctrine->em;
		$utill_obj = new Common_utill();
		if($this->input->post('module_id') != null){
			$this->form_validation->set_rules('module_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$module_id = $this->input->post('module_id');
				$find_module = $em->getRepository('Entity\Module')->find($module_id); 
				if($find_module != null){
					$data['module_obj'] = $find_module;
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}else{
					$this->session->set_flashdata('error', '<p>Wrong Module</p>');
					redirect('index.php/user/module_crud','refresh');
				}
			}else{
				$this->view_call($main_folder_name,$sub_folder_name,$file_name);
			}
		}elseif($this->input->post('delete_module_id') != null){ 
			$module_id = $this->input->post('delete_module_id');
			$data['module_obj'] = $em->getRepository('Entity\Module')->find($module_id); 
			$this->form_validation->set_rules('delete_module_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$module_id = $this->input->post('delete_module_id');
				$find_module = $em->getRepository('Entity\Module')->find($module_id); 
				$find_access = $em->getRepository('Entity\Access')->findBy(array('module_id'=>$module_id)); 
				$em->getConnection()->beginTransaction(); 
				
				$data['module_obj'] = $find_module;
				if($find_module != null){ 
					if(count($find_access)>0){
						for($i=0;$i<count($find_access);){
							$del_access = $utill_obj->delete_access($find_access[$i]->getAccessId());
							if($del_access == 1){
								if($i == count($find_access)-1){
									$em->remove($find_module);
									$em->flush();
									$em->getConnection()->commit();
									$this->session->set_flashdata('success', '<p>Module Deleted Successfully</p>');
									redirect('index.php/user/module_crud','refresh');
								}
								$i++;
							}
						}
					}else{
						$em->remove($find_module);
						$em->flush();
						$em->getConnection()->commit();
						$this->session->set_flashdata('success', '<p>Module Deleted Successfully</p>');
						redirect('index.php/user/module_crud','refresh');
					}
				}else{
					$this->session->set_flashdata('error', '<p>Wrong Module</p>');
					redirect('index.php/user/module_crud','refresh');
				}
			}else{
				$this->session->set_flashdata('error', '<p>Error in Fields</p>');
				$this->view_call($main_folder_name,$sub_folder_name,$file_name);
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function ajax_delete(){
		$em = $this->doctrine->em;
		if($this->input->post('delete_module_id') != null){
			$this->form_validation->set_rules('delete_module_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$module_id = $this->input->post('delete_module_id');
				$find_module = $em->getRepository('Entity\Module')->find($module_id); 
				$find_access = $em->getRepository('Entity\Access')->findBy(array('module_id'=>$module_id));
				if($find_module != null){
					if(count($find_access)>0){


						for($i=0;$i<count($find_access);$i++){
							$access = $em->getRepository('Entity\Access')->find($find_access[$i]);
							$em->remove($access);
							if($i == count($find_access)-1){
								$em->remove($find_module);
								$em->flush();
								echo 1;
							}
						}

					}else{
						try{
							$em->remove($find_module);
							$em->flush();
							echo 1;
						}catch(ForeignKeyConstraintViolationException $e){
							echo -1;
					   	}
					}
				}else{
					echo -1;
				}
			}else{
				echo 0;
			}
		}else{
			echo 0;
		}
	}
	public function alphabets_and_number($str){
	   if (preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function find_alphabets($str){
	   if (preg_match('#[a-zA-Z]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function find_numbers($str){
	   if (preg_match('#[0-9]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
		$this->load->view('header');
		$this->load->view($main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
		$this->load->view('footer');
	}

}
?>