<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\Query\ResultSetMapping;
require_once 'ssp.customized.class.php';
class Stock_pointer extends CI_Controller {
  	public function __construct(){
    	parent::__construct();
    	is_user_logged_in();
  	}
  	public function index(){ 
	    $this->view();
  	}
  	public function datatable_ajax(){
		$CI = &get_instance();
		$CI->load->database();
		$sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
		$table = 'User';
		$primaryKey = 'user_id';
		$columns = array(
		    array( 'db' => '`a`.`user_id`', 'dt' => 0,'field' => 'user_id'),
		    array( 'db' => '`a`.`user_name`', 'dt' => 1,'field' => 'user_name'),
		    array( 'db' => '`p`.`user_account_available_amount`', 'dt' => 2,'field' => 'user_account_available_amount'),
		    array( 'db' => '`a`.`user_id`',
			       'dt' => 3,
			       'field' => 'user_id',
			       'formatter' => function( $d, $row ) {
		           $utill_obj = new Common_utill();
							// $edit = $utill_obj->has_access('pincode','edit');
							// $delete = $utill_obj->has_access('pincode','delete');
							// $s_view = $utill_obj->has_access('pincode','s_view');
							$edit = "asd";
							$delete = "asd";
							$s_view = "asd";

		            if($s_view != null){
		        		$ss = '<form method="POST" class="icon_edit_form" action="'.base_url().'index.php/user/Stock_pointer/S_view">
                            <input type="hidden" name="user_id" value="'.$d.'">
                            <i class="fa fa-search-plus float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
		        	}else{$ss = '';}

		        	if($edit != null){
		        		$ee = ' <form method="POST" class="icon_edit_form" action="'.base_url().'index.php/user/Stock_pointer/Edit">
                            <input type="hidden" name="user_id" value="'.$d.'">
                            <i class="fa fa-pencil float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
		        	}else{$ee='';}

		        	if($delete != null){
		        		$dd = '<i class="fa fa-trash-o float_left btn_delete but_del" aria-hidden="true" data-value="'.$d.'" ></i>';
		        	}else{$dd='';}

		        	return $ss.$ee.$dd;
	        	}
		    )
		);
		$joinQuery = "FROM `User` AS `a` INNER JOIN `User_account` AS `p` ON (`a`.`user_id` = `p`.`user_id`) AND `a`.`user_role_id` = ".RT::$default_stock_pointer_id;
		echo json_encode(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery));
	}
	public function view(){
		$main_folder_name='user';$sub_folder_name='stock_pointer';$file_name='View';
		$this->view_call($main_folder_name,$sub_folder_name,$file_name);
	}
	public function add(){
		$em = $this->doctrine->em;
		$utill_obj = new Common_utill();
		$main_folder_name='user';$sub_folder_name='stock_pointer';$file_name='Add';
		if($this->input->post('user_id') != null || $this->input->post('acc') != null){
			$user_id = $this->input->post('user_id');
			$user_obj = $utill_obj->get_single_row_using_primary_key('User',$user_id);
			$account_obj = $utill_obj->get_single_row_using_array_value('User_account','user_id',$user_id);
			if($user_obj != null && $account_obj != null){
				$role_obj = $utill_obj->get_single_row_using_primary_key('User_role',RT::$default_stock_pointer_id);
				if($role_obj != null){
					$amt = 0;
					if($this->input->post('stock_amount') != null){
						$amt = $this->input->post('stock_amount');
					}
					$account_obj->setUserAccountAvailableAmount($amt);
					$user_obj->setUserRoleId($role_obj);
					$em->persist($user_obj);
					$em->persist($account_obj);
					$em->flush();
					$this->session->set_flashdata('success', '<p>Stock Pointer Added Succfully</p>');
					redirect('index.php/user/stock_pointer','refresh');
				}else{
					$this->session->set_flashdata('error', '<p>Invalid Role</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name);	
				}
			}else{
				$this->session->set_flashdata('error', '<p>Invalid User</p>');
				$this->view_call($main_folder_name,$sub_folder_name,$file_name);	
			}
		}else{	
			$this->view_call($main_folder_name,$sub_folder_name,$file_name);		
		}
	}
	public function edit(){
		$em = $this->doctrine->em;
		$utill_obj = new Common_utill();
		$main_folder_name='user';$sub_folder_name='stock_pointer';$file_name='Edit';
		if($this->input->post('user_id') != null){
			$user_id = $this->input->post('user_id');
			$user_obj = $utill_obj->get_single_row_using_primary_key('User',$user_id);
			$account_obj = $utill_obj->get_single_row_using_array_value('User_account','user_id',$user_id);
			$data['obj'] = array('user_id' => $user_obj->getUserId(),'user_name' => $user_obj->getUserName(),'stock_amount' => $account_obj->getUserAccountAvailableAmount());
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}elseif($this->input->post('edit_user_id') != null){
			$user_id = $this->input->post('edit_user_id');
			$account_obj = $utill_obj->get_single_row_using_array_value('User_account','user_id',$user_id);
			$amt = 0;
			if($this->input->post('stock_amount') != null){
				$amt = $this->input->post('stock_amount');
			}
			$account_obj->setUserAccountAvailableAmount($amt);
			$em->persist($account_obj);
			$em->flush();
			$this->session->set_flashdata('success', '<p>Stock Pointer Money Updated Succfully</p>');
			redirect('index.php/user/stock_pointer','refresh');
		}else{
			redirect('index.php/user/stock_pointer','refresh');
		}
	}
	public function s_view(){
		$em = $this->doctrine->em;
		$utill_obj = new Common_utill();
		$main_folder_name='user';$sub_folder_name='stock_pointer';$file_name='S_view';
		if($this->input->post('user_id') != null){
			$user_id = $this->input->post('user_id');
			$user_obj = $utill_obj->get_single_row_using_primary_key('User',$user_id);
			$account_obj = $utill_obj->get_single_row_using_array_value('User_account','user_id',$user_id);
			$data['obj'] = array('user_id' => $user_obj->getUserId(),'user_name' => $user_obj->getUserName(),'stock_amount' => $account_obj->getUserAccountAvailableAmount());
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}else{
			redirect('index.php/user/stock_pointer','refresh');
		}
	}
	public function ajax_delete(){
		$utill_obj = new Common_utill();
		$em = $this->doctrine->em;
		if($this->input->post('delete_id') != null){
			$user_obj = $utill_obj->get_single_row_using_primary_key('User',$this->input->post('delete_id'));
			$account_obj = $utill_obj->get_single_row_using_array_value('User_account','user_id',$this->input->post('delete_id'));
			if($user_obj != null && $account_obj != null){
				$role_obj = $utill_obj->get_single_row_using_primary_key('User_role',RT::$deafault_user_role_id);
				if($role_obj != null){
					$user_obj->setUserRoleId($role_obj);
					$account_obj->setUserAccountAvailableAmount(0);
					$em->persist($account_obj);
					$em->flush();
					echo 1;
				}else{echo 0;}
			}
		}
	}
	public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
		$this->load->view('header');
		$this->load->view(''.$main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
		$this->load->view('footer');
	}
}
?>