<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\Query\ResultSetMapping;
require_once 'ssp.customized.class.php';
class User_role_crud extends CI_Controller {
	public function __construct(){
		parent::__construct();
		is_user_logged_in();
	}
	public function index(){ 
		$this->view();
		$current_data = array(  'cur_data' => "");
		$this->session->set_userdata($current_data);
	}
	public function view(){
		$this->load->view('header');
		$this->load->view('user/user_role/View');
		$this->load->view('footer');	
	}
	public function datatable_ajax(){
		$CI = &get_instance();
		$CI->load->database();
		$sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
		$table = 'User_role';
		$primaryKey = 'user_role_id';
		$columns = array(
		    array( 'db' => 'user_role_name', 'dt' => 0 ),
		    array( 'db' => 'user_role_desc',  'dt' => 1 ),
		    array(
			        'db'        => 'user_role_id',
			        'dt'        => 2,
			        'formatter' => function( $d, $row ) {
			        	$utill_obj = new Common_utill();
							$edit = "asd";
							$delete = "asd";
							$s_view = "asd";

			            if($s_view != null){
			        		$ss = '<form method="POST" class="icon_edit_form" action="'.base_url().'index.php/user/User_role_crud/S_view">
	                            <input type="hidden" name="user_role_id" value="'.$d.'">
	                            <i class="fa fa-search-plus float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
	                        </form>';
			        	}else{$ss = '';}

			        	if($edit != null){
			        		$ee = ' <form method="POST" class="icon_edit_form" action="'.base_url().'index.php/user/User_role_crud/Edit">
	                            <input type="hidden" name="user_role_id" value="'.$d.'">
	                            <i class="fa fa-pencil float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
	                        </form>';
			        	}else{$ee='';}

			        	if($delete != null){
			        		$dd = '<i class="fa fa-trash-o float_left btn_delete but_del" aria-hidden="true" data-value="'.$d.'" ></i>';
			        	}else{$dd='';}

			        	return $ss.$ee.$dd;

	        	}
		    )
		);
		echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns));
	}
	public function add(){ 
		$main_folder_name='user';$sub_folder_name='user_role';$file_name='Add';
		$em = $this->doctrine->em;
		$data['module_list'] = $em->getRepository("Entity\Module")->findAll();
		$data['access_list'] = $em->getRepository("Entity\Access")->findAll();
		$utill_obj = new Common_utill();
		if($this->input->post('role_name') != null || $this->input->post('user_role') != null){ 
			$this->form_validation->set_rules('role_name', 'Role Name','trim|callback_find_alphabets|required|min_length[3]|max_length[255]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]',array('required'=> 'RoleName Requierd','min_length' => 'RoleName needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			$this->form_validation->set_rules('role_desc', 'Description','trim|callback_find_alphabets|max_length[255]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]',array('required'=> 'ModuleDesc Requierd', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			if ($this->form_validation->run() != false){
				$role_name = $this->input->post('role_name');
				$role_desc = $this->input->post('role_desc');
				$role_user_role_ids = implode(',',array_filter(explode(',',$this->input->post('hid_access_id'))));
				$find_role = $em->getRepository('Entity\User_role')->findBy(array('user_role_name'=>$role_name));
				if($find_role == null){ 
					$role_obj = new Entity\User_role;
					$role_obj->setUserRoleName($role_name);
					$role_obj->setUserRoleDesc($role_desc);
					$role_obj->setUserRoleAccessIds($role_user_role_ids);
					$em->persist($role_obj);
					try {
						$em->flush();
						$this->session->set_flashdata('success', '<p>Role Created Successfully</p>');
						redirect('index.php/user/user_role_crud','refresh');
					}catch (UniqueConstraintViolationException $e){
						$this->session->set_flashdata('error', '<p>Role Already Exists</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name);
					}
				}else{
					$this->session->set_flashdata('error', '<p>Role Already Exists</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}
			}else{
				$this->session->set_flashdata('error', '<p>Error in Fields</p>');
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function edit(){
		$main_folder_name='user';$sub_folder_name='user_role';$file_name='Edit';
		$em = $this->doctrine->em;
		$data['module_list'] = $em->getRepository("Entity\Module")->findAll();
		$data['access_list'] = $em->getRepository("Entity\Access")->findAll();
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('User_role', 'getUserRoleId', 'getUserRoleName', 'index.php/user/user_role_crud', 'Edit', 'user_role_id');
		if($this->input->post('user_role_id') != null){
			$this->form_validation->set_rules('user_role_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$user_role_id = $this->input->post('user_role_id');
				$find_role = $em->getRepository('Entity\User_role')->find($user_role_id); 
				if($find_role != null){

					$current_data = array(  'cur_data' => $user_role_id);
					$this->session->set_userdata($current_data);

					$data['role_obj'] = $find_role;
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}else{
					$this->session->set_flashdata('error', '<p>Wrong Module</p>');
					redirect('index.php/user/user_role_crud','refresh');
				}
			}else{
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}
		}elseif($this->input->post('edit_user_role_id') != null){
			if($_SESSION['cur_data'] == $this->input->post('edit_user_role_id')){ 
				$user_role_id = $this->input->post('edit_user_role_id');
				$data['role_obj'] = $em->getRepository('Entity\User_role')->find($user_role_id); 
				$this->form_validation->set_rules('edit_user_role_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
				$this->form_validation->set_rules('role_name', 'Role Name','trim|callback_find_alphabets|required|min_length[3]|max_length[255]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]',array('required'=> 'RoleName Requierd','min_length' => 'RoleName needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
				$this->form_validation->set_rules('role_desc', 'Description','trim|callback_find_alphabets|max_length[255]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]',array('required'=> 'ModuleDesc Requierd', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
				if ($this->form_validation->run() != false){
					$user_role_id = $this->input->post('edit_user_role_id');
					$role_name = $this->input->post('role_name');
					$role_desc = $this->input->post('role_desc');
					$role_user_role_ids = implode(',',array_filter(explode(',',$this->input->post('hid_access_id'))));
					$find_role = $em->getRepository('Entity\User_role')->find($user_role_id); 
					if($find_role != null){
						$find_role->setUserRoleName($role_name);
						$find_role->setUserRoleDesc($role_desc);
						$find_role->setUserRoleAccessIds($role_user_role_ids);
						$em->persist($find_role);
						try {
							$em->flush();
							$this->session->set_flashdata('success', '<p>Role Updated Successfully</p>');
							redirect('index.php/user/user_role_crud','refresh');
						}catch (UniqueConstraintViolationException $e){
							$this->session->set_flashdata('error', '<p>Role Already Exists</p>');
							$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
						}
					}else{
						$this->session->set_flashdata('error', '<p>Role Not Exists</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					}
				}else{
					$this->session->set_flashdata('error', '<p>Error in Fields</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}
			}else{
				$this->session->set_flashdata('error', '<p>Wrong ID</p>');
				redirect('index.php/user/User_role_crud','refresh');
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function s_view(){
		$main_folder_name='user';$sub_folder_name='user_role';$file_name='S_view';
		$em = $this->doctrine->em;
		$data['module_list'] = $em->getRepository("Entity\Module")->findAll();
		$data['access_list'] = $em->getRepository("Entity\Access")->findAll();
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('User_role', 'getUserRoleId', 'getUserRoleName', 'index.php/user/user_role_crud', 'S_view', 'user_role_id');
		if($this->input->post('user_role_id') != null){
			$this->form_validation->set_rules('user_role_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$user_role_id = $this->input->post('user_role_id');
				$find_role = $em->getRepository('Entity\User_role')->find($user_role_id); 
				if($find_role != null){
					$data['role_obj'] = $find_role;
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}else{
					$this->session->set_flashdata('error', '<p>Wrong Module</p>');
					redirect('index.php/user/user_role_crud','refresh');
				}
			}else{
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function delete(){
		$main_folder_name='user';$sub_folder_name='user_role';$file_name='Delete';
		$em = $this->doctrine->em;
		$data['module_list'] = $em->getRepository("Entity\Module")->findAll();
		$data['access_list'] = $em->getRepository("Entity\Access")->findAll();
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('User_role', 'getUserRoleId', 'getUserRoleName', 'index.php/user/user_role_crud', 'Delete', 'user_role_id');
		if($this->input->post('user_role_id') != null){
			$this->form_validation->set_rules('user_role_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$user_role_id = $this->input->post('user_role_id');
				$find_role = $em->getRepository('Entity\User_role')->find($user_role_id); 
				if($find_role != null){

					$current_data = array(  'cur_data' => $user_role_id);
					$this->session->set_userdata($current_data);

					$data['role_obj'] = $find_role;
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}else{
					$this->session->set_flashdata('error', '<p>Wrong Module</p>');
					redirect('index.php/user/user_role_crud','refresh');
				}
			}else{
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}
		}elseif($this->input->post('delete_user_role_id') != null){
			if($_SESSION['cur_data'] == $this->input->post('delete_user_role_id')){ 
				$this->form_validation->set_rules('delete_user_role_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
				$user_role_id = $this->input->post('delete_user_role_id');
				$find_role = $em->getRepository('Entity\User_role')->find($user_role_id); 
				if ($this->form_validation->run() != false){
					$data['role_obj'] = $find_role;
					$em->remove($find_role);
					try {
						$em->flush();
						$this->session->set_flashdata('success', '<p>Role Deleted Successfully</p>');
						redirect('index.php/user/user_role_crud','refresh');
					}catch (ForeignKeyConstraintViolationException $e){
						$this->session->set_flashdata('error', '<p>Role Not Exists</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					}
				}else{
					$this->session->set_flashdata('error', '<p>Error in Fields</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}
			}else{
				$this->session->set_flashdata('error', '<p>Wrong ID</p>');
				redirect('index.php/user/User_role_crud','refresh');
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function ajax_delete(){
		$em = $this->doctrine->em;
		if($this->input->post('delete_user_role_id') != null){
			$this->form_validation->set_rules('delete_user_role_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$user_role_id = $this->input->post('delete_user_role_id');
				$find_role = $em->getRepository('Entity\User_role')->find($user_role_id); 
				$data['role_obj'] = $find_role;
				if($find_role != null){
					$em->remove($find_role);
					try{
						$em->flush();
						echo 1;
					}catch(ForeignKeyConstraintViolationException $e){
						echo -1;
				   	}
				}else{
					echo -1;
				}
			}else{
				echo 0;
			}
		}else{
			echo 0;
		}
	}
	public function alphabets_and_number($str){
	   if (preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function find_alphabets($str){
	   if (preg_match('#[a-zA-Z]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function find_numbers($str){
	   if (preg_match('#[0-9]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
		$this->load->view('header');
		$this->load->view(''.$main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
		$this->load->view('footer');
	}
}
?>