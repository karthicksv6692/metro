<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\Query\ResultSetMapping;
require_once 'ssp.customized.class.php';
class Question_crud extends CI_Controller {
	public function __construct(){
		parent::__construct();
		is_user_logged_in();
	}
	public function index(){ 
		$this->view();
	}
	public function view(){
		$this->load->view('d-login/header');
		$this->load->view('d-login/user/question/View');
		$this->load->view('d-login/footer');	
	}
	public function datatable_ajax(){
		$CI = &get_instance();
		$CI->load->database();
		$sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
		$table = 'Questions';
		$primaryKey = 'question_id';
		$columns = array(
		    array( 'db' => 'question_name', 'dt' => 0 ),
		    array(
			        'db'        => 'question_id',
			        'dt'        => 1,
			        'formatter' => function( $d, $row ) {
		            return '<form method="POST" class="icon_edit_form" action="'.base_url().'user/question_crud/s_view">
	                            <input type="hidden" name="question_id" value="'.$d.'">
                                <i class="mdi-action-pageview pink_color" onclick="$(this).closest(\'form\').submit();"></i>
	                        </form>
	                        <form method="POST" class="icon_edit_form" action="'.base_url().'user/question_crud/edit">
	                            <input type="hidden" name="question_id" value="'.$d.'">
                                <i class="mdi-editor-mode-edit pink_color" onclick="$(this).closest(\'form\').submit();"></i>
	                        </form>
	                        <form method="POST" class="icon_edit_form" action="'.base_url().'user/question_crud/delete">
	                            <input type="hidden" name="question_id" value="'.$d.'">
                                <i class="mdi-action-delete pink_color" onclick="$(this).closest(\'form\').submit();"></i>
	                        </form> ';
	        	}
		    )
		);
		echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns));
	}
	public function add(){
		$main_folder_name='user';$sub_folder_name='question';$file_name='Add';
		$em = $this->doctrine->em;
		if($this->input->post('question_name') != null || $this->input->post('qus') != null){
			$this->form_validation->set_rules('question_name', 'Question Name','trim|callback_find_alphabets|required|min_length[3]|max_length[255]|regex_match[/^[a-zA-Z0-9._ ,?%-]+$/]',array('required'=> 'QuestionName Requierd','min_length' => 'QuestionName needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			if ($this->form_validation->run() != false){
				$question_name = $this->input->post('question_name');
				$question_obj = new Entity\Questions;
				$question_obj->setQuestionName($question_name);
				$em->persist($question_obj);
				try {
					$em->flush();
					$this->session->set_flashdata('success', '<p>Question Created Successfully</p>');
					redirect('user/question_crud','refresh');
				}catch (UniqueConstraintViolationException $e){
					$this->session->set_flashdata('error', '<p>Question Alredy Exists</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name);
				}
			}else{
				$this->session->set_flashdata('error', '<p>Error in Fields</p>');
				$this->view_call($main_folder_name,$sub_folder_name,$file_name);
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name);
		}
	}
	public function edit(){
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Questions', 'getQuestionId', 'getQuestionName', 'user/question_crud', 'Edit', 'question_id');
		$main_folder_name='user';$sub_folder_name='question';$file_name='Edit';
		$em = $this->doctrine->em;
		if($this->input->post('question_id') != null){
			$this->form_validation->set_rules('question_id', 'QuestionId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$question_id = $this->input->post('question_id');
				$find_question = $em->getRepository('Entity\Questions')->find($question_id); 
				if($find_question != null){
					$data['question_obj'] = $find_question;
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}else{
					$this->session->set_flashdata('error', '<p>Question Not Exists</p>');
					redirect('user/question_crud','refresh');
				}
			}else{
				$this->session->set_flashdata('error', '<p>Error in Fields</p>');
				redirect('user/question_crud','refresh');
			}
		}elseif($this->input->post('edit_question_id') != null){
			$this->form_validation->set_rules('edit_question_id', 'QuestionId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			$this->form_validation->set_rules('question_name', 'Question Name','trim|callback_find_alphabets|required|min_length[3]|max_length[255]|regex_match[/^[a-zA-Z0-9._ ,?%-]+$/]',array('required'=> 'QuestionName Requierd','min_length' => 'QuestionName needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			$question_id = $this->input->post('edit_question_id');
			$find_question = $em->getRepository('Entity\Questions')->find($question_id); 
			$data['question_obj'] = $find_question;
			if ($this->form_validation->run() != false){
				$question_id = $this->input->post('edit_question_id');
				$question_name = $this->input->post('question_name');
				$find_question = $em->getRepository('Entity\Questions')->find($question_id); 
				$data['question_obj'] = $find_question;
				if($find_question != null){
					$find_question->setQuestionName($question_name);
					$em->persist($find_question);
					try {
						$em->flush();
						$this->session->set_flashdata('success', '<p>Question Updaeted Successfully</p>');
						redirect('user/question_crud','refresh');
					}catch (UniqueConstraintViolationException $e){
						$this->session->set_flashdata('error', '<p>Question Alredy Exists</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name);
					}
				}else{
					$this->session->set_flashdata('error', '<p>Question Not Exists</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}
			}else{
				$this->session->set_flashdata('error', '<p>Error in Fields</p>');
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function s_view(){
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Questions', 'getQuestionId', 'getQuestionName', 'user/question_crud', 'S_view', 'question_id');
		$main_folder_name='user';$sub_folder_name='question';$file_name='S_view';
		$em = $this->doctrine->em;
		if($this->input->post('question_id') != null){
			$this->form_validation->set_rules('question_id', 'QuestionId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$question_id = $this->input->post('question_id');
				$find_question = $em->getRepository('Entity\Questions')->find($question_id); 
				if($find_question != null){
					$data['question_obj'] = $find_question;
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}else{
					$this->session->set_flashdata('error', '<p>Question Not Exists</p>');
					redirect('user/question_crud','refresh');
				}
			}else{
				$this->session->set_flashdata('error', '<p>Error in Fields</p>');
				redirect('user/question_crud','refresh');
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function delete(){
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Questions', 'getQuestionId', 'getQuestionName', 'user/question_crud', 'Delete', 'question_id');
		$main_folder_name='user';$sub_folder_name='question';$file_name='Delete';
		$em = $this->doctrine->em;
		if($this->input->post('question_id') != null){
			$this->form_validation->set_rules('question_id', 'QuestionId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$question_id = $this->input->post('question_id');
				$find_question = $em->getRepository('Entity\Questions')->find($question_id); 
				if($find_question != null){
					$data['question_obj'] = $find_question;
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}else{
					$this->session->set_flashdata('error', '<p>Question Not Exists</p>');
					redirect('user/question_crud','refresh');
				}
			}else{
				$this->session->set_flashdata('error', '<p>Error in Fields</p>');
				redirect('user/question_crud','refresh');
			}
		}elseif($this->input->post('delete_question_id') != null){
			$this->form_validation->set_rules('delete_question_id', 'QuestionId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			$question_id = $this->input->post('delete_question_id');
			$find_question = $em->getRepository('Entity\Questions')->find($question_id); 
			if ($this->form_validation->run() != false){
				$question_id = $this->input->post('delete_question_id');
				if($find_question != null){
					$em->remove($find_question);
					$em->flush();
					$this->session->set_flashdata('success', '<p>Question Deleted Successfully</p>');
					redirect('user/question_crud','refresh');
				}else{
					$this->session->set_flashdata('error', '<p>Question Not Exists</p>');
					redirect('user/question_crud','refresh');
				}
			}else{
				$this->session->set_flashdata('error', '<p>Error in Fields</p>');
				$this->view_call($main_folder_name,$sub_folder_name,$file_name);
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function alphabets_and_number($str){
	   if (preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function find_alphabets($str){
	   if (preg_match('#[a-zA-Z]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function find_numbers($str){
	   if (preg_match('#[0-9]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
		$this->load->view('d-login/header');
		$this->load->view('d-login/'.$main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
		$this->load->view('d-login/footer');
	}
}
?>