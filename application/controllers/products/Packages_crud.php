<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\Query\ResultSetMapping;
require_once 'ssp.customized.class.php';
class Packages_crud extends CI_Controller {

	public function __construct(){
		parent::__construct();
		is_user_logged_in();
	}
	public function index(){
		$current_data = array(  'cur_data' => "");
		$this->session->set_userdata($current_data);
		$this->view();
	}
	public function view(){
		$this->load->view('header');
		$this->load->view('products/packages/View');
		$this->load->view('footer');
	}
	public function datatable_ajax(){
		$CI = &get_instance();
		$CI->load->database();
		$sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
		$table = 'Packages';
		$primaryKey = 'package_id';
		$columns = array(
		    array( 'db' => 'package_name', 'dt' => 0 ),
		    array( 'db' => 'package_point_value',  'dt' => 1 ),
		    // array( 'db' => 'package_ceiling_price',  'dt' => 2 ),
		    array( 'db' => 'level_income_amount',  'dt' => 2 ),
		    // array( 'db' => 'package_short_desc',  'dt' => 4 ),
		    array( 'db' => 'package_is_visible',  'dt' => 3 ,'formatter' => function( $d, $row ){
		    	if($row['package_is_visible'] == 0){
		    		return "Not-Active";
		    	}else{return "Active";}
		    }),
		    array(
			        'db'        => 'package_id',
			        'dt'        => 4,
			        'formatter' => function( $d, $row ) {
		           	$utill_obj = new Common_utill();
					// $edit = $utill_obj->has_access('pincode','edit');
					// $delete = $utill_obj->has_access('pincode','delete');
					// $s_view = $utill_obj->has_access('pincode','s_view');
					$edit = "asd";
					$delete = "asd";
					$s_view = "asd";

					if($s_view != null){
		        		$ss = '<form method="POST" class="icon_edit_form" action="'.base_url().'index.php/products/packages_crud/S_view">
                            <input type="hidden" name="package_id" value="'.$d.'">
                            <i class="fa fa-search-plus float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
		        	}else{$ss = '';}

		        	if($edit != null){
		        		$ee = ' <form method="POST" class="icon_edit_form" action="'.base_url().'index.php/products/packages_crud/Edit">
                            <input type="hidden" name="package_id" value="'.$d.'">
                            <i class="fa fa-pencil float_left " aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
		        	}else{$ee='';}

		        	if($delete != null){
		        		$dd = '<i class="fa fa-trash-o float_left btn_delete but_del pull-left" aria-hidden="true" data-value="'.$d.'" ></i>';
		        	}else{$dd='';}
		        	return $ss.$ee.$dd;
	        	}
		    )
		);
		echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns));
	}
	public function add(){
		$main_folder_name='products';$sub_folder_name='packages';$file_name='Add';
		if($this->input->post('package_name') !== null){
			$this->form_validation->set_rules('package_name', 'Package Name','trim|callback_find_alphabets|required|min_length[3]|max_length[255]|regex_match[/^[a-zA-Z0-9._ ,-]+$/]',array('required'=> 'PackageName Requierd','min_length' => 'PackageName needs Minimum 5 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			$this->form_validation->set_rules('package_desc', 'Package Description','trim|callback_find_alphabets|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed'));
      $this->form_validation->set_rules('package_price', 'Package Price','trim|min_length[1]|max_length[11]|regex_match[/^[0-9.%]+$/]',array('regex_match' => 'Numbers Only Allowed','find_alphabets'=>'Albhabets Required'));
			$this->form_validation->set_rules('level_income', 'Level Income','trim|min_length[1]|max_length[11]|regex_match[/^[0-9.%]+$/]',array('regex_match' => 'Numbers Only Allowed','find_alphabets'=>'Albhabets Required'));

      $this->form_validation->set_rules('point_value', 'Point Value','trim|min_length[1]|max_length[11]|regex_match[/^[0-9.%]+$/]',array('regex_match' => 'Numbers Only Allowed','find_alphabets'=>'Albhabets Required'));


			$this->form_validation->set_rules('package_short_desc', 'Package Short Description','trim|callback_find_alphabets|max_length[255]|regex_match[/^[a-zA-Z0-9-._ %,]+$/]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			$this->form_validation->set_rules('limited_pair_cutoff', 'Limited Pair Cutoff','trim|min_length[1]|max_length[11]|regex_match[/^[0-9]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			$this->form_validation->set_rules('pair_income', 'Pair Income','trim|min_length[1]|max_length[11]|regex_match[/^[0-9.%]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			$this->form_validation->set_rules('package_ceiling', 'Package Ceiling','trim|min_length[1]|max_length[11]|regex_match[/^[0-9]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			$this->form_validation->set_rules('package_ceiling_price', 'Package Ceiling Price','trim|regex_match[/^[0-9.%]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			$this->form_validation->set_rules('package_is_visible', 'Package Visible','trim|min_length[1]|max_length[11]|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){ 
				$em=$this->doctrine->em;
				$em->getConnection()->beginTransaction();
				 // print_r($_FILES);exit();
				if($this->input->post('is_active') == null){
					$active = 0;
				}else{$active = 1;}
				$package = new Entity\Packages;
				$package->setPackageName($this->input->post('package_name'));
				$package->setPackageDesc($this->input->post('package_desc'));
        $package->setPackagePrice($this->input->post('package_price'));
        $package->setLevelIncomeAmount($this->input->post('level_income'));

				$package->setPackagePointValue($this->input->post('point_value'));

				$package->setPackageShortDesc($this->input->post('package_short_desc'));
				$package->setPackageLimitedPairCutoff($this->input->post('limited_pair_cutoff'));
				$package->setPackagePairIncome($this->input->post('pair_income'));
				$package->setPackageCeilingPerMonth($this->input->post('package_ceiling'));
				$package->setPackageCeilingPrice($this->input->post('package_ceiling_price'));
				$package->setPackageIsVisible($active);
				
				$em->persist($package);
				try {
					$em->flush();
				  	//-----------Code for upload Attachments----
				  	if($_FILES['file']['error']!= 4){ 
						for($i=0;$i<count($_FILES['file']['name']);$i++){
							$url = $this->Common_utill->svk_add_single_image($_FILES['file'],$i);
							$product_short_desc='Package desc';
							$product_name=$this->input->post('package_name');
							$attach = $this->Common_utill->add_attachment($product_name,$url,$package->getPackageId(),5,$product_short_desc);
							if($attach == 1){ 
								if($i == count($_FILES['file']['name'])-1){
									//return $user_obj->getUserId();
									$em->flush();
    								$em->getConnection()->commit();
								}
							}
						}
					}else{
						
    					$em->getConnection()->commit();
					}
				  	//-----------EndCode for upload Attachments----
				   	$this->session->set_flashdata('success', '<p>Successfully Created</p>');
 					redirect('index.php/products/packages_crud','refresh');
			  	}
			  	catch (UniqueConstraintViolationException $e){
			  		$this->session->set_flashdata('error', '<p>Already Exists</p>');
     				$this->view_call($main_folder_name,$sub_folder_name,$file_name);
			  	}
			  	
			}else{
				$this->view_call($main_folder_name,$sub_folder_name,$file_name);
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name);
		}
	}
	public function edit(){
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Packages', 'getPackageId', 'getPackageName', 'index.php/products/packages_crud', 'Edit', 'package_id');

		$main_folder_name='products';$sub_folder_name='packages';$file_name='Edit';
		$em = $this->doctrine->em;
		$data['reviews'] = $em->getRepository('Entity\Reviews')->findBy(array('review_item_id'=>$this->input->post('package_id'),'review_item_type'=>5));
		$data['offer'] = $em->getRepository('Entity\Offer')->findBy(array('offer_item_id'=>$this->input->post('package_id'),'offer_item_type'=>5));

		if($this->input->post('package_id') !== null){
			$this->form_validation->set_rules('package_id', 'Package id','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$em=$this->doctrine->em;

				$current_data = array(  'cur_data' => $this->input->post('package_id'));
				$this->session->set_userdata($current_data);

				$data['single_package']=$em->find('Entity\Packages',$this->input->post('package_id'));
				$data['single_package_attach']=$em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' =>$this->input->post('package_id'),'attachment_referer_type'=>5));
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}else{
				$this->session->set_flashdata('error', '<p>Wrong Package ID</p>');
				redirect('index.php/products/packages_crud','refresh');
			}

		}elseif($this->input->post('edit_package_id')!== null){
			if($_SESSION['cur_data'] == $this->input->post('edit_package_id')){ 
				$em=$this->doctrine->em;
				$data['single_package']=$em->find('Entity\Packages',$this->input->post('edit_package_id'));
				$this->form_validation->set_rules('edit_package_id', 'Package id','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
				
				$this->form_validation->set_rules('package_name', 'Package Name','trim|callback_find_alphabets|required|min_length[3]|max_length[255]|regex_match[/^[a-zA-Z0-9._ ,-]+$/]',array('required'=> 'PackageName Requierd','min_length' => 'PackageName needs Minimum 5 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
				$this->form_validation->set_rules('package_desc', 'Package Description','trim|callback_find_alphabets|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed'));
        $this->form_validation->set_rules('package_price', 'Package Price','trim|min_length[1]|max_length[11]|regex_match[/^[0-9.%]+$/]',array('regex_match' => 'Numbers Only Allowed','find_alphabets'=>'Albhabets Required'));
				$this->form_validation->set_rules('level_income', 'Level Income','trim|min_length[1]|max_length[11]|regex_match[/^[0-9.%]+$/]',array('regex_match' => 'Numbers Only Allowed','find_alphabets'=>'Albhabets Required'));
        $this->form_validation->set_rules('point_value', 'Point Value','trim|min_length[1]|max_length[11]|regex_match[/^[0-9.%]+$/]',array('regex_match' => 'Numbers Only Allowed','find_alphabets'=>'Albhabets Required'));
				$this->form_validation->set_rules('package_short_desc', 'Package Short Description','trim|callback_find_alphabets|max_length[255]|regex_match[/^[a-zA-Z0-9-._ %,]+$/]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
				$this->form_validation->set_rules('limited_pair_cutoff', 'Limited Pair Cutoff','trim|min_length[1]|max_length[11]|regex_match[/^[0-9]+$/]',array('regex_match' => 'Numbers Only Allowed'));
				$this->form_validation->set_rules('pair_income', 'Pair Income','trim|min_length[1]|max_length[11]|regex_match[/^[0-9.%]+$/]',array('regex_match' => 'Numbers Only Allowed'));
				$this->form_validation->set_rules('package_ceiling', 'Package Ceiling','trim|min_length[1]|max_length[11]|regex_match[/^[0-9]+$/]',array('regex_match' => 'Numbers Only Allowed'));
				$this->form_validation->set_rules('package_ceiling_price', 'Package Ceiling Price','trim|regex_match[/^[0-9.%]+$/]',array('regex_match' => 'Numbers Only Allowed'));
				$this->form_validation->set_rules('package_is_visible', 'Package Visible','trim|min_length[1]|max_length[11]|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
					if ($this->form_validation->run() != false){ 


						$em=$this->doctrine->em;
						$em->getConnection()->beginTransaction();


						if($this->input->post('is_active') == null){
							$active = 0;
						}else{$active = 1;}
						
						$package=$em->find('Entity\Packages',$this->input->post('edit_package_id'));
						$package->setPackageName($this->input->post('package_name'));
						$package->setPackageDesc($this->input->post('package_desc'));
            $package->setPackagePrice($this->input->post('package_price'));
            $package->setLevelIncomeAmount($this->input->post('level_income'));
						$package->setPackagePointValue($this->input->post('point_value'));
						$package->setPackageShortDesc($this->input->post('package_short_desc'));
						$package->setPackageLimitedPairCutoff($this->input->post('limited_pair_cutoff'));
						$package->setPackagePairIncome($this->input->post('pair_income'));
						$package->setPackageCeilingPerMonth($this->input->post('package_ceiling'));
						$package->setPackageCeilingPrice($this->input->post('package_ceiling_price'));
						$package->setPackageIsVisible($active);
						$em->persist($package);	
						try {
						   	$em->flush();
						   	if($this->input->post('nw_ad_img') == 0){
						   		$em->flush();
								$em->getConnection()->commit();
					   			$this->session->set_flashdata('success', '<p>Successfully Updated</p>');
			 					redirect('index.php/products/packages_crud','refresh');
						   	}else{
						   		$find_attach = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' =>$this->input->post('edit_package_id'),'attachment_referer_type' => 5));

						   		if(count($find_attach) > 0){
						   			for($i=0;$i<count($find_attach);$i++){
						   				$em->remove($find_attach[$i]);
						   				$em->flush();
	    								// $em->getConnection()->commit();
						   			}
						   		}


						   		if($this->input->post('package_image') != null){
						   			$usr_img = explode(',', $this->input->post('package_image'));
						   			if(count($usr_img) > 0){
						   				for($i=0;$i<count($usr_img);$i++){
						   					$attach = $this->Common_utill->add_attachment($this->input->post('package_name'),$usr_img[$i],$this->input->post('edit_package_id'),5,$this->input->post('package_short_desc'));
						   					if($i == count($usr_img)-1){ 
						   						$em->flush();
													$em->getConnection()->commit();
						   						if($_FILES['files']['error'][0]!= 4){ //print_r($_FILES);exit();
									   				for($k=0;$k<count($_FILES['files']['name']);$k++){
														$url = $this->Common_utill->svk_add_single_image($_FILES['files'],$k);
														$product_short_desc='Package desc';
														$product_name=$this->input->post('package_name');
														$attach = $this->Common_utill->add_attachment($product_name,$url,$this->input->post('edit_package_id'),5,$product_short_desc);
														if($attach == 1){ 
															if($k == count($_FILES['files']['name'])-1){
																$em->flush();
							    								// $em->getConnection()->commit();
							    								$this->session->set_flashdata('success', '<p>Successfully Updated</p>');
							 									redirect('index.php/products/packages_crud','refresh');
															}
														}
													}
									   			}else{ //echo"xx";exit();
									   				$em->flush();
									   				$this->session->set_flashdata('success', '<p>Successfully Updated</p>');
							 						redirect('index.php/products/packages_crud','refresh');
									   			}
						   					}
											
						   				}
						   			}
						   		}else if($_FILES['files']['error'][0]!= 4){
					   				for($i=0;$i<count($_FILES['files']['name']);$i++){
										$url = $this->Common_utill->svk_add_single_image($_FILES['files'],$i);
										$product_short_desc='Package desc';
										$product_name=$this->input->post('package_name');
										$attach = $this->Common_utill->add_attachment($product_name,$url,$this->input->post('edit_package_id'),5,$product_short_desc);
										if($attach == 1){ 
											if($i == count($_FILES['files']['name'])-1){
												//return $user_obj->getUserId();
												$em->flush();
			    								$em->getConnection()->commit();
			    								$this->session->set_flashdata('success', '<p>Successfully Updated</p>');
							 						redirect('index.php/products/packages_crud','refresh');
											}
										}
									}
					   			}else{
					   				$em->flush();
									$em->getConnection()->commit();
					   				$this->session->set_flashdata('success', '<p>Successfully Updated</p>');
			 						redirect('index.php/products/packages_crud','refresh');
					   			}

						   	}
				
							   
					  	}catch (UniqueConstraintViolationException $e){
					  		$this->session->set_flashdata('error', '<p>Already Exists</p>');
		     				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					  	}
					}else{
						$this->session->set_flashdata('error', '<p>Error in Fields</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					}
			}else{
				$this->session->set_flashdata('error', '<p>Wrong ID</p>');
				redirect('index.php/products/packages_crud','refresh');
			}
				
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function s_view(){
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Packages', 'getPackageId', 'getPackageName', 'index.php/products/packages_crud', 'S_view', 'package_id');
		$main_folder_name='products';$sub_folder_name='packages';$file_name='S_view';
		$em = $this->doctrine->em;
  		$data['reviews'] = $em->getRepository('Entity\Reviews')->findBy(array('review_item_id'=>$this->input->post('package_id'),'review_item_type'=>5));
		$data['offer'] = $em->getRepository('Entity\Offer')->findBy(array('offer_item_id'=>$this->input->post('package_id'),'offer_item_type'=>5));


		if($this->input->post('package_id') !== null){
			$this->form_validation->set_rules('package_id', 'Package id','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$em=$this->doctrine->em;
				$data['single_package']=$em->find('Entity\Packages',$this->input->post('package_id'));
				$data['single_package_attach']=$em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' =>$this->input->post('package_id'),'attachment_referer_type'=>5));
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}else{
				$this->session->set_flashdata('error', '<p>Wrong Package ID</p>');
				redirect('index.php/products/packages_crud','refresh');
			}
			
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function delete(){
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Packages', 'getPackageId', 'getPackageName', 'index.php/products/packages_crud', 'Delete', 'package_id');
		$main_folder_name='products';$sub_folder_name='packages';$file_name='Delete';
		if($this->input->post('package_id') !== null){
			$this->form_validation->set_rules('package_id', 'Package id','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){

				$current_data = array(  'cur_data' => $this->input->post('package_id'));
				$this->session->set_userdata($current_data);

				$em=$this->doctrine->em;
				$data['single_package']=$em->find('Entity\Packages',$this->input->post('package_id'));
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}else{
				$this->session->set_flashdata('error', '<p>Wrong Package ID</p>');
				redirect('index.php/products/packages_crud','refresh');
			}
			
		}elseif($this->input->post('delete_package_id') !== null){
			if($_SESSION['cur_data'] == $this->input->post('delete_package_id')){ 
				$this->form_validation->set_rules('delete_package_id', 'Package id','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
				if ($this->form_validation->run() != false){
					$em=$this->doctrine->em;
					$package=$em->find('Entity\Packages',$this->input->post('delete_package_id'));
					$em->remove($package);
					try {
					   	$em->flush();
			   			$find_attach = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' =>$this->input->post('edit_package_id'),'attachment_referer_type' => 5));

				   		if(count($find_attach) > 0){
				   			for($i=0;$i<count($find_attach);$i++){
				   				$em->remove($find_attach[$i]);
				   				$em->flush();
								// $em->getConnection()->commit();
				   			}
				   		}
					   	$this->session->set_flashdata('success', '<p>Successfully Deleted</p>');
						redirect('index.php/products/packages_crud','refresh');
				  	}catch (ForeignKeyConstraintViolationException $e){
				  		$this->session->set_flashdata('error', '<p>ForeignKey ExceptionDeleted Failed</p>');
	     				redirect('index.php/products/packages_crud','refresh');
				  	}
				}else{
					$this->session->set_flashdata('error', '<p>Wrong Package ID</p>');
					redirect('index.php/products/packages_crud','refresh');
				}
			}else{
				$this->session->set_flashdata('error', '<p>Wrong ID</p>');
				redirect('index.php/products/packages_crud','refresh');
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function ajax_delete(){
		$em = $this->doctrine->em;
		if($this->input->post('delete_package_id')!= null){
			$this->form_validation->set_rules('delete_package_id', 'Package id','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run()!= false){
				$package_id = $this->input->post('delete_package_id');
				$find_package = $em->getRepository('Entity\Packages')->find($package_id); 
				if($find_package != null){
					$em->remove($find_package);
					try{
						$em->flush();
						echo 1;
					}catch(ForeignKeyConstraintViolationException $e){
						echo -1;
				   	}
				}else{
					echo -1;
				}
			}else{
				echo 0;
			}
		}else{
			echo 0;
		}
	}
	
	public function alphabets_and_number($str){
	   if (preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function find_alphabets($str){
	   if (preg_match('#[a-zA-Z]#', $str)) {
	     return true;
	   }
	   $this->form_validation->set_message('find_alphabets','Alphabets required');
	   return false;
	}
	public function find_numbers($str){
	   if (preg_match('#[0-9]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function rej_review(){ 
		if($this->input->post('reject_review_id') != null){
			$this->form_validation->set_rules('reject_review_id', 'ReviewId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$review_id = $this->input->post('reject_review_id');
				$em=$this->doctrine->em;
				$review_exists = $em->find('Entity\Reviews',$review_id);
				if($review_exists != null){
					$review_status = 2;
					$review_exists->setReviewStatus($review_status);
					$em = $this->doctrine->em;
					$em->persist($review_exists);
					try {
						$em->flush();
						$this->edit();
					}catch (UniqueConstraintViolationException $e){
						// return true;
					}							
				}else{
					echo -1;
				}	
			}else{
				echo -1;
			}
		}
	}
	public function approve_review(){
		if($this->input->post('approve_review_id') != null){
			$this->form_validation->set_rules('approve_review_id', 'ReviewId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$review_id = $this->input->post('approve_review_id');
				$em=$this->doctrine->em;
				$review_exists = $em->find('Entity\Reviews',$review_id);
				if($review_exists != null){
					$review_status = 1;
					$review_exists->setReviewStatus($review_status);
					$em = $this->doctrine->em;
					$em->persist($review_exists);
					try {
						$em->flush();
						// $this->session->set_flashdata('package_product_id',$this->input->post('package_product_id'));
						// redirect("products/package_product_crud/sess_edit");
						$this->edit();
					}catch (UniqueConstraintViolationException $e){
						// return true;
					}							
				}else{
					echo -1;
				}	
			}else{
				echo -1;
			}
		}
	}
	public function delete_review(){
		if($this->input->post('delete_review_id') != null){
			$this->form_validation->set_rules('delete_review_id', 'ReviewId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$review_id = $this->input->post('delete_review_id');
				$em=$this->doctrine->em;
				$review_exists = $em->find('Entity\Reviews',$review_id);
				if($review_exists != null){
					$em = $this->doctrine->em;
					$em->remove($review_exists);
					try {
						$em->flush();
						$this->edit();
					}catch (UniqueConstraintViolationException $e){
						// return true;
					}							
				}else{
					echo -1;
				}	
			}else{
				echo -1;
			}
		}
	}
	public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
		$this->load->view('header');
		$this->load->view(''.$main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
		$this->load->view('footer');
	}

}

?>