<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\Query\ResultSetMapping;
require_once 'ssp.customized.class.php';
class product_crud extends CI_Controller {

	public function __construct(){
		parent::__construct();
		is_user_logged_in();
	}
	public function index(){
		$current_data = array(  'cur_data' => "");
		$this->view();
	}
	public function view(){
		$this->load->view('header');
		$this->load->view('products/product/View');
		$this->load->view('footer');	
	}
	public function datatable_ajax(){
		$CI = &get_instance();
		$CI->load->database();
		$sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);

		$table = 'Product';
		$primaryKey = 'product_id';
		$columns = array(
		    array( 'db' => '`a`.`product_code`', 'dt' => 0,'field' => 'product_code'),
		    array( 'db' => '`a`.`product_name`', 'dt' => 1,'field' => 'product_name'),
		    array( 'db' => '`p`.`vendor_name`', 'dt' => 2,'field' => 'vendor_name'),
		    array( 'db' => '`a`.`product_price`', 'dt' => 3,'field' => 'product_price'),
		    array( 'db' => '`a`.`product_quantity`', 'dt' => 4,'field' => 'product_quantity'),
		    array( 'db' => '`a`.`product_is_visible`',  'dt' => 5 ,'field' => 'product_is_visible','formatter' => function( $d, $row ){
				    	if($row['product_is_visible'] == 0){
				    		return "Not-Active";
				    	}else{return "Active";}
				    }),
		    array( 'db' => '`a`.`product_id`',
			       'dt' => 6,
			       'field' => 'product_id',
			       'formatter' => function( $d, $row ) {
		            	$utill_obj = new Common_utill();
						// $edit = $utill_obj->has_access('pincode','edit');
						// $delete = $utill_obj->has_access('pincode','delete');
						// $s_view = $utill_obj->has_access('pincode','s_view');
						$edit = "asd";
						$delete = "asd";
						$s_view = "asd";

						if($s_view != null){
			        		$ss = '<form method="POST" class="icon_edit_form" action="'.base_url().'index.php/products/Product_crud/S_view">
	                            <input type="hidden" name="product_id" value="'.$d.'">
	                            <i class="fa fa-search-plus float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
	                        </form>';
			        	}else{$ss = '';}

			        	if($edit != null){
			        		$ee = ' <form method="POST" class="icon_edit_form" action="'.base_url().'index.php/products/Product_crud/Edit">
	                            <input type="hidden" name="product_id" value="'.$d.'">
	                            <i class="fa fa-pencil float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
	                        </form>';
			        	}else{$ee='';}

			        	if($delete != null){
			        		$dd = '<i class="fa fa-trash-o float_left btn_delete but_del" aria-hidden="true" data-value="'.$d.'" ></i>';
			        	}else{$dd='';}
			        	return $ss.$ee.$dd;
	        	}
		    )
		);
		$joinQuery = "FROM `Product` AS `a` INNER JOIN `Vendor` AS `p` ON (`a`.`product_vendor_id_id` = `p`.`vendor_id`)";
		 
		echo json_encode(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery));
	}
	public function add(){
		$main_folder_name='products';$sub_folder_name='product';$file_name='Add';
		$em = $this->doctrine->em;
		$product = new Entity\Product;
		$data['vendor'] = $em->getRepository("Entity\Vendor")->findAll();
		if($this->input->post('product_name') !== null || $this->input->post('prod') !== null ){ 
      //print_r($_POST);exit();
			$this->form_validation->set_rules('product_code', 'product_code Name','trim|required|callback_alphabets_and_number|min_length[1]|max_length[11]|regex_match[/^[a-zA-Z0-9._ ,-]+$/]',array('required'=> 'ProductCode Requierd','min_length' => 'ProductCode needs Minimum 1 charecters', 'max_length' => 'Maximum 255 Charecters are allowed'));
			$this->form_validation->set_rules('product_name', 'Product Name','trim|callback_find_alphabets|required|min_length[3]|max_length[255]',array('required'=> 'ProductName Requierd','min_length' => 'ProductName needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			$this->form_validation->set_rules('product_quantity', 'Quantity','trim|min_length[1]|max_length[11]|required|regex_match[/^[0-9]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			$this->form_validation->set_rules('product_price', 'Price','trim|required|min_length[1]|max_length[11]|regex_match[/^[0-9.%]+$/]',array('regex_match' => 'Numbers Only Allowed'));
      $this->form_validation->set_rules('product_display_price', 'Price','trim|required|min_length[1]|max_length[11]|regex_match[/^[0-9.%]+$/]',array('regex_match' => 'Numbers Only Allowed'));
      $this->form_validation->set_rules('point_value', 'Price','trim|required|min_length[1]|max_length[11]|regex_match[/^[0-9.%]+$/]',array('regex_match' => 'Numbers Only Allowed'));
      $this->form_validation->set_rules('product_vendor_id', 'Vendor','trim|required|min_length[1]|max_length[11]|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));


			$this->form_validation->set_rules('category_id', 'Category_id','trim|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Wrong Category'));


			$this->form_validation->set_rules('product_reorder_level', 'ReOrder Level','trim|required|min_length[1]|max_length[11]|regex_match[/^[0-9]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			$this->form_validation->set_rules('product_desc', 'Description','trim|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			$this->form_validation->set_rules('product_short_desc', 'Short Description','trim|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			if ($this->form_validation->run() != false){ 


				if($this->input->post('is_active') != null){ $is_active = 1; }else{$is_active = 0;}
				$product_name = $this->input->post('product_name');
				$product_code = $this->input->post('product_code');
				$product_quantity = $this->input->post('product_quantity');
        $product_price = $this->input->post('product_price');
        $product_display_price = $this->input->post('product_display_price');
				$point_value = $this->input->post('point_value');

        $prd_vendor_id = $this->input->post('product_vendor_id');
        $category_id = 0;
        if($this->input->post('category_id') != null){
          $category_id = $this->input->post('category_id');
        }
        $user_choice=0;
        if($this->input->post('user_choice') != null){
          $user_choice = $this->input->post('user_choice');
        }

				$product_reorder_level = $this->input->post('product_reorder_level');
				$product_desc = preg_replace( "/\n/", "^", $this->input->post('product_desc') );
				$product_short_desc = preg_replace( "/\n/", "^", $this->input->post('product_short_desc'));
				$vendor_object = $em->getRepository('Entity\Vendor')->find($prd_vendor_id);
        $product_vendor_id = $em->getReference('Entity\Vendor',$vendor_object->getVendorId()); 

        $cat_obj = 0;
        if($category_id > 0){
          $obnj = $em->getReference('Entity\Category',$category_id); 
          if($obnj != null){
            $cat_obj = $obnj;
          }
        }

				$find_product = $em->getRepository('Entity\Product')->findOneBy(array('product_name' => $product_name,'product_code'=>$product_code));
				if($find_product == null){
					$utill_obj = new Common_utill();
					$product_obj = $utill_obj->add_product_with_attachment($is_active,$product_name,$product_code,$product_quantity,$product_price,$product_display_price,$prd_vendor_id,$product_reorder_level,$product_desc,$product_short_desc,$product_vendor_id,$_FILES['files'],$cat_obj,$point_value,$user_choice);
					if($product_obj == 1){
						$em->getConnection()->commit();
						$this->session->set_flashdata('success', '<p>Successfully Created</p>');
						redirect('index.php/products/product_crud','refresh');
					}else{ 
						$this->session->set_flashdata('error', '<p>Product Created Failed</p>');
						redirect('index.php/products/product_crud','refresh');
					}
				}else{
					$this->session->set_flashdata('error', '<p>Product Name or Code Already Exists</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}
			}else{
				$this->session->set_flashdata('error', '<p>Error in Fields</p>');
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function edit(){
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Product', 'getProductId', 'getProductName', 'index.php/products/product_crud', 'Edit', 'product_id');
		$main_folder_name='products';$sub_folder_name='product';$file_name='Edit';
		$em = $this->doctrine->em;
		$product = new Entity\Product;
		$data['vendor'] = $em->getRepository("Entity\Vendor")->findAll();
		if($this->input->post('product_id') !== null){
			$this->form_validation->set_rules('product_id', 'ProductId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$product_id = $this->input->post('product_id');
				$product_object  = $em->find('Entity\Product',$product_id); 
				$vendor_object  = $em->getRepository('Entity\Vendor')->find($product_object->getProductVendorId());
				$data['attachment']  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $product_id, 'attachment_referer_type'=>4));
				//$product_object->setAttachmentRefererId($attachment);

				$current_data = array(  'cur_data' => $this->input->post('product_id'));
				$this->session->set_userdata($current_data);
        $par_ar = array('parent_id'=>'','parent_name'=>'');
        if($product_object->getCategoryId() != null){
          $parent_obj = $em->getRepository('Entity\Category')->find($product_object->getCategoryId()->getCategoryId()); 
          
          if($parent_obj != null){
            $par_ar = array('parent_id'=>$parent_obj->getCategoryId(),'parent_name'=>$parent_obj->getCategoryName());
          }
        }
          
        $data['parent_obj'] =  $par_ar;
				$data['single_product']=$product_object;
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}else{
				redirect('index.php/products/product_crud','refresh');
			}
		}elseif($this->input->post('edit_product_id') !== null){
			if($_SESSION['cur_data'] == $this->input->post('edit_product_id')){ 
        // print_r($_POST);exit();
				$product_id = $this->input->post('edit_product_id');
				$product_object  = $em->find('Entity\Product',$product_id); 
				$vendor_object  = $em->getRepository('Entity\Vendor')->find($product_object->getProductVendorId());
				$data['attachment']  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $product_id, 'attachment_referer_type'=>4));
				//$product_object->setAttachmentRefererId($attachment);
				$data['single_product']=$product_object;
				$this->form_validation->set_rules('edit_product_id', 'ProductId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
				$this->form_validation->set_rules('product_code', 'product_code Name','trim|required|callback_alphabets_and_number|min_length[1]|max_length[11]|regex_match[/^[a-zA-Z0-9._ ,-]+$/]',array('required'=> 'ProductCode Requierd','min_length' => 'ProductCode needs Minimum 1 charecters', 'max_length' => 'Maximum 255 Charecters are allowed'));
				$this->form_validation->set_rules('product_name', 'Product Name','trim|callback_find_alphabets|required|min_length[3]|max_length[255]|regex_match[/^[a-zA-Z0-9._ ,-]+$/]',array('required'=> 'ProductName Requierd','min_length' => 'ProductName needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
				$this->form_validation->set_rules('product_quantity', 'Quantity','trim|min_length[1]|max_length[11]|required|regex_match[/^[0-9]+$/]',array('regex_match' => 'Numbers Only Allowed'));
				$this->form_validation->set_rules('product_price', 'Price','trim|required|min_length[1]|max_length[11]|regex_match[/^[0-9.%]+$/]',array('regex_match' => 'Numbers Only Allowed'));
        $this->form_validation->set_rules('product_display_price', 'Price','trim|required|min_length[1]|max_length[11]|regex_match[/^[0-9.%]+$/]',array('regex_match' => 'Numbers Only Allowed'));
				$this->form_validation->set_rules('product_vendor_id', 'Vendor','trim|required|min_length[1]|max_length[11]|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
				$this->form_validation->set_rules('product_reorder_level', 'ReOrder Level','trim|required|min_length[1]|max_length[11]|regex_match[/^[0-9]+$/]',array('regex_match' => 'Numbers Only Allowed'));
				$this->form_validation->set_rules('product_desc', 'Description','trim|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
				$this->form_validation->set_rules('product_short_desc', 'Short Description','trim|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));

				if ($this->form_validation->run() != false){ 

          // print_r($_POST);exit();

					 $em->getConnection()->beginTransaction();

					if($this->input->post('is_active') != null){ $is_active = 1; }else{$is_active = 0;}
					$product_name = $this->input->post('product_name');
					$product_id = $this->input->post('edit_product_id');
					$product_code = $this->input->post('product_code');
					$product_quantity = $this->input->post('product_quantity');
          $product_price = $this->input->post('product_price');
          $product_display_price = $this->input->post('product_display_price');
					$point_value = $this->input->post('point_value');
          $user_choice=0;
          if($this->input->post('user_choice') != null){
            $user_choice = $this->input->post('user_choice');
          }

          $prd_vendor_id = $this->input->post('product_vendor_id');
					$category_id = $this->input->post('category_id');
					$product_reorder_level = $this->input->post('product_reorder_level');
					$product_desc = preg_replace( "/\n/", "^", $this->input->post('product_desc') );
          $product_short_desc = preg_replace( "/\n/", "^", $this->input->post('product_short_desc'));
					$vendor_object = $em->getRepository('Entity\Vendor')->find($prd_vendor_id);
					$product_vendor_id = $em->getReference('Entity\Vendor',$vendor_object->getVendorId()); 
					$product_id = $this->input->post('edit_product_id');
					$product_exists = $em->find('Entity\Product',$product_id);

					$nw_ad_img = $this->input->post('nw_ad_img');
					$package_image = $this->input->post('package_image');

					$table_name = "Product";
					$field_name = "product_code";
					$field_value = $product_code;
					$primary_key = 'product_id';
					$key_value = $product_id;
					$product_code_exist =  $utill_obj->check_availability_on_update($table_name,$field_name,$field_value,$primary_key,$key_value);
					if($product_code_exist == 1){
						if($product_exists != null){
							$img_list = $this->input->post('img_list');
							$utill_obj = new Common_utill();


              $cat_obj = 0;
              if($category_id > 0){
                $obnj = $em->getReference('Entity\Category',$category_id); 
                if($obnj != null){
                  $cat_obj = $obnj;
                }
              }
							$product_obj = $utill_obj->update_product_with_attachment($product_id,$is_active,$product_name,$product_code,$product_quantity,$product_price,$product_display_price,$product_vendor_id,$product_reorder_level,$product_desc,$product_short_desc,$_FILES['files'],$img_list,$nw_ad_img,$package_image,$cat_obj,$point_value,$user_choice);

							if($product_obj == 1){
								$em->getConnection()->commit();
								$this->session->set_flashdata('success', '<p>Successfully Updated</p>');
								redirect('index.php/products/product_crud','refresh');
							}else{
								$this->session->set_flashdata('error', '<p>Updated Failed</p>');
								$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
							}
						}else{
							$this->session->set_flashdata('error', '<p>Product Not Exist</p>');
							redirect('index.php/products/product_crud','refresh');
						}
					}else{
						$this->session->set_flashdata('error', '<p>Product Code Already Exists</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					}
				}else{
					$this->session->set_flashdata('error', '<p>Error in Fields</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}
			}else{
				$this->session->set_flashdata('error', '<p>Wrong ID</p>');
				redirect('index.php/products/product_crud','refresh');
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function s_view(){
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Product', 'getProductId', 'getProductName', 'index.php/products/product_crud', 'S_view', 'product_id');
		$main_folder_name='products';$sub_folder_name='product';$file_name='S_view';
		$em = $this->doctrine->em;
		$qb = $em->createQueryBuilder();
		$rsm = new ResultSetMapping();
		$reviews_fields = $em->getClassMetadata('Entity\Reviews')->getFieldNames();
		$offer_fields = $em->getClassMetadata('Entity\Offer')->getFieldNames();
		$rsm->addEntityResult('Entity\Reviews', 'r');
		$rsm->addEntityResult('Entity\Offer', 'of');
		for($i=0;$i<count($reviews_fields);$i++){
			$rsm->addFieldResult('r', $reviews_fields[$i],$reviews_fields[$i]);
		}
		for($i=0;$i<count($offer_fields);$i++){
			$rsm->addFieldResult('of', $offer_fields[$i],$offer_fields[$i]);
		}

	  	$review_query = $em->createNativeQuery('SELECT * from Reviews where review_item_id="'.$this->input->post('product_id').'" AND review_item_type=4',$rsm);
	  	$data['reviews'] = $review_query->getResult();

		$offer_query = $em->createNativeQuery('SELECT * from Offer where offer_item_id="'.$this->input->post('product_id').'" AND offer_item_type=4',$rsm);
	  	$data['offer'] = $offer_query->getResult();
		$em = $this->doctrine->em;
		$qb = $em->createQueryBuilder();
		$rsm = new ResultSetMapping();
		$rsm->addEntityResult('Entity\Vendor', 'u');
		$rsm->addFieldResult('u', 'vendor_id', 'vendor_id');
		$rsm->addFieldResult('u', 'vendor_name', 'vendor_name');
		$query = $em->createNativeQuery('SELECT * FROM Vendor ',$rsm);
	  	$data['vendor'] = $query->getResult();
		if($this->input->post('product_id') !== null){
			$this->form_validation->set_rules('product_id', 'ProductId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){  
				$product_id = $this->input->post('product_id');
				$em = $this->doctrine->em;
				$product_object  = $em->find('Entity\Product',$product_id); 
				$vendor_object  = $em->getRepository('Entity\Vendor')->find($product_object->getProductVendorId());
				$attachment  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $product_id));
				$product_object->setAttachmentRefererId($attachment);
				$data['single_product']=$product_object;
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}else{
				$this->session->set_flashdata('error', '<p>Wrong Product ID</p>');
				redirect('index.php/products/product_crud','refresh');
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function delete(){
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Product', 'getProductId', 'getProductName', 'index.php/products/product_crud', 'Delete', 'product_id');
		$main_folder_name='products';$sub_folder_name='product';$file_name='Delete';
		$em = $this->doctrine->em;
		$qb = $em->createQueryBuilder();
		$rsm = new ResultSetMapping();
		$rsm->addEntityResult('Entity\Vendor', 'u');
		$rsm->addFieldResult('u', 'vendor_id', 'vendor_id');
		$rsm->addFieldResult('u', 'vendor_name', 'vendor_name');
		$query = $em->createNativeQuery('SELECT * FROM Vendor ',$rsm);
	  	$data['vendor'] = $query->getResult();
		if($this->input->post('product_id') !== null){
			$this->form_validation->set_rules('product_id', 'ProductId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){  
				$product_id = $this->input->post('product_id');
				$em = $this->doctrine->em;
				$product_object  = $em->find('Entity\Product',$product_id); 
				$vendor_object  = $em->getRepository('Entity\Vendor')->find($product_object->getProductVendorId());
				$attachment  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $product_id, 'attachment_referer_type'=>4));

				$current_data = array(  'cur_data' => $this->input->post('product_id'));
				$this->session->set_userdata($current_data);

				$product_object->setAttachmentRefererId($attachment);
				$data['single_product']=$product_object;
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}else{
				$this->session->set_flashdata('error', '<p>Wrong Product ID</p>');
				redirect('index.php/products/product_crud','refresh');
			}
		}elseif($this->input->post('delete_product_id') !== null){
			if($_SESSION['cur_data'] == $this->input->post('delete_product_id')){
				$this->form_validation->set_rules('delete_product_id', 'ProductId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
				if ($this->form_validation->run() != false){
					$product_id = $this->input->post('delete_product_id');
					$em = $this->doctrine->em;
					$attachment_vendor = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_id' => $product_id, 'attachment_referer_type'=>4));
					if($attachment_vendor != null){
						$em->remove($attachment_vendor);
						try{
							$em->flush();
							$del_vendor = $em->find('Entity\Product',$product_id);
							$em->remove($del_vendor);
							try{
								$em->flush();
								$this->session->set_flashdata('success', '<p>Delete Successfully</p>');
								redirect('index.php/products/product_crud','refresh');
							}catch(ForeignKeyConstraintViolationException $e){
								$this->session->set_flashdata('error', '<p>ForeignKeyConstraint Delete Failed</p>');
								redirect('index.php/products/product_crud','refresh');
							}
						}catch(ForeignKeyConstraintViolationException $e){
							$this->session->set_flashdata('error', '<p>ForeignKeyConstraint Delete Failed</p>');
							redirect('index.php/products/product_crud','refresh');
						}
					}else{
						$del_vendor = $em->find('Entity\Product',$product_id);
						$em->remove($del_vendor);
						try{
							$em->flush();
							$this->session->set_flashdata('success', '<p>Delete Successfully</p>');
							redirect('index.php/products/product_crud','refresh');
						}catch(ForeignKeyConstraintViolationException $e){
							$this->session->set_flashdata('error', '<p>ForeignKeyConstraint Delete Failed</p>');
							redirect('index.php/products/product_crud','refresh');
						}
					}
				}else{
					$this->session->set_flashdata('error', '<p>Product Delete Error</p>');
					redirect('index.php/products/product_crud','refresh');
				}
			}else{
				$this->session->set_flashdata('error', '<p>Wrong ID</p>');
				redirect('index.php/products/product_crud','refresh');
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function ajax_delete(){
		$em = $this->doctrine->em;
		if($this->input->post('delete_product_id')!= null){
			$this->form_validation->set_rules('delete_product_id', 'Package id','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run()!= false){
				$product_id = $this->input->post('delete_product_id');
				$attachment_vendor = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_id' => $product_id, 'attachment_referer_type'=>4));

				if($attachment_vendor != null){
					$em->remove($attachment_vendor);
					try{
						$em->flush();
            $pack_prd_items = $em->getRepository('Entity\Package_product_items')->findBy(array('package_product_item_product_id' => $product_id));
            if($pack_prd_items != null){
              if(count($pack_prd_items) > 0){
                foreach ($pack_prd_items as $pack_prd_item) {
                  $em->remove($pack_prd_item);
                }
              }
            }
						$del_vendor = $em->find('Entity\Product',$product_id);
						$em->remove($del_vendor);
						try{
							$em->flush();
							echo 1;
						}catch(ForeignKeyConstraintViolationException $e){ echo 0;}
					}catch(ForeignKeyConstraintViolationException $e){echo 0;}
				}else{
          $pack_prd_items = $em->getRepository('Entity\Package_product_items')->findBy(array('package_product_item_product_id' => $product_id));
            if($pack_prd_items != null){
              if(count($pack_prd_items) > 0){
                foreach ($pack_prd_items as $pack_prd_item) {
                  $em->remove($pack_prd_item);
                }
              }
            }
					$del_vendor = $em->find('Entity\Product',$product_id);
					$em->remove($del_vendor);
					try{
						$em->flush();
						echo 1;
					}catch(ForeignKeyConstraintViolationException $e){echo 0;}
				}
			}else{
				echo 0;
			}
		}else{
			echo 0;
		}
	}
	public function alphabets_and_number($str){
	   if (preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str)) {
	     return true;
	   }
	   $this->form_validation->set_message('alphabets_and_number','Product Code Must be Alphanumeric');
	   return false;
	}
	public function find_alphabets($str){
	   if (preg_match('#[a-zA-Z]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function find_numbers($str){
	   if (preg_match('#[0-9]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
		$this->load->view('header');
		$this->load->view(''.$main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
		$this->load->view('footer');
	}
}

?>