<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\Query\ResultSetMapping;
require_once 'ssp.customized.class.php';
class Package_product_crud extends CI_Controller {

	public function __construct(){
		parent::__construct();
		is_user_logged_in();
	}
	public function index(){
		$current_data = array(  'cur_data' => "");
		$this->view();
	}
	public function view(){
		$this->load->view('header');
		$this->load->view('products/package_product/View');
		$this->load->view('footer');	
	}
	public function datatable_ajax(){
		
    $ss='';$ee='';$dd='';

    $CI = &get_instance();
    $CI->load->database();
    $sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
    $table = 'Package_products';
    $primaryKey = 'package_product_id';
    $columns = array(
        array( 'db' => '`pp`.`package_product_name`', 'dt' => 0,'field' => 'package_product_name' ),
        array( 'db' => '`pa`.`package_name`', 'dt' => 1,'field' => 'package_name' ),

        array( 'db' => '`pp`.`package_product_display_price`',  'dt' => 2 ,'field' => 'package_product_display_price'), 

        array(     
          'db'=> '`pp`.`package_product_id`', 'dt'=> 3, 'field' => 'package_product_id',  'formatter' =>
          function( $d, $row ) {
            return '<form method="POST" class="icon_edit_form" action="'.base_url().'index.php/products/Package_product_crud/S_view"> <input type="hidden" name="package_product_id" value="'.$d.'"> <i class="fa fa-search-plus float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i> </form> <form method="POST" class="icon_edit_form" action="'.base_url().'index.php/products/Package_product_crud/Edit"> <input type="hidden" name="package_product_id" value="'.$d.'"> <i class="fa fa-pencil float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i> </form><i class="fa fa-trash-o float_left btn_delete but_del" aria-hidden="true" data-value="'.$d.'" ></i>';
          }
        )
        
    );
    $joinQuery = "FROM `Package_products` AS `pp` INNER JOIN `Packages` AS `pa` ON (`pa`.`package_id` = `pp`.`package_id`) ";
     
    echo json_encode(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery));
	}
  
	public function add(){
		$main_folder_name='products';$sub_folder_name='package_product';$file_name='Add';
		$utill_obj = new Common_utill();
		$em = $this->doctrine->em;
		$query1 = $em->createQuery('SELECT V FROM Entity\Packages V LEFT JOIN Entity\Package_products E WITH V.package_id = E.package_product_id WHERE E.package_product_id IS NULL ');
	  	$data['package'] = $query1->getResult();
	  	$query2 = $em->createQuery('SELECT u from Entity\Product u where u.product_is_visible=1');
		$data['product']  = $query2->getResult();
		if($this->input->post('package_product_name') != null || $this->input->post('p_prd') != null  ){ //print_r($_POST);exit();
			$this->form_validation->set_rules('package_product_name', 'Package Product Name','trim|required|min_length[3]|max_length[255]|regex_match[/^[a-zA-Z0-9._ ,-]+$/]',array('required'=> 'PackageProductName Requierd','min_length' => 'PackageProductName needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));

			//$this->form_validation->set_rules('package_id', 'Package','trim|required|min_length[1]|max_length[11]|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed','find_numbers'=>'Numbers Required'));
			// $this->form_validation->set_rules('package_product_quantity', 'Quantity','trim|callback_find_numbers|required|min_length[1]|max_length[11]|regex_match[/^[0-9]+$/]',array('regex_match' => 'Numbers Only Allowed','find_numbers'=>'Numbers Required'));
			$this->form_validation->set_rules('package_product_price', 'Price','trim|callback_find_numbers|required|min_length[1]|max_length[11]|regex_match[/^[0-9.%]+$/]',array('regex_match' => 'Numbers Only Allowed','find_numbers'=>'Numbers Required'));
			$this->form_validation->set_rules('package_product_display_price', 'Display Price','trim|callback_find_numbers|required|min_length[1]|max_length[11]|regex_match[/^[0-9.%]+$/]',array('regex_match' => 'Numbers Only Allowed','find_numbers'=>'Numbers Required'));
			
			// if(isset($_POST['product_quantity'])){
			// 	if (is_array($_POST['product_quantity'])) {
			// 	    foreach ($_POST['product_quantity'] as $key => $value) {
			// 	       $this->form_validation->set_rules('product_quantity['.$key.']', 'Product Quantity','trim|required|callback_find_numbers|required|regex_match[/^[0-9\-]+$/]',array('required'=> 'Required','regex_match' => 'Numbers Only Allowed','find_numbers'=>'Quantity is Numeric'));
			// 	    }
			// 	}
			// }
						
			if ($this->form_validation->run() != false){ //print_r($_POST);exit();
				if($this->input->post('is_active') != null){ $is_active = 1; }else{$is_active = 0;}
				$package_product_name = $this->input->post('package_product_name');
				$package_product_quantity = $this->input->post('package_product_quantity');
				$package_product_price = $this->input->post('package_product_price');
				$package_product_display_price = $this->input->post('package_product_display_price');
				$package_prd_package_id = $this->input->post('package_id');
				$package_product_desc = $this->input->post('package_product_desc');
				$package_product_short_desc = $this->input->post('package_product_short_desc');
				$package_object = $em->getRepository('Entity\Packages')->find($package_prd_package_id);
				$package_id = $em->getReference('Entity\Packages',$package_object->getPackageId()); 
				$package_product = new Entity\Package_products;
				$em = $this->doctrine->em;
				$em->getConnection()->beginTransaction();
				$find_package_product = $em->getRepository('Entity\Package_products')->findOneBy(array('package_product_name' => $package_product_name,'package_id' =>$package_prd_package_id)); 
				if($find_package_product != null){
					$this->session->set_flashdata('error', '<p>PackageProduct Already Exists</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}else{
					if(isset($_FILES['files'])){
						$images = $_FILES['files'];
					}else{
						$images=null;
					}
					$product_obj = $utill_obj->add_package_product_with_attachment($is_active,$package_product_name,$package_product_quantity,$package_product_price,$package_product_display_price,$package_prd_package_id,$package_id,$package_product_desc,$package_product_short_desc,$images);
					//print_r($images);exit();
					if($product_obj != null){ //print_r($product_obj);print_r($this->input->post('product_name'));
						//$new_package_product_id =  $em->getReference('Entity\Package_products',16);
						if(count($this->input->post('product_name')[0]) == 0 && $product_obj > 0){ 
							$em->getConnection()->commit();
							$this->session->set_flashdata('success', '<p>Successfully Created</p>');
						  redirect('index.php/products/Package_product_crud','refresh');
						}elseif(count($this->input->post('product_name')) > 0 && $product_obj > 0){
							for($i=0;$i<count($this->input->post('product_name'));$i++){
								$package_product_items = new Entity\Package_product_items;
								$find_package_product_item = $em->getRepository('Entity\Package_products')->findOneBy(array('package_product_name' => $this->input->post('product_name')[$i]));
								
								if($find_package_product_item == null){
									$new_package_product_id =  $em->getReference('Entity\Product',$this->input->post('product_name')[$i]);
									$new_product_id =  $em->getReference('Entity\Package_products',$product_obj);
									$package_product_items->setPackageProductId($new_product_id);
									$package_product_items->setPackageProductQuantity($this->input->post('product_quantity')[$i]);
									$package_product_items->setPackageProductItemProductId($new_package_product_id);
									
									$em->persist($package_product_items);
									$em->flush();	
									if($i == count($this->input->post('product_name'))-1){ 
										$em->getConnection()->commit();
										$this->session->set_flashdata('success', '<p>Successfully Created</p>');
										redirect('index.php/products/Package_product_crud','refresh');
									}
								}
							}
						}else{
							$this->session->set_flashdata('error', '<p>Package Product Created Error</p>');
							$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
						}
					}else{
						$this->session->set_flashdata('error', '<p>Already Exists</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					}
				}
			}else{//print_r($_POST);exit();	
				$this->session->set_flashdata('error', '<p>Error in Fields.Check</p>');
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function edit(){
		$em = $this->doctrine->em;
    $utill_obj = new Common_utill();
    $data['search'] = $utill_obj->search_field('Package_products', 'getPackageProductId', 'getPackageProductName', 'index.php/products/Package_product_crud', 'Edit', 'package_product_id');
    $main_folder_name='products';$sub_folder_name='package_product';$file_name='Edit';
		if($this->input->post('package_product_id') != null){
			$query2 = $em->createQuery('SELECT u from Entity\Product u where u.product_is_visible=1');
			$data['product']  = $query2->getResult();
			$data['attachment']  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $this->input->post('package_product_id'), 'attachment_referer_type'=>6));
			$this->form_validation->set_rules('package_product_id', 'PackageProductId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));

			if ($this->form_validation->run() != false){
				$package_product_id = $this->input->post('package_product_id');
				$em=$this->doctrine->em;
				$package_product_obj  = $em->find('Entity\Package_products',$package_product_id);
				$package_id = $package_product_obj->getPackageId()->getPackageId();

        // echo $package_product_id;exit();
				
				$data['package_product_item'] = $em->getRepository('Entity\Package_product_items')->findBy(array('package_product_id'=>$package_product_id)); 

        //print_r($xx[0]->getPackageProductItemProductId()->getProductId());exit();

				$query1 = $em->createQuery('SELECT V FROM Entity\Packages V LEFT JOIN Entity\Package_products E WITH V.package_id = E.package_product_id WHERE E.package_product_id IS NULL ');
	  			$data['package'] = $query1->getResult();

				$data['obj']=$package_product_obj;

				$current_data = array(  'cur_data' => $this->input->post('package_product_id'));
				$this->session->set_userdata($current_data);

				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}else{
				$this->session->set_flashdata('error', '<p>PackageProductId Error</p>');
				redirect('index.php/products/Package_product_crud','refresh');
			}
		}elseif($this->input->post('edit_package_product_id') != null){
			$query2 = $em->createQuery('SELECT u from Entity\Product u where u.product_is_visible=1');
			$data['product']  = $query2->getResult();

			$query3 = $em->createQuery('SELECT u from Entity\Reviews u where u.review_item_id='.$this->input->post('edit_package_product_id').'AND u.review_item_type=6');
			$data['reviews']  = $query3->getResult();

			$query4 = $em->createQuery('SELECT u from Entity\Offer u where u.offer_item_id='.$this->input->post('edit_package_product_id').'AND u.offer_item_type=6');
			$data['offer']  = $query4->getResult();

			$data['attachment']  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $this->input->post('edit_package_product_id'), 'attachment_referer_type'=>6));
			$package_product_id = $this->input->post('edit_package_product_id');
			$package_product_obj  = $em->find('Entity\Package_products',$package_product_id);
			$package_id = $package_product_obj->getPackageId()->getPackageId();
			$data['package_product_item'] = $em->getRepository('Entity\Package_product_items')->findBy(array('package_product_id'=>$package_product_id)); 
			$query1 = $em->createQuery('SELECT V FROM Entity\Packages V LEFT JOIN Entity\Package_products E WITH V.package_id = E.package_product_id WHERE E.package_product_id IS NULL ');
  			$data['package'] = $query1->getResult();

  			$data['obj']=$package_product_obj; //print_r($_POST);exit();
			$this->form_validation->set_rules('edit_package_product_id', 'PackageProductId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			
			$this->form_validation->set_rules('package_id', 'Package','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed','find_numbers'=>'Numbers Required'));
			$this->form_validation->set_rules('package_product_quantity', 'Quantity','trim|callback_find_numbers|required|min_length[1]|max_length[11]|regex_match[/^[0-9]+$/]',array('regex_match' => 'Numbers Only Allowed','find_numbers'=>'Numbers Required'));
			$this->form_validation->set_rules('package_product_price', 'Price','trim|callback_find_numbers|required|min_length[1]|max_length[11]|regex_match[/^[0-9.%]+$/]',array('regex_match' => 'Numbers Only Allowed','find_numbers'=>'Numbers Required'));
			$this->form_validation->set_rules('package_product_display_price', 'Display Price','trim|callback_find_numbers|required|min_length[1]|max_length[11]|regex_match[/^[0-9.%]+$/]',array('regex_match' => 'Numbers Only Allowed','find_numbers'=>'Numbers Required'));
			
      if($this->input->post['product_name'] != null){
        if (is_array($_POST['product_name'])) {
            foreach ($_POST['product_name'] as $key => $value) {
                $this->form_validation->set_rules('product_name['.$key.']', 'Product Quantity','trim|required|callback_find_numbers|required|regex_match[/^[0-9\-]+$/]',array('required'=> 'Required','regex_match' => 'Numbers Only Allowed','find_numbers'=>'Required'));
            }
        }
        if (is_array($_POST['product_quantity'])) {
            foreach ($_POST['product_quantity'] as $key => $value) {
               $this->form_validation->set_rules('product_quantity['.$key.']', 'Product Quantity','trim|required|callback_find_numbers|required|regex_match[/^[0-9\-]+$/]',array('required'=> 'Required','regex_match' => 'Numbers Only Allowed','find_numbers'=>'Quantity is Numeric'));
            }
        }
      }
			
			
			if ($this->form_validation->run() != false){  //print_r($_POST);exit();
				if($this->input->post('is_active') != null){ $is_active = 1; }else{$is_active = 0;}
				$package_product_id = $this->input->post('edit_package_product_id');
				$package_product_name = $this->input->post('package_product_name');
				$package_product_quantity = $this->input->post('package_product_quantity');
				$package_product_price = $this->input->post('package_product_price');
				$package_product_display_price = $this->input->post('package_product_display_price');
				$package_prd_package_id = $this->input->post('package_id');
				$package_product_desc = $this->input->post('package_product_desc');
				$package_product_short_desc = $this->input->post('package_product_short_desc');
				$img_list = $this->input->post('img_list');

					$nw_ad_img = $this->input->post('nw_ad_img');
					$package_image = $this->input->post('package_image');

				$package_object = $em->getRepository('Entity\Packages')->find($package_prd_package_id);
				$package_id = $em->getReference('Entity\Packages',$package_object->getPackageId()); 
				$package_product = new Entity\Package_products;
				$em = $this->doctrine->em;
				$package_product_exists = $em->find('Entity\Package_products',$package_product_id);

        $p_id = $package_id->getPackageId();
				$table_name = "Package_products";
				$field_name = "package_product_name";
				$field_value = $package_product_name;
				$primary_key = 'package_product_id';
				$key_value = $package_product_id;

        $q = $em->createQuery("SELECT u from Entity\Package_products u WHERE u.package_product_name='".$package_product_name."' AND u.package_id = $p_id AND u.package_product_id !=$package_product_id" );
        $users = 

				$product_code_exist = $q->getResult();
				if(count($product_code_exist) ==0 ){
					if($package_product_exists != null){
						if(isset($_FILES['files'])){ $images = $_FILES['files']; }else{ $images=null; }
						$product_obj = $utill_obj->update_package_product_with_attachment($package_product_id,$is_active,$package_product_name,$package_product_quantity,$package_product_price,$package_product_display_price,$package_prd_package_id,$package_id,$package_product_desc,$package_product_short_desc,$images,$img_list,$nw_ad_img,$package_image);
						  // print_r($product_obj);exit();
						if($product_obj == 1){
							$new_package_product_id =  $em->getReference('Entity\Package_products',$package_product_exists->getPackageProductId());
							$product =  $em->getRepository('Entity\Package_product_items')->findBy(array('package_product_id'=>$new_package_product_id));

							$j = count($product)-1;
							if(count($product) > 0){ 
								for($i=0;$i<count($product);$i++){
									$product_item_id = $product[$i]->getPackageProductItemId(); 
									$package_product_item_find = $em->find('Entity\Package_product_items',$product_item_id);
									 // print_r($product_item_id);exit();
									$em->remove($package_product_item_find);
									$em->flush();
								}
								if($this->input->post('product_name') != null){
									for($k=0;$k<count($this->input->post('product_name'));$k++){
										$package_product_items = new Entity\Package_product_items;
										$find_package_product_item = $em->getRepository('Entity\Package_products')->findOneBy(array('package_product_name' => $this->input->post('product_name')[$k]));

										if($find_package_product_item == null){

											$new_package_product_id =  $em->getReference('Entity\Product',$this->input->post('product_name')[$k]);
											$new_product_id =  $em->getReference('Entity\Package_products',$package_product_id);
											 //print_r($product_obj);exit();
											$package_product_items->setPackageProductId($new_product_id);
											$package_product_items->setPackageProductQuantity($this->input->post('product_quantity')[$k]);
											$package_product_items->setPackageProductItemProductId($new_package_product_id);
											
											$em->persist($package_product_items);
											$em->flush();	

											if($k == count($this->input->post('product_name'))-1){ 
												$em->getConnection()->commit();

												$this->session->set_flashdata('success', '<p>Successfully Created</p>');
												redirect('index.php/products/Package_product_crud','refresh');
											}
										}
									}
								}else{
									$em->flush();
									$em->getConnection()->commit();

									$this->session->set_flashdata('success', '<p>Update Successfully</p>');
									redirect('index.php/products/Package_product_crud','refresh');
								}
							}elseif($this->input->post('product_name') != null){  
									for($k=0;$k<count($this->input->post('product_name'));$k++){
										$package_product_items = new Entity\Package_product_items;
										$find_package_product_item = $em->getRepository('Entity\Package_products')->findOneBy(array('package_product_name' => $this->input->post('product_name')[$k]));
										if($find_package_product_item == null){
											$new_package_product_id =  $em->getReference('Entity\Product',$this->input->post('product_name')[$k]);
											$new_product_id =  $em->getReference('Entity\Package_products',$product_obj);
											$package_product_items->setPackageProductId($new_product_id);
											$package_product_items->setPackageProductQuantity($this->input->post('product_quantity')[$k]);
											$package_product_items->setPackageProductItemProductId($new_package_product_id);
											
											$em->persist($package_product_items);
											$em->flush();	
											if($k == count($this->input->post('product_name'))-1){ 
												$em->getConnection()->commit();
												$this->session->set_flashdata('success', '<p>Successfully Created</p>');
												redirect('index.php/products/Package_product_crud','refresh');
											}
										}
									}
							}else{ 
								$em->flush();
								$em->getConnection()->commit();
								$this->session->set_flashdata('success', '<p>Updated Successfully</p>');
								redirect('index.php/products/Package_product_crud','refresh');
							}
						}else{
							$this->session->set_flashdata('error', '<p>Update Failed</p>');
							$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
						}
					}else{
						$this->session->set_flashdata('error', '<p>Not Exists</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					}
				}else{
					$this->session->set_flashdata('error', '<p>Package Product Name Already Exists</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}
			}else{
				$this->session->set_flashdata('error', '<p>Error in Fields</p>');
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}
		}else{ 
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function delete(){
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Package_products', 'getPackageProductId', 'getPackageProductName', 'index.php/products/Package_product_crud', 'Delete', 'package_product_id');

		$data['search'] = $utill_obj->search_field('Package_products', 'getPackageProductId', 'getPackageProductName', 'index.php/products/Package_product_crud', 'Delete', 'package_product_id');
		$em = $this->doctrine->em;
		$query2 = $em->createQuery('SELECT u from Entity\Product u where u.product_is_visible=1');
		$data['product']  = $query2->getResult();
		if($this->input->post('package_product_id') != null){
		  	$query3 = $em->createQuery('SELECT u from Entity\Reviews u where u.review_item_id='.$this->input->post('package_product_id').'AND u.review_item_type=6');
				$data['reviews']  = $query3->getResult();
			$query4 = $em->createQuery('SELECT u from Entity\Offer u where u.offer_item_id='.$this->input->post('package_product_id').'AND u.offer_item_type=6');
			$data['offer']  = $query4->getResult();
		}

		if($this->input->post('package_product_id') !== null){
			$this->form_validation->set_rules('package_product_id', 'PackageProductId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$package_product_id = $this->input->post('package_product_id');
				$em=$this->doctrine->em;
				$package_product_obj  = $em->find('Entity\Package_products',$package_product_id);
				$package_id = $package_product_obj->getPackageId()->getPackageId();
				$qb = $em->createQueryBuilder();
				$rsm = new ResultSetMapping();
				$rsm->addEntityResult('Entity\Packages', 'u');
				$rsm->addFieldResult('u', 'package_id', 'package_id');
				$rsm->addFieldResult('u', 'package_name', 'package_name');
				$query = $em->createNativeQuery('SELECT V.* FROM Packages V LEFT JOIN Package_products E ON V.package_id!= '.$package_id.' AND V.package_id = E.package_id  WHERE E.package_id  IS NULL',$rsm);
				$data['package'] = $query->getResult();
				$data['obj']=$package_product_obj;
				$data['package_product_item'] = $em->getRepository('Entity\Package_product_items')->findBy(array('package_product_id'=>$package_product_id)); 
				$data['attachment']  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $this->input->post('edit_package_product_id'), 'attachment_referer_type'=>6));
				
				$current_data = array(  'cur_data' => $this->input->post('package_product_id'));
				$this->session->set_userdata($current_data);

				$this->load->view('header');	
				$this->load->view('products/package_product/delete',$data);
				$this->load->view('footer');
			}else{
				$this->session->set_flashdata('error', '<p>PackageProductId Error</p>');
				redirect('index.php/products/Package_product_crud','refresh');
			}
		}elseif($this->input->post('delete_package_product_id') !== null){
			$this->form_validation->set_rules('delete_package_product_id', 'PackageProductId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$em = $this->doctrine->em;
				$package_product_id = $this->input->post('delete_package_product_id');
				$package_product_exists = $em->find('Entity\Package_products',$package_product_id);
				if($package_product_exists != null){
					$product =  $em->getRepository('Entity\Package_product_items')->findBy(array('package_product_id'=>$package_product_id));
					$j = count($product)-1;
					if(count($product) == 0){
						$em->remove($package_product_exists);
						$em->flush();
						$this->session->set_flashdata('success', '<p>Deleted Successfully</p>');
						redirect('index.php/products/Package_product_crud','refresh');
					}
					for($i=0;$i<count($product);$i++){
						$product_item_id = $product[$i]->getPackageProductItemId();
						$package_product_items = new Entity\Package_product_items;
						$package_product_item_find = $em->find('Entity\Package_product_items',$product_item_id);
						$em->remove($package_product_item_find);
						if($i == $j){
							$em->remove($package_product_exists);
							try {
								$em->flush();
								$this->session->set_flashdata('success', '<p>Deleted Successfully</p>');
								redirect('index.php/products/Package_product_crud','refresh');
							}catch (ForeignKeyConstraintViolationException $e){
								$this->session->set_flashdata('error', '<p>Deleted Failed</p>');
								redirect('index.php/products/Package_product_crud','refresh');
							}
						}
					}
				}else{
					$this->session->set_flashdata('error', '<p>Deleted Failed</p>');
					redirect('index.php/products/Package_product_crud','refresh');
				}
			}else{
				$this->session->set_flashdata('error', '<p>PackageProductId Error</p>');
				redirect('index.php/products/Package_product_crud','refresh');
			}
		}else{
			$this->load->view('header');	
			$this->load->view('products/package_product/delete',$data);
			$this->load->view('footer');
		}
	}
	public function ajax_delete(){
		$em = $this->doctrine->em;
		if($this->input->post('package_product_id')!= null){
			$this->form_validation->set_rules('package_product_id', 'Package id','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run()!= false){
				$package_product_id = $this->input->post('package_product_id');
				$package_product_exists = $em->find('Entity\Package_products',$package_product_id);
				if($package_product_exists != null){
					$product =  $em->getRepository('Entity\Package_product_items')->findBy(array('package_product_id'=>$package_product_id));
					$j = count($product)-1;
					if(count($product) == 0){
            $em->remove($package_product_exists);
            $em->flush();
						echo 1;
					}
					for($i=0;$i<count($product);$i++){
						$product_item_id = $product[$i]->getPackageProductItemId();
						$package_product_items = new Entity\Package_product_items;
						$package_product_item_find = $em->find('Entity\Package_product_items',$product_item_id);
						$em->remove($package_product_item_find);
            $em->flush();
						if($i == $j){
							$em->remove($package_product_exists);
              try {
                $em->flush();
								echo 1;
							}catch (ForeignKeyConstraintViolationException $e){
								echo -4;
							}
						}
					}
				}else{
					echo -3;
				}
			}else{
				echo -2;
			}
		}else{
			echo -1;
		}
	}
	public function s_view(){
		$em = $this->doctrine->em;
		
		$query2 = $em->createQuery('SELECT u from Entity\Product u where u.product_is_visible=1');
		$data['product']  = $query2->getResult();
		if($this->input->post('package_product_id') != null){
		  	$query3 = $em->createQuery('SELECT u from Entity\Reviews u where u.review_item_id='.$this->input->post('package_product_id').'AND u.review_item_type=6');
				$data['reviews']  = $query3->getResult();

			$query4 = $em->createQuery('SELECT u from Entity\Offer u where u.offer_item_id='.$this->input->post('package_product_id').'AND u.offer_item_type=6');
			$data['offer']  = $query4->getResult();
		}
			
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Package_products', 'getPackageProductId', 'getPackageProductName', 'index.php/products/Package_product_crud', 'S_view', 'package_product_id');
		if($this->input->post('package_product_id') !== null){
			$this->form_validation->set_rules('package_product_id', 'PackageProductId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$package_product_id = $this->input->post('package_product_id');
				$em=$this->doctrine->em;
				$package_product_obj  = $em->find('Entity\Package_products',$package_product_id);
				$package_id = $package_product_obj->getPackageId()->getPackageId();
				$qb = $em->createQueryBuilder();
				$rsm = new ResultSetMapping();
				$rsm->addEntityResult('Entity\Packages', 'u');
				$rsm->addFieldResult('u', 'package_id', 'package_id');
				$rsm->addFieldResult('u', 'package_name', 'package_name');
				$query = $em->createNativeQuery('SELECT V.* FROM Packages V LEFT JOIN Package_products E ON V.package_id!= '.$package_id.' AND V.package_id = E.package_id  WHERE E.package_id  IS NULL',$rsm);
				$data['attachment']  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $this->input->post('package_product_id'), 'attachment_referer_type'=>6));
				$data['package'] = $query->getResult();
				$data['package_product_item'] = $em->getRepository('Entity\Package_product_items')->findBy(array('package_product_id'=>$package_product_id)); 

				$data['obj']=$package_product_obj;
				$this->load->view('header');	
				$this->load->view('products/package_product/S_view',$data);
				$this->load->view('footer');
			}else{
				$this->session->set_flashdata('error', '<p>PackageProductId Error</p>');
				redirect('index.php/products/Package_product_crud','refresh');
			}
		}else{
			$this->load->view('header');	
			$this->load->view('products/package_product/S_view',$data);
			$this->load->view('footer');
		}
	}
	public function alphabets_and_number($str){
	   if (preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function find_alphabets($str){
	   if (preg_match('#[a-zA-Z]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function find_numbers($str){
	   if (preg_match('#[0-9]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function delete_review(){
		if($this->input->post('delete_review_id') != null){
			$this->form_validation->set_rules('delete_review_id', 'ReviewId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$review_id = $this->input->post('delete_review_id');
				$em=$this->doctrine->em;
				$review_exists = $em->find('Entity\Reviews',$review_id);
				if($review_exists != null){
					$em = $this->doctrine->em;
					$em->remove($review_exists);
					try {
						$em->flush();
						echo 1;
					}catch (UniqueConstraintViolationException $e){
						echo 0;
					}							
				}else{
					echo 0;
				}	
			}else{
				echo 0;
			}
		}
	}
	public function approve_review(){
		if($this->input->post('approve_review_id') != null){
			$this->form_validation->set_rules('approve_review_id', 'ReviewId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$review_id = $this->input->post('approve_review_id');
				$em=$this->doctrine->em;
				$review_exists = $em->find('Entity\Reviews',$review_id);
				if($review_exists != null){
					$review_status = 1;
					$review_exists->setReviewStatus($review_status);
					$em = $this->doctrine->em;
					$em->persist($review_exists);
					try {
						$em->flush();
						echo 1;
					}catch (UniqueConstraintViolationException $e){
						echo 0;
					}							
				}else{
					echo 0;
				}	
			}else{
				echo 0;
			}
		}
	}
	public function rej_review(){ 
		if($this->input->post('reject_review_id') != null){
			$this->form_validation->set_rules('reject_review_id', 'ReviewId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$review_id = $this->input->post('reject_review_id');
				$em=$this->doctrine->em;
				$review_exists = $em->find('Entity\Reviews',$review_id);
				if($review_exists != null){
					$review_status = 2;
					$review_exists->setReviewStatus($review_status);
					$em = $this->doctrine->em;
					$em->persist($review_exists);
					try {
						$em->flush();
						echo 1;
					}catch (UniqueConstraintViolationException $e){
						echo 0;
					}							
				}else{
					echo 0;
				}	
			}else{
				echo 0;
			}
		}
	}
	public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
		$this->load->view('header');
		$this->load->view(''.$main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
		$this->load->view('footer');
	}
}
?>