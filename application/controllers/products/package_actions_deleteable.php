<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/master/Validation.php");
require_once 'ssp.customized.class.php';

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ORMInvalidArgumentException;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class Package_actions extends CI_Controller {
	private $data=array(); 
	public function __construct(){
		parent::__construct();
		is_user_logged_in();
	}
	public function index(){
		$this->view_all_packages();
	}
	public function package_add(){
		$this->load->view('header');
		$this->load->view('packages/package_add');
		$this->load->view('footer');
	}
	public function save_new_package(){
		$da=new validation();
		$em=$this->doctrine->em;
		$this->form_validation->set_rules('fback_string','fback_string','required',$da->v_msg);
		if($this->form_validation->run() != FALSE){
			$pkg_name=
			$pkg_price=
			$pkg_desc=
			$pkg_short_desc=
			$pkg_limit_per_cut_off=
			$pkg_pair_income=
			$pkg_ceiling_per_month=
			$pkg_ceiling_price=
			$pkg_is_visible=
			try {	
				//------------
				$fb_obj = new Entity\Cf_feedback;
				$fb_obj->setFeedbackString($feedback_string);
				$fb_obj->setFbackUser($user_id);
				$em->persist($fb_obj);
				$em->flush();
				//------------
				$this->session->set_userdata('fback_send',1);
				echo 'success';
			}
			catch(NotNullConstraintViolationException $e){
				echo 'Some fields are missing';
			}
			catch(UniqueConstraintViolationException $e){
				echo 'unique field already exist';
			}
			catch(Exception $e){
				echo 'Error saving data';
			}
		}
		else{
			echo 'fail';
		}

	}
	public function package_edit(){

	}
	public function package_delete(){
		$deteteable_fb_id=$this->input->post('deleteable_fb');
		$data=$this->common_functions->delete_data_by_id('Entity\Cf_feedback',$deteteable_fb_id);
		if($data=='Success'){
			echo 'success';
		}else{
			echo 'fail';			
		}
	}
	public function view_single_package(){

	}
	public function view_all_packages(){
		$em=$this->doctrine->em;
		$data['Feedback']='Feedback&Suggestion';
		$data['error_box']=$this->load->view('error_box','', TRUE);
		$query="select fb.feedback_string from Entity\Cf_feedback as fb";
		$que = $em->createQuery($query);
		$data['feedback_data']= $que->getResult();
		$this->load->view('common/header',$data);
		$this->load->view('feedback/feedback_all',$data);
		$this->load->view('common/footer');
	}
	public function ajax_view_all_packages(){
		$ss='';$ee='';$dd='';
		$CI = &get_instance();
		$CI->load->database();
		$sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);

		$table = 'Cf_feedback';
		$primaryKey = 'fback_id';
		$columns = array(
			array( 'db' => '`fb`.`created`', 'dt' => 0,'field' => 'created'),
		    array( 'db' => '`u`.`first_name`', 'dt' => 1,'field' => 'first_name'),
		    array( 'db' => '`u`.`user_mobile_primary`', 'dt' => 2,'field' => 'user_mobile_primary'),
		    array( 'db' => '`fb`.`feedback_string`', 'dt' => 3,'field' => 'feedback_string'),
		    array( 'db' => '`fb`.`fback_id`',
			       'dt' =>4,
			       'field' => 'fback_id',
			       'formatter' => function( $d, $row ){
	        		$delete_fb= '<form method="POST" class="feedback_delete_form icon_form gj_nw_srch" action="'.base_url().'visitor/feedback/delete_user_feedback1">
                    <input type="hidden" name="deleteable_fb" value="'.$d.'">
                    <button class="btn-floating waves-effect waves-light teal lighten-2 search" type="submit"><i class="fa fa-search-plus right searchicon"></i></button>
                    </form>';
			        return $delete_fb;
	        	}
		    )
		);
		$joinQuery = "FROM  `Cf_feedback` As `fb` INNER JOIN `Cf_user` As `u` ON (`fb`.`fback_user`=`u`.`user_id`) ";
		echo json_encode(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery));
	}
}
?>