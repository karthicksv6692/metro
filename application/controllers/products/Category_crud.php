<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\Query\ResultSetMapping;
require_once 'ssp.customized.class.php';
class Category_crud extends CI_Controller {

  public function __construct(){
    parent::__construct();
    is_user_logged_in();
  }
  public function index(){ 
    $this->view();
  }
  public function view(){
    $this->load->view('header');
    $this->load->view('products/category/View');
    $this->load->view('footer');  
  }
  public function datatable_ajax(){
    $CI = &get_instance();
    $CI->load->database();
    $sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
    $table = 'Category';
    $primaryKey = 'category_id';
    $columns = array(
        array( 'db' => 'category_name', 'dt' => 0 ),
        array( 'db' => 'category_desc',  'dt' => 1 ),
        array(
              'db'        => 'category_id',
              'dt'        => 2,
              'formatter' => function( $d, $row ) {

                  $utill_obj = new Common_utill();
              // $edit = $utill_obj->has_access('pincode','edit');
              // $delete = $utill_obj->has_access('pincode','delete');
              // $s_view = $utill_obj->has_access('pincode','s_view');
              $edit = "asd";
              $delete = "asd";
              $s_view = "asd";

                if($s_view != null){
                $ss = '<form method="POST" class="icon_edit_form" action="'.base_url().'index.php/products/Category_crud/S_view">
                            <input type="hidden" name="category_id" value="'.$d.'">
                            <i class="fa fa-search-plus float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
              }else{$ss = '';}

              if($edit != null){
                $ee = ' <form method="POST" class="icon_edit_form" action="'.base_url().'index.php/products/Category_crud/Edit">
                            <input type="hidden" name="category_id" value="'.$d.'">
                            <i class="fa fa-pencil float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
              }else{$ee='';}

              if($delete != null){
                $dd = '<i class="fa fa-trash-o float_left btn_delete but_del" aria-hidden="true" data-value="'.$d.'" ></i>';
              }else{$dd='';}

              return $ss.$ee.$dd;
            }
        )
    );
    echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns));
  }
  public function add(){ 
    $main_folder_name='products';$sub_folder_name='category';$file_name='Add';
    $em = $this->doctrine->em;
    $utill_obj = new Common_utill();
    if($this->input->post('category_name') != null || $this->input->post('cat') != null){
      $this->form_validation->set_rules('category_name', 'Module Name','trim|callback_find_alphabets|required|min_length[3]|max_length[255]',array('required'=> 'ModuleName Requierd','min_length' => 'Category needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
      $this->form_validation->set_rules('category_desc', 'Description','trim|callback_find_alphabets|max_length[255]',array('required'=> 'ModuleDesc Requierd', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));

      $this->form_validation->set_rules('parent_id', 'Parent','trim|regex_match[/^[0-9]+$/]',array('regex_match' => 'Select Valid Category'));

      if ($this->form_validation->run() != false){ 
        $category_name = $this->input->post('category_name');
        $category_desc = $this->input->post('category_desc');
        $parent_id = $this->input->post('parent_id');
        $status =1;
        if($parent_id == null){
          $parent_id = 0;
        }
        if($parent_id != 0 || $parent_id != null ){
          $find_cat = $em->getRepository('Entity\Category')->find($parent_id);
          if($find_cat != null){
            $status =1;
          }else{$status =0;}
        }
        if($status == 1){
          $find_module = $em->getRepository('Entity\Category')->findBy(array('category_name'=>$category_name));
          if($find_module == null){ 
            $cat_obj = new Entity\Category;
            $cat_obj->setCategoryName($category_name);
            $cat_obj->setCategoryDesc($category_desc);
            $cat_obj->setParentId($parent_id);
            $em->persist($cat_obj);
            try {
              $em->flush();
              $this->session->set_flashdata('success', '<p>Category Created Successfully</p>');
              redirect('index.php/products/category_crud','refresh');
            }catch (UniqueConstraintViolationException $e){
              $this->session->set_flashdata('error', '<p>Category Already Exists</p>');
              $this->view_call($main_folder_name,$sub_folder_name,$file_name);
            }
          }else{
            $this->session->set_flashdata('error', '<p>Category Already Exists</p>');
            $this->view_call($main_folder_name,$sub_folder_name,$file_name);
          }
        }else{
          $this->session->set_flashdata('error', '<p>Parent Category Not Exists</p>');
          $this->view_call($main_folder_name,$sub_folder_name,$file_name);
        }
      }else{
        $this->session->set_flashdata('error', '<p>Error in Fields</p>');
        $this->view_call($main_folder_name,$sub_folder_name,$file_name);
      }
    }else{
      $this->view_call($main_folder_name,$sub_folder_name,$file_name);
    }
  }
  public function edit(){
    $utill_obj = new Common_utill();
    $data['search'] = $utill_obj->search_field('Category', 'getCategoryId', 'getCategoryName', 'index.php/products/category_crud', 'Edit', 'category_id');
    $main_folder_name='products';$sub_folder_name='category';$file_name='Edit';
    $em = $this->doctrine->em;
    if($this->input->post('category_id') != null){
      $this->form_validation->set_rules('category_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
      if ($this->form_validation->run() != false){
        $category_id = $this->input->post('category_id');
        $find_module = $em->getRepository('Entity\Category')->find($category_id); 
        if($find_module != null){
          $parent_obj = $em->getRepository('Entity\Category')->find($find_module->getParentId()); 
          $par_ar = array('parent_id'=>'','parent_name'=>'');
          if($parent_obj != null){
            $par_ar = array('parent_id'=>$parent_obj->getCategoryId(),'parent_name'=>$parent_obj->getCategoryName());
          }
          $data['parent_obj'] =  $par_ar;
          $data['cat_obj'] = $find_module;
          $this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
        }else{
          $this->session->set_flashdata('error', '<p>Wrong Module</p>');
          redirect('index.php/products/category_crud','refresh');
        }
      }else{
        $this->view_call($main_folder_name,$sub_folder_name,$file_name);
      }
    }elseif($this->input->post('edit_category_id') != null){ 
      $category_id = $this->input->post('edit_category_id');
      $data['cat_obj'] = $em->getRepository('Entity\Category')->find($category_id); 
      $this->form_validation->set_rules('edit_category_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));

      $this->form_validation->set_rules('category_name', 'Module Name','trim|callback_find_alphabets|required|min_length[3]|max_length[255]',array('required'=> 'ModuleName Requierd','min_length' => 'Category needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
      $this->form_validation->set_rules('category_desc', 'Description','trim|callback_find_alphabets|max_length[255]',array('required'=> 'ModuleDesc Requierd', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
      $this->form_validation->set_rules('parent_id', 'Parent','trim|regex_match[/^[0-9]+$/]',array('regex_match' => 'Select Valid Category'));

      if ($this->form_validation->run() != false){
        $category_id = $this->input->post('edit_category_id');
        $category_name = $this->input->post('category_name');
        $category_desc = $this->input->post('category_desc');
        $parent_id = $this->input->post('parent_id');
        $status =1;
        if($parent_id == null){
          $parent_id = 0;
        }
        if($parent_id != 0 || $parent_id != null ){
          $find_cat = $em->getRepository('Entity\Category')->find($parent_id);
          if($find_cat != null){
            $status =1;
          }else{$status =0;}
        }
        if($status == 1){
          $find_module = $em->getRepository('Entity\Category')->find($category_id); 
          if($find_module != null){
              $find_module->setCategoryName($category_name);
              $find_module->setCategoryDesc($category_desc);
              $find_module->setParentId($parent_id);
              $em->persist($find_module);
              try {
                $em->flush();
                $this->session->set_flashdata('success', '<p>Category Updated Successfully</p>');
                redirect('index.php/products/category_crud','refresh');
              }catch (UniqueConstraintViolationException $e){
                $this->session->set_flashdata('error', '<p>Category Already Exists</p>');
                $this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
              }
          }else{
            $this->session->set_flashdata('error', '<p>Wrong Category</p>');
            redirect('index.php/products/category_crud','refresh');
          }
        }else{
          $this->session->set_flashdata('error', '<p>Parent Category Not Exists</p>');
          $this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
        }
      }else{
        $this->session->set_flashdata('error', '<p>Error in Fields</p>');
        $this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
      }
    }else{ $this->view_call($main_folder_name,$sub_folder_name,$file_name,$data); }
  }
  public function s_view(){
    $utill_obj = new Common_utill();
    $data['search'] = $utill_obj->search_field('Category', 'getCategoryId', 'getCategoryName', 'index.php/products/category_crud', 'S_view', 'category_id');
    $main_folder_name='products';$sub_folder_name='category';$file_name='S_view';
    $em = $this->doctrine->em;
    if($this->input->post('category_id') != null){
      $this->form_validation->set_rules('category_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
      if ($this->form_validation->run() != false){
        $category_id = $this->input->post('category_id');
        $find_module = $em->getRepository('Entity\Category')->find($category_id); 
        if($find_module != null){
          $parent_obj = $em->getRepository('Entity\Category')->find($find_module->getParentId()); 
          $par_ar = "";
          if($parent_obj != null){
            $par_ar = $parent_obj->getCategoryName();
          }
          $data['parent_name'] =  $par_ar;
          $data['cat_obj'] = $find_module;
          $this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
        }else{
          $this->session->set_flashdata('error', '<p>Wrong Category</p>');
          redirect('index.php/products/category_crud','refresh');
        }
      }else{
        $this->view_call($main_folder_name,$sub_folder_name,$file_name);
      }
    }
  }
  public function delete(){
    $utill_obj = new Common_utill();
    $data['search'] = $utill_obj->search_field('Category', 'getCategoryId', 'getCategoryName', 'index.php/products/category_crud', 'Delete', 'category_id');
    $main_folder_name='products';$sub_folder_name='category';$file_name='Delete';
    $em = $this->doctrine->em;
    if($this->input->post('category_id') != null){
      $this->form_validation->set_rules('category_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
      if ($this->form_validation->run() != false){
        $category_id = $this->input->post('category_id');
        $find_module = $em->getRepository('Entity\Category')->find($category_id); 
        if($find_module != null){
          $parent_obj = $em->getRepository('Entity\Category')->find($find_module->getParentId()); 
          $par_ar = "";
          if($parent_obj != null){
            $par_ar = $parent_obj->getCategoryName();
          }
          $data['parent_name'] =  $par_ar;
          $data['cat_obj'] = $find_module;
          $this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
        }else{
          $this->session->set_flashdata('error', '<p>Wrong Category</p>');
          redirect('index.php/products/category_crud','refresh');
        }
      }else{
        $this->view_call($main_folder_name,$sub_folder_name,$file_name);
      }
    }elseif($this->input->post('delete_category_id') != null){ 
      $this->form_validation->set_rules('delete_category_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
      if ($this->form_validation->run() != false){
        $cat_id = $this->input->post('delete_category_id');
        $find_module = $em->getRepository('Entity\Category')->find($cat_id); 
          if($find_module != null){
            $em->remove($find_module);
            $em->flush();
            $find_sub_cat = $em->getRepository('Entity\Category')->findBy(array('parent_id'=>$cat_id));
            if($find_sub_cat != null){
              foreach ($find_sub_cat as $cat) {
                $find_up_cat = $em->getRepository('Entity\Category')->find($cat->getCategoryId()); 
                if($find_up_cat != null){
                  $find_up_cat->setParentId(0);
                  $em->persist($find_up_cat);
                  $em->flush();
                }
              }
              $this->session->set_flashdata('success', '<p>Category Deleted Successfully</p>');
              redirect('index.php/products/category_crud','refresh');
            }else{
              $this->session->set_flashdata('success', '<p>Category Deleted Successfully</p>');
              redirect('index.php/products/category_crud','refresh');
            }
          }else{
            $this->session->set_flashdata('error', '<p>Wrong Category</p>');
            redirect('index.php/products/category_crud','refresh');
          }
      }else{
        $this->session->set_flashdata('error', '<p>Error in Fields</p>');
        $this->view_call($main_folder_name,$sub_folder_name,$file_name);
      }

    }else{
      $this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
    }
  }
  public function ajax_delete(){
    $em = $this->doctrine->em;
    if($this->input->post('delete_category_id') != null){
      $this->form_validation->set_rules('delete_category_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
      if ($this->form_validation->run() != false){
        $cat_id = $this->input->post('delete_category_id');
        $find_module = $em->getRepository('Entity\Category')->find($cat_id); 

          if($find_module != null){
            $em->remove($find_module);
            $em->flush();
            $find_sub_cat = $em->getRepository('Entity\Category')->findBy(array('parent_id'=>$cat_id));
            if($find_sub_cat != null){
              foreach ($find_sub_cat as $cat) {
                $find_up_cat = $em->getRepository('Entity\Category')->find($cat->getCategoryId()); 
                if($find_up_cat != null){
                  $find_up_cat->setParentId(0);
                  $em->persist($find_up_cat);
                  $em->flush();
                }
              }
              echo 1;
            }else{
              echo 1;
            }
          }else{
            echo -1;
          }
      }else{
        echo 0;
      }
    }else{
      echo 0;
    }
  }
  public function alphabets_and_number($str){
     if (preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str)) {
       return true;
     }
     return false;
  }
  public function find_alphabets($str){
     if (preg_match('#[a-zA-Z]#', $str)) {
       return true;
     }
     return false;
  }
  public function find_numbers($str){
     if (preg_match('#[0-9]#', $str)) {
       return true;
     }
     return false;
  }
  public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
    $this->load->view('header');
    $this->load->view($main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
    $this->load->view('footer');
  }

  public function sample(){
    $main_folder_name='products';$sub_folder_name='category';$file_name='Sample';
    $this->view_call($main_folder_name,$sub_folder_name,$file_name);
  }

}
?>