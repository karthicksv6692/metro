<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\Query\ResultSetMapping;
require_once 'ssp.customized.class.php';
class Vendor_crud extends CI_Controller {

	public function __construct(){
		parent::__construct();
		is_user_logged_in();
	}
	public function index(){ 
		$this->view();
	}
	public function view(){
		$this->load->view('header');
		$this->load->view('products/vendor/View');
		$this->load->view('footer');	
	}
	public function test(){
		if(isset($_POST['lname'])){
			//$this->form_validation->set_rules('lname[]', 'Name','required|min_length[5]|numeric');
			foreach ($_POST['lname'] as $key => $value) {
				$this->form_validation->set_rules('lname['.$key.']', 'Name','required|min_length[5]|numeric');
			}
			if ($this->form_validation->run()!= false){
				echo 'validation success';
			}
			else{
				echo 'validation failed';
				//echo validation_errors();
				echo form_error('lname[0]');
				echo form_error('lname[1]');
			}
		}else{
			$this->load->view('header');
			$this->load->view('products/vendor/add');
			$this->load->view('footer');
		}
	}
	public function datatable_ajax(){
		$CI = &get_instance();
		$CI->load->database();
		$sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
		$table = 'Vendor';
		$primaryKey = 'vendor_id';
		$columns = array(
		    array( 'db' => 'vendor_name', 'dt' => 0 ),
		    array( 'db' => 'vendor_tin_no',  'dt' => 1 ),
		    array( 'db' => 'vendor_pan_no',  'dt' => 2 ),
		    array(
			        'db'        => 'vendor_id',
			        'dt'        => 3,
			        'formatter' => function( $d, $row ) {

	        		$utill_obj = new Common_utill();
					// $edit = $utill_obj->has_access('pincode','edit');
					// $delete = $utill_obj->has_access('pincode','delete');
					// $s_view = $utill_obj->has_access('pincode','s_view');
					$edit = "asd";
					$delete = "asd";
					$s_view = "asd";

					if($s_view != null){
		        		$ss = '<form method="POST" class="icon_edit_form" action="'.base_url().'index.php/products/Vendor_crud/S_view">
                            <input type="hidden" name="vendor_id" value="'.$d.'">
                            <i class="fa fa-search-plus float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
		        	}else{$ss = '';}

		        	if($edit != null){
		        		$ee = ' <form method="POST" class="icon_edit_form" action="'.base_url().'index.php/products/Vendor_crud/Edit">
                            <input type="hidden" name="vendor_id" value="'.$d.'">
                            <i class="fa fa-pencil float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
		        	}else{$ee='';}

		        	if($delete != null){
		        		$dd = '<i class="fa fa-trash-o float_left btn_delete but_del" aria-hidden="true" data-value="'.$d.'" ></i>';
		        	}else{$dd='';}

		        	return $ss.$ee.$dd;
		           
	        	}
		    )
		);
		echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns));
	}
	public function add(){
		
		$main_folder_name='products';$sub_folder_name='vendor';$file_name='Add';
		$em = $this->doctrine->em;
		$utill_obj = new Common_utill();
		$data['pincode_list'] = $em->getRepository('Entity\Pincode')->findAll();
		$data['bank_list'] = $em->getRepository('Entity\Bank_details')->findAll();
		$data['country_list'] = $em->getRepository("Entity\Country")->findAll();
		if($this->input->post('vendor_name') !== null || $this->input->post('ven') != null){ //print_r($_POST);exit();
			$this->form_validation->set_rules('vendor_name', 'Vendor Name','trim|callback_find_alphabets|required|min_length[3]|max_length[255]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]',array('required'=> 'PackageName Requierd','min_length' => 'PackageName needs Minimum 5 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			$this->form_validation->set_rules('full_address', 'Address','trim|required|callback_find_alphabets|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			$this->form_validation->set_rules('pincode_code', 'Pincode','trim|required|regex_match[/^[0-9]+$/]|min_length[4]|max_length[6]',array('regex_match' => 'Numbers Only Allowed'));
			$this->form_validation->set_rules('vendor_tin_no', 'Tin Number','trim|regex_match[/^[0-9]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			$this->form_validation->set_rules('vendor_pan_no', 'Pan Number','callback_alphabets_and_number|trim|required|min_length[10]|max_length[10]|regex_match[/^[a-zA-Z0-9]+$/]',array('regex_match' => 'Numbers Only Allowed','alphabets_and_number'=>'Pan Number Must Containt NUmber and Alphabets'));
			$this->form_validation->set_rules('district', 'District','trim|required|callback_find_alphabets|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			$this->form_validation->set_rules('country', 'Country','trim|required|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));

			//$this->form_validation->set_rules('place', 'Place','trim|required|callback_find_alphabets|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));

			$this->form_validation->set_rules('state', 'State','trim|required|callback_find_alphabets|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			$this->form_validation->set_rules('vendor_bank_ids', 'Bank Ids','trim',array());


			if (is_array($_POST['account_holder_name'])) {
			    foreach ($_POST['account_holder_name'] as $key => $value) {
			        $this->form_validation->set_rules('account_holder_name['.$key.']', 'Something', 'trim|callback_find_alphabets|required|min_length[3]|max_length[255]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]',array('required'=> 'PackageName Requierd','min_length' => 'PackageName needs Minimum 5 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			    }
			}
			if (is_array($_POST['account_number'])) {
			    foreach ($_POST['account_number'] as $key => $value) {
			       $this->form_validation->set_rules('account_number['.$key.']', 'Account No','trim|required|min_length[10]|max_length[20]|regex_match[/^[0-9]+$/]',array('required'=> 'PackageName Requierd','min_length' => 'AccountHolderName needs Minimum 5 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			    }
			}


			if ($this->form_validation->run() != false){ //print_r($_POST);exit();

				$em->getConnection()->beginTransaction();

				$country_id = $this->input->post('country');
				// $country_code = $this->input->post('country_code');
				$pincode_no = $this->input->post('pincode_code');
				$district = $this->input->post('district');
				$place = $this->input->post('place');
				$state = $this->input->post('state');
				$full_address = $this->input->post('full_address');

				$vendor_name = $this->input->post('vendor_name');
				$vendor_tin_no = $this->input->post('vendor_tin_no');
				$vendor_pan_no = $this->input->post('vendor_pan_no');
				$vendor_gst_no = $this->input->post('vendor_gst_no');

				$vendor_contact_name = "primary";
				$vendor_contact_designation = "primary";
				$vendor_contact_mobile_primary = $this->input->post('vendor_contact_mobile_primary');
				$vendor_contact_mobile_secondary = $this->input->post('vendor_contact_mobile_secondary');
				$vendor_contact_email = $this->input->post('vendor_contact_email');
				$vendor_contact_landline = $this->input->post('vendor_contact_landline');

				$address_obj = $utill_obj->add_address_with_pincode_country_id($country_id,$pincode_no,$place,$district,$state,$full_address);

				if($address_obj != null){
					$contact_refer_type=2;$contact_refer=0;
					$contact_obj = $utill_obj->add_contact($contact_refer_type,$contact_refer,$vendor_contact_name,$vendor_contact_designation,$vendor_contact_mobile_primary,$vendor_contact_mobile_secondary,$vendor_contact_email,$vendor_contact_landline);

					if($contact_obj != null){
						$new_vendor_id = $this->add_vendor($vendor_name,$vendor_tin_no,$vendor_pan_no,$vendor_gst_no,$address_obj,$contact_obj);

						if($new_vendor_id != null){
							$referer_type=2;
							for($i=0;$i<count($this->input->post('account_number'));){ 
								$add_account_id = $utill_obj->add_account($this->input->post('account_holder_name')[$i],$this->input->post('account_number')[$i],$this->input->post('account_type')[$i],$em->getRepository('Entity\Bank_details')->findOneBy(array('bank_ifsc' =>$this->input->post('bank_ifsc')[$i])),$new_vendor_id,$referer_type);
								
								if($add_account_id == 1){
									$contact_refer_type=2;
									if($i == count($this->input->post('account_number'))-1){
										if(count($this->input->post('contact_name')[0]) >= 1){
											for($j=0;$j<count($this->input->post('contact_name')[0]);$j++){
												$alter_contact = $utill_obj->add_contact($contact_refer_type,$new_vendor_id,$this->input->post('contact_name')[0][$j],$this->input->post('contact_designation')[1][$j],$this->input->post('contact_mobile_primary')[2][$j],000000,000000,000000);
												if($j == count($this->input->post('contact_name')[0])-1){
													$em->getConnection()->commit();
													$this->session->set_flashdata('success', '<p>Successfully Created</p>');
													redirect('index.php/products/vendor_crud','refresh');
												}
											}
										}else{
											$em->getConnection()->commit();
											$this->session->set_flashdata('success', '<p>Successfully Created</p>');
											redirect('index.php/products/vendor_crud','refresh');
										}
									}
									$i++;
								}elseif($add_account_id == -1){
									$em->getConnection()->rollBack();
									$this->session->set_flashdata('error', '<p>Account Number Already Exists</p>');
									$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
								}else{
									$em->getConnection()->rollBack();
									$this->session->set_flashdata('error', '<p>Invalid IFSC Code</p>');
									$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
								}
							}
						}else{
							$em->getConnection()->rollBack();
							$this->session->set_flashdata('error', '<p>Vendor Already Exists</p>');
							$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
						}
					}else{
						$em->getConnection()->rollBack();
						$this->session->set_flashdata('error', '<p>Contact Insert Error</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					}
				}elseif($address_obj == null){
					$em->getConnection()->rollBack();
					$this->session->set_flashdata('error', '<p>Address Insert Error</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}elseif($address_obj == -2){
					$em->getConnection()->rollBack();
					$this->session->set_flashdata('error', '<p>Pincode Insert Error</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}elseif($address_obj == -1){
					$em->getConnection()->rollBack();
					$this->session->set_flashdata('error', '<p>Country Insert Error</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}
			}else{
				$this->session->set_flashdata('error', '<p>Error in Fields</p>');
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function edit(){
		$main_folder_name='products';$sub_folder_name='vendor';$file_name='Edit';
		$em = $this->doctrine->em;
		$data['pincode_list'] = $em->getRepository("Entity\Pincode")->findAll();
		$data['bank_list'] = $em->getRepository("Entity\Bank_details")->findAll();
		$data['country_list'] = $em->getRepository("Entity\Country")->findAll();
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Vendor', 'getVendorId', 'getVendorName', 'index.php/products/vendor_crud', 'Edit', 'vendor_id');
		if($this->input->post('vendor_id') !== null){
			$this->form_validation->set_rules('vendor_id', 'Vendor id','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$vendor_id = $this->input->post('vendor_id');
				$account_obj = $em->getRepository("Entity\Account_details")->findBy(array('account_refer_id'=>$vendor_id));

				$vendor_obj = $em->find('Entity\Vendor',$vendor_id);
				$contact_obj = $vendor_obj->getVendorContactId();
				$vendor_address_obj = $vendor_obj->getVendorAddressId();
				if($vendor_address_obj != null){
					$vendor_address_pincode_obj = $vendor_address_obj->get_Address_pincode_id();
					if($vendor_address_pincode_obj != null){
						$vendor_address_pincode_country_obj = $vendor_address_pincode_obj->getPincodeCountryId();
					}
				}

				$current_data = array(  'cur_data' => $this->input->post('vendor_id'));
				$this->session->set_userdata($current_data);

				$alter_contact_obj = $em->getRepository("Entity\Contact")->findBy(array('contact_refer_id'=>$vendor_id,'contact_refer_type'=>2));
				$data['obj'] = $vendor_obj;
				$data['account_obj'] = $account_obj;
				$data['alter_contact_obj'] = $alter_contact_obj;
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}else{
				$this->session->set_flashdata('error', '<p>Wrong Vendor ID</p>');
				redirect('index.php/products/vendor_crud','refresh');
			}
		}elseif($this->input->post('edit_vendor_id') !== null){ //print_r($_POST);exit();
			if($_SESSION['cur_data'] == $this->input->post('edit_vendor_id')){
				$vendor_id = $this->input->post('edit_vendor_id');
				$vendor_obj = $em->find('Entity\Vendor',$vendor_id);
				$contact_obj = $vendor_obj->getVendorContactId();
				$vendor_address_obj = $vendor_obj->getVendorAddressId();
				if($vendor_address_obj != null){
					$vendor_address_pincode_obj = $vendor_address_obj->get_Address_pincode_id();
					if($vendor_address_pincode_obj != null){
						$vendor_address_pincode_country_obj = $vendor_address_pincode_obj->getPincodeCountryId();
					}
					$adrs_id = $vendor_address_obj->get_Address_id();
				}else{$vendor_address_obj = null;}
				$cont_id = $contact_obj->getContactId();
				$data['obj'] = $vendor_obj;
				$account_obj = $em->getRepository("Entity\Account_details")->findBy(array('account_refer_id'=>$vendor_id));
				$data['account_obj'] = $account_obj;
				$alter_contact_obj = $em->getRepository("Entity\Contact")->findBy(array('contact_refer_id'=>$vendor_id));
				$data['alter_contact_obj'] = $alter_contact_obj;

				$this->form_validation->set_rules('edit_vendor_id', 'Vendor','trim|regex_match[/^[0-9\-]+$/]|required',array('regex_match' => 'Numbers Only Allowed'));
				$this->form_validation->set_rules('vendor_name', 'Vendor Name','trim|callback_find_alphabets|required|min_length[3]|max_length[255]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]',array('required'=> 'PackageName Requierd','min_length' => 'PackageName needs Minimum 5 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
				$this->form_validation->set_rules('full_address', 'Address','trim|required|callback_find_alphabets|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
				$this->form_validation->set_rules('pincode_code', 'Pincode','trim|required|regex_match[/^[0-9]+$/]|min_length[4]|max_length[6]',array('regex_match' => 'Numbers Only Allowed'));
				$this->form_validation->set_rules('vendor_tin_no', 'Tin Number','trim|regex_match[/^[0-9]+$/]',array('regex_match' => 'Numbers Only Allowed'));
				$this->form_validation->set_rules('vendor_pan_no', 'Pan Number','callback_alphabets_and_number|trim|required|min_length[10]|max_length[10]|regex_match[/^[a-zA-Z0-9]+$/]',array('regex_match' => 'Numbers Only Allowed','alphabets_and_number'=>'Pan Number Must Containt NUmber and Alphabets'));
				$this->form_validation->set_rules('district', 'District','trim|required|callback_find_alphabets|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
				$this->form_validation->set_rules('country', 'Country','trim|required|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));

				$this->form_validation->set_rules('state', 'State','trim|required|callback_find_alphabets|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
				$this->form_validation->set_rules('vendor_bank_ids', 'Bank Ids','trim',array());

				if (is_array($_POST['account_holder_name'])) {
				    foreach ($_POST['account_holder_name'] as $key => $value) {
				        $this->form_validation->set_rules('account_holder_name['.$key.']', 'Something', 'trim|callback_find_alphabets|required|min_length[3]|max_length[255]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]',array('required'=> 'PackageName Requierd','min_length' => 'PackageName needs Minimum 5 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
				    }
				}
				if (is_array($_POST['account_number'])) {
				    foreach ($_POST['account_number'] as $key => $value) {
				       $this->form_validation->set_rules('account_number['.$key.']', 'Account No','trim|required|min_length[10]|max_length[20]|regex_match[/^[0-9]+$/]',array('required'=> 'PackageName Requierd','min_length' => 'AccountHolderName needs Minimum 5 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
				    }
				}

				// $bank_validations = array(
				// 	array(
				//         'field' => 'account_no[]',
				//         'label' => 'Account Number',
				//         'rules' => 'trim|required|min_length[12]|max_length[25]|regex_match[/^[0-9]+$/]',array('regex_match' => 'Numbers Only Allowed')
				//     ),
				//     array(
				//         'field' => 'account_holder_name[]',
				//         'label' => 'Account Holder Name',
				//         'rules' => 'trim|callback_find_alphabets|required|min_length[3]|max_length[255]|regex_match[/^[a-zA-Z0-9._ ,-]+$/]',array('required'=> 'PackageName Requierd','min_length' => 'AccountHolderName needs Minimum 5 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required')
				//     ),
				//     array(
				//         'field' => 'account_type[]',
				//         'label' => 'Account Type',
				//         'rules' => 'trim|required|min_length[1]|max_length[255]|regex_match[/^[0-9]+$/]',array('required'=> 'PackageName Requierd','min_length' => 'AccountType needs Minimum 5 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required')
				//     ),
				//     array(
				//         'field' => 'bank_ifsc[]',
				//         'label' => 'Account Type',
				//         'rules' => 'trim|callback_find_alphabets|required|min_length[6]|max_length[255]|regex_match[/^[a-zA-Z0-9]+$/]',array('required'=> 'IFSC Code Requierd','min_length' => 'IFSC Code needs Minimum 6 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required')
				//     )
				// );
				// $this->form_validation->set_rules($bank_validations);

				if ($this->form_validation->run() != false){
					$em->getConnection()->beginTransaction();
					//print_r($_POST);exit();
					$country_id = $this->input->post('country');
					// $country_code = $this->input->post('country_code');
					$pincode_no = $this->input->post('pincode_code');
					$district = $this->input->post('district');
					$place = $this->input->post('place');
					$state = $this->input->post('state');
					$full_address = $this->input->post('full_address');

					$vendor_name = $this->input->post('vendor_name');
					$vendor_tin_no = $this->input->post('vendor_tin_no');
					$vendor_pan_no = $this->input->post('vendor_pan_no');
					$vendor_gst_no = $this->input->post('vendor_gst_no');

					$vendor_contact_name = "primary";
					$vendor_contact_designation = "primary";
					$vendor_contact_mobile_primary = $this->input->post('vendor_contact_mobile_primary');
					$vendor_contact_mobile_secondary = $this->input->post('vendor_contact_mobile_secondary');
					$vendor_contact_email = $this->input->post('vendor_contact_email');
					$vendor_contact_landline = $this->input->post('vendor_contact_landline');

					$updated_vendor_id = $this->edit_vendor($vendor_id,$vendor_name,$vendor_tin_no,$vendor_pan_no,$vendor_gst_no,$em->getReference('Entity\Address',$vendor_address_obj->get_Address_id()));
					// print_r($updated_vendor_id);exit();
					if($updated_vendor_id != null){
						$pincode_id=null;
						if($vendor_address_pincode_obj != null){
							if($vendor_address_pincode_obj->getPincodeNo() == $pincode_no){
								$pin_id = $vendor_address_pincode_obj->getPincodeId();
								$pincode_id=$pin_id;
							}else{
								$pin_obj = $em->getRepository('Entity\Pincode')->findOneBy(array('pincode_no'=>$pincode_no));
								$pincode_id = $pin_obj->getPincodeId();
							}
						}else{
							$pincode_no = $this->input->post('pincode_no');
							$pin_obj = $em->getRepository('Entity\Pincode')->findOneBy(array('pincode_no'=>$pincode_no));
							$pincode_id = $pin_obj->getPincodeId();
						}
						if($pincode_id != null){
							$address_id = $this->edit_address($adrs_id,$full_address,$em->getReference('Entity\Pincode',$pincode_id));
							if($address_id != null){
								$vendor_contact_refer_id = $updated_vendor_id;$vendor_contact_refer_type = 2;
								$contact_id = $this->edit_contact($cont_id,$vendor_contact_name,$vendor_contact_designation,$vendor_contact_mobile_primary,$vendor_contact_mobile_secondary,$vendor_contact_email,$vendor_contact_landline,$vendor_contact_refer_id,$vendor_contact_refer_type,$address_id);
								if($contact_id != null){ 
									$account_obj = $em->getRepository("Entity\Account_details")->findBy(array('account_refer_id'=>$vendor_id));
									if(count($account_obj)>0){
										for($i=0;$i<count($account_obj);$i++){
											$account = $em->find('Entity\Account_details', $account_obj[$i]->getAccountId());
											$em->remove($account);
											$em->flush();
										}
									}
									if(count($this->input->post('account_number'))>0){
										for($j=0;$j<count($this->input->post('account_number'));$j++){
											$add_account_id = $this->add_account_details($this->input->post('account_number')[$j],$this->input->post('account_type')[$j],$this->input->post('account_holder_name')[$j],$this->input->post('bank_ifsc')[$j],$updated_vendor_id->getVendorId());

											if($add_account_id == 1){
												if($j == count($this->input->post('account_number'))-1){
													$em->flush();
													$contact = $em->getRepository('Entity\Contact')->findBy(array('contact_refer_id' => $updated_vendor_id->getVendorId()));
													if($contact != null){
														for($i=0;$i<count($contact);$i++){
															$em->remove($em->getReference('Entity\Contact',$contact[$i]->getContactId()));
															$em->flush();
														}
														if(count($this->input->post('contact_name')[0])>0){
															for($i=0;$i<count($this->input->post('contact_name')[0]);$i++){
										                        $em=$this->doctrine->em;
										                        $contact = new Entity\Contact;
										                        $contact->setContactName($this->input->post('contact_name')[0][$i]);
										                        $contact->setContactDesignation($this->input->post('contact_designation')[1][$i]);
										                        $contact->setContactMobilePrimary($this->input->post('contact_mobile_primary')[2][$i]);
										                        $contact->setContactMobileSecondary(0000);
										                        $contact->setContactEmailId(0000);
										                        $contact->setContactLandline(0000);
										                        $contact->setContactReferId($updated_vendor_id->getVendorId());
										                        $contact->setContactReferType(2);
										                        $contact->setContactAddressId($address_id);
										                        $em->persist($contact);
										                        $em->flush(); 
										                        if($i == count($this->input->post('contact_name')[0])-1){
																	// echo"successxxx";
																	// print_r($_POST);
										                        	$em->getConnection()->commit();
										                          	$this->session->set_flashdata('success', '<p>Successfully Updated</p>');
										                          	redirect('index.php/products/vendor_crud','refresh');
										                        }
									                      	}
														}else{
															$em->flush(); 
															$em->getConnection()->commit();
															$this->session->set_flashdata('success', '<p>Successfully Updated</p>');
									                      	redirect('index.php/products/vendor_crud','refresh');
														}
													}else{ //print_r($_POST);
														if(count($this->input->post('c_name')[0])>0){
															for($i=0;$i<count($this->input->post('c_name')[0]);$i++){
										                        $em=$this->doctrine->em;
										                        $contact = new Entity\Contact;
										                        $contact->setContactName($this->input->post('c_name')[$i]);
										                        $contact->setContactDesignation($this->input->post('desig')[$i]);
										                        $contact->setContactMobilePrimary($this->input->post('c_mob')[$i]);
										                        $contact->setContactMobileSecondary($this->input->post('c_alter_mob')[$i]);
										                        $contact->setContactEmailId($this->input->post('c_email')[$i]);
										                        $contact->setContactLandline($this->input->post('contact_landline')[$i]);
										                        $contact->setContactReferId($updated_vendor_id->getVendorId());
										                        $contact->setContactReferType(2);
										                        $contact->setContactAddressId($address_id);
										                        $em->persist($contact);
										                        $em->flush(); 
										                        if($i == count($this->input->post('c_name')[0])-1){
																	// echo"successxxx";
																	// print_r($contact);exit();
																	$em->flush(); 
										                        	$em->getConnection()->commit();
										                          	$this->session->set_flashdata('success', '<p>Successfully Updated</p>');
										                          	redirect('index.php/products/vendor_crud','refresh');
										                        }
									                      	}
														}else{
															$em->flush(); 
															$em->getConnection()->commit();
															$this->session->set_flashdata('success', '<p>Successfully Updated</p>');
									                      	redirect('index.php/products/vendor_crud','refresh');
														}
													}
													
												}
											}elseif($add_account_id == -1){
												$this->session->set_flashdata('error', '<p>Account Number Already Exists</p>');
												$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
											}elseif($add_account_id == -2){
												$this->session->set_flashdata('error', '<p>Bank Not Exists</p>');
												$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
											}
										}
									}
									
								}else{
									$this->session->set_flashdata('error', '<p>Vendor Contact Update Failed</p>');
									$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
								}
							}else{
								$this->session->set_flashdata('error', '<p>Vendor Address Not Exixts</p>');
								$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
							}
						}
					}elseif($updated_vendor_id == -1){
						$this->session->set_flashdata('error', '<p>VendorName Already Exixts</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					}else{
						$this->session->set_flashdata('error', '<p>Vendor Not Exixts</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					}
				}else{
					$this->session->set_flashdata('error', '<p>validation Error</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}
			}else{
				$this->session->set_flashdata('error', '<p>Wrong ID</p>');
				redirect('index.php/products/Vendor_crud','refresh');
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function ajax_pincode(){
		$pincode = new Entity\Pincode; 
		$em=$this->doctrine->em;
		// echo $this->input->post('pincode_no');exit();
		$single_pincode = $em->getRepository('Entity\Pincode')->findOneBy(array('pincode_no' => $this->input->post('pincode')));	
		$pincode_name = $single_pincode->getPincodeNo();
		$pincode_place = $single_pincode->getPincodePlace();
		$state = $single_pincode->getPincodeState();
		if($single_pincode->getPincodeCountryId() != null){
			$country = $single_pincode->getPincodeCountryId()->getCountryName();
			$country_code = $single_pincode->getPincodeCountryId()->getCountryCode();
		}
		$district = $single_pincode->getPincodeDistrict();
		$pincode_data = array();
		array_push($pincode_data, array('pincode_no'=>$pincode_name));
		array_push($pincode_data, array('pincode_place'=>$pincode_place));
		array_push($pincode_data, array('pincode_state'=>$state));
		if($single_pincode->getPincodeCountryId() != null){
			array_push($pincode_data, array('pincode_country'=>$country));
			array_push($pincode_data, array('pincode_country_code'=>$country_code));
		}else{
			array_push($pincode_data, array('pincode_country'=>""));
			array_push($pincode_data, array('pincode_country_code'=>""));
		}
		array_push($pincode_data, array('pincode_district'=>$district));
		echo json_encode($pincode_data);
	}
	public function delete(){
		$main_folder_name='products';$sub_folder_name='vendor';$file_name='Delete';
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Vendor', 'getVendorId', 'getVendorName', 'index.php/products/vendor_crud', 'Delete', 'vendor_id');
		if($this->input->post('vendor_id') !== null){
			$this->form_validation->set_rules('vendor_id', 'Vendor id','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$vendor_id = $this->input->post('vendor_id');
				$em=$this->doctrine->em;
				$vendor_object  = $em->find('Entity\Vendor',$vendor_id);
				$address_object  = $em->getRepository('Entity\Address')->find($vendor_object->getVendorAddressId());
				$pincode_object = $address_object->get_Address_pincode_id();
				if($pincode_object != null){
					$country_object = $pincode_object->getPincodeCountryId();
					$pincode_id = $pincode_object->getPincodeId();
					$country_id = $country_object->getCountryId();
				}

				$current_data = array(  'cur_data' => $this->input->post('vendor_id'));
				$this->session->set_userdata($current_data);

				$adrs_id = $address_object->get_Address_id();
				$data['account_obj'] = $em->getRepository("Entity\Account_details")->findBy(array('account_refer_id'=>$vendor_id));
				$data['alter_contact_obj'] = $em->getRepository("Entity\Contact")->findBy(array('contact_refer_id'=>$vendor_id));
				$data['obj'] = $vendor_object;
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}else{
				$this->session->set_flashdata('error', '<p>Wrong Package ID</p>');
				redirect('index.php/products/vendor_crud','refresh');
			}
		}
		elseif($this->input->post('delete_vendor_id') !== null){
			if($_SESSION['cur_data'] == $this->input->post('delete_vendor_id')){
				$this->form_validation->set_rules('delete_vendor_id', 'Vendor','trim|regex_match[/^[0-9\-]+$/]|required',array('regex_match' => 'Numbers Only Allowed'));
				if ($this->form_validation->run() != false){ 
					$em=$this->doctrine->em;
					$em->getConnection()->beginTransaction();
					$vendor_id = $this->input->Post('delete_vendor_id');
					$vendor_object  = $em->find('Entity\Vendor',$vendor_id);
					$address_object  = $em->getRepository('Entity\Address')->find($vendor_object->getVendorAddressId());
					$contact_object  = $em->getRepository('Entity\Contact')->find($vendor_object->getVendorContactId());
					$pincode_object = $address_object->get_Address_pincode_id();
					if($pincode_object != null){
						$country_object = $pincode_object->getPincodeCountryId();
					}
					$account_object  = $em->getRepository('Entity\Account_details')->findOneBy(array('account_refer_id'=>$vendor_object->getVendorId()));

					$adrs_id = $address_object->get_Address_id();
					if($pincode_object != null){
						$pin_id = $pincode_object->getPincodeId();
						$con_id = $country_object->getCountryId();
					}
					if($account_object != null){
						$account_id = $account_object->getAccountId();
					}
					$contact_id = $contact_object->getContactId();

					$deleteted_vendor_id = $this->delete_vendor($vendor_id);   
					if($deleteted_vendor_id == 1){ 
						$deleted_contact_id = $this->delete_contact($contact_id);  
						if($deleted_contact_id == 1){ 
							$deleted_address_id = $this->delete_address($adrs_id);  

							if($deleted_address_id == 1){  
								$em = $this->doctrine->em;
								$alter_contact = $em->getRepository('Entity\Contact')->findBy(array('contact_refer_id' =>$vendor_id));
								if(count($alter_contact)>0){
									for($i=0;$i<count($alter_contact);){
										$em->remove($alter_contact[$i]);
										if($i == count($alter_contact)-1){
											try{
												$em->flush();
												$em->getConnection()->commit();
												$this->session->set_flashdata('success', '<p>Successfully Deleted</p>');
						                   		redirect('index.php/products/vendor_crud','refresh');
											}catch(ForeignKeyConstraintViolationException $e){
												$em->getConnection()->rollBack();
												$this->session->set_flashdata('error', '<p>Contact Delete Failed</p>');
												redirect('index.php/products/vendor_crud','refresh');
											}
										}
										$i++;
									}
								}else{
									$em->flush();
									$em->getConnection()->commit();
									$this->session->set_flashdata('success', '<p>Successfully Deleted</p>');
			                   		redirect('index.php/products/vendor_crud','refresh');
								}
							}else{
								$this->session->set_flashdata('error', '<p>Contact Delete Failed</p>');
								redirect('index.php/products/vendor_crud','refresh');
							}
						}else{
							$this->session->set_flashdata('error', '<p>Pincode Delete Failed</p>');
							redirect('index.php/products/vendor_crud','refresh');
						}
					}else{
						$this->session->set_flashdata('error', '<p>ForeignKey Constrain. Delete Faild</p>');
						redirect('index.php/products/vendor_crud','refresh');
					}
				}else{
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}
			}else{
				$this->session->set_flashdata('error', '<p>Wrong ID</p>');
				redirect('index.php/products/packages_crud','refresh');
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
		public function ajax_delete(){
		$em = $this->doctrine->em;
		if($this->input->post('delete_vendor_id')!= null){
			$this->form_validation->set_rules('delete_vendor_id', 'Package id','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run()!= false){
				$vendor_id = $this->input->Post('delete_vendor_id');
					$vendor_object  = $em->find('Entity\Vendor',$vendor_id);
					$address_object  = $em->getRepository('Entity\Address')->find($vendor_object->getVendorAddressId());
					$contact_object  = $em->getRepository('Entity\Contact')->find($vendor_object->getVendorContactId());
					$pincode_object = $address_object->get_Address_pincode_id();
					if($pincode_object != null){
						$country_object = $pincode_object->getPincodeCountryId();
					}
					$account_object  = $em->getRepository('Entity\Account_details')->findOneBy(array('account_refer_id'=>$vendor_object->getVendorId()));

					$adrs_id = $address_object->get_Address_id();
					if($pincode_object != null){
						$pin_id = $pincode_object->getPincodeId();
						$con_id = $country_object->getCountryId();
					}
					if($account_object != null){
						$account_id = $account_object->getAccountId();
					}
					$contact_id = $contact_object->getContactId();

					$deleteted_vendor_id = $this->delete_vendor($vendor_id);   
					if($deleteted_vendor_id == 1){ 
						$deleted_contact_id = $this->delete_contact($contact_id);  
						if($deleted_contact_id == 1){ 
							$deleted_address_id = $this->delete_address($adrs_id);  

							if($deleted_address_id == 1){  
								$em = $this->doctrine->em;
								$alter_contact = $em->getRepository('Entity\Contact')->findBy(array('contact_refer_id' =>$vendor_id));
								if(count($alter_contact)>0){
									for($i=0;$i<count($alter_contact);){
										$em->remove($alter_contact[$i]);
										if($i == count($alter_contact)-1){
											try{
												$em->flush();
												$em->getConnection()->commit();
												echo 1;
											}catch(ForeignKeyConstraintViolationException $e){echo 0;}
										}
										$i++;
									}
								}else{echo 1;}
							}else{echo 0;}
						}else{echo 0;}
					}else{echo 0;}
			}else{
				echo 0;
			}
		}else{
			echo 0;
		}
	}


	public function edit_vendor($vendor_id,$vendor_name,$vendor_tin_no,$v_pan_no,$v_gst_no,$address_id){
	  	$em=$this->doctrine->em;
		$vendor_exists = $em->find('Entity\Vendor',$vendor_id);
		if($vendor_exists != null){
			$vendor_exists->setVendorName($vendor_name);
		  	$vendor_exists->setVendorAddressId($address_id);
		  	$vendor_exists->setVendorTinNo($vendor_tin_no);
		  	$vendor_exists->setVendorPanNo($v_pan_no);
		  	$vendor_exists->setVendorGstNo($v_gst_no);
			$em->persist($vendor_exists);
			try {
				$em->flush();
				return $em->getReference('Entity\Vendor',$vendor_exists->getVendorId());
		   	}
		   	catch(UniqueConstraintViolationException $e){
		   		return false;
		   	}
		}else{
			return false;
		}
	}
	
	public function s_view(){
		$main_folder_name='products';$sub_folder_name='vendor';$file_name='S_view';
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Vendor', 'getVendorId', 'getVendorName', 'index.php/products/vendor_crud', 'S_view', 'vendor_id');
		if($this->input->post('vendor_id') !== null){
			$this->form_validation->set_rules('vendor_id', 'Vendor id','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$vendor_id = $this->input->post('vendor_id');
				$em=$this->doctrine->em;
				$vendor_object  = $em->find('Entity\Vendor',$vendor_id); 
				$address_object  = $em->getRepository('Entity\Address')->find($vendor_object->getVendorAddressId());
				$pincode_object = $address_object->get_Address_pincode_id();
				$country_object = $pincode_object->getPincodeCountryId();
				$pincode_id = $pincode_object->getPincodeId();
				$country_id = $country_object->getCountryId();
				$adrs_id = $address_object->get_Address_id();
				$data['account_obj'] = $em->getRepository("Entity\Account_details")->findBy(array('account_refer_id'=>$vendor_id));
				$data['alter_contact_obj'] = $em->getRepository("Entity\Contact")->findBy(array('contact_refer_id'=>$vendor_id));
				$data['obj'] = $vendor_object;
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}else{
				$this->session->set_flashdata('error', '<p>Wrong Package ID</p>');
				redirect('index.php/products/vendor_crud','refresh');
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function alphabets_and_number($str){
	   if (preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function find_alphabets($str){
	   if (preg_match('#[a-zA-Z]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function find_numbers($str){
	   if (preg_match('#[0-9]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
		$this->load->view('header');
		$this->load->view(''.$main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
		$this->load->view('footer');
	}
	public function add_vendor($vendor_name,$vendor_tin_no,$ven_pan_no,$ven_gst_no,$address_id,$contact_obj){
		$vendor = new Entity\Vendor;
	  	$em=$this->doctrine->em;
	  	$find_vendor = $em->getRepository('Entity\Vendor')->findOneBy(array('vendor_name' => $vendor_name));
		if($find_vendor !== null){
			return false;
		}else{
		  	$vendor->setVendorName($vendor_name);
		  	$vendor->setVendorAddressId($address_id);
		  	$vendor->setVendorContactId($contact_obj);
		  	$vendor->setVendorTinNo($vendor_tin_no);
		  	$vendor->setVendorPanNo($ven_pan_no);
		  	$vendor->setVendorGstNo($ven_gst_no);
			$em->persist($vendor);
			try {
				$em->flush();
				// return $em->getReference('Entity\Vendor',$vendor->getVendorId());
				return $vendor->getVendorId();
		   	}
		   	catch(UniqueConstraintViolationException $e){
		   		$em->getConnection()->rollBack();
		   		return false;
		   	}
		}
	}
	public function edit_address($address_id,$full_address,$pincode_id){
		$em=$this->doctrine->em;
		$address_exists = $em->find('Entity\Address',$address_id);
		if($address_exists !== null){
		  	$address_exists->set_Address_full_address($full_address);
		  	$address_exists->set_Address_pincode_id($pincode_id);
		  	$em = $this->doctrine->em;
			$em->persist($address_exists);
			$em->flush();
			// return $em->getReference('Entity\Address',$address_exists->get_Address_id());
			return $address_exists->get_Address_id();
		}else{
			$em->getConnection()->rollBack();
			return false;
		}
	}
	public function edit_contact($contact_id,$contact_name,$contact_designation,$contact_mobile_primary,$contact_mobile_secondary,$contact_email,$contact_landline,$contact_refer_id,$contact_refer_type,$address_id){
		$em=$this->doctrine->em;
		$contact_exists = $em->find('Entity\Contact',$contact_id);
		if($contact_exists !== null){
			$contact_exists->setContactName($contact_name);
		  	$contact_exists->setContactDesignation($contact_designation);
		  	$contact_exists->setContactMobilePrimary($contact_mobile_primary);
		  	$contact_exists->setContactMobileSecondary($contact_mobile_secondary);
		  	$contact_exists->setContactEmailId($contact_email);
		  	$contact_exists->setContactLandline($contact_landline);
		  	$contact_exists->setContactAddressId($address_id);
		  	$contact_exists->setContactRefereId($contact_refer_id);
		  	$contact_exists->setContactRefereType($contact_refer_type);
		  	$em = $this->doctrine->em;
			$em->persist($contact_exists);
			return $em->getReference('Entity\Contact',$contact_exists->getContactId());
			$em->flush();
		}else{
			$em->getConnection()->rollBack();
			return false;
		}
	}
	public function add_account_details($account_no,$account_type,$account_holder_name,$ifsc,$new_vendor_id){
		$em=$this->doctrine->em;
		$account = new Entity\Account_details;
		$find_bank = $em->getRepository('Entity\Bank_details')->findOneBy(array('bank_ifsc' => $ifsc));
		if($find_bank != null){
			$bank_id = $em->getReference('Entity\Bank_details',$find_bank->getBankId());
			$find_account = $em->getRepository('Entity\Account_details')->findOneBy(array('account_no' => $account_no));
			if($find_account == null){
				$account->setAccountNo($account_no);
				$account->setAccountType($account_type);
				$account->setBankId($bank_id);
				$account->setAccountHolderName($account_holder_name);
				$account->setAccountReferId($new_vendor_id);
				$account->setAccountReferType(2);
				$em->persist($account);
				try {
					$em->flush();
					return true;
					// return $em->getReference('Entity\Account_details',$account->getAccountId());
				}catch (UniqueConstraintViolationException $e){
					$em->getConnection()->rollBack();
					return false;
				}
			}else{
				return -1;
			}
		}else{
			return -2;
		}
	}
	public function delete_account($account_id){ //return true;
		$em = $this->doctrine->em;
		$del_account=$em->find('Entity\Account_details',$account_id);
		$em->remove($del_account);
		try{
			$em->flush();
			return true;
		}catch(ForeignKeyConstraintViolationException $e){
			return false;
		}
	}
	public function delete_contact($contact_id){
		$em = $this->doctrine->em;
		$del_contact=$em->find('Entity\Contact',$contact_id);
		$em->remove($del_contact);
		try{
			$em->flush();
			return true;
		}catch(ForeignKeyConstraintViolationException $e){
			return false;
		}
	}
	public function delete_address($address_id){ //return true;
		$em = $this->doctrine->em;
		$del_address=$em->find('Entity\Address',$address_id);
		$em->remove($del_address);
		try{
			$em->flush();
			return true;
		}catch(ForeignKeyConstraintViolationException $e){
			return false;
		}
	}
	public function delete_vendor($vendor_id){
		$em = $this->doctrine->em;
		$del_vendor = $em->find('Entity\Vendor',$vendor_id);
		$em->remove($del_vendor);
		try{
			$em->flush();
			return true;
		}catch(ForeignKeyConstraintViolationException $e){
			return false;
		}
	}
}
	
?>