<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\Query\ResultSetMapping;
require_once 'ssp.customized.class.php';
class Package_purchase extends CI_Controller {

	protected static $leftcnt=0;
	protected static 	$rightcnt=0;
  protected static $findobj=null;

  protected static $hunbvi_left=0;
	protected static $hunbvi_right=0;

  protected static $ar=array();
  protected static $obj_ar=array();

	public function __construct(){
		parent::__construct();
	}
	public function index(){
		$main_folder_name='package_purchase';$sub_folder_name='purchase';$file_name='Genology';
		$this->view_call($main_folder_name,$sub_folder_name,$file_name);
	}
	
	public function add(){
		$main_folder_name='package_purchase';$sub_folder_name='purchase';$file_name='Add';

		if($this->input->post('pin') !== null){ //print_r($_POST);exit();
				$utill_obj = new Common_utill();
				$em=$this->doctrine->em;
				
				$pin_no = $this->input->post('pin');
				$em->getConnection()->beginTransaction();

				$for = 0;
				$obj = $utill_obj->check_pin_get_details($pin_no,$for);

				//print_r($obj->getPinRequestedUserId()->getUserId());exit();
				if( !is_numeric($obj) && $obj != null){

					$user_refere_obj = $obj->getPinRequestedUserId();
					$sponser_user_id = $user_refere_obj->getUserId();
					$sponser_user_purchase_id = $utill_obj->get_single_row_using_array_value('User_package_purchase','user_id',$sponser_user_id)->getUserPurchaseId();
					$sponcer_gene_side = $this->input->post('sponcer_gene_side');
					$actual_gene_side = $this->input->post('actual_gene_side');
					$user_purchase_pin_id = $obj;


					$user_obj = $obj->getPinRequestedUserId()->getUserName();
					$req_user_id = $obj->getPinRequestedUserId()->getUserId();

					$req_user_purchase_obj = $utill_obj->get_single_row_using_array_value('User_package_purchase','user_id',$req_user_id);

					$used_user_id = $this->input->post('pin_used_user_id');

					$left_pair_count = 0;
					$right_pair_count = 0;
					$pin_used_user_obj = $utill_obj->get_reference_obj("User",$used_user_id);
					
					$sponcer_gene_side = $this->input->post('sponcer_gene_side');
					$actual_gene_side = $this->input->post('actual_gene_side');

					$sponcer_user_puchase_id = $req_user_purchase_obj->getUserPurchaseId();

					// Find Actual Gene Start
					$actual_gene_up_id = 0;
						$actual_obj = $utill_obj->get_node_has_children($sponser_user_purchase_id);

						if($actual_obj != -5){
							if($actual_obj == 2 || $actual_obj == 3 || $actual_obj == 4 ){
								$actual_gene_up_id = $sponcer_user_puchase_id;
							}else{
								if($actual_gene_side == 0){
									//Find Left Empty Nodes related to the root_id (sponcer_id)
									echo "left way------------------";
									$left_ar = $utill_obj->get_left_empty_node_xx($sponcer_user_puchase_id,0);
									if($left_ar != -5){
										$actual_gene_up_id = $left_ar;
									}
									
								}else{
									//Find Right Empty Nodes related to the root_id (sponcer_id)
									echo "right way------------------";
									$right_ar = $utill_obj->get_left_empty_node_xx($sponcer_user_puchase_id,1);

									if($right_ar != -5){
										$actual_gene_up_id = $right_ar;
									}
								}
							}
						}
						//echo $sponcer_user_puchase_id;exit();
					// Find Actual Gene End

					$pack_obj = new Entity\User_package_purchase;
					$pack_obj->setUserPurchasePinId($obj);

					$pack_obj->setUserId($pin_used_user_obj);
					$pack_obj->setUserPurchaseSponserUserPurchaseId($sponcer_user_puchase_id);
					$pack_obj->setUserPurchaseActualGeneUserPurchaseId($actual_gene_up_id);
					$pack_obj->setUserPurchaseSponserGeneSide($sponcer_gene_side);
					$pack_obj->setUserPurchasePinId($obj);
					$pack_obj->setUserPurchaseActualGeneSide($actual_gene_side);
					$pack_obj->setUserPurchaseGeneLeftPurchaseId(0);
					$pack_obj->setUserPurchaseGeneRightPurchaseId(0);
					$pack_obj->setUserPruchasePairUserPurchaseId(0);
					$pack_obj->setUserPurchaseMoneyAlloted(0);
					$pack_obj->setUserPurchaseTransactionId($utill_obj->get_reference_obj('Transactions',1));
					

					$em->persist($pack_obj);
					try {

						$em->flush();
						$set_sub = $utill_obj->set_sub_node($actual_gene_up_id,$actual_gene_side, $pack_obj->getUserPurchaseId());
						// echo $set_sub;exit();
						if( $set_sub == 1){
							$em->getConnection()->commit();
							echo "SUccess";
						}else{
							$em->getConnection()->rollBack();
							echo "Error in Common_utill /set_sub_node_sample_upp/ Funtion";
						}
						
					}catch (UniqueConstraintViolationException $e){ 
						$em->getConnection()->rollBack();
						echo "Catch Errror";
					}
					
				}else{
					$em->getConnection()->rollBack();
					print_r($obj);echo"Errror";
				}
			  	
			
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name);
		}
	}

	public function getRightLeftCount($obj1,$id){
		self::$leftcnt=0;
		self::$rightcnt=0;
    if($obj1->getLeftObj()!=null){
      self::$leftcnt=1;
		  $this->printTreeL($obj1->getLeftObj(),$id);
    }
    if($obj1->getRightObj()!=null){
      self::$rightcnt=1;  
      $this->printTreeR($obj1->getRightObj(),$id);
    }
    $op_ar = array(
      'tot_right_count' =>self::$rightcnt,
      'tot_left_count' =>self::$leftcnt,
      'hunbv_left_count' =>self::$hunbvi_left,
      'hunbv_right_count' =>self::$hunbvi_right
    );
		return $op_ar;
	}
    

	public function printTreeL($obj1,$id){
		if($obj1->getLeftObj()!=null){
			self::$leftcnt++;
			$this->printTreeL($obj1->getLeftObj(),$id);
		}
		if($obj1->getRightObj()!=null){
			self::$leftcnt++;
			$this->printTreeL($obj1->getRightObj(),$id);
		}
	}

  public function printTreeR($obj1,$id){
    if($obj1->getLeftObj()!=null){
      self::$rightcnt++;
      $this->printTreeR($obj1->getLeftObj(),$id);
    }
    if($obj1->getRightObj()!=null){
      self::$rightcnt++;
      $this->printTreeR($obj1->getRightObj(),$id);
    }
  }

	public function findIDInTree($obj1,$id){
		$this->findTree($obj1,$id);
		return self::$findobj;
	}

	public function findTree($obj1,$id){
		if($obj1->getPurchaseId() == $id){
			self::$findobj=$obj1;
		}
		if($obj1->getLeftObj()!=null){
			$this->findTree($obj1->getLeftObj(),$id);
		}
		if($obj1->getRightObj()!=null){
			$this->findTree($obj1->getRightObj(),$id);
		}
	}
	
	public function get_next_node_ajax(){
		if($this->input->post('id') != null){
			$obj1 = $this->get_tree_obj($this->input->post('id'));
			$obj=$this->findIDInTree($obj1,$this->input->post('id'));
     
			$cnt = $this->getRightLeftCount($obj1,$this->input->post('id'));
      // print_r($cnt);
			if($obj != null){
				$html = '<div class="row rr" style="margin-top: 2em;">
							<div class="col-lg-6 col-md-6 pull-left">
								<div class="gj_tree_l_count">
									<span class="gj_tree_left_cnt">Total Left Side Count : <span class="gj_l_cnt_no">'.$cnt["tot_left_count"].'</span></span>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 pull-left">
								<div class="gj_tree_r_count">
									<span class="gj_tree_right_cnt">Total Right Side Count : <span class="gj_r_cnt_no">'.$cnt["tot_right_count"].'</span></span>
								</div>
							</div>
						</div>';

				$html .='<div class="row">
							<div class="col-md-12">
								<div class="gj_first_tree gj_tree_vws">
									<p>'.$obj->getUserId().'</p>';
                  if(! is_numeric($obj->getImage())){
                    $html .='<img src="'.base_url($obj1->getImage()).'" alt="tree">';
                  }else{
                    $html .='<img src="'.base_url('assets/images/default_user.png').'" alt="tree">';
                  }
									 $html .='<p>'.$obj->getUserName().'</p>
									<p>'.$obj->getFirstName().'</p>
									<p class="fa fa-clock-o">'.$obj->getDate()->format('Y-m-d h:m:s').'</p>
                  <div class="gj_tree_table_vw">
                    <div class="gj_cutoff_table gj_tree_resp table-responsive">
                        <table id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th class="text-center gj_tree_downline" colspan="9">Downline BV Details For 00001</th>
                            </tr>
                            <tr>
                              <th class="text-center" rowspan="2">Package Name</th>
                              <th class="text-center" colspan="2">Total</th>
                              <th class="text-center" colspan="2">Personal</th>
                              <th class="text-center" colspan="2">Current</th>
                              <th class="text-center" colspan="2">Carry Forward</th>
                            </tr>
                            <tr>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                            </tr>
                        </thead>
                        <tbody class="pck_bdy">
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <input type="hidden" class="iid" value="'.$obj->getPurchaseId().'">
								</div>
							
							</div>
						</div>';

				$html .= '<div class="row gj_row_bot">';
					$html .= '<div class="col-md-6 col-lg-6 gj_tree_vw1_struct">';
						if($obj->getLeftObj() != null){
							$html .='<div class="gj_first_tree gj_tree_vws" onclick="get_next_node('.$obj->getLeftObj()->getPurchaseId().')">
									<p>'.$obj->getLeftObj()->getUserId().'</p>';
                   if(! is_numeric($obj->getLeftObj()->getImage())){
                      $html .='<img src="'.base_url($obj->getLeftObj()->getImage()).'" alt="tree">';
                    }else{
                      $html .='<img src="'.base_url('assets/images/default_user.png').'" alt="tree">';
                    }
									$html .= '<p>'.$obj->getLeftObj()->getUserName().'</p>
									<p>'.$obj->getLeftObj()->getFirstName().'</p>
									<p class="fa fa-clock-o">'.$obj->getLeftObj()->getDate()->format('Y-m-d h:m:s').'</p>
                  <div class="gj_tree_table_vw">
                    <div class="gj_cutoff_table gj_tree_resp table-responsive">
                        <table id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th class="text-center gj_tree_downline" colspan="9">Downline BV Details For 00001</th>
                            </tr>
                            <tr>
                              <th class="text-center" rowspan="2">Package Name</th>
                              <th class="text-center" colspan="2">Total</th>
                              <th class="text-center" colspan="2">Personal</th>
                              <th class="text-center" colspan="2">Current</th>
                              <th class="text-center" colspan="2">Carry Forward</th>
                            </tr>
                            <tr>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                            </tr>
                        </thead>
                        <tbody class="pck_bdy">
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <input type="hidden" class="iid" value="'.$obj->getLeftObj()->getPurchaseId().'">
								</div>
								';
						}else{
							$html .='<div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
										<div class="gj_tree_id_div gj_tree_id">-----</div>
										<img src="'.base_url('assets/images/default_user.png').'" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
										<p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>';
                    if($obj->getPurchaseId() != null){
                      $html .= '<form method="post" action="'.base_url('index.php/registration/cus_select_enter_pin').'" class="subrt_cls">
                                   <a onclick=$(this).closest("form").submit()>Add New User</a>
                                  <input type="hidden" name="top_id" value="'.$obj->getUserId().'">
                                   <input type="hidden" name="pos" value="0">
                                </form>';
                    }
									 $html .= '</div>';
						}
					$html .= '</div>';

					$html .= '<div class="col-md-6 col-lg-6 gj_tree_vw1_struct">';
						if($obj->getRightObj() != null){
							$html .='<div class="gj_first_tree gj_tree_vws" onclick="get_next_node('.$obj->getRightObj()->getPurchaseId().')">
									<p>'.$obj->getRightObj()->getUserId().'</p>';
                   if(! is_numeric($obj->getRightObj()->getImage())){
                      $html .='<img src="'.base_url($obj->getRightObj()->getImage()).'" alt="tree">';
                    }else{
                      $html .='<img src="'.base_url('assets/images/default_user.png').'" alt="tree">';
                    }
                  $html .= '<p>'.$obj->getRightObj()->getUserName().'</p>
									<p>'.$obj->getRightObj()->getFirstName().'</p>
									<p class="fa fa-clock-o">'.$obj->getRightObj()->getDate()->format('Y-m-d h:m:s').'</p>
                  <div class="gj_tree_table_vw" style="top: auto; left: auto; z-index: 99; bottom: 60px; right: 0;">
                    <div class="gj_cutoff_table gj_tree_resp table-responsive">
                        <table id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th class="text-center gj_tree_downline" colspan="9">Downline BV Details For 00001</th>
                            </tr>
                            <tr>
                              <th class="text-center" rowspan="2">Package Name</th>
                              <th class="text-center" colspan="2">Total</th>
                              <th class="text-center" colspan="2">Personal</th>
                              <th class="text-center" colspan="2">Current</th>
                              <th class="text-center" colspan="2">Carry Forward</th>
                            </tr>
                            <tr>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                            </tr>
                        </thead>
                        <tbody class="pck_bdy">
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <input type="hidden" class="iid" value="'.$obj->getRightObj()->getPurchaseId().'">
								</div>';
						}else{
							$html .='<div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
										<div class="gj_tree_id_div gj_tree_id">-----</div>
										<img src="'.base_url('assets/images/default_user.png').'" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
										<p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>';
                     if($obj->getPurchaseId() != null){
                      $html .= '<form method="post" action="'.base_url('index.php/registration/cus_select_enter_pin').'" class="subrt_cls">
                                   <a onclick=$(this).closest("form").submit()>Add New User</a>
                                  <input type="hidden" name="top_id" value="'.$obj->getUserId().'">
                                   <input type="hidden" name="pos" value="1">
                                </form>';
                    }
									 $html .= '</div>';
						}
					$html .= '</div>';
				$html .= '</div>';


				$html .= '<div class="row gj_row_bot">';
					$html .= '<div class="col-md-3 col-lg-3 gj_tree_vw1_struct">';
						if($obj->getLeftObj() != null){
							if($obj->getLeftObj()->getLeftObj() != null){
								$html .='<div class="gj_first_tree gj_tree_vws" onclick="get_next_node('.$obj->getLeftObj()->getLeftObj()->getPurchaseId().')">
									<p>'.$obj->getLeftObj()->getLeftObj()->getUserId().'</p>';
                   if(! is_numeric($obj->getLeftObj()->getLeftObj()->getImage())){
                      $html .='<img src="'.base_url($obj->getLeftObj()->getLeftObj()->getImage()).'" alt="tree">';
                    }else{
                      $html .='<img src="'.base_url('assets/images/default_user.png').'" alt="tree">';
                    }
                  $html .= '<p>'.$obj->getLeftObj()->getLeftObj()->getUserName().'</p>
									<p>'.$obj->getLeftObj()->getLeftObj()->getFirstName().'</p>
									<p class="fa fa-clock-o">'.$obj->getLeftObj()->getLeftObj()->getDate()->format('Y-m-d h:m:s').'</p>
                  <div class="gj_tree_table_vw">
                    <div class="gj_cutoff_table gj_tree_resp table-responsive">
                        <table id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th class="text-center gj_tree_downline" colspan="9">Downline BV Details For 00001</th>
                            </tr>
                            <tr>
                              <th class="text-center" rowspan="2">Package Name</th>
                              <th class="text-center" colspan="2">Total</th>
                              <th class="text-center" colspan="2">Personal</th>
                              <th class="text-center" colspan="2">Current</th>
                              <th class="text-center" colspan="2">Carry Forward</th>
                            </tr>
                            <tr>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                            </tr>
                        </thead>
                        <tbody class="pck_bdy">
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <input type="hidden" class="iid" value="'.$obj->getLeftObj()->getLeftObj()->getPurchaseId().'">
								</div>
								';
							}else{
								$html .='<div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
											<div class="gj_tree_id_div gj_tree_id">-----</div>
											<img src="'.base_url('assets/images/default_user.png').'" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
											<p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>';
                       if($obj->getLeftObj() != null){
                          $html .= '<form method="post" action="'.base_url('index.php/registration/cus_select_enter_pin').'" class="subrt_cls">
                                       <a onclick=$(this).closest("form").submit()>Add New User</a>
                                      <input type="hidden" name="top_id" value="'.$obj->getLeftObj()->getUserId().'">
                                       <input type="hidden" name="pos" value="0">
                                    </form>';
                        }
										$html .= '</div>';
							}
						}else{
							$html .='<div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
										<div class="gj_tree_id_div gj_tree_id">-----</div>
										<img src="'.base_url('assets/images/default_user.png').'" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
										<p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
									</div>';
						}
					$html .= '</div>';

					$html .= '<div class="col-md-3 col-lg-3 gj_tree_vw1_struct">';
						if($obj->getLeftObj() != null){
							if($obj->getLeftObj()->getRightObj() != null){
								$html .='<div class="gj_first_tree gj_tree_vws" onclick="get_next_node('.$obj->getLeftObj()->getRightObj()->getPurchaseId().')">
									<p>'.$obj->getLeftObj()->getRightObj()->getUserId().'</p>';
                   if(! is_numeric($obj->getLeftObj()->getRightObj()->getImage())){
                      $html .='<img src="'.base_url($obj->getLeftObj()->getRightObj()->getImage()).'" alt="tree">';
                    }else{
                      $html .='<img src="'.base_url('assets/images/default_user.png').'" alt="tree">';
                    }
                  $html .= '<p>'.$obj->getLeftObj()->getRightObj()->getUserName().'</p>
									<p>'.$obj->getLeftObj()->getRightObj()->getFirstName().'</p>
									<p class="fa fa-clock-o">'.$obj->getLeftObj()->getRightObj()->getDate()->format('Y-m-d h:m:s').'</p>
                  <div class="gj_tree_table_vw">
                    <div class="gj_cutoff_table gj_tree_resp table-responsive">
                        <table id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th class="text-center gj_tree_downline" colspan="9">Downline BV Details For 00001</th>
                            </tr>
                            <tr>
                              <th class="text-center" rowspan="2">Package Name</th>
                              <th class="text-center" colspan="2">Total</th>
                              <th class="text-center" colspan="2">Personal</th>
                              <th class="text-center" colspan="2">Current</th>
                              <th class="text-center" colspan="2">Carry Forward</th>
                            </tr>
                            <tr>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                            </tr>
                        </thead>
                        <tbody class="pck_bdy">
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <input type="hidden" class="iid" value="'.$obj->getLeftObj()->getRightObj()->getPurchaseId().'">
								</div>
								';
							}else{
								$html .='<div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
											<div class="gj_tree_id_div gj_tree_id">-----</div>
											<img src="'.base_url('assets/images/default_user.png').'" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
											<p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>';
										   if($obj->getLeftObj() != null){
                      $html .= '<form method="post" action="'.base_url('index.php/registration/cus_select_enter_pin').'" class="subrt_cls">
                                   <a onclick=$(this).closest("form").submit()>Add New User</a>
                                  <input type="hidden" name="top_id" value="'.$obj->getLeftObj()->getUserId().'">
                                   <input type="hidden" name="pos" value="1">
                                </form>';
                      }
                    $html .= '</div>';
							}
						}else{
							$html .='<div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
										<div class="gj_tree_id_div gj_tree_id">-----</div>
										<img src="'.base_url('assets/images/default_user.png').'" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
										<p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
									</div>';
						}
					$html .= '</div>';

					$html .= '<div class="col-md-3 col-lg-3 gj_tree_vw1_struct">';
						if($obj->getRightObj() != null){
							if($obj->getRightObj()->getLeftObj() != null){
								$html .='<div class="gj_first_tree gj_tree_vws" onclick="get_next_node('.$obj->getRightObj()->getLeftObj()->getPurchaseId().')">
									<p>'.$obj->getRightObj()->getLeftObj()->getUserId().'</p>';
                   if(! is_numeric($obj->getRightObj()->getLeftObj()->getImage())){
                      $html .='<img src="'.base_url($obj->getRightObj()->getLeftObj()->getImage()).'" alt="tree">';
                    }else{
                      $html .='<img src="'.base_url('assets/images/default_user.png').'" alt="tree">';
                    }
                  $html .= '<p>'.$obj->getRightObj()->getLeftObj()->getUserName().'</p>
									<p>'.$obj->getRightObj()->getLeftObj()->getFirstName().'</p>
									<p class="fa fa-clock-o">'.$obj->getRightObj()->getLeftObj()->getDate()->format('Y-m-d h:m:s').'</p>
                  <div class="gj_tree_table_vw" style="top: auto; left: auto; z-index: 99; bottom: 60px; right: 0;">
                    <div class="gj_cutoff_table gj_tree_resp table-responsive">
                        <table id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th class="text-center gj_tree_downline" colspan="9">Downline BV Details For 00001</th>
                            </tr>
                            <tr>
                              <th class="text-center" rowspan="2">Package Name</th>
                              <th class="text-center" colspan="2">Total</th>
                              <th class="text-center" colspan="2">Personal</th>
                              <th class="text-center" colspan="2">Current</th>
                              <th class="text-center" colspan="2">Carry Forward</th>
                            </tr>
                            <tr>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                            </tr>
                        </thead>
                        <tbody class="pck_bdy">
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <input type="hidden" class="iid" value="'.$obj->getRightObj()->getLeftObj()->getPurchaseId().'">
								</div>
								';
							}else{
								$html .='<div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
											<div class="gj_tree_id_div gj_tree_id">-----</div>
											<img src="'.base_url('assets/images/default_user.png').'" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
											<p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>';
										   if($obj->getRightObj() != null){
                          $html .= '<form method="post" action="'.base_url('index.php/registration/cus_select_enter_pin').'" class="subrt_cls">
                                       <a onclick=$(this).closest("form").submit()>Add New User</a>
                                      <input type="hidden" name="top_id" value="'.$obj->getRightObj()->getUserId().'">
                                       <input type="hidden" name="pos" value="0">
                                    </form>';
                        }
                      $html .= '</div>';
							}
						}else{
							$html .='<div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
										<div class="gj_tree_id_div gj_tree_id">-----</div>
										<img src="'.base_url('assets/images/default_user.png').'" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
										<p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>';
									if($obj->getRightObj() != null){
                    $html .= '<form method="post" action="");">
                                 <a onclick="$(this).closest("form").submit()">Add New User</a>
                                <input type="hidden" name="top_id" value="'.$obj->getRightObj()->getUserId().'">
                                 <input type="hidden" name="pos" value="1">
                              </form>';
                  }
                  $html .= '</div>';
						}
					$html .= '</div>';

					$html .= '<div class="col-md-3 col-lg-3 gj_tree_vw1_struct">';
						if($obj->getRightObj() != null){
							if($obj->getRightObj()->getRightObj() != null){
								$html .='<div class="gj_first_tree gj_tree_vws" onclick="get_next_node('.$obj->getRightObj()->getRightObj()->getPurchaseId().')">
									<p>'.$obj->getRightObj()->getRightObj()->getUserId().'</p>';
                   if(! is_numeric($obj->getRightObj()->getRightObj()->getImage())){
                      $html .='<img src="'.base_url($obj->getRightObj()->getRightObj()->getImage()).'" alt="tree">';
                    }else{
                      $html .='<img src="'.base_url('assets/images/default_user.png').'" alt="tree">';
                    }
                  $html .= '<p>'.$obj->getRightObj()->getRightObj()->getUserName().'</p>
									<p>'.$obj->getRightObj()->getRightObj()->getFirstName().'</p>
									<p class="fa fa-clock-o">'.$obj->getRightObj()->getRightObj()->getDate()->format('Y-m-d h:m:s').'</p>
                  <div class="gj_tree_table_vw" style="top: auto; left: auto; z-index: 99; bottom: 60px; right: 0;">
                    <div class="gj_cutoff_table gj_tree_resp table-responsive">
                        <table id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th class="text-center gj_tree_downline" colspan="9">Downline BV Details For 00001</th>
                            </tr>
                            <tr>
                              <th class="text-center" rowspan="2">Package Name</th>
                              <th class="text-center" colspan="2">Total</th>
                              <th class="text-center" colspan="2">Personal</th>
                              <th class="text-center" colspan="2">Current</th>
                              <th class="text-center" colspan="2">Carry Forward</th>
                            </tr>
                            <tr>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                            </tr>
                        </thead>
                        <tbody class="pck_bdy">
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <input type="hidden" class="iid" value="'.$obj->getRightObj()->getRightObj()->getPurchaseId().'">
								</div>
								';
							}else{
								$html .='<div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
											<div class="gj_tree_id_div gj_tree_id">-----</div>
											<img src="'.base_url('assets/images/default_user.png').'" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
											<p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>';
										  if($obj->getRightObj() != null){
                          $html .= '<form method="post" action="'.base_url('index.php/registration/cus_select_enter_pin').'" class="subrt_cls">
                                       <a onclick=$(this).closest("form").submit()>Add New User</a>
                                      <input type="hidden" name="top_id" value="'.$obj->getRightObj()->getUserId().'">
                                       <input type="hidden" name="pos" value="1">
                                    </form>';
                        }
                   $html .= '</div>';
							}
						}else{
							$html .='<div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
										<div class="gj_tree_id_div gj_tree_id">-----</div>
										<img src="'.base_url('assets/images/default_user.png').'" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
										<p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
									</div>';
						}
					$html .= '</div>';
				$html .= '</div>';
				echo json_encode($html);
			}else{echo 0;}
	  }
	}

	public function get_tree_obj($id){
		$utill_obj = new Common_utill();
		$em = $this->doctrine->em;

		$res = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$id);

		$uid = $res->getUserId()->getUserId();

		$un = $res->getUserId()->getUserName();
		$fn = $res->getUserId()->getFirstName();

		$attachment  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $uid, 'attachment_referer_type'=>1));
		$img = 0;
		if($attachment != null){
			if($attachment[0]->getAttachmentImageUrl() != null || $attachment[0]->getAttachmentImageUrl() != -1){
				$img = $attachment[0]->getAttachmentImageUrl();
			}else{ $img = 0; }
		}else{ $img = 0; }

		$pid = $res->getUserPurchaseId();
		$ct = $res->getUserId()->getCreatedAt();

    $pin_obj = $res->getUserPurchasePinId();
    $package_obj = $pin_obj->getPinRequestPackageId();
    if($package_obj != null){
      $package_id = $package_obj->getPackageId();
      $package_name = $package_obj->getPackageName();
      $package_price = $package_obj->getPackagePrice();
    }else{$package_id=0;$package_name=0;$package_price=0;}

		$obj = new Entity\Geanology();
		$obj->setPurchaseId($pid);
		$obj->setUserId($uid);
		$obj->setUserName($un);
		$obj->setFirstName($fn);
		$obj->setImage($img);
		$obj->setDate($ct);
    $obj->setPackageId($package_id);
    $obj->setPackageName($package_name);
    $obj->setPackagePrice($package_price);
    $obj->setRootId($res->getUserPurchaseActualGeneUserPurchaseId());

		if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
			$obj->setLeftObj($this->get_tree_obj($res->getUserPurchaseGeneLeftPurchaseId()));
		}
		if($res->getUserPurchaseGeneRightPurchaseId() != 0){
			$obj->setRightObj($this->get_tree_obj($res->getUserPurchaseGeneRightPurchaseId()));
		}
		return $obj;
	}

	public function tree(){
		$main_folder_name='package_purchase';$sub_folder_name='purchase';$file_name='Geanology_2';
    $utill_obj = new Common_utill();
    $res = $utill_obj->get_single_row_using_array_value('User_package_purchase','user_id',$_SESSION['user_id']);

		$obj1 = $this->get_tree_obj($res->getUserPurchaseId());
		$data['obj'] = $obj1;
		$xx = $this->getRightLeftCount($obj1,$_SESSION['user_id'] );
    
		$data['left_count'] = $xx['tot_left_count'];
    $data['right_count'] = $xx['tot_right_count'];

  //   $data['hunbv_left_count'] = $xx['hunbv_left_count'];
		// $data['hunbv_right_count'] = $xx['hunbv_right_count'];

    $this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
	}

	public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
		$this->load->view('header');
		$this->load->view(''.$main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
		$this->load->view('footer');
	}

  public function ajax_single_package_count(){
    if($this->input->post('id') != null){
      $id = $this->input->post('id');
      $obj1 = $this->get_tree_tree_obj($id);
      $obj = $this->payout_all($obj1);
      // print_r($obj);exit();
      $pack_ids = $this->input->post('pack_id');
      $html ="";
      $ar = explode(" ",$pack_ids);
      for($i=0;$i<count($ar);$i++){
        $ct = $this->get_left_right_package_count($obj1,$ar[$i]);
        self::$hunbvi_left=0;
        self::$hunbvi_right=0;
        $ctt = $this->get_left_right_package_count($obj1,13);
        self::$hunbvi_left=0;
        self::$hunbvi_right=0;
        $html .= '<tr><td>100Bvi</td>';
          $html .= '<td>'.$ct[0].'</td>';
          $html .= '<td>'.$ct[1].'</td>'; 
          $html .= '<td>0</td>';
          $html .= '<td>0</td>';

          $html .= '<td>0</td>';
          $html .= '<td>0</td>';

          $html .= '<td>0</td>';
          $html .= '<td>0</td>';

        $html .= '</tr>';
        $html .= '<tr><td>200Bvi</td>';
          $html .= '<td>'.$ctt[0].'</td>';
          $html .= '<td>'.$ctt[1].'</td>'; 
          $html .= '<td>0</td>';
          $html .= '<td>0</td>';

          $html .= '<td>0</td>';
          $html .= '<td>0</td>';

          $html .= '<td>0</td>';
          $html .= '<td>0</td>';

        $html .= '</tr>';
      }
    }

    echo json_encode($html);
  }
  public function payout_all($obj){ //print_r($obj);exit();
    if(! is_numeric($obj->getLeftObj()) && ! is_numeric($obj->getRightObj())){
      if($obj->getLeftObj()->getPackageId() == $obj->getLeftObj()->getPackageId()){

          $new_price = $obj->getLeftObj()->getPackagePrice()/10;
          $objj = new Entity\Sample_payout_price();
          $objj->setPurchaseId($obj->getPurchaseId());
          $objj->setUserName($obj->getUserName());
          $objj->setUserId($obj->getUserId());
          $objj->setPrice($new_price);
          $objj->setPair(1);
         array_push(self::$ar,$objj);

          $left_root_id = $obj->getLeftObj()->getRootId();
          $right_root_id = $obj->getRightObj()->getRootId();
          if($left_root_id != 0){
            $v = $this->get_root($left_root_id);
          }
          if($right_root_id != 0){
            $v = $this->get_root($right_root_id);
          }
          if(!is_numeric($obj->getLeftObj())){
            $this->payout_all($obj->getLeftObj());
          }
          if(!is_numeric($obj->getRightObj())){
            $this->payout_all($obj->getRightObj());
          }
      }
    }else{
     
      $objj = new Entity\Sample_payout_price();
      $objj->setPurchaseId($obj->getPurchaseId());
      $objj->setUserName($obj->getUserName());
      $objj->setUserId($obj->getUserId());
      $objj->setPrice(0);
      $objj->setPair(0);
      array_push(self::$ar,$objj);
      if(!is_numeric($obj->getLeftObj())){
        $this->payout_all($obj->getLeftObj());
      }
      if(!is_numeric($obj->getRightObj())){
        $this->payout_all($obj->getRightObj());
      }
    }
    return self::$ar;
  }
  public function get_root($p_id){
      foreach (self::$obj_ar as $aa) {
        if($aa->getPurchaseId() == $p_id){
            // print_r($aa->getPurchaseId());
            if($aa->getRootId() != 0){
              foreach (self::$ar as $as) {
                if($as->getPurchaseId() == $aa->getPurchaseId()){
                  $as->setPrice($as->getPrice()+200);
                  $as->setPair($as->getPair()+1);
                }
              }
              // $this->get_root($aa->getPurchaseId());
            }else{
              foreach (self::$ar as $as) {
                if($as->getPurchaseId() == $aa->getPurchaseId()){
                  $as->setPrice($as->getPrice()+200);
                  $as->setPair($as->getPair()+1);
                }
              }
            }
        }
      }
      return true;
  }

  public function get_tree_tree_obj($id){
      $utill_obj = new Common_utill();
      $em = $this->doctrine->em;
      $res = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$id);

      $pin_obj = $res->getUserPurchasePinId();
      $package_obj = $pin_obj->getPinRequestPackageId();
      if($package_obj != null){
        $package_id = $package_obj->getPackageId();
        $package_name = $package_obj->getPackageName();
        $package_price = $package_obj->getPackagePrice();
      }else{$package_id=0;$package_name=0;$package_price=0;}

      $uid = $res->getUserId()->getUserId();
      $un = $res->getUserId()->getUserName();

      $obj = new Entity\Sample_payout();
      $obj->setPurchaseId($res->getUserPurchaseId());
      $obj->setUserName($un);
      $obj->setUserId($uid);
      $obj->setPackageId($package_id);
      $obj->setPackageName($package_name);
      $obj->setPackagePrice($package_price);
      $obj->setRootId($res->getUserPurchaseActualGeneUserPurchaseId());

      if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
        $obj->setLeftObj($this->get_tree_tree_obj($res->getUserPurchaseGeneLeftPurchaseId()));
      }else{$obj->setLeftObj(0);}
      if($res->getUserPurchaseGeneRightPurchaseId() != 0){
        $obj->setRightObj($this->get_tree_tree_obj($res->getUserPurchaseGeneRightPurchaseId()));
      }else{$obj->setRightObj(0);}
      
      return $obj;
  }
	public function get_left_right_package_count($obj,$id){ //print_r($obj);exit();
      if($obj->getLeftObj() != null){
            if($obj->getLeftObj()->getPackageId() == $id){
              self::$hunbvi_left=1;
            }
            $this->printTreePackageL($obj->getLeftObj(),$id);
          
      }
      if($obj->getRightObj() != null){
            if($obj->getRightObj()->getPackageId() == $id){
              self::$hunbvi_right=1;
            }
            $this->printTreePackageR($obj->getRightObj(),$id);
           
      }
      return [self::$hunbvi_left,self::$hunbvi_right];
  }
  public function printTreePackageL($obj,$id){

        if($obj->getLeftObj()!= null)
        {
          if($obj->getLeftObj()->getPackageId() == $id){
            self::$hunbvi_left++;
            $this->printTreePackageL($obj->getLeftObj(),$id);
          }

        }
        if($obj->getRightObj() != null)
        {
          if($obj->getRightObj()->getPackageId() == $id){
            self::$hunbvi_left++;
            $this->printTreePackageL($obj->getRightObj(),$id);
          }
        }
  }
  public function printTreePackageR($obj,$id){

        if($obj->getLeftObj() != null)
        {
          if($obj->getLeftObj()->getPackageId() == $id){
            self::$hunbvi_right++;
            $this->printTreePackageR($obj->getLeftObj(),$id);
          }

        }
        if($obj->getRightObj() != null)
        {
          if($obj->getRightObj()->getPackageId() == $id){
            self::$hunbvi_right++;
            $this->printTreePackageR($obj->getRightObj(),$id);
          }
        }
  } 

}

?>