<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

require_once 'ssp.customized.class.php';

class Common_controller extends CI_Controller {

	protected static $id = array();
  protected static $ar_obj = array();
	protected static $reverse_tree_ar = array();

	public function __construct() {
		parent::__construct();
	}
	public function ajax_pincode() {
		$pincode = new Entity\Pincode;
		$em = $this->doctrine->em;
		$single_pincode = $em->getRepository('Entity\Pincode')->findOneBy(array('pincode_no' => $this->input->post('pincode')));
		if ($single_pincode != null) {
			$pincode_name = $single_pincode->getPincodeNo();
			$pincode_place = $single_pincode->getPincodePlace();
			$state = $single_pincode->getPincodeState();
			$district = $single_pincode->getPincodeDistrict();
			$country = $single_pincode->getPincodeCountryId()->getCountryName();
			$country_code = $single_pincode->getPincodeCountryId()->getCountryCode();
			$country_id = $single_pincode->getPincodeCountryId()->getCountryId();
			$pincode_data = array();
			array_push($pincode_data, array('pincode_no' => $pincode_name));
			array_push($pincode_data, array('pincode_place' => $pincode_place));
			array_push($pincode_data, array('pincode_district' => $district));
			array_push($pincode_data, array('pincode_state' => $state));
			array_push($pincode_data, array('pincode_country' => $country));
			array_push($pincode_data, array('pincode_country_code' => $country_code));
			array_push($pincode_data, array('country_id' => $country_id));
			echo json_encode($pincode_data);
		} else {echo 0;}
	}
	public function user_name_available() {
		$table_name = "User";
		$field_name = "user_name";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		$name_check = $utill->check_availability($table_name, $field_name, $field_value);
		$id_check = $utill->check_availability($table_name, 'user_id', $field_value);
		if ($id_check == 1 && $name_check == 1) {
			echo 1;
		} else {echo 0;}
	}
	public function user_email_available() {
		$table_name = "User";
		$field_name = "user_email";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name, $field_name, $field_value);
	}
	public function user_mobile_available() {
		$table_name = "Contact";
		$field_name = "contact_mobile_primary";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name, $field_name, $field_value);
	}
	public function country_name_available() {
		$table_name = "Country";
		$field_name = "country_name";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name, $field_name, $field_value);
	}
	public function country_code_available() {
		$table_name = "Country";
		$field_name = "country_code";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name, $field_name, $field_value);
	}
	public function pincode_available() {
		$table_name = "Pincode";
		$field_name = "pincode_no";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name, $field_name, $field_value);
	}
	public function ifsc_available() {
		$table_name = "Bank_details";
		$field_name = "bank_ifsc";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name, $field_name, $field_value);
	}
	public function module_name_available() {
		$table_name = "Module";
		$field_name = "module_name";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name, $field_name, $field_value);
	}
	public function category_name_available() {
		$table_name = "Category";
		$field_name = "category_name";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name, $field_name, $field_value);
	}
	public function role_name_available() {
		$table_name = "User_role";
		$field_name = "user_role_name";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name, $field_name, $field_value);
	}
	public function vendor_name_available() {
		$table_name = "Vendor";
		$field_name = "vendor_name";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name, $field_name, $field_value);
	}
	public function vendor_tin_no_available() {
		$table_name = "Vendor";
		$field_name = "vendor_tin_no";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name, $field_name, $field_value);
	}
	public function vendor_pan_no_available() {
		$table_name = "Vendor";
		$field_name = "vendor_pan_no";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name, $field_name, $field_value);
	}
	public function vendor_gst_no_available() {
		$table_name = "Vendor";
		$field_name = "vendor_gst_no";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name, $field_name, $field_value);
	}
	public function product_code_available() {
		$table_name = "Product";
		$field_name = "product_code";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name, $field_name, $field_value);
	}
	public function package_product_name_available() {
		$table_name = "Package_products";
		$field_name = "package_product_name";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name, $field_name, $field_value);
	}

	public function get_available_bill_no() {
		$utill = new Common_utill();
		$res = $utill->get_last_record();
		if (!is_integer($res)) {
			$val = preg_replace("/[^0-9]/", "", $res);
			$res = $val + 1;
			$rr = "B" . '' . $res . '';
			$utill->set_last_record($rr);
			echo $rr;
		} else {
			$new_val = "B" . '' . date("Ymdh") . '' . "01";
			$utill->set_last_record($new_val);
			echo $new_val;
		}
	}

	public function country_name_available_on_update() {
		$utill = new Common_utill();
		$table_name = "Country";
		$field_name = "country_name";
		$field_value = $this->input->post('field_value');
		$primary_key = 'country_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name, $field_name, $field_value, $primary_key, $key_value);
	}
	public function country_code_available_on_update() {
		$utill = new Common_utill();
		$table_name = "Country";
		$field_name = "country_code";
		$field_value = $this->input->post('field_value');
		$primary_key = 'country_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name, $field_name, $field_value, $primary_key, $key_value);
	}
	public function pincode_available_on_update() {
		$utill = new Common_utill();
		$table_name = "Pincode";
		$field_name = "pincode_no";
		$field_value = $this->input->post('field_value');
		$primary_key = 'pincode_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name, $field_name, $field_value, $primary_key, $key_value);
	}
	public function ifsc_available_on_update() {
		$utill = new Common_utill();
		$table_name = "Bank_details";
		$field_name = "bank_ifsc";
		$field_value = $this->input->post('field_value');
		$primary_key = 'bank_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name, $field_name, $field_value, $primary_key, $key_value);
	}
	public function module_name_available_on_update() {
		$utill = new Common_utill();
		$table_name = "Module";
		$field_name = "module_name";
		$field_value = $this->input->post('field_value');
		$primary_key = 'module_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name, $field_name, $field_value, $primary_key, $key_value);
	}
	public function category_name_available_on_update() {
		$utill = new Common_utill();
		$table_name = "Category";
		$field_name = "category_name";
		$field_value = $this->input->post('field_value');
		$primary_key = 'category_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name, $field_name, $field_value, $primary_key, $key_value);
	}
	public function user_name_available_on_update() {
		$utill = new Common_utill();
		$table_name = "User";
		$field_name = "user_name";
		$field_value = $this->input->post('field_value');
		$primary_key = 'user_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name, $field_name, $field_value, $primary_key, $key_value);
	}
	public function user_email_available_on_update() {
		$utill = new Common_utill();
		$table_name = "User";
		$field_name = "user_email";
		$field_value = $this->input->post('field_value');
		$primary_key = 'user_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name, $field_name, $field_value, $primary_key, $key_value);
	}
	public function user_mobile_available_on_update() {
		$utill = new Common_utill();
		$table_name = "Contact";
		$field_name = "contact_mobile_primary";
		$field_value = $this->input->post('field_value');
		$primary_key = 'contact_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name, $field_name, $field_value, $primary_key, $key_value);
	}
	public function user_role_name_available_on_update() {
		$utill = new Common_utill();
		$table_name = "User_role";
		$field_name = "user_role_name";
		$field_value = $this->input->post('field_value');
		$primary_key = 'user_role_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name, $field_name, $field_value, $primary_key, $key_value);
	}
	public function vendor_name_available_on_update() {
		$utill = new Common_utill();
		$table_name = "Vendor";
		$field_name = "vendor_name";
		$field_value = $this->input->post('field_value');
		$primary_key = 'vendor_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name, $field_name, $field_value, $primary_key, $key_value);
	}

	public function vendor_tin_no_available_on_update() {
		$utill = new Common_utill();
		$table_name = "Vendor";
		$field_name = "vendor_tin_no";
		$field_value = $this->input->post('field_value');
		$primary_key = 'vendor_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name, $field_name, $field_value, $primary_key, $key_value);
	}

	public function vendor_pan_no_available_on_update() {
		$utill = new Common_utill();
		$table_name = "Vendor";
		$field_name = "vendor_pan_no";
		$field_value = $this->input->post('field_value');
		$primary_key = 'vendor_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name, $field_name, $field_value, $primary_key, $key_value);
	}

	public function vendor_gst_no_available_on_update() {
		$utill = new Common_utill();
		$table_name = "Vendor";
		$field_name = "vendor_gst_no";
		$field_value = $this->input->post('field_value');
		$primary_key = 'vendor_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name, $field_name, $field_value, $primary_key, $key_value);
	}
	public function product_code_available_on_update() {
		$utill = new Common_utill();
		$table_name = "Product";
		$field_name = "product_code";
		$field_value = $this->input->post('field_value');
		$primary_key = 'product_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name, $field_name, $field_value, $primary_key, $key_value);
	}
	public function package_product_name_available_on_update() {
		$utill = new Common_utill();
		$table_name = "Package_products";
		$field_name = "package_product_name";
		$field_value = $this->input->post('field_value');
		$primary_key = 'package_product_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name, $field_name, $field_value, $primary_key, $key_value);
	}

	/*-------------jalin added fns*/
	public function package_name_available() {
		$table_name = "Packages";
		$field_name = "package_name";
		$field_value = $this->input->post('field_value');
		$utill = new Common_utill();
		echo $utill->check_availability($table_name, $field_name, $field_value);
	}
	public function package_name_available_on_update() {
		$utill = new Common_utill();
		$table_name = "Packages";
		$field_name = "package_name";
		$field_value = $this->input->post('field_value');
		$primary_key = 'package_id';
		$key_value = $this->input->post('pk');
		echo $utill->check_availability_on_update($table_name, $field_name, $field_value, $primary_key, $key_value);
	}

	public function get_products() {
		$utill = new Common_utill();
		$prd = $utill->get_available_product();
		echo json_encode($prd);
	}
	public function get_ifsc() {

    $utill = new Common_utill();
    $bank_ar = $utill->get_ifsc_list($this->input->post('key'));

    echo json_encode($bank_ar);
  }
  public function get_pincode() {

		$utill = new Common_utill();
		$bank_ar = $utill->get_pincode_list($this->input->post('key'));

		echo json_encode($bank_ar);
	}

	public function get_all_packages_ajax() {
    $utill = new Common_utill();
    $val = 0;
    if ($this->input->post('val') != null) {
      $val = $this->input->post('val');
    }
    $pack = $utill->get_packages($val);

    echo json_encode($pack);
  }
  public function get_all_packages_name() {
		$utill = new Common_utill();
		$pack = $utill->get_all_packages_name();
		echo json_encode($pack);
	}
	public function get_products_specific_vendor() {
		$utill = new Common_utill();
		if ($this->input->post('id') != 0) {
			$prd = $utill->get_available_product_spc_ven($this->input->post('id'));
			echo json_encode($prd);
		} else {
			$this->get_products();
		}
	}
	public function get_products_with_price_range() {
		$utill = new Common_utill();
		if ($this->input->post('range') != 0) {

		} else {
			$this->get_products();
		}
	}
	public function get_bank_details_from_bank() {
		$id = $this->input->post('id');
		$utill = new Common_utill();
		$bank_ar = $utill->get_single_row_using_primary_key('Bank_details', $id);
		$ar = array('bank_name' => $bank_ar->getBankName(), 'bank_branch' => $bank_ar->getBankBranch());
		echo json_encode($ar);
	}
  public function get_pincode_details() {
    $id = $this->input->post('id');
    $utill = new Common_utill();
    $c_name = '';
    $c_id = '';
    $pincode_ar = $utill->get_single_row_using_array_value('Pincode','pincode_no', $id);
    if($pincode_ar != null){
      if($pincode_ar->getPincodeCountryId() != null){
        $c_id = $pincode_ar->getPincodeCountryId()->getCountryId();
        $c_name = $pincode_ar->getPincodeCountryId()->getCountryName();
      }
      $ar = array('pincode_id' => $pincode_ar->getPincodeId(),'state' => $pincode_ar->getPincodeState(), 'district' => $pincode_ar->getPincodeDistrict(), 'country_name' => $c_name , 'country_id' => $c_id);
      echo json_encode($ar);
    }else{echo 0;}
  }
	public function add_to_cart() {
		$utill = new Common_utill();
		if ($this->input->post('id') != null) {
			$get = $utill->get_product_detail_from_product_id_for_cart($this->input->post('id'));

			if ($get != 0) {
				$user_quantity = 1;
				if ($this->input->post('quantity') != null) {
					$user_quantity = $this->input->post('quantity');
				}
				$ar = array('user_quantity' => $user_quantity, 'user_price' => $get['product_display_price'] * $user_quantity, 'user_pv' => $get['point_value'] * $user_quantity);
				$aa = array_merge($get, $ar);
				$price = 0;
				$pv = 0;
        if(isset($_SESSION['cart_items'])){
          for ($i = 0; $i < count($_SESSION['cart_items']); $i++) {
            $price += $_SESSION['cart_items'][$i]['user_price'];
            $pv += $_SESSION['cart_items'][$i]['user_pv'];
          }
          $cnt = array('tot' => count($_SESSION['cart_items']) + 1, 'cart_tot_price' => $price + ($user_quantity * $aa['product_display_price']), 'cart_tot_pv' => $pv + ($user_quantity * $aa['point_value']));
        }else{
          $cnt = array('tot' => 1, 'cart_tot_price' => $price + ($user_quantity * $aa['product_display_price']), 'cart_tot_pv' => $pv + ($user_quantity * $aa['point_value']));
        }
  				

  				
  				$aa = array_merge($aa, $cnt);
  				$bb = $this->session->userdata('cart_items');
  				if($bb != null){
  				  $key = array_search($this->input->post('id'), array_column($bb, 'product_id'));
          }else{$bb = array();}

  				if (count($bb) > 0) {
  					if (is_int($key)) {
  						echo 1;
  					} else {
  						array_push($bb, $aa);
  						$this->session->set_userdata('cart_items', $bb);
  						echo json_encode($aa);
  					}
  				} else {
  					array_push($bb, $aa);
  					$this->session->set_userdata('cart_items', $bb);
  					echo json_encode($aa);
  				}

			} else {echo 0;}
		}
	}
	public function add_to_cart_cnt($id, $user_quantity) {
		$utill = new Common_utill();

		$get = $utill->get_product_detail_from_product_id_for_cart($id);
		// return $get;
		if ($get != 0) {
			$bb = $this->session->userdata('cart_items');
      $key = array_search($id, array_column($bb, 'product_id'));


			if (is_int($key)) {
				$_SESSION['cart_items'][$key]['user_quantity'] = $user_quantity;
				$_SESSION['cart_items'][$key]['user_price'] = $get['product_display_price'] * $user_quantity;
				$_SESSION['cart_items'][$key]['user_pv'] = $get['point_value'] * $user_quantity;
				$price = 0;
				$pv = 0;
				for ($i = 0; $i < count($_SESSION['cart_items']); $i++) {
					$price += $_SESSION['cart_items'][$i]['user_price'];
					$pv += $_SESSION['cart_items'][$i]['user_pv'];
				}
				$_SESSION['cart_items'][$key]['cart_tot_price'] = $price;
				$_SESSION['cart_items'][$key]['cart_tot_pv'] = $pv;

				return $_SESSION['cart_items'][$key];
			} else {return 0;}
		} else {return 0;}

	}
	public function remove_from_cart() {
		$bb = $this->session->userdata('cart_items');
		$key = array_search($this->input->post('id'), array_column($bb, 'product_id'));
		if (is_int($key)) {
			array_splice($bb, $key, 1);
			$this->session->set_userdata('cart_items', $bb);

			$price = 0;
			$pv = 0;
			for ($i = 0; $i < count($_SESSION['cart_items']); $i++) {
				$price += $_SESSION['cart_items'][$i]['user_price'];
				$pv += $_SESSION['cart_items'][$i]['user_pv'];
			}

			$cnt = array('status' => 1, 'tot' => count($_SESSION['cart_items']), 'cart_tot_price' => $price, 'cart_tot_pv' => $pv);
			echo json_encode($cnt);
		}
	}

	public function quantitiy_change() {
		$bb = $this->session->userdata('cart_items');
		$id = $this->input->post('id');
		$cnt_val = $this->input->post('cnt_val');
		echo json_encode($this->add_to_cart_cnt($id, $cnt_val));

	}

	public function unset_sess() {
		$this->session->unset_userdata('cart_items');
		$cart_items = array();
		$this->session->set_userdata('cart_items', $cart_items);
	}

	public function test_img_up() {
		$this->load->helper('string');
		$utill = new Common_utill();
		$file = $_FILES['profile_picture'];
		$upload_url = 'attachments';
		if (!is_dir($upload_url)) {
			mkdir($upload_url);
		}
		$file_name = 'user_' . random_string('alnum', 8);
		$upload_data = $utill->upload_single_compressed_image($file, $upload_url, $file_name);

		if ($upload_data != null) {
			return $upload_data[0];
		}
	}
	public function test_img_up_file() {
		$this->load->view('sample/sample/Img_upload');
	}

	public function get_category_names() {
		$key = $this->input->get('q');
		$em = $this->doctrine->em;
		$bnk_ar = array();

		$query = $em->createQuery("SELECT b.category_id,b.category_name FROM Entity\Category as b WHERE b.category_name LIKE '%$key%'")->setMaxResults(5);

    $single_bank = $query->getResult();
		$json = [];
		for ($i = 0; $i < count($single_bank); $i++) {
			$json[] = ['id' => $single_bank[$i]['category_id'], 'text' => $single_bank[$i]['category_name']];
		}
		echo json_encode($json);
	}
	public function get_category_names_on_edit() {
		$id = $this->input->get('id');
		$key = $this->input->get('q');
		$em = $this->doctrine->em;
		$bnk_ar = array();

		$query = $em->createQuery("SELECT b.category_id,b.category_name FROM Entity\Category as b WHERE b.category_name LIKE '%$key%' AND b.category_id != " . $id)->setMaxResults(5);

		$single_bank = $query->getResult();
		$json = [];
		for ($i = 0; $i < count($single_bank); $i++) {
			$json[] = ['id' => $single_bank[$i]['category_id'], 'text' => $single_bank[$i]['category_name']];
		}
		echo json_encode($json);
	}

	public function sample_ajax_select() {
		$key = $this->input->get('q');
		$em = $this->doctrine->em;
		$bnk_ar = array();

		$query = $em->createQuery("SELECT b.bank_id,b.bank_ifsc FROM Entity\Bank_details as b WHERE b.bank_ifsc LIKE '%$key%'")->setMaxResults(5);

		$single_bank = $query->getResult();
		$json = [];
		for ($i = 0; $i < count($single_bank); $i++) {
			$json[] = ['id' => $single_bank[$i]['bank_id'], 'text' => $single_bank[$i]['bank_ifsc']];
		}

		echo json_encode($json);
	}

	public function get_products_from_cat_id() {
		$utill = new Common_utill();
		$prd = $utill->get_products_by_cat_id($this->input->post('cat_id'), $this->input->post('pck_id'));

        

		if (!is_integer($prd)) {
      $ar = array();
      for($i=0;$i<count($prd);$i++){
        array_push($ar,$utill->get_product_detail_from_product_id_for_cart($prd[$i]['product_id']));
      } 
			$html = '';
      foreach ($ar as $pp) {
				$html .= '<div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 s_xs_width box wow bounceInDown center">
          <div class="s_product_item1">';
            if(is_numeric($pp['product_image_url'])){ 
              $html .= '<img src="'.base_url('attachments/no_image.jpg').'" alt="product-image" class="img-responsive s_img_wid_height">';
            }else{ 
              $html .= '<img src="'.base_url().$pp['product_image_url'].'" alt="product-image" class="img-responsive s_img_wid_height">';
            } 
            
            $html .= '<div class="s_pro_rate_bg">
              <span class="s_product_rupee"><b>₹ ' . $pp['product_price'] . '</b></span>
              <p class="s_product_offerrupee">' . $pp['point_value'] . '  PV</p>
              <p class="s_product_name1">' . $pp['product_name'] . '</p>
              <p class="s_product_shortdescription">' . $pp['product_short_desc'][0]. '</p>

              <div class="gj_s_p_hd text-center">';
				$ser = array_search($pp['product_id'], array_column($_SESSION['cart_items'], 'product_id'));
				if (!is_numeric($ser)) {
					$html .= '<button class="gj_cart_but btn-success" id="bt_' . $pp['product_id'] . '" type="button" onclick="common_add_remove(this,' . $pp['product_id'] . ',1)"><i class="fa fa-check"></i> Add Cart</button>';
				} else {
					$html .= '<button class="gj_cart_but btn-danger" id="bt_' . $pp['product_id'] . '" type="button" onclick="common_add_remove(this,' . $pp['product_id'] . ',1)"><i class="fa fa-close"></i> Remove</button>';
				}
				$html .= '</div>
            </div>
          </div>
        </div>';
			}
			echo json_encode($html);
		} else {echo 0;}
	}
  public function get_products_from_cat_id_user_choice() {
    $utill = new Common_utill();
    $prd = $utill->get_products_by_cat_id_user_choice($this->input->post('cat_id'));
    if (!is_integer($prd)) {
      $html = '';
      foreach ($prd as $pp) {
        $html .= '<div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 s_xs_width box wow bounceInDown center">
          <div class="s_product_item1">
            <img src="' . base_url('assets/images/dhinatechnologies_metrokings_Product_pic1.png') . '" alt="product-image" class="img-responsive s_img_wid_height">
            <div class="s_pro_rate_bg">
              <span class="s_product_rupee"><b>₹ ' . $pp['product_price'] . '</b></span>
              <p class="s_product_offerrupee">' . $pp['point_value'] . '  PV</p>
              <p class="s_product_name1">' . $pp['product_name'] . '</p>
              <p class="s_product_shortdescription">' . $pp['product_short_desc'] . '</p>

              <div class="gj_s_p_hd text-center">';
        $ser = array_search($pp['product_id'], array_column($_SESSION['cart_items'], 'product_id'));
        if (!is_numeric($ser)) {
          $html .= '<button class="gj_cart_but btn-success" id="bt_' . $pp['product_id'] . '" type="button" onclick="common_add_remove(this,' . $pp['product_id'] . ',1)"><i class="fa fa-check"></i> Add Cart</button>';
        } else {
          $html .= '<button class="gj_cart_but btn-danger" id="bt_' . $pp['product_id'] . '" type="button" onclick="common_add_remove(this,' . $pp['product_id'] . ',1)"><i class="fa fa-close"></i> Remove</button>';
        }
        $html .= '</div>
            </div>
          </div>
        </div>';
      }
      echo json_encode($html);
    } else {echo 0;}
  }
	public function get_all_active_users() {
		$id = $_SESSION['user_id'];
		$key = $this->input->get('q');
		$em = $this->doctrine->em;
		$bnk_ar = array();

		// $query = $em->createQuery("SELECT b.user_id,b.user_name FROM Entity\User as b WHERE b.user_name LIKE '%$key%' AND b.is_active = 1 AND b.user_id !=".$id )->setMaxResults(5);
		$query = $em->createQuery("SELECT b.user_id,b.user_name FROM Entity\User as b WHERE b.user_name LIKE '%$key%' AND b.user_id !=" . $id)->setMaxResults(5);

		$single_bank = $query->getResult();
		$json = [];
		for ($i = 0; $i < count($single_bank); $i++) {
			$json[] = ['id' => $single_bank[$i]['user_id'], 'text' => $single_bank[$i]['user_name']];
		}
		echo json_encode($json);
	}
	public function get_unused_pins() {
		$key = $this->input->get('q');
		$em = $this->doctrine->em;
		$bnk_ar = array();

		$query = $em->createQuery("SELECT b.pin_id,b.pin_no FROM Entity\Pins as b WHERE b.pin_no LIKE '%$key%' AND b.pin_status = 1 AND b.pin_transfer_user_id = 0")->setMaxResults(5);

		$single_bank = $query->getResult();
		$json = [];
		for ($i = 0; $i < count($single_bank); $i++) {
			$json[] = ['id' => $single_bank[$i]['pin_id'], 'text' => $single_bank[$i]['pin_no']];
		}
		echo json_encode($json);
	}

	public function get_all_packages() {
		$key = $this->input->get('q');
		$em = $this->doctrine->em;
		$bnk_ar = array();
		$query = $em->createQuery("SELECT b.package_id,b.package_name FROM Entity\Packages as b WHERE b.package_name LIKE '%$key%'")->setMaxResults(5);

		$single_bank = $query->getResult();
		$json = [];
		for ($i = 0; $i < count($single_bank); $i++) {
			$json[] = ['id' => $single_bank[$i]['package_id'], 'text' => $single_bank[$i]['package_name']];
		}
		echo json_encode($json);
	}

	public function get_tot_payout_abstract() {
		$em = $this->doctrine->em;
		$dt1 = '00:00:00 02:00:00';
		$dt2 = '00:00:00 02:00:00';

		if ($this->input->post('pos') == 0) {
			$dt1 = $this->input->post('dt') . ' 02:00:00';
			$dt2 = $this->input->post('dt') . ' 14:00:00';
		} elseif ($this->input->post('pos') == 1) {
			$nxt = date('Y-m-d', strtotime('+1 day', strtotime($this->input->post('dt'))));
			$dt1 = $this->input->post('dt') . ' 14:00:00';
			$dt2 = $nxt . ' 02:00:00';
		}

		if ($this->input->post('stat') == 0) {
			$con = '';
			// $status = 1;
		} elseif ($this->input->post('stat') == 1) {
			$con = 'AND pr.payment_status = 1';
		} else {
			$con = 'AND pr.payment_status = 0';
		}

		$rep_qry = $em->createQuery("SELECT SUM(pr.net_payout) as net_sum FROM Entity\Payout_result as pr WHERE  pr.created_at BETWEEN '" . $dt1 . "' AND '" . $dt2 . "' $con");
		//echo "SELECT pr.net_payout FROM Entity\Payout_result as pr WHERE  pr.created_at BETWEEN ('".$dt1."' AND '".$dt2."') $con";
		$rep_obj = $rep_qry->getResult();
		if (count($rep_obj) > 0) {

			$left_pv_qry = $em->createQuery("SELECT pr.total_left_pv,pr.total_right_pv,pr.net_payout FROM Entity\Payout_result as pr WHERE  pr.created_at BETWEEN '" . $dt1 . "' AND '" . $dt2 . "' $con");
			$lef_obj = $left_pv_qry->getResult();

			// $rit_pv_qry = $em->createQuery("SELECT pr.total_right_pv FROM Entity\Payout_result as pr WHERE  pr.created_at BETWEEN '".$dt1."' AND '".$dt2."' $con");
			// $rit_obj = $rit_pv_qry->getResult();

			//print_r($lef_obj);

			$ar = array();
			$service = array();
			$repurchase = array();
			$tds = array();

			for ($i = 0; $i < count($lef_obj); $i++) {
				if ($lef_obj[$i]['total_right_pv'] > $lef_obj[$i]['total_left_pv']) {
					array_push($ar, $lef_obj[$i]['total_left_pv']);

					$tt = 2 * $lef_obj[$i]['total_left_pv'];
					$ser = $tt * 5 / 100;
					$pur = $tt * 10 / 100;
					$ded = $ser + $pur;
					array_push($service, $ser);
					array_push($repurchase, $pur);

					$net_pay = $lef_obj[$i]['net_payout'];
					$cc = $net_pay - $ded;
					if ($cc != 0) {
						$val = $cc * 100 / $tt;
					} else {
						$val = 0;
					}
					array_push($tds, $val);
				} else {
					array_push($ar, $lef_obj[$i]['total_right_pv']);
					$tt = 2 * $lef_obj[$i]['total_right_pv'];
					$ser = $tt * 5 / 100;
					$pur = $tt * 10 / 100;
					$ded = $ser + $pur;
					array_push($service, $ser);
					array_push($repurchase, $pur);

					$net_pay = $lef_obj[$i]['net_payout'];
					$cc = $net_pay - $ded;
					if ($cc != 0) {
						$val = $cc * 100 / $tt;
					} else {
						$val = 0;
					}
					array_push($tds, $val);
				}
			}

			$total_gross = 2 * (array_sum($ar));
			$total_net = $rep_obj[0]['net_sum'];
			$total_tds = array_sum($tds);
			$total_service = array_sum($service);
			$total_repurchase = array_sum($repurchase);

			echo json_encode(array(
				'total_gross' => $total_gross,
				'total_net' => $total_net,
				'total_tds' => $total_tds,
				'total_service' => $total_service,
				'total_repurchase' => $total_repurchase,
			));

		} else {
			echo 0;
		}
	}
	public function get_package_names_aj() {
		$key = $this->input->get('q');
		$em = $this->doctrine->em;
		$bnk_ar = array();

		$query = $em->createQuery("SELECT b.package_id,b.package_name,b.package_price FROM Entity\Packages as b WHERE b.package_name LIKE '%$key%' AND b.package_is_visible = 1")->setMaxResults(5);

		$single_bank = $query->getResult();
		$json = [];
		for ($i = 0; $i < count($single_bank); $i++) {
			$json[] = ['id' => $single_bank[$i]['package_id'], 'text' => $single_bank[$i]['package_name'], 'price' => $single_bank[$i]['package_price']];
		}

		echo json_encode($json);
	}
  public function get_package_prd_names() {
    $key = $this->input->get('q');
    $xx = $this->input->get('id');

    $em = $this->doctrine->em;
    $bnk_ar = array();

    $query = $em->createQuery("SELECT b.package_product_id,b.package_product_name FROM Entity\Package_products as b  WHERE b.package_product_name LIKE '%$key%'")->setMaxResults(3);

    $single_bank = $query->getResult();
    $json = [];
    for ($i = 0; $i < count($single_bank); $i++) {
      $json[] = ['id' => $single_bank[$i]['package_product_id'], 'text' => $single_bank[$i]['package_product_name']];
    }

    echo json_encode($json);
  }
	public function get_package_prd_names_aj() {
		$key = $this->input->get('q');
		$xx = $this->input->get('id');

		$em = $this->doctrine->em;
		$bnk_ar = array();

		$query = $em->createQuery("SELECT b.package_product_id,b.package_product_name,b.package_product_display_price FROM Entity\Package_products as b  WHERE b.package_product_name LIKE '%$key%' AND b.package_product_is_visible = 1 AND b.package_id =" . $xx)->setMaxResults(3);

		$single_bank = $query->getResult();
		$json = [];
		for ($i = 0; $i < count($single_bank); $i++) {
			$json[] = ['id' => $single_bank[$i]['package_product_id'], 'text' => $single_bank[$i]['package_product_name'], 'price' => $single_bank[$i]['package_product_display_price']];
		}

		echo json_encode($json);
	}
	public function get_all_user_id() {
		$em = $this->doctrine->em;
		$key = $this->input->get('q');
		$query = $em->createQuery("SELECT b.user_id FROM Entity\User as b WHERE b.user_id LIKE '%$key%' AND b.user_id != 2")->setMaxResults(3);

		$single_bank = $query->getResult();
		$json = [];
		for ($i = 0; $i < count($single_bank); $i++) {
			$json[] = ['id' => $single_bank[$i]['user_id'], 'text' => $single_bank[$i]['user_id']];
		}
		echo json_encode($json);
	}
	public function get_all_user_name() {
		$em = $this->doctrine->em;
		$key = $this->input->get('q');
		$query = $em->createQuery("SELECT b.user_id,b.user_name FROM Entity\User as b WHERE b.user_name LIKE '%$key%' AND b.user_id != 2")->setMaxResults(3);

		$single_bank = $query->getResult();
		$json = [];
		for ($i = 0; $i < count($single_bank); $i++) {
			$json[] = ['id' => $single_bank[$i]['user_id'], 'text' => $single_bank[$i]['user_name']];
		}
		echo json_encode($json);
	}
  public function get_all_first_name() {
    $em = $this->doctrine->em;
    $key = $this->input->get('q');
    $query = $em->createQuery("SELECT b.user_id,b.first_name,b.last_name FROM Entity\User as b WHERE b.first_name LIKE '%$key%' AND b.user_id != 2")->setMaxResults(3);

    $single_bank = $query->getResult();
    $json = [];
    for ($i = 0; $i < count($single_bank); $i++) {
      $json[] = ['id' => $single_bank[$i]['user_id'], 'text' => $single_bank[$i]['first_name'].' '.$single_bank[$i]['last_name']];
    }
    echo json_encode($json);
  }
	public function get_sub_user_id() {
		$utill = new Common_utill();

		if ($_SESSION['role_id'] == 7) {
			$_SESSION['user_id'] = 1;
		} else {
			$_SESSION['user_id'] = $_SESSION['user_id'];
		}

		if ($_SESSION['user_id'] == self::$id) {
			if (self::$ar_obj != null) {
				$obj = self::$ar_obj;
			} else {
				$h_obj = $utill->get_single_row_using_array_value('User_package_purchase', 'user_id', $_SESSION['user_id']);
				$obj = $utill->get_tree_obj($h_obj->getUserPurchaseId());
				self::$id = $_SESSION['user_id'];
				self::$ar_obj = $obj;
			}
		} else {
			$h_obj = $utill->get_single_row_using_array_value('User_package_purchase', 'user_id', $_SESSION['user_id']);
			$obj = $utill->get_tree_obj($h_obj->getUserPurchaseId());
			self::$id = $_SESSION['user_id'];
			self::$ar_obj = $obj;
		}

		if ($this->input->get('rs') == 1) {

			$id_ar = array();
			$name_ar = array();
			foreach ($obj as $ss) {
				array_push($id_ar, $ss['id']);
				array_push($name_ar, $ss['name']);
			}
			$input = preg_quote($this->input->get('q'), '~');
			$data = $name_ar;
			$result = preg_grep('~' . $input . '~', $data);
			$json = [];
			$i = 0;

			foreach ($result as $res) {
				$i++;
				if ($i < 4) {
					$ind = array_search($res, $name_ar);
					$json[] = ['id' => $id_ar[$ind], 'text' => $res];
				}
			}
			echo json_encode($json);
		} else {
			$input = preg_quote($this->input->get('q'), '~');
			$id_ar = array();
			foreach ($obj as $ss) {
				array_push($id_ar, $ss['id']);
			}
			$data = $id_ar;
			$result = preg_grep('~' . $input . '~', $data);
			$json = [];
			$i = 0;
			foreach ($result as $res) {
				$i++;
				if ($i < 4) {
					$json[] = ['id' => $res, 'text' => $res];
				}
			}
			echo json_encode($json);
		}

	}
	public function invoice() {
		$utill = new Common_utill();
		$up_obj = $utill->get_single_row_using_array_value('User_package_purchase', 'user_id', $_SESSION['user_id']);
		$distributor_id = "";
		$distributor_name = "";
		$distributor_created = "";
		$placement_name = "";
		$dis_pos = "";
		$package_name = "";
		$package_price = "";
		if ($up_obj != null) {
			$distributor_id = $up_obj->getUserId()->getUserId();
			$distributor_name = $up_obj->getUserId()->getUserName();
			$distributor_created = $up_obj->getUserId()->getCreatedAt()->format('Y-m-d H:i:s');

			$placement_id = $up_obj->getUserPurchaseActualGeneUserPurchaseId();
			$act_up_obj = $utill->get_single_row_using_primary_key('User_package_purchase', $placement_id);
			if ($act_up_obj != null) {
				$placement_name = $act_up_obj->getUserId()->getUserName();
				if ($act_up_obj->getUserPurchaseGeneLeftPurchaseId() == $up_obj->getUserPurchaseId()) {
					$dis_pos = "LEFT";
				} else { $dis_pos = "RIGHT";}
			} else {
				$placement_name = "";
				$dis_pos = "";
			}

			$package_name = $up_obj->getUserPurchasePinId()->getPinRequestPackageId()->getPackageName();
			$package_price = $up_obj->getUserPurchasePinId()->getPinRequestPackageProductId()->getPackageProductPrice();

		}

		$data['obj'] = array(
			'distributor_id' => $distributor_id,
			'distributor_name' => $distributor_name,
			'distributor_created' => $distributor_created,
			'placement_id' => $placement_id,
			'placement_name' => $placement_name,
			'dis_pos' => $dis_pos,
			'package_name' => $package_name,
			'package_price' => $package_price,
		);

		$this->load->view('header');
		$this->load->view('common/Invoice', $data);
		$this->load->view('footer');
	}

	public function get_all_user_name_for_message() {
		$em = $this->doctrine->em;
		$key = $this->input->get('q');
		$id = $_SESSION['user_id'];
		$query = $em->createQuery("SELECT b.user_id,b.user_name FROM Entity\User as b WHERE b.user_id != $id AND b.user_name LIKE '%$key%'")->setMaxResults(5);

		$single_bank = $query->getResult();
		$json = [];
		for ($i = 0; $i < count($single_bank); $i++) {
			$json[] = ['id' => $single_bank[$i]['user_id'], 'text' => $single_bank[$i]['user_name']];
		}
		echo json_encode($json);
	}
	public function get_tot_payout_abstract_full() {
    $utill = new Common_utill();
		$em = $this->doctrine->em;
		$dt1 = '';
    $dt2 = '';
    $u_con = '';
    $con = '';
    if($this->input->post('stat') == 0){
      $con = '';
      // $status = 1;
    }elseif($this->input->post('stat') == 1){
      $con =  ' AND `a`.`payment_status` = 1';
    }else{
      $con =  ' AND `a`.`payment_status` = 0';
    }

    if($this->input->post('dt1') != null){
      if($this->input->post('pos1') == 0){
        $dt1 = $this->input->post('dt1').' 02:00:00';
        if($this->input->post('dt2') == null){
          $dt2 = $this->input->post('dt1').' 13:59:59';
        }
      }elseif($this->input->post('pos1') == 1){
        $dt1 = $this->input->post('dt1').' 14:00:00';
        if($this->input->post('dt2') == null){
           $d = $this->input->post('dt1').' 01:59:59';
           $dt2 = date("Y-m-d H:i:s", strtotime($d."+1 days"));
          
          //echo($dt2);
        }
      }else{
          $dt1 = $this->input->post('dt1').' 02:00:00';
          $d = $this->input->post('dt1').' 01:59:59';
          $dt2 = date("Y-m-d H:i:s", strtotime($d."+1 days"));
      }
    }
   
    if($this->input->post('dt1') != null){
      if($this->input->post('dt2') != null){
        if($this->input->post('pos2') == 0){
          $dt2 = $this->input->post('dt2').' 02:00:00';
        }else{
          $dt2 = $this->input->post('dt2').' 13:59:59';
        }
      }
    }

    if($this->input->post('user_id') != null || $this->input->post('user_name') != null){
      if($this->input->post('user_id') != null && $this->input->post('user_name') == null){
        $obj = $utill->get_single_row_using_array_value('User_package_purchase','user_id',$this->input->post('user_id'));
        $up_id = $obj->getUserPurchaseId();
        if($dt1 != null){
          $u_con = " AND pr.payout_result_user_id = ".$up_id;
        }else{
          $u_con = "  pr.payout_result_user_id = ".$up_id;
        }
      }elseif($this->input->post('user_id') == null && $this->input->post('user_name') != null){
        $obj = $utill->get_single_row_using_array_value('User_package_purchase','user_id',$this->input->post('user_name'));
        $up_id = $obj->getUserPurchaseId();
        if($dt1 != null){
          $u_con = " AND pr.payout_result_user_id = ".$up_id;
        }else{
          $u_con = "  pr.payout_result_user_id = ".$up_id;
        }
      }else{
        $obj = $utill->get_single_row_using_array_value('User_package_purchase','user_id',$this->input->post('user_id'));
        $up_id = $obj->getUserPurchaseId();
        if($dt1 != null){
          $u_con = " AND pr.payout_result_user_id = ".$up_id;
        }else{
          $u_con = "  pr.payout_result_user_id = ".$up_id;
        }
      }
    }

    if($dt1 != null){
		  $rep_qry = $em->createQuery("SELECT SUM(pr.net_payout) as net_sum FROM Entity\Payout_result as pr WHERE  pr.created_at BETWEEN '" . $dt1 . "' AND '" . $dt2 . "' $con $u_con");
    }else{
      $rep_qry = $em->createQuery("SELECT SUM(pr.net_payout) as net_sum FROM Entity\Payout_result as pr WHERE $u_con");
    }
		//echo "SELECT SUM(pr.net_payout) as net_sum FROM Entity\Payout_result as pr WHERE $u_con";
		$rep_obj = $rep_qry->getResult();
		if (count($rep_obj) > 0) {
      if($dt1 != null){
        $left_pv_qry = $em->createQuery("SELECT pr.total_left_pv,pr.total_right_pv,pr.net_payout,pr.payout_result_user_id,pr.created_at FROM Entity\Payout_result as pr WHERE  pr.created_at BETWEEN '" . $dt1 . "' AND '" . $dt2 . "' $con $u_con");
      }else{
			 $left_pv_qry = $em->createQuery("SELECT pr.total_left_pv,pr.total_right_pv,pr.net_payout,pr.payout_result_user_id,pr.created_at FROM Entity\Payout_result as pr WHERE  $u_con");
      }

			$lef_obj = $left_pv_qry->getResult();

			// $rit_pv_qry = $em->createQuery("SELECT pr.total_right_pv FROM Entity\Payout_result as pr WHERE  pr.created_at BETWEEN '".$dt1."' AND '".$dt2."' $con");
			// $rit_obj = $rit_pv_qry->getResult();

			//print_r($lef_obj);

			$ar = array();
			$service = array();
			$repurchase = array();
			$tds = array();

      $tt_svk  = array();

			for ($i = 0; $i < count($lef_obj); $i++) {
				if ($lef_obj[$i]['total_right_pv'] > $lef_obj[$i]['total_left_pv']) {
					array_push($ar, $lef_obj[$i]['total_left_pv']);

					$tt = 2 * $lef_obj[$i]['total_left_pv'];
					$ser = $tt * 5 / 100;
					$pur = $tt * 10 / 100;
					$ded = $ser + $pur;
					array_push($service, $ser);
					array_push($repurchase, $pur);

          if($tt > 0){
            // New Panverified Code Start
                $us_ob = $utill->get_single_row_using_primary_key('User_package_purchase',$lef_obj[$i]['payout_result_user_id']);

                $user_id = $us_ob->getUserId()->getUserId();
                $pan_verified = $us_ob->getUserId()->getPanVerified();


                $created_dt_obj = $us_ob->getUserId()->getCreatedAt();
                $created_at = $created_dt_obj->format('Y-m-d');

                $cc = $lef_obj[$i]['created_at'];
                $ccx = $cc->format('Y-m-d');



                $endx = date("Y-m-d", strtotime($created_at."+120 days"));

                $dd1=date_create($ccx);
                $dd2=date_create($endx);
                $diffx=date_diff($dd1,$dd2);


                if($diffx->days <= 120){
                  $pan_status = 1;
                }else{
                  $pan_status = 0;
                }

                $first = $tt-($tt*35/100);
                $second = ($tt-$tt*35/100)/2;
                $third = $tt-($tt*20/100);
                $fourth = ($tt-$tt*20/100)/2;

                if($first == $lef_obj[$i]['net_payout'] ||$second == $lef_obj[$i]['net_payout'] ){
                    $td = $tt*20/100;
                    array_push($tt_svk,$td);
                }elseif($third == $lef_obj[$i]['net_payout'] ||$fourth == $lef_obj[$i]['net_payout'] ){
                  $td = $tt*5/100;
                  array_push($tt_svk,$td);
                }else{
                  array_push($tt_svk,0);
                }


                // if($pan_verified == 1){  
                //   $td = $tt*5/100;
                //   array_push($tt_svk,$td);
                // }else{
                //     $td = $tt*20/100;
                //     array_push($tt_svk,$td);
                // }
            // New Panverified Code Start
          }

					$net_pay = $lef_obj[$i]['net_payout'];
					$cc = $net_pay - $ded;
					if ($cc != 0) {
						$val = $cc * 100 / $tt;
					} else {
						$val = 0;
					}
					array_push($tds, $val);
				} else {
					array_push($ar, $lef_obj[$i]['total_right_pv']);
					$tt = 2 * $lef_obj[$i]['total_right_pv'];
					$ser = $tt * 5 / 100;
					$pur = $tt * 10 / 100;
					$ded = $ser + $pur;
					array_push($service, $ser);
					array_push($repurchase, $pur);

          if($tt > 0){
            // New Panverified Code Start
                $us_ob = $utill->get_single_row_using_primary_key('User_package_purchase',$lef_obj[$i]['payout_result_user_id']);

                $user_id = $us_ob->getUserId()->getUserId();
                $pan_verified = $us_ob->getUserId()->getPanVerified();


                $created_dt_obj = $us_ob->getUserId()->getCreatedAt();
                $created_at = $created_dt_obj->format('Y-m-d');
                $nowx = date("Y-m-d");

                $cc = $lef_obj[$i]['created_at'];
                $ccx = $cc->format('Y-m-d');



                $endx = date("Y-m-d", strtotime($created_at."+120 days"));

                $dd1=date_create($ccx);
                $dd2=date_create($endx);
                $diffx=date_diff($dd1,$dd2);

                if($diffx->days <= 120){
                  $pan_status = 1;
                }else{
                  $pan_status = 0;
                }
                $first = $tt-($tt*35/100);
                $second = ($tt-$tt*35/100)/2;
                $third = $tt-($tt*20/100);
                $fourth = ($tt-$tt*20/100)/2;

                if($first == $lef_obj[$i]['net_payout'] ||$second == $lef_obj[$i]['net_payout'] ){
                    $td = $tt*20/100;
                    array_push($tt_svk,$td);
                }elseif($third == $lef_obj[$i]['net_payout'] ||$fourth == $lef_obj[$i]['net_payout'] ){
                  $td = $tt*5/100;
                  array_push($tt_svk,$td);
                }else{
                  array_push($tt_svk,0);
                }
                // if($pan_verified == 1){
                //   $td = $tt*5/100;
                //   array_push($tt_svk,$td);
                // }else{
                //     $td = $tt*20/100;
                //     array_push($tt_svk,$td);
                // }
            // New Panverified Code Start
          }
					$net_pay = $lef_obj[$i]['net_payout'];
					$cc = $net_pay - $ded;
					if ($cc != 0) {
						$val = $cc * 100 / $tt;
					} else {
						$val = 0;
					}
					array_push($tds, $val);
				}
			}

			$total_gross = 2 * (array_sum($ar));
			$total_net = $rep_obj[0]['net_sum'];
      $total_service = array_sum($service);
      $total_repurchase = array_sum($repurchase);
			$total_tds = array_sum($tt_svk);

			echo json_encode(array(
				'total_gross' => $total_gross,
				'total_net' => $total_net,
				'total_tds' => $total_tds,
				'total_service' => $total_service,
				'total_repurchase' => $total_repurchase,
			));

		} else {
			echo 0;
		}
	}
  public function get_tot_payout_abstract_full_customer(){ 
    $em = $this->doctrine->em;
    //date($_GET['dt'], strtotime(' +1 day'))
    $dt1 = '00:00:00 02:00:00';
    $dt2 = '00:00:00 02:00:00';

    $cc = "";
    if($_SESSION['role_id'] != 7){
      $cc = " AND us.user_id = ".$_SESSION['user_id'];
    }

    if($this->input->post('pos1') == 0){
      $dt1 = $this->input->post('dt1').' 02:00:00';
    }else{
      $dt1 = $this->input->post('dt1').' 13:59:59';
    }

    if($this->input->post('pos2') == 0){
      $dt2 = $this->input->post('dt2').' 02:00:00';
    }else{
      $dt2 = $this->input->post('dt2').' 13:59:59';
    }

    $rep_qry = $em->createQuery("SELECT a.total_pv_left,a.total_pv_right  FROM Entity\Payout as a INNER JOIN Entity\Packages AS p WITH (a.package_id = p.package_id) INNER JOIN Entity\User_package_purchase as up WITH (a.user_purchase_id = up.user_purchase_id) INNER JOIN Entity\User as us WITH (us.user_id = up.user_id) WHERE (a.total_pv_left != 0 OR a.total_pv_right != 0) AND a.created_at BETWEEN '".$dt1."' AND '".$dt2."' $cc");
    //echo "SELECT *  FROM Entity\Payout as a INNER JOIN Entity\Packages AS p WITH (a.package_id = p.package_id) INNER JOIN Entity\User_package_purchase as up WITH (a.user_purchase_id = up.user_purchase_id) INNER JOIN Entity\User as us WITH (us.user_id = up.user_id) WHERE (a.total_pv_left != 0 OR a.total_pv_right != 0) AND a.created_at BETWEEN '2017-11-23 01:00:00' AND '2017-11-23 14:00:00' $cc";
    $left_ar = array();
    $right_ar = array();
    $rep_obj = $rep_qry->getResult();
    if (count($rep_obj) > 0) {
      foreach ($rep_obj as $obj) {
        array_push($left_ar, $obj['total_pv_left']);
        array_push($right_ar, $obj['total_pv_right']);
      }
      $tot_l = array_sum($left_ar);
      $tot_r = array_sum($right_ar);
      echo json_encode(array('left'=>$tot_l,'right'=>$tot_r));
    }else{
      echo 0;
    }
  }
  public function get_tot_payout_abstract_full_customer_admin(){ 
    $em = $this->doctrine->em;
    //date($_GET['dt'], strtotime(' +1 day'))
    $dt1 = '';
    $dt2 = '';

    $cc = "";
    $cc = " AND us.user_id = ".$this->input->post('user_id');

    if($this->input->post('dt1') != null){
      if($this->input->post('pos1') == 0){
        $dt1 = $this->input->post('dt1').' 02:00:00';
      }else{
        $dt1 = $this->input->post('dt1').' 13:59:59';
      }
    }

    if($this->input->post('dt2') != null){
      if($this->input->post('pos2') == 0){
        $dt2 = $this->input->post('dt2').' 02:00:00';
      }else{
        $dt2 = $this->input->post('dt2').' 13:59:59';
      }
    }

    if($dt1 != null && $dt2 != null){
      $rep_qry = $em->createQuery("SELECT a.total_pv_left,a.total_pv_right  FROM Entity\Payout as a INNER JOIN Entity\Packages AS p WITH (a.package_id = p.package_id) INNER JOIN Entity\User_package_purchase as up WITH (a.user_purchase_id = up.user_purchase_id) INNER JOIN Entity\User as us WITH (us.user_id = up.user_id) WHERE (a.total_pv_left != 0 OR a.total_pv_right != 0) AND a.created_at BETWEEN '".$dt1."' AND '".$dt2."' $cc");
    }else{
       $rep_qry = $em->createQuery("SELECT a.total_pv_left,a.total_pv_right  FROM Entity\Payout as a INNER JOIN Entity\Packages AS p WITH (a.package_id = p.package_id) INNER JOIN Entity\User_package_purchase as up WITH (a.user_purchase_id = up.user_purchase_id) INNER JOIN Entity\User as us WITH (us.user_id = up.user_id) WHERE (a.total_pv_left != 0 OR a.total_pv_right != 0)  $cc");
    }
    
    $left_ar = array();
    $right_ar = array();
    $rep_obj = $rep_qry->getResult();
    if (count($rep_obj) > 0) {
      foreach ($rep_obj as $obj) {
        array_push($left_ar, $obj['total_pv_left']);
        array_push($right_ar, $obj['total_pv_right']);
      }
      $tot_l = array_sum($left_ar);
      $tot_r = array_sum($right_ar);
      echo json_encode(array('left'=>$tot_l,'right'=>$tot_r));
    }else{
      echo 0;
    }
  }
  public function get_tot_payout_result_full_customer(){
    $em = $this->doctrine->em;
    $dt1 = '00:00:00 02:00:00';
    $dt2 = '00:00:00 02:00:00';
    if($this->input->post('pos1') == 0){
      $dt1 = $this->input->post('dt1').' 02:00:00';
    }else{
      $dt1 = $this->input->post('dt1').' 13:59:59';
    }
    if($this->input->post('pos2') == 0){
      $dt2 = $this->input->post('dt2').' 02:00:00';
    }else{
      $dt2 = $this->input->post('dt2').' 13:59:59';
    }
    $cc = "";
    if($_SESSION['role_id'] != 7 ){
      $cc = " AND us.user_id = ".$_SESSION['user_id'];
    }
    $rep_qry = $em->createQuery("SELECT a.net_payout FROM Entity\Payout_result AS a  INNER JOIN Entity\User_package_purchase as up WITH (a.payout_result_user_id = up.user_purchase_id) INNER JOIN Entity\User as us WITH (us.user_id = up.user_id) AND a.created_at BETWEEN '".$dt1."' AND '".$dt2."' $cc");
    $rep_obj = $rep_qry->getResult();
    if (count($rep_obj) > 0) {
      $ar = array();
      
      for ($i = 0; $i < count($rep_obj); $i++) {
        array_push($ar, $rep_obj[$i]['net_payout']);
      }
      $total_amt = array_sum($ar);
      echo $total_amt;
    }else{echo 0;}

  }

	public function get_payout_res() {
		$em = $this->doctrine->em;
		if ($this->input->post('id') != null) {

			$rep_qry = $em->createQuery("SELECT pck.package_name,pr.personal_left,pr.personal_right,pr.previous_left,pr.previous_right,pr.current_left,pr.current_right,pr.total_left,pr.total_right,pr.carry_forward,pr.carry_forward_side,pr.pair_details_total,pr.pair_details_flushed,ppr.total_left_pv,ppr.total_right_pv  FROM Entity\Payout as pr INNER JOIN Entity\Packages as pck WITH (pck.package_id = pr.package_id) INNER JOIN Entity\Payout_result as ppr WITH (pr.user_purchase_id = ppr.payout_result_user_id) WHERE (pr.total_left != 0 OR pr.total_right != 0) AND  pr.created_at='" . $this->input->post('date') . "' AND ppr.created_at = '" . $this->input->post('date') . "' AND pr.user_purchase_id=" . $this->input->post('id'));
			$rep_obj = $rep_qry->getResult();

			if (count($rep_obj) > 0) {
				$html = '<table  id="gj_tree_struct_vwee" class="table table-striped table-bordered" cellspacing="10" width="100%">
        <thead>
            <tr>
              <th class="text-center" rowspan="2" style="vertical-align: middle;">PACKAGE NAME</th>
              <th class="text-center" colspan="2">Personal Sales</th>
              <th class="text-center" colspan="2">Previous</th>
              <th class="text-center" colspan="2">Current Cutoff</th>
              <th class="text-center" colspan="2">Total Cutoff</th>
              <th class="text-center" colspan="2">Carry</th>
              <th class="text-center" colspan="2">Pair Details</th>
              <th class="text-center" colspan="2">Gross</th>
            </tr>
            <tr>
              <th class="text-center">Count</th>
              <th class="text-center">Side</th>

              <th class="text-center">Left</th>
              <th class="text-center">Right</th>

              <th class="text-center">Left</th>
              <th class="text-center">Right</th>

              <th class="text-center">Left</th>
              <th class="text-center">Right</th>

              <th class="text-center">Left</th>
              <th class="text-center">Right</th>

              <th class="text-center">Total</th>
              <th class="text-center">Flushed</th>
              <th class="text-center"></th>

            </tr>
        </thead>
        <tbody>';
				foreach ($rep_obj as $rep) {
					$html .= '<tr>';
					$html .= '<td>' . $rep['package_name'] . '</td>';
					$html .= '<td>' . $rep['personal_left'] . '</td>';
					$html .= '<td>' . $rep['personal_right'] . '</td>';
					$html .= '<td>' . $rep['previous_left'] . '</td>';
					$html .= '<td>' . $rep['previous_right'] . '</td>';
					$html .= '<td>' . $rep['current_left'] . '</td>';
					$html .= '<td>' . $rep['current_right'] . '</td>';
					$html .= '<td>' . $rep['total_left'] . '</td>';
					$html .= '<td>' . $rep['total_right'] . '</td>';
					if ($rep['carry_forward_side'] == 1) {
						$html .= '<td>0</td>';
						$html .= '<td>' . $rep['carry_forward'] . '</td>';
					} else {
						$html .= '<td>' . $rep['carry_forward'] . '</td>';
						$html .= '<td>0</td>';
					}
					$html .= '<td>' . $rep['pair_details_total'] . '</td>';
					$html .= '<td>' . $rep['pair_details_flushed'] . '</td>';
          if($rep['total_left_pv']<=$rep['total_right_pv']){
            $html .= '<td>' . 2*($rep['total_left_pv']) . '</td>';
          }else{
            $html .= '<td>' . 2*($rep['total_right_pv']) . '</td>';
          }
					//$html .= '<td>' . ($rep['total_left_pv'] + $rep['total_right_pv']) . '</td>';
					$html .= '</tr>';
				}
				$html .= '</tbody></table>';
				echo json_encode($html);
			} else {
				echo 0;
			}
		}
	}

	public function get_all_pin_code() {
		$em = $this->doctrine->em;
		$key = $this->input->get('q');
		$query = $em->createQuery("SELECT b.pincode_id,b.pincode_no FROM Entity\Pincode as b WHERE b.pincode_no LIKE '%$key%'")->setMaxResults(3);

		$single_bank = $query->getResult();
		$json = [];
		for ($i = 0; $i < count($single_bank); $i++) {
			$json[] = ['id' => $single_bank[$i]['pincode_id'], 'text' => $single_bank[$i]['pincode_no']];
		}
		echo json_encode($json);
	}

	public function new_user() {
		$this->load->view('header');
		$this->load->view('registration/user/New_user.php');
		$this->load->view('footer');
	}
	public function add_user() {
    $main_folder_name='registration'; $sub_folder_name='user'; $file_name='New_user';
		$utill_obj = new Common_utill();
		$em = $this->doctrine->em;
		$this->form_validation->set_rules('user_name', 'User Name', 'trim|required|min_length[1]|max_length[30]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]', array('required' => 'User Name Requierd', 'min_length' => 'User Name needs Minimum 1 charecters', 'max_length' => 'Maximum 20 Charecters are allowed'));
		$this->form_validation->set_rules('first_name', 'First Name', 'trim|required|min_length[3]|max_length[20]|regex_match[/^[a-zA-Z._ ,%-]+$/]', array('required' => 'First Name Requierd', 'min_length' => 'First Name needs Minimum 3 charecters', 'max_length' => 'Maximum 20 Charecters are allowed', 'find_alphabets' => 'Albhabets Required'));
		$this->form_validation->set_rules('pwd', 'Password', 'trim|required|min_length[5]|max_length[20]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]', array('required' => 'Password Required', 'min_length' => 'Minimum 5 Charecters Need', 'max_length' => 'Maximum 20 Charecters are allowed', 'find_alphabets' => 'Albhabets Required'));
		$this->form_validation->set_rules('r_password', 'Password', 'trim|required|matches[pwd]', array('matches' => 'Password Does not Match'));
		$this->form_validation->set_rules('pin_pwd', 'Pin Password', 'trim|required|min_length[5]|max_length[20]|regex_match[/^[a-zA-Z0-9._ ,%-]+$/]', array('required' => 'Password Required', 'min_length' => 'Minimum 5 Charecters Need', 'max_length' => 'Maximum 20 Charecters are allowed', 'find_alphabets' => 'Albhabets Required'));
		if ($this->form_validation->run() != false) {
			$em->getConnection()->beginTransaction();
			$password_obj = null;
			$nominee_obj = null;
			$contact_obj = null;
			$address_obj = null;
			$user_name = $this->input->post('user_name');
			$user_email = $this->input->post('user_email');
			$first_name = $this->input->post('first_name');
			$last_name = $this->input->post('last_name');
			$gender = $this->input->post('gender');
			if ($this->input->post('dob') != null) {
				$dob = new DateTime($this->input->post('dob'));
			} else {
				$dob = "";
			}
			$is_active = 1;

			$exist = $utill_obj->get_single_row_using_array_value('User', 'user_name', $user_name);
			if ($exist == null) {

				$pwd = $this->input->post('pwd');
				$pin_password = $this->input->post('pin_pwd');

				$password = new Entity\Password;
				$password->setPassword($pwd);
				$password->setSalt($pwd);
				$password->setPasswordRequestOrNot("");
				$em->persist($password);
				$em->flush();
				$password_obj = $em->getReference('Entity\Password', $password->getPasswordId());

				$user_obj = new Entity\User;
				$user_obj->setUserName($user_name);
				$user_obj->setUserEmail($user_email);
				$user_obj->setFirstName($first_name);
				$user_obj->setLastName($last_name);
				$user_obj->setGender($gender);
				$role_obj = $em->getReference('Entity\User_role', RT::$deafault_user_role_id);
				if ($role_obj != null) {
					$user_obj->setUserRoleId($role_obj);
				}
				$user_obj->setUserEmailVerified(1);
				$user_obj->setIsActive(1);
				$user_obj->setPinStorePassword($pin_password);

				if ($this->input->post('primary_mobile') != null || $this->input->post('secondary_mobile') != null || $this->input->post('landline') != null) {
					$primary_mobile = $this->input->post('primary_mobile');
					$secondary_mobile = $this->input->post('secondary_mobile');
					$contact_email = $this->input->post('contact_email');
					$landline = $this->input->post('landline');
					$contact = new Entity\Contact();
					$contact->setContactName($first_name);
					$contact->setContactMobilePrimary($primary_mobile);
					$contact->setContactMobileSecondary($secondary_mobile);
					$contact->setContactEmailId($contact_email);
					$contact->setContactLandline($landline);
					$contact->setContactReferId(0);
					$contact->setContactReferType(0);
					$em->persist($contact);
					$em->flush();
					$contact_obj = $em->getReference('Entity\Contact', $contact->getContactId());
				}

				if ($this->input->post('full_address') != null) {
					$full_address = $this->input->post('full_address');
					$address = new Entity\Address();
					if ($this->input->post('pincode_no') != null) {
						$pincode_no = $this->input->post('pincode_no');
						$district = $this->input->post('district');
						$state = $this->input->post('state');
						$pincode_exist = $utill_obj->get_single_row_using_array_value('Pincode', 'pincode_no', $pincode_no);
						if ($pincode_exist != null) {
							$address->set_Address_pincode_id($pincode_exist);
						} else {
							$pincode = new Entity\Pincode();
							$pincode->setPincodeNo($pincode_no);
							$pincode->setPincodePlace("");
							$pincode->setPincodeDistrict($district);
							$pincode->setPincodeState($state);
							if ($this->input->post('country_id') != null) {
								$country_id = $this->input->post('country_id');
								$country_obj = $utill_obj->get_reference_obj('Country', $country_id);
								if ($country_obj != null) {
									$pincode->setPincodeCountryId($country_obj);
								}
							}
							$em->persist($pincode);
							$em->flush();
							$address->set_Address_pincode_id($em->getReference('Entity\Pincode', $pincode->getPincodeId()));
						}
					}
					$address->set_Address_full_address($full_address);
					$em->persist($address);
					$em->flush();
					$address_obj = $em->getReference('Entity\Address', $address->get_Address_id());
				}
				if ($this->input->post('nominee_name') != null) {
					$nominee_name = $this->input->post('nominee_name');
					$n_mobile = $this->input->post('n_mobile');
					$relation = $this->input->post('relation');
					$n_dob = $this->input->post('n_dob');
					$n_gender = $this->input->post('n_gender');
					$nominee = new Entity\Nominee;
					$nominee->setNomineeName($nominee_name);
					if ($this->input->post('n_dob') != null) {
						$n_dob = new DateTime($this->input->post('n_dob'));
						$nominee->setNomineeDob($n_dob);
					}
					$nominee->setNomineeRelationship($relation);
					$nominee->setNomineePhoneNo($n_mobile);
					$nominee->setNomineeGender($n_gender);
					$em->persist($nominee);
					$em->flush();
					$nominee_obj = $em->getReference('Entity\Nominee', $nominee->getNomineeId());
				}

				$user_obj->setContactId($contact_obj);
				$user_obj->setNomineeId($nominee_obj);
				$user_obj->setPasswordId($password_obj);
				$user_obj->setAddressId($address_obj);
				$em->persist($user_obj);
				$user_account_available_amount = 0;
				$user_account_re_purchase_amount = 0;
				$user_account_transfer_per_day_limit_amount = RT::$user_account_transfer_per_day_limit_amount;
				$user_account_transfer_per_day_limit_transfer = RT::$user_account_transfer_per_day_limit_transfer;
				$user_account_transfer_per_month_limit_amount = RT::$user_account_transfer_per_month_limit_amount;
				$user_account_transfer_per_month_limit_transfer = RT::$user_account_transfer_per_month_limit_transfer;
				$user_account_account_per_day_transfer_enable = 0;
				$user_account_per_month_transfer_enable = 0;
				$user_account_is_active = 0;
				$user_account_locked_reason = 0;
				$user_account_hold_amount = 0;
				$user_account_hold_enabled = 0;
				$user_account_transfer_enabled = 0;
				try {
					if ($this->input->post('account_no') != null) {
						$bank_obj = null;
						$account_no = $this->input->post('account_no');
						$account_type = $this->input->post('account_type');
						if ($this->input->post('bank_ifsc') != null) {
							$ifsc = $this->input->post('bank_ifsc');
							$bank_obj = $utill_obj->get_single_row_using_array_value('Bank_details', 'bank_ifsc', $ifsc);
							if ($bank_obj == null) {
								if ($this->input->post('bank_name') != null && $this->input->post('bank_branch') != null) {
									$bank_name = $this->input->post('bank_name');
									$bank_branch = $this->input->post('bank_branch');
									$address_obj = null;
									$bk_obj = $utill_obj->add_bank($bank_name, $ifsc, $bank_branch, $address_obj);
									if ($bk_obj != null) {
										$bank_obj = $bk_obj;
									} else { $bank_obj = null;}
								} else {
									$bank_obj = null;
								}
							}
						}
						$referer_id = $user_obj->getUserId();
						$referer_type = 1;
						$acnt = $utill_obj->add_account($first_name, $account_no, $account_type, $bank_obj, $referer_id, $referer_type);
						if ($acnt == 1) {
							$user_account_obj = $utill_obj->add_user_account($user_obj, $user_account_available_amount, $user_account_re_purchase_amount, $user_account_transfer_per_day_limit_amount, $user_account_transfer_per_day_limit_transfer, $user_account_transfer_per_month_limit_amount, $user_account_transfer_per_month_limit_transfer, $user_account_account_per_day_transfer_enable, $user_account_per_month_transfer_enable, $user_account_hold_amount, $user_account_hold_enabled, $user_account_transfer_enabled, $user_account_is_active, $user_account_locked_reason);
							if ($user_account_obj != null) {
								$em->flush();
								$em->getConnection()->commit();
								$this->session->set_flashdata('success', '<p>User Created Successfully</p>');
                $this->view_call($main_folder_name, $sub_folder_name, $file_name);
							} else {
								$em->getConnection()->rollBack();
								$this->session->set_flashdata('error', '<p>Account Failed</p>');
                $this->view_call($main_folder_name, $sub_folder_name, $file_name);
							}

						} elseif ($acnt == -1) {
							$em->getConnection()->rollBack();
							$this->session->set_flashdata('error', '<p>Account Already Exsists</p>');
              $this->view_call($main_folder_name, $sub_folder_name, $file_name);
						} else {
							$em->getConnection()->rollBack();
							$this->session->set_flashdata('error', '<p>User Account Failed</p>');
              $this->view_call($main_folder_name, $sub_folder_name, $file_name);
						}
					} else {
						// ACcount Number Not Exsist
						$user_account_obj = $utill_obj->add_user_account($user_obj, $user_account_available_amount, $user_account_re_purchase_amount, $user_account_transfer_per_day_limit_amount, $user_account_transfer_per_day_limit_transfer, $user_account_transfer_per_month_limit_amount, $user_account_transfer_per_month_limit_transfer, $user_account_account_per_day_transfer_enable, $user_account_per_month_transfer_enable, $user_account_hold_amount, $user_account_hold_enabled, $user_account_transfer_enabled, $user_account_is_active, $user_account_locked_reason);
						if ($user_account_obj != null) {
							$em->flush();
							$em->getConnection()->commit();
							echo "Success";
						} else {
							$em->getConnection()->rollBack();
							$this->session->set_flashdata('error', '<p>User Account Failed</p>');
              $this->view_call($main_folder_name, $sub_folder_name, $file_name);
						}
					}

				} catch (UniqueConstraintViolationException $e) {
					$em->getConnection()->rollBack();
          $this->session->set_flashdata('error', '<p>User Already Exists</p>');
          $this->view_call($main_folder_name, $sub_folder_name, $file_name);
				}
			} else {
				$this->session->set_flashdata('error', '<p>User Name Already Exists</p>');
				$this->load->view('header');
				$this->load->view('registration/user/New_user.php');
				$this->load->view('footer');
			}
		} else {
			$this->session->set_flashdata('error', '<p>Error in Fields</p>');
			$this->view_call($main_folder_name, $sub_folder_name, $file_name);
    }
	}
  public function view_call($main_folder_name, $sub_folder_name, $file_name, $data = null) {
    $this->load->view('header');
    $this->load->view($main_folder_name . '/' . $sub_folder_name . '/' . $file_name, $data);
    $this->load->view('footer');
  }
	public function update_user_from_pers_but() {
		$utill_obj = new Common_utill();
    	$em = $this->doctrine->em;
		if ($this->input->post('id') != null) {
			$this->form_validation->set_rules('first_name', 'First Name', 'trim|required|min_length[3]|max_length[20]|regex_match[/^[a-zA-Z._ ,%-]+$/]', array('required' => 'First Name Requierd', 'min_length' => 'First Name needs Minimum 3 charecters', 'max_length' => 'Maximum 20 Charecters are allowed', 'find_alphabets' => 'Albhabets Required'));
			if ($this->form_validation->run() != false) {
				$user_obj = $utill_obj->get_single_row_using_primary_key('User', $this->input->post('id'));
				if ($user_obj != null) {
          $user_obj->setFirstName($this->input->post('first_name'));
          $user_obj->setLastName($this->input->post('last_name'));
          $user_obj->setUserEmail($this->input->post('user_email'));
          $user_obj->setGender($this->input->post('gender'));
          if ($this->input->post('dob') != null) {
            $dob = new DateTime($this->input->post('dob'));
            $user_obj->setDob($dob);
          } else {
           $user_obj->setDob("");
          }
          $em->persist($user_obj);

          if($this->input->post('primary_mobile') != null){
            if($user_obj->getContactId() != null){
              $user_obj->getContactId()->setContactMobilePrimary($this->input->post('primary_mobile'));
              $user_obj->getContactId()->setContactMobileSecondary($this->input->post('secondary_mobile'));
            }else{
              $contact_obj = new Entity\Contact();
              $contact_obj->setContactMobilePrimary($this->input->post('primary_mobile'));
              $contact_obj->setContactMobileSecondary($this->input->post('secondary_mobile'));
              $em->persist($contact_obj);
              $em->flush();
              $user_obj->setContactId($em->getReference('Entity\Contact',$contact_obj->getContactId()));
            }
          }

          try {
            $em->flush();
            echo json_encode(array('name' => 'success','status'=>'1'));
          }catch (UniqueConstraintViolationException $e){ echo json_encode(array('name' => 'error','status'=>'-1')); }
				} else {echo 0;}
			} else {
         echo json_encode(array('name' => 'error','status'=>'0','array'=>$this->form_validation->error_array()));
			}

		}
	}
	public function update_user_aj_with_prof() {
		$utill_obj = new Common_utill();
    	$em = $this->doctrine->em;
		if ($this->input->post('edit_user_id') != null) {
			$this->form_validation->set_rules('first_name', 'First Name', 'trim|required|min_length[3]|max_length[20]|regex_match[/^[a-zA-Z._ ,%-]+$/]', array('required' => 'First Name Requierd', 'min_length' => 'First Name needs Minimum 3 charecters', 'max_length' => 'Maximum 20 Charecters are allowed', 'find_alphabets' => 'Albhabets Required'));
			if ($this->form_validation->run() != false) {
				$user_obj = $utill_obj->get_single_row_using_primary_key('User', $this->input->post('edit_user_id'));
				if ($user_obj != null) {
          // print_r($_POST);
          $user_obj->setFirstName($this->input->post('first_name'));
          $user_obj->setLastName($this->input->post('last_name'));
          $user_obj->setUserEmail($this->input->post('user_email'));
          $user_obj->setGender($this->input->post('gender'));
          $user_obj->setIsActive($this->input->post('is_active'));
          if ($this->input->post('dob') != null) {
            $dob = new DateTime($this->input->post('dob'));
            $user_obj->setDob($dob);
          }
          if($_FILES['files']['error'][0] != 4){
			$img_url = $utill_obj->add_id_single_image($_FILES['files']);
			$user_obj->setProfilePictureUrl($img_url);
		  }
          
          $em->persist($user_obj);

          if($this->input->post('primary_mobile') != null){
            if($user_obj->getContactId() != null){
              $user_obj->getContactId()->setContactMobilePrimary($this->input->post('primary_mobile'));
              $user_obj->getContactId()->setContactMobileSecondary($this->input->post('secondary_mobile'));
              $em->persist($user_obj->getContactId());
              $em->flush();
            }else{
              $contact_obj = new Entity\Contact();
              $contact_obj->setContactMobilePrimary($this->input->post('primary_mobile'));
              $contact_obj->setContactMobileSecondary($this->input->post('secondary_mobile'));
              $em->persist($contact_obj);
              $em->flush();
              $user_obj->setContactId($em->getReference('Entity\Contact',$contact_obj->getContactId()));
            }
          }

          try {
            $em->flush();
            echo json_encode(array('name' => 'success','status'=>'1'));
          }catch (UniqueConstraintViolationException $e){ echo json_encode(array('name' => 'error','status'=>'-1')); }
				} else {echo 0;}
			} else {
         echo json_encode(array('name' => 'error','status'=>'0','array'=>$this->form_validation->error_array()));
			}

		}
	}
	public function update_attach_aj_with(){
		$utill_obj = new Common_utill();
    	$em = $this->doctrine->em;
		if ($this->input->post('ed_user_id') != null) {
			$user_obj = $utill_obj->get_single_row_using_primary_key('User', $this->input->post('ed_user_id'));
       //print_r($_FILES);exit();
			if ($user_obj != null) {
				if($this->input->post('pan_verified') != null){
					$user_obj->setPanVerified($this->input->post('pan_verified'));
				}else{$user_obj->setPanVerified(0);}
				if($this->input->post('pass_verified') != null){
					$user_obj->setPassVerified($this->input->post('pass_verified'));
				}else{$user_obj->setPassVerified(0);}
				if($this->input->post('id_verified') != null){
					$user_obj->setAadharVerified($this->input->post('id_verified'));
				}else{$user_obj->setAadharVerified(0);}
        
				if($_FILES['gj_file1']['error'] != 4){
      			$img_url = $utill_obj->add_id_single_image($_FILES['gj_file1']);
      			$find = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_id'=>$this->input->post('ed_user_id'),'attachment_referer_type'=>7,'attachment_name'=>'pan_front')); 
			    if($find != null){
			      $find->setAttachmentImageUrl($img_url);
			      $em->persist($find);
			      $em->flush($find);
			    }else{
			      $atch = new Entity\Attachments();
			      $atch->setAttachmentImageUrl($img_url);	
			      $atch->setAttachmentName('pan_front');	
			      $atch->setAttachmentRefererType(7);	
			      $atch->setAttachmentRefererId($this->input->post('ed_user_id'));	
			      $atch->setAttachmentRemark('xxx');	
			      $em->persist($atch);
			      $em->flush();
			    }
		    }
		    if($_FILES['gj_file4']['error'] != 4){
      			$img_url = $utill_obj->add_id_single_image($_FILES['gj_file4']);
      			$find = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_id'=>$this->input->post('ed_user_id'),'attachment_referer_type'=>7,'attachment_name'=>'aadhar_front')); 

			    if($find != null){
			      $find->setAttachmentImageUrl($img_url);
			      $em->persist($find);
			      $em->flush();
			    }else{
			      $atch = new Entity\Attachments();
			      $atch->setAttachmentImageUrl($img_url);	
			      $atch->setAttachmentName('aadhar_front');	
			      $atch->setAttachmentRefererType(7);	
			      $atch->setAttachmentRefererId($this->input->post('ed_user_id'));	
			      $atch->setAttachmentRemark('xxx');	
			      $em->persist($atch);
			      $em->flush();
			    }
		    }
		    if($_FILES['gj_file5']['error'] != 4){
    			$img_url = $utill_obj->add_id_single_image($_FILES['gj_file5']);
    			$find = $em->getRepository('Entity\Attachments')->findOneBy(array('attachment_referer_id'=>$this->input->post('ed_user_id'),'attachment_referer_type'=>7,'attachment_name'=>'pass_front')); 
			    if($find != null){
			      $find->setAttachmentImageUrl($img_url);
			      $em->persist($find);
			      $em->flush();
			    }else{
			      $atch = new Entity\Attachments();
			      $atch->setAttachmentImageUrl($img_url);	
			      $atch->setAttachmentName('pass_front');	
			      $atch->setAttachmentRefererType(7);	
			      $atch->setAttachmentRefererId($this->input->post('ed_user_id'));
			      $atch->setAttachmentRemark('xxx');	
			      $em->persist($atch);
			      $em->flush();	
			    }
		    }

		    if($this->input->post('pan_no') != null){
		    	$user_obj->setPanNo($this->input->post('pan_no'));
		    }
		    if($this->input->post('aadhar_no') != null){
		    	$user_obj->setAadharNo($this->input->post('aadhar_no'));
		    }
		    $em->persist($user_obj);
      	$em->flush();
      	echo json_encode(array('name' => 'success','status'=>'1'));
			}else {echo 0;}
		}
	}
  	public function update_bank_details_from_bk_but(){
	    $utill_obj = new Common_utill();
	    $em = $this->doctrine->em;
	    if ($this->input->post('id') != null) { //print_r($_POST);exit();
	      $user_obj = $utill_obj->get_single_row_using_primary_key('User', $this->input->post('id'));
	      $bank_obj = $em->getRepository('Entity\Bank_details')->findOneBy(array('bank_ifsc' => $this->input->post('bank_ifsc')));
	      if ($user_obj != null) {
	        $account_obj = $em->getRepository('Entity\Account_details')->findOneBy(array('account_refer_id' => $this->input->post('id'), 'account_refer_type'=>1));
	        if($account_obj != null){
	          $account_obj->setAccountNo($this->input->post('account_no'));
	          if($bank_obj != null){
	            $account_obj->setBankId($bank_obj);
	          }else{
	            $bk_obj = new Entity\Bank_details();
	            $bk_obj->setBankName($this->input->post('bank_name'));
	            $bk_obj->setBankIfsc($this->input->post('bank_ifsc'));
	            $bk_obj->setBankBranch($this->input->post('bank_branch'));
	            $em->persist($bk_obj);
	            $em->flush();
	            $account_obj->setBankId($bk_obj);
	          }
	          $em->persist($account_obj);
	          $em->flush();
	          try {
	            $em->flush();
	            echo json_encode(array('name' => 'success','status'=>'1'));
	          }catch (UniqueConstraintViolationException $e){ 
	            echo json_encode(array('name' => 'error','status'=>'-1')); 
	          }
	        }else{
	          $ac_obj = new Entity\Account_details();
	          $ac_obj->setAccountNo($this->input->post('account_no'));
	          if($this->input->post('bank_ifsc') != null){
	            
	            if($bank_obj != null){
	              $ac_obj->setBankId($bank_obj);
	            }else{
	              $bk_obj = new Entity\Bank_details();
	              $bk_obj->setBankName($this->input->post('bank_name'));
	              $bk_obj->setBankIfsc($this->input->post('bank_ifsc'));
	              $bk_obj->setBankBranch($this->input->post('bank_branch'));
	              $em->persist($bk_obj);
	              $em->flush();
	              $account_obj->setBankId($bk_obj);
	            }
	          }else{
	            $em->persist($account_obj);
	            $em->flush();
	            try {
	              $em->flush();
	              echo json_encode(array('name' => 'success','status'=>'1'));
	            }catch (UniqueConstraintViolationException $e){ 
	              echo json_encode(array('name' => 'error','status'=>'-1')); 
	            }
	          }
	        }
	      } else {echo 0;}
	    }
  	}
  	public function update_communication_details_from_cm_but(){
	    $utill_obj = new Common_utill();
	    $em = $this->doctrine->em;
	
	    if ($this->input->post('id') != null) {
	      if ($this->input->post('id') != null) { //print_r($_POST);exit();
	        $user_obj = $utill_obj->get_single_row_using_primary_key('User', $this->input->post('id'));
	        if ($user_obj != null) {
	          if($user_obj->getAddressId() != null){
	          	$address_obj = $user_obj->getAddressId();
	            $address_obj->set_Address_full_address($this->input->post('full_address'));
	            if($this->input->post('pincode_no') != null){
	              $pin_obj = $utill_obj->get_single_row_using_array_value('Pincode','pincode_no', $this->input->post('pincode_no'));
	              if($pin_obj != null){
	                $address_obj->set_Address_pincode_id($pin_obj);
	              }else{
	                $pincode = Entity\Pincode();
	                $pincode->setPincodeNo($this->input->post('pincode_no'));
	                $pincode->setPincodeState($this->input->post('state'));
	                $pincode->setPincodeDistrict($this->input->post('district'));
	                if($this->input->post('country') != null){
	                  $cun_obj = $utill_obj->get_single_row_using_array_value('Country', $this->input->post('country'));
	                  if($cun_obj != null){
	                    $pincode->setPincodeCountryId($cun_obj);
	                  }
	                }
	                $em->persist($pincode);
	                $em->flush();
	                $address_obj->set_Address_pincode_id($pincode);
	              }
	            }
	            $em->persist($address_obj);
	            try {
	              $em->flush();
	              echo json_encode(array('name' => 'success','status'=>'1'));
	            }catch (UniqueConstraintViolationException $e){ 
	              echo json_encode(array('name' => 'error','status'=>'-1')); 
	            }
	          }else{
	            $ad_obj = new Entity\Address();
	            $ad_obj->set_Address_full_address($this->input->post('full_address'));
	            if($this->input->post('pincode_no') != null){
	              $pin_obj = $utill_obj->get_single_row_using_array_value('Pincode', $this->input->post('pincode_no'));
	              if($pin_obj != null){
	                $ad_obj->set_Address_pincode_id($pin_obj);
	              }else{
	                $pincode = Entity\Pincode();
	                $pincode->setPincodeNo($this->input->post('pincode_no'));
	                $pincode->setPincodeState($this->input->post('state'));
	                $pincode->setPincodeDistrict($this->input->post('district'));
	                if($this->input->post('country') != null){
	                  $cun_obj = $utill_obj->get_single_row_using_array_value('Country', $this->input->post('country'));
	                  if($cun_obj != null){
	                    $pincode->setPincodeCountryId($cun_obj);
	                  }
	                }
	                $em->persist($pincode);
	                $em->flush();
	                $ad_obj->set_Address_pincode_id($pincode);
	              }
	            }
	            $em->persist($ad_obj);
	            try {
	              $em->flush();
	              echo json_encode(array('name' => 'success','status'=>'1'));
	            }catch (UniqueConstraintViolationException $e){ 
	              echo json_encode(array('name' => 'error','status'=>'-1')); 
	            }
	          }
	        }else {echo 0;}
	      }
	    }
  	}
  	public function update_nominee_details_from_no_but(){
  		$utill_obj = new Common_utill();
	    $em = $this->doctrine->em;
	    if ($this->input->post('id') != null) {
	    	$user_obj = $utill_obj->get_single_row_using_primary_key('User', $this->input->post('id'));
	    	$nominee_obj = $user_obj->getNomineeId();
	    	if($nominee_obj != null){
	    		$nominee_obj->setNomineeName($this->input->post('nominee_name'));
	    		if ($this->input->post('n_dob') != null) {
	            	$dob = new DateTime($this->input->post('n_dob'));
	            	$nominee_obj->setNomineeDob($dob);
	          	} else {
	           		$nominee_obj->setNomineeDob("");
	          	}	
           		$nominee_obj->setNomineeRelationship($this->input->post('relation'));
           		$nominee_obj->setNomineePhoneNo($this->input->post('n_mobile'));
           		$nominee_obj->setNomineeGender($this->input->post('n_gender'));
           		$em->persist($nominee_obj);
	          	$em->flush();
	          	echo json_encode(array('name' => 'success','status'=>'1'));
	    	}else{
	    		$nominee = new Entity\Nominee();
	    		$nominee->setNomineeName($this->input->post('nominee_name'));
	    		if ($this->input->post('n_dob') != null) {
	            	$dob = new DateTime($this->input->post('n_dob'));
	            	$nominee->setDob($dob);
	          	} else {
	           		$nominee->setDob("");
	          	}	
           		$nominee->setNomineeRelationship($this->input->post('relation'));
           		$nominee->setNomineePhoneNo($this->input->post('n_mobile'));
           		$nominee->setNomineeGender($this->input->post('n_gender'));
           		$em->persist($nominee);
	          	$em->flush();
	          	$user_obj->setNomineeId($nominee);
           		$em->persist($user_obj);
	          	$em->flush();
	          	echo json_encode(array('name' => 'success','status'=>'1'));
	    	}
	    }
  	}
  	public function update_pass_details_from_pas_but(){
  		$utill_obj = new Common_utill();
	    $em = $this->doctrine->em;
	    if ($this->input->post('id') != null) {
	    	$user_obj = $utill_obj->get_single_row_using_primary_key('User', $this->input->post('id'));
	    	$pass_obj = $user_obj->getPasswordId();
	    	if($pass_obj != null){
	    		$pass_obj->setPassword($this->input->post('pwd'));
	    		$pass_obj->setSalt($this->input->post('pwd'));
	    		$user_obj->setPinStorePassword($this->input->post('pin_pwd'));
           		$em->persist($pass_obj);
           		$em->persist($user_obj);
	          	$em->flush();
	          	echo json_encode(array('name' => 'success','status'=>'1'));
	    	}else{
	          	echo json_encode(array('name' => 'fail','status'=>'0'));
	    	}
	    }
  	}
  	public function get_all_user_name_for_stockpointer() {
		$em = $this->doctrine->em;
		$key = $this->input->get('q');
		$query = $em->createQuery("SELECT a.user_id,a.user_name FROM Entity\User as a WHERE a.user_name LIKE '%$key%'  AND a.user_id != 2 AND a.user_role_id != ".RT::$default_stock_pointer_id)->setMaxResults(5);

		$single_bank = $query->getResult();
		$json = [];
		for ($i = 0; $i < count($single_bank); $i++) {
			$json[] = ['id' => $single_bank[$i]['user_id'], 'text' => $single_bank[$i]['user_name']];
		}
		echo json_encode($json);
	}
	public function get_all_user_id_for_stockpointer() {
		$em = $this->doctrine->em;
		$key = $this->input->get('q');
		$query = $em->createQuery("SELECT a.user_id FROM Entity\User as a WHERE a.user_name LIKE '%$key%'  AND a.user_id != 2 AND a.user_role_id !=".RT::$default_stock_pointer_id)->setMaxResults(5);

		$single_bank = $query->getResult();
		$json = [];
		for ($i = 0; $i < count($single_bank); $i++) {
			$json[] = ['id' => $single_bank[$i]['user_id'], 'text' => $single_bank[$i]['user_id']];
		}
		echo json_encode($json);
	}

  public function optimize_up(){
    $em = $this->doctrine->em;
    $up_obj = $em->getRepository('Entity\User_package_purchase')->findAll();
    $time_start = microtime(true);
    foreach ($up_obj as $up) {
      $user_purchase_id  = $up->getUserPurchaseId();
      $user_obj  = $up->getUserId();
      $sponcer_user_obj = $up->getUserPurchaseSponserUserPurchaseId();
      $sponcer_gene_side = $up->getUserPurchaseSponserGeneSide();
      $pin_obj =  null;
      if($up->getUserPurchasePinId() != null){
        $pin_obj = $up->getUserPurchasePinId();
      }
      $user_purchase_actual_gene_side = $up->getUserPurchaseActualGeneSide();
      $left_purchase_obj ='';
      if($up->getUserPurchaseGeneLeftPurchaseId() > 0){
        $left_purchase_obj = $em->getReference('Entity\User_package_purchase',$up->getUserPurchaseGeneLeftPurchaseId());
      }
      $right_purchase_obj ='';
      if($up->getUserPurchaseGeneRightPurchaseId()>0){
        $right_purchase_obj = $em->getReference('Entity\User_package_purchase',$up->getUserPurchaseGeneRightPurchaseId());
      }
      $money_alloted = $up->getUserPurchaseMoneyAlloted();
      $transaction_obj = $up->getUserPurchaseTransactionId();
      $pair_user_purchase_id = $up->getUserPruchasePairUserPurchaseId();
      $user_purchase_actual_gene_user_purchase_id='';
      if($up->getUserPurchaseActualGeneUserPurchaseId() > 0){
        $user_purchase_actual_gene_user_purchase_id = $em->getReference('Entity\User_package_purchase',$up->getUserPurchaseActualGeneUserPurchaseId());
      }
      $pair_achieve = $up->getPairAchieve();
      $pair_achieve_side = $up->getPairAchieveSide();
      $pair_achieve_user_purchase_id = '';
      if($up->getPairAchieveUserPurchaseId() > 0){
        $pair_achieve_user_purchase_id = $em->getReference('Entity\User_package_purchase',$up->getPairAchieveUserPurchaseId());
      }
      $created_at = $up->getCreatedAt()->format('Y-m-d');
      $updated_at='';
      if($up->getUpdatedAt() != null){
        $updated_at = $up->getUpdatedAt()->format('Y-m-d');
      }
      $payout_released = $up->getPayoutReleased();

      $obj = new Entity\New_user_package_purchase();
      $obj->setUserPurchaseId($user_purchase_id);
      $obj->setUserId($user_obj);
      $obj->setUserPurchaseSponserUserPurchaseId($sponcer_user_obj);
      $obj->setUserPurchaseActualGeneSide($sponcer_gene_side);
      if($left_purchase_obj != ''){
        $obj->setUserPurchaseGeneLeftPurchaseId($left_purchase_obj);
      }
      if($right_purchase_obj != ''){
        $obj->setUserPurchaseGeneRightPurchaseId($right_purchase_obj);
      }
      if($pin_obj != ''){
        $obj->setUserPurchasePinId($pin_obj);
      }
      if($user_purchase_actual_gene_user_purchase_id != ''){
        $obj->setUserPurchaseActualGeneUserPurchaseId($user_purchase_actual_gene_user_purchase_id);
      }
      $obj->setUserPruchasePairUserPurchaseId($pair_user_purchase_id);
      $obj->setUserPurchaseMoneyAlloted($money_alloted);
      $obj->setUserPurchaseTransactionId($transaction_obj);
      $obj->setPairAchieve($pair_achieve);
      if($pair_achieve_user_purchase_id != ''){
        $obj->setPairAchieveUserPurchaseId($pair_achieve_user_purchase_id);
      }
      $obj->setPairAchieveSide($pair_achieve_side);
      $obj->setPayoutReleased($payout_released);
      $obj->setCreatedAt($created_at);
      if($updated_at != null){
        $obj->setUpdatedAt($updated_at);
      }
      $em->persist($obj);
      $em->flush();
    }
    $time_end = microtime(true);
     echo $time = $time_end - $time_start;
     exit();
  }

  public function det_pin(){
    $em = $this->doctrine->em;
    $utill_obj = new Common_utill();
    $user_purchase_id = 1;
    $exist = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$user_purchase_id);

    if($exist->getUserPurchasePinId()->getPinRequestPackageId()->getPackageId() == RT::$default_ec_pack_id){
      $repurchase_pin_used_user_id = $exist->getUserId()->getUserId();

      $rep_qry = $em->createQuery("SELECT p.pin_id FROM Entity\Pins as p  WHERE p.pin_request_for = 1 AND p.pin_status=0 AND p.pin_used_user_id =".$repurchase_pin_used_user_id);
      $rep_obj = $rep_qry->getResult();
      if(count($rep_obj) > 0){
        echo"scu";
      }else{
        $created_dt_objy = $exist->getUserId()->getCreatedAt();
        $created_aty = $created_dt_objy->format('Y-m-d');
        $nowxy = date("Y-m-d");
        $endxy = date("Y-m-d", strtotime($created_aty."+30 days"));

        $date1=date_create($nowxy);
        $date2=date_create($endxy);
        $diff=date_diff($date1,$date2);
        echo"<pre>";print_r($diff->invert);
        
        
      }
    }else{
      echo"Hai";
    }
  }
  public function payout_left_right_manage(){
    $utill_obj = new Common_utill();
    $utill_obj->payout_left_right_manage();
  }
  public function update_auto_second(){
    $em = $this->doctrine->em;
    $up_d = date("Y-m-d").' 13:59:59';
    $d = date("Y-m-d").' 14:%';
    $qb = $em->createQueryBuilder();
    $q = $qb->update('Entity\Payout', 'u')
            ->set('u.created_at',"'".$up_d."'")
            ->where("u.created_at LIKE '".$d."'")
            ->getQuery();
    $p = $q->execute();

    $qx = $qb->update('Entity\Payout_result', 'u')
            ->set('u.created_at',"'".$up_d."'")
            ->where("u.created_at LIKE '".$d."'")
            ->getQuery();
    $px = $qx->execute();
  }
  public function update_auto_first(){
    $em = $this->doctrine->em;
    $up_d = date("Y-m-d").' 01:59:59';
    $d = date("Y-m-d").' 02:%';
    $qb = $em->createQueryBuilder();
    $q = $qb->update('Entity\Payout', 'u')
            ->set('u.created_at',"'".$up_d."'")
            ->where("u.created_at LIKE '".$d."'")
            ->getQuery();
    $p = $q->execute();

    $qx = $qb->update('Entity\Payout_result', 'u')
            ->set('u.created_at',"'".$up_d."'")
            ->where("u.created_at LIKE '".$d."'")
            ->getQuery();
    $px = $qx->execute();
  }
  public function common_update_auto_second(){
    $em = $this->doctrine->em;
    $up_d = '2017-12-05 13:59:59';
    $d = '2017-12-05 14:%';
    $qb = $em->createQueryBuilder();
    $q = $qb->update('Entity\Payout', 'u')
            ->set('u.created_at',"'".$up_d."'")
            ->where("u.created_at LIKE '".$d."'")
            ->getQuery();
    $p = $q->execute();

    $qx = $qb->update('Entity\Payout_result', 'u')
            ->set('u.created_at',"'".$up_d."'")
            ->where("u.created_at LIKE '".$d."'")
            ->getQuery();
    $px = $qx->execute();
  }
  public function send_message_first_payout(){
    $em = $this->doctrine->em;
    $up_d = date("Y-m-d").' 01:59:59';
    $utill_obj = new Common_utill();
    $query = $em->createQuery("SELECT b.payout_result_user_id,b.net_payout FROM Entity\Payout_result as b WHERE b.created_at LIKE '".$up_d."' AND b.net_payout > 0");
    $res = $query->getResult();
    if($res != null && count($res) > 0){
      for($i=0;$i<count($res);$i++){
        $up_obj = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$res[$i]['payout_result_user_id']);
        if($up_obj != null){
          $mobile = $up_obj->getUserId()->getContactId()->getContactMobilePrimary();
          $first_name = $up_obj->getUserId()->getFirstName();
          $last_name = $up_obj->getUserId()->getLastName();
          if($mobile != null){
            // MEssage Start
              $sent_message = "Dear ".$first_name.' '.$last_name.', Your Latest Net Payout Rs:'.$res[$i]['net_payout'].'. Last Cutoff DateTime : '.$up_d;
              $mobile_number = $mobile;
              $var_message = urlencode($sent_message);
              $var_destination = urlencode($mobile_number);
              $var_message_type = 2;
              $var_senderid = urlencode('METROK');
              $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL, "http://sms.dhinatechnologies.com/api/sms_api.php?username=metrokings&api_password=swyvpq558x0&message=$var_message&destination=$var_destination&type=$var_message_type&sender=$var_senderid");
              curl_setopt($ch, CURLOPT_HEADER, 0);  
              curl_exec($ch);
              curl_close($ch);
            // MEssage Start
          }
        }
      }
    }
    return true;
  }
  public function send_message_second_payout(){
    $em = $this->doctrine->em;
    $up_d = date("Y-m-d").' 13:59:59';
    $utill_obj = new Common_utill();
    $query = $em->createQuery("SELECT b.payout_result_user_id,b.net_payout FROM Entity\Payout_result as b WHERE b.created_at LIKE '".$up_d."' AND b.net_payout > 0");
    $res = $query->getResult();
    if($res != null && count($res) > 0){
      for($i=0;$i<count($res);$i++){
        $up_obj = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$res[$i]['payout_result_user_id']);
        if($up_obj != null){
          $mobile = $up_obj->getUserId()->getContactId()->getContactMobilePrimary();
          $first_name = $up_obj->getUserId()->getFirstName();
          $last_name = $up_obj->getUserId()->getLastName();
          if($mobile != null){
            // MEssage Start
              $sent_message = "Dear ".$first_name.' '.$last_name.', Your Latest Net Payout Rs:'.$res[$i]['net_payout'].'. Last Cutoff DateTime : '.$up_d;
              $mobile_number = $mobile;
              $var_message = urlencode($sent_message);
              $var_destination = urlencode($mobile_number);
              $var_message_type = 2;
              $var_senderid = urlencode('METROK');
              $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL, "http://sms.dhinatechnologies.com/api/sms_api.php?username=metrokings&api_password=swyvpq558x0&message=$var_message&destination=$var_destination&type=$var_message_type&sender=$var_senderid");
              curl_setopt($ch, CURLOPT_HEADER, 0);  
              curl_exec($ch);
              curl_close($ch);
            // MEssage Start
          }
        }
      }
    }
    return true;
  }
  public function user_search_with_ad_details(){
    $this->load->view('header');
    $this->load->view('common/User_search_with_ad_details');
    $this->load->view('footer');
  }
  public function get_user_address_information(){
    $em = $this->doctrine->em;
    $key = $this->input->get('q');
    $search_tool = $this->input->get('search_tool');
    if($search_tool == 0){
      $query = $em->createQuery("SELECT b.pincode_id,b.pincode_no FROM Entity\Pincode as b WHERE b.pincode_no LIKE '%$key%'")->setMaxResults(10);
      $single_bank = $query->getResult();
      $json = [];
      for ($i = 0; $i < count($single_bank); $i++) {
        $json[] = ['id' => $single_bank[$i]['pincode_id'], 'text' => $single_bank[$i]['pincode_no']];
      }
      echo json_encode($json);
    }elseif($search_tool == 1){
      $query = $em->createQuery("SELECT DISTINCT b.pincode_place FROM Entity\Pincode as b WHERE b.pincode_place LIKE '%$key%' ")->setMaxResults(10);
      $single_bank = $query->getResult();
      $json = [];
      for ($i = 0; $i < count($single_bank); $i++) {
        $json[] = ['id' => $single_bank[$i]['pincode_place'], 'text' => $single_bank[$i]['pincode_place']];
      }
      echo json_encode($json);
    }else{
      $query = $em->createQuery("SELECT DISTINCT b.pincode_district FROM Entity\Pincode as b WHERE b.pincode_place LIKE '%$key%'")->setMaxResults(10);
      $single_bank = $query->getResult();
      $json = [];
      for ($i = 0; $i < count($single_bank); $i++) {
        $json[] = ['id' => $single_bank[$i]['pincode_district'], 'text' => $single_bank[$i]['pincode_district']];
      }
      echo json_encode($json);
    }
  }
  public function datatable_get_user_address_information(){
    if($this->input->get('key') != null){
      $CI = &get_instance();
      $CI->load->database();
      $sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
      $tot_tds = 0;
      $table = 'User';
      $primaryKey = 'user_id';
      $columns = array(
        array( 'db' => '`us`.`user_id`', 'dt' => 0,'field' => 'user_id'),
        array( 'db' => '`us`.`user_id`', 'dt' => 1,'field' => 'user_id'),
        array( 'db' => '`us`.`user_name`', 'dt' => 2,'field' => 'user_name'),
        array( 'db' => '`us`.`first_name`', 'dt' => 3,'field' => 'first_name',
          'formatter' => function( $d, $row ) {
            return $row['first_name'].' '.$row['last_name'];
          }
        ),
        array( 'db' => '`c`.`contact_mobile_primary`', 'dt' => 4,'field' => 'contact_mobile_primary'),
        array( 'db' => '`p`.`pincode_no`', 'dt' => 5,'field' => 'pincode_no'),
        array( 'db' => '`p`.`pincode_place`', 'dt' => 6,'field' => 'pincode_place'),
        array( 'db' => '`p`.`pincode_district`', 'dt' => 7,'field' => 'pincode_district'),
        array( 'db' => '`us`.`created_at`', 'dt' => 8,'field' => 'created_at'),
        array( 'db' => '`us`.`last_name`', 'dt' => 9,'field' => 'last_name'),
      );
      $joinQuery = "FROM `User` AS `us`  INNER JOIN `Contact` as c ON (`c`.`contact_id` = `us`.`contact_id`) INNER JOIN `Address` as a ON (`a`.`address_id` = `us`.`address_id`) INNER JOIN `Pincode` as p ON (`a`.`address_pincode_id` = `p`.`pincode_id`)";
      $con = '';
      if($this->input->get('search_tool') == 0){
        $con = ' AND `p`.`pincode_id` = '.$this->input->get('key');
      }elseif($this->input->get('search_tool') == 1){
        $con = ' AND `p`.`pincode_place` LIKE "%'.$this->input->get('key').'%"';
      }else{
        $con = ' AND `p`.`pincode_district` LIKE "%'.$this->input->get('key').'%"';
      }
      $where = " 1 = 1 AND `us`.`user_id` != 2 $con";
      $result = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery,$where);
      $start=$_REQUEST['start']+1;
      $idx=0;
      foreach($result['data'] as &$res){
          $res[0]=(string)$start;
          $start++;
          $idx++;
      }
      echo json_encode($result);
    }
  }
  public function get_all_users_mobile(){
    $utill_obj = new Common_utill();
    $res = $utill_obj->get_all_users_mobile();
  }
  public function get_stock_pointers(){
    $key = $this->input->get('q');
    $em=$this->doctrine->em;
    $bnk_ar = array();
    $query = $em->createQuery("SELECT b.user_id,b.user_name FROM Entity\User as b WHERE b.user_role_id=13 OR b.user_role_id = 7" )->setMaxResults(5);
    $single_bank = $query->getResult();
    $json = [];
    for ($i=0; $i < count($single_bank); $i++) { 
      $json[] = ['id'=>$single_bank[$i]['user_id'], 'text'=>$single_bank[$i]['user_name']];
    }
    echo json_encode($json);
  }
  public function get_request_details(){
    $utill_obj = new Common_utill();
    if($this->input->post('request_id') != null){
      $obj = $utill_obj->get_single_row_using_primary_key('Shopping_pin_requests',$this->input->post('request_id'));
      if($obj != null){

        $prd_details = unserialize($obj->getProductDetails());
        
        $html = '<table class="table table-bordered">';
          $html .= '<thead><tr>';
            $html .= '<th>#</th><th>Code</th><th>Name</th><th>Price</th><th>Quantity</th><th>Total</th>';
          $html .= '</tr></thead>';
          $html .= '<tbody>';
            $tot = 0;
            if(count($prd_details) > 0){
              $i=1;
              foreach ($prd_details as $prd) {
                $html .= '<tr>';
                  $html .= '<td>'.$i.'</td>';
                  $html .= '<td>'.$prd['product_code'].'</td>';
                  $html .= '<td>'.$prd['product_name'].'</td>';
                  $html .= '<td>'.$prd['product_display_price'].'</td>';
                  $html .= '<td>'.$prd['user_quantity'].'</td>';
                  $html .= '<td>'.($prd['product_display_price']*$prd['user_quantity']).'</td>';
                $html .= '</tr>';
                $tot += $prd['product_display_price']*$prd['user_quantity'];
                $i++;
              }
            }else{
              $html .= '<tr><td colspan="6">List Empty</td></tr>';
            }
          $html .= '</tbody>';
          $html .= '<tfoot>';
            $html .= '<tr>';
               $html .= '<tr><td colspan="5" class="text-right">Total</td><td>'.$tot.'</td></tr>';
            $html .= '</tr>';
          $html .= '</tfoot>';
        $html .= '</table>';

        $html .= '<div class="row">';
          if($obj->getRequestStatus() != 1){
            $html .= '<div class="rr" style="width:100%;"><div class="col-md-4">';
            $html .= '<input type="button" class="cus_but" value="Genarate & Send" onclick="gen_and_send('.$this->input->post('request_id').')">';
            $html .= '</div><div class="col-md-4">';
            $html .= "<select class='sel_val' style='width: 100px;height: 39px;margin-right: 12px;'><option value='2'>Pending</option><option value='3'>Deleted</option></select>";
            $html .= '<input type="button" class="cus_but" value="Update Status" onclick="up_sts('.$this->input->post('request_id').')"></div>';
          }else{
            $html .= '<h4>Status : Pin Transfered</h4>';
          }
          $html .= '</div>';
        $html .= '</div>';
        echo json_encode($html);
      }else{echo 0;}
    }
  }
  public function get_request_details_view_bill(){
    $utill_obj = new Common_utill();
    if($this->input->post('request_id') != null){
      $obj = $utill_obj->get_single_row_using_primary_key('Shopping_pin_requests',$this->input->post('request_id'));
      if($obj != null){
        if($obj->getRequestUserId()->getAddressId() != null){
          $full_address = $obj->getRequestUserId()->getAddressId()->get_Address_full_address();
          if($obj->getRequestUserId()->getAddressId()->get_Address_pincode_id() != null){
            $pincode = $obj->getRequestUserId()->getAddressId()->get_Address_pincode_id()->getPinCodeNo();
            $district = $obj->getRequestUserId()->getAddressId()->get_Address_pincode_id()->getPincodeDistrict();
            $state = $obj->getRequestUserId()->getAddressId()->get_Address_pincode_id()->getPincodeState();
          }else{$pincode='';$district='';$state='';}
        }else{$full_address='';$pincode='';$district='';$state='';}
        if($obj->getRequestUserId()->getContactId() != null){
          $mobile=$obj->getRequestUserId()->getContactId()->getContactMobilePrimary();
        }else{$mobile='';}
        $date = $obj->getCreatedAt()->format('Y-m-d H:i:s');
        $invoice_id = $obj->getRequestId()+45;
        

        $prd_details = unserialize($obj->getProductDetails());
        $html='';
        $html .= '<!DOCTYPE html>
          <html>
          <head>
            <link rel="stylesheet" type="text/css" href="bootstrap.min.css">
            <script type="text/javascript" src="bootstrap.min.js"></script>
          </head>
          <body>
            <from>
              <div class="metro_gst">
                <div class="container">
                  <div class="metro_addressdiv">
                    <div class="row">
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="metrp_purcompany">
                           <div class="logo_metro_line">
                            <img src="'.base_url('assets/images/b_w_logo.png').'" class="img-responsive" alt="no_logo" />
                           </div>
                           <div class="metro_our_detail">
                            <p class="cin_no">CIN: U7499TN2015PTC102225</p>
                            <h2>METROKINGS</h2>
                            <h3>MARKETING PRIVATE LIMITED</h3>
                            <div class="full_address">
                              <p>15/64, Vattavila, Pacode P.O., Kanyakumumari District, Tamilnadu. Phone:04561 263811</p>
                              <p class="statecode">State Code:33 | GSTIN:33AAJCM9177A1ZG</p>
                            </div>

                           </div>
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="bank_detail">
                          <table>
                            <tr>
                              <th>Invoice No :</th>
                              <td>'.$invoice_id.'</td>
                            </tr>
                            <tr>
                              <th>Invoice Date :</th>
                              <td>'.$date.'</td>
                            </tr>
                            <tr>
                              <th>Bank :</th>
                              <td>Indian Overseas Bank, Kuzhithurai</td>
                            </tr>
                            <tr>
                              <th>Account Number :</th>
                              <td>28690200000013</td>
                            </tr>
                            <tr> 
                              <th>IFSC : </th>
                              <td>IOBA0002869</td>
                            </tr>
                          </table>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6 col-sm-6 col-xs-12 paddingleft">
                        <div class="Bulidin_address_div boderleft">
                          <h5>Billing Address</h5>
                          <div class="address_billding">
                            <p>'.$full_address.','.$district.' District, '.$state.'. </p>
                            <p>Pincode : '.$pincode.'</p>
                            <p>Phone: '.$mobile.'</p>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 padding">
                        <div class="Bulidin_address_div borderright">
                          <h5>Shipping Address</h5>
                          <div class="address_billding">
                            <p>'.$full_address.','.$district.' District, '.$state.'. </p>
                            <p>Pincode : '.$pincode.'</p>
                            <p>Phone: '.$mobile.'</p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="producthead">
                      <h3 class="text-center">PRODUCT SUPPLIED</h3>
                    </div>
                    <div class="table-responsive producttable">
                      <table class="  custamizetable" width="100%">
                        <thead >
                          <tr>
                            <th rowspan="2">S.NO.</th>
                            <th rowspan="2">PRODUCT</th>
                            <th rowspan="2">HSN/SAC</th>
                            <th rowspan="2">QTY</th>
                            <th rowspan="2">UOM</th>
                            <th rowspan="2">RATE</th>
                            <th rowspan="2">TOTAL</th>
                            <th rowspan="2">DISCOUNT</th>
                            <th rowspan="2">GROSS</th>
                            <th colspan="2">CGST</th>
                            <th colspan="2">SGST</th>
                            <th colspan="2">IGST</th>
                          </tr>
                          <tr>
                            <th>Rate</th>
                            <th>Amount</th>
                            <th>Rate</th>
                            <th>Amount</th>
                            <th>Rate</th>
                            <th>Amount</th>
                          </tr>
                        </thead>
                        <tbody class="tablebody">';
                        $i=0;$tot=0;$gross=0;$cgst=0;$sgst=0;$igst=0;
                        foreach ($prd_details as $prd) {
                          $i++;
                          $html .= '<tr>';
                            $html .= '<td>'.$i.'</td>';
                            $html .= '<td>'.$prd['product_name'].'</td>';
                            $html .= '<td></td>';
                            $html .= '<td>'.$prd['user_quantity'].'</td>';
                            $html .= '<td></td>';
                            $html .= '<td>'.$prd['product_display_price'].'</td>';
                            $html .= '<td>'.($prd['product_display_price']*$prd['user_quantity']).'</td>';
                            $html .= '<td></td>';
                            $html .= '<td>'.($prd['product_display_price']*$prd['user_quantity']).'</td>';
                            $html .= '<td>3</td>';
                            $html .= '<td>'.(($prd['product_display_price']*$prd['user_quantity'])*3/100).'</td>';
                            $html .= '<td>3</td>';
                            $html .= '<td>'.(($prd['product_display_price']*$prd['user_quantity'])*3/100).'</td>';
                            $html .= '<td></td>';
                            $html .= '<td></td>';
                          $html .= '</tr>';
                          $tot += $prd['product_display_price']*$prd['user_quantity'];
                          $gross += $prd['product_display_price']*$prd['user_quantity'];
                          $cgst += (($prd['product_display_price']*$prd['user_quantity'])*3/100);
                          $sgst += (($prd['product_display_price']*$prd['user_quantity'])*3/100);
                        }
                          $html .= '</tbody>
                          <tr class="tabodt2">
                            <td colspan="6" class="toalright">TOTAL</td>
                            <td>'.$tot.'</td>
                            <td></td>
                            <td>'.$gross.'</td>
                            <td colspan="2">'.$cgst.'</td>
                            <td colspan="2">'.$sgst.'</td>
                            <td colspan="2"></td>
                          </tr>
                          <tr class="tabodt23" >
                            <td colspan="8" rowspan="1" class="bordernone">For Metrokings</td>
                            <td colspan="3" class="bortaotal22">TOTAL INVOICE VALUE</td>
                            <td colspan="4"  class="bortaotal22">'.$tot.'</td>
                          </tr>
                          <tr class="fontoal">
                            <td colspan="4" class="checkborder2 botexbott" rowspan="2" >Checked by</td>
                            <td colspan="4" rowspan="2" class="botexbott">Verified by</td>
                            <td colspan="3" class="bortaotal22">TOTAL DISCOUNTS</td>
                            <td colspan="4"  class="bortaotal22">0</td>
                          </tr>
                          <tr class="fontoal">
                            <td colspan="3" class="bortaotal22">TOTAL TAXABLE VALUE</td>
                            <td colspan="4"  class="bortaotal22">0</td>
                          </tr>
                          <tr class="botrlast">
                            <td colspan="2">TOTAL CGST</td>
                            <td colspan="1">'. $cgst.'</td>
                            <td colspan="2">TOTAL SGST</td>
                            <td colspan="2">'.$sgst.'</td>
                            <td colspan="1">TOTAL IGST</td>
                            <td colspan="1">0</td>
                            <td colspan="2">TOTAL IGST</td>
                            <td colspan="4">'.($cgst+$sgst).'</td>
                          </tr>
                          <tr class="botrlast">
                            <td colspan="9"></td>
                            <td colspan="2">GRAND TOTAL</td>
                            <td colspan="4">'.($tot+$cgst+$sgst).'</td>
                          </tr>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </from>
          <style>
          p{
            margin:0px;
          }
          h1, h2, h3, h4, h5, h6{
            margin:0pc;
          }
          .metro_addressdiv {
              width: 100%;
              float: left;
              border: 2px solid black;
              padding: 10px 0px 0px;
              border-bottom: none;
          }
          .metro_our_detail {
              width: 100%;
              /* float: left; */
              text-align: right;
              padding-right: 24%;
              font-weight: 600;
              margin-bottom: 10px;
          }
          .logo_metro_line {
              width: 30%;
              float: left;
          }
          .logo_metro_line img {
              width: 50%;
              float: right;
          }
          p.cin_no {
              margin-bottom: 15px;
              font-weight: 600;
          }
          .metro_our_detail h2 {
              font-size: 30px;
              font-weight: bold;
              font-family: initial;
          }
          .metro_our_detail h3 {
              font-size: 15px;
              font-family: inherit;
              font-weight: 600;
              letter-spacing: 1px;
              margin: 3px 0px;
          }
          .full_address p {
              font-size: 11px;
              font-weight: 600;
          }
          p.statecode {
              font-size: 13px;
          }
          .full_address {
              line-height: 18px;
          }
          .bank_detail tr th {
              text-align: right;
          }
          .bank_detail tr td {
              padding-left: 15px;
          }
          .bank_detail tr {
              font-size: 15px;
              line-height: 23px;
          }
          .bank_detail tr td {
              padding-left: 15px;
              font-weight: 600;
              font-size: 13px;
          }
          .Bulidin_address_div h5 {
              border: 1px solid black;
              padding: 10px 20px;
          }
          .Bulidin_address_div{
            font-weight: 600;
          }
          .Bulidin_address_div h5 {
              border: 1px solid black;
              padding: 10px 20px;
              font-size: 16px;
              font-weight: 600;
          }
          .Bulidin_address_div.boderleft h5 {
              border-left: 0;
          }
          .Bulidin_address_div.borderright h5 {
              border-right: 0;
          }
          .address_billding {
              padding: 20px;
          }
          .bank_detail {
              padding-left: 4%;
          }
          .producthead h3 {
              font-size: 16px;
              font-weight: 600;
          }
          .producthead {
              border-bottom: 2px solid black;
              border-top: 2px solid black;
              padding: 6px 0px;
              margin-bottom: 5px;
          }
          .custamizetable {
              border: 2px solid black;
              border-left: none;
              border-right: none;
              margin-bottom: 0px;
          }
          .custamizetable th, tr {
              text-align: center;
              font-size: 16px;
          }
          .table-responsive.producttable th, td {
              border: 1px solid transparent !important;
              border-right: 1px solid black !important;
          }
          .producttable thead tr {
              border-bottom: 2px solid black !important;
          }
          tbody.tablebody {
              border-bottom: 2px solid black;
          }
          .table-responsive.producttable thead th {
              vertical-align: middle;
          }
          .bank_detail table td {
              border-right: none !important;
              text-align: left;
          }
          .paddingleft {
              padding-right: 5px;
          }
          .padding {
              padding-left: 5px;
          }
          tr.tabodt2 {
              border-bottom: 2px solid black;
          }
          tr.tabodt2 td {
              font-weight: 600;
              font-size: 14px;
              text-align: center;
          }
          .toalright{
            text-align: right !important;
          }
          td.bortaotal22 {
              border-bottom: 2px solid black !important;
          }
          td.checkborder2 {
              border-right: none !important;
          }
          .botexbott{
            vertical-align: bottom !important;
          }
          tr.tabodt2 td {
              font-weight: 600;
          }
          tr.tabodt23 td {
              font-weight: 600;
          }
          .fontoal{
            font-weight: 600;
          }
          tr.botrlast {
              border-top: 2px solid #151414;
              font-size: 14px;
              font-weight: 600;
          } 
          </style>
          </body>

          </html> ';
        echo json_encode($html);
      }else{echo 0;}
    }
  }
  public function send_pin_to_user(){
    $em = $this->doctrine->em;
    $utill_obj = new Common_utill();
    if($this->input->post('id') != null){
      $req_obj = $utill_obj->get_single_row_using_primary_key('Shopping_pin_requests',$this->input->post('id'));
      if($req_obj != null){
        $pin_no = $utill_obj->get_pin_number();
        $pin_requested_user_id = $req_obj->getRequestUserId()->getUserId();
        $pin_used_user_id = 0;
        $pin_status = 1;
        $pin_validity_time = null;
        $pin_remarks = null;
        $pin_used_time = null;
        $pck_id = 0;

        $tot_pv = 0;
        $tot_price = 0;
        $prd_detail_obj = unserialize($req_obj->getProductDetails());
        foreach ($prd_detail_obj as $prd) {
          $tot_pv += $prd['user_pv'];
          $tot_price += $prd['user_quantity']*$prd['product_display_price'];
        }

        if($req_obj->getRequestPackageId() != null){
          $pck_id = $req_obj->getRequestPackageId()->getPackageId();
          $pin_request_package_id = $req_obj->getRequestPackageId();
        }else{
          if($tot_pv >=100 && $tot_pv < 200){
            $pck_id = RT::$_100bvi_ID;
          }elseif($tot_pv >=200 && $tot_pv < 300){
            $pck_id = RT::$_200bvi_ID;
          }elseif($tot_pv >=300 && $tot_pv < 400){
            $pck_id = RT::$_300bvi_ID;
          }elseif($tot_pv >=400 && $tot_pv < 500){
            $pck_id = RT::$_400bvi_ID;
          }elseif($tot_pv >500){
            $pck_id = RT::$_500bvi_ID;
          }else{ echo 0; }
          $pin_request_package_id = $utill_obj->get_reference_obj('Packages',$pck_id);
        }
        
        $query = $em->createQuery("SELECT E.package_product_id FROM  Entity\Package_products as E WHERE E.package_id = $pck_id AND E.package_product_price <= $tot_price" );
        $pp_obj = $query->getResult();
        if($pp_obj != null){
          $pin_request_package_product_id = $utill_obj->get_reference_obj('Package_products',$pp_obj[0]);
          $login_user_obj = $utill_obj->get_single_row_using_primary_key('User',$_SESSION['user_id']);
        }else{
          $pin_request_package_product_id = null;
        }
        $pin_request_for = $req_obj->getRequestType();

       // $pin_obj = $utill_obj->add_pin($pin_no,$pin_requested_user_id,$pin_used_user_id,$pin_status,$pin_validity_time,$pin_remarks,$pin_used_time,$pin_request_package_id,$pin_request_package_product_id,$pin_request_for);

        $pin = new Entity\Pins;
        $pin->setPinNo($pin_no);
        $pin->setPinRequestedUserId($login_user_obj);
        $pin->setPinUsedUserId($pin_used_user_id);
        $pin->setPinStatus($pin_status);
        $pin->setPinValidityTime($pin_validity_time);
        $pin->setPinRemarks($pin_remarks);
        $pin->setPinUsedTime($pin_used_time);
        $pin->setPinRequestPackageId($pin_request_package_id);
        $pin->setPinRequestPackageProductId($pin_request_package_product_id);
        $pin->setPinRequestFor($pin_request_for);
        $pin->setPinTransferUserId($pin_requested_user_id);
        $pin->onPrePersist();
        $em->persist($pin);

        

        try {
          $em->flush();
          if($pin_request_for == 1){
            $req = 'Repurchase';
          }else{
            $req = 'Registration';
          }
          $html = '<!DOCTYPE html> <html> <head> 
                                <style> 
                                  table {
                                      font-family: arial, sans-serif;
                                      border-collapse: collapse;
                                      width: 100%;
                                  }
                                  td, th {
                                      border: 1px solid #dddddd;
                                      text-align: left;
                                      padding: 8px;
                                  }

                                  tr:nth-child(even) {
                                      background-color: #dddddd;
                                  }
                                  </style>
                                  </head>
                                  <body>
                                    <table>
                                      <tr>
                                        <th>User Name</th>
                                        <th>Pin No</th>
                                        <th>Package Name</th>
                                        <th>Package Price</th>
                                        <th>For</th>
                                        <th>Date</th>
                                      </tr>';
          $html .= '<tr><td>'.$login_user_obj->getUserName().'</td><td>'.$pin->getPinNo().'</td><td>'.$pin_request_package_id->getPackageName().'</td><td>'.$pin_request_package_product_id->getPackageProductDisplayPrice().'</td><td>'.$req.'</td><td>'.$pin->getCraetedAt()->format('Y-m-d H:i:s').'</td></tr>';
          $html .= '</table> </body> </html>';
          $to = "svkarthicksv@gmail.com";
          $subject = "Pin Generate Report";
          $txt = $html;
          $headers  = 'MIME-Version: 1.0' . "\r\n";
          $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
          $headers .= "From: webmaster@example.com";

          mail($to,$subject,$txt,$headers);

          $req_obj->setRequestStatus(1);
          $req_obj->onPreUpdate();
          $em->persist($req_obj);
          $em->flush();
          echo 1;
        }catch (ForeignKeyConstraintViolationException $e){ echo 0; }
      }else{echo 0;}
    }
  }

  public function update_shop_status(){
    $em = $this->doctrine->em;
    $utill_obj = new Common_utill();
    if($this->input->post('id') != null){
      $req_obj = $utill_obj->get_single_row_using_primary_key('Shopping_pin_requests',$this->input->post('id'));
      if($req_obj != null){
          $req_obj->setRequestStatus($this->input->post('status'));
          $req_obj->onPreUpdate();
          $em->persist($req_obj);
          $em->flush();
          echo 1;
      }else{echo 0;}
    }
  }

  public function optimize_payout_result(){
    $em = $this->doctrine->em;
    $utill_obj = new Common_utill();
     $query = $em->createQuery("SELECT pr,u.created_at,u.user_id,pck.package_id,u.pan_verified FROM Entity\Payout_result as pr INNER JOIN Entity\User_package_purchase as up WITH (pr.payout_result_user_id = up.user_purchase_id) INNER JOIN Entity\User as u WITH (u.user_id = up.user_id) INNER JOIN Entity\Pins as p WITH (up.user_purchase_pin_id = p.pin_id) INNER JOIN Entity\Packages as pck WITH (p.pin_request_package_id = pck.package_id) WHERE u.pan_verified=0");

    $res = $query->getResult();

    foreach ($res as $xx) {
      $created_dt_objy = $xx['created_at']->format('Y-m-d');   //  User Created At
      $nowxy = $xx[0]->getCreatedAt()->format('Y-m-d');        //  Payout Result Created At
      $date1=date_create($created_dt_objy);
      $date2=date_create($nowxy);
      $diff=date_diff($date1,$date2);

      $full_total_left = $xx[0]->getTotalLeftPv();
      $full_total_right = $xx[0]->getTotalRightPv();

      if($full_total_left >0 && $full_total_right > 0){
        $pan_verified = $xx['pan_verified'];


        if($xx['package_id'] == 18){
          $rep_qry = $em->createQuery("SELECT p.pin_id FROM Entity\Pins as p  WHERE p.pin_request_for = 1 AND p.pin_status=0 AND p.pin_used_user_id =".$xx['user_id']);
          $rep_obj = $rep_qry->getResult();
          if(count($rep_obj) > 0){
            $rep_cnt = 1;
          }else{
            $created_dt_objy = $xx['created_at']->format('Y-m-d');   //  User Created At
            $nowxy = $xx[0]->getCreatedAt()->format('Y-m-d');        //  Payout Result Created At
            $date1=date_create($created_dt_objy);
            $date2=date_create($nowxy);
            $diff=date_diff($date1,$date2);
            if($diff->days <= 30){
              $rep_cnt = 1;
            }else{
              $rep_cnt = 0;
            }
          }
        }else{$rep_cnt = 1;}

        if($full_total_left>$full_total_right){
          $full_tot = 2*$full_total_right;
          if($full_tot != 0){
            if($pan_verified == 1){
              if($rep_cnt == 1){
                $full_tot -= $full_tot*20/100;
              }else{
                $x = $full_tot*20/100;
                $full_tot = ($full_tot-$x)/2;
              }
            }else{
              if($rep_cnt == 1){
                $full_tot -= $full_tot*35/100;
              }else{
                $x = $full_tot*35/100;
                $full_tot = ($full_tot-$x)/2;
              }
            }
          }
        }else{
          $full_tot = 2*$full_total_left;
          if($full_tot != 0){
            if($pan_verified == 1){
              if($rep_cnt == 1){
                $full_tot -= $full_tot*20/100;
              }else{
                $x = $full_tot*20/100;
                $full_tot = ($full_tot-$x)/2;
              }
            }else{
              if($rep_cnt == 1){
                $full_tot -= $full_tot*35/100;
              }else{
                $x = $full_tot*35/100;
                $full_tot = ($full_tot-$x)/2;
              }
            }
          }
        }

        $obj = $utill_obj->get_single_row_using_primary_key('Payout_result',$xx[0]->getPayoutResultId());
        if($obj != null){
          $obj->setNetPayOut($full_tot);
          $em->persist($obj);
          $em->flush();
        }else{
          echo"Problem".$xx[0]->getPayoutResultId();
        }


        // echo 'ID     '.$xx[0]->getPayoutResultId();echo"<br>";
        // echo 'full_total_left'.$full_total_left;echo"<br>";
        // echo 'full_total_right'.$full_total_right;echo"<br>";
        // if($full_total_left > $full_total_right){
        //   echo 'tot'.(2*$full_total_right);echo"<br>";
        // }else{echo 'tot'.(2*$full_total_left);echo"<br>";}
        // echo $full_tot;echo"<br>";

        // // echo $xx['user_id'];



        // echo"<pre>";print_r($diff);
      }

    }
  }

  public function reverse_tree($user_id){
    // $user_purchase_id = 1165;
    $obj = $utill_obj->get_single_row_using_array_value('User_package_purchase','user_id',$user_id);
    $tree_ar = $this->get_r_tree($obj->getUserPurchaseId(),$obj->getUserPurchaseId());
    return true;
  }
  public function get_r_tree($id,$or_up_id){
    $em=$this->doctrine->em;
    $utill_obj = new Common_utill();
    $obj = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$id);
    if($obj != null){
      if($obj->getLevelIncomeAchieve() == 0){
        $bbj = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$or_up_id);
        $user_id = $obj->getUserId();
        $amount = 5;
        $achieve_user_id = $bbj->getUserId();
        $u_id = $obj->getUserId()->getUserId();
        if($obj->getUserPurchaseActualGeneUserPurchaseId() == 0){
          $utill_obj->add_level_income($user_id,$amount,$achieve_user_id);

          $query = $em->createQuery("SELECT COUNT(c.income_id) FROM Entity\Level_income as c WHERE c.user_id = $u_id");
          $cnt = $query->getResult();
          if($cnt[0][1] == 10){
            $obj->setLevelIncomeAchieve(1);
            $em->persist($obj);
            $em->flush();
          }
          //array_push(self::$reverse_tree_ar,$obj->getUserPurchaseId());
        }else{
          $utill_obj->add_level_income($user_id,$amount,$achieve_user_id);

          $query = $em->createQuery("SELECT COUNT(c.income_id) FROM Entity\Level_income as c WHERE c.user_id = $u_id");
          $cnt = $query->getResult();
          if($cnt[0][1] == 10){
            $obj->setLevelIncomeAchieve(1);
            $em->persist($obj);
            $em->flush();
          }

          //array_push(self::$reverse_tree_ar,$obj->getUserPurchaseId());
          $this->get_r_tree($obj->getUserPurchaseActualGeneUserPurchaseId(),$or_up_id);
        }
      }else{
        $this->get_r_tree($obj->getUserPurchaseActualGeneUserPurchaseId(),$or_up_id);
      }
      return self::$reverse_tree_ar;
    }
  }
  public function reverse_tree_payout($id){
    $utill_obj = new Common_utill();
    $obj = $utill_obj->get_single_row_using_array_value('User_package_purchase','user_id',$user_id);
    // $obj = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$user_purchase_id);
    if($obj != null){
      if($obj->getUserPurchaseActualGeneUserPurchaseId() > 0 ){
        $tree_ar = $this->get_r_tree_for_payout($obj->getUserPurchaseActualGeneUserPurchaseId());
        return true;
      }else{return false;}
    }else{return false;}
  }
  public function get_r_tree_for_payout($id){
    $em=$this->doctrine->em;
    $utill_obj = new Common_utill();
    $obj = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$id);
    if($obj != null){
      if($obj->getUserPurchaseActualGeneUserPurchaseId() > 0 ){
        if($obj->getPairAchieve() == 1){
          $obj->setLevelIncomeAchieve(1);
          $em->persist($obj);
          $em->flush();
          // array_push(self::$reverse_tree_ar,$obj->getUserPurchaseId());
          $obj->setPayoutAch(1);
          $em->persist($obj);
          $em->flush();
        }else{
          $this->get_r_tree_for_payout($obj->getUserPurchaseId());
        }
      }else{
        if($obj->getPairAchieve() == 1){
          // array_push(self::$reverse_tree_ar,$obj->getUserPurchaseId());
          $obj->setPayoutAch(1);
          $em->persist($obj);
          $em->flush();
        }
      }
    }
    return true;
    // return self::$reverse_tree_ar;
  }
  public function pay_res_gross(){
    $em=$this->doctrine->em;
    $utill_obj = new Common_utill();
    $obj = $em->getRepository('Entity\Payout_result')->findAll();
    foreach ($obj as $bbj) {
      $left = $bbj->getTotalLeftPv();
      $right = $bbj->getTotalRightPv();
      if($left <= $right){
        $tot = 2*$left;
      }else{
        $tot = 2*$right;
      }
      $bbj->setTotalGross($tot);
      $em->persist($bbj);
      $em->flush();
    }
  }

  public function get_payout_report(){
    $em=$this->doctrine->em;
    $utill_obj = new Common_utill();
    $ar = array();
    $query = $em->createQuery("SELECT b.total_left,b.total_right,b.created_at,b.carry_forward,b.carry_forward_side,b.previous_left,b.previous_right FROM Entity\Payout as b WHERE b.user_purchase_id=331 AND (b.total_left > 0 OR b.total_right > 0)");
    $res = $query->getResult();
    foreach ($res as $rr) {
      $query = $em->createQuery("SELECT b.carry_forward_pv,b.carry_forward_pv_side,b.net_payout FROM Entity\Payout_result as b WHERE b.payout_result_user_id=331 AND b.created_at LIKE '".$rr['created_at']->format('Y-m-d H:i:s')."'");
       $rx = $query->getResult();
       $a = array();

    }
  }

  public function optimize_repurchase_pin(){
    $em=$this->doctrine->em;
    $utill_obj = new Common_utill();
    $res = $utill_obj->get_multiple_rows_using_array_value('Pins','pin_request_for',1);
    foreach ($res as $rs) {
      $result = $utill_obj->get_pin_detail_status_repurchase_int($rs->getPinNo());
      if($result == 1){
        $rs->setRepurchaseType(1);
        $em->persist($rs);
        $em->flush();
      }

    }
  }
}?>
