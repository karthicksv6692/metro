<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Validation{
	public	$v_required ='required|trim';

	//Valid name
	public	$v_name ='trim|min_length[3]|max_length[15]|regex_match[/^[A-Za-z][A-Za-z0-9 .]+$/]';
	public  $v_name_msg=array('required'=>'Provide %s.','regex_match'=>'Must starts with alphabet,No special chars are allowed.');

	public	$v_name_multi='trim|min_length[3]|max_length[50]|regex_match[/^[A-Za-z][A-Za-z0-9-_ \.&]+$/]';
	public  $v_name_multi_msg=array('required'=>'Provide %s.','regex_match'=>'Provide valid name.');
	public	$car_name='trim|min_length[3]|max_length[50]|regex_match[/^[A-Za-z][A-Za-z0-9-_ \.&]+$/]';


	public	$v_email ='valid_email';
	public	$v_phone ='trim|numeric|min_length[10]|max_length[12]';
	public	$v_pincode ='trim|is_natural|numeric|exact_length[6]';
	public	$v_sm_desc ='trim|min_length[10]';
	public	$v_address ='trim|min_length[10]';
	public	$v_tin_no ='trim|min_length[10]|alpha_numeric';
	public	$v_pan_no ='trim|min_length[10]|alpha_numeric';
	public	$v_gst_no ='trim|min_length[10]|alpha_numeric';
	public	$v_acc_no ='trim|min_length[10]|alpha_numeric|max_length[15]';
	public	$v_ifsc_code ='trim|min_length[8]|alpha_numeric|max_length[15]';
	public	$v_landline ='trim|min_length[8]|max_length[16]|regex_match[/^[0-9]\d{2,6}-\d{6,8}$/]';

	//Numeric
	public	$v_numeric ='trim|numeric';
	public	$v_numeric_min_2 ='trim|numeric|min_length[2]';

	public	$v_float ='trim|regex_match[/^(?<=^| )\d+(\.\d+)?(?=$| )|(?<=^| )\.\d+(?=$| )$/]';
	public	$v_float_msg=array('required'=>'Provid %s.','regex_match'=>'Provide data like 3.00');

	public	$v_range ='trim|regex_match[/^(?<=^| )\d+(\-\d+)?(?=$| )|(?<=^| )\-\d+(?=$| )$/]';

	//Alpha
	public	$v_alpha ='trim|alpha';
	public	$v_alpha_space ='trim|regex_match[/^[a-zA-Z\s]*$/]';
	public	$v_alpha_space_min_3 ='trim|regex_match[/^[a-zA-Z\s]*$/]|min_length[3]';

	// Alpha numeric
	public	$v_alphamumeric ='trim|alpha_numeric';
	public	$v_alphanumeric_space ='trim|regex_match[/^[A-Za-z0-9 \-]+$/]';
	public	$v_alphanumeric_space_dot ='trim|regex_match[/^[A-Za-z0-9 .\-]+$/]';
	public	$v_alphanumeric_space_min_3 ='trim|regex_match[/^[A-Za-z0-9 \-]+$/]|min_length[3]';

	
	public $v_msg=array('required'=>'Provide %s.','alpha'=>'Provide only letters','numeric'=>'Provide only numbers','regex_match'=>'Provide valid data');
	//End validation strings and message
}
/* Alpha numeric start with char min 3*/// regx /^[a-zA-Z][a-zA-Z0-9 ]{2,}$/