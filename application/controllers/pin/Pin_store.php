<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\Query\ResultSetMapping;
require_once 'ssp.customized.class.php';
  class Pin_store extends CI_Controller {
    public function __construct(){
      parent::__construct(); 
      is_user_logged_in();
      
    }
    public function index(){
      $current_data = array(  'cur_data' => "");
      $this->session->set_userdata($current_data);
      $utill_obj = new Common_utill();
      //$data['add'] = $utill_obj->has_access('pincode','add');
      $this->view();
    }
    public function view(){
      is_pinstore_logged_in();
      $this->load->view('header');
      $this->load->view('pin/pin_store/View');
      $this->load->view('footer');  
    }
    public function used(){
      is_pinstore_logged_in();
      $this->load->view('header');
      $this->load->view('pin/pin_store/Used');
      $this->load->view('footer');  
    }
    public function un_used(){
      is_pinstore_logged_in();
      $em=$this->doctrine->em;
      $utill_obj = new Common_utill();
      if($_SESSION['user_id'] == 2){
        $query = $em->createQuery("SELECT COUNT(p.pin_id),u.user_name,u.user_id FROM Entity\Pins as p INNER JOIN Entity\User as u WITH(u.user_id = p.pin_transfer_user_id) WHERE p.pin_status=1 AND p.pin_transfer_user_id > 0 AND p.pin_request_for = 0 GROUP BY p.pin_transfer_user_id");
        $data['trans_registration'] = $query->getResult();
        //print_r($res);
        $query = $em->createQuery("SELECT COUNT(p.pin_id),u.user_name,u.user_id FROM Entity\Pins as p INNER JOIN Entity\User as u WITH(u.user_id = p.pin_transfer_user_id) WHERE p.pin_status=1 AND p.pin_transfer_user_id > 0 AND p.pin_request_for = 1 GROUP BY p.pin_transfer_user_id");
        $data['trans_repurchase'] = $query->getResult();
        //print_r($res);
        $query = $em->createQuery("SELECT COUNT(p.pin_id),u.user_name,u.user_id FROM Entity\Pins as p INNER JOIN Entity\User as u WITH(u.user_id = p.pin_requested_user_id) WHERE p.pin_status=1 AND p.pin_transfer_user_id = 0 AND p.pin_request_for = 1 GROUP BY p.pin_requested_user_id");
        $data['repurchase'] = $query->getResult();
        $query = $em->createQuery("SELECT COUNT(p.pin_id),u.user_name,u.user_id FROM Entity\Pins as p INNER JOIN Entity\User as u WITH(u.user_id = p.pin_requested_user_id) WHERE p.pin_status=1 AND p.pin_transfer_user_id = 0 AND p.pin_request_for = 0 GROUP BY p.pin_requested_user_id");
        $data['registration'] = $query->getResult();

        $this->load->view('header');
        $this->load->view('pin/pin_store/Un-used',$data);
        $this->load->view('footer'); 
      }else{
        $this->load->view('header');
        $this->load->view('pin/pin_store/Un_used_user');
        $this->load->view('footer');
      } 
    }
    public function un_used_user(){
      is_pinstore_logged_in();
      $this->load->view('header');
      $this->load->view('pin/pin_store/Un_used_user');
      $this->load->view('footer');
    }
    public function un_used_view(){ //print_r($_POST);exit();
      $em=$this->doctrine->em;
      if($this->input->post('user_id') != null){
        if($this->input->post('pin_type') == 0){
          $query = $em->createQuery("SELECT p.pin_no,p.created_at,pck.package_name,pp.package_product_name,pp.package_product_display_price,u.user_name FROM Entity\Pins as p INNER JOIN Entity\User as u WITH(u.user_id = p.pin_requested_user_id) INNER JOIN Entity\Packages as pck WITH (pck.package_id = p.pin_request_package_id) INNER JOIN Entity\Package_products as pp WITH (pp.package_product_id=p.pin_request_package_product_id) WHERE p.pin_status=1 AND p.pin_transfer_user_id = 0 AND p.pin_request_for = 0 AND p.pin_requested_user_id=".$this->input->post('user_id'));
        }elseif($this->input->post('pin_type') == 1){
          $query = $em->createQuery("SELECT p.pin_no,p.created_at,pck.package_name,pp.package_product_name,pp.package_product_display_price,u.user_name FROM Entity\Pins as p INNER JOIN Entity\User as u WITH(u.user_id = p.pin_transfer_user_id) INNER JOIN Entity\Packages as pck WITH (pck.package_id = p.pin_request_package_id) INNER JOIN Entity\Package_products as pp WITH (pp.package_product_id=p.pin_request_package_product_id) WHERE p.pin_status=1 AND p.pin_transfer_user_id > 0 AND p.pin_request_for = 0 AND p.pin_transfer_user_id=".$this->input->post('user_id'));
        }elseif($this->input->post('pin_type') == 2){
          $query = $em->createQuery("SELECT p.pin_no,p.created_at,pck.package_name,pp.package_product_name,pp.package_product_display_price,u.user_name FROM Entity\Pins as p INNER JOIN Entity\User as u WITH(u.user_id = p.pin_requested_user_id) INNER JOIN Entity\Packages as pck WITH (pck.package_id = p.pin_request_package_id) INNER JOIN Entity\Package_products as pp WITH (pp.package_product_id=p.pin_request_package_product_id) WHERE p.pin_status=1 AND p.pin_transfer_user_id = 0 AND p.pin_request_for = 1 AND p.pin_requested_user_id=".$this->input->post('user_id'));
        }elseif($this->input->post('pin_type') == 3){
          $query = $em->createQuery("SELECT p.pin_no,p.created_at,pck.package_name,pp.package_product_name,pp.package_product_display_price,u.user_name FROM Entity\Pins as p INNER JOIN Entity\User as u WITH(u.user_id = p.pin_transfer_user_id) INNER JOIN Entity\Packages as pck WITH (pck.package_id = p.pin_request_package_id) INNER JOIN Entity\Package_products as pp WITH (pp.package_product_id=p.pin_request_package_product_id) WHERE p.pin_status=1 AND p.pin_transfer_user_id > 0 AND p.pin_request_for = 1 AND p.pin_transfer_user_id=".$this->input->post('user_id'));
        }else{$this->un_used();}
        $data['res'] = $query->getResult();
        $data['user'] = $data['res'][0]['user_name'];
        $this->load->view('header');
        $this->load->view('pin/pin_store/Un_used_view',$data);
        $this->load->view('footer');
      }else{
        $this->un_used();
      }
    }
    public function datatable_ajax(){
      $ss='';$ee='';$dd='';
      $CI = &get_instance();
      $CI->load->database();
      $sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
      $table = 'Pins';
      $primaryKey = 'pin_id';
      $columns = array(
          array( 'db' => '`a`.`pin_no`', 'dt' => 0,'field' => 'pin_no'),
          array( 'db' => '`a`.`pin_no`', 'dt' => 1,'field' => 'pin_no'),
          array( 'db' => '`a`.`pin_request_for`', 'dt' => 2,'field' => 'pin_request_for','formatter' => function( $d, $row ){
                if($row['pin_request_for'] == 0){
                  return "Registration";
                }else{return "Repurchase";}
              }),

          array( 'db' => '`p`.`package_name`', 'dt' => 3,'field' => 'package_name'),
          array( 'db' => '`pp`.`package_product_name`', 'dt' => 4,'field' => 'package_product_name'),
          array( 'db' => 'a.pin_status',  'dt' => 5 ,'field' => 'pin_status','formatter' => function( $d, $row ){
              if($row['pin_status'] == 0){
                return "Used";
              }else{return "Not-Used";}
            }),
      );
      $joinQuery = "FROM `Pins` AS `a` INNER JOIN `Packages` AS `p` ON (`a`.`pin_request_package_id` = `p`.`package_id`) INNER JOIN `Package_products` as `pp` ON (`a`.`pin_request_package_product_id` = `pp`.`package_product_id`)";

      $where = "pin_requested_user_id = ".$_SESSION['user_id'];
       
      echo json_encode(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery,$where));
    }
    public function datatable_ajax_used_pins(){
      $ss='';$ee='';$dd='';
      $CI = &get_instance();
      $CI->load->database();
      $sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
      $table = 'Pins';
      $primaryKey = 'pin_id';
      $columns = array(
          // array( 'db' => '`a`.`pin_no`', 'dt' => 0,'field' => 'pin_no'),
          array( 'db' => '`a`.`pin_no`', 'dt' => 1,'field' => 'pin_no'),
          array( 'db' => '`u`.`user_id`', 'dt' => 2,'field' => 'user_id'),
          array( 'db' => '`a`.`pin_request_for`', 'dt' => 3,'field' => 'pin_request_for','formatter' => function( $d, $row ){
                if($row['pin_request_for'] == 0){
                  return "Registration";
                }else{return "Repurchase";}
              }),

          array( 'db' => '`p`.`package_name`', 'dt' => 4,'field' => 'package_name'),
          array( 'db' => '`pp`.`package_product_name`', 'dt' => 5,'field' => 'package_product_name'),
          array( 'db' => '`u`.`created_at`', 'dt' => 6,'field' => 'created_at'),
          
      );
      $joinQuery = "FROM `Pins` AS `a` INNER JOIN `Packages` AS `p` ON (`a`.`pin_request_package_id` = `p`.`package_id`) INNER JOIN `Package_products` as `pp` ON (`a`.`pin_request_package_product_id` = `pp`.`package_product_id`) INNER JOIN `User` AS `u` ON (`u`.`user_id` = `a`.`pin_used_user_id` )";

      if($_SESSION['user_id'] == 2 || $_SESSION['role_id'] == 1){
       $where = "`a`.`pin_status` = 0 AND  `a`.pin_requested_user_id = ".$_SESSION['user_id']." AND `a`.pin_used_user_id != 0";
      }else{
        $where = "`a`.`pin_status` = 0 AND  `a`.pin_transfer_user_id = ".$_SESSION['user_id']." AND `a`.pin_used_user_id != 0";
      }

       
      $result=SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery,$where);
      $start=$_REQUEST['start']+1;
      $idx=0;
      foreach($result['data'] as &$res){
          $res[0]=(string)$start;
          $start++;
          $idx++;
      }
      echo json_encode($result);
    }
    public function datatable_ajax_unused_pins(){
      $ss='';$ee='';$dd='';
      $CI = &get_instance();
      $CI->load->database();
      $sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
      $table = 'Pins';
      $primaryKey = 'pin_id';
      $columns = array(
          array( 'db' => '`a`.`pin_no`', 'dt' => 0,'field' => 'pin_no'),
          array( 'db' => '`a`.`pin_no`', 'dt' => 1,'field' => 'pin_no'),
          array( 'db' => '`a`.`pin_request_for`', 'dt' => 2,'field' => 'pin_request_for','formatter' => function( $d, $row ){
                if($row['pin_request_for'] == 0){
                  return "Registration";
                }else{return "Repurchase";}
              }),

          array( 'db' => '`p`.`package_name`', 'dt' => 3,'field' => 'package_name'),
          array( 'db' => '`pp`.`package_product_name`', 'dt' => 4,'field' => 'package_product_name'),
          
      );
      $joinQuery = "FROM `Pins` AS `a` INNER JOIN `Packages` AS `p` ON (`a`.`pin_request_package_id` = `p`.`package_id`) INNER JOIN `Package_products` as `pp` ON (`a`.`pin_request_package_product_id` = `pp`.`package_product_id`)";

      // print_r($_SESSION)

      if($_SESSION['role_id'] == 7){
        $where = "`a`.`pin_status` = 1 AND `a`.pin_requested_user_id = ".$_SESSION['user_id']." AND `a`.pin_used_user_id = 0 ";
      }elseif($_SESSION['role_id'] == 8){
        $where = "`a`.`pin_status` = 1 AND (`a`.pin_requested_user_id = ".$_SESSION['user_id']." OR `a`.pin_transfer_user_id = ".$_SESSION['user_id'].") AND `a`.pin_used_user_id = 0 ";
      }else{
        $where = "`a`.`pin_status` = 1 AND `a`.pin_transfer_user_id = ".$_SESSION['user_id']." AND `a`.pin_used_user_id = 0";
      }

      $result=SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery,$where);
      $start=$_REQUEST['start']+1;
      $idx=0;
      foreach($result['data'] as &$res){
          $res[0]=(string)$start;
          $start++;
          $idx++;
      }
      echo json_encode($result);
    }
    public function pin_transfer(){ 
      $main_folder_name='pin';$sub_folder_name='pin_store';
      if($_SESSION['user_id'] == 2){
        $file_name='Transfer_pin';
      }else{$file_name='User_transfer_pin';}
      is_pinstore_logged_in();
      $em=$this->doctrine->em;
      $utill_obj = new Common_utill();
      if($this->input->post('user') != null && $this->input->post('hid_frm') != null){ 
        // print_r($_POST);exit();
        $this->form_validation->set_rules('hid_ar', 'Pin','trim|required',array('required'=>'Required','regex_match' => 'Enter Valid Pin'));
        $this->form_validation->set_rules('user', 'User','trim|required|regex_match[/^[0-9\-]+$/]',array('required'=>'Required','regex_match' => 'Enter Valid User'));
        if ($this->form_validation->run() != false){
          $pin_ar = explode(',', $this->input->post('hid_ar'));
          // print_r($pin_ar);exit();
          if(count($pin_ar) > 0){
            if(count($pin_ar) == 1 && $pin_ar[0] == 0){
              $this->session->set_flashdata('error', '<p>Select Pins</p>');
              $this->view_call($main_folder_name,$sub_folder_name,$file_name);
            }else{
              $user_id = $this->input->post('user');
              $user_check = $utill_obj->get_single_row_using_primary_key('User',$user_id);
              if($user_check != null){
                for($i=0;$i<count($pin_ar);$i++){
                  $pin_obj = $utill_obj->get_single_row_using_primary_key('Pins',$pin_ar[$i]);
                  if($pin_obj != null){
                    $pin_obj->setPinTransferUserId($user_id);
                    $pin_obj->onPreUpdate();
                    $pin_obj->setPinTransferDate();
                    $em->persist($pin_obj);
                    $em->flush();
                  }
                  if($i == count($pin_ar)-1 ){
                    $this->session->set_flashdata('success', '<p>Pin Transfer Successfully</p>');
                    redirect('index.php/pin/pin_store','refresh');
                  }
                }
              }else{
                $this->session->set_flashdata('error', '<p>User Not Exists</p>');
                $this->view_call($main_folder_name,$sub_folder_name,$file_name);
              }

            }
          }else{
            $this->session->set_flashdata('error', '<p>Select Pins</p>');
            $this->view_call($main_folder_name,$sub_folder_name,$file_name);
          }
        }else{
          $this->session->set_flashdata('error', '<p>Error in Fields</p>');
          $this->view_call($main_folder_name,$sub_folder_name,$file_name);
        }
      }else{
        $this->view_call($main_folder_name,$sub_folder_name,$file_name);
      }
    }
    public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
      $this->load->view('header');
      $this->load->view(''.$main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
      $this->load->view('footer');
    }
    
    public function pin_transfer_history(){
      is_pinstore_logged_in();
      $this->load->view('header');
      $this->load->view('pin/pin_store/Transfer_history');
      $this->load->view('footer');
    }
    public function datatable_ajax_tranfer_pins(){
      $ss='';$ee='';$dd='';
      $CI = &get_instance();
      $CI->load->database();
      $sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
      $table = 'Pins';
      $primaryKey = 'pin_id';
      $columns = array(
          array( 'db' => '`a`.`pin_no`', 'dt' => 0,'field' => 'pin_no'),
          array( 'db' => '`a`.`pin_no`', 'dt' => 1,'field' => 'pin_no'),
          array( 'db' => '`a`.`pin_request_for`', 'dt' => 2,'field' => 'pin_request_for','formatter' => function( $d, $row ){
                if($row['pin_request_for'] == 0){
                  return "Registration";
                }else{return "Repurchase";}
              }),

          array( 'db' => '`p`.`package_name`', 'dt' => 3,'field' => 'package_name'),
          array( 'db' => '`pp`.`package_product_name`', 'dt' => 4,'field' => 'package_product_name'),

          array( 'db' => 'a.pin_transfer_user_id',  'dt' => 5 ,'field' => 'pin_transfer_user_id','formatter' => function( $d, $row ){
            if($row['pin_transfer_user_id'] == $_SESSION['user_id']){
              return "Received";
            }else{return "Sent";}
          }),
          array( 'db' => 'usx.user_name',  'dt' => 6 ,'field' => 'user_name'),
          array( 'db' => 'usx.user_id',  'dt' => 7 ,'field' => 'user_id'),
          array( 'db' => 'a.pin_transfer_date',  'dt' =>8 ,'field' => 'pin_transfer_date','formatter' => function( $d, $row ){
            if($row['pin_transfer_date'] != null){
              return $row['pin_transfer_date'];
            }else{return "--";}
          }),
      );
      $joinQuery = "FROM `Pins` AS `a` INNER JOIN `Packages` AS `p` ON (`a`.`pin_request_package_id` = `p`.`package_id`) INNER JOIN `User` as `us` ON (`a`.`pin_requested_user_id` = `us`.`user_id`) INNER JOIN `Package_products` as `pp` ON (`a`.`pin_request_package_product_id` = `pp`.`package_product_id`) INNER JOIN `User` as `usx` ON (`a`.`pin_transfer_user_id` = `usx`.`user_id`)";

      $where = "  `a`.`pin_transfer_user_id` != 0  AND `a`.pin_requested_user_id = ".$_SESSION['user_id'];
       
      $result=SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery,$where);
      $start=$_REQUEST['start']+1;
      $idx=0;
      foreach($result['data'] as &$res){
          $res[0]=(string)$start;
          $start++;
          $idx++;
      }
      echo json_encode($result);
    }
    public function datatable_ajax_tranfer_pins_search(){
      if($_GET['dt1'] != null){
        if($_GET['pos1'] == 0){
          $dt1 = $_GET['dt1'].' 02:00:00';
          if($_GET['dt2'] == null){
            $dt2 = $_GET['dt1'].' 13:59:59';
          }
        }elseif($_GET['pos1'] == 1){
          $dt1 = $_GET['dt1'].' 14:00:00';
          if($_GET['dt2'] == null){
             $d = $_GET['dt1'].' 01:59:59';
             $dt2 = date("Y-m-d H:i:s", strtotime($d."+1 days"));
            
            //echo($dt2);
          }
        }else{
            $dt1 = $_GET['dt1'].' 02:00:00';
            $d = $_GET['dt1'].' 01:59:59';
            $dt2 = date("Y-m-d H:i:s", strtotime($d."+1 days"));
        }
      }
      if($_GET['dt1'] != null){
        if($_GET['dt2'] != null){
          if($_GET['pos2'] == 0){
            $dt2 = $_GET['dt2'].' 02:00:00';
          }else{
            $dt2 = $_GET['dt2'].' 13:59:59';
          }
        }
      }
      
      $CI = &get_instance();
      $CI->load->database();
      $sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
      $tot_tds = 0;
      $table = 'Pins';
      $primaryKey = 'pin_id';
      $columns = array(
        array( 'db' => '`a`.`pin_no`', 'dt' => 0,'field' => 'pin_no'),
        array( 'db' => '`a`.`pin_no`', 'dt' => 1,'field' => 'pin_no'),
        array( 'db' => '`a`.`pin_request_for`', 'dt' => 2,'field' => 'pin_request_for','formatter' => function( $d, $row ){
          if($row['pin_request_for'] == 0){
            return "Registration";
          }else{return "Repurchase";}
        }),
        array( 'db' => '`p`.`package_name`', 'dt' => 3,'field' => 'package_name'),
        array( 'db' => '`pp`.`package_product_name`', 'dt' => 4,'field' => 'package_product_name'),
        array( 'db' => '`us`.`user_name`', 'dt' => 5,'field' => 'user_name'),
        array( 'db' => 'a.pin_transfer_date',  'dt' =>6 ,'field' => 'pin_transfer_date','formatter' => function( $d, $row ){
          if($row['pin_transfer_date'] != null){
            return $row['pin_transfer_date'];
          }else{return "--";}
        }),
        array( 'db' => '`usx`.`user_id`', 'dt' => 7,'field' => 'user_id','formatter' => function( $d, $row ){
          return $row['first_name'].' '.$row['last_name'];
        }),
        array( 'db' => '`a`.`pin_status`', 'dt' => 8,'field' => 'pin_status','formatter' => function( $d, $row ){
          if($row['pin_status'] == 1){
            return 'Not-Used';
          }else{return 'Used';}
        }),
        array( 'db' => '`usx`.`first_name`', 'dt' => 9,'field' => 'first_name'),
        array( 'db' => '`usx`.`last_name`', 'dt' => 10,'field' => 'last_name'),
      );
      $joinQuery = "FROM `Pins` AS `a` INNER JOIN `Packages` AS `p` ON (`a`.`pin_request_package_id` = `p`.`package_id`) INNER JOIN `User` as `us` ON (`a`.`pin_requested_user_id` = `us`.`user_id`) INNER JOIN `Package_products` as `pp` ON (`a`.`pin_request_package_product_id` = `pp`.`package_product_id`) INNER JOIN `User` as `usx` ON (`a`.`pin_transfer_user_id` = `usx`.`user_id`)  ";

      if($_SESSION['user_id'] != 2){
        $con = ' AND us.user_id = '.$_SESSION['user_id'];
        $where = "1=1  $con";
      }else{
       $where = "  `a`.`pin_transfer_date` BETWEEN '".str_replace('/','-',$dt1)."' AND '".str_replace('/','-',$dt2)."'";
      }
      
      

      //echo $joinQuery.' WHERE '.$where;

      $result=SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery,$where);
      $start=$_REQUEST['start']+1;
      $idx=0;
      foreach($result['data'] as &$res){
          $res[0]=(string)$start;
          $start++;
          $idx++;
      }
      echo json_encode($result);
    }

    public function datatable_ajax_unused_pins_for_transfer(){
      $ss='';$ee='';$dd='';
      $CI = &get_instance();
      $CI->load->database();
      $sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
      $table = 'Pins';
      $primaryKey = 'pin_id';
      $columns = array(
          array( 'db' => '`a`.`pin_no`', 'dt' => 0,'field' => 'pin_no'),
          array( 'db' => '`a`.`pin_no`', 'dt' => 1,'field' => 'pin_no'),
          array( 'db' => '`a`.`pin_request_for`', 'dt' => 2,'field' => 'pin_request_for','formatter' => function( $d, $row ){
                if($row['pin_request_for'] == 0){
                  return "Registration";
                }else{return "Repurchase";}
              }),

          array( 'db' => '`p`.`package_name`', 'dt' => 3,'field' => 'package_name'),
          array( 'db' => '`pp`.`package_product_name`', 'dt' => 4,'field' => 'package_product_name'),
          
          array( 'db' => '`a`.`pin_id`',
             'dt' => 5,
             'field' => 'pin_id',
             'formatter' => function( $d, $row ) {
                $ss = '<input type="checkbox" value="'.$d.'" onclick="func('.$d.')">';
                return $ss;
              }
          )
      );
      $joinQuery = "FROM `Pins` AS `a` INNER JOIN `Packages` AS `p` ON (`a`.`pin_request_package_id` = `p`.`package_id`) INNER JOIN `Package_products` as `pp` ON (`a`.`pin_request_package_product_id` = `pp`.`package_product_id`)";

      if($_SESSION['role_id'] == RT::$deafault_admin_role_id || $_SESSION['role_id'] == RT::$default_stock_pointer_id){
        $where = "`a`.pin_requested_user_id = ".$_SESSION['user_id']." AND `a`.pin_transfer_user_id = 0 AND  `a`.pin_status = 1 AND a.pin_payment_type=0";
      }else{
        $where = " `a`.pin_transfer_user_id =  ".$_SESSION['user_id']."  AND  `a`.pin_status = 1 AND a.pin_payment_type=0";
      }

     // $where = "`a`.pin_requested_user_id = ".$_SESSION['user_id']." AND `a`.pin_transfer_user_id = 0 AND  `a`.pin_status = 1";
       
      $result=SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery,$where);
      $start=$_REQUEST['start']+1;
      $idx=0;
      foreach($result['data'] as &$res){
          $res[0]=(string)$start;
          $start++;
          $idx++;
      }
      echo json_encode($result);
    }

    public function pin_store_view(){
      $utill_obj = new Common_utill();
      if($this->input->post('pwd') != null){
        $pwd = $this->input->post('pwd');
        $uobj = $utill_obj->get_single_row_using_primary_key('User',$this->session->userdata('user_id'));
        if($uobj != null){
          $this->session->set_userdata('pinstore_logged_in',1);
          if($uobj->getPinStorePassword() == $pwd){
            $this->pin_store_viewx();
          }else{
             $this->session->userdata('pinstore_logged_in',0);
            $this->session->set_flashdata('error', '<p>Password Wrong</p>');
            $this->load->view('header');
            $this->load->view('pin/pin_store/Pin_store_pwd');
            $this->load->view('footer');  
          }
        }else{
           $this->session->userdata('pinstore_logged_in',0);
          $this->session->set_flashdata('error', '<p>User Not Exists</p>');
          $this->load->view('header');
          $this->load->view('pin/pin_store/Pin_store_pwd');
          $this->load->view('footer');  
        }
      }else{
        $this->session->userdata('pinstore_logged_in',0);
        $this->load->view('header');
        $this->load->view('pin/pin_store/Pin_store_pwd');
        $this->load->view('footer');  
      }
    }

    public function pin_store_viewx(){
      $this->load->view('header');
      $utill_obj = new Common_utill;
      $uobj = $utill_obj->get_single_row_using_primary_key('User',$this->session->userdata('user_id'));
      if($uobj->getUserRoleId()->getUserRoleId() == 7){
        $this->load->view('pin/pin_store/Pin_store_admin');
      }else{
        $data['obj'] = $this->get_pin_home();
        $this->load->view('pin/pin_store/Pin_store_main',$data);
      }
      $this->load->view('footer');  
    }
    public function get_pin_home(){
      $id=0;
      if($this->input->post('id') != null){
        $id = $this->input->post('id');
      }else{
        $id = 0;
      }
      $em=$this->doctrine->em;
      $utill_obj = new Common_utill();

      $pck_ar = $utill_obj->get_all_active_packages_promary_key();

      $reg = array();$rep = array();

      if(count($pck_ar) > 0){
        for($i=0;$i<count($pck_ar);$i++){

          if($id > 0){
            $qry = $em->createQuery("SELECT pck.package_id,pck.package_name,pi.pin_request_for,pi.pin_status FROM Entity\Pins as pi INNER JOIN Entity\Packages as pck WITH (pi.pin_request_package_id = pck.package_id) WHERE (pi.pin_requested_user_id =".$id." OR pi.pin_transfer_user_id = ".$id.")  AND  pi.pin_request_package_id = ".$pck_ar[$i]);
          }else{
            $qry = $em->createQuery("SELECT pck.package_id,pck.package_name,pi.pin_request_for,pi.pin_status FROM Entity\Pins as pi INNER JOIN Entity\Packages as pck WITH (pi.pin_request_package_id = pck.package_id) WHERE (pi.pin_requested_user_id =".$_SESSION['user_id']." OR pi.pin_transfer_user_id = ".$_SESSION['user_id'].")  AND  pi.pin_request_package_id = ".$pck_ar[$i]);
          }

          $s_pin_res = $qry->getResult();
          
          $lcnt = 0;$rcnt = 0;
          $lcnt_rep = 0;$rcnt_rep = 0;
          foreach ($s_pin_res as $rs) {
            if($rs['pin_request_for'] == 0){
              if($rs['pin_status'] == 0){
                $lcnt++;
              }else{
                $rcnt++;
              }
            }else{
              if($rs['pin_status'] == 0){
                $lcnt_rep++;
              }else{
                $rcnt_rep++;
              }
            }
          }

          if($lcnt > 0 || $rcnt > 0){
            $aa = array(
              'package_name' => $s_pin_res[0]['package_name'],
              'used' => $lcnt,
              'un_used' => $rcnt,
              'for' => 'REGISTRATION',
              'package_id' => $s_pin_res[0]['package_id'],
              'id' => $id,
            );
            array_push($reg, $aa);
          }

          if($lcnt_rep > 0 || $rcnt_rep > 0){
            $bb = array(
              'package_name' => $s_pin_res[0]['package_name'],
              'used' => $lcnt_rep,
              'un_used' => $rcnt_rep,
              'for' => 'REPURCHASE',
              'package_id' => $s_pin_res[0]['package_id'],
              'id' => $id,
            );
            array_push($rep, $bb);
          }

        }

        // echo"<pre>";print_r($reg);exit();

        $obj = array('reg' => $reg, 'rep' => $rep);
        if($id > 0){
          echo json_encode($obj);
        }else{
          return $obj;
        }
      }
    }

    public function get_pin_whole_details(){

      //registration -   TYPE = 0;  //used  status = 0  //unused  status = 1
      //repurchase   -  TYPE = 1;  //used  status = 0  //unused  status = 1

      if($this->input->post('iid') != null){
        $id = $this->input->post('iid');
        $pack_id = $this->input->post('pack_id');
        $type = $this->input->post('type');
        $status = $this->input->post('status');
        $data['obj'] = array('id'=>$id,'pack_id'=>$pack_id,'type'=>$type,'status'=>$status);
        $this->load->view('header');
        $this->load->view('pin/pin_store/Particular_pin_details_with_user',$data);
        $this->load->view('footer');

      }
    }

    public function datatable_particular_pins_list(){
      $id = 0;
      if($_GET['id'] != null){
        $id = $_GET['id'];
      }
      $pack_id = $_GET['pack_id'];
      $status = $_GET['status'];
      $type = $_GET['type'];

      $CI = &get_instance();
      $CI->load->database();
      $sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
      $table = 'Pins';
      $primaryKey = 'pin_id';

      $columns = array(
        array( 'db' => '`p`.`pin_no`', 'dt' => 0,'field' => 'pin_no'),
        array( 'db' => '`p`.`pin_no`', 'dt' => 1,'field' => 'pin_no'),
        array( 'db' => '`p`.`created_at`', 'dt' => 2,'field' => 'created_at'),
        array( 'db' => '`pc`.`package_name`', 'dt' => 3,'field' => 'package_name'),
        array( 'db' => '`pack`.`package_product_name`', 'dt' => 4,'field' => 'package_product_name'),
        array( 'db' => '`pack`.`package_product_display_price`', 'dt' => 5,'field' => 'package_product_display_price'),
       
      );
      $joinQuery = "FROM Pins As p INNER JOIN Package_products as pack ON (pack.package_product_id = p.pin_request_package_product_id) INNER JOIN Packages as pc ON (p.pin_request_package_id = pc.package_id)";
      $where = '`p`.`pin_requested_user_id` = '.$id.' AND pc.package_id ='.$pack_id.' AND p.pin_request_for='.$type.' AND p.pin_status = '.$status;
      $result=SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery,$where);
      $start=$_REQUEST['start']+1;
      $idx=0;
      foreach($result['data'] as &$res){
          $res[0]=(string)$start;
          $start++;
          $idx++;
      }
      echo json_encode($result);
    }

  }
?>