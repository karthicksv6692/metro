<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\Query\ResultSetMapping;
require_once 'ssp.customized.class.php';
class Pin_crud extends CI_Controller {

  public function __construct(){
    parent::__construct(); 
    is_user_logged_in();
  }
  public function index(){
    is_enter();
    $current_data = array(  'cur_data' => "");
    $this->session->set_userdata($current_data);

    $utill_obj = new Common_utill();
    //$data['add'] = $utill_obj->has_access('pincode','add');

    $this->view();
  }
  public function view(){
    is_enter();
    $this->load->view('header');
    $this->load->view('pin/pin/View');
    $this->load->view('footer');  
  }
  public function datatable_ajax(){
    $ss='';$ee='';$dd='';

    $CI = &get_instance();
    $CI->load->database();
    $sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);

    $table = 'Pins';
    $primaryKey = 'pin_id';
    $columns = array(
        array( 'db' => '`a`.`pin_no`', 'dt' => 0,'field' => 'pin_no'),
        array( 'db' => '`a`.`pin_request_for`', 'dt' => 1,'field' => 'pin_request_for','formatter' => function( $d, $row ){
              if($row['pin_request_for'] == 0){
                return "Registration";
              }else{return "Repurchase";}
            }),

       
        array( 'db' => '`q`.`package_name`', 'dt' => 2,'field' => 'package_name'),
        array( 'db' => '`pp`.`package_product_name`', 'dt' => 3,'field' => 'package_product_name'),
         array( 'db' => 'a.pin_status',  'dt' => 4 ,'field' => 'pin_status','formatter' => function( $d, $row ){
              if($row['pin_status'] == 0){
                return "Used";
              }else{return "Not-Used";}
            }),
        
        array( 'db' => '`a`.`pin_id`',
             'dt' => 5,
             'field' => 'pin_id',
             'formatter' => function( $d, $row ) {

              $utill_obj = new Common_utill();
          // $edit = $utill_obj->has_access('pincode','edit');
          // $delete = $utill_obj->has_access('pincode','delete');
          // $s_view = $utill_obj->has_access('pincode','s_view');
          $edit = "asd";
          $delete = "asd";
          $s_view = "asd";

          if($s_view != null){
                $ss = '<form method="POST" class="icon_edit_form" action="'.base_url().'index.php/pin/Pin_crud/S_view">
                            <input type="hidden" name="pin_id" value="'.$d.'">
                            <i class="fa fa-search-plus float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
              }else{$ss = '';}

              if($edit != null){
                $ee = '';
              }else{$ee='';}

              if($delete != null){
                $dd = '<i class="fa fa-trash-o float_left btn_delete but_del" aria-hidden="true" data-value="'.$d.'" ></i>';
              }else{$dd='';}

              return $ss.$ee.$dd;
            }
          )
        
    );
    $joinQuery = "FROM `Pins` AS `a` INNER JOIN `User` AS `p` ON (`a`.`pin_requested_user_id` = `p`.`user_id`) INNER JOIN `Packages` AS `q` ON (`a`.`pin_request_package_id` = `q`.`package_id`) INNER JOIN `Package_products` as `pp` ON (`a`.`pin_request_package_product_id` = `pp`.`package_product_id`)";
     
    echo json_encode(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery));
  }
  public function un_used(){
    $main_folder_name='pin';$sub_folder_name='pin';$file_name='Un_used';
    $this->view_call($main_folder_name,$sub_folder_name,$file_name);
  }
  public function un_used_ajax(){
        $ss='';$ee='';$dd='';
  
        $CI = &get_instance();
        $CI->load->database();
        $sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
  
        $table = 'Pins';
        $primaryKey = 'pin_id';
        $columns = array(
            array( 'db' => '`a`.`pin_no`', 'dt' => 0,'field' => 'pin_no'),
            
  
            array( 'db' => '`p`.`package_name`', 'dt' => 1,'field' => 'package_name'),
            array( 'db' => '`pp`.`package_product_name`', 'dt' => 2,'field' => 'package_product_name'),

            array( 'db' => '`a`.`pin_request_for`', 'dt' => 3,'field' => 'pin_request_for','formatter' => function( $d, $row ){
                  if($row['pin_request_for'] == 0){
                    return "Registration";
                  }else{return "Repurchase";}
                }),
  
            array( 'db' => '`a`.`pin_id`',
                 'dt' => 4,
                 'field' => 'pin_id',
                 'formatter' => function( $d, $row ) {
  
                  $utill_obj = new Common_utill();
             
              $edit = "asd";
              $delete = "asd";
              $s_view = "asd";
  
              if($s_view != null){
                    $ss = '<form method="POST" class="icon_edit_form" action="'.base_url().'index.php/pin/Pin_crud/S_view">
                                <input type="hidden" name="pin_id" value="'.$d.'">
                                <i class="fa fa-search-plus float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                            </form>';
                  }else{$ss = '';}
  
                  if($edit != null){
                    $ee = '';
                  }else{$ee='';}
  
                  if($delete != null){
                    $dd = '<i class="fa fa-trash-o float_left btn_delete but_del" aria-hidden="true" data-value="'.$d.'" ></i>';
                  }else{$dd='';}
  
                  return $ss.$ee.$dd;
                }
              )
        );
        $joinQuery = "FROM `Pins` AS `a` INNER JOIN `Packages` AS `p` ON (`a`.`pin_request_package_id` = `p`.`package_id`) INNER JOIN `Package_products` as `pp` ON (`a`.`pin_request_package_product_id` = `pp`.`package_product_id`)";
  
        $where = "pin_status = 1";
         
        echo json_encode(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery,$where));
    }
  public function un_used_ajax_old(){
    $ss='';$ee='';$dd='';

    $CI = &get_instance();
    $CI->load->database();
    $sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
    $table = 'Pins';
    $primaryKey = 'pin_id';
    $columns = array(
        array( 'db' => 'pin_no', 'dt' => 0 ),
        array( 'db' => 'pin_request_for',  'dt' => 1,'formatter' => function( $d, $row ){
              if($row['pin_request_for'] == 0){
                return "Registration";
              }else{return "Repurchase";}
            }),
        array( 'db' => 'pin_validity_time',  'dt' => 2 ),
        array(
              'db'        => 'pin_id',
              'dt'        => 3,
              'formatter' => function( $d, $row ) {
                $utill_obj = new Common_utill();
            $edit = "asd";
            $delete = "asd";
            $s_view = "asd";
                if($s_view != null){
                  $ss = '<form method="POST" class="icon_edit_form" action="'.base_url().'index.php/pin/Pin_crud/S_view">
                            <input type="hidden" name="pin_id" value="'.$d.'">
                            <i class="fa fa-search-plus float_left dark_blue" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
                }else{$ss = '';}

                if($edit != null){
                  $ee = '';
                }else{$ee='';}

                if($delete != null){
                  $dd = '<i class="fa fa-trash-o float_left btn_delete but_del dark_blue" aria-hidden="true" data-value="'.$d.'" ></i>';
                }else{$dd='';}
                return $ss.$ee.$dd;
            }
        )
    );
    $where = "pin_status = 1";
    echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns,null,$where));
  }
  public function used(){
    $main_folder_name='pin';$sub_folder_name='pin';$file_name='Used';
    $this->view_call($main_folder_name,$sub_folder_name,$file_name);
  }
  public function used_ajax(){
    $ss='';$ee='';$dd='';

    $CI = &get_instance();
    $CI->load->database();
    $sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);

    $table = 'Pins';
    $primaryKey = 'pin_id';
    $columns = array(
        array( 'db' => '`a`.`pin_no`', 'dt' => 0,'field' => 'pin_no'),
        array( 'db' => 'q.package_name',  'dt' => 1 ,'field' => 'package_name'),
        array( 'db' => 'pp.package_product_name',  'dt' => 2 ,'field' => 'package_product_name'),

        array( 'db' => '`a`.`pin_request_for`', 'dt' => 3,'field' => 'pin_request_for','formatter' => function( $d, $row ){
              if($row['pin_request_for'] == 0){
                return "Registration";
              }else{return "Repurchase";}
            }),

        array( 'db' => 'p.user_name',  'dt' => 4 ,'field' => 'user_name'),
        array( 'db' => '`a`.`pin_used_time`', 'dt' => 5,'field' => 'pin_used_time'),
        
        
        array( 'db' => '`a`.`pin_id`',
             'dt' => 6,
             'field' => 'pin_id',
             'formatter' => function( $d, $row ) {

              $utill_obj = new Common_utill();
          
          $edit = "asd";
          $delete = "asd";
          $s_view = "asd";

          if($s_view != null){
                $ss = '<form method="POST" class="icon_edit_form" action="'.base_url().'index.php/pin/Pin_crud/S_view">
                            <input type="hidden" name="pin_id" value="'.$d.'">
                            <i class="fa fa-search-plus float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
              }else{$ss = '';}

              if($edit != null){
                $ee = '';
              }else{$ee='';}

              if($delete != null){
                $dd = '';
              }else{$dd='';}

              return $ss.$ee.$dd;
            }
          )
    );
    $joinQuery = "FROM `Pins` AS `a` INNER JOIN `User` AS `p` ON (`a`.`pin_used_user_id` = `p`.`user_id`) iNNER JOIN `Packages` AS `q` ON (`a`.`pin_request_package_id` = `q`.`package_id`) INNER JOIN `Package_products` as `pp` ON (`a`.`pin_request_package_product_id` = `pp`.`package_product_id`)";

     
    echo json_encode(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery));
  }
  
  public function add(){
    is_pin_enter();
    $em = $this->doctrine->em;
    $utill_obj = new Common_utill();
    $main_folder_name='pin';$sub_folder_name='pin';$file_name='Add';
    if($this->input->post('pin_req_id') != null || $this->input->post('pin_req') ){

      //$this->form_validation->set_rules('package_id', 'PackageId','trim|required|regex_match[/^[0-9\-]+$/]',array('required'=>'Required','regex_match' => 'Select Correct Package'));
      //$this->form_validation->set_rules('pck_prd', 'PackageId','trim|required|regex_match[/^[0-9\-]+$/]',array('required'=>'Required','regex_match' => 'Select Correct PackageProduct'));
      $this->form_validation->set_rules('nop', 'Numbr','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Required'));
      $this->form_validation->set_rules('for', 'Numbr','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Required'));
      if ($this->form_validation->run() != false){

        $em->getConnection()->beginTransaction();

        $user_obj         = $utill_obj->get_reference_obj('User',$_SESSION['user_id']);
        if($this->input->post('package_id') != null){
          $package_obj      = $utill_obj->get_reference_obj('Packages',$this->input->post('package_id'));
        }else{$package_obj='';}
        if($this->input->post('pck_prd') != null){
          $package_product_obj  = $utill_obj->get_reference_obj('Package_products',$this->input->post('pck_prd'));
        }else{
          $package_product_obj='';
        }
        if($user_obj->getUserRoleId()->getUserRoleId() == RT::$default_stock_pointer_id){
          $account_obj = $utill_obj->get_single_row_using_array_value('User_account','user_id',$_SESSION['user_id']);
          $avail_amount = $account_obj->getUserAccountAvailableAmount();
          if($package_product_obj != ''){
            $pp_price = $package_product_obj->getPackageProductDisplayPrice();
          }else{$pp_price=0;}
          $tot_amt = $pp_price*$this->input->post('nop');
          if($avail_amount >= $tot_amt){
            $account_obj->setUserAccountAvailableAmount($avail_amount-$tot_amt);
            $em->persist($account_obj);
            $em->flush();
            // if($package_obj != null){
              // if($package_product_obj != null){
                $pin_request_user_id          =   $user_obj;
                $pin_request_package_id             =   $package_obj;
                $pin_request_package_product_id     =   $package_product_obj;
                $pin_req_quantity             =   $this->input->post('nop');
                $pin_request_for              =   $this->input->post('for');
                $pin_used_user_id           =   0;
                $pin_status               =   1;
                $pin_validity_time          =   new \DateTime( Date('y-m-d h:i:s', strtotime("+5 days")));
                $pin_used_time            =   new \DateTime("0000-00-00 00:00:00");
                $pin_remarks              =   "";
                $ar = array();
                for($i=1;$i<=$pin_req_quantity;){
                  $pin_no  =  $utill_obj->get_pin_number();

                  $pin_obj = $utill_obj->add_pin($pin_no,$pin_request_user_id,$pin_used_user_id,$pin_status,$pin_validity_time,$pin_remarks,$pin_used_time,$pin_request_package_id,$pin_request_package_product_id,$pin_request_for);
                  
                  if($pin_obj != null){
                    if($pin_request_for == 1){
                      $req = 'Repurchase';
                    }else{
                      $req = 'Registration';
                    }
                    $x = array(
                      'pin_no' => $pin_obj->getPinNo(),
                      'user_name' => $pin_request_user_id->getUserName(),
                      'package_name' => $pin_request_package_id->getPackageName(),
                      'price' => $pin_request_package_product_id->getPackageProductDisplayPrice(),
                      'for' => $req,
                      'created_at' => $pin_obj->getCreatedAt()->format('Y-m-d H:i:s'),
                    );
                    array_push($ar, $x);
                    if($i == $pin_req_quantity){
                     
                      $html = '<!DOCTYPE html> <html> <head> 
                                <style> 
                                  table {
                                      font-family: arial, sans-serif;
                                      border-collapse: collapse;
                                      width: 100%;
                                  }
                                  td, th {
                                      border: 1px solid #dddddd;
                                      text-align: left;
                                      padding: 8px;
                                  }

                                  tr:nth-child(even) {
                                      background-color: #dddddd;
                                  }
                                  </style>
                                  </head>
                                  <body>
                                    <table>
                                      <tr>
                                        <th>User Name</th>
                                        <th>Pin No</th>
                                        <th>Package Name</th>
                                        <th>Package Price</th>
                                        <th>For</th>
                                        <th>Date</th>
                                      </tr>';
                                      foreach ($ar as  $as) {
                                        $html .= '<tr><td>'.$as['user_name'].'</td><td>'.$as['pin_no'].'</td><td>'.$as['package_name'].'</td><td>'.$as['price'].'</td><td>'.$as['for'].'</td><td>'.$as['created_at'].'</td></tr>';
                                      }
                                      $html .= '</table> </body> </html>';


                      $to = "svkarthicksv@gmail.com";
                      $subject = "Pin Generate Report";
                      $txt = $html;
                      $headers  = 'MIME-Version: 1.0' . "\r\n";
                      $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                      $headers .= "From: webmaster@example.com";

                     //mail($to,$subject,$txt,$headers);

                      $em->getConnection()->commit();
                      $this->session->set_flashdata('success', '<p>Pin Created Sussfully</p>');
                      redirect('index.php/login/dashboard','refresh');
                    }
                    $i++;
                  }else{
                    $em->getConnection()->rollBack();
                    $this->session->set_flashdata('error', '<p>Pin Request Failed</p>');
                    $this->view_call($main_folder_name,$sub_folder_name,$file_name);
                  }
                }
              // }else{
                // $this->session->set_flashdata('error', '<p>PackageProduct Not Exists</p>');
                // $this->view_call($main_folder_name,$sub_folder_name,$file_name);
              // }
            // }else{
              // $this->session->set_flashdata('error', '<p>Package Not Exists</p>');
              // $this->view_call($main_folder_name,$sub_folder_name,$file_name);
            // }
          }else{
            $this->session->set_flashdata('error', '<p>Low Balance</p>');
            redirect('index.php/login/dashboard','refresh');
          }
        }elseif($user_obj->getUserRoleId()->getUserRoleId() == RT::$deafault_admin_role_id){
          // if($package_obj != null){
              // if($package_product_obj != null){
                $pin_request_user_id          =   $user_obj;
                $pin_request_package_id             =   $package_obj;
                $pin_request_package_product_id     =   $package_product_obj;
                $pin_req_quantity             =   $this->input->post('nop');
                $pin_request_for              =   $this->input->post('for');
                $pin_used_user_id           =   0;
                $pin_status               =   1;
                $pin_validity_time          =   new \DateTime( Date('y-m-d h:i:s', strtotime("+5 days")));
                $pin_used_time            =   new \DateTime("0000-00-00 00:00:00");
                $pin_remarks              =   "";

                for($i=1;$i<=$pin_req_quantity;){
                  $pin_no  =  $utill_obj->get_pin_number();

                  $pin_obj = $utill_obj->add_pin($pin_no,$pin_request_user_id,$pin_used_user_id,$pin_status,$pin_validity_time,$pin_remarks,$pin_used_time,$pin_request_package_id,$pin_request_package_product_id,$pin_request_for);
                  
                  if($pin_obj != null){
                    if($i == $pin_req_quantity){
                      $em->getConnection()->commit();
                      $this->session->set_flashdata('success', '<p>Pin Created Sussfully</p>');
                      redirect('index.php/pin/pin_crud','refresh');
                    }
                    $i++;
                  }else{
                    $em->getConnection()->rollBack();
                    $this->session->set_flashdata('error', '<p>Pin Request Failed</p>');
                    $this->view_call($main_folder_name,$sub_folder_name,$file_name);
                  }
                }
              // }else{
                // $this->session->set_flashdata('error', '<p>PackageProduct Not Exists</p>');
                // $this->view_call($main_folder_name,$sub_folder_name,$file_name);
              // }
            // }else{
              // $this->session->set_flashdata('error', '<p>Package Not Exists</p>');
              // $this->view_call($main_folder_name,$sub_folder_name,$file_name);
            // }
          }else{
            $this->session->set_flashdata('error', '<p>Invalid Role</p>');
            redirect('index.php/login/dashboard','refresh');
          }
        }

    }else{
      $user_obj         = $utill_obj->get_reference_obj('User',$_SESSION['user_id']);
      if($_SESSION['role_id'] != RT::$deafault_admin_role_id){
       $account_obj = $utill_obj->get_single_row_using_array_value('User_account','user_id',$_SESSION['user_id']);
        $data['avail_amount'] = $account_obj->getUserAccountAvailableAmount();
        $this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
      }else{
        
        $data['avail_amount'] = 0;
        $this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
      }
    }
  }
  
  public function s_view(){
    is_enter();
    $utill_obj = new Common_utill();
    $data['search'] = $utill_obj->search_field('Pins', 'getPinId', 'getPinNo', 'index.php/pin/pin_crud', 'S_view', 'pin_id');
    if($this->input->post('pin_id') != null){
      $this->form_validation->set_rules('pin_id', 'Bank id','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
      if ($this->form_validation->run() != false){
        $pin_id = $this->input->post('pin_id');
        $em = $this->doctrine->em;
        $bank_obj  = $em->find('Entity\Pins',$pin_id); 
        if($bank_obj != null){
          $data['pin_obj'] = $bank_obj;
          $this->load->view('header');
          $this->load->view('pin/pin/S_view',$data);
          $this->load->view('footer');
        }else{
          $this->session->set_flashdata('error', '<p>Bank Not Exists</p>');
          redirect('index.php/account_details/bank_crud','refresh');
        }
      }else{
        redirect('index.php/account_details/bank_crud','refresh');
      }
    }else{
      $this->load->view('header');
      $this->load->view('pin/pin//S_view',$data);
      $this->load->view('footer');
    }
  }
  
  
  public function ajax_delete(){
    is_enter();
    $em = $this->doctrine->em;
    if($this->input->post('delete_pin_id') != null){
      $this->form_validation->set_rules('delete_pin_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
      if ($this->form_validation->run() != false){
        $pin_id = $this->input->post('delete_pin_id');
        $find_pin = $em->getRepository('Entity\Pins')->findOneBy(array('pin_id'=>$pin_id,'pin_status'=>1)); 
        $data['pin_obj'] = $find_pin;
        if($find_pin != null){
          $em->remove($find_pin);
          $em->flush();
          echo 1;
        }else{ echo -1; }
      }else{ echo 0; }
    }else{ echo 0; }
  }
  public function admin_unused_pin_view(){
    $this->load->view('header');
    $this->load->view('pin/Admin_unused_pin_view');
    $this->load->view('footer');
  }
  public function admin_used_pin_view(){
    $this->load->view('header');
    $this->load->view('pin/Admin_used_pin_view');
    $this->load->view('footer');
  }
  public function datatable_admin_unused_pin_view(){
    $dt1 = '';
    $dt2 = '';
    $u_con = '';
    $con = '';
    if($_GET['pack_type'] == 0){
      $con = '';
    }elseif($_GET['pack_type'] == 1){
      $con =  ' AND `p`.`pin_request_for` = 0';
    }else{
      $con =  ' AND `p`.`pin_request_for` = 1';
    }
    if($_GET['dt1'] != null){
      if($_GET['pos1'] == 0){
        $dt1 = $_GET['dt1'].' 02:00:00';
        if($_GET['dt2'] == null){
          $dt2 = $_GET['dt1'].' 13:59:59';
        }
      }elseif($_GET['pos1'] == 1){
        $dt1 = $_GET['dt1'].' 14:00:00';
        if($_GET['dt2'] == null){
          $d = $_GET['dt1'].' 01:59:59';
          $dt2 = date("Y-m-d H:i:s", strtotime($d."+1 days"));
        }
      }else{
          $dt1 = $_GET['dt1'].' 02:00:00';
          $d = $_GET['dt1'].' 01:59:59';
          $dt2 = date("Y-m-d H:i:s", strtotime($d."+1 days"));
      }
    }
    if($_GET['dt1'] != null){
      if($_GET['dt2'] != null){
        if($_GET['pos2'] == 0){
          $dt2 = $_GET['dt2'].' 02:00:00';
        }else{
          $dt2 = $_GET['dt2'].' 13:59:59';
        }
      }
    }
    if($_GET['user_id'] != null || $_GET['user_name'] != null){
      if($_GET['user_id'] != null && $_GET['user_name'] == null){
        if($dt1 != null){
          $u_con = " AND `u`.`user_id` = ".$_GET['user_id'];
        }else{
          $u_con = "  `u`.`user_id` = ".$_GET['user_id'];
        }
      }elseif($_GET['user_id'] == null && $_GET['user_name'] != null){
        if($dt1 != null){
          $u_con = " AND `u`.`user_id` = ".$_GET['user_name'];
        }else{
          $u_con = "  `u`.`user_id` = ".$_GET['user_name'];
        }
      }else{
        if($dt1 != null){
          $u_con = " AND `u`.`user_id` = ".$_GET['user_id'];
        }else{
          $u_con = "  `u`.`user_id` = ".$_GET['user_id'];
        }
      }
    }

    $CI = &get_instance();
    $CI->load->database();
    $sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
    $tot_tds = 0;
    $table = 'Pins';
    $primaryKey = 'pin_id';
    $columns = array(
      array( 'db' => '`p`.`pin_id`', 'dt' => 0,'field' => 'pin_id'),
      array( 'db' => '`p`.`created_at`', 'dt' => 1,'field' => 'created_at'),
      array( 'db' => '`p`.`pin_no`', 'dt' => 2,'field' => 'pin_no'),
      array( 'db' => '`pa`.`package_name`', 'dt' => 3,'field' => 'package_name'),
      array( 'db' => '`pp`.`package_product_name`', 'dt' => 4,'field' => 'package_product_name'),
      array( 'db' => '`u`.`user_id`', 'dt' => 5,'field' => 'user_id'),
      array( 'db' => '`u`.`user_name`', 'dt' => 6,'field' => 'user_name'),
      array( 'db' => '`p`.`pin_request_for`', 'dt' => 7,'field' => 'pin_request_for','formatter' => function( $d, $row ){
        if($row['pin_request_for'] == 0){
          return "Registration";
        }else{return "Repurchase";}
      }),
      array( 'db' => '`pp`.`package_product_display_price`', 'dt' => 8,'field' => 'package_product_display_price'),
    );
    $joinQuery = "FROM `Pins` AS `p` INNER JOIN `Packages` AS `pa` ON (`p`.`pin_request_package_id` = `pa`.`package_id`) INNER JOIN `Package_products` as `pp` ON (`p`.`pin_request_package_product_id` = `pp`.`package_product_id`) INNER JOIN `User` as `u` ON (u.user_id = p.pin_requested_user_id)";
    if($dt1 != null){
      $where = "p.created_at BETWEEN '".$dt1."' AND '".$dt2."' $con $u_con AND p.pin_status = 0";
    }else{
      $where = " $u_con AND p.pin_status = 0";
    }
    $result = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery,$where);
    $start=$_GET['start']+1;
    $idx=0;
    foreach($result['data'] as &$res){
      $res[0]=(string)$start;
      $start++;
      $idx++;
    }
    echo json_encode($result);
  }
  public function get_admin_unused_pin_total(){
    $em = $this->doctrine->em;
    $dt1 = '';
    $dt2 = '';
    $u_con = '';
    $con = '';
    if($this->input->post('pack_type') == 0){
      $con = '';
    }elseif($this->input->post('pack_type') == 1){
      $con =  ' AND `p`.`pin_request_for` = 0';
    }else{
      $con =  ' AND `p`.`pin_request_for` = 1';
    }
    if($this->input->post('dt1') != null){
      if($this->input->post('pos1') == 0){
        $dt1 = $this->input->post('dt1').' 02:00:00';
        if($this->input->post('dt2') == null){
          $dt2 = $this->input->post('dt1').' 13:59:59';
        }
      }elseif($this->input->post('pos1') == 1){
        $dt1 = $this->input->post('dt1').' 14:00:00';
        if($this->input->post('dt2') == null){
          $d = $this->input->post('dt1').' 01:59:59';
          $dt2 = date("Y-m-d H:i:s", strtotime($d."+1 days"));
        }
      }else{
          $dt1 = $this->input->post('dt1').' 02:00:00';
          $d = $this->input->post('dt1').' 01:59:59';
          $dt2 = date("Y-m-d H:i:s", strtotime($d."+1 days"));
      }
    }
    if($this->input->post('dt1') != null){
      if($this->input->post('dt2') != null){
        if($this->input->post('pos2') == 0){
          $dt2 = $this->input->post('dt2').' 02:00:00';
        }else{
          $dt2 = $this->input->post('dt2').' 13:59:59';
        }
      }
    }
    if($this->input->post('user_id') != null || $this->input->post('user_name') != null){
      if($this->input->post('user_id') != null && $this->input->post('user_name') == null){
        if($dt1 != null){
          $u_con = " AND `u`.`user_id` = ".$this->input->post('user_id');
        }else{
          $u_con = "  `u`.`user_id` = ".$this->input->post('user_id');
        }
      }elseif($this->input->post('user_id') == null && $this->input->post('user_name') != null){
        if($dt1 != null){
          $u_con = " AND `u`.`user_id` = ".$this->input->post('user_name');
        }else{
          $u_con = "  `u`.`user_id` = ".$this->input->post('user_name');
        }
      }else{
        if($dt1 != null){
          $u_con = " AND `u`.`user_id` = ".$this->input->post('user_id');
        }else{
          $u_con = "  `u`.`user_id` = ".$this->input->post('user_id');
        }
      }
    }

    $rep_qry='';
    if($dt1 != null){
      $rep_qry = $em->createQuery("SELECT pp.package_product_display_price FROM Entity\Pins AS p INNER JOIN Entity\Packages AS pa WITH (p.pin_request_package_id = pa.package_id) INNER JOIN Entity\Package_products as pp WITH (p.pin_request_package_product_id = pp.package_product_id) INNER JOIN Entity\User as u WITH (u.user_id = p.pin_requested_user_id) WHERE p.created_at BETWEEN '".$dt1."' AND '".$dt2."' $con $u_con AND p.pin_status = 1");
    }else{
      $rep_qry = $em->createQuery("SELECT pp.package_product_display_price FROM Entity\Pins AS p INNER JOIN Entity\Packages AS p WITH (p.pin_request_package_id = pa.package_id) INNER JOIN Entity\Package_products as pp WITH (p.pin_request_package_product_id = pp.package_product_id) INNER JOIN Entity\User as u WITH (u.user_id = p.pin_requested_user_id) WHERE   p.pin_status = 1 $con $u_con");
    }
    $res = $rep_qry->getResult();
    $arr = array();
    if(count($res)>0){
      foreach ($res as $value) {
        array_push($arr, $value['package_product_display_price']);
      }
      echo array_sum($arr);
    }else{echo 0;}

    // echo($res[0][1]);
  }
  public function alphabets_and_number($str){
     if (preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str)) {
       return true;
     }
     return false;
  }
  public function find_alphabets($str){
     if (preg_match('#[a-zA-Z]#', $str)) {
       return true;
     }
     return false;
  }
  public function find_numbers($str){
     if (preg_match('#[0-9]#', $str)) {
       return true;
     }
     return false;
  }
  public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
    $this->load->view('header');
    $this->load->view($main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
    $this->load->view('footer');
  }

  public function pin_abstract(){
    is_enter();
    $this->load->view('header');
    $this->load->view('pin/Pin_payment');
    $this->load->view('footer');
  }

  public function datatable_pin_abstract(){
    $dt1 = '00:00:00 02:00:00';
    $dt2 = '00:00:00 02:00:00';
    
    if($_GET['pos'] == 0){
      $dt1 = $_GET['dt'].' 02:00:00';
      $dt2 = $_GET['dt'].' 13:59:59';
    }elseif($_GET['pos'] == 1){
      $nxt = date('Y-m-d', strtotime('+1 day', strtotime($_GET['dt'])));
      $dt1 = $_GET['dt'].' 14:00:00';
      $dt2 = $nxt.' 01:59:59';
    }

    $CI = &get_instance();
    $CI->load->database();
    $sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
    $table = 'Payout_result';
    $primaryKey = 'payout_result_id';

    $columns = array(
      array( 'db' => '`pin`.`pin_no`', 'dt' => 0,'field' => 'pin_no'),
      array( 'db' => '`pin`.`pin_no`', 'dt' => 1,'field' => 'pin_no'),
      array( 'db' => '`u`.`user_name`', 'dt' => 2,'field' => 'user_name'),
      array( 'db' => '`p`.`package_name`', 'dt' => 3,'field' => 'package_name'),
      array( 'db' => '`pack`.`package_product_name`', 'dt' => 4,'field' => 'package_product_name'),
      array( 'db' => '`up`.`created_at`', 'dt' => 5,'field' => 'created_at'),
     
    );
    $joinQuery = "FROM User_package_purchase AS up INNER JOIN Pins as pin ON (up.user_purchase_pin_id = pin.pin_id) INNER JOIN Package_products as pack ON (pack.package_product_id = pin.pin_request_package_product_id) INNER JOIN Packages as p ON (pin.pin_request_package_id = p.package_id) INNER JOIN User as u ON (up.user_id = u.user_id)";
     $where = "up.created_at BETWEEN '".$dt1."' AND '".$dt2."'";
    //echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery,$where));
    $result=SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery,$where);
    $start=$_REQUEST['start']+1;
    $idx=0;
    foreach($result['data'] as &$res){
        $res[0]=(string)$start;
        $start++;
        $idx++;
    }
    echo json_encode($result);
  }
  public function get_total_pin_report(){
    $em=$this->doctrine->em;
    $utill_obj = new Common_utill();
    $pack_ids = $utill_obj->get_all_active_packages_primary_key_name();
    $dt1 = '';$dt2 = '';$where = '';$ar = array();$res_ar = array();

    if($this->input->post('dt1') != null){
      if($this->input->post('pos1') == 0){
        $dt1 = $this->input->post('dt1').' 02:00:00';
        if($this->input->post('dt2') == null){
          $dt2 = $this->input->post('dt1').' 13:59:59';
        }
      }elseif($this->input->post('pos1') == 1){
        $dt1 = $this->input->post('dt1').' 14:00:00';
        if($this->input->post('dt2') == null){
           $d = $this->input->post('dt1').' 01:59:59';
           $dt2 = date("Y-m-d H:i:s", strtotime($d."+1 days"));
          
          //echo($dt2);
        }
      }else{
          $dt1 = $this->input->post('dt1').' 02:00:00';
          $d = $this->input->post('dt1').' 01:59:59';
          $dt2 = date("Y-m-d H:i:s", strtotime($d."+1 days"));
      }
    }
   
    if($this->input->post('dt1') != null){
      if($this->input->post('dt2') != null){
        if($this->input->post('pos2') == 0){
          $dt2 = $this->input->post('dt2').' 02:00:00';
        }else{
          $dt2 = $this->input->post('dt2').' 13:59:59';
        }
      }
    }

    // if($this->input->post('dt1') != null){
    //   if($this->input->post('pos1') == 0){
    //     $dt1 = $this->input->post('dt1').' 02:00:00';
    //   }else{
    //     $dt1 = $this->input->post('dt1').' 14:00:00';
    //   }
    // }
    // if($this->input->post('dt2') != null){
    //   if($this->input->post('pos2') == 0){
    //     $dt2 = $this->input->post('dt2').' 02:00:00';
    //   }else{
    //     $dt2 = $this->input->post('dt2').' 14:00:00';
    //   }
    // }

    if($dt1 != '' && $dt2 != '' ){
      $where = " WHERE up.created_at BETWEEN '".$dt1."' AND '".$dt2."'";
    }elseif($dt1 != '' && $dt2 == ''){
      $where = " WHERE up.created_at >= $dt1";
    }elseif($dt1 == '' && $dt2 != ''){
      $where = " WHERE up.created_at >= $dt2";
    }else{
      $where = "";
    }

    $query = $em->createQuery("SELECT p.package_id FROM Entity\User_package_purchase AS up INNER JOIN Entity\Pins as pin WITH (up.user_purchase_pin_id = pin.pin_id) INNER JOIN Entity\Package_products as pack WITH (pack.package_product_id = pin.pin_request_package_product_id) INNER JOIN Entity\Packages as p WITH (pin.pin_request_package_id = p.package_id) INNER JOIN Entity\User as u WITH (up.user_id = u.user_id) $where" );
    $res = $query->getResult();

    if(count($res) > 0){
      foreach ($res as $r) {
        array_push($res_ar, $r['package_id']);
      }
      if(count($pack_ids) > 0){
        $counts = array_count_values($res_ar);
        foreach ($pack_ids as $pck) {
          if(in_array($pck['package_id'], $res_ar)){
            if($counts[$pck['package_id']] > 0){
              array_push($ar, array('package_id' => $pck['package_id'],'package_name' => $pck['package_name'], 'count' => $counts[$pck['package_id']],'from'=>$dt1,'to'=>$dt2));
            }
          }
        }
        echo json_encode($ar);
      }else{echo 0;}
    }else{echo 0;}

  }

  public function get_pin_details(){
    is_enter();
    if($this->input->post('package_id') != null){ 
      $data['details'] = array('package_id' => $this->input->post('package_id'),'from' =>$this->input->post('from'),'to'=>$this->input->post('to'));
      $this->load->view('header');
      $this->load->view('pin/Pin_payment_list',$data);
      $this->load->view('footer');
    }
  }

   public function datatable_pin_payment_list(){
    $dt1 = '';
    $dt2 = '';
    $where = '';
    $ar = array();
    $res_ar = array();
    if($_GET['fr'] != null){
      $dt1 = $_GET['fr'];
    }
    if($_GET['to'] != null){
      $dt2 = $_GET['to'];
    }

    if($dt1 != '' && $dt2 != '' ){
      $where = " p.package_id = ".$_GET['id']." AND up.created_at BETWEEN '".$dt1."' AND '".$dt2."'";
    }elseif($dt1 != '' && $dt2 == ''){
      $where = " p.package_id = ".$_GET['id']." AND up.created_at >= $dt1";
    }elseif($dt1 == '' && $dt2 != ''){
      $where = " p.package_id = ".$_GET['id']." AND up.created_at >= $dt2";
    }else{
      $where = "p.package_id = ".$_GET['id'];
    }

    $CI = &get_instance();
    $CI->load->database();
    $sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
    $table = 'Payout_result';
    $primaryKey = 'payout_result_id';

    $columns = array(
      array( 'db' => '`pin`.`pin_no`', 'dt' => 0,'field' => 'pin_no'),
      array( 'db' => '`pin`.`pin_no`', 'dt' => 1,'field' => 'pin_no'),
      array( 'db' => '`u`.`user_id`', 'dt' => 2,'field' => 'user_id'),
      array( 'db' => '`u`.`user_name`', 'dt' => 3,'field' => 'user_name'),
      array( 'db' => '`u`.`first_name`', 'dt' => 4,'field' => 'first_name'),

      array( 'db' => '`u`.`first_name`', 'dt' => 4,'field' => 'first_name','formatter' => function( $d, $row ){
          return $row['first_name'].' '.$row['last_name'];
      }),


      array( 'db' => '`p`.`package_name`', 'dt' => 5,'field' => 'package_name'),
      array( 'db' => '`pack`.`package_product_name`', 'dt' => 6,'field' => 'package_product_name'),
      array( 'db' => '`pack`.`package_product_display_price`', 'dt' => 7,'field' => 'package_product_display_price'),
      array( 'db' => '`up`.`created_at`', 'dt' => 8,'field' => 'created_at'),
      array( 'db' => '`u`.`last_name`', 'dt' => 9,'field' => 'last_name'),
     
    );
    $joinQuery = "FROM User_package_purchase AS up INNER JOIN Pins as pin ON (up.user_purchase_pin_id = pin.pin_id) INNER JOIN Package_products as pack ON (pack.package_product_id = pin.pin_request_package_product_id) INNER JOIN Packages as p ON (pin.pin_request_package_id = p.package_id) INNER JOIN User as u ON (up.user_id = u.user_id)";
    $result=SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery,$where);
    $start=$_REQUEST['start']+1;
    $idx=0;
    foreach($result['data'] as &$res){
        $res[0]=(string)$start;
        $start++;
        $idx++;
    }
    echo json_encode($result);

  }
  public function get_total_count_package_price(){
    is_enter();
    $em=$this->doctrine->em;
    $utill_obj = new Common_utill();
    $pack_ids = $utill_obj->get_all_active_packages_primary_key_name();
    $dt1 = '';
    $dt2 = '';
    $where = '';
    $ar = array();
    $res_ar = array();
    if($this->input->post('dt1') != null){
      $dt1 = $this->input->post('dt1');
    }
    if($this->input->post('dt2') != null){
      $dt2 = $this->input->post('dt2');
    }

    if($dt1 != '' && $dt2 != '' ){
      $where = " WHERE  p.package_id = ".$this->input->post('id')." AND up.created_at BETWEEN '".$dt1."' AND '".$dt2."'";
    }elseif($dt1 != '' && $dt2 == ''){
      $where = " WHERE  p.package_id = ".$this->input->post('id')." AND up.created_at >= $dt1";
    }elseif($dt1 == '' && $dt2 != ''){
      $where = " WHERE  p.package_id = ".$this->input->post('id')." AND up.created_at >= $dt2";
    }else{
      $where = " WHERE  p.package_id = ".$this->input->post('id');
    }

    $query = $em->createQuery("SELECT SUM(pack.package_product_display_price) FROM Entity\User_package_purchase AS up INNER JOIN Entity\Pins as pin WITH (up.user_purchase_pin_id = pin.pin_id) INNER JOIN Entity\Package_products as pack WITH (pack.package_product_id = pin.pin_request_package_product_id) INNER JOIN Entity\Packages as p WITH (pin.pin_request_package_id = p.package_id) INNER JOIN Entity\User as u WITH (up.user_id = u.user_id) $where" );
    $res = $query->getResult();

    if(count($res) > 0){
     echo $res[0][1];
    }else{echo 0;}
  }
  public function admin_transfer_pin_view(){
    $this->load->view('header');
    $this->load->view('pin/Admin_transfer_pin_view');
    $this->load->view('footer');
  }
  public function datatable_admin_transfer_pin_view(){
    $dt1 = '';
    $dt2 = '';
    $u_con = '';
    $con = '';
    if($_GET['pack_type'] == 0){
      $con = '';
    }elseif($_GET['pack_type'] == 1){
      $con =  ' AND `p`.`pin_request_for` = 0';
    }else{
      $con =  ' AND `p`.`pin_request_for` = 1';
    }
    if($_GET['pay'] == 0){
      $con .=  ' AND `p`.`pin_payment_type` = 0';
    }else{
      $con .=  ' AND `p`.`pin_payment_type` = 1';
    }

    

    if($_GET['dt1'] != null){
      if($_GET['pos1'] == 0){
        $dt1 = $_GET['dt1'].' 02:00:00';
        if($_GET['dt2'] == null){
          $dt2 = $_GET['dt1'].' 13:59:59';
        }
      }elseif($_GET['pos1'] == 1){
        $dt1 = $_GET['dt1'].' 14:00:00';
        if($_GET['dt2'] == null){
          $d = $_GET['dt1'].' 01:59:59';
          $dt2 = date("Y-m-d H:i:s", strtotime($d."+1 days"));
        }
      }else{
          $dt1 = $_GET['dt1'].' 02:00:00';
          $d = $_GET['dt1'].' 01:59:59';
          $dt2 = date("Y-m-d H:i:s", strtotime($d."+1 days"));
      }
    }
    if($_GET['dt1'] != null){
      if($_GET['dt2'] != null){
        if($_GET['pos2'] == 0){
          $dt2 = $_GET['dt2'].' 02:00:00';
        }else{
          $dt2 = $_GET['dt2'].' 13:59:59';
        }
      }
    }
    if($_GET['user_id'] != null || $_GET['user_name'] != null){
      if($_GET['user_id'] != null && $_GET['user_name'] == null){
        if($dt1 != null){
          $u_con = " AND `u`.`user_id` = ".$_GET['user_id'];
        }else{
          $u_con = "  `u`.`user_id` = ".$_GET['user_id'];
        }
      }elseif($_GET['user_id'] == null && $_GET['user_name'] != null){
        if($dt1 != null){
          $u_con = " AND `u`.`user_id` = ".$_GET['user_name'];
        }else{
          $u_con = "  `u`.`user_id` = ".$_GET['user_name'];
        }
      }else{
        if($dt1 != null){
          $u_con = " AND `u`.`user_id` = ".$_GET['user_id'];
        }else{
          $u_con = "  `u`.`user_id` = ".$_GET['user_id'];
        }
      }
    }

    $CI = &get_instance();
    $CI->load->database();
    $sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
    $tot_tds = 0;
    $table = 'Pins';
    $primaryKey = 'pin_id';
    $columns = array(
      array( 'db' => '`p`.`pin_id`', 'dt' => 0,'field' => 'pin_id'),
      array( 'db' => '`p`.`created_at`', 'dt' => 1,'field' => 'created_at'),
      array( 'db' => '`p`.`pin_no`', 'dt' => 2,'field' => 'pin_no'),
      array( 'db' => '`pa`.`package_name`', 'dt' => 3,'field' => 'package_name'),
      array( 'db' => '`pp`.`package_product_name`', 'dt' => 4,'field' => 'package_product_name'),
      array( 'db' => '`u`.`user_id`', 'dt' => 5,'field' => 'user_id'),
      array( 'db' => '`u`.`user_name`', 'dt' => 6,'field' => 'user_name'),
      array( 'db' => '`p`.`pin_request_for`', 'dt' => 7,'field' => 'pin_request_for','formatter' => function( $d, $row ){
        if($row['pin_request_for'] == 0){
          return "Registration";
        }else{
          $utill_obj = new Common_utill();
          return $utill_obj->get_pin_detail_status_repurchase($row['pin_no']);
          // return "Repurchase";
        }
      }),
      array( 'db' => '`pp`.`package_product_display_price`', 'dt' => 8,'field' => 'package_product_display_price'),
    );

    
    
    $joinQuery = "FROM `Pins` AS `p` INNER JOIN `Packages` AS `pa` ON (`p`.`pin_request_package_id` = `pa`.`package_id`) INNER JOIN `Package_products` as `pp` ON (`p`.`pin_request_package_product_id` = `pp`.`package_product_id`) INNER JOIN `User` as `u` ON (u.user_id = p.pin_transfer_user_id)";
    if($dt1 != null){

      if($_GET['status'] == 0){
        $where = " (p.created_at BETWEEN '".$dt1."' AND '".$dt2."' OR p.pin_used_time BETWEEN '".$dt1."' AND '".$dt2."') $con $u_con";
      }elseif($_GET['status'] == 1){
        $where = " p.pin_used_time BETWEEN '".$dt1."' AND '".$dt2."'  $con $u_con";
        // $st_con =  ' AND `p`.`pin_status` = 0';  AND `p`.`pin_status` = 0
      }elseif($_GET['status'] == 3){
        $where = " p.created_at BETWEEN '".$dt1."' AND '".$dt2."'  $con $u_con AND p.pin_request_for = 1 AND p.repurchase_type=0";
      }elseif($_GET['status'] == 4){
        $where = " p.created_at BETWEEN '".$dt1."' AND '".$dt2."'  $con $u_con AND p.pin_request_for = 1 AND p.repurchase_type=1";
      }else{
        $where = " p.created_at BETWEEN '".$dt1."' AND '".$dt2."'  $con $u_con";
        // $st_con =  ' AND `p`.`pin_status` = 1';AND `p`.`pin_status` = 1
      } 
    }else{
      $where = " $u_con";//$st_con
    }
     //echo $joinQuery.' '.$where;
    $result = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery,$where);
    $start=$_GET['start']+1;
    $idx=0;
    
    foreach($result['data'] as &$res){
      $res[0]=(string)$start;
      $start++;
      $idx++;
    }
    echo json_encode($result);
  }
  public function get_admin_transfer_total(){
    $em = $this->doctrine->em;
    $dt1 = '';
    $dt2 = '';
    $u_con = '';
    $con = '';
    if($this->input->post('pack_type') == 0){
      $con = '';
    }elseif($this->input->post('pack_type') == 1){
      $con =  ' AND p.pin_request_for = 0';
    }else{
      $con =  ' AND p.pin_request_for = 1';
    }

    // if($this->input->post('status') == 0){
    //   $st_con = '';
    // }elseif($this->input->post('status') == 1){
    //   $st_con =  ' AND p.pin_status = 0';
    // }else{
    //   $st_con =  ' AND p.pin_status = 1';
    // }
    if($this->input->post('dt1') != null){
      if($this->input->post('pos1') == 0){
        $dt1 = $this->input->post('dt1').' 02:00:00';
        if($this->input->post('dt2') == null){
          $dt2 = $this->input->post('dt1').' 13:59:59';
        }
      }elseif($this->input->post('pos1') == 1){
        $dt1 = $this->input->post('dt1').' 14:00:00';
        if($this->input->post('dt2') == null){
          $d = $this->input->post('dt1').' 01:59:59';
          $dt2 = date("Y-m-d H:i:s", strtotime($d."+1 days"));
        }
      }else{
          $dt1 = $this->input->post('dt1').' 02:00:00';
          $d = $this->input->post('dt1').' 01:59:59';
          $dt2 = date("Y-m-d H:i:s", strtotime($d."+1 days"));
      }
    }
    if($this->input->post('dt1') != null){
      if($this->input->post('dt2') != null){
        if($this->input->post('pos2') == 0){
          $dt2 = $this->input->post('dt2').' 02:00:00';
        }else{
          $dt2 = $this->input->post('dt2').' 13:59:59';
        }
      }
    }
    if($this->input->post('user_id') != null || $this->input->post('user_name') != null){
      if($this->input->post('user_id') != null && $this->input->post('user_name') == null){
        if($dt1 != null){
          $u_con = " AND u.user_id = ".$this->input->post('user_id');
        }else{
          $u_con = "  u.user_id = ".$this->input->post('user_id');
        }
      }elseif($this->input->post('user_id') == null && $this->input->post('user_name') != null){
        if($dt1 != null){
          $u_con = " AND u.user_id = ".$this->input->post('user_name');
        }else{
          $u_con = "  u.user_id = ".$this->input->post('user_name');
        }
      }else{
        if($dt1 != null){
          $u_con = " AND u.user_id = ".$this->input->post('user_id');
        }else{
          $u_con = "  u.user_id = ".$this->input->post('user_id');
        }
      }
    }
    if($dt1 != null){
      // $where = "p.created_at BETWEEN '".$dt1."' AND '".$dt2."' $con $u_con $st_con";

      if($this->input->post('status') == 0){
        $where = "(p.created_at BETWEEN '".$dt1."' AND '".$dt2."' OR p.pin_used_time BETWEEN '".$dt1."' AND '".$dt2."') $con $u_con";
      }elseif($this->input->post('status') == 1){
        $where = " p.pin_used_time BETWEEN '".$dt1."' AND '".$dt2."'  $con $u_con";
        // $st_con =  ' AND `p`.`pin_status` = 0';  AND `p`.`pin_status` = 0
      }elseif($this->input->post('status') == 3){
        $where = " p.created_at BETWEEN '".$dt1."' AND '".$dt2."'  $con $u_con AND p.pin_request_for = 1 AND p.repurchase_type = 0";
      }elseif($this->input->post('status') == 4){
        $where = " p.created_at BETWEEN '".$dt1."' AND '".$dt2."'  $con $u_con AND p.pin_request_for = 1 AND p.repurchase_type = 1";
      }else{
        $where = " p.created_at BETWEEN '".$dt1."' AND '".$dt2."'  $con $u_con";
        // $st_con =  ' AND `p`.`pin_status` = 1';AND `p`.`pin_status` = 1
      } 

    }else{
      $where = " $u_con ";//$st_con
    }
    $rep_qry = $em->createQuery("SELECT SUM(pp.package_product_display_price) FROM Entity\Pins AS p INNER JOIN Entity\Packages AS pa WITH (p.pin_request_package_id = pa.package_id) INNER JOIN Entity\Package_products as pp WITH (p.pin_request_package_product_id = pp.package_product_id) INNER JOIN Entity\User as u WITH (u.user_id = p.pin_transfer_user_id) WHERE $where");
    $rep_obj = $rep_qry->getResult();
    echo $rep_obj[0][1];
  }

}

?>