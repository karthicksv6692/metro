<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\Query\ResultSetMapping;
require_once 'ssp.customized.class.php';
class Country_crud extends CI_Controller {

	public function __construct(){
		parent::__construct();
		is_user_logged_in();
	}
	public function index(){

		$current_data = array(  'cur_data' => "");
		$this->session->set_userdata($current_data);

		$utill_obj = new Common_utill();
		//$data['add'] = $utill_obj->has_access('country','add');

		$this->load->view('header');
		$this->load->view('common/country/View');
		$this->load->view('footer');
	}
	public function view(){
		$this->index();
	}
	public function datatable_ajax(){

		$ss='';$ee='';$dd='';

		$CI = &get_instance();
		$CI->load->database();
		$sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
		$table = 'Country';
		$primaryKey = 'country_id';
		$columns = array(
		    array( 'db' => 'country_name', 'dt' => 0 ),
		    array( 'db' => 'country_code',  'dt' => 1 ),
		    array(
			        'db'        => 'country_id',
			        'dt'        => 2,
			        'formatter' => function( $d, $row ) {

			        		$utill_obj = new Common_utill();
							//$edit = $utill_obj->has_access('country','edit');
							//$delete = $utill_obj->has_access('country','delete');
							//$s_view = $utill_obj->has_access('country','s_view');

							$edit = "asd";
							$delete = "asd";
							$s_view = "asd";
			        	if($s_view != null){
			        		$ss = '<form method="POST" class="icon_edit_form" action="'.base_url().'index.php/common/Country_crud/S_view">
                            <input type="hidden" name="country_id" value="'.$d.'">
                            <i class="fa fa-search-plus float_left dark_blue" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
			        	}else{$ss = '';}

			        	if($edit != null){
			        		$ee = '<form method="POST" class="icon_edit_form" action="'.base_url().'index.php/common/Country_crud/Edit">
                            <input type="hidden" name="country_id" value="'.$d.'">
                            <i class="fa fa-pencil float_left dark_blue" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
			        	}else{$ee='';}

			        	if($delete != null){
			        		$dd = '<i class="fa fa-trash-o float_left btn_delete but_del dark_blue" aria-hidden="true" data-value="'.$d.'" ></i>';
			        	}else{$dd='';}

		            
			        	return $ss.$ee.$dd;
	        	}
		    )
		);
		echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns));
	}
	public function add(){ 
		//has_access('country','add');
		$main_folder_name = "common"; $sub_folder_name = "country"; $file_name = "Add";
		$em=$this->doctrine->em;
		$utill_obj = new Common_utill();
		if($this->input->post('country_name') != null || $this->input->post('country') != null){
			$this->form_validation->set_rules('country_name', 'Country Name','trim|required|callback_find_alphabets|min_length[2]|max_length[255]|regex_match[/^[[a-zA-Z0-9._ ,\-]+$/]',array('required'=> 'Country Name Requierd','min_length' => 'Country Name needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			$this->form_validation->set_rules('country_code', 'Country Code','trim|required|callback_find_numbers|min_length[1]|max_length[255]|regex_match[/^[[0-9+ ,\-]+$/]',array('required'=> 'Country Code Requierd','min_length' => 'Country Code needs Minimum 2 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_numbers'=>'Numbers Required','regex'=>'Enter Valid Country Code'));
			if($this->form_validation->run() != false){
				$country_name = $this->input->post('country_name');
				$country_code = $this->input->post('country_code');

				$country_name_exists = $utill_obj->get_single_row_using_array_value("Country","country_name",$country_name);
				
				if($country_name_exists == null){
					$country_code_exists = $utill_obj->get_single_row_using_array_value("Country","country_code",$country_code);
					if($country_code_exists == null){
						$country = new Entity\Country;
						$country->setCountryName($country_name);
						$country->setCountryCode($country_code);
						$em->persist($country);
						try{
							$em->flush();
							$this->session->set_flashdata('success', '<p>Successfully Created</p>');
							redirect('index.php/common/country_crud','refresh');
						}catch(UniqueConstraintViolationException $e){
							$this->session->set_flashdata('error', '<p>Already Exists</p>');
							$this->view_call($main_folder_name,$sub_folder_name,$file_name);
					   	}
					}else{
						$this->session->set_flashdata('error', '<p>Country Code Already Exists</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name);
					}
				}else{
					$this->session->set_flashdata('error', '<p>Country Name Already Exists</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name);
				}
			}else{ $this->view_call($main_folder_name,$sub_folder_name,$file_name); }
		}else{ $this->view_call($main_folder_name,$sub_folder_name,$file_name); }
	}
	public function edit(){
		//has_access('country','edit'); 
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Country', 'getCountryId', 'getCountryName', 'index.php/common/country_crud', 'Edit', 'country_id');
		$main_folder_name = "common"; $sub_folder_name = "country"; $file_name = "Edit";
		if($this->input->post('country_id') != null){
			$this->form_validation->set_rules('country_id', 'Country id','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$country_id = $this->input->post('country_id');

				$current_data = array(  'cur_data' => $country_id);
				$this->session->set_userdata($current_data);

				$em = $this->doctrine->em;
				$country_obj  = $em->find('Entity\Country',$country_id); 
				if($country_obj != null){
					$data['country_obj'] = $country_obj;
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}else{
					$this->session->set_flashdata('error', '<p>Country Not Exists</p>');
					redirect('index.php/common/country_crud','refresh');
				}
			}else{
				redirect('index.php/common/country_crud','refresh');
			}
		}elseif($this->input->post('edit_country_id') != null){ 
			if($_SESSION['cur_data'] == $this->input->post('edit_country_id')){
				$this->form_validation->set_rules('edit_country_id', 'Country id','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
				$this->form_validation->set_rules('country_name', 'Country Name','trim|callback_find_alphabets|required|min_length[2]|max_length[255]|regex_match[/^[[a-zA-Z0-9._ ,\-]+$/]',array('required'=> 'Country Name Requierd','min_length' => 'Country Name needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
				$this->form_validation->set_rules('country_code', 'Country Code','trim|callback_find_numbers|required|min_length[1]|max_length[255]|regex_match[/^[[0-9+ ,\-]+$/]',array('required'=> 'Country Code Requierd','min_length' => 'Country Code needs Minimum 3 charecters', 'max_length' => 'Maximum 255 Charecters are allowed','find_numbers'=>'Numbers Required'));
				$em = $this->doctrine->em;
				$country_id = $this->session->userdata('cur_data');
				$country_obj  = $em->find('Entity\Country',$country_id); 
				$data['country_obj'] = $country_obj;
				if ($this->form_validation->run() != false){

					$country_id = $this->input->post('edit_country_id');
					$country_obj  = $em->find('Entity\Country',$country_id); 
					$data['country_obj'] = $country_obj;

					$country_id = $this->input->post('edit_country_id');
					$country_name = $this->input->post('country_name');
					$country_code = $this->input->post('country_code');
					$country_exists  = $em->find('Entity\Country',$country_id); 
					if($country_exists != null){
						$country_exists->setCountryName($country_name);
						$country_exists->setCountryCode($country_code);
						$em->persist($country_exists);
						try{
							$em->flush();
							$this->session->set_flashdata('success', '<p>Successfully Updated</p>');
							redirect('index.php/common/country_crud','refresh');
						}catch(UniqueConstraintViolationException $e){
							$this->session->set_flashdata('error', '<p>Update Failed Already Exists. </p>');
							$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					   	}
					}else{
						$this->session->set_flashdata('error', '<p>Country Not Exists</p>');
						$$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					}
				}else{
					$this->session->set_flashdata('error', '<p>Check the Fields</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}
			}else{
				$this->session->set_flashdata('error', '<p>Wrong ID</p>');
				redirect('index.php/common/country_crud','refresh');
			}
		}else{ $this->view_call($main_folder_name,$sub_folder_name,$file_name,$data); }
	}
	public function s_view(){
		//has_access('country','s_view');
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Country', 'getCountryId', 'getCountryName', 'index.php/common/country_crud', 'S_view', 'country_id');
		$title['title'] = "COUNTRY";
		$main_folder_name = "common"; $sub_folder_name = "country"; $file_name = "S_view";
		if($this->input->post('country_id') != null){
			$this->form_validation->set_rules('country_id', 'Country id','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$country_id = $this->input->post('country_id');
				$em = $this->doctrine->em;
				$country_obj  = $em->find('Entity\Country',$country_id); 
				if($country_obj != null){
					$data['country_obj'] = $country_obj;
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data,$title);
				}else{
					$this->session->set_flashdata('error', '<p>Country Not Exists</p>');
					redirect('index.php/common/country_crud','refresh');
				}
			}else{
				redirect('index.php/common/country_crud','refresh');
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data,$title);
		}
	}
	public function delete(){
		//has_access('country','delete');
		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Country', 'getCountryId', 'getCountryName', 'index.php/common/country_crud', 'Delete', 'country_id');
		$title['title'] = "COUNTRY";
		$main_folder_name = "common"; $sub_folder_name = "country"; $file_name = "Delete";
		if($this->input->post('country_id') != null){
			$this->form_validation->set_rules('country_id', 'Country id','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$country_id = $this->input->post('country_id');

				$current_data = array(  'cur_data' => $country_id);
				$this->session->set_userdata($current_data);

				$em = $this->doctrine->em;
				$country_obj  = $em->find('Entity\Country',$country_id); 
				if($country_obj != null){
					$data['country_obj'] = $country_obj;
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data,$title);
				}else{
					$this->session->set_flashdata('error', '<p>Country Not Exists</p>');
					redirect('index.php/common/country_crud','refresh');
				}
			}else{
				redirect('index.php/common/country_crud','refresh');
			}
		}elseif($this->input->post('delete_country_id') != null){
			if($_SESSION['cur_data'] == $this->input->post('delete_country_id')){
				$this->form_validation->set_rules('delete_country_id', 'Country id','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
				$em = $this->doctrine->em;
				$country_id = $this->input->post('delete_country_id');
				$country_obj  = $em->find('Entity\Country',$country_id); 
				$data['country_obj'] = $country_obj;
				if ($this->form_validation->run() != false){
					$country_exists  = $em->find('Entity\Country',$this->input->post('delete_country_id')); 
					if($country_exists != null){
						$em->remove($country_exists);
						try{
							$em->flush();
							$this->session->set_flashdata('success', '<p>Successfully Deleted</p>');
							redirect('index.php/common/country_crud','refresh');
						}catch(ForeignKeyConstraintViolationException $e){
							$this->session->set_flashdata('error', '<p>Other Table Use this Country. Delete Failed</p>');
							$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data,$title);
					   	}
					}else{
						$this->session->set_flashdata('error', '<p>Country Not Exists</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data,$title);
					}
				}else{
					$this->session->set_flashdata('error', '<p>Check the Fields</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data,$title);
				}
			}else{
				$this->session->set_flashdata('error', '<p>Wrong ID</p>');
				redirect('index.php/common/country_crud','refresh');
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data,$title);
		}
	}
	public function ajax_delete(){
		$em = $this->doctrine->em;
		if($this->input->post('delete_country_id') != null){
			$this->form_validation->set_rules('delete_country_id', 'CountryId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$country_id = $this->input->post('delete_country_id');
				$find_country = $em->getRepository('Entity\Country')->find($country_id); 
				$data['country_obj'] = $find_country;
				if($find_country != null){
					$em->remove($find_country);
					try{
						$em->flush();
						echo 1;
					}catch(ForeignKeyConstraintViolationException $e){ echo -1; }
				}else{ echo -1; }
			}else{ echo 0; }
		}else{ echo 0; }
	}
	public function find_alphabets($str){
	   if (preg_match('#[a-zA-Z]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function find_numbers($str){
	   if (preg_match('#[0-9]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
		$this->load->view('header');
		$this->load->view($main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
		$this->load->view('footer');
	}

	

}
?>