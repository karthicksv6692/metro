<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\Query\ResultSetMapping;
require_once 'ssp.customized.class.php';
class Messages_crud extends CI_Controller {

	public function __construct(){
		parent::__construct();
		is_user_logged_in();
		//get_email_count();
		//get_unread_mail();
	}
	public function index(){
		$main_folder_name = "common"; $sub_folder_name = "messages"; $file_name = "Inbox";	
		$utill_obj = new Common_utill();
		$em = $this->doctrine->em;

		$current_data = array(  'cur_data' => "");
		$this->session->set_userdata($current_data);

		$user_id = $this->session->userdata['user_id'];
		$data['messages'] = $utill_obj->get_inbox_messages($user_id);
		//$data['attachment']  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $user_id, 'attachment_referer_type'=>RT::$REF['messages']));

		$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);//
	}
  public function mobile_message(){
    $main_folder_name = "common"; $sub_folder_name = "mobile"; $file_name = "Mobile_message";  
    $this->view_call($main_folder_name,$sub_folder_name,$file_name);
  }
  public function mobile_message_send(){
    $utill_obj = new Common_utill();
    if($this->input->post('type') != null){
      $type = $this->input->post('type');
      if($type == 1){
        if($this->input->post('user_id') != null){
          if($this->input->post('message') != null){
            $mobs = $utill_obj->get_all_users_mobile($this->input->post('user_id'));
            if(!is_numeric($mobs)){
              // MEssage Start
                $sent_message = $this->input->post('message');
                $mobile_number = $mobs[0];
                $var_message = urlencode($sent_message);
                $var_destination = urlencode($mobile_number);
                $var_message_type = 2;
                $var_senderid = urlencode('METROK');
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "http://sms.dhinatechnologies.com/api/sms_api.php?username=metrokings&api_password=swyvpq558x0&message=$var_message&destination=$var_destination&type=$var_message_type&sender=$var_senderid");
                curl_setopt($ch, CURLOPT_HEADER, 0);  
                curl_exec($ch);
                curl_close($ch);
              // MEssage Start
              $this->session->set_flashdata('success', '<p>Message Send Successfully</p>');
              $this>redirect('index.php/common/messages_crud/mobile_message','refresh');
            }else{
              $this->session->set_flashdata('error', '<p>Mobile Number Not Exists</p>');
              $this->mobile_message();
            }
          }else{
            $this->session->set_flashdata('error', '<p>Please Fill Message</p>');
            $this->mobile_message();
          }
        }else{
          $this->session->set_flashdata('error', '<p>Select User</p>');
          $this->mobile_message();
        }
      }else{
        if($this->input->post('message') != null){
          $mobs = $utill_obj->get_all_users_mobile();
          if(!is_numeric($mobs)){
            foreach ($mobs as $mob) {
              // MEssage Start
                $sent_message = $this->input->post('message');
                $mobile_number = $mob;
                $var_message = urlencode($sent_message);
                $var_destination = urlencode($mobile_number);
                $var_message_type = 2;
                $var_senderid = urlencode('METROK');
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "http://sms.dhinatechnologies.com/api/sms_api.php?username=metrokings&api_password=swyvpq558x0&message=$var_message&destination=$var_destination&type=$var_message_type&sender=$var_senderid");
                curl_setopt($ch, CURLOPT_HEADER, 0);  
                curl_exec($ch);
                curl_close($ch);
              // MEssage Start
            }
            $this->session->set_flashdata('success', '<p>Message Send Successfully</p>');
            $this>redirect('index.php/common/messages_crud/mobile_message','refresh');
          }else{
            $this->session->set_flashdata('error', '<p>MobileNumber Empty</p>');
            $this->mobile_message();
          }
        }else{
          $this->session->set_flashdata('error', '<p>Please Fill Message</p>');
          $this->mobile_message();
        }
      }
    }
  }
	public function create_message(){
		$em=$this->doctrine->em;

		$main_folder_name = "common"; $sub_folder_name = "messages"; $file_name = "New_message";	
		$utill_obj = new Common_utill();
		if($this->input->post('msg') != null || $this->input->post('user_name') != null || $this->input->post('select_all') != null){ 
        //print_r($_POST);exit();

      if($this->input->post('select_all') != null){
        $query = $em->createQuery("SELECT b.user_id FROM Entity\User as b WHERE b.user_id != ".$_SESSION['user_id']);
        $usr = $query->getResult();
        if($usr!= null){
          for($i=0;$i<count($usr);$i++){
            $from = $utill_obj->get_reference_obj('User',$this->session->userdata('user_id'));
            $to = $utill_obj->get_reference_obj('User',$usr[$i]);
            if($to != null){
              $subject = $this->input->post('subject');
              $full_message = $this->input->post('message');

              $date = date('Y-m-d h:m:s');
              $now = new DateTime($date);
              $images = $_FILES['files'];
              $msg_result = $utill_obj->add_message($from,$to,$subject,$full_message,$now,$images);
              if($i == count($usr)-1){
                $this->session->set_flashdata('success', '<p>Message Sent Successfully</p>');
                $this>redirect('index.php/common/Messages_crud','refresh');
              }
            }
          }
        }
      }elseif($this->input->post('user_name') != null){
        $ar = $this->input->post('user_name');
        if(count($ar) > 0){
          for($i=0;$i<count($ar);$i++){
            $from = $utill_obj->get_reference_obj('User',$this->session->userdata('user_id'));
            $to = $utill_obj->get_reference_obj('User',$ar[$i]);
            if($to != null){
              $subject = $this->input->post('subject');
              $full_message = $this->input->post('message');

              $date = date('Y-m-d h:m:s');
              $now = new DateTime($date);
              $images = $_FILES['files'];
              $msg_result = $utill_obj->add_message($from,$to,$subject,$full_message,$now,$images);
              if($i == count($ar)-1){
                $this->session->set_flashdata('success', '<p>Message Sent Successfully</p>');
                $this>redirect('index.php/common/Messages_crud','refresh');
              }
            }
          }
        }
      }elseif($this->input->post('reply') != null){
        $query = $em->createQuery("SELECT b.user_id FROM Entity\User as b WHERE b.user_id =".$this->input->post('reply'));
        $usr = $query->getResult();
        if($usr != null){
          for($i=0;$i<count($usr);$i++){
            $from = $utill_obj->get_reference_obj('User',$this->session->userdata('user_id'));
            $to = $utill_obj->get_reference_obj('User',$usr[$i]);
            if($to != null){
              $subject = $this->input->post('subject');
              $full_message = $this->input->post('message');

              $date = date('Y-m-d h:m:s');
              $now = new DateTime($date);
              $images = $_FILES['files'];
              $msg_result = $utill_obj->add_message($from,$to,$subject,$full_message,$now,$images);
              if($i == count($usr)-1){
                $this->session->set_flashdata('success', '<p>Message Sent Successfully</p>');
                $this>redirect('index.php/common/Messages_crud','refresh');
              }
            }
          }
        }
      }else{
        if($_SESSION['role_id'] != 7){
             $query = $em->createQuery("SELECT b.user_id FROM Entity\User as b WHERE b.user_role_id = 7");
              $usr = $query->getResult();
             if($usr!= null){
              for($i=0;$i<count($usr);$i++){
                $from = $utill_obj->get_reference_obj('User',$this->session->userdata('user_id'));
                $to = $utill_obj->get_reference_obj('User',$usr[$i]);
                if($to != null){
                  $subject = $this->input->post('subject');
                  $full_message = $this->input->post('message');

                  $date = date('Y-m-d h:m:s');
                  $now = new DateTime($date);
                  $images = $_FILES['files'];
                  $msg_result = $utill_obj->add_message($from,$to,$subject,$full_message,$now,$images);
                  if($i == count($usr)-1){
                    $this->session->set_flashdata('success', '<p>Message Sent Successfully</p>');
                    $this>redirect('index.php/common/Messages_crud','refresh');
                  }
                }
              }
            }
        }else{
          $this->view_call($main_folder_name,$sub_folder_name,$file_name);
        }
      }
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name);
		}
	}
	public function sent_items(){
		$main_folder_name = "common"; $sub_folder_name = "messages"; $file_name = "Sent";	
		$utill_obj = new Common_utill();

		$current_data = array(  'cur_data' => "");
		$this->session->set_userdata($current_data);

		$user_id = $this->session->userdata['user_id'];
		$data['messages'] = $utill_obj->get_sent_messages($user_id);


		$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
	}
	public function view_sent_message(){
		// print_r($_POST);
		$main_folder_name = "common"; $sub_folder_name = "messages"; $file_name = "Sent_single_view";	
		$utill_obj = new Common_utill();
		$em = $this->doctrine->em;
		$this->form_validation->set_rules('hid_msg_id', 'Country id','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
		if ($this->form_validation->run() != false){
			$check = $utill_obj->get_single_row_using_array_value_two('User_messages','user_messages_id',$this->input->post('hid_msg_id'));
			//$data['attachment']  = $em->getRepository('Entity\Attachments')->findBy(array('attachement_referer_id' => $this->input->post('hid_msg_id'), 'attachment_referer_type'=>7));

			if($check != null){
				// $utill_obj->set_message_has_read_one($this->input->post('hid_message_id'));
				$data['from_user'] = $check->getUserMessagesFromId()->getUserEmail();
				$data['to_user'] = $check->getUserMessagesToId()->getUserName();
				$data['message'] = base64_decode($check->getUserMessagesDetail());
        $data['date'] = $check->getUserMessagesReceivedDate();
			}else{$data['single_message'] = "";}
			
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}else{
			$this->session->set_flashdata('error', '<p>Wrong Message Id</p>');
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function view_inbox_message(){//print_r($_POST);exit();
		$main_folder_name = "common"; $sub_folder_name = "messages"; $file_name = "Inbox_single_view";	
		$utill_obj = new Common_utill();
		$em = $this->doctrine->em;
		$this->form_validation->set_rules('hid_msg_id', 'Country id','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
		if ($this->form_validation->run() != false){
			$check = $utill_obj->get_single_row_using_array_value_two('User_messages','user_messages_id',$this->input->post('hid_msg_id'));
			if($check != null){
				//$utill_obj->set_message_has_read_one($this->input->post('hid_msg_id'));
        $data['from_user'] = $check->getUserMessagesFromId()->getUserName();
				$data['from_user_id'] = $check->getUserMessagesFromId()->getUserId();
        $data['id'] = $check->getUserMessagesId();
				$data['date'] = $check->getUserMessagesReceivedDate();
				$data['to_user'] = $check->getUserMessagesToId()->getUserName();
        $data['message'] = base64_decode($check->getUserMessagesDetail());

				$data['subject'] = $check->getUserMessagesSubject();
        $data['attachment']  = $em->getRepository('Entity\Attachments')->findBy(array('attachment_referer_id' => $this->input->post('hid_msg_id'), 'attachment_referer_type'=>RT::$REF['messages']));

			}else{$data['single_message'] = "";}
			//get_email_count();
			//get_unread_mail();
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}else{
			$this->session->set_flashdata('error', '<p>Wrong Message Id</p>');
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function reply_message(){

		$main_folder_name = "common"; $sub_folder_name = "messages"; $file_name = "Reply_message";	
		$utill_obj = new Common_utill();

		if($this->input->post('to_id') != null){ //print_r($_POST);exit();
      $data{'to_id'} = $this->input->post('to_id');
      $data['user_name'] = "Admin";
      $this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}elseif($this->input->post('msg') != null || $this->input->post('user_name') != null){
			
		}else{ //$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data); 
		}
	}
	public function inbox_reply_message(){
		$title['title'] = "REPLY MESSAGE";
		$data['title'] = "REPLY MESSAGE";
		$main_folder_name = "common"; $sub_folder_name = "messages"; $file_name = "Inbox_reply_message";	
		$utill_obj = new Common_utill();
		if($this->input->post('to_id') != null){
			$msg_obj = $utill_obj->get_single_row_using_array_value('User','user_id',$this->input->post('to_id'));
			if($msg_obj != null){

				$data['user_name'] = $msg_obj->getUserName();
				$data['user_mail'] = $msg_obj->getUserEmail();
				$data['user_id'] = $msg_obj->getUserId();
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}else{
				$this->session->set_flashdata('error', '<p>Messeage Not Exist</p>');
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}
		}elseif($this->input->post('msg') != null || $this->input->post('user_name') != null){
			
		}else{ //$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data); 
		}
	}
	public function trash_message(){
		$utill_obj = new Common_utill();
		$main_folder_name = "common"; $sub_folder_name = "messages"; $file_name = "Inbox_single_view";
		if($this->input->post('trash_id') != null){
			$trash = $utill_obj->message_trash( $this->input->post('trash_id') );
			if($trash == 1){
				$this->session->set_flashdata('success', '<p>Successfully</p>');
				$this>redirect('common/Messages_crud','refresh');
			}else{
				$this->session->set_flashdata('error', '<p>Trash Failed</p>');
				$this>redirect('common/Messages_crud','refresh');
			}
		}
	}
	public function trash(){
		$title['title'] = "MESSAGES";
		$main_folder_name = "common"; $sub_folder_name = "messages"; $file_name = "Trash";	
		$utill_obj = new Common_utill();

		$current_data = array(  'cur_data' => "");
		$this->session->set_userdata($current_data);

		$user_id = $this->session->userdata['user_id'];
		$data['messages'] = $utill_obj->get_trash_messages($user_id);


		$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
	}
	public function un_trash(){
		$utill_obj = new Common_utill();
		if($this->input->post('trash_id') != null){ //print_r($_POST);exit();
			$trash = $utill_obj->message_un_trash( $this->input->post('trash_id') );
			if($trash == 1){
				$this->session->set_flashdata('success', '<p>Successfully</p>');
				$this>redirect('svk_common/Messages_crud/trash','refresh');
			}else{
				$this->session->set_flashdata('error', '<p>Trash Failed</p>');
				$this>redirect('svk_common/Messages_crud/trash','refresh');
			}
		}
	}
	public function delete_message(){//print_r($_POST);exit();
		$utill_obj = new Common_utill();
		if($this->input->post('trash_id') != null){ 
			$delete = $utill_obj->delete_message( $this->input->post('trash_id') );
			if($delete == 1){
				$this->session->set_flashdata('success', '<p>Successfully</p>');
				$this>redirect('index.php/common/Messages_crud','refresh');
			}else{
				$this->session->set_flashdata('error', '<p>Delete Failed</p>');
				$this>redirect('index.php/common/Messages_crud','refresh');
			}
		}elseif($this->input->post('sent_trash_id') != null){ 
			$delete = $utill_obj->delete_message( $this->input->post('sent_trash_id') );
			if($delete == 1){
				$this->session->set_flashdata('success', '<p>Successfully</p>');
				$this>redirect('index.php/common/Messages_crud/sent_items','refresh');
			}else{
				$this->session->set_flashdata('error', '<p>Delete Failed</p>');
				$this>redirect('index.php/common/Messages_crud/sent_items','refresh');
			}
		}
	}
	public function view_trash_message(){
		$title['title'] = "MESSAGE";
		$main_folder_name = "common"; $sub_folder_name = "messages"; $file_name = "Trash_single_view";	
		$utill_obj = new Common_utill();
		$em = $this->doctrine->em;
		$this->form_validation->set_rules('hid_message_id', 'Country id','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
		if ($this->form_validation->run() != false){
			$check = $utill_obj->get_single_row_using_array_value_two('User_messages','user_messages_id',$this->input->post('hid_message_id'));

			$data['attachment']  = $em->getRepository('Entity\Attachments')->findBy(array('attachement_referer_id' => $this->input->post('hid_message_id'), 'attachment_referer_type'=>7));

			if($check != null){
				// $utill_obj->set_message_has_read_one($this->input->post('hid_message_id'));
				$from_user = $utill_obj->get_user_name_with_primary_key($check->getUserMessagesFromId());
				$to_user = $utill_obj->get_user_name_with_primary_key($check->getUserMessagesToId());

				$obj = new Entity\Single_mail;
				$obj->setFromUser($from_user);
				$obj->setToUser($to_user);
				$obj->setObj($check);

				$data['single_message'] = $obj;
			}else{$data['single_message'] = "";}
			
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}else{
			$this->session->set_flashdata('error', '<p>Wrong Message Id</p>');
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function get_ref(){
		$utill_obj = new Common_utill();
		$xx = $utill_obj->get_reference_obj('user',28);
		print_r($xx);
	}
	public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
		$this->load->view('header');
		$this->load->view($main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
		$this->load->view('footer');
	}
}
?>