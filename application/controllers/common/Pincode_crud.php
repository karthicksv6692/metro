<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\Query\ResultSetMapping;
require_once 'ssp.customized.class.php';
class Pincode_crud extends CI_Controller {

	public function __construct(){
		parent::__construct();
		is_user_logged_in();
	}
	public function index(){

		$current_data = array(  'cur_data' => "");
		$this->session->set_userdata($current_data);

		$utill_obj = new Common_utill();
		//$data['add'] = $utill_obj->has_access('pincode','add');

		$this->load->view('header');
		$this->load->view('common/pincode/View');
		$this->load->view('footer');
	}
	public function view(){
		$this->index();
	}
	public function datatable_ajax(){

		$ss='';$ee='';$dd='';

		$CI = &get_instance();
		$CI->load->database();
		$sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);

		$table = 'Pincode';
		$primaryKey = 'pincode_id';
		$columns = array(
		    array( 'db' => '`a`.`pincode_no`', 'dt' => 0,'field' => 'pincode_no'),
		    array( 'db' => '`a`.`pincode_place`', 'dt' => 1,'field' => 'pincode_place'),
		    array( 'db' => '`a`.`pincode_district`', 'dt' => 2,'field' => 'pincode_district'),
		    array( 'db' => '`a`.`pincode_state`', 'dt' => 3,'field' => 'pincode_state'),
		    array( 'db' => '`p`.`country_name`', 'dt' => 4,'field' => 'country_name'),
		    array( 'db' => '`a`.`pincode_id`',
			       'dt' => 5,
			       'field' => 'pincode_id',
			       'formatter' => function( $d, $row ) {

			       	$utill_obj = new Common_utill();
					// $edit = $utill_obj->has_access('pincode','edit');
					// $delete = $utill_obj->has_access('pincode','delete');
					// $s_view = $utill_obj->has_access('pincode','s_view');
					$edit = "asd";
					$delete = "asd";
					$s_view = "asd";

					if($s_view != null){
		        		$ss = '<form method="POST" class="icon_edit_form" action="'.base_url().'index.php/common/Pincode_crud/S_view">
                            <input type="hidden" name="pincode_id" value="'.$d.'">
                            <i class="fa fa-search-plus float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
		        	}else{$ss = '';}

		        	if($edit != null){
		        		$ee = ' <form method="POST" class="icon_edit_form" action="'.base_url().'index.php/common/Pincode_crud/Edit">
                            <input type="hidden" name="pincode_id" value="'.$d.'">
                            <i class="fa fa-pencil float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
		        	}else{$ee='';}

		        	if($delete != null){
		        		$dd = '<i class="fa fa-trash-o float_left btn_delete but_del" aria-hidden="true" data-value="'.$d.'" ></i>';
		        	}else{$dd='';}

		            // return '<form method="POST" class="icon_edit_form" action="'.base_url().'common/Pincode_crud/S_view">
              //               <input type="hidden" name="pincode_id" value="'.$d.'">
              //               <i class="fa fa-search-plus float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
              //           </form>
              //           <form method="POST" class="icon_edit_form" action="'.base_url().'common/Pincode_crud/Edit">
              //               <input type="hidden" name="pincode_id" value="'.$d.'">
              //               <i class="fa fa-pencil float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
              //           </form>
              //               <i class="fa fa-trash-o float_left btn_delete but_del" aria-hidden="true" data-value="'.$d.'" ></i>';

		        	return $ss.$ee.$dd;
	        	}
	        )
		);
		$joinQuery = "FROM `Pincode` AS `a` INNER JOIN `Country` AS `p` ON (`a`.`pincode_country_id` = `p`.`country_id`)";
		 
		echo json_encode(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery));
	}
	public function add(){
		// has_access('pincode','add');
		$main_folder_name = "common"; $sub_folder_name = "pincode"; $file_name = "Add";

		$em = $this->doctrine->em;
		
		$data['country_list'] = $em->getRepository("Entity\Country")->findAll();
		if($this->input->post('pincode') != null){
			$this->form_validation->set_rules('pincode_no', 'Pincode','trim|required|regex_match[/^[0-9]+$/]|min_length[4]|max_length[8]',array('regex_match' => 'Numbers Only Allowed','min_length'=>'Minimumm length 4','max_length'=>'Maximum Length 8'));
			$this->form_validation->set_rules('district', 'District','trim|required|callback_find_alphabets|min_length[3]|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			$this->form_validation->set_rules('country_id', 'Country','trim|required|numeric',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			$this->form_validation->set_rules('place', 'Place','trim|required|callback_find_alphabets|min_length[3]|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			$this->form_validation->set_rules('state', 'State','trim|required|callback_find_alphabets|min_length[3]|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
			if ($this->form_validation->run() != false){ 
				$pincode_no = $this->input->post('pincode_no');
				$pincode_district = $this->input->post('district');
				$pincode_country_id = $this->input->post('country_id');
				$pincode_place = $this->input->post('place');
				$pincode_state = $this->input->post('state');

				$find_pincode = $em->getRepository('Entity\Pincode')->findOneBy(array('pincode_no' => $pincode_no)); 
				$find_country = $em->getRepository('Entity\Country')->find($pincode_country_id);
				if($find_country != null){
					if($find_pincode == null){
						$pincode_obj = new Entity\Pincode;
						$pincode_obj->setPincodeNo($pincode_no);
						$pincode_obj->setPincodePlace($pincode_place);
						$pincode_obj->setPincodeDistrict($pincode_district);
						$pincode_obj->setPincodeState($pincode_state);
						$pincode_obj->setPincodeCountryId($em->getReference('Entity\Country',$pincode_country_id));
						$em->persist($pincode_obj);
						try {
							$em->flush();
							$new_pincode_id =  $em->getReference('Entity\Pincode',$pincode_obj->getPincodeId())->getPincodeId();
							if($new_pincode_id > 0){ 
								$this->session->set_flashdata('success', '<p>Successfully Created</p>');
								redirect('index.php/common/Pincode_crud','refresh');
							}else{
								$this->session->set_flashdata('error', '<p>Pincode Created Failed</p>');
								$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
							}
						}catch (UniqueConstraintViolationException $e){
							$this->session->set_flashdata('error', '<p>Pincode Already Exists</p>');
							$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
						}
					}else{
						$this->session->set_flashdata('error', '<p>Pincode Already Exists</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					}
				}else{
					$this->session->set_flashdata('error', '<p>Selected Country is Not Exists. Select another One</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}
			}else{
				$this->session->set_flashdata('error', '<p>Error in Fields</p>');
				$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
			}
		}else{ $this->view_call($main_folder_name,$sub_folder_name,$file_name,$data); }
	}
	public function edit(){
		// has_access('pincode','edit');
		$main_folder_name = "common"; $sub_folder_name = "pincode"; $file_name = "Edit";

		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Pincode', 'getPincodeId', 'getPincodeNo', 'index.php/common/pincode_crud', 'Edit', 'pincode_id');

		$em = $this->doctrine->em;
		$country_list = $em->getRepository("Entity\Country")->findAll();
		$data['country_obj'] = $country_list;
		if($this->input->post('pincode_id') != null){
			$this->form_validation->set_rules('pincode_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$pincode_id = $this->input->post('pincode_id');

				$current_data = array(  'cur_data' => $pincode_id);
				$this->session->set_userdata($current_data);


				$find_pincode = $em->getRepository('Entity\Pincode')->find($pincode_id); 
				// print_r($find_pincode);
				if($find_pincode != null){
					//$find_pincode->setPincodeCountryId($country_list);
					$data['pincode_obj'] = $find_pincode;
					$data['country_obj'] = $country_list;
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}else{
					$this->session->set_flashdata('error', '<p>Wrong Pincode ID</p>');
					redirect('index.php/common/Pincode_crud','refresh');
				}
			}else{
				redirect('index.php/common/Pincode_crud','refresh');
			}
		}elseif($this->input->post('edit_pincode_id') != null){
			if($_SESSION['cur_data'] == $this->input->post('edit_pincode_id')){
				$data['pincode_obj'] = $em->getRepository('Entity\Pincode')->find($this->input->post('edit_pincode_id'));
				// validation start
					$this->form_validation->set_rules('edit_pincode_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
					$this->form_validation->set_rules('pincode_no', 'Pincode','trim|required|regex_match[/^[0-9]+$/]|min_length[4]|max_length[8]',array('regex_match' => 'Numbers Only Allowed','min_length'=>'Minimumm length 4','max_length'=>'Maximum Length 8'));
					$this->form_validation->set_rules('district', 'District','trim|required|callback_find_alphabets|min_length[3]|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
					$this->form_validation->set_rules('country_id', 'Country','trim|required|numeric',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
					$this->form_validation->set_rules('place', 'Place','trim|required|callback_find_alphabets|min_length[3]|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
					$this->form_validation->set_rules('state', 'State','trim|required|callback_find_alphabets|min_length[3]|max_length[255]',array('max_length' => 'Maximum 255 Charecters are allowed','find_alphabets'=>'Albhabets Required'));
				// validation End
				if ($this->form_validation->run() != false){   
					$pincode_id = $this->input->post('edit_pincode_id');
					$pincode_no = $this->input->post('pincode_no');
					$pincode_district = $this->input->post('district');
					$pincode_country_id = $this->input->post('country_id');
					$pincode_place = $this->input->post('place');
					$pincode_state = $this->input->post('state');

					$find_country = $em->getRepository('Entity\Country')->find($pincode_country_id);
					$find_pincode = $em->getRepository('Entity\Pincode')->find($pincode_id); 
					//$find_pincode->setpincodeCountryId($country_list);
					if($find_country != null){
						$find_pincode->setPincodeNo($pincode_no);
						$find_pincode->setPincodePlace($pincode_place);
						$find_pincode->setPincodeDistrict($pincode_district);
						$find_pincode->setPincodeState($pincode_state);
						$find_pincode->setPincodeCountryId($em->getReference('Entity\Country',$pincode_country_id));
						$em->persist($find_pincode);
						try {
							$em->flush();
							$this->session->set_flashdata('success', '<p>Successfully Updated</p>');
							redirect('index.php/common/Pincode_crud','refresh');
						}catch (UniqueConstraintViolationException $e){ //print_r($_POST);exit();	
							$this->session->set_flashdata('error', '<p>Pincode Already Exists</p>');
							$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
						}
						
					}else{
						$this->session->set_flashdata('error', '<p>Pincode Already Exists</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					}
				}else{
					$this->session->set_flashdata('error', '<p>Selected Country is Not Exists. Select another One</p>');
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}
			}else{
				$this->session->set_flashdata('error', '<p>Wrong ID</p>');
				redirect('index.php/common/Pincode_crud','refresh');
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function s_view(){
		//has_access('pincode','s_view');
		$main_folder_name = "common"; $sub_folder_name = "pincode"; $file_name = "S_view";

		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Pincode', 'getPincodeId', 'getPincodeNo', 'index.php/common/pincode_crud', 'S_view', 'pincode_id');

		$em = $this->doctrine->em;

		if($this->input->post('pincode_id') != null){
			$this->form_validation->set_rules('pincode_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$pincode_id = $this->input->post('pincode_id');
				$find_pincode = $em->getRepository('Entity\Pincode')->find($pincode_id); 
				if($find_pincode != null){
					$data['pincode_obj'] = $find_pincode;
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}else{
					$this->session->set_flashdata('error', '<p>Wrong Pincode ID</p>');
					redirect('index.php/common/Pincode_crud','refresh');
				}
			}else{
				redirect('index.php/common/Pincode_crud','refresh');
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function delete(){
		//has_access('pincode','delete');
		$main_folder_name = "common"; $sub_folder_name = "pincode"; $file_name = "Delete";

		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Pincode', 'getPincodeId', 'getPincodeNo', 'index.php/common/pincode_crud', 'Delete', 'pincode_id');

		$em = $this->doctrine->em;

		if($this->input->post('pincode_id') != null){
			$this->form_validation->set_rules('pincode_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$pincode_id = $this->input->post('pincode_id');

				$current_data = array(  'cur_data' => $pincode_id);
				$this->session->set_userdata($current_data);

				$find_pincode = $em->getRepository('Entity\Pincode')->find($pincode_id); 
				if($find_pincode != null){
					$data['pincode_obj'] = $find_pincode;
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}else{
					$this->session->set_flashdata('error', '<p>Wrong Pincode ID</p>');
					redirect('index.php/common/Pincode_crud','refresh');
				}
			}else{
				redirect('index.php/common/Pincode_crud','refresh');
			}
		}elseif($this->input->post('delete_pincode_id') != null){
			if($_SESSION['cur_data'] == $this->input->post('delete_pincode_id')){
				$this->form_validation->set_rules('delete_pincode_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
				if ($this->form_validation->run() != false){
					$pincode_id = $this->input->post('delete_pincode_id');
					$find_pincode = $em->getRepository('Entity\Pincode')->find($pincode_id); 
					$data['pincode_obj'] = $find_pincode;
					if($find_pincode != null){
						$em->remove($find_pincode);
						try{
							$em->flush();
							$this->session->set_flashdata('success', '<p>Successfully Deleted</p>');
							redirect('index.php/common/Pincode_crud','refresh');
						}catch(ForeignKeyConstraintViolationException $e){
							$this->session->set_flashdata('error', '<p>Other Table Use this Country. Delete Failed</p>');
							$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					   	}
					}else{
						$this->session->set_flashdata('error', '<p>Pincode Not Exists</p>');
						$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
					}
				}else{
					redirect('index.php/common/Pincode_crud','refresh');
				}
			}else{
				$this->session->set_flashdata('error', '<p>Wrong ID</p>');
				redirect('index.php/common/Pincode_crud','refresh');
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function ajax_delete(){
		$em = $this->doctrine->em;
		if($this->input->post('delete_pincode_id') != null){
			$this->form_validation->set_rules('delete_pincode_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$pincode_id = $this->input->post('delete_pincode_id');
				$find_pincode = $em->getRepository('Entity\Pincode')->find($pincode_id); 
				$data['pincode_obj'] = $find_pincode;
				if($find_pincode != null){
					$em->remove($find_pincode);
					try{
						$em->flush();
						echo 1;
					}catch(ForeignKeyConstraintViolationException $e){
						echo -1;
				   	}
				}else{
					echo -1;
				}
			}else{
				echo 0;
			}
		}else{
			echo 0;
		}
	}
	public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
		$this->load->view('header');
		$this->load->view($main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
		$this->load->view('footer');
	}
	public function alphabets_and_number($str){
	   if (preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function find_alphabets($str){
	   if (preg_match('#[a-zA-Z]#', $str)) {
	     return true;
	   }
	   return false;
	}
	public function find_numbers($str){
	   if (preg_match('#[0-9]#', $str)) {
	     return true;
	   }
	   return false;
	}
}
?>