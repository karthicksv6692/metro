<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\Query\ResultSetMapping;
require_once 'ssp.customized.class.php';
class Address_crud extends CI_Controller {

	public function __construct(){
		parent::__construct();
		is_user_logged_in();
	}
	public function index(){

		$current_data = array(  'cur_data' => "");
		$this->session->set_userdata($current_data);

		$utill_obj = new Common_utill();
		//$data['add'] = $utill_obj->has_access('pincode','add');

		$this->load->view('header');
		$this->load->view('common/address/View');
		$this->load->view('footer');
	}
	public function view(){
		$this->index();
	}
	public function datatable_ajax(){

		$ss='';$ee='';$dd='';

		$CI = &get_instance();
		$CI->load->database();
		$sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
		$table = 'Address';
		$primaryKey = 'address_id';
		$columns = array(
		    array( 'db' => '`a`.`address_full_address`', 'dt' => 0,'field' => 'address_full_address' ),
		    array( 'db' => '`p`.`pincode_no`', 'dt' => 1,'field' => 'pincode_no'),
		    array( 'db' => '`p`.`pincode_place`', 'dt' => 2,'field' => 'pincode_place'),
		    array( 'db' => '`p`.`pincode_district`', 'dt' => 3,'field' => 'pincode_district'),
		    array( 'db' => '`p`.`pincode_state`', 'dt' => 4,'field' => 'pincode_state'),
		    array( 'db' => '`a`.`address_id`',
			       'dt' => 5,
			       'field' => 'address_id',
			       'formatter' => function( $d, $row ) {

			       			$utill_obj = new Common_utill();
							// $edit = $utill_obj->has_access('address','edit');
							// $delete = $utill_obj->has_access('address','delete');
							// $s_view = $utill_obj->has_access('address','s_view');


							$edit = "asd";
							$delete = "asd";
							$s_view = "asd";


							if($s_view != null){
				        		$ss = '<form method="POST" class="icon_edit_form" action="'.base_url().'index.php/common/Address_crud/S_view">
                            <input type="hidden" name="address_id" value="'.$d.'">
                            <i class="fa fa-search-plus float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
				        	}else{$ss = '';}

				        	if($edit == null){
				        		$ee = ' <form method="POST" class="icon_edit_form" action="'.base_url().'index.php/common/Address_crud/Edit">
                            <input type="hidden" name="address_id" value="'.$d.'">
                            <i class="fa fa-pencil float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                        </form>';
				        	}else{$ee='';}

				        	if($delete == null){
				        		$dd = ' <i class="fa fa-trash-o float_left btn_delete but_del" aria-hidden="true" data-value="'.$d.'" ></i>';
				        	}else{$dd='';}

		      
				        	return $ss.$ee.$dd;
	        	}
		    ),
		   // array( 'db' => '`a`.`created_at`', 'dt' => 6,'field' => 'created_at'),

		);
		$joinQuery = "FROM `Address` AS `a` INNER JOIN `Pincode` AS `p` ON (`a`.`address_pincode_id` = `p`.`pincode_id`) ";
		 
		echo json_encode(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns,$joinQuery));
	}
	public function s_view(){
		//has_access('pincode','s_view');
		$main_folder_name = "common"; $sub_folder_name = "address"; $file_name = "S_view";

		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Pincode', 'getPincodeId', 'getPincodeNo', 'index.php/common/pincode_crud', 'S_view', 'pincode_id');

		$em = $this->doctrine->em;

		if($this->input->post('address_id') != null){
			$this->form_validation->set_rules('address_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$address_id = $this->input->post('address_id');
				$find_address = $em->getRepository('Entity\Address')->find($address_id); 
				if($find_address != null){
					$data['address_obj'] = $find_address;
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}else{
					$this->session->set_flashdata('error', '<p>Wrong Pincode ID</p>');
					redirect('index.php/common/Pincode_crud','refresh');
				}
			}else{
				redirect('index.php/common/Pincode_crud','refresh');
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
		$this->load->view('header');
		$this->load->view($main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
		$this->load->view('footer');
	}
	
}
?>