<?php
defined('BASEPATH') OR exit('No direct script address allowed');
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\Query\ResultSetMapping;
require_once 'ssp.customized.class.php';
class Contact_crud extends CI_Controller {

	public function __construct(){
		parent::__construct();
		is_user_logged_in();
	}
	public function index(){

		$current_data = array(  'cur_data' => "");
		$this->session->set_userdata($current_data);

		$utill_obj = new Common_utill();
		//$data['add'] = $utill_obj->has_access('pincode','add');

		$this->load->view('header');
		$this->load->view('common/contact/View');
		$this->load->view('footer');
	}
	public function view(){
		$this->index();
	}
	public function datatable_ajax(){

		$ss='';$ee='';$dd='';

		$CI = &get_instance();
		$CI->load->database();
		$sql_details = array('user' => $CI->db->username,'pass' => $CI->db->password,'db'   => $CI->db->database,'host' => $CI->db->hostname);
		$table = 'Contact';
		$primaryKey = 'contact_id';
		$columns = array(
		    array( 'db' => 'contact_name', 'dt' => 0 ),
		    array( 'db' => 'contact_designation',  'dt' => 1 ),
		    array( 'db' => 'contact_mobile_primary',  'dt' => 2 ),
		    array( 'db' => 'contact_email_id',  'dt' => 3 ),
		    array(
			        'db'        => 'contact_id',
			        'dt'        => 4,
			        'formatter' => function( $d, $row ) {

			        	$utill_obj = new Common_utill();
						// $s_view = $utill_obj->has_access('contact','s_view');
						$s_view = "asdasd";

						if($s_view != null){
				        		$ss = '<form method="POST" class="icon_edit_form" action="'.base_url().'index.php/common/Contact_crud/S_view">
                            			<input type="hidden" name="contact_id" value="'.$d.'">
                           				 <i class="fa fa-search-plus float_left" aria-hidden="true" onclick="$(this).closest(\'form\').submit();"></i>
                       					 </form>';
			        	}else{$ss = '';}

		            return $ss;

	        	}
		    )
		);
		echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns));
	}
	public function s_view(){
		//has_access('pincode','s_view');
		$main_folder_name = "common"; $sub_folder_name = "contact"; $file_name = "S_view";

		$utill_obj = new Common_utill();
		$data['search'] = $utill_obj->search_field('Contact', 'getContactId', 'getContactName', 'index.php/common/contact_crud', 'S_view', 'contact_id');

		$em = $this->doctrine->em;

		if($this->input->post('contact_id') != null){
			$this->form_validation->set_rules('contact_id', 'PincodeId','trim|required|regex_match[/^[0-9\-]+$/]',array('regex_match' => 'Numbers Only Allowed'));
			if ($this->form_validation->run() != false){
				$contact_id = $this->input->post('contact_id');
				$find_contact = $em->getRepository('Entity\Contact')->find($contact_id); 
				if($find_contact != null){
					$data['contact_obj'] = $find_contact;
					$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
				}else{
					$this->session->set_flashdata('error', '<p>Wrong Contact ID</p>');
					redirect('index.php/common/contact_crud','refresh');
				}
			}else{
				redirect('index.php/common/contact_crud','refresh');
			}
		}else{
			$this->view_call($main_folder_name,$sub_folder_name,$file_name,$data);
		}
	}
	public function view_call($main_folder_name,$sub_folder_name,$file_name,$data=null){
		$this->load->view('header');
		$this->load->view($main_folder_name.'/'.$sub_folder_name.'/'.$file_name,$data);
		$this->load->view('footer');
	}
	
}
?>