<?php
  defined('BASEPATH') OR exit('No direct script address allowed');
  use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
  use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
  use Doctrine\ORM\Query\ResultSetMapping;
  require_once 'ssp.customized.class.php';
  class Payout extends CI_Controller {

    protected static $cnt=0;
    protected static $ar=array();
    protected static $obj_ar=array();

    protected static $hunbvi_left=0;
    protected static $hunbvi_right=0;


    public function __construct(){
      parent::__construct();
      is_user_logged_in();
    }
    public function index(){
      $em = $this->doctrine->em;
      $utill_obj = new Common_utill();
      $final_obj_ar = array();
      $db_obj_ar = array();
      $a = $utill_obj->get_all_up_ids();
      $pck_ar = $utill_obj->get_all_active_packages_promary_key();
      rsort($pck_ar);
      for($j=0;$j<count($a);$j++){
        $user_purchase_id = $a[$j];
        // $user_purchase_id = 1;
        $exist = $utill_obj->get_single_row_using_array_value('User_package_purchase','user_purchase_id',$user_purchase_id);
        if($exist->getPairAchieve() == 1){
          $obj = $this->get_tree_obj($user_purchase_id);
          for($i=0;$i<count($pck_ar);$i++){
            // $pck_ar[$i] = 18;$pck_ar[$i]
            // $pck_ar[$i] = 18;

            self::$hunbvi_left=0;
            self::$hunbvi_right=0;

            $pack = $utill_obj->get_single_row_using_primary_key('Packages',$pck_ar[$i]);
                
            $cnt = $this->get_left_right_package_count($obj,$pck_ar[$i]);//$pck_ar[$i]
            //echo"<pre>";echo $pck_ar[$i];echo"xx";echo $user_purchase_id;echo"yy";print_r($cnt);echo"</pre>";
            if($exist->getPayoutReleased() == 0){
              if($obj->getPairAchieveSide() == 0){
                if($cnt['cnt_left'] != 0){
                  $cnt['cnt_left'] = $cnt['cnt_left']-1;
                }
              }else{
                if($cnt['cnt_right'] != 0){
                  $cnt['cnt_right'] = $cnt['cnt_right']-1;
                }
              }
            }

            $res = $em->getRepository('Entity\Payout')->findOneBy(array('user_purchase_id' => $user_purchase_id,'package_id'=>$pck_ar[$i]));

            $carry_forward_sale = 0;
            $carry_forward_sale_side = 0;
            if($res != null && $obj->getPackageId() == $pck_ar[$i]){
              $carry_forward_sale = $res->getCarryForwardSale();
              $carry_forward_sale_side = $res->getCarryForwardSaleSide();
            }

            $final_obj = new Entity\Payout_details_view();
            $db_obj = new Entity\Payout();
            $f_obj = new Entity\Final_payout();

            $final_obj->setPackageName($pack->getPackageName());
            $final_obj->setPersonalSalesCount(0);
            $final_obj->setPersonalSalesSide(0);

            $f_obj->setPackageId($pack->getPackageId());
            $f_obj->setPackageName($pack->getPackageName());
            $f_obj->setUserPurchaseId($user_purchase_id);
            $f_obj->setPersonalSalesCount(0);
            $f_obj->setPersonalSalesSide(0);

            $db_obj->setUserPurchaseId($user_purchase_id);
            $db_obj->setPackageId($utill_obj->get_reference_obj('Packages',$pack->getPackageId()));
            $db_obj->setPersonalLeft(0);
            $db_obj->setPersonalRight(0);

           
            $tot_cutoff_right = 0;
            $tot_cutoff_left = 0;

            $fin_res = $em->getRepository('Entity\Final_payout')->findOneBy(array('user_purchase_id' => $user_purchase_id,'package_id'=>$pack->getPackageId()));
            $exist_carry_left = 0;
            $exist_carry_right = 0;
            $exist_carry_pv = 0;
            $exist_carry_pv_side = 0;
            if($fin_res != null){
              $exist_carry_left = $fin_res->getCarryLeft();
              $exist_carry_right = $fin_res->getCarryRight();

              $exist_carry_pv = $fin_res->getCarryPv();
              $exist_carry_pv_side = $fin_res->getCarryPvSide();
            }

            $final_obj->setPreviousLeft($exist_carry_left);
            $final_obj->setPreviousRight($exist_carry_right);

            $f_obj->setPreviousLeft($exist_carry_left);
            $f_obj->setPreviousRight($exist_carry_right);

            $tot_cutoff_right = $exist_carry_right+$cnt['cnt_right'];
            $tot_cutoff_left = $exist_carry_left+$cnt['cnt_left'];


            // if($carry_forward_sale_side == 1){
            //   $final_obj->setPreviousLeft(0);
            //   $final_obj->setPreviousRight($carry_forward_sale);

            //   $f_obj->setPreviousLeft(0);
            //   $f_obj->setPreviousRight($carry_forward_sale);

            //   $tot_cutoff_right = $carry_forward_sale+$cnt['cnt_right'];
            //   $tot_cutoff_left = $cnt['cnt_left'];
            // }else{
            //   $final_obj->setPreviousRight(0);
            //   $final_obj->setPreviousLeft($carry_forward_sale);

            //   $f_obj->setPreviousRight(0);
            //   $f_obj->setPreviousLeft($carry_forward_sale);

            //   $tot_cutoff_left = $carry_forward_sale+$cnt['cnt_left'];
            //   $tot_cutoff_right = $cnt['cnt_right'];
            // }

            $final_obj->setCurrentLeft($cnt['cnt_left']);
            $final_obj->setCurrentRight($cnt['cnt_right']);
            $final_obj->setTotalLeft($tot_cutoff_left);
            $final_obj->setTotalRight($tot_cutoff_right);

            $f_obj->setCurrentLeft($cnt['cnt_left']);
            $f_obj->setCurrentRight($cnt['cnt_right']);
            $f_obj->setTotalLeft($tot_cutoff_left);
            $f_obj->setTotalRight($tot_cutoff_right);

            $db_obj->setCurrentLeft($cnt['cnt_left']);
            $db_obj->setCurrentRight($cnt['cnt_right']);
            $db_obj->setTotalLeft($tot_cutoff_left);
            $db_obj->setTotalRight($tot_cutoff_right);
            $db_obj->onPrePersist();
            
            $carry_left = 0;
            $carry_right = 0;
            $pair_details_total = 0;
            $pair_details_flushed = 0;

            $limit_per_cut_off = $pack->getPackageLimitedPairCutoff();
            $package_pv = $pack->getPackagePointValue();

            if($tot_cutoff_left > 0 || $tot_cutoff_right > 0){
              if($tot_cutoff_left >= $limit_per_cut_off && $tot_cutoff_right >= $limit_per_cut_off){
                if($tot_cutoff_left > $tot_cutoff_right){
                  $carry_left = $tot_cutoff_left-$tot_cutoff_right;
                  $pair_details_total = $limit_per_cut_off;
                  $pair_details_flushed = $tot_cutoff_left-$limit_per_cut_off;
                  $db_obj->setCarryForward($carry_left);
                  $db_obj->setCarryForwardSide(0);
                  $db_obj->setCarryForwardSale($tot_cutoff_left-$tot_cutoff_right);
                  $db_obj->setCarryForwardSaleSide(0);
                  
                }elseif($tot_cutoff_right > $tot_cutoff_left){
                  $carry_right = $tot_cutoff_right-$tot_cutoff_left;
                  $pair_details_total = $limit_per_cut_off;
                  $pair_details_flushed = $tot_cutoff_right-$limit_per_cut_off;
                  $db_obj->setCarryForward($carry_right);
                  $db_obj->setCarryForwardSide(1);
                  $db_obj->setCarryForwardSale($tot_cutoff_right-$tot_cutoff_left);
                  $db_obj->setCarryForwardSaleSide(1);
                  // $db_obj->setCarryForwardPointValue(($tot_cutoff_right-$tot_cutoff_left)*$package_pv);
                  // $db_obj->setCarryForwardPointValueSide(1);
                }else{
                  $pair_details_total = $limit_per_cut_off;
                  $db_obj->setCarryForward(0);
                  $db_obj->setCarryForwardSide(0);
                  $db_obj->setCarryForwardSale(0);
                  $db_obj->setCarryForwardSaleSide(0);
                  // $db_obj->setCarryForwardPointValue(0);
                  // $db_obj->setCarryForwardPointValueSide(0);
                }
                $db_obj->setCarryForwardPointValue($exist_carry_pv);
                $f_obj->setCarryPv($exist_carry_pv);
                if($exist_carry_pv_side = 0){
                  $db_obj->setCarryForwardPointValueSide(0);
                  $f_obj->setCarryPvSide(0);
                }else{
                  $db_obj->setCarryForwardPointValueSide(1);
                  $f_obj->setCarryPvSide(1);
                }
              }else{
                if($tot_cutoff_left > $tot_cutoff_right){
                  $pair_details_total = $tot_cutoff_right;
                }elseif($tot_cutoff_right > $tot_cutoff_left){
                  $pair_details_total = $tot_cutoff_left;
                }else{
                  $pair_details_total = $tot_cutoff_left;
                }
                $db_obj->setCarryForward(0);
                $db_obj->setCarryForwardSide(0);

                $f_obj->setCarryPv($exist_carry_pv);
                $f_obj->setCarryPvSide($exist_carry_pv_side);

                $db_obj->setCarryForwardSale(0);
                $db_obj->setCarryForwardSaleSide(0);
                if($tot_cutoff_left > $tot_cutoff_right){
                  if($exist_carry_pv_side == 0){
                    $db_obj->setCarryForwardPointValue((($tot_cutoff_left-$tot_cutoff_right)*$package_pv)+$exist_carry_pv);
                    $db_obj->setCarryForwardPointValueSide(0);

                    $f_obj->setCarryPv((($tot_cutoff_left-$tot_cutoff_right)*$package_pv)+$exist_carry_pv);
                    $f_obj->setCarryPvSide(0);
                  }else{
                    $db_obj->setCarryForwardPointValue(($tot_cutoff_left-$tot_cutoff_right)*$package_pv);
                    $db_obj->setCarryForwardPointValueSide(0);

                    $f_obj->setCarryPv(($tot_cutoff_left-$tot_cutoff_right)*$package_pv);
                    $f_obj->setCarryPvSide(0);
                  }
                  
                }else{
                  if($exist_carry_pv_side == 1){
                    $db_obj->setCarryForwardPointValue((($tot_cutoff_right-$tot_cutoff_left)*$package_pv)+$exist_carry_pv);
                    $db_obj->setCarryForwardPointValueSide(1);

                    $f_obj->setCarryPv((($tot_cutoff_right-$tot_cutoff_left)*$package_pv)+$exist_carry_pv);
                    $f_obj->setCarryPvSide(1);
                  }else{
                    $db_obj->setCarryForwardPointValue(($tot_cutoff_right-$tot_cutoff_left)*$package_pv);
                    $db_obj->setCarryForwardPointValueSide(1);

                    $f_obj->setCarryPv(($tot_cutoff_right-$tot_cutoff_left)*$package_pv);
                    $f_obj->setCarryPvSide(1);
                  }
                  
                }
              }
            }else{
              $db_obj->setCarryForward(0);
              $db_obj->setCarryForwardSide(0);
              $db_obj->setCarryForwardPointValue($exist_carry_pv);
              $f_obj->setCarryPv($exist_carry_pv);
              if($exist_carry_pv_side = 0){
                $db_obj->setCarryForwardPointValueSide(0);
                $f_obj->setCarryPvSide(0);
              }else{
                $db_obj->setCarryForwardPointValueSide(1);
                $f_obj->setCarryPvSide(1);
              }
              $db_obj->setCarryForwardSale(0);
              $db_obj->setCarryForwardSaleSide(0);
            }

            $final_obj->setCarryLeft($carry_left);
            $final_obj->setCarryRight($carry_right);
            $final_obj->setPairDetailsTotal($pair_details_total);

            if($exist_carry_pv_side = 0){
              $final_obj->setTotalPvLeft(($pair_details_total*$package_pv)+$exist_carry_pv);
              $f_obj->setTotalPvLeft(($pair_details_total*$package_pv)+$exist_carry_pv);
            }else{
              $final_obj->setTotalPvLeft($pair_details_total*$package_pv);
              $f_obj->setTotalPvLeft($pair_details_total*$package_pv);
            }
            if($exist_carry_pv_side = 1){
              $final_obj->setTotalPvRight(($pair_details_total*$package_pv)+$exist_carry_pv);
              $f_obj->setTotalPvRight(($pair_details_total*$package_pv)+$exist_carry_pv);
            }else{
              $final_obj->setTotalPvRight($pair_details_total*$package_pv);
              $f_obj->setTotalPvRight($pair_details_total*$package_pv);
            }

            $final_obj->setPairDetailsFlushed($pair_details_flushed);

            $f_obj->setCarryLeft($carry_left);
            $f_obj->setCarryRight($carry_right);
            $f_obj->setPairTotal($pair_details_total);
            
            $f_obj->setFlushed($pair_details_flushed);

            if($fin_res != null){
              $em->remove($fin_res);
              $em->flush();
            }

            $em->persist($f_obj);
            $em->flush();

            array_push($final_obj_ar, $final_obj);
            array_push($db_obj_ar, $db_obj);
          }
          foreach ($db_obj_ar as $db) {
            $em->persist($db);
            $em->flush();  
          }
          
          if($exist != null){
            $exist->setPayoutReleased(1);
            $em->persist($exist);
            $em->flush();
          }

        }
      }
          echo"<pre>";
          print_r($final_obj_ar);
          echo"</pre>";

          echo"--------------------------------------------------------------";

          echo"<pre>";
          print_r($db_obj_ar);
          echo"</pre>";
          // exit();
      // echo"Hai";exit();
      echo"Payout Success";
    }
    public function get_left_right_package_count($obj,$id){//print_r($obj);exit();
      $now_time = new \DateTime("now");
      if(!is_numeric($obj->getLeftObj())){
        if($obj->getPayoutReleased() == 1){
          if($now_time->format('H') >= 13){
              $dt = $obj->getLeftObj()->getCreatedAt()->format('Y-m-d H:i:s');
              $check = $this->second_cutoff($dt);
              if($check == 1){
                if($obj->getLeftObj()->getPackageId() == $id ){
                  self::$hunbvi_left=1;
                }
              }
          }else{
            $dt = $obj->getLeftObj()->getCreatedAt()->format('Y-m-d H:i:s');
              $check = $this->first_cutoff($dt);
              if($check == 1){
                if($obj->getLeftObj()->getPackageId() == $id ){
                  self::$hunbvi_left=1;
                }
              }
          }
        }else{
          if($obj->getLeftObj()->getPackageId() == $id ){
            self::$hunbvi_left=1;
          }
        }
        $this->printTreeL($obj->getLeftObj(),$id,$obj);
      }
      if(!is_numeric($obj->getRightObj())){
        if($obj->getPayoutReleased() == 1){
          if($now_time->format('H') >= 13){
              $dt = $obj->getRightObj()->getCreatedAt()->format('Y-m-d H:i:s');
              $check = $this->second_cutoff($dt);
              if($check == 1){
                if($obj->getRightObj()->getPackageId() == $id ){
                  self::$hunbvi_right=1;
                }
              }
          }else{
            $dt = $obj->getRightObj()->getCreatedAt()->format('Y-m-d H:i:s');
              $check = $this->first_cutoff($dt);
              if($check == 1){
                if($obj->getRightObj()->getPackageId() == $id ){
                  self::$hunbvi_right=1;
                }
              }
          }
        }else{
          if($obj->getRightObj()->getPackageId() == $id ){
            self::$hunbvi_right=1;
          }
        }
        $this->printTreeR($obj->getRightObj(),$id,$obj);
      }
      return ['cnt_left'=>self::$hunbvi_left,'cnt_right'=>self::$hunbvi_right];
    }
    public function printTreeL($obj,$id,$robj){
      $now_time = new \DateTime("now");
      // echo $obj->getUserId();echo"<br>";
      if(!is_numeric($obj->getLeftObj())){
        if($robj->getPayoutReleased() == 1){
          if($now_time->format('H') >= 13){
              $dt = $obj->getLeftObj()->getCreatedAt()->format('Y-m-d H:i:s');
              $check = $this->second_cutoff($dt);
              if($check == 1){
                if($obj->getLeftObj()->getPackageId() == $id ){
                  self::$hunbvi_left++;
                }
              }
          }else{
            $dt = $obj->getLeftObj()->getCreatedAt()->format('Y-m-d H:i:s');
              $check = $this->first_cutoff($dt);
              if($check == 1){
                if($obj->getLeftObj()->getPackageId() == $id ){
                  self::$hunbvi_left++;
                }
              }
          }
        }else{
          if($obj->getLeftObj()->getPackageId() == $id ){
            self::$hunbvi_left++;
          }
        }
        $this->printTreeL($obj->getLeftObj(),$id,$robj);
      }
      if(!is_numeric($obj->getRightObj())){
        if($robj->getPayoutReleased() == 1){
          if($now_time->format('H') >= 13){
              $dt = $obj->getRightObj()->getCreatedAt()->format('Y-m-d H:i:s');
              $check = $this->second_cutoff($dt);
              if($check == 1){
                if($obj->getRightObj()->getPackageId() == $id ){
                  self::$hunbvi_left++;
                }
              }
          }else{
            $dt = $obj->getRightObj()->getCreatedAt()->format('Y-m-d H:i:s');
              $check = $this->first_cutoff($dt);
              if($check == 1){
                if($obj->getRightObj()->getPackageId() == $id ){
                  self::$hunbvi_left++;
                }
              }
          }
        }else{
          if($obj->getRightObj()->getPackageId() == $id ){
            self::$hunbvi_left++;
          }
        }
        $this->printTreeL($obj->getRightObj(),$id,$robj);
      }
    }
    public function printTreeR($obj,$id,$robj){
      $now_time = new \DateTime("now");
       //echo $obj->getUserId();echo"<br>";
      // echo $robj->getUserId();echo"<br>";
      if(!is_numeric($obj->getLeftObj())){
        if($robj->getPayoutReleased() == 1){
          if($now_time->format('H') >= 13){
              $dt = $obj->getLeftObj()->getCreatedAt()->format('Y-m-d H:i:s');
              $check = $this->second_cutoff($dt);
              if($check == 1){
                if($obj->getLeftObj()->getPackageId() == $id ){
                  self::$hunbvi_right++;
                }
              }
          }else{
            $dt = $obj->getLeftObj()->getCreatedAt()->format('Y-m-d H:i:s');
              $check = $this->first_cutoff($dt);
              if($check == 1){
                if($obj->getLeftObj()->getPackageId() == $id ){
                  self::$hunbvi_right++;
                }
              }
          }
        }else{
          if($obj->getLeftObj()->getPackageId() == $id ){
            self::$hunbvi_right++;
          }
        }
        $this->printTreeR($obj->getLeftObj(),$id,$robj);
      }
      if(!is_numeric($obj->getRightObj())){
        if($robj->getPayoutReleased() == 1){
          if($now_time->format('H') >= 13){
              $dt = $obj->getRightObj()->getCreatedAt()->format('Y-m-d H:i:s');
              $check = $this->second_cutoff($dt);
              if($check == 1){
                if($obj->getRightObj()->getPackageId() == $id ){
                  self::$hunbvi_right++;
                }
              }
          }else{
            $dt = $obj->getRightObj()->getCreatedAt()->format('Y-m-d H:i:s');
              $check = $this->first_cutoff($dt);
              if($check == 1){
                if($obj->getRightObj()->getPackageId() == $id ){
                  self::$hunbvi_right++;
                }
              }
          }
        }else{
          if($obj->getRightObj()->getPackageId() == $id ){
              self::$hunbvi_right++;
            }
        }
        $this->printTreeR($obj->getRightObj(),$id,$robj);
      }
    }
    public function dt(){
      $d = "2017-10-05 02:01:00";
      $this->time_check($d);
    }
    public function first_cutoff($dt){
      // Timing Same Day 02:00 To Same Day 14:00
      $now_time = new \DateTime("now");
      $first_time = $now_time->format('Y-m-d H:i:s');
      // echo $first_time;echo"&nbsp;&nbsp;&nbsp;";
      // echo $dt;echo"<br><br>";
      // exit();
      $first_time = new DateTime($first_time);
      $common_time = new DateTime($dt);
      $first_interval = $common_time->diff($first_time);
      if($first_interval->d == 0 && $first_interval->y == 0 && $first_interval->m == 0){
        if($first_interval->h < 12){
          if($first_interval->i <= 59 && $first_interval->s <= 59){
            return 1;
          }else{return fasle;}
        }else{return false;}
      }else{ return false; }
    }

    public function first_cutoffx($from,$to){
      $start = new DateTime($from);
      $end = new DateTime($to);
      $first_interval = $start->diff($end);
      if($first_interval->d == 0 && $first_interval->y == 0 && $first_interval->m == 0){
        if($first_interval->h < 12){
          if($first_interval->i <= 59 && $first_interval->s <= 59){
            return 1;
          }else{return fasle;}
        }else{return false;}
      }else{ return false; }
    }

    public function second_cutoff($dt){
      // Timing Same Day 14:01 To Next Day 01:59
      $now_time = new \DateTime("now");
      // $second_time = $now_time->format('Y-m-d 02:00:00');
      $second_time = $now_time->format('Y-m-d H:i:s');
      // echo"---------------------------------------";echo"<br>";
      // echo $second_time;echo"&nbsp;&nbsp;&nbsp;";
      // echo $dt;echo"<br>";
      $second_time = new DateTime($second_time);
      //$second_time = $second_time->modify('+1 day');
      // print_r($second_time);
      $common_time = new DateTime($dt);
      $first_interval = $common_time->diff($second_time);
      // echo"<pre>";print_r($first_interval);echo"</pre>";
      // echo $first_interval->d;echo"||";echo $first_interval->y;echo"||";echo $first_interval->m;echo"||";
      // echo $first_interval->h; echo"||";echo $first_interval->i;echo"||";echo $first_interval->s;echo"<br>";
      // echo"---------------------------------------";echo"<br>";
      // exit();
      if($first_interval->d == 0 && $first_interval->y == 0 && $first_interval->m == 0){
        // echo"heare";
        if($first_interval->h <= 11){
          if($first_interval->i <= 59 && $first_interval->s <= 59){
            return 1;
          }else{return fasle;}
        }else{return false;}
      }else{ return false; }
    }

    public function second_cutoffx($from,$to){
      $start = new DateTime($from);
      $end = new DateTime($to);
      $first_interval = $start->diff($end);
      if($first_interval->d == 0 && $first_interval->y == 0 && $first_interval->m == 0){
        if($first_interval->h <= 11){
          if($first_interval->i <= 59 && $first_interval->s <= 59){
            return 1;
          }else{return fasle;}
        }else{return false;}
      }else{ return false; }
    }
    public function time_check($dt){
      $now_time = new \DateTime("now");
      // $first_time = $now_time->format('Y-m-d 14:00:00');
      $first_time= '2017-10-05 14:00:00';
      $first_time = new DateTime($first_time);
      $common_time = new DateTime($dt);
      $first_interval = $common_time->diff($first_time);
      // print_r($first_time) ;
      // echo"<pre>";
      // print_r($first_interval);
      // echo"</pre>";
      // print_r($common_time) ;

      if($first_interval->d == 0 && $first_interval->y == 0 && $first_interval->m == 0){
        if($first_interval->h <= 11){
          if($first_interval->i <= 59 && $first_interval->s <= 59){
            return true;
          }else{return fasle;}
        }else{return false;}
      }else{ return false; }
    }
    public function get_tree_obj($id){
      $utill_obj = new Common_utill();
      $em = $this->doctrine->em;
      $obj = 0;
     
        $res = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$id);
        $resx = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$res->getPairAchieveUserPurchaseId());
        $package_idx = 0;
        if($resx != null){

          $pinx_obj = $resx->getUserPurchasePinId();
          $packagex_obj = $pinx_obj->getPinRequestPackageId();
          if($packagex_obj != null){
            $package_idx = $packagex_obj->getPackageId();
          }else{
            $package_idx = 0;
          }
        }
        $pin_obj = $res->getUserPurchasePinId();
        $package_obj = $pin_obj->getPinRequestPackageId();
        if($package_obj != null){
          $package_id = $package_obj->getPackageId();
          $package_name = $package_obj->getPackageName();
          $package_price = $package_obj->getPackagePointValue();
          $limit_per_cut_off = $package_obj->getPackageLimitedPairCutoff();
        }else{$package_id=0;$package_name=0;$package_price=0;}

        $uid = $res->getUserId()->getUserId();
        $un = $res->getUserId()->getUserName();

        $obj = new Entity\Sample_payout();
        $obj->setPurchaseId($res->getUserPurchaseId());
        $obj->setUserName($un);
        $obj->setUserId($uid);
        $obj->setPackageId($package_id);
        $obj->setPackageName($package_name);
        $obj->setPackagePrice($package_price);
        $obj->setPackageLimitedPairCutoff($limit_per_cut_off);

        $obj->setCreatedAt($res->getCreatedAt());

        $obj->setPayoutReleased($res->getPayoutReleased());
        $obj->setPairAchieve($res->getPairAchieve());
        $obj->setPairAchieveSide($res->getPairAchieveSide());
        $obj->setPairAchieveUserPurchaseId($res->getPairAchieveUserPurchaseId());

        $obj->setPairAchievePackageId($package_idx);

        $obj->setRootId($res->getUserPurchaseActualGeneUserPurchaseId());

        if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
          $obj->setLeftObj($this->get_tree_obj($res->getUserPurchaseGeneLeftPurchaseId()));
        }else{$obj->setLeftObj(0);}
        if($res->getUserPurchaseGeneRightPurchaseId() != 0){
          $obj->setRightObj($this->get_tree_obj($res->getUserPurchaseGeneRightPurchaseId()));
        }else{$obj->setRightObj(0);}
      
      return $obj;
    }
    public function payout_view(){
      $em = $this->doctrine->em;
      $utill_obj = new Common_utill();
      $final_obj_ar = array();
      $db_obj_ar = array();
      
      $user_id = $_SESSION['user_id'];
      $up_obj = $utill_obj->get_single_row_using_array_value('User_package_purchase','user_id',$user_id);
      
      $user_purchase_id = $up_obj->getUserPurchaseId();

      $fin_res = $em->getRepository('Entity\Final_payout')->findBy(array('user_purchase_id' => $user_purchase_id));
      $data['final_obj_ar'] = $fin_res;
      
      $this->load->view('header');
      $this->load->view('payout/Payout',$data);
      $this->load->view('footer');
    }

  }
?>