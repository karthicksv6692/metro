<?php
defined('BASEPATH') OR exit('No direct script address allowed');
  use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
  use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
  use Doctrine\ORM\Query\ResultSetMapping;
  require_once 'ssp.customized.class.php';
  class Auto_payout_second extends CI_Controller {

    protected static $cnt=0;
    protected static $ar=array();
    protected static $obj_ar=array();

    protected static $hunbvi_left=0;
    protected static $hunbvi_right=0;

    protected static $tree_var=0;

    protected static $tree_left_var=array();
    protected static $tree_right_var=array();
    protected static $tree_up_var_left = array();
    protected static $tree_up_var_right = array();
    protected static $ft_rt = 0;
    protected static $ft_lt = 0;


    public function __construct(){
      parent::__construct();
      //is_user_logged_in();
    }
    public function index(){
        $start = date("Y-m-d").' 12:00:00';
        $end = date("Y-m-d").' 23:59:59';
        $em = $this->doctrine->em;
        $start = $this->input->post('start');
        $end = $this->input->post('end');
        $em->getConnection()->beginTransaction();
        $this->optimize_user_active();
        $ret = $this->payout_work($start,$end);
        if($ret == 1){
          $em->getConnection()->commit();
          $up_d = date("Y-m-d").' 23:59:59';
          $d = date("Y-m-d").' 00:%';
          $qb = $em->createQueryBuilder();
          $q = $qb->update('Entity\Payout', 'u')
                  ->set('u.created_at',"'".$up_d."'")
                  ->where("u.created_at LIKE '".$d."'")
                  ->getQuery();
          $p = $q->execute();

          $qx = $qb->update('Entity\Payout_result', 'u')
                  ->set('u.created_at',"'".$up_d."'")
                  ->where("u.created_at LIKE '".$d."'")
                  ->getQuery();
          $px = $qx->execute();

          $utill_obj = new Common_utill();
          $query = $em->createQuery("SELECT b.payout_result_user_id,b.net_payout FROM Entity\Payout_result as b WHERE b.created_at LIKE '".$up_d."' AND b.net_payout > 0");
          $res = $query->getResult();
          if($res != null && count($res) > 0){
            for($i=0;$i<count($res);$i++){
              $up_obj = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$res[$i]['payout_result_user_id']);
              if($up_obj != null){
                $mobile = $up_obj->getUserId()->getContactId()->getContactMobilePrimary();
                $first_name = $up_obj->getUserId()->getFirstName();
                $last_name = $up_obj->getUserId()->getLastName();
                if($mobile != null){
                  // MEssage Start
                    // $sent_message = "Dear ".$first_name.' '.$last_name.', Your Latest Net Payout Rs:'.$res[$i]['net_payout'].'. Last Cutoff DateTime : '.$up_d;
                    // $mobile_number = $mobile;
                    // $var_message = urlencode($sent_message);
                    // $var_destination = urlencode($mobile_number);
                    // $var_message_type = 2;
                    // $var_senderid = urlencode('METROK');
                    // $ch = curl_init();
                    // curl_setopt($ch, CURLOPT_URL, "http://sms.dhinatechnologies.com/api/sms_api.php?username=metrokings&api_password=swyvpq558x0&message=$var_message&destination=$var_destination&type=$var_message_type&sender=$var_senderid");
                    // curl_setopt($ch, CURLOPT_HEADER, 0);  
                    // curl_exec($ch);
                    // curl_close($ch);
                  // MEssage Start
                }
              }
            }
          }
          $qry = $em->createQuery("UPDATE Entity\User_package_purchase as u SET u.payout_ach=0 WHERE u.user_purchase_id>0");
          $res = $qry->getResult();
          echo"success";
        }else{
          $em->getConnection()->rollBack();
        }
    }
    public function payout_work($start,$end){
      $em = $this->doctrine->em;
      $new_time = $end;
      $dt = new DateTime($new_time);
      $utill_obj = new Common_utill();
      $db_obj_ar = array();
      $a = $utill_obj->get_all_up_ids();
      if(count($a)>0){
        $pck_ar = $utill_obj->get_all_active_packages_promary_key();
        rsort($pck_ar);
        $time_start = microtime(true);
        $exist_cf_amount_sidex = 0;
        for($j=0;$j<count($a);$j++){
          $full_total_left = 0;
          $full_total_right = 0;
          self::$ft_rt = 0;
          self::$ft_lt = 0;
          $user_purchase_id = $a[$j];
          $rep_cnt = 0;
          $pan_status = 0;
           // $user_purchase_id = 8; //demo
          $exist = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$user_purchase_id);
          if($exist->getUserId()->getIsActive() == 1){
            if($exist->getUserPurchasePinId()->getPinRequestPackageId()->getPackageId() == RT::$default_ec_pack_id){
              $repurchase_pin_used_user_id = $exist->getUserId()->getUserId();

              $created_dt_objy = $exist->getUserId()->getCreatedAt();
              $created_aty = $created_dt_objy->format('Y-m-d');
              $nowxy = date("Y-m-d");
              $date1=date_create($nowxy);
              $date2=date_create($created_aty);
              $diff=date_diff($date1,$date2);

              if($diff->days <= 30){
                $rep_cnt = 1;
              }else{
                $rep_cnt = $exist->getRepCount();
              }

            }else{
              $rep_cnt = 1;
            }

            $created_dt_obj = $exist->getUserId()->getCreatedAt();
            $created_at = $created_dt_obj->format('Y-m-d');
            $nowx = date("Y-m-d");
            $dd1=date_create($nowx);
            $dd2=date_create($created_at);
            $diffx=date_diff($dd1,$dd2);

            if($diffx->days <= 120){
              $pan_status = 1;
            }else{
              $pan_status = 0;
            }

            // echo $exist->getPairAchieve();exit();
            if($exist->getPairAchieve() == 1){
              $tree_var = 0;

              self::$tree_left_var=array();
              self::$tree_right_var=array();
              self::$tree_up_var_left=array();
              self::$tree_up_var_right=array();
              $obj = $this->get_tree_obj($user_purchase_id,$end);

              for($i=0;$i<count($pck_ar);$i++){
                self::$hunbvi_left=0;
                self::$hunbvi_right=0;
                $pack = $utill_obj->get_single_row_using_primary_key('Packages',$pck_ar[$i]);

                if(!is_integer($obj->getLeftObj())){
                  if(count($obj->getLeftObj() > 0)){
                   $left_array  =  $obj->getLeftObj();
                   $counts = array_count_values($left_array);
                   if(array_key_exists($pck_ar[$i],$counts)){
                    $cnt['cnt_left'] = $counts[$pck_ar[$i]];
                    $pair_ach_pck_id = 0;
                    if($obj->getPairAchievePackageId() != 0 ||$obj->getPairAchievePackageId() != null ){
                      $pair_ach_pck_id = $obj->getPairAchievePackageId();
                      if(in_array($obj->getPairAchieveUserPurchaseId(), self::$tree_up_var_left)){
                        if(self::$ft_lt == 0){
                          $cnt['cnt_left'] = $cnt['cnt_left']-1;
                        }
                        self::$ft_lt++;
                      }
                    }
                   }else{$cnt['cnt_left'] = 0;}
                  }else{ $cnt['cnt_left'] = 0; }
                }else{ $cnt['cnt_left'] = 0; }

                if(!is_integer($obj->getRightObj())){
                  if(count($obj->getRightObj() > 0)){
                   $right_array  =  $obj->getRightObj();
                   $counts = array_count_values($right_array);
                   if(in_array($pck_ar[$i], $right_array)){
                    $cnt['cnt_right'] = $counts[$pck_ar[$i]];
                    $pair_ach_pck_id = 0;
                    if($obj->getPairAchievePackageId() != 0 ||$obj->getPairAchievePackageId() != null ){
                      $pair_ach_pck_id = $obj->getPairAchievePackageId();
                      if(in_array($obj->getPairAchieveUserPurchaseId(), self::$tree_up_var_right)){
                        if(self::$ft_rt == 0){
                          $cnt['cnt_right'] = $cnt['cnt_right']-1;
                        }
                        self::$ft_rt++;
                      }
                    }
                   }else{$cnt['cnt_right'] = 0;}
                  }else{ $cnt['cnt_right'] = 0; }
                }else{ $cnt['cnt_right'] = 0; }

                $res = $em->getRepository('Entity\Payout')->findOneBy(array('user_purchase_id' => $user_purchase_id,'package_id'=>$pck_ar[$i]));

                $db_obj = new Entity\Payout();
                $db_res_obj = new Entity\Payout_result();

                $db_obj->setUserPurchaseId($user_purchase_id);
                $db_obj->setPackageId($utill_obj->get_reference_obj('Packages',$pack->getPackageId()));
                $db_obj->setPersonalLeft(0);
                $db_obj->setPersonalRight(0);
               
                $tot_cutoff_right = 0;
                $tot_cutoff_left = 0;

                $query = $em->createQuery("SELECT MAX(b.payout_id) FROM Entity\Payout as b WHERE b.package_id=".$pack->getPackageId()." AND b.user_purchase_id=".$user_purchase_id);
                $single_res = $query->getResult();

                $exist_carry_left = 0;
                $exist_carry_right = 0;

                $exist_cf_amount = 0;
                $exist_cf_amount_side = 0;

                if($single_res[0][1] != null){
                  $max_payout_id = $single_res[0][1];
                  $fin_res = $em->getRepository('Entity\Payout')->find($max_payout_id);
                  if($fin_res != null){
                    if($fin_res->getCarryForwardSide() == 0){
                      $exist_carry_left = $fin_res->getCarryForward();
                      $exist_cf_amount_side = 0;
                    }else{
                      $exist_carry_right = $fin_res->getCarryForward();
                      $exist_cf_amount_side = 1;
                    }

                    $qry = $em->createQuery("SELECT MAX(b.payout_result_id) FROM Entity\Payout_result as b WHERE b.payout_result_user_id=".$user_purchase_id);
                    $s_res = $qry->getResult();
                    // print_r($s_res);exit(); 
                    if($s_res[0][1] != null){
                      $max_payout_res_id = $s_res[0][1];
                      $pay_resx = $em->getRepository('Entity\Payout_result')->find($max_payout_res_id);
                      if($pay_resx != null){
                        $exist_cf_amount = $pay_resx->getCarryForwardPv();
                        $exist_cf_amount_sidex = $pay_resx->getCarryForwardPvSide();
                      }
                    }
                  }
                }
                
                $tot_cutoff_right = $exist_carry_right+$cnt['cnt_right'];
                $tot_cutoff_left = $exist_carry_left+$cnt['cnt_left'];

                $db_obj->setCurrentLeft($cnt['cnt_left']);
                $db_obj->setCurrentRight($cnt['cnt_right']);
                
                $db_obj->setPreviousLeft($exist_carry_left);
                $db_obj->setPreviousRight($exist_carry_right);

                $db_obj->setTotalLeft($tot_cutoff_left);
                $db_obj->setTotalRight($tot_cutoff_right);
                $db_obj->onPrePersist($new_time);
                
                $carry_left = 0;
                $carry_right = 0;
                $pair_details_total = 0;
                $pair_details_flushed = 0;

                $limit_per_cut_off = $pack->getPackageLimitedPairCutoff();
                // $limit_per_cut_off = 10; demo

                $package_pv = $pack->getPackagePointValue();

                if($tot_cutoff_left > 0 || $tot_cutoff_right > 0){
                  if($tot_cutoff_left >= $limit_per_cut_off && $tot_cutoff_right >= $limit_per_cut_off){
                    if($tot_cutoff_left > $tot_cutoff_right){
                      $carry_left = $tot_cutoff_left-$tot_cutoff_right;
                      $pair_details_total = $limit_per_cut_off;
                      $pair_details_flushed = $tot_cutoff_right-$limit_per_cut_off;
                      $db_obj->setCarryForward($carry_left);
                      $db_obj->setCarryForwardSide(0);

                      $db_obj->setPairDetailsTotal($pair_details_total);
                      $db_obj->setPairDetailsFlushed($pair_details_flushed);
                    }elseif($tot_cutoff_right > $tot_cutoff_left){
                      $carry_right = $tot_cutoff_right-$tot_cutoff_left;
                      $pair_details_total = $limit_per_cut_off;
                      $pair_details_flushed = $tot_cutoff_left-$limit_per_cut_off;
                      $db_obj->setCarryForward($carry_right);
                      $db_obj->setCarryForwardSide(1);

                      $db_obj->setPairDetailsTotal($pair_details_total);
                      $db_obj->setPairDetailsFlushed($pair_details_flushed);
                    }else{
                      $pair_details_total = $limit_per_cut_off;
                      $db_obj->setCarryForward(0);
                      $db_obj->setCarryForwardSide(0);
                      $db_obj->setPairDetailsTotal(0);
                      $db_obj->setPairDetailsFlushed(0);
                    }

                    $db_obj->setTotalPvLeft($limit_per_cut_off*$package_pv);
                    $db_obj->setTotalPvRight($limit_per_cut_off*$package_pv);

                    $full_total_left += $limit_per_cut_off*$package_pv;
                    $full_total_right += $limit_per_cut_off*$package_pv;
                    
                  }else{
                    if($tot_cutoff_left >= $limit_per_cut_off){
                      $current_cf = $tot_cutoff_left-$limit_per_cut_off;
                      $current_cf_side = 0;
                      $current_cf_left_pv = $limit_per_cut_off*$package_pv;
                      $current_cf_right_pv = $tot_cutoff_right*$package_pv;

                      $db_obj->setCarryForward($current_cf);
                      $db_obj->setCarryForwardSide($current_cf_side);

                      $db_obj->setPairDetailsTotal(0);
                      $db_obj->setPairDetailsFlushed(0);

                      $db_obj->setTotalPvLeft($limit_per_cut_off*$package_pv);
                      $db_obj->setTotalPvRight($tot_cutoff_right*$package_pv);

                      $full_total_left += $limit_per_cut_off*$package_pv;
                      $full_total_right += $tot_cutoff_right*$package_pv;
                      
                    }elseif($tot_cutoff_right >= $limit_per_cut_off){
                      $current_cf = $tot_cutoff_right-$limit_per_cut_off;

                      $current_cf_side = 1;
                      $current_cf_left_pv = $tot_cutoff_left*$package_pv;
                      $current_cf_right_pv = $limit_per_cut_off*$package_pv;

                      $db_obj->setCarryForward($current_cf);
                      $db_obj->setCarryForwardSide($current_cf_side);

                      $db_obj->setPairDetailsTotal(0);
                      $db_obj->setPairDetailsFlushed(0);

                      $db_obj->setTotalPvLeft($tot_cutoff_left*$package_pv);
                      $db_obj->setTotalPvRight($limit_per_cut_off*$package_pv);

                      $full_total_left += $tot_cutoff_left*$package_pv;
                      $full_total_right += $limit_per_cut_off*$package_pv;

                    }else{
                      $current_cf = 0;
                      $current_cf_left_pv = $tot_cutoff_left*$package_pv;
                      $current_cf_right_pv = $tot_cutoff_right*$package_pv;

                      $db_obj->setCarryForward($current_cf);
                      $db_obj->setCarryForwardSide(0);
                      $db_obj->setPairDetailsTotal(0);
                      $db_obj->setPairDetailsFlushed(0);

                      $db_obj->setTotalPvLeft($tot_cutoff_left*$package_pv);
                      $db_obj->setTotalPvRight($tot_cutoff_right*$package_pv);

                      $full_total_left += $tot_cutoff_left*$package_pv;
                      $full_total_right += $tot_cutoff_right*$package_pv;
                    }
                  }
                  $em->persist($db_obj);
                  $em->flush();
                }
                
              }// Package Array End

              if($exist_cf_amount_sidex == 1){
                $full_total_right += $exist_cf_amount;
              }else{
                $full_total_left += $exist_cf_amount;
              }

              $pan_verified = 0;$aadhar_verified = 0;$pass_verified = 0;
              $tot_bal = 0;$p_sts = 0;

              $pan_verified = $exist->getUserId()->getPanVerified();
              $aadhar_verified = $exist->getUserId()->getAadharVerified();
              $pass_verified = $exist->getUserId()->getPassVerified();

              $repurchase_bal = 0;
              $pay_res = new Entity\Payout_result();
              $pay_res->setPayoutResultUserId($a[$j]);
              $pay_res->setPayoutResultLeft(0);
              $pay_res->setTotalLeftPv($full_total_left);
              $pay_res->setTotalRightPv($full_total_right);
              if($full_total_left>$full_total_right){
                $pay_res->setCarryForwardPv($full_total_left-$full_total_right);
                $pay_res->setCarryForwardPvSide(0);
                $f_t = 2*$full_total_right;
                $td = 0;
                if($f_t != 0){
                  $td = 2*($f_t*5/100);//service 5% + tds 5%
                  $repurchase_bal = $f_t*10/100;//repurchase 5%
                  $td = $td+$repurchase_bal;
                }

                if($pan_verified == 1){
                    if($rep_cnt > 0){
                      $tot_bal = $f_t-$td;
                    }else{
                      $tot_bal = $f_t-$td;
                      $tot_bal = floor($tot_bal/2);
                    }
                  }else{
                    $td = $td+($f_t*15/100);
                    if($rep_cnt > 0){
                      $tot_bal = $f_t-$td;
                    }else{
                      $tot_bal = $f_t-$td;
                      $tot_bal = floor($tot_bal/2);
                    }
                  }

                if($exist != null){
                  $pay_res->setNetPayout($tot_bal);
                  $pay_res->setPaymentStatus($p_sts);
                }
              }else{
                $pay_res->setCarryForwardPv($full_total_right-$full_total_left);
                $pay_res->setCarryForwardPvSide(1);
                $f_t = $full_total_left+$full_total_left;
                $td = 0;
                if($f_t != 0){
                  $td = 2*($f_t*5/100);
                  $repurchase_bal = $f_t*10/100;
                  $td = $td+$repurchase_bal;

                  if($pan_verified == 1){
                    if($rep_cnt > 0){
                      $tot_bal = $f_t-$td;
                    }else{
                      $tot_bal = $f_t-$td;
                      $tot_bal = floor($tot_bal/2);
                    }
                  }else{
                    $td = $td+($f_t*15/100);
                    if($rep_cnt > 0){
                      $tot_bal = $f_t-$td;
                    }else{
                      $tot_bal = $f_t-$td;
                      $tot_bal = floor($tot_bal/2);
                    }
                  }
                }
                if($exist != null){
                  $pay_res->setNetPayout($tot_bal);
                  $pay_res->setPaymentStatus($p_sts);
                }
              }

              $res_up = $em->getRepository('Entity\User_package_purchase')->find($user_purchase_id);
              if($res_up != null){
                $u_obj = $res_up->getUserId();
                if($u_obj != null){
                  $u_id = $u_obj->getUserId();
                  $ua_res = $em->getRepository('Entity\User_account')->findOneBy(array('user_id'=>$u_id));
                  if($ua_res != null){
                    $prev_up_amnt = $ua_res->getUserAccountRepurchaseAmount();
                    $new_bal = $repurchase_bal+$prev_up_amnt;
                    $prev_up_amnt = $ua_res->setUserAccountRepurchaseAmount($new_bal);
                    $em->persist($ua_res);
                    $em->flush();
                  }
                }
              }

              $pay_res->setPayoutResultType(0);
              $pay_res->onPrePersist($new_time);
              $em->persist($pay_res);
              $em->flush();
               $exist = $utill_obj->get_single_row_using_array_value('User_package_purchase','user_purchase_id',$user_purchase_id);      
              if($exist != null){
                $exist->setPayoutReleased(1);
                $exist->onPreUpdate();
                $em->persist($exist);
                $em->flush();
                
              }
            }
          }
        }
        $time_end = microtime(true);
        return true;
      }else{
        return true;
      }
      
    }
    public function optimize_user_active(){
      $em = $this->doctrine->em;
      $find_usr = $em->getRepository('Entity\User')->findBy(array('is_active'=>1));
      $nowxy = date("Y-m-d");
      if($find_usr != null){
        foreach ($find_usr as $usr) {
          $created_date = $usr->getCreatedAt()->format('Y-m-d');
          $date1=date_create($nowxy);
          $date2=date_create($created_date);
          $diff=date_diff($date1,$date2);
          if($diff->days > 120){
            if($usr->getPanVerified() == 0 || $usr->getAadharVerified() == 0 || $usr->getPassVerified() == 0){
              $usr->setIsActive(0);
              $em->persist($usr);
              $em->flush();
            }
          }
        }
      }
    }
    public function get_tree_obj($id,$end){
      $utill_obj = new Common_utill();
      $em = $this->doctrine->em;
      $res = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$id);
      $resx = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$res->getPairAchieveUserPurchaseId());
      $package_idx = 0;
      if($resx != null){
        $pinx_obj = $resx->getUserPurchasePinId();
        $packagex_obj = $pinx_obj->getPinRequestPackageId();
        if($packagex_obj != null){
          $package_idx = $packagex_obj->getPackageId();
        }else{
          $package_idx = 0;
        }
      }
      $pin_obj = $res->getUserPurchasePinId();
      $package_obj = $pin_obj->getPinRequestPackageId();
      if($package_obj != null){
        $package_id = $package_obj->getPackageId();
        $package_name = $package_obj->getPackageName();
        $package_price = $package_obj->getPackagePointValue();
        $limit_per_cut_off = $package_obj->getPackageLimitedPairCutoff();
      }else{$package_id=0;$package_name=0;$package_price=0;}

      $uid = $res->getUserId()->getUserId();
      $un = $res->getUserId()->getUserName();
      $obj = new Entity\Sample_payout();
      $obj->setPurchaseId($res->getUserPurchaseId());
      $obj->setUserName($un);
      $obj->setUserId($uid);
      $obj->setPackageId($package_id);
      $obj->setPackageName($package_name);
      $obj->setPackagePrice($package_price);
      $obj->setPackageLimitedPairCutoff($limit_per_cut_off);
      $obj->setCreatedAt($res->getCreatedAt());
      $obj->setPayoutReleased($res->getPayoutReleased());
      $obj->setPairAchieve($res->getPairAchieve());
      $obj->setPairAchieveSide($res->getPairAchieveSide());
      $obj->setPairAchieveUserPurchaseId($res->getPairAchieveUserPurchaseId());
      $obj->setPairAchievePackageId($package_idx);
      $obj->setRootId($res->getUserPurchaseActualGeneUserPurchaseId());
      self::$tree_var++;
      if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
        $obj->setLeftObj($this->get_lf($res->getUserPurchaseGeneLeftPurchaseId(),$end,$res->getPayoutReleased()));
      }else{$obj->setLeftObj(0);}
      if($res->getUserPurchaseGeneRightPurchaseId() != 0){
        $obj->setRightObj($this->get_rf($res->getUserPurchaseGeneRightPurchaseId(),$end,$res->getPayoutReleased()));
      }else{$obj->setRightObj(0);}
      return $obj;
    }
    public function get_lf($id,$end,$pay){
      $utill_obj = new Common_utill();
      $em = $this->doctrine->em;
      // $obj = 0;
      $res = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$id);
      if($pay == 1){
        $start = new DateTime($end);
        $first_interval = $res->getCreatedAt()->diff($start);
        
        if($first_interval->d == 0 && $first_interval->y == 0 && $first_interval->m == 0){
          if($first_interval->h <= 11){

            if($first_interval->i <= 59 && $first_interval->s <= 59){
              // echo $id;echo"<br>";

              $res = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$id);
              $resx = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$res->getPairAchieveUserPurchaseId());
                  $package_idx = 0;
                  if($resx != null){
                    $pinx_obj = $resx->getUserPurchasePinId();
                    $packagex_obj = $pinx_obj->getPinRequestPackageId();
                    if($packagex_obj != null){
                      $package_idx = $packagex_obj->getPackageId();
                    }else{
                      $package_idx = 0;
                    }
                  }
                  $pin_obj = $res->getUserPurchasePinId();
                  $package_obj = $pin_obj->getPinRequestPackageId();
                  if($package_obj != null){
                    $package_id = $package_obj->getPackageId();
                    $package_name = $package_obj->getPackageName();
                    $package_price = $package_obj->getPackagePointValue();
                    $limit_per_cut_off = $package_obj->getPackageLimitedPairCutoff();
                  }else{$package_id=0;$package_name=0;$package_price=0;}

                  $uid = $res->getUserId()->getUserId();
                  $un = $res->getUserId()->getUserName();

                  array_push(self::$tree_left_var, $package_id);
                  array_push(self::$tree_up_var_left, $id);

                  if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
                    $this->get_lf($res->getUserPurchaseGeneLeftPurchaseId(),$end,$pay);
                  }
                  if($res->getUserPurchaseGeneRightPurchaseId() != 0){
                   $this->get_lf($res->getUserPurchaseGeneRightPurchaseId(),$end,$pay);
                  }

            }else{
              if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
                $this->get_lf($res->getUserPurchaseGeneLeftPurchaseId(),$end,$pay);
              }
              if($res->getUserPurchaseGeneRightPurchaseId() != 0){
                $this->get_lf($res->getUserPurchaseGeneRightPurchaseId(),$end,$pay);
              }
            }
          }else{
            if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
              $this->get_lf($res->getUserPurchaseGeneLeftPurchaseId(),$end,$pay);
            }
            if($res->getUserPurchaseGeneRightPurchaseId() != 0){
              $this->get_lf($res->getUserPurchaseGeneRightPurchaseId(),$end,$pay);
            }
          }
        }else{
            if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
              $this->get_lf($res->getUserPurchaseGeneLeftPurchaseId(),$end,$pay);
            }
            if($res->getUserPurchaseGeneRightPurchaseId() != 0){
              $this->get_lf($res->getUserPurchaseGeneRightPurchaseId(),$end,$pay);
            }
        }
      }else{
        $pin_obj = $res->getUserPurchasePinId();
        $package_obj = $pin_obj->getPinRequestPackageId();
        if($package_obj != null){
          $package_id = $package_obj->getPackageId();
          $package_name = $package_obj->getPackageName();
          $package_price = $package_obj->getPackagePointValue();
          $limit_per_cut_off = $package_obj->getPackageLimitedPairCutoff();
        }else{$package_id=0;$package_name=0;$package_price=0;}

        $uid = $res->getUserId()->getUserId();
        $un = $res->getUserId()->getUserName();

        array_push(self::$tree_left_var, $package_id);
        array_push(self::$tree_up_var_left, $id);

        if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
          $this->get_lf($res->getUserPurchaseGeneLeftPurchaseId(),$end,$pay);
        }
        if($res->getUserPurchaseGeneRightPurchaseId() != 0){
         $this->get_lf($res->getUserPurchaseGeneRightPurchaseId(),$end,$pay);
        }
      }

      return self::$tree_left_var;
    }
    public function get_rf($id,$end,$pay){
      $utill_obj = new Common_utill();
      $em = $this->doctrine->em;
      // $obj = 0;
      $res = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$id);
      $start = new DateTime($end);
      if($pay == 1){
        $first_interval = $res->getCreatedAt()->diff($start);
        if($first_interval->d == 0 && $first_interval->y == 0 && $first_interval->m == 0){
          if($first_interval->h <= 11){

            if($first_interval->i <= 59 && $first_interval->s <= 59){
               // echo $id;echo"---r--";echo"<br>";
              $res = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$id);
              $resx = $utill_obj->get_single_row_using_primary_key('User_package_purchase',$res->getPairAchieveUserPurchaseId());
                  $package_idx = 0;
                  if($resx != null){
                    $pinx_obj = $resx->getUserPurchasePinId();
                    $packagex_obj = $pinx_obj->getPinRequestPackageId();
                    if($packagex_obj != null){
                      $package_idx = $packagex_obj->getPackageId();
                    }else{
                      $package_idx = 0;
                    }
                  }
                  $pin_obj = $res->getUserPurchasePinId();
                  $package_obj = $pin_obj->getPinRequestPackageId();
                  if($package_obj != null){
                    $package_id = $package_obj->getPackageId();
                    $package_name = $package_obj->getPackageName();
                    $package_price = $package_obj->getPackagePointValue();
                    $limit_per_cut_off = $package_obj->getPackageLimitedPairCutoff();
                  }else{$package_id=0;$package_name=0;$package_price=0;}

                  $uid = $res->getUserId()->getUserId();
                  $un = $res->getUserId()->getUserName();

                  array_push(self::$tree_right_var, $package_id);
                  array_push(self::$tree_up_var_right, $id);

                  if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
                    $this->get_rf($res->getUserPurchaseGeneLeftPurchaseId(),$end,$pay);
                  }
                  if($res->getUserPurchaseGeneRightPurchaseId() != 0){
                    $this->get_rf($res->getUserPurchaseGeneRightPurchaseId(),$end,$pay);
                  }
                  
            }else{
              if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
                $this->get_rf($res->getUserPurchaseGeneLeftPurchaseId(),$end,$pay);
              }
              if($res->getUserPurchaseGeneRightPurchaseId() != 0){
                $this->get_rf($res->getUserPurchaseGeneRightPurchaseId(),$end,$pay);
              }
            }
          }else{
            if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
              $this->get_rf($res->getUserPurchaseGeneLeftPurchaseId(),$end,$pay);
            }
            if($res->getUserPurchaseGeneRightPurchaseId() != 0){
              $this->get_rf($res->getUserPurchaseGeneRightPurchaseId(),$end,$pay);
            }
          }
        }else{
            if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
              $this->get_rf($res->getUserPurchaseGeneLeftPurchaseId(),$end,$pay);
            }
            if($res->getUserPurchaseGeneRightPurchaseId() != 0){
              $this->get_rf($res->getUserPurchaseGeneRightPurchaseId(),$end,$pay);
            }
        }
      }else{
        $pin_obj = $res->getUserPurchasePinId();
        $package_obj = $pin_obj->getPinRequestPackageId();
        if($package_obj != null){
          $package_id = $package_obj->getPackageId();
          $package_name = $package_obj->getPackageName();
          $package_price = $package_obj->getPackagePointValue();
          $limit_per_cut_off = $package_obj->getPackageLimitedPairCutoff();
        }else{$package_id=0;$package_name=0;$package_price=0;}

        $uid = $res->getUserId()->getUserId();
        $un = $res->getUserId()->getUserName();

        array_push(self::$tree_right_var, $package_id);
        array_push(self::$tree_up_var_right, $id);

        if($res->getUserPurchaseGeneLeftPurchaseId() != 0){
          $this->get_rf($res->getUserPurchaseGeneLeftPurchaseId(),$end,$pay);
        }
        if($res->getUserPurchaseGeneRightPurchaseId() != 0){
          $this->get_rf($res->getUserPurchaseGeneRightPurchaseId(),$end,$pay);
        }
      }

      return self::$tree_right_var;
    }
  }
?>
