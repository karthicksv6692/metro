<body class="login_bgcolor">
<div class="dkbody1">
	<div class="">
		<div class="admin_login">
			<form id="admin_login_form" method="post" action="<?php echo base_url('index.php/login/do_login'); ?>">
				<div class="admin_login_div">
					<div class="text-center admin_login_logo_image">
						<img src="<?php echo base_url('assets/images/') ?>logo.png" class="img-responsive mb-3">
					</div>
				
					<div class="admin_form">
						<div class="admin_form_heading">
							<h4 class="color text-center">ADMIN LOGIN</h4>
						</div>
						<div class="admin_form_content mb-3">
							<div class="input-group">
							  	<span class="input-group-addon" id="basic-addon1"><i class="fa fa-user"></i></span>
							  	<input type="text" class="form-control user_name" placeholder="Username" aria-describedby="basic-addon1" name="user_name">
							</div>
						</div>
						<div class="admin_form_content mb-4">
							<div class="input-group">
							  	<span class="input-group-addon" id="basic-addon1"><i class="fa fa-shopping-bag"></i></span>
							  	<input type="password" class="form-control password" placeholder="Password" aria-describedby="basic-addon1" name="password">
							</div>
						</div>
						<div class="row border-bottom">
							<div class="col-md-6 pull-left mt-3 pl-0 mb-4">
                                <!-- <label class="checkbox-inline"><input type="checkbox" value="" name="genderradio"><span class="checkbox-material"><span class="check"></span> Stay With Me</span></label> -->
                                 <div class="checkboxFive"><input
                                 type="checkbox" value="1" id="checkboxFiveInput" name=""/><label for="checkboxFiveInput"></label><span class="checkbox-material ml-4"><span class="check"></span> Stay With Me</span></div>
   
                            </div>
                            <div class="col-md-6 pull-left mt-2 mb-4 pr-0 xs-pl-0">
                                <input type="hidden" name="login" value="asdf"> 
                                <button type="submit" name="login" class="login_sigin_button"> Sign In <i class="fa  fa-sign-in" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        <div class="col-md-6 pull-left mt-4 pl-0 mb-3 xs-mt-mb-login">
                        	<a href="<?php echo base_url('index.php/login/forget'); ?>"><p class="thead1">Forget Password</p></a>
                        </div>
                        <div class="col-md-6 pull-left mt-4 pl-0 pr-0 mb-3 xs-mt-mb-login">
                            <p class="thead1 text-right">Not a Member?<span class="color">Request</span></p>
                        </div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>	