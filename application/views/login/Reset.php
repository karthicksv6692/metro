<body class="login_bgcolor">
<div class="dkbody1">
	<div class="">
		<div class="admin_login">
			<form id="admin_login_form" method="post" action="<?php echo base_url('index.php/login/reset'); ?>">
				<div class="admin_login_div">
					<div class="text-center admin_login_logo_image">
						<img src="<?php echo base_url('assets/images/') ?>logo.png" class="img-responsive mb-3">
					</div>
				
					<div class="admin_form">
						<div class="admin_form_heading">
							<h4 class="color text-center">RESET PASSWORD</h4>
						</div>
						<div class="admin_form_content mb-3">
							<div class="input-group">
							  	<span class="input-group-addon" id="basic-addon1"><i class="fa fa-shopping-bag"></i></span>
							  	<input type="password" class="form-control password" placeholder="Password" aria-describedby="basic-addon1" name="password">
							</div>
						</div>
							<?php echo form_error('password','<span class="help-block form-error">', '</span>'); ?>	
						<div class="admin_form_content mb-4">
							<div class="input-group">
							  	<span class="input-group-addon" id="basic-addon1"><i class="fa fa-shopping-bag"></i></span>
							  	<input type="password" class="form-control password" placeholder="Retype-Password" aria-describedby="basic-addon1" name="repassword">
							</div>
						</div>
							<?php echo form_error('repassword','<span class="help-block form-error">', '</span>'); ?>	
						<div class="row border-bottom">
                            <div class="col-md-12 pull-left mt-2 mb-4 pr-0 xs-pl-0">
                                <button type="submit" name="login" class="login_sigin_button"> Sign In <i class="fa  fa-sign-in" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        <div class="col-md-12 pull-left mt-4 pl-0 mb-3 xs-mt-mb-login text-center">
                           	<a href="<?php echo base_url('index.php/login'); ?>"><p class="thead1 color"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back to SignIn</p></a>
                        </div>
                        <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>	