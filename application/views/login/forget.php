<body class="login_bgcolor">
<div class="dkbody1">
  <div class="">
    <div class="admin_login">
      <form id="admin_login_form" method="post" action="<?php echo base_url('index.php/login/forget'); ?>">
        <div class="admin_login_div">
          <div class="text-center admin_login_logo_image">
            <img src="<?php echo base_url('assets/images/') ?>logo.png" class="img-responsive mb-3">
          </div>
          
          <div class="text-center" style="color: red !important">
               <?php if(isset($error)){ ?> 
                <?php if($error != null){ ?>
                  <span class="help-block svk_err_cls" style="color:red;font-size:18px"><?php echo $error; ?></span>
                <?php } ?>
               <?php } ?>
            </div>
        
          <div class="admin_form">
            <div class="admin_form_heading">
              <h4 class="color text-center">FORGET PASSWORD</h4>
            </div>

            

            <div class="admin_form_content mb-3">
              <div class="input-group">
                  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user"></i></span>
                  <input type="text" class="form-control user_name" placeholder="User Id (or) User Name" aria-describedby="basic-addon1" name="forget_name">
              </div>
              <?php echo form_error('forget_name','<span class="help-block form-error">', '</span>'); ?> 
              <br>
              <div class="input-group">
                  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user"></i></span>
                  <input type="text" class="form-control forget_mobile" placeholder="Registered Mobile Number" aria-describedby="basic-addon1" name="forget_mobile">
              </div>
            </div>
            <div>
                  <?php echo form_error('forget_mobile','<span class="help-block form-error">', '</span>'); ?> 
            <input type="hidden" name="forget" value="forget">
            </div>
            
            <div class="row border-bottom container">
                  <div class="col-md-6 pull-left mt-2 mb-4 pr-0 xs-pl-0">
                        <input type="hidden" name="login" value="asdf"> 
                        <a href="<?php echo base_url('index.php/login'); ?>"><button type="button" name="login" class="forget_cancel_button"> Cancel <i class="fa  fa-close" aria-hidden="true"></i></button></a>
                    </div>
                    <div class="col-md-6 pull-left mt-2 mb-4 pr-0 xs-pl-0">
                        <input type="hidden" name="login" value="asdf"> 
                        <button type="submit" name="login" class="login_sigin_button"> Send <i class="fa  fa-sign-in" aria-hidden="true"></i></button>
                    </div>
                </div>
                <div class="col-md-12 pull-left mt-4 pl-0 mb-3 xs-mt-mb-login text-center">
                  <a href="<?php echo base_url('index.php/login'); ?>"><p class="thead1 color"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back to SignIn</p></a>
                
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>  
<style type="text/css">
  span.help-block.form-error {
      padding: 0px 0px 0px 15px !important;
      margin-top: 0px !important;
  }
  .svk_err_cls{
    font-size: 18px;
    border: 1px solid;
    padding: 5px;
    background: red;
    color: #fff !important;
    width: 80%;
    margin: 0px auto;
  }
</style>