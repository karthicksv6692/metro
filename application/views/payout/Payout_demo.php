<div class="dkbody">
  <div class="container-fluid">
    <div class="page-header">
      <h3>Payouts</h3>
    </div>
    <form method="post" action="<?php echo base_url('index.php/payout_demo'); ?>">
      <div class="">
        Start Date (<span style="color: rgb(3, 181, 255)">Please give Railway Time</span>)
        <input type="text" name="start" class="xx">
        Example : <span style="color: rgb(3, 181, 255)">YYYY-MM-DD HH:MM:SS</span>
      </div>
      <div class="">
        End Date (<span style="color: rgb(3, 181, 255)">Please give Railway Time</span>)
        <input type="text" name="end" class="yy">
        Example : <span style="color: rgb(3, 181, 255)">YYYY-MM-DD HH:MM:SS</span>
      </div>
      <input type="submit" value="Payout">
    </form>
  </div>
</div>

<script type="text/javascript">
    $(function(){
      var d = new Date();
      var strDate1 = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate() + " " + "14" + ":" + "00" +":"+"00";
      var strDate2 = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate() + " " + "02" + ":" + "00" +":"+"00";
      
      $('.xx').val(strDate1);
      $('.yy').val(strDate2);
    })
  </script>