<div class="dkbody">
  <div class="container-fluid">
    <div class="page-header">
      <h3>Payout - Cutoff</h3>
    </div>
    <div class="rr">
      <div class="col-md-12 col-lg-12 gj_tree_struct">
          <div class="gj_tree_div text-center">
              <div class="rowx rrx" style="margin-bottom: 1em">
                <div class="col-lg-4 col-md-4 pull-left pr-0 pl-0">
                  <div style="">
                    <select class="user_id form-control" style="" name="user_id"></select>
                  </div>
                </div>

                <div class="col-lg-4 col-md-4 pull-left pr-0 pl-0">
                  <div style="">
                    <select class="user_name form-control" style="" name="user_name"></select>
                  </div>
                </div>

              </div>
          </div>
      </div>
    </div>
    <div class="rr">
        
        <div class="col-md-3 pull-left form-group">
          <span class="input input--nao" id="sandbox-container">
            <input type="text" class="input__field input__field--nao form-control dtt1"  name="dob" value="" 
             data-validation="date" />
             <!-- data-validation-format="yyyy-mm-dd" -->
            <label class="input__label input__label--nao" for="input-1">
              <span class="input__label-content input__label-content--nao">Select Payout Date</span>
            </label>
            <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
              <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
            </svg>
          </span> 
        </div>

        <div class="col-md-2 pull-left row">
            <select class="form-control slt_type pos1" id="exampleSelect1" name="">
                <option value="0">First Cutoff</option>
                <option value="1">Second Cutoff</option>
            </select>
            </label>
        </div>


        <div class="col-md-3 pull-left form-group">
          <span class="input input--nao" id="sandbox-container">
            <input type="text" class="input__field input__field--nao form-control dtt2"  name="dob" value="" 
             data-validation="date" />
             <!-- data-validation-format="yyyy-mm-dd" -->
            <label class="input__label input__label--nao" for="input-1">
              <span class="input__label-content input__label-content--nao">Select Payout Date</span>
            </label>
            <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
              <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
            </svg>
          </span> 
        </div>

        <div class="col-md-2 pull-left row">
            <select class="form-control slt_type pos2" id="exampleSelect1" name="">
                <option value="0">First Cutoff</option>
                <option value="1">Second Cutoff</option>
            </select>
            </label>
        </div>






        <button type="button" class="btn btn_row1  btn-7 btn-7a icon-truck mb-5" id="date-search"> <i class="fa fa-search" aria-hidden="true"></i></button>

     <!--  <input type="date" class="dtt"><br>
      <select class="pos">
        <option value="0">First CutOff</option>
        <option value="1">Second CutOff</option>
      </select> -->
      
    </div>
    <div class="page-content">                   
        <div class="container-fluid">
          <table  id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="10" width="100%">
            <thead>
                <tr>
                    <th colspan="1"></th>
                    <th colspan="2">P.Sales</th>
                    <th colspan="2">Prev</th>
                    <th colspan="2">Curr</th>
                    <th colspan="2">Tot</th>
                    <th colspan="2">Car</th>
                    <th colspan="2">Pair.Det</th>
                    <th colspan="2">Tot.PV</th>
                    <th colspan="1"></th>
                </tr>
                <tr>
                  <th class="text-center">Package Name</th>
                  <th class="text-center">L</th>
                  <th class="text-center">R</th>
                  <th class="text-center">L</th>
                  <th class="text-center">R</th>
                  <th class="text-center">L</th>
                  <th class="text-center">R</th>
                  <th class="text-center">L</th>
                  <th class="text-center">R</th>
                  <th class="text-center">L</th>
                  <th class="text-center">R</th>
                  <th class="text-center">Tot</th>
                  <th class="text-center">Flushed</th>
                  <th class="text-center">L</th>
                  <th class="text-center">R</th>
                  <th class="text-center">DATE</th>
                </tr>
            </thead>
            <tbody>
              <tfoot>
              <tr>
                <td colspan="13"><h4 style="text-align: right">Total:</h4></td>
                <td><h4 class="left"></h4></td>
                <td><h4 class="right"></h4></td>
                <td></td>
              </tr>
            </tfoot>
            </tbody>
          </table>
        </div>
         
      </div>
  </div>
</div>
<script type="text/javascript">

  $('.user_id').select2({
    placeholder: 'Search With User Id',
    allowClear: true,
    ajax: {
      url: '<?php echo base_url("index.php/common_controller/get_all_user_id") ?>',
      dataType: 'json',
      delay: 1,
      data: function (params) {
            return { q: params.term 
            };
      },
      processResults: function (data) { return { results: data }; },
      cache: true
    },
  });
  $('.user_name').select2({
    placeholder: 'Search With User Name',
    allowClear: true,
    ajax: {
      url: '<?php echo base_url("index.php/common_controller/get_all_user_name") ?>',
      dataType: 'json',
      delay: 1,
      data: function (params) {
            return { rs:1,q: params.term 
            };
      },
      processResults: function (data) { return { results: data }; },
      cache: true
    },
  });

  $(document).ready(function() {
    var tab = "";
    tab = $('#gj_tree_struct_vw').DataTable({});

      document.querySelector("#date-search").addEventListener('click',function(){
            dt1 = $('.dtt1').val();
            user_name = $('.user_name').val();
            user_id = $('.user_id').val();
            pos1 = $('.pos1').val(); 
            dt2 = $('.dtt2').val();
            pos2 = $('.pos2').val();
            id=0;
            if(user_id){
              id = user_id;
            }else{
              id=user_name;
            }
            if(user_name || user_id){
              tab.destroy();
              tab = $('#gj_tree_struct_vw').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                  "url": '<?php echo base_url(); ?>index.php/payout_demo/datatable_payout_admin',
                   "data": {'dt1':dt1,'pos1':pos1,'dt2':dt2,'pos2':pos2,'user_id':id},
                }
              });
            }
            id=0;
            if(user_id){
              id = user_id;
            }else{
              id=user_name;
            }
            $.ajax({
            type: "POST",
            url : '<?php echo base_url('index.php/common_controller/get_tot_payout_abstract_full_customer_admin') ?>',
            "data": {'dt1':dt1,'pos1':pos1,'dt2':dt2,'pos2':pos2,'user_id':id},
            success:  function (data) {
              if(data != 0){
                var option = JSON.parse(data);
                if(option['left'] != "" ){
                  $('.left').text(option['left']);
                }else{
                  $('.left').text('00.00');
                }
                if(option['right'] != "" ){
                  $('.right').text(option['right']);
                }else{
                  $('.right').text('00.00');
                }
                
               
              }else{
                 $('.right').text('00.00'); $('.left').text('00.00');
              }
            }
          })
      })
  });
</script>
<style type="text/css">
  select.form-control:not([size]):not([multiple]) {
      height: calc(2.25rem + 30px);
  }
  button#date-search {
      height: 50px;
      width: 60px;
      margin-left: 20px;
  }
  i.fa.fa-search {
      font-size: 25px;
  }
</style>