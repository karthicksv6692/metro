<?php //echo"<pre>";print_r($final_obj);echo"</pre>"; ?>
<div class="dkbody">
  <div class="container-fluid">
    <div class="page-header">
      <h3>Payouts</h3>
    </div>
    
    <div class="row">
      <div class="container text-left">
        <h4 style="margin: 15px 0px;color: #d93653;">Current CutOff : <span class="ct_dt" style="color: #2bb1e9;font-weight: bold;">00-00-0000 00:00:00 </span></h4>
      </div>
    </div>
      <table  id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="10" width="100%">
        <thead>
            <tr>
              <th class="text-center" rowspan="2" style="vertical-align: middle;">PACKAGE NAME</th>
              <th class="text-center" colspan="2">Personal Sales</th>
              <th class="text-center" colspan="2">Previous</th>
              <th class="text-center" colspan="2">Current Cutoff</th>
              <th class="text-center" colspan="2">Total Cutoff</th>
              <th class="text-center" colspan="2">Carry</th>
              <th class="text-center" colspan="2">Pair Details</th>
              <th class="text-center" colspan="2">Total PV</th>
            </tr>
            <tr>
              <th class="text-center">Count</th>
              <th class="text-center">Side</th>

              <th class="text-center">Left</th>
              <th class="text-center">Right</th>
              
              <th class="text-center">Left</th>
              <th class="text-center">Right</th>
              <th class="text-center">Left</th>
              <th class="text-center">Right</th>
              <th class="text-center">Left</th>
              <th class="text-center">Right</th>
              <th class="text-center">Total</th>
              <th class="text-center">Flushed</th>
              <th class="text-center">Left</th>
              <th class="text-center">Right</th>
            </tr>
        </thead>
        <tbody>
          <?php //echo "<pre>";print_r($final_obj_ar);echo"</pre>"; ?>
              <?php if(isset($final_obj_ar)){ ?> 
                <?php if($final_obj_ar != null){ ?> 
                  <?php foreach ($final_obj_ar as $final) { ?>
                   <tr>
                    <td><?php echo $final->getPackageName(); ?></td>
                    <td><?php echo $final->getPersonalSalesCount(); ?></td>
                    <td><?php echo $final->getPersonalSalesSide(); ?></td>
                    <td><?php echo $final->getPreviousLeft(); ?></td>
                    <td><?php echo $final->getPreviousRight(); ?></td>
                    <td><?php echo $final->getCurrentLeft(); ?></td>
                    <td><?php echo $final->getCurrentRight(); ?></td>
                    <td><?php echo $final->getTotalLeft(); ?></td>
                    <td><?php echo $final->getTotalRight(); ?></td>
                    <td><?php echo $final->getCarryLeft(); ?></td>
                    <td><?php echo $final->getCarryRight(); ?></td>
                    <td><?php echo $final->getPairDetailsTotal(); ?></td>
                    <td><?php echo $final->getPairDetailsFlushed(); ?></td>
                    <td><?php echo $final->getTotalPvLeft(); ?></td>
                    <td><?php echo $final->getTotalPvRight(); ?></td>
                  </tr>
                <?php } ?>
              <?php }else{?>
               <tr> <td colspan="15">Not Eligible For Payout.</td></tr>
              <?php } ?>
            <?php }else{?>
               <tr> <td colspan="15">Not Eligible For Payout.</td></tr>
            <?php } ?>

                    
          
        </tbody>
      </table>
      <?php //print_r($final_obj_res); ?>
      <?php if(isset($final_obj_res)){ ?> 
        <?php if($final_obj_res != null){ ?> 
        <div class="rower">
            <h4>Total Left : <?php echo $final_obj_res->getTotalLeftPv(); ?></h4>
            <h4>Total Right   : <?php echo $final_obj_res->getTotalRightPv(); ?></h4>
            <h4>CarryForward PV   : <?php echo $final_obj_res->getCarryForwardPv(); ?></h4>

            <?php if($final_obj_res->getCarryForwardPvSide() == 0){ ?>
              <h4>CarryForward Side   : Left</h4>
            <?php }else{ ?>
              <h4>CarryForward Side   : Right</h4>
            <?php } ?>

            <h4>net_payout PV   : <?php echo $final_obj_res->getNetPayout(); ?></h4>
        </div>
        <?php } ?>
      <?php } ?>
  </div>
</div>
<style type="text/css">
  .rower h4{
    width: 100%;
  }
  .rower {
      width: 22%;
      float: right;
  }
</style>

<script type="text/javascript">
  $(document).ready(function(){
    $.ajax({
      url : "<?php echo base_url('index.php/common_controller/get_payout_date'); ?>",
      success: function(data){
        console.log(data);
        if(data != 0){
          $('.ct_dt').text(data);
        }
      }
    })
  })
</script>