<div class="dkbody">
  <div class="container-fluid">
    <div class="page-header">
      <h3>Payout-Sales - KYC UPDATED LIST</h3>
    </div>
    <div class="row">
      <div class="col-md-3 pull-left form-group">
          <span class="input input--nao" id="sandbox-container">
            <input type="text" class="input__field input__field--nao form-control dtt"  name="dob" value="" 
             data-validation="date" />
             <!-- data-validation-format="yyyy-mm-dd" -->
            <label class="input__label input__label--nao" for="input-1">
              <span class="input__label-content input__label-content--nao">Select Payout Date</span>
            </label>
            <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
              <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
            </svg>
          </span> 
        </div>

        <div class="col-md-3 pull-left row">
            <select class="form-control slt_type pos" id="exampleSelect1" name="">
                <option value="0">First Cutoff</option>
                <option value="1">Second Cutoff</option>
            </select>
            </label>
        </div>
        <button type="button" class="btn btn_row1  btn-7 btn-7a icon-truck mb-5" id="date-search"> <i class="fa fa-search" aria-hidden="true"></i></button>
    </div>
    <div class="page-content">                   
        <div class="container-fluid">
          <table  id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="10" width="100%">
            <thead>
                
                <tr>
                  <th class="text-center">User Name</th>
                  <th class="text-center">Tot.Left</th>
                  <th class="text-center">Tot.Right</th>
                  <th class="text-center">CF Left PV</th>
                  <th class="text-center">CF Right PV</th>
                  <th class="text-center">TDS (5%)</th>
                  <th class="text-center">SERVICE (5%)</th>
                  <th class="text-center">Netpayout</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
         
      </div>
  </div>
</div>
<script type="text/javascript">

  $(document).ready(function() {
    var tab = "";
    tab = $('#gj_tree_struct_vw').DataTable({});

      document.querySelector("#date-search").addEventListener('click',function(){
            dt = $('.dtt').val();
            pos = $('.pos').val();
            if(dt.length>0){
              tab.destroy();
              tab = $('#gj_tree_struct_vw').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                  "url": '<?php echo base_url(); ?>index.php/payout_demo/datatable_payout_result_kyc_updated',
                   "data": {'dt':dt,'pos':pos},
                }
              });
            }
      })
  });
</script>
<style type="text/css">
  select.form-control:not([size]):not([multiple]) {
      height: calc(2.25rem + 30px);
  }
  button#date-search {
      height: 50px;
      width: 60px;
      margin-left: 20px;
  }
  i.fa.fa-search {
      font-size: 25px;
  }
</style>