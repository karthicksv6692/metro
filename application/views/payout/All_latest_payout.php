<?php //echo"<pre>";print_r($final_obj);echo"</pre>"; ?>
<div class="dkbody">
  <div class="container-fluid">
    <div class="page-header">
      <h3>Payouts</h3>
    </div>
    
     <div class="rr">
      <div class="col-md-12 col-lg-12 gj_tree_struct">
          <div class="gj_tree_div text-center">
              <div class="rowx rrx" style="margin-bottom: 1em">
                <div class="col-lg-4 col-md-4 pull-left pr-0 pl-0">
                  <div style="">
                    <select class="user_id form-control" style="" name="user_id"></select>
                  </div>
                </div>

                <div class="col-lg-4 col-md-4 pull-left pr-0 pl-0">
                  <div style="">
                    <select class="user_name form-control" style="" name="user_name"></select>
                  </div>
                </div>

                <div class="col-lg-3 col-md-3 pull-left pr-0 text-left">
                  <button type="submit" id="gj_dist_search" class="gj_dist_view" style="margin-top: 25px"><i class="fa fa-search gj_icon_srh"></i> SEARCH</button>    
                </div>
              </div>
          </div>
      </div>
    </div>
      <div class="ldr_tb_whole">
        <img src="<?php echo base_url('assets/images/loader/Rolling.gif'); ?>">
      </div>
      <table  id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="10" width="100%">
        <thead>
            <tr>
              <th class="text-center" rowspan="2" style="vertical-align: middle;">PACKAGE NAME</th>
              <th class="text-center" colspan="2">Total</th>
              <th class="text-center" colspan="2">Personal Sales</th>
              <th class="text-center" colspan="2">Previous</th>
              <th class="text-center" colspan="2">Current Cutoff</th>
              <th class="text-center" colspan="2">Total Cutoff</th>
              <th class="text-center" colspan="2">Carry</th>
              <th class="text-center" colspan="2">Pair Details</th>
              <th class="text-center" colspan="2">Total PV</th>
            </tr>
            <tr>
              <th class="text-center">Left</th>
              <th class="text-center">Right</th>
              <th class="text-center">Count</th>
              <th class="text-center">Side</th>

              <th class="text-center">Left</th>
              <th class="text-center">Right</th>
              
              <th class="text-center">Left</th>
              <th class="text-center">Right</th>
              <th class="text-center">Left</th>
              <th class="text-center">Right</th>
              <th class="text-center">Left</th>
              <th class="text-center">Right</th>
              <th class="text-center">Total</th>
              <th class="text-center">Flushed</th>
              <th class="text-center">Left</th>
              <th class="text-center">Right</th>
              
            </tr>
        </thead>
        <tbody class="tbdy">
          <tr>
            <td colspan="14"></td>
            <td>Total : </td>
            <td class="tot_right">00.00</td>
            <td class="tot_left">00.00</td>
          </tr>
          <tr>
            <td colspan="14"></td>
            <td>Net Payout : </td>
            <td colspan="2" class="net_pay" align="center">00.00</td>
          </tr>
        </tbody>
        
          
        
      </table>

      <table  id="carry_table" class="table table-striped table-bordered" cellspacing="10" width="500px">
        <thead>
          <tr>
            <th>Carry Left PV</th>
            <th>Carry Right PV</th>
          </tr>
        </thead>
        <tbody class="carry_tbdy"></tbody>
      </table>
      
  </div>
</div>
<style type="text/css">
  .rower h4{
    width: 100%;
  }
  .rower {
      width: 22%;
      float: right;
  }
</style>

<script type="text/javascript">
  $(function(){$('.ldr_tb_whole').css({'display':'none'});})
  $('.user_id').select2({
    placeholder: 'Search With User Id',
    allowClear: true,
    ajax: {
      url: '<?php echo base_url("index.php/common_controller/get_all_user_id") ?>',
      dataType: 'json',
      delay: 1,
      data: function (params) {
            return { q: params.term 
            };
      },
      processResults: function (data) { return { results: data }; },
      cache: true
    },
  });
  $('.user_name').select2({
    placeholder: 'Search With User Name',
    allowClear: true,
    ajax: {
      url: '<?php echo base_url("index.php/common_controller/get_all_user_name") ?>',
      dataType: 'json',
      delay: 1,
      data: function (params) {
            return { rs:1,q: params.term 
            };
      },
      processResults: function (data) { return { results: data }; },
      cache: true
    },
  });
  $('button#gj_dist_search').click(function(){
    user_id = $('.user_id').val();
    user_name = $('.user_name').val();
    if(user_id || user_name){
      if(user_id && !user_name){
        id = user_id;
      }else{
        id = user_name;
      }
      $('.ldr_tb_whole').css({'display':'block'});

      $.ajax({
        type : 'post',
        url  : "<?php echo base_url('index.php/payout_demo/all_payout'); ?>",
        data : {'user_id':id},
        success : function(data){
          if(data != 0){
            res = JSON.parse(data);
            console.log(res)
            $('.tbdy').html('');
            $('.carry_tbdy').html('');
            $('.tbdy').html(res[0]['first']);
            // $('.carry_tbdy').html(res[1]['second']);
            $('.ldr_tb_whole').css({'display':'none'});
          }else{
            $('.tbdy').html('');
            $('.tbdy').html('<tr><td colspan="15">Payout Not Eligible</td></tr>');
            $('.carry_tbdy').html('');
            $('.ldr_tb_whole').css({'display':'none'});
          }
        }
      })
    }
  })
</script>
<style type="text/css">
  table#carry_table {
      width: 300PX;
      float: right;
  }
  .ldr_tb_whole {
      width: 98%;
      position: absolute;
      background: #fff;
      display: block;
      text-align: center;
      height: 70%;
      z-index: 2;
  }
  .ldr_tb_whole img {
      top: 40%;
      position: absolute;
  }
</style>