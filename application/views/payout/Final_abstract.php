

<div class="dkbody">
  <div class="container-fluid">
    <div class="page-header">
      <h3>Payout-Sales</h3>
    </div>
    <div class="rr">
      <div class="col-md-12 col-lg-12 gj_tree_struct">
          <div class="gj_tree_div text-center">
              <div class="rowx rrx" style="margin-bottom: 1em">
                <div class="col-lg-4 col-md-4 pull-left pr-0 pl-0">
                  <div style="">
                    <select class="user_id form-control" style="" name="user_id"></select>
                  </div>
                </div>

                <div class="col-lg-4 col-md-4 pull-left pr-0 pl-0">
                  <div style="">
                    <select class="user_name form-control" style="" name="user_name"></select>
                  </div>
                </div>

                <div class="col-lg-4 col-md-4 pull-left pr-0 pl-0">
                  <div style="">
                    <?php
                      $d1 = '2017-10-29';
                      $d2 = date('Y-m-d');
                      $x= new DateTime($d1);
                      $y= new DateTime($d2);
                      $interval = $x->diff($y);
                      $d = $interval->days;
                    ?>
                    <select class="cut_off" style="width:250px">
                      <option value="-1">Select a Cutoff wise</option>
                     <?php $j=0;
                        $t = 2*($d+1)+1;
                        for($i=0;$i<=$d;$i++){ $j++;?>
                          <option value="<?php echo date( 'Y-m-d', strtotime( $d2 . ' -'.$i.' day' ) ).' 02:00:00' ?>,<?php echo date( 'Y-m-d', strtotime( $d2 . ' -'.$i.' day' ) ).' 13:59:59' ?>"><?php echo $t-$j ;echo" - "; echo date( 'Y-m-d', strtotime( $d2 . ' -'.$i.' day' ) ); ?>  : 1</option>
                          <?php $j++; ?>
                          <option value="<?php echo date( 'Y-m-d', strtotime( $d2 . ' -'.$i.' day' ) ).' 14:00:00' ?>,<?php echo date( 'Y-m-d', strtotime( $d2 . ' -'.($i-1).' day' ) ).' 01:59:59' ?>"><?php echo $t-$j;echo" - "; echo date( 'Y-m-d', strtotime( $d2 . ' -'.$i.' day' ) ); ?>  : 2</option>
                        <?php } ?>
                    </select>
                  </div>
                </div>

              </div>
          </div>
      </div>
    </div>
    <div class="rr">
        <div class="col-md-2 pull-left form-group">
          <span class="input input--nao" id="sandbox-container">
            <input type="text" class="input__field input__field--nao form-control dtt1"  name="dob" value="" 
             data-validation="date" />
             <!-- data-validation-format="yyyy-mm-dd" -->
            <label class="input__label input__label--nao" for="input-1">
              <span class="input__label-content input__label-content--nao">From</span>
            </label>
            <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
              <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
            </svg>
          </span> 
        </div>

        <div class="col-md-2 pull-left row">
            <select class="form-control slt_type pos1" id="exampleSelect1" name="">
                <option value="0">First Cutoff</option>
                <option value="1">Second Cutoff</option>
                <option value="2">All</option>
            </select>
            </label>
        </div>

        <div class="col-md-2 pull-left form-group">
          <span class="input input--nao" id="sandbox-container">
            <input type="text" class="input__field input__field--nao form-control dtt2"  name="dob" value="" 
             data-validation="date" />
             <!-- data-validation-format="yyyy-mm-dd" -->
            <label class="input__label input__label--nao" for="input-1">
              <span class="input__label-content input__label-content--nao">To</span>
            </label>
            <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
              <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
            </svg>
          </span> 
        </div>

        <div class="col-md-2 pull-left row">
            <select class="form-control slt_type pos2" id="exampleSelect1" name="">
                <option value="0">First Cutoff</option>
                <option value="1">Second Cutoff</option>
            </select>
            </label>
        </div>

        <div class="col-md-3 pull-left row">
            <select class="form-control slt_type stat" id="exampleSelect1" name="">
                <option value="0">All</option>
                <option value="1">With PAN</option>
                <option value="2">With Out PAN</option>
            </select>
            </label>
        </div>
        <button type="button" class="btn btn_row1  btn-7 btn-7a icon-truck mb-5" id="date-search"> <i class="fa fa-search" aria-hidden="true"></i></button>
    </div>

    <div class="page-content">                   
        <div class="container-fluid" style="margin-bottom: 2em">
          <input type="button" value="print" class="cus_but">
          <table  id="gj_tree_struct_vw" class="table table-striped table-bordered table-responsive data_table_style" cellspacing="10"  style="width: 100%;">
            <thead>
                
                <tr>
                  <th class="text-center col-md-1" style="max-width: 30px !important;width: 30px">#</th>
                  <th class="text-center" style="max-width: 30px !important;width: 30px">Id</th>
                  <th class="text-center" style="max-width: 100px !important">User-Name</th>
                  <th class="text-center" style="max-width: 150px !important">Name</th>
                  <!-- <th class="text-center" style="max-width: 100px !important">Last Name</th> -->
                  <th class="text-center" style="max-width: 80px !important">Pan</th>
                  <th class="text-center" style="max-width: 90px !important">Cut of Date</th>
                  <th class="text-center" style="width: 100px !important;max-width: 100px !important;">Bank</th>
                  <th class="text-center" style="max-width: 80px !important">Branch</th>
                  <th class="text-center" style="max-width: 100px !important">Ac/no</th>
                  <th class="text-center" style="max-width: 80px !important">Ifsc</th>

                  <th class="text-center" style="max-width: 80px !important">Gross</th>
                  <th class="text-center" style="max-width: 80px !important">Tds</th>
                  <th class="text-center" style="max-width: 55px !important">Ser (5%)</th>
                  <th class="text-center" style="max-width: 70px !important">Repur (10%)</th>
                  <th class="text-center" style="max-width: 65px !important">Net</th>
                  <th class="text-center" style="max-width: 65px !important">Action</th>
                </tr>
            </thead>
            <tbody style="text-align: center;" class="my_tab_bdy">
              <!-- <tr><td>Karthick</td><td>Karthick</td><td>Karthick</td><td>Karthick</td></tr> -->
            </tbody>
            <tfoot>
              <tr>
                <td colspan="10" class="text-right"><h4>Total:</h4></td>
                <td><h4 class="gross"></h4></td>
                <td><h4 class="ttds"></h4></td>
                <td><h4 class="ser"></h4></td>
                <td><h4 class="trep"></h4></td>
                <td><h4 class="netpay"></h4></td>
                <td></td>
              </tr>
            </tfoot>
          </table>
        </div>
         
      </div>
  </div>
</div>
<button type="button" class="btn btn-info btn-lg mod" data-toggle="modal" data-target="#myModal" style="opacity: 0">Open Modal</button>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
      $('.cut_off').select2({
        placeholder: "Select a Cutoff wise",
        allowClear: true,
        id: '-1',
        text: 'Select a Cutoff wise',
      });
  })
 
  $('.user_id').select2({
    placeholder: 'Search With User Id',
    allowClear: true,
    ajax: {
      url: '<?php echo base_url("index.php/common_controller/get_all_user_id") ?>',
      dataType: 'json',
      delay: 1,
      data: function (params) {
            return { q: params.term 
            };
      },
      processResults: function (data) { return { results: data }; },
      cache: true
    },
  });
  $('.user_name').select2({
    placeholder: 'Search With User Name',
    allowClear: true,
    ajax: {
      url: '<?php echo base_url("index.php/common_controller/get_all_user_name") ?>',
      dataType: 'json',
      delay: 1,
      data: function (params) {
            return { rs:1,q: params.term 
            };
      },
      processResults: function (data) { return { results: data }; },
      cache: true
    },
  });

  $(document).ready(function() {

    var tab = "";
    tab = $('#gj_tree_struct_vw').DataTable({

      dom: 'Blfrtip',
      buttons: [
          {
            extend: 'excel',
            exportOptions: {
                columns: [ 0, 1, 5 ]
            }
         }
          //'pdfHtml5'
      ],
      "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      

     
    });
    
    // && dt2.length>0
   
    
    document.querySelector("#date-search").addEventListener('click',function(){
          dt1 = $('.dtt1').val();
          pos1 = $('.pos1').val();

          dt2 = $('.dtt2').val();
          pos2 = $('.pos2').val();

          user_name = $('.user_name').val();
          user_id = $('.user_id').val();

          stat = $('.stat').val();
          cut_off = $('.cut_off').val();
          if(dt1.length>0 ||user_name!=null || user_id != null ){
            tab.destroy();
            tab = $('#gj_tree_struct_vw').DataTable({
              
              "processing": true,
              "serverSide": true,
              "ajax": {
                "url": '<?php echo base_url(); ?>index.php/payout_demo/datatable_payout_abstract',
                 "data": {'dt1':dt1,'pos1':pos1,'dt2':dt2,'pos2':pos2,'stat':stat,'user_name':user_name,'user_id':user_id},
              },
              dom: 'Blfrtip',
                buttons: [
                {
                  extend: 'pdfHtml5',
                  footer:true,
                  exportOptions: {
                     stripNewlines: false
                  },
                },
                    'excelHtml5',
                    'pdfHtml5',

                ],

                "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bInfo": false
            });
            
          }else if(cut_off != -1){
            com = cut_off.split(",")
            //console.log(com);
            //console.log(com[0]);
            //console.log(com[1]);
            d = com[0].split(' ');
            dt1 = d[0];
            if(d[1] == "02:00:00"){
              pos1=0;
            }else{
              pos1=1;
            }
            if(dt1.length>0 ||user_name!=null || user_id != null ){
            tab.destroy();
            tab = $('#gj_tree_struct_vw').DataTable({
              
              "processing": true,
              "serverSide": true,
              "ajax": {
                "url": '<?php echo base_url(); ?>index.php/payout_demo/datatable_payout_abstract',
                 "data": {'dt1':dt1,'pos1':pos1,'dt2':dt2,'pos2':pos2,'stat':stat,'user_name':user_name,'user_id':user_id},
              },
              dom: 'Blfrtip',
                buttons: [
                {
                  extend: 'pdfHtml5',
                  footer:true,
                  exportOptions: {
                     stripNewlines: false
                  },
                },
                    'excelHtml5',
                    'pdfHtml5',

                ],

                "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bInfo": false
            });
            
          }
          }

          

          stat = $('.stat').val();
          $.ajax({
            type: "POST",
            url : '<?php echo base_url('index.php/common_controller/get_tot_payout_abstract_full') ?>',
             "data": {'dt1':dt1,'pos1':pos1,'dt2':dt2,'pos2':pos2,'stat':stat,'user_name':user_name,'user_id':user_id},
            success:  function (data) {
              if(data != 0){
                var option = JSON.parse(data);
                if(option['total_gross'] != "" ){
                  $('.gross').text(option['total_gross']);
                }else{
                  $('.gross').text('00.00');
                }
                
                if(option['total_net'] != "" ){
                  $('.netpay').text(option['total_net']);
                }else{
                  $('.netpay').text('00.00');
                }

                if(option['total_tds'] != "" ){
                  $('.ttds').text(option['total_tds']);
                }else{
                  $('.ttds').text('00.00');
                }
                if(option['total_service'] != "" ){
                  $('.ser').text(option['total_service']);
                }else{
                  $('.ser').text('00.00');
                }
                if(option['total_repurchase'] != "" ){
                  $('.trep').text(option['total_repurchase']);
                }else{
                  $('.trep').text('00.00');
                }
              }else{
                $('.gross').text('');$('.deduction').text('00.00');$('.netpay').text('');
                $('.trep').text(''); $('.ser').text('');$('.ttds').text('');
              }
            }
          })
    })

    $('.cus_but').click(function(){

      var gr = $('.gross').text();
      var netpay = $('.netpay').text();
      var ttds = $('.ttds').text();
      var ser = $('.ser').text();
      var trep = $('.trep').text();

       tab.row.add( [ '','','','','','','','','','Total',gr,ttds,ser,trep,netpay ] ).draw();

      //$('table#gj_tree_struct_vw > tbody tr:last').after('<tr><td>Karthick</td><td>Karthick</td><td>Karthick</td><td>Karthick</td></tr>');
        $('a.dt-button.buttons-excel.buttons-html5').click();
    })

  });

  function get_payout_res(id,date){
    if(id>0){
      $.ajax({
        type : 'post',
        url  : "<?php echo base_url('index.php/common_controller/get_payout_res'); ?>",
        data : {'id':id,'date':date},
        success: function(data){
          var option = JSON.parse(data);
          $('.modal-body').html("");
          $('.modal-body').html(option);
          $('.mod').click();

        }
      })
    }
  }


</script>
<style type="text/css">

table#gj_tree_struct_vwee thead tr th {
    background: #2bb1e9;
    color: #fff;
    min-width: 40px;
}
div#myModal {
    padding-top: 11%;
}
.modal-dialog {
    width: 60%;
    margin: 30px auto;
    max-width: 100%;
}
a.dt-button.buttons-excel.buttons-html5{
  opacity: 0;
  display: none;
}
  .dt-buttons a {
      padding: 10px 25px;
      margin: 10px;
      background: #2bb1e9;
      color: #fff;
  }
  a.dt-button.buttons-excel.buttons-html5 {
      position: absolute;
      left: 7em;
  }
  a.dt-button.buttons-pdf.buttons-html5 {
      position: absolute;
      left: -9px;
  }
  .dt-buttons {
      padding-bottom: 20px;
      position: absolute;
      left: 170px;
      top: -20px;
  }

  select.form-control:not([size]):not([multiple]) {
      height: calc(2.25rem + 30px);
  }
  button#date-search {
      height: 50px;
      width: 60px;
      margin-left: 20px;
  }
  i.fa.fa-search {
      font-size: 25px;
  }
  .data_table_style thead tr th{
    background-color: rgba(22, 169, 231, 0.91) !important;
    color: #fff;
    font-size: 14px;
    padding: 7px;
    border: 1px solid;
    /*min-width: 160px;*/
  }
  .box{
    width: 30%;
    float: right;
  }
  .f12{
    width: 100%;
  }
  .f6{
    width: 50%;
    float: left;
  }
  .hd{
    color: #088bc1 !important;
  }
  .rs{
        color: #ff4081;
  }
  input.cus_but {
    background: #2bb1e9;
    padding: 7px 25px;
    font-size: 15px;
    color: #fff;
    border: 0;
    position: absolute;
    left: 16%;
    z-index: 2;
}
  table.display {
    margin: 0 auto;
    width: 100%;
    clear: both;
    border-collapse: collapse;
    table-layout: fixed;         // add this 
    word-wrap:break-word;        // add this 
  }
  a.dt-button.buttons-pdf.buttons-html5 {
    z-index: 1;
    opacity: 0;
}
/*#tbl_qunatity { table-layout: fixed; }*/

</style>