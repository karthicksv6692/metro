<div class="dkbody">
	<div class="container-fluid">
		<div class="page-header gj_new_pack">
			<h3>Distributor Details View</h3>
		</div>
		<p class="gj_package_dets"><span><i class="fa fa-cubes"></i></span> Distributor Details</p>
		<div class="row">
			<!-- <div class="col-md-12 col-lg-12 gj_tree_struct">
				<div class="gj_tree_div text-center">
					<form>
						<div class="row rr">
							<div class="col-lg-3 col-md-3 offset-lg-2 offset-md-2 pull-left pr-0 pl-0">
								<span class="input input--nao">
									<input class="input__field input__field--nao" type="text" id="distributor_id"
									 />
									<label class="input__label input__label--nao" for="distributor_id">
										<span class="input__label-content input__label-content--nao">Distributor ID</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
								</span>		
							</div>

							<div class="col-lg-3 col-md-3 pull-left pr-0">
								<span class="input input--nao">
									<input class="input__field input__field--nao" type="text" id="distributor_name"
									 />
									<label class="input__label input__label--nao" for="distributor_name">
										<span class="input__label-content input__label-content--nao">Name</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
								</span>		
							</div>

							<div class="col-lg-2 col-md-3 pull-left pr-0">
								<button type="button" id="gj_dist_search" class="gj_dist_view"><i class="fa fa-search gj_icon_srh"></i> View</button>		
							</div>
						</div>
						
					</form>	
				</div>
			</div> -->
		</div>
		
		<div class="gj_whole_tree">
				<div class="row rr">
					<div class="col-lg-6 col-md-6 pull-left">
						<div class="gj_tree_l_count">
							<span class="gj_tree_left_cnt">Total Left Side Count : <span class="gj_l_cnt_no">2</span></span>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 pull-left">
						<div class="gj_tree_r_count">
							<span class="gj_tree_right_cnt">Total Right Side Count : <span class="gj_r_cnt_no">2</span></span>
						</div>
					</div>
				</div>
			<div class="row gj_row_bot">
				<div class="col-md-12 col-lg-12 gj_tree_vw_struct">
					<div class="gj_first_tree gj_tree_vws">
						<div class="gj_tree_id_div gj_tree_id">37702</div>
						<img src="images/tree_icon/user_1.png" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
						<div class="gj_user_det">
							<p class="gj_tree_inf gj_tree_name">METRO KINGS</p>
							<p class="gj_tree_inf gj_tree_name">METRO KINGS</p>
							<p class="gj_tree_inf"><span class="fa fa-clock-o gj_tree_ph"></span>17/12/2016 00:00:00</p>
						</div>
						<div class="gj_tree_table_vw">
							<div class="gj_cutoff_table gj_tree_resp table-responsive">
								<table id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="0" width="100%">
								    <thead>
								      	<tr>
									        <th class="text-center gj_tree_downline" colspan="9">Downline BV Details For 00001</th>
								      	</tr>
								      	<tr>
									        <th class="text-center" rowspan="2">Package Name</th>
									        <th class="text-center" colspan="2">Total</th>
									        <th class="text-center" colspan="2">Personal</th>
									        <th class="text-center" colspan="2">Current</th>
									        <th class="text-center" colspan="2">Carry Forward</th>
								      	</tr>
								      	<tr>
									        <th class="text-center">Left</th>
									        <th class="text-center">Right</th>
									        <th class="text-center">Left</th>
									        <th class="text-center">Right</th>
									        <th class="text-center">Left</th>
									        <th class="text-center">Right</th>
									        <th class="text-center">Left</th>
									        <th class="text-center">Right</th>
								      	</tr>
								    </thead>
								    <tbody>
								      	<tr>
									        <td>PACKAGE 100BV</td>
									        <td>2719</td>
									        <td>1084</td>
									        <td>0</td>
									        <td>3</td>
									        <td>0</td>
									        <td>0</td>
									        <td>1639</td>
									        <td>0</td>
								      	</tr>
								      	<tr>
									        <td>PACKAGE 200BV</td>
									        <td>5</td>
									        <td>3</td>
									        <td>0</td>
									        <td>0</td>
									        <td>0</td>
									        <td>0</td>
									        <td>2</td>
									        <td>0</td>
								      	</tr>
								      	<tr>
									        <td>BUNDLE OFFER</td>
									        <td>408</td>
									        <td>247</td>
									        <td>0</td>
									        <td>0</td>
									        <td>0</td>
									        <td>0</td>
									        <td>159</td>
									        <td>0</td>
								      	</tr>
								    </tbody>
								</table>
							</div>
						</div>
					</div>
					<hr class="gj_tree_hr"/>
				</div>
			</div>

			<div class="row gj_row_bot">
				<div class="col-md-6 col-lg-6 gj_tree_vw1_struct">
					<div class="gj_second_tree gj_tree_vws">
						<div class="gj_tree_id_div gj_tree_id">69841</div>
						<img src="images/tree_icon/user_1.png" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
						<div class="gj_user_det">
							<p class="gj_tree_inf gj_tree_name">SK</p>
							<p class="gj_tree_inf gj_tree_name">METRO KINGS</p>
							<p class="gj_tree_inf"><span class="fa fa-clock-o gj_tree_ph"></span>17/12/2016 00:00:00</p>
						</div>
						
					</div>
					<hr class="gj_tree_hr"/>
				</div>
				<div class="col-md-6 col-lg-6 gj_tree_vw1_struct">
					<div class="gj_second_tree gj_second_tree_nxt gj_tree_vws">
						<div class="gj_tree_id_div gj_tree_id">92566</div>
						<img src="images/tree_icon/user_1.png" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
						<div class="gj_user_det">
							<p class="gj_tree_inf gj_tree_name">MK</p>
							<p class="gj_tree_inf gj_tree_name">METRO KINGS</p>
							<p class="gj_tree_inf"><span class="fa fa-clock-o gj_tree_ph"></span>17/12/2016 00:00:00</p>
						</div>
						
					</div>
					<hr class="gj_tree_hr"/>
				</div>
			</div>

			<div class="row gj_row_bot">
				<div class="col-md-3 col-lg-3 gj_tree_vw1_struct">
					<div class="gj_third_tree gj_tree_vws">
						<div class="gj_tree_id_div gj_tree_id">-----</div>
						<img src="images/tree_icon/user_3.png" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
						<p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
						
					</div>
				</div>
				<div class="col-md-3 col-lg-3 gj_tree_vw1_struct">
					<div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
						<div class="gj_tree_id_div gj_tree_id">02216</div>
						<img src="images/tree_icon/user_1.png" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
						<div class="gj_user_det">
							<p class="gj_tree_inf gj_tree_name">SSK</p>
							<p class="gj_tree_inf gj_tree_name">METRO KINGS</p>
							<p class="gj_tree_inf"><span class="fa fa-clock-o gj_tree_ph"></span>17/12/2016 00:00:00</p>
						</div>
						
					</div>
				</div>
				<div class="col-md-3 col-lg-3 gj_tree_vw1_struct">
					<div class="gj_third_tree gj_tree_vws">
						<div class="gj_tree_id_div gj_tree_id">77979</div>
						<img src="images/tree_icon/user_2.png" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
						<div class="gj_user_det">
							<p class="gj_tree_inf gj_tree_name">SS</p>
							<p class="gj_tree_inf gj_tree_name">METRO KINGS</p>
							<p class="gj_tree_inf"><span class="fa fa-clock-o gj_tree_ph"></span>17/12/2016 00:00:00</p>
						</div>
						
					</div>
				</div>
				<div class="col-md-3 col-lg-3 gj_tree_vw1_struct">
					<div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
						<div class="gj_tree_id_div gj_tree_id">-----</div>
						<img src="images/tree_icon/user_3.png" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
						<p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
						
					</div>
				</div>
			</div>
		</div>

		<div class="country_width_100 col-12 mt-5">	      	
	        <div class="country_width_100 mt-3 mb-3">					      		  
			    <div class="country-right">
			      	<button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button>
			    </div>	
			</div>
		</div>
	</div>	
</div>