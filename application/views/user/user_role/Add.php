<div class="container-fluid">
	<div class="page-header">
		<h3>User Role</h3>
	</div>
	<form method="post" action="<?php echo base_url('index.php/user/user_role_crud/add'); ?>" id="role_add">
		<div class="row full_width">

			<div class="col-md-5">
					<div class="col-md-12 pull-left	form-group">
						<span class="input input--nao">
							<input class="input__field input__field--nao role_name" type="text" id="role_name" name="role_name" value="<?php echo set_value('role_name'); ?>"
							data-validation="required custom length" 
							data-validation-length="2-100"
							data-validation-regexp="^[A-Za-z][A-Za-z0-9-_.&amp;]+$"
							data-validation-error-msg-custom="please provide valid role name"
							>
							<label class="input__label input__label--nao" for="access_add_name">
								<span class="input__label-content input__label-content--nao">Role Name</span>
							</label>
							<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
								<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
							</svg>
							<img src="<?php echo base_url('assets/images/loader/') ?>default.gif" class="ldr pin_ldr">
						</span>	
						<p class="pin_suc avail_er suc">Available</p>
						<p class="pin_err avail_er err">Already Exists</p>
						<?php echo form_error('role_name','<span class="help-block form-error">', '</span>'); ?>
					</div>

					<div class="col-md-12 pull-left form-group">
						<span class="input input--nao">
							 <textarea class="input__field input__field--nao" rows="3" id="module_add_description" name="role_desc" value="<?php echo set_value('role_desc'); ?>"
							 	data-validation="required length" 
								data-validation-length="min10"
							 	></textarea >
							<label class="input__label input__label--nao" for="module_add_description">
								<span class="input__label-content input__label-content--nao">Description</span>
							</label>
							<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
								<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></path>
							</svg>
						</span>	
						<?php echo form_error('role_desc','<span class="help-block form-error">', '</span>'); ?>	
					</div>
			</div>

			<div class="col-md-7 pull-left mt-5 ">
				
				<h4 class="userrole_addaccess">Access</h4>
				<?php if($module_list != null){  ?>
					<?php if(count($module_list)>0 && count($access_list)>0){ ?>	
						<?php foreach ($module_list as $m_list) { ?>
							<div class="row rr mt-3 ml-2">
								<div class="checkboxFive">
		                        	<input type="checkbox" id="<?php echo $m_list->getModuleId(); ?>" onchange="mul(<?php echo $m_list->getModuleId(); ?>)" class="role-<?php echo $m_list->getModuleId(); ?>" >  
		                        	<label for="<?php echo $m_list->getModuleId(); ?>"></label>
		                        	<span class="checkbox-material ml-4">
		                        		<span class="check"></span> <?php if(isset($m_list)){if($m_list->getModuleName() != null){echo $m_list->getModuleName();}} ?>
		                        	</span>
		                        </div>
		                    </div>
		                  	<div class="row rr mt-3 ml-2">
		                  		<?php foreach ($access_list as $a_list) { 
				                    if($a_list->getModuleId()->getModuleId() == $m_list->getModuleId()){
				               	?>
				               		<div class="checkboxFive pull-left pr-3">
				               			<input type="checkbox"  data-role="<?php echo $m_list->getModuleId(); ?>" class="access-<?php echo $a_list->getAccessId(); ?>" value="<?php echo $a_list->getAccessId(); ?>" id="<?php echo $a_list->getAccessId();?>-<?php echo $a_list->getAccessName();?>" onchange="access_change(<?php echo $a_list->getAccessId(); ?>)" <?php if(set_value('hid_access_id') != null){$ar = explode(',',set_value('hid_access_id')); if(in_array($a_list->getAccessId(), $ar)){echo"checked='checked'";}} ?>>
			                        	<label for="<?php echo $a_list->getAccessId();?>-<?php echo $a_list->getAccessName();?>"></label>
			                        	<span class="checkbox-material ml-4">
			                        		<span class="check"></span> <?php echo $a_list->getAccessName(); ?>
			                        	</span>
			                        </div>
		                        <?php }} ?>
		                   	</div>
		                <?php } ?>
		            <?php } ?>
		        <?php } ?>
			</div>
		</div>
		<input type="hidden" class="hid_access_id" name="hid_access_id" value="<?php echo set_value('hid_access_id'); ?>">
		<input type="hidden" name="user_role" value="asdf">
		<div class="country_width_100 col-12 mt-5">
			<div class="country_width_100">
				<div class="butt_sec_width mt-3 mb-3">
			      	<button type="submit" name="country_submit" class="country_button mr-2" >SUBMIT <i class="fa fa-paper-plane" aria-hidden="true"></i></button>		
				    <button type="reset" name="country_reset" class="country_button">RESET <i class="fa fa-refresh" aria-hidden="true"></i></button>
				</div>
		  	</div>
		  	
		    <div class="country_width_100 mt-3 mb-3">					      		  
			    <div class="country-right">
			      	<a href="<?php echo base_url('index.php/user/user_role_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button></a>
			    </div>	
			</div>
		</div>
	</form>
</div>

<script type="text/javascript">
	$(function(){
		$('.pin_suc,.pin_err,.pin_ldr').hide();
	})
	$.validate({
		lang: 'en',
	    form:'#role_add',
	    //modules :'security'
	    modules :'logic,security,date',
	    onModulesLoaded : function() {
	      console.log('Modules are loaded successfully');
	  	}
	});
	$('.role_name').focus(function()  {
	  var a = $('.role_name').val();
	  if(a.length>0){
	    cc_name = a;
	  }else{
	    cc_name="";
	  }
	})

	$('.role_name').blur(function()  {
	    var role_name = $('.role_name').val();
	    if(role_name.length>4 && cc_name != role_name){
	      $('.pin_ldr').show();
	      $.ajax({
	        type: "POST",
	        url: "<?php echo base_url(); ?>index.php/Common_controller/role_name_available",
	        data: {'field_value':role_name},
	          success:  function (data) { 
	            $('.pin_ldr').hide();
	            if(data == 1){
	            	$('.pin_suc').show();
	            	$('.pin_err').hide();
	            }else{
	            	$('.pin_suc').hide();
	            	$('.pin_err').show();
	            }
	          }
	      })
	    }else if(role_name.length == 0 && cc_name.length > 2){
	     $('.pin_suc,.pin_err').hide();
	    }
  	})
  	$(document).on("click", "button[type='reset']", function(){
	 $('.pin_suc,.pin_err,.pin_ldr').hide();
	});





	function mul(id){
	    $('[data-role='+id+']').each(function(){
	      ac_id =$('.hid_access_id').val();
	      var result=ac_id.split(',');
	      if($('.role-'+id).prop("checked") == true){
	        $("[data-role='"+id+"']").addClass('sss-'+id);
	        // $('data-role'+id).addClass('sss-'+val);
	        if(result.indexOf($(this).val())==-1){
	          result.push($(this).val());
	        }
	        $('.hid_access_id').val(array_splice(result));
	        $(this).prop('checked',true);
	      }
	      if($('.role-'+id).prop("checked") == false){
	        $("[data-role='"+id+"']").removeClass('sss-'+id);
	        var remove_value_index = result.indexOf(""+$(this).val());
	        result.splice( remove_value_index, 1 );
	        $('.hid_access_id').val(array_splice(result));
	        $(this).prop('checked',false);
	      }
	    })
  	}
  	function access_change(id){ 
	    var val = $('.access-'+id).attr('data-role');
	    $('.access-'+id).addClass('sss-'+val);
	    ac_id =$('.hid_access_id').val();
	    var result=ac_id.split(',');
	    var checked = $('.access-'+id+':checked').val();
	    if($('.access-'+id). prop("checked") == true){
	      var count = $('.sss-'+val).length;
	      var cc = $("[data-role='"+val+"'][data-cls='View']" ).val();
	      if($("[data-role='"+val+"']").length == $('.sss-'+val).length){ 
	        $('.role-'+val).click(); 
	      }
	      if(result.indexOf(checked)==-1){
	        result.push(checked);
	        if(result.indexOf(cc)==-1){
	          result.push(cc);
	        }
	      }
	      $('.hid_access_id').val(array_splice(result));
	    }else{
	      var val = $('.access-'+id).attr('data-role');
	      if($(".sss-"+val).length == 1){
	        if($('.role-'+val). prop("checked") == true){
	            $('.role-'+val).attr('checked', true); 
	        }
	      }
	      $('.access-'+id).removeClass('sss-'+val);
	      if($("[data-role='"+val+"']").length-1 == $('.sss-'+val).length){
	        $('.role-'+val).attr('checked', false); 
	      }
	      var remove_value_index = result.indexOf(""+id);
	      result.splice( remove_value_index, 1 );
	      $('.hid_access_id').val(array_splice(result));
	      if($('.sss-'+val).length == 0){
	        $("[data-role='"+val+"'][data-cls='View']" ).attr('checked',false);
	      }
	      if($("[data-role='"+val+"'][data-cls='View']" ).hasClass('sss-'+val)){
	        $("[data-role='"+val+"'][data-cls='View']" ).attr('checked',true);
	      }else{
	        $("[data-role='"+val+"'][data-cls='View']" ).attr('checked',false);
	      }
	    }
	  }
	  function array_splice(ar){
	     	for (var i = 0; i < ar.length; i++) {
		      	if (ar[i] == "") {         
		        	ar.splice(i, 1);
		      	}
	      		return ar;
			}
	  	}
</script>