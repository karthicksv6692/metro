<?php 
	if(isset($role_obj) && count($role_obj)>0){

		if($role_obj->getUserRoleId() != null)	{
			$role_id = $role_obj->getUserRoleId();
		}else{$role_id = "";}

		if($role_obj->getUserRoleName() != null)	{
			$role_name = $role_obj->getUserRoleName();
		}else{$role_name = "";}

		if($role_obj->getUserRoleDesc() != null)	{
			$role_desc = $role_obj->getUserRoleDesc();
		}else{$role_desc = "";}

		if($role_obj->getUserRoleAccessIds() != null){
			$access_id = $role_obj->getUserRoleAccessIds();
		}else{$access_id = "";}


?>
	<div class="container-fluid">
		<div class="page-header">
			<h3>User Role</h3>
		</div>
		<form method="post" action="<?php echo base_url('index.php/user/user_role_crud/Delete'); ?>">
			<div class="row full_width">
				<div class="col-md-5 mt-5 ">
					<table style="width: 100%" class="single_view_table">
						<tr>
							<td class="single_table_view_head">Role Name</td>
							<td class="single_table_view_head">:</td>
							<td class="single_table_view_data"><?php echo $role_name; ?></td>
						</tr>
						<tr>
							<td class="single_table_view_head">Description</td>
							<td class="single_table_view_head">:</td>
							<td class="single_table_view_data"><?php echo $role_desc; ?></td>
						</tr>
						
					</table>
				</div>

				<div class="col-md-7 pull-left mt-5 ">
					<h4 class="userrole_addaccess">Access</h4>
					<?php if($module_list != null){  ?>
						<?php if(count($module_list)>0 && count($access_list)>0){ ?>	
							<?php foreach ($module_list as $m_list) { ?>


								<div class="row rr mt-3 ml-2 hid_div_svk">
									<div class="checkboxFive">
			                        	
			                        	<span class="check"></span> <?php if(isset($m_list)){if($m_list->getModuleName() != null){echo $m_list->getModuleName();}} ?>
			                        </div>
			                    </div>
		                      	<div class="row rr mt-3 ml-2">
		                      		<?php foreach ($access_list as $a_list) { 
					                    if($a_list->getModuleId()->getModuleId() == $m_list->getModuleId()){
					               	?>
					               		

					               				<?php 
					               					if($access_id != null){
						               					$ar = explode(',',$access_id); 
						               					if(in_array($a_list->getAccessId(), $ar)){ ?>
						               						<div class="checkboxFive pull-left pr-3 cc">
									                        	<span class="checkbox-material ml-4">
									                        		<span class="check"></span> <?php echo $a_list->getAccessName(); ?>
									                        	</span>
									                        </div>
									                <?php
						               					}
					               					} ?>


				                        
			                        <?php }} ?>
			                   	</div>
			                <?php } ?>
		                <?php }?>
		            <?php } ?>
				</div>
			</div>


			<input type="hidden" class="hid_access_id" name="hid_access_id" value="<?php echo $access_id; ?>">
			<input type="hidden" name="delete_user_role_id" value="<?php echo $role_id; ?>">
			<div class="country_width_100 col-12 mt-5">
				<div class="country_width_100">
					<div class="butt_sec_width mt-3 mb-3">
				      	<button type="submit" name="country_submit" class="country_button mr-2" >DELETE <i class="fa fa-paper-plane" aria-hidden="true"></i></button>		
					    <button type="reset" name="country_reset" class="country_button">RESET <i class="fa fa-refresh" aria-hidden="true"></i></button>
					</div>
			  	</div>
			  	
			    <div class="country_width_100 mt-3 mb-3">					      		  
				    <div class="country-right">
				      	<a href="<?php echo base_url('index.php/user/user_role_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button></a>
				    </div>	
				</div>
			</div>
		</form>
	</div>	

	<script type="text/javascript">

	$.expr[':'].blank = function(obj){
	  return obj.innerHTML.trim().length === 0;
	};
	$('.row.rr.mt-3.ml-2:blank').addClass("hid_for_script") 
	$('.hid_for_script').prev().addClass('none');
	$('hid_for_script').addClass('none')
	</script>	

	<style type="text/css">
		.none{
			display: none;
		}
	</style>

<?php }else{ ?>
	<div class="dkbody">
		<div class="container-fluid">
			<div class="page-header"> <h3>Select</h3> </div>
			<?php echo $search; ?>
		</div>
	</div>
	<script type="text/javascript"> $("#combobox").select2(); </script>
<?php }  ?>