<?php if(isset($module_list)){ 
	if(count($module_list) > 0){
?>
		<?php if(isset($access_obj)){ 


			if($access_obj->getAccessId() != null)	{
				$access_id = $access_obj->getAccessId();
			}else{$access_id = "";}

			if($access_obj->getAccessName() != null)	{
				$access_name = $access_obj->getAccessName();
			}else{$access_name = "";}

			if($access_obj->getAccessUrl() != null)	{
				$access_url = $access_obj->getAccessUrl();
			}else{$access_url = "";}

			if($access_obj->getModuleId() != null)	{
				$module_id = $access_obj->getModuleId()->getModuleId();
			}else{$module_id = "";}


		?>

			<div class="dkbody">
				<div class="container-fluid">
					<div class="page-header">
						<h3>Access Details</h3>
					</div>
					
					<div class="page-content">
						<section class="content bgcolor-1">
							<form method="post" action="<?php echo base_url('index.php/user/access_crud/edit'); ?>" id="access_edit">
								<div class="col-md-5 row form-group">
									<span class="input input--nao">
										<input class="input__field input__field--nao" type="text" id="access_add_name"
										  name="access_name" value="<?php echo $access_name; ?>" 
										  data-validation="required length alphanumeric"
					                       data-validation-length="min3"
					                       data-validation-ignore=" "
										  />
										<label class="input__label input__label--nao" for="access_add_name">
											<span class="input__label-content input__label-content--nao">Access Name</span>
										</label>
										<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
											<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
										</svg>
									</span>		
								</div>
								<div class="col-md-5 row form-group">
									<span class="input input--nao">
										<input class="input__field input__field--nao" type="text" id="access_add_url"
										  name="access_url" value="<?php echo $access_url; ?>"
										   data-validation="required length alphanumeric"
					                       data-validation-length="min3"
					                       data-validation-ignore="&/-_ :"
										  />
										<label class="input__label input__label--nao" for="access_add_url">
											<span class="input__label-content input__label-content--nao">Access Url</span>
										</label>
										<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
											<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
										</svg>
									</span>		
								</div>
								<div class="col-md-5 row ">
									<label for="id_label_single" class="input input--nao">
										<select class="js-example-basic-single select2_style module" name="module_id"
										 data-validation="required number"
										>
											<?php foreach ($module_list as $list) { ?>
												<option value="<?php echo $list->getModuleId(); ?>" <?php if($module_id == $list->getModuleId()){echo "selected='selected'";} ?>><?php echo $list->getModuleName(); ?></option>
											<?php } ?>
										</select>
									</label>
								</div>
								<input type="hidden" name="edit_access_id" value="<?php echo $access_id; ?>">
								<div class="country_width_100 col-12 mt-5">
									<div class="country_width_100">
										<div class="butt_sec_width mt-3 mb-3">
									      	<button type="submit" name="moduleadd_submit" class="country_button mr-2" >SUBMIT <i class="fa fa-paper-plane" aria-hidden="true"></i></button>		
										    <button type="reset" name="moduleadd_reset" class="country_button">RESET <i class="fa fa-refresh" aria-hidden="true"></i></button>
								      	
										</div>
							      	</div>
							      	
							        <div class="country_width_100 mt-3 mb-3">					      		  
									    <div class="country-right">
									      	<a href="<?php echo base_url('index.php/user/Access_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button>
								    </div>	</a>
									    </div>	
									</div>
								</div>
							</form>
						</section>
					</div>
				</div>
			</div>
		<?php }else{ ?>

			<div class="dkbody">
				<div class="container-fluid">
					<div class="page-header">
						<h3>Select</h3>
					</div>
					<?php echo $search; ?>
				</div>
			</div>


			<script type="text/javascript">
				$("#combobox").select2();
			</script>

		<?php }  ?>







	<?php }else{
		echo "Module is Empty. Add Module First";
		} ?>
<?php } ?>

<script type="text/javascript">
	$(function(){
		$('.module').select2();
	})
	$.validate({
	    form:'#access_edit',
	});  
</script>