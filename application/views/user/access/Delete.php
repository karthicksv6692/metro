<?php if(isset($access_obj)){ 


	if($access_obj->getAccessId() != null)	{
		$access_id = $access_obj->getAccessId();
	}else{$access_id = "";}

	if($access_obj->getAccessName() != null)	{
		$access_name = $access_obj->getAccessName();
	}else{$access_name = "";}

	if($access_obj->getAccessUrl() != null)	{
		$access_url = $access_obj->getAccessUrl();
	}else{$access_url = "";}

	if($access_obj->getModuleId() != null)	{
		$module_id = $access_obj->getModuleId()->getModuleName();
	}else{$module_id = "";}


?>
	<div class="dkbody">
		<div class="container-fluid">
			<div class="page-header">
				<h3>Access Details</h3>
			</div>
			<form method="post" action="<?php echo base_url('index.php/user/Access_crud/delete'); ?>">
				<div class="page-content">
					<section class="content bgcolor-1">
						<div class="col-md-6 pull-left table-responsive">
							<table style="width: 100%" class="single_view_table ">
								<tr>
									<td class="single_table_view_head">Access Name</td>
									<td class="single_table_view_head">:</td>
									<td class="single_table_view_data"><?php echo $access_name; ?></td>
								</tr>
								<tr>
									<td class="single_table_view_head">Access Url</td>
									<td class="single_table_view_head">:</td>
									<td class="single_table_view_data"><?php echo $access_url; ?></td>
								</tr>
								<tr>
									<td class="single_table_view_head">Module</td>
									<td class="single_table_view_head">:</td>
									<td class="single_table_view_data"><?php echo $module_id; ?></td>
								</tr>
								
							</table>
						</div>
						<input type="hidden" name="delete_access_id" value="<?php echo $access_id; ?>">
						<div class="country_width_100 col-12 mt-5">
								<div class="country_width_100">
									<div class="butt_sec_width mt-3 mb-3">
								      	<button type="submit" name="country_submit" class="country_button mr-2" >EDIT <i class="fa fa-pencil" aria-hidden="true"></i></button>		
									    <a href="<?php echo base_url('index.php/user/Access_crud'); ?>"><button type="button" name="country_reset" class="country_button">CANCEL <i class="fa fa-trash" aria-hidden="true"></i></button></a>
							      	
									</div>
						      	</div>
						      	
						        <div class="country_width_100 mt-3 mb-3">					      		  
								    <div class="country-right">
								      	<a href="<?php echo base_url('index.php/user/Access_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button>
								    </div>	</a>
								</div>
							</div>
					</section>
				</div>
			</form>
		</div>
	</div>
<?php }else{ ?>

	<div class="dkbody">
		<div class="container-fluid">
			<div class="page-header">
				<h3>Select</h3>
			</div>
			<?php echo $search; ?>
		</div>
	</div>


	<script type="text/javascript">
		$("#combobox").select2();
	</script>

<?php }  ?>