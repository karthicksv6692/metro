<?php 
	if(isset($obj)){
	
	if(count($obj) > 0){  //print_r($obj);exit();



		if($obj->getUserId() != null){$user_id = $obj->getUserId();}else{$user_id = "";}
		if($obj->getUserName() != null){$user_name = $obj->getUserName();}else{$user_name = "";}
		if($obj->getLastName() != null){$last_name = $obj->getLastName();}else{$last_name = "";}

		if($obj->getUuFirstName() != null){$uu_first_name = $obj->getUuFirstName();}else{$uu_first_name = "";}
		if($obj->getDob() != null){$dob = $obj->getDob();}else{$dob = "";}
		if($obj->getGender() != null){$gender = $obj->getGender();}else{$gender = "";}
		if($obj->getPhoneNo() != null){$phone_no = $obj->getPhoneNo();}else{$phone_no = "";}
		if($obj->getEmail() != null){$email = $obj->getEmail();}else{$email = "";}

		if($obj->getContactId() != null){$contact_id = $obj->getContactId();}else{$contact_id = "";}

		if($obj->getNomineeName() != null){$nominee_name = $obj->getNomineeName();}else{$nominee_name = "";}
		if($obj->getRelationship() != null){$relationship = $obj->getRelationship();}else{$relationship = "";}
		if($obj->getNDob() != null){$n_dob = $obj->getNDob();}else{$n_dob = "";}
		if($obj->getNGender() != null){$n_gender = $obj->getNGender();}else{$n_gender = "";}
		if($obj->getNPhoneNo() != null){$n_phone_no = $obj->getNPhoneNo();}else{$n_phone_no = "";}

		if($obj->getFullAddress() != null){$full_address = $obj->getFullAddress();}else{$full_address = "";}
		if($obj->getPincode() != null){$pincode = $obj->getPincode();}else{$pincode = "";}
		if($obj->getDistrict() != null){$district = $obj->getDistrict();}else{$district = "";}
		if($obj->getState() != null){$state = $obj->getState();}else{$state = "";}
		if($obj->getCountry() != null){$country = $obj->getCountry();}else{$country = "";}


		if($obj->getAadharNo() != null){$aadhar_no = $obj->getAadharNo();}else{$aadhar_no = "";}
		if($obj->getPanNo() != null){$pan_no = $obj->getPanNo();}else{$pan_no = "";}

		if($obj->getSecondaryPhone() != null){$alter_mob = $obj->getSecondaryPhone();}else{$alter_mob = "";}
		if($obj->getCountryId() != null){$country_id = $obj->getCountryId();}else{$country_id = "";}
		if($obj->getRoleId() != null){$role_id = $obj->getRoleId();}else{$role_id = "";}
		if($obj->getAccessIds() != null){$access_id = $obj->getAccessIds();}else{$access_id = "";}


		//$referal_id = "";$sponcer_name = "";$placement_id = "";$placement_name = "";$position = "";$pin_no = "";$package_name = "";$products = "";$products_count = "";$uu_first_name = "";$dob = "";$gender = "";$phone_no = "";$email = "";$nominee_name = "";$relationship = "";$n_dob = "";$n_gender = "";$n_phone_no = "";$full_address = "";$pincode = "";$district = "";$state = "";$country = "";$account_holder_name = "";$account_number = "";$account_type = "";$ifsc = "";$bank_name = "";$bank_branch = "";$aadhar_no = "";$pan_no = "";$pass_no = "";$aadhar_front = "";$aadhar_back = "";$pan_front = "";$pan_back = "";$pass_front = "";$getPassBack = "";$alter_mob = "";$country_id = "";$role_id = "";$access_id = "";$user_id="";$last_name="";$user_name="";$contact_id = "";$address_id = "";$password_id = "";


	}else{$referal_id = "";$sponcer_name = "";$placement_id = "";$placement_name = "";$position = "";$pin_no = "";$package_name = "";$products = "";$products_count = "";$uu_first_name = "";$dob = "";$gender = "";$phone_no = "";$email = "";$nominee_name = "";$relationship = "";$n_dob = "";$n_gender = "";$n_phone_no = "";$full_address = "";$pincode = "";$district = "";$state = "";$country = "";$account_holder_name = "";$account_number = "";$account_type = "";$ifsc = "";$bank_name = "";$bank_branch = "";$aadhar_no = "";$pan_no = "";$pass_no = "";$aadhar_front = "";$aadhar_back = "";$pan_front = "";$pan_back = "";$pass_front = "";$getPassBack = "";$alter_mob = "";$country_id = "";$role_id = "";$access_id = "";$user_id="";$last_name="";$user_name="";$contact_id = "";$address_id = "";$password_id = "";} 

?>

	<div class="dkbody">
		<div class="container-fluid">
			<div class="page-header">
				<h3>User Module</h3>
			</div>
			<p class="bank_add_details"><span><i class="fa fa-user" aria-hidden="true"></i></span> Profile Details</p>
			<div class="page-content">
				<section class="content bgcolor-1">
					<form id='user_edit' method="post" action="<?php echo base_url('index.php/user/user_crud/edit'); ?>" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-10 pull-left">
								<div class="row full_width">
									<div class="col-md-3 pull-left form-group">
										<span class="input input--nao">
											<input class="input__field input__field--nao user_name" type="text" id="user_profile_username"
											 name="user_name" value="<?php echo $user_name; ?>" 
										data-validation="required custom length"
										data-validation-length="3-100"
										data-validation-regexp="^[A-Za-z][A-Za-z0-9&amp;]+$"
										data-validation-error-msg-custom="Please provide a valid user name"
											 />
											<label class="input__label input__label--nao" for="user_profile_username">
												<span class="input__label-content input__label-content--nao">User Name</span>
											</label>
											<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
												<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
											</svg>
											<img src="<?php echo base_url('assets/images/loader/') ?>default.gif" class="ldr uname_ldr">
										</span>		
										<p class="uname_suc avail_er suc">Available</p>
										<p class="uname_err avail_er err">Already Exists</p>	
										<?php echo form_error('user_name','<span class="help-block form-error">', '</span>'); ?>	
									</div>

									<div class="col-md-3 pull-left row form-group">
										<span class="input input--nao">
											<input class="input__field input__field--nao" type="text" id="user_profile_lname"
											  name="first_name" value="<?php echo $uu_first_name; ?>"
											  data-validation=" required length alphanumeric"
											data-validation-ignore="."
											data-validation-length="min3"
											   />
											<label class="input__label input__label--nao" for="user_profile_lname">
												<span class="input__label-content input__label-content--nao">First Name</span>
											</label>
											<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
												<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></path>
											</svg>
										</span>		
										<?php echo form_error('first_name','<span class="help-block form-error">', '</span>'); ?>
									</div>

									<div class="col-md-3 pull-left row form-group">
										<span class="input input--nao">
											<input class="input__field input__field--nao" type="text" id="user_profile_lname"
											  name="last_name" value="<?php echo $last_name; ?>" 
											   data-validation="length alphanumeric"
										  data-validation-ignore="."
										  data-validation-length="min3"
										  data-validation-optional="true"
											  />
											<label class="input__label input__label--nao" for="user_profile_lname">
												<span class="input__label-content input__label-content--nao">Last Name</span>
											</label>
											<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
												<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></path>
											</svg>
										</span>		
										<?php echo form_error('last_name','<span class="help-block form-error">', '</span>'); ?>
									</div>

									<div class="col-md-3 pull-left mt-5 pad_zero">
										<div class="form-group radio_box">
											<label>Gender</label> 
											<label class="chkbo" style="display:inline">M
												<input type="radio" id="radio" name="gender" value="1" <?php if($gender == 1){echo "checked='checked'";} ?>data-validation="required" >
												<label for="radio"></label>
											</label>
											<label class="chkbo" style="display:inline" <?php if($gender == 0){echo "checked='checked'";} ?>> F
												<input type="radio" id="radio2" name="gender"  value="0" data-validation="required" >
												<label for="radio2"></label>
											</label>
											<label class="chkbo" style="display:inline" <?php if($gender == 2){echo "checked='checked'";} ?>>O
												<input type="radio" id="radio3"  name="gender"  value="2" data-validation="required">
												<label for="radio3"></label>
											</label>
										</div>
									</div>
									<?php echo form_error('gender','<span class="help-block form-error">', '</span>'); ?>
								</div>

								<div class="row full_width">
									<div class="col-md-3 pull-left form-group">
										<span class="input input--nao" id="sandbox-container">
											<input type="text" class="input__field input__field--nao form-control mar_tp_5px"  name="dob" value="<?php echo $dob; ?>"  data-validation="date" data-validation-format="yyyy-mm-dd">
											<label class="input__label input__label--nao" for="input-1">
												<span class="input__label-content input__label-content--nao">Date of Birth </span>
											</label>
											<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
												<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
											</svg>
										</span>	
										<?php echo form_error('dob','<span class="help-block form-error">', '</span>'); ?>						
									</div>

									<div class="col-md-3 pull-left row form-group">
										<span class="input input--nao">
											<input class="input__field input__field--nao user_email" type="email" id="user_profile_emailid"
											  name="user_email" value="<?php echo $email; ?>" 
											  data-validation="required email"
											  />
											<label class="input__label input__label--nao" for="user_profile_emailid">
												<span class="input__label-content input__label-content--nao">Email ID</span>
											</label>
											<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
												<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
											</svg>
											<img src="<?php echo base_url('assets/images/loader/') ?>default.gif" class="ldr umail_ldr">
										</span>		
										<p class="umail_suc avail_er suc">Available</p>
										<p class="umail_err avail_er err">Already Exists</p>
										<?php echo form_error('user_email','<span class="help-block form-error">', '</span>'); ?>
									</div>
									<div class="col-md-3 pull-left row form-group">
										<span class="input input--nao">
											<input class="input__field input__field--nao mobile" type="number" id="user_profile_primary_phoneno"
											  name="mobile" value="<?php echo $phone_no; ?>"
											  data-validation="required length alphanumeric"
											data-validation-ignore="+ " 
											data-validation-length="10-12"
											  />
											<label class="input__label input__label--nao" for="user_profile_primary_phoneno">
												<span class="input__label-content input__label-content--nao">Primary Phone No</span>
											</label>
											<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
												<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
											</svg>
											<img src="<?php echo base_url('assets/images/loader/') ?>default.gif" class="ldr umob_ldr">

										</span>		
										<p class="umob_suc avail_er suc">Available</p>
										<p class="umob_err avail_er err">Already Exists</p>
										<?php echo form_error('mobile','<span class="help-block form-error">', '</span>'); ?>
									</div>
									<div class="col-md-3 pull-left row form-group">
										<span class="input input--nao">
											<input class="input__field input__field--nao" type="number" id="user_profile_secondary_phoneno"
											  name="alter_mob" value="<?php echo $alter_mob; ?>" 
											   data-validation="required length alphanumeric"
											data-validation-ignore="+ " 
											data-validation-length="10-12"
											data-validation-optional="true"
											  />
											<label class="input__label input__label--nao" for="user_profile_secondary_phoneno">
												<span class="input__label-content input__label-content--nao">Secondary Phone No</span>
											</label>
											<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
												<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
											</svg>
										</span>		
										<?php echo form_error('alter_mob','<span class="help-block form-error">', '</span>'); ?>
									</div>
								</div>
							</div>
							<div class="col-md-2 pull-left">
								<div class="col-12 user_module pkfile" id="user_access_pofile_image">
		                            <div class="toppkfile" style="float:left">
		                                <?php if(isset($attachment) && count($attachment) > 0){  ?>
		                                	<div class="user_profile_default_image">
	                                	 		<img src="<?php echo base_url(''); ?><?php echo $attachment[0]->getAttachmentImageUrl();?>" class="img-responsive user_profile_image">
	                                	 	</div>	
		                                <?php }else{ ?>
			                                <div class="user_profile_default_image">
			                                    <img src="<?php echo base_url('assets/'); ?>images/user_photo.png" class="img-responsive user_profile_image">
			                                </div>
			                            <?php } ?>
		                            </div>
		                        </div>
		                        <input type="file"  name="file" id="user_profile_image" class="form-control img-responsive">
							</div>
						</div>
						<p class="bank_add_details"><span><i class="fa fa-user" aria-hidden="true"></i></span> Nominee Details</p>
						<div class="row full_width">
							<div class="col-md-3 pull-left form-group">
								<span class="input input--nao">
									<input class="input__field input__field--nao" type="text" id="user_nominee_name"
									 name="n_name" value="<?php echo $nominee_name; ?>" 
								data-validation="required custom length"
								data-validation-length="2-100"
								data-validation-regexp="^[A-Za-z][A-Za-z ]+$"
								data-validation-error-msg-custom="Please provide valid nominee name"
									 />
									<label class="input__label input__label--nao" for="user_nominee_name">
										<span class="input__label-content input__label-content--nao">Nominee Name</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></path>
									</svg>
								</span>		
								<?php echo form_error('n_name','<span class="help-block form-error">', '</span>'); ?>
							</div>
							<div class="col-md-2 pull-left form-group">
								<span class="input input--nao" id="sandbox-container">
									<input type="text" class="input__field input__field--nao form-control"  name="n_dob" value="<?php echo $n_dob; ?>"
									data-validation="date" 
									data-validation-format="yyyy-mm-dd"
									>
									<label class="input__label input__label--nao" for="input-1">
										<span class="input__label-content input__label-content--nao">Date of Birth </span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
								</span>	
								<?php echo form_error('n_dob','<span class="help-block form-error">', '</span>'); ?>						
							</div>
							<div class="col-md-2 pull-left form-group">
								<label for="id_label_single" class="input input--nao">
									<select class="js-example-basic-single1 select2_style relation_select usr_edt" name="n_relationship" data-validation="required">
										<option class="copmbo"value="" selected="selected">Relationship</option>
										<option value="Father" <?php if($relationship == "Father"){echo"selected='selected'";} ?>>Father</option>
										<option value="Mother" <?php if($relationship == "Mother"){echo"selected='selected'";} ?>>Mother</option>
										<option value="Son" <?php if($relationship == "Son"){echo"selected='selected'";} ?>>Son</option>
										<option value="Daughter" <?php if($relationship == "Daughter"){echo"selected='selected'";} ?>>Daughter</option>
										<option value="Wife" <?php if($relationship == "Wife"){echo"selected='selected'";} ?>>Wife</option>
										<option value="Brother" <?php if($relationship == "Brother"){echo"selected='selected'";} ?>>Brother</option>
										<option value="Sister" <?php if($relationship == "Sister"){echo"selected='selected'";} ?>>Sister</option>
									</select>
								</label>		
								<?php echo form_error('n_relationship','<span class="help-block form-error">', '</span>'); ?>	
							</div>
							<div class="col-md-2 pull-left form-group">
								<span class="input input--nao">
									<input class="input__field input__field--nao" type="number" id="user_nominee_phoneno"
									 name="n_mob" value="<?php echo $n_phone_no; ?>" 
									  data-validation="required number length"
									  data-validation-ignore="+-" 
			                          data-validation-length="10-12"
									 />
									<label class="input__label input__label--nao" for="user_nominee_phoneno">
										<span class="input__label-content input__label-content--nao">Phone No</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
									<?php echo form_error('n_mob','<span class="help-block form-error">', '</span>'); ?>
								</span>		
							</div>

							<div class="col-md-3 pull-left row mt-5 height_30px">
								<div class="form-group radio_box">
									<span class="usermodulegender">Gender</span>
									<label class="chkbo" style="display:inline">M
										<input type="radio" id="radio" name="n_gender" value="1" <?php if($n_gender == 1){echo "checked";} ?>  data-validation="required">
										<label for="radio"></label>
									</label>
									<label class="chkbo" style="display:inline"> F
										<input type="radio" id="radio2" name="n_gender"  value="0" <?php if($n_gender == 0){echo "checked";} ?>  data-validation="required">
										<label for="radio2"></label>
									</label>
									<label class="chkbo" style="display:inline" >O
										<input type="radio" id="radio3"  name="n_gender"  value="2" <?php if($n_gender == 2){echo "checked";} ?>  data-validation="required">
										<label for="radio3"></label>
									</label>
								</div>
							</div>
						</div>
						<p class="bank_add_details"><span><i class="fa fa-user" aria-hidden="true"></i></span> Communication Details</p>
						<div class="row full_width">
							<div class="col-md-4 pull-left form-group">
								<span class="input input--nao">
									<input class="input__field input__field--nao" type="text" id="user_communication_full_address"
									 name="full_address" value="<?php echo $full_address; ?>" 
									 data-validation="length"
								data-validation-optional="true"
								data-validation-length="min10"
								data-validaton-optional="true"
									 />
									<label class="input__label input__label--nao" for="user_communication_full_address">
										<span class="input__label-content input__label-content--nao">Full Address</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
								</span>		
								<?php echo form_error('full_address','<span class="help-block form-error">', '</span>'); ?>
							</div>
							<div class="col-md-2 pull-left form-group">
								<span class="input input--nao">
									<input class="input__field input__field--nao pincode_no" type="number" id="user_communication_pincode" name="pincode_no" value="<?php echo $pincode; ?>" 
									data-validational="length required"
									data-validation-length="4-6"
									/>
									<label class="input__label input__label--nao" for="user_communication_pincode">
										<span class="input__label-content input__label-content--nao">Pincode</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
								</span>		
								<?php echo form_error('pincode_no','<span class="help-block form-error">', '</span>'); ?>
							</div>
							<div class="col-md-2 pull-left form-group">
								<span class="input input--nao district_lbl">
									<input class="input__field input__field--nao district" type="text" id="user_communication_district"
									 name="district" value="<?php echo $district; ?>" 
									 data-validation="required custom length" 
 									data-validation-length="2-100"
									data-validation-regexp="^[A-Za-z][A-Za-z0-9-_\ .&amp;]+$"
									data-validation-error-msg-custom="Provide valid district name"
									 />
									<label class="input__label input__label--nao" for="user_communication_district">
										<span class="input__label-content input__label-content--nao">District</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
								</span>		
								<?php echo form_error('district','<span class="help-block form-error">', '</span>'); ?>
							</div>
							<div class="col-md-2 pull-left form-group">
								<span class="input input--nao state_lbl">
									<input class="input__field input__field--nao state" type="text" id="user_communication_state"
									 name="state" value="<?php echo $state; ?>" 
									 data-validation="required custom length" 
 									data-validation-length="2-100"
									data-validation-regexp="^[A-Za-z][A-Za-z0-9-_\ .&amp;]+$"
									data-validation-error-msg-custom="Provide valid district name"
									 />
									<label class="input__label input__label--nao" for="user_communication_state">
										<span class="input__label-content input__label-content--nao">State</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
								</span>		
								<?php echo form_error('state','<span class="help-block form-error">', '</span>'); ?>
							</div>
							<div class="col-md-2 pull-left cntry form-group">
								<select class="js-example-basic-single select2_style ad_usr country" name="country_id" id="combobox">
									<?php foreach ($country_list as $list) {?>
										<option value="<?php echo $list->getCountryId();  ?>" <?php if($country == $list->getCountryId()){echo "selected='selected'";} ?>><?php echo $list->getCountryName();  ?></option>
									<?php  } ?>
								</select>
								<?php echo form_error('country_id','<span class="help-block form-error">', '</span>'); ?>	
							</div>
						</div>

					</div>


						<input type="hidden" class="img_list" name="img_list" value="">
						<input type="hidden" name="edit_user_id" value="<?php echo $user_id; ?>">

						<input type="hidden" name="address_id" value="<?php echo $obj->getAddressId(); ?>">
			     		<input type="hidden" name="contact_id" value="<?php echo $obj->getContactId(); ?>">
			     		<input type="hidden" name="password_id" value="<?php echo $obj->getPasswordId(); ?>">
						<div class="country_width_100 col-12 mt-5">
							<div class="country_width_100">
								<div class="butt_sec_width mt-3 mb-3">
							      	<button type="submit" name="country_submit" class="country_button mr-2" >SUBMIT <i class="fa fa-paper-plane" aria-hidden="true"></i></button>		
								    <button type="reset" name="country_reset" class="country_button">RESET <i class="fa fa-refresh" aria-hidden="true"></i></button>
								</div>
					      	</div>
					      	
					        <div class="country_width_100 mt-3 mb-3">					      		  
							    <div class="country-right">
							      	<a href="<?php echo base_url('index.php/login/login_user'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button></a>
							    </div>	
							</div>
						</div>

					</form>
				</section>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$.validate({
		 	form:'#user_edit'
		 }); 
		$(document).ready(function() { 
			$('.we_need').hide();
			$(".js-example-basic-single").select2({ 
				placeholder: "Select a Role",
	  			allowClear: true
	  		}); 

	  		$('.relation_select').select2({ 
				placeholder: "Relationship",
	  			allowClear: true
	  		}); 
	  		$(".relation_select").val('<?php echo $relationship; ?>').trigger('change');
		});
	</script>
	<script type="text/javascript">
	    function handleFileSelect(evt) 
	    { 
	        //console.log(evt.target.id);
	        var files = evt.target.files; // FileList object // Loop through the FileList and render image files as thumbnails. 
	        for (var i = 0, f; f = files[i]; i++) 
	        { 
	         if (!f.type.match('image.*')) 
	            { 
	                continue; 
	            } 
	            var reader = new FileReader(); // Closure to capture the file information. 
	            reader.onload = (function(theFile) 
	            { 
	                console.log(evt.target.id);
	                return function(e) 
	                { 
	                    if(evt.target.id=="user_profile_image")
	                    {
	                        $('#user_access_pofile_image #user_profile_image').remove();
	                        $('#user_access_pofile_image .user_profile_image').remove();
	                        $('#user_access_pofile_image .col-12.p1-0.mr-2.mb-2.bdrstyle_hover').remove();
	                        $('#user_access_pofile_image').prepend('<div class="col-12 p1-0 pl-0 pr-0 mr-2 mb-2 bdrstyle_hover" style="float:left"><img class="width-60px bdrstyle" style="width:100%;height:auto" src="'+e.target.result+'"><span>X</span></div>'); 
	                        $('.bdrstyle_hover>span').on('click',function(){
	                            $(this).parent().remove(); 
	                            $('.toppkfile').prepend('<input type="file" name="user_profile_image[]" id="user_profile_image" class="form-control img-responsive">');
	                            $('#user_access_pofile_image .user_profile_default_image').prepend('<img src="<?php echo base_url('assets/'); ?>images/user_photo.png" class="img-responsive user_profile_image">');
	                                            
	                        }); 
	                    }
	                    
	                }; 
	            })(f); 
	            reader.readAsDataURL(f); 
	        } 
	    } 
	    $('.bdrstyle_hover>span').on('click',function(){ $(this).parent().remove(); });
	    document.getElementById('user_profile_image').addEventListener('change', handleFileSelect, false); 
	</script>
	<style type="text/css">
		.page-content section.content.bgcolor-1 form span.select2.select2-container.select2-container--default.select2-container--focus, span.select2.select2-container.select2-container--default.select2-container--below, span.select2.select2-container.select2-container--default {
		    width: 170px !important;
		    margin: 30px auto !important;
		    margin-top: 15px !important;
		    text-align: left;
		}
		.page-content section.content.bgcolor-1 form  .cntry span.select2.select2-container.select2-container--default.select2-container--focus, .page-content section.content.bgcolor-1 form  .cntry span.select2.select2-container.select2-container--default.select2-container--below, s.page-content section.content.bgcolor-1 form  .cntry pan.select2.select2-container.select2-container--default {
		    width: 170px !important;
		    margin-top: 45px !important;
		    text-align: left;
		}
		 .cntry span.select2.select2-container.select2-container--default{
		 	 margin-top: 45px !important;
		 }
	</style>
	<script type="text/javascript">

		$( function() { 
	      
	      	//$('.district,.state,.country').attr('disabled', 'disabled');
	      	$('.uname_ldr,.umail_ldr,.umob_ldr').hide();
			$('.uname_suc,.uname_err,.umail_suc,.umail_err,.umob_suc,.umob_err').hide();
	    });

	    //Pincode Ajax Start
		    $('.pincode_no').blur(function()  {
		  		var pincode = $('.pincode_no').val();
		      	if(pincode.length > 0){
			      	$.ajax({
			        type: "POST",
			        url: "<?php echo base_url(); ?>index.php/Common_controller/ajax_pincode",
			        data: {'pincode':pincode},
		          	success:  function (data) {
		          		if(data != 0){
				            var option = JSON.parse(data);
				            console.log(option);
				            if(option[2]['pincode_district'] == false){
				              $('.district').val("");
				              $('.district').attr('disabled', 'disabled');
				            }else{
				              $('.district_lbl').addClass('input--filled');
				              $('.district').val(option[2]['pincode_district']).removeAttr('disabled');
				              $('.district').attr('disabled', false);
				            }
				            if(option[3]['pincode_state'] == false){
				              $('.state').val("");
				              $('.state').attr('disabled', 'disabled');
				            }else{
				              $('.state_lbl').addClass('input--filled');
				              $('.state').attr('disabled', 'false');
				              $('.state').val(option[3]['pincode_state']).removeAttr('disabled');
				            }
				            if(option[6]['pincode_country'] == false){
				              $('.country').val("");
				              $('.country').attr('disabled', 'disabled');
				            }else{
				             	$('.country_lbl').addClass('input--filled');
			              		$('.country').val(option[6]['country_id']).trigger('change');  
				            }
				        }else{
					      	 $('.district,.state,.country').val('');
				        	$('.state_label,.district_label,.country_label').removeClass('input--filled');
					      	 $('.district,.state,.country').attr('disabled', 'disabled');
				        }
		          	}
		      	});
		      	}else{
		      		$('.state_label,.district_label,.country_label').removeClass('input--filled');
		      	 	$('.district,.state,.country').attr('disabled', 'disabled');
		      	 	$('.district,.state,.country').val('');
		      	}
		    });
	  	//Pincode Ajax End
		
	  
	    
	    function validateEmail(email) {
		  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		  return re.test(email);
		}
	</script>
	<script>
		  	function readURL(input,class_name) {
			    if (input.files && input.files[0]) {
			        var reader = new FileReader();
			        reader.onload = function (e) {
			            $('.'+class_name+'_img').prepend('<div class="p1-0 mr-2 mb-2 bdrstyle_hover" style="float:left"><img class="width-60px bdrstyle" src="'+e.target.result+'"><span>X</span></div>');
			            $('.'+class_name+'_status').val(1);
			           $('.'+class_name+'_xx').val(1);
			        }
			        reader.readAsDataURL(input.files[0]);
			    }
			}

			$(".filesx").change(function(){
			    readURL(this,$(this).attr('name'));
			});
			$('.bdrstyle_hover>span').on('click',function(){
				id = $(this).parent().attr('id');
				$('.'+id+'_status').val(1);
				$('.'+id+'_xx').val(1);
				$('.'+id+'_check').prop('checked', false);
				$(this).parent().remove();
			});	
			$('.skip_cbox').click(function(){
				if($(this).is(':checked')){
					$('.we_need').show();
				}else{$('.we_need').hide()}
			})
	</script>
	<style type="text/css">
		.pkfile input.form-control {
		    opacity: 0;
		    height: 90px;
		    position: absolute;
		    width: 95px;
		}
		img.width-60px.bdrstyle {
		    width: 90px;
		    height: 80px;
		    padding: 2px;
		    border: 1px solid #d7d7d7;
		    margin-left: -5px;
		}
		.p1-0.mr-2.mb-2.bdrstyle_hover {
		    position: absolute;
		    top: 0;
		}
		.bdrstyle_hover span {
		    right: -12px;
		    top: -6px;
		}
	</style>
<?php }else{ ?>

	<!-- <div class="dkbody">
	<div class="container-fluid">
		<div class="page-header">
			<h3>Select</h3>
		</div>
		<?php echo $search; ?>
	</div>
</div>


<script type="text/javascript">
	$("#combobox").select2();
</script> -->

<?php }  ?>
