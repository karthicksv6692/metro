
<?php //print_r($_SESSION); ?>
<div class="dkbody gj_body">
  <div class="container-fluid">
    <section class="gj_section1">

    <?php if($obj->getKycStatus() == 0){ ?>
      <div class="marquee_div" style="color: #ff2580;margin: 1rem 0px;">
        <marquee><h3><strong>ALERT..! YOUR KYC IS NOT UPDATED. PAYMENT WILL BE HELD UP TILL YOUR KYC IS BEEN APPROVED</strong></h3></marquee>
      </div>
    <?php } ?>

    <?php //print_r($in); ?>

      <div class="rowxx rep_cls">
        <p>Dear Mr.<span class="rep_name" style="color: #ff2580;text-transform: uppercase;">     <?php echo $obj->getUserName(); ?></span></p>
        <p>Metrokings Marketing Private Limited Welcomes You !!!</p>
        <p>Repurchase Balance Rs. : <span class="bll" style="color: #ff2580;text-transform: uppercase;">
          <?php if(isset($repurchase_bal)){
            if($repurchase_bal != null){
              echo $repurchase_bal;
            }else{
              echo"00.00";}
            }else{echo"00.00";} ?>
        </span></p>
        <p>Current Package : <span class="pck_nm" style="color: #ff2580;text-transform: uppercase;">
          <?php if(isset($pck_name)){
            if($pck_name != null){
              echo $pck_name;
            }else{
              echo"Not-Available";}
            }else{echo"Not-Available";} ?>
        </span></p>
      </div>

      <div class="row">
        <div class="col-lg-3 col-md-6">
          <div class="gj_dash_orange">
            <div class="gj_layout">
              <p class="gj_payout">Total Left</p>
              <p class="gj_payout_count"><?php echo $left_sum; ?></p>
              <div class="gj_lay_img_div">
                <!-- <img src="images/layouts/payout.png"  class="gj_lay_img img-responsive"/> -->
              </div>
              <div class="gj_lay_vw_div">
                <a href="<?php echo base_url('index.php/package_purchase/tree'); ?>" class="gj_view_details">View Geanology <i class="fa fa-arrow-right gj_view_arrw"></i></a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="gj_dash_orange gj_dash_green">
            <div class="gj_layout">
              <p class="gj_payout">Total Right</p>
              <p class="gj_payout_count"><?php echo $right_sum; ?></p>
              <div class="gj_lay_img_div">
                <!-- <img src="images/layouts/cutoff.png"  class="gj_lay_img img-responsive"/> -->
              </div>
              <div class="gj_lay_vw_div">
                <a href="<?php echo base_url('index.php/package_purchase/tree'); ?>" class="gj_view_details">View Geanology <i class="fa fa-arrow-right gj_view_arrw"></i></a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="gj_dash_orange gj_dash_pink">
            <div class="gj_layout">
              <p class="gj_payout">Repurchase Bal</p>
              <p class="gj_payout_count_rep" style="font-size: 17px;">
                <?php if(isset($repurchase_bal)){
                if($repurchase_bal != null){
                  echo $repurchase_bal;
                }else{
                  echo"00.00";}
                }else{echo"00.00";} ?>       
              </p>
              <div class="gj_lay_img_div">
                <!-- <img src="images/layouts/ceiling.png"  class="gj_lay_img img-responsive"/> -->
              </div>
              <div class="gj_lay_vw_div">
                <a href="<?php echo base_url('index.php/payout_demo/payout_view'); ?>" class="gj_view_details">View Current Payout <i class="fa fa-arrow-right gj_view_arrw"></i></a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <a href="<?php echo base_url('index.php/login/login_user'); ?>" class="gj_view_details"><div class="gj_dash_orange gj_dash_blue">
            <div class="gj_layout" style="height: 105px !important; position: relative !important;">
              <p class="gj_payout" style="font-size: 22px; font-weight: 600;">View Profile</p>
              <p class="gj_payout_count"></p>
              <div class="gj_lay_img_div">
                <?php 
                  if(isset($_SESSION)){
                    if(isset($_SESSION['profile_picture_url'])){ ?>
                      <?php if(isset($_SESSION['profile_picture_url']) != null){ ?>
                        <img src="<?php echo base_url()?><?php echo $_SESSION['profile_picture_url']; ?>" class="gj_lay_img img-responsive">
                      <?php }else{ ?>
                        <img src="<?php echo base_url('assets/')?>images/user.jpg" class="gj_lay_img img-responsive">
                      <?php } ?>
                    <?php }else{ ?>
                      <img src="<?php echo base_url('assets/')?>images/user.jpg" class="gj_lay_img img-responsive">
                    <?php } ?>
                  <?php }else{ ?>
                    <img src="<?php echo base_url('assets/')?>images/user.jpg" class="gj_lay_img img-responsive">
                  <?php }
                ?>
                <!-- <img src="images/layouts/profile.png"  class="gj_lay_img img-responsive"/> -->
              </div>
              <div class="gj_lay_vw_div">
                <a href="" class="gj_view_details" style="position: absolute !important; bottom: 3px !important;">View Details<i class="fa fa-arrow-right gj_view_arrw" style="margin-left: 5px ;margin-top: -5px"></i></a>
              </div>
            </div>
          </a>
          </div>
        </div>
      </div>
    </section>

    <?php if($_SESSION['user_id'] != 2){ ?>
      <!-- chart -->
      <section class="gj_section1">
        <div class="row">
          <div class="col-md-12">
            <div class="gj_dash_chart">
              <div id="container" class="gj_cht_container"></div>
            </div>
          </div>
        </div>
      </section>
      <!-- chart -->

      <!-- table -->
      <section class="gj_section1">
        <div class="row">
          <div class="col-lg-6 col-md-12">
            <div class="gj_lastcutoff">
              <p class="gj_lco">LAST CUTOFF</p>
            </div>
            <div class="gj_cutoff_table table-responsive">
              <table class="table table-hover table-bordered">
                  <thead>
                      <tr>
                        <th class="text-center" rowspan="2">Package Name</th>
                        <th class="text-center" colspan="2">Total Package</th>
                        <th class="text-center" colspan="2">Current Package</th>
                        <th class="text-center" colspan="2">Carry Forward</th>
                      </tr>
                      <tr>
                        <th class="text-center">Left</th>
                        <th class="text-center">Right</th>
                        <th class="text-center">Left</th>
                        <th class="text-center">Right</th>
                        <th class="text-center">Left</th>
                        <th class="text-center">Right</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php if(isset($final_obj_ar)){ ?> 
                        <?php if($final_obj_ar != null){ ?> 
                          <?php foreach ($final_obj_ar as $final) { ?>
                            <tr>
                              <td><?php echo $final->getPackageName(); ?></td>
                              <td><?php echo $final->getTotalLeft(); ?></td>
                              <td><?php echo $final->getTotalRight(); ?></td>
                              <td><?php echo $final->getCurrentLeft(); ?></td>
                              <td><?php echo $final->getCurrentRight(); ?></td>
                              <td><?php echo $final->getCarryLeft(); ?></td>
                              <td><?php echo $final->getCarryRight(); ?></td>
                            </tr>
                          <?php } ?>
                        <?php }else{echo"<tr><td colspan='7'>Not Eligible For Payout</td></tr>";} ?>
                      <?php }else{echo"<tr><td colspan='7'>Not Eligible For Payout</td></tr>";} ?>
                  </tbody>
              </table>
            </div>
          </div>
          <div class="col-lg-6 col-md-12">
            <!-- <div class="gj_bonus_table1">
              <div class="gj_lastcutoff">
                <p class="gj_lco">ROYALITY</p>
              </div>
              <div class="gj_cutoff_table table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                          <th class="text-center">Package Name</th>
                          <th class="text-center">Profit Pairs</th>
                          <th class="text-center">Achived Pairs</th>
                          <th class="text-center">Balance Pairs</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                          <td>Package 100BV</td>
                          <td>1500</td>
                          <td>456</td>
                          <td>4</td>
                        </tr>
                        <tr>
                          <td>Package 200BV</td>
                          <td>3000</td>
                          <td>323</td>
                          <td>177</td>
                        </tr>
                        <tr>
                          <td>Package 300BV</td>
                          <td>2500</td>
                          <td>231</td>
                          <td>269</td>
                        </tr>
                        <tr>
                          <td colspan="7">
                            <div class="gj_page_div text-right">
                            <select class="gj_selbox" id="gj_page2">
                              <option>Page 1</option>
                              <option>Page 2</option>
                              <option>Page 3</option>
                              <option>Page 4</option>
                              <option>Page 5</option>
                          </select>
                          <span class="gj_paginate">Page 1 - 5 of 26</span>
                          <span class="fa fa-chevron-left gj_chev_left"></span>
                          <span class="fa fa-chevron-right gj_chevr_rht"></span>
                        </div>
                          </td>
                        </tr>
                    </tbody>
                </table>
              </div>
            </div>
             -->
          </div>
        </div>
      </section>
      <!-- table -->
    <?php } ?>
  </div>
</div>

<?php 
 
  function js_str($s) { return '"' . addcslashes($s, "\0..\37\"\\") . '"'; }
  function js_array($array) { 
    $temp = array_map('js_str', $array);
    return '[' . implode(',', $temp) . ']';
  }
  function js_array_int($array) { 
    $string =  "[" . implode(',', $array) . "]";
    $integerIDs = array_map('intval', explode(',', $string));
    return "[" . implode(',', $integerIDs) . "]";
  }
?>
 <script type="text/javascript" src="<?php echo base_url('assets/')?>js/highcharts.js"></script>
<script type="text/javascript">

  Highcharts.chart('container', {
      chart: {
          type: 'column'
      },
      title: {
          text: 'PAYOUT STATEMENTS'
      },
      xAxis: {
          categories: <?php echo  js_array($pck);?>,

          crosshair: true
      },
      yAxis: {
          min: 0,
          title: {
              text: ''
          }
      },
      tooltip: {
          headerFormat: '<div class="gj_cts_tip"><span style="font-size:10px">{point.key}</span><table>',
          pointFormat: '<tr><td style="padding:0">{series.name}(s): </td>' +
              '<td style="padding:0"><b>{point.y:1f}</b></td></tr>',
          footerFormat: '</table><span style="font-size:10px">Net Amount 20000.00</span></div>',
          shared: true,
          useHTML: true
      },
      plotOptions: {
          column: {
              pointPadding: 0.2,
              borderWidth: 0
          }
      },
      series: [{
          name: 'left',
          data: <?php echo  js_array_int($lf);?>,
      }, {
          name: 'right',
          data: <?php echo  js_array_int($rt);?>,
      }]
  });
  
</script>
<style type="text/css">
  .rowxx.rep_cls {
    text-align: center;
    border: 1px solid #d3c7c7;
    width: 60%;
    margin: 0 auto;
    margin-top: 3em;
    margin-bottom: 0.5em;
    padding: 20px;
    color: #2bb1e9;
    font-size: 18px;
    font-weight: bold;
  }
  .rowxx.rep_cls p {
      padding: 5px 0px;
  }
  img.gj_lay_img.img-responsive {
      width: 70px;
      height: 50px;
      margin-top: -26px;
  }
</style>
<script type="text/javascript" src="<?php echo base_url('assets/')?>js/exporting.js"></script>
<script type="text/javascript">


  $(document).ready(function(){
    $.ajax({
      url:"<?php echo base_url('index.php/login/get_pck_name');?>",
      success: function(data){
        $('.pck_nm').text(data);
      }
    })
     $.ajax({
      url:"<?php echo base_url('index.php/login/get_rep_amt');?>",
      success: function(data){
        $('.bll').text(data);
        $('.gj_payout_count_rep').text(data);
      }
    })
    
  })
</script>