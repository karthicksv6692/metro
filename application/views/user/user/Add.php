<div class="dkbody">
	<div class="container-fluid">
		<div class="page-header">
			<h3>User Module</h3>
		</div>
		<p class="bank_add_details"><span><i class="fa fa-user" aria-hidden="true"></i></span> Profile Details</p>
		<div class="page-content">
			<section class="content bgcolor-1">
				<form method="post" action="<?php echo base_url('index.php/user/user_crud/add'); ?>" id="user_add_form" enctype="multipart/form-data" >
					<div class="row full_width container-fluid mar_top_1_em">Is Active &nbsp;&nbsp;&nbsp;

						<label class="switch">
						  <input type="checkbox" name="is_active" value="1">
						  <span class="slider round"></span>
						</label>
					</div>
					<div class="row">
						<div class="col-md-10 pull-left">
							<div class="row full_width">
								<div class="col-md-3 pull-left form-group">
									<span class="input input--nao">
										<input class="input__field input__field--nao user_name" type="text" id="user_profile_username"
										 name="user_name" value="<?php echo set_value('user_name'); ?>" 
										 data-validation="required custom length"
										data-validation-length="3-100"
										data-validation-regexp="^[A-Za-z][A-Za-z0-9_&amp;]+$"
										data-validation-error-msg-custom="Please provide a valid user name"
										 />
										<label class="input__label input__label--nao" for="user_profile_username">
											<span class="input__label-content input__label-content--nao">User Name</span>
										</label>
										<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
											<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
										</svg>
										<img src="<?php echo base_url('assets/images/loader/') ?>default.gif" class="ldr uname_ldr">
									</span>		
									<p class="uname_suc avail_er suc">Available</p>
									<p class="uname_err avail_er err">Already Exists</p>	
									<?php echo form_error('user_name','<span class="help-block form-error">', '</span>'); ?>	
								</div>
								<div class="col-md-3 pull-left row form-group">
									<span class="input input--nao">
										<input class="input__field input__field--nao" type="text" id="user_profile_lname"
										  name="first_name" value="<?php echo set_value('first_name'); ?>" 
										 	data-validation=" required length alphanumeric"
											data-validation-ignore=".  "
											data-validation-length="min3"
										  />
										<label class="input__label input__label--nao" for="user_profile_lname">
											<span class="input__label-content input__label-content--nao">First Name</span>
										</label>
										<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
											<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></path>
										</svg>
									</span>		
									<?php echo form_error('first_name','<span class="help-block form-error">', '</span>'); ?>
								</div>
								<div class="col-md-3 pull-left row form-group">
									<span class="input input--nao">
										<input class="input__field input__field--nao" type="text" id="user_profile_lname"
										  name="last_name" value="<?php echo set_value('last_name'); ?>" 
										  data-validation="length alphanumeric"
										  data-validation-ignore="."
										  data-validation-length="min3"
										  data-validation-optional="true"
										  />
										<label class="input__label input__label--nao" for="user_profile_lname">
											<span class="input__label-content input__label-content--nao">Last Name</span>
										</label>
										<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
											<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></path>
										</svg>
									</span>		
									<?php echo form_error('last_name','<span class="help-block form-error">', '</span>'); ?>
								</div>
								
								<div class="col-md-3 pull-left mt-5 pad_zero">
									<div class="form_group radio_box">
										<label>Gender</label> 
										<label class="chkbo" style="display:inline">M
											<input type="radio" id="radio" name="gender" value="1" <?php if(set_value('gender') != null){if(set_radio('gender', '1')) { echo"checked";}}?>  data-validation="required">
											<label for="radio"></label>
										</label>
										<label class="chkbo" style="display:inline"  > F
											<input type="radio" id="radio2" name="gender"  value="0" data-validation="required" <?php if(set_value('gender') != null){if(set_radio('gender','0')) { echo"checked";}}?>>
											<label for="radio2"></label>
										</label>
										<label class="chkbo" style="display:inline"  >O
											<input type="radio" id="radio3"  name="gender"  value="2" <?php if(set_value('gender') != null){if(set_radio('gender', '2')) { echo"checked";}}?>>
											<label for="radio3"></label>
										</label>
										<?php echo form_error('gender','<span class="help-block form-error">', '</span>'); ?>
									</div>
								</div>
							</div>

							<div class="row full_width">
								<div class="col-md-3 pull-left form-group">
									<span class="input input--nao" id="sandbox-container">
										<input type="text" class="input__field input__field--nao form-control"  name="dob" value="<?php if(set_value('dob') != null){echo set_value('dob');} ?>" 
										 
										 />
										 <!-- data-validation="birthdate" data-validation-format="yyyy/mm/dd" -->
										<label class="input__label input__label--nao" for="input-1">
											<span class="input__label-content input__label-content--nao">Date of Birth </span>
										</label>
										<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
											<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
										</svg>
									</span>	
									<?php echo form_error('dob','<span class="help-block form-error">', '</span>'); ?>						
								</div>

								<div class="col-md-3 pull-left row form-group">
									<span class="input input--nao">
										<input class="input__field input__field--nao user_email" type="email" id="user_profile_emailid"
										  name="user_email" value="<?php echo set_value('user_email'); ?>"
										  data-validation="required email"
										  />
										<label class="input__label input__label--nao" for="user_profile_emailid">
											<span class="input__label-content input__label-content--nao">Email ID</span>
										</label>
										<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
											<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
										</svg>
										<img src="<?php echo base_url('assets/images/loader/') ?>default.gif" class="ldr umail_ldr">
									</span>		
									<p class="umail_suc avail_er suc">Available</p>
									<p class="umail_err avail_er err">Already Exists</p>
									<?php echo form_error('user_email','<span class="help-block form-error">', '</span>'); ?>
								</div>

								<div class="col-md-3 pull-left row form-group">
									<span class="input input--nao">
										<input class="input__field input__field--nao mobile" type="number" id="user_profile_primary_phoneno"
										  name="mobile" value="<?php echo set_value('mobile'); ?>"
										  	data-validation="required length alphanumeric"
											data-validation-ignore="+ " 
											data-validation-length="10-12"
										  />
										<label class="input__label input__label--nao" for="user_profile_primary_phoneno">
											<span class="input__label-content input__label-content--nao">Primary Phone No</span>
										</label>
										<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
											<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
										</svg>
										<img src="<?php echo base_url('assets/images/loader/') ?>default.gif" class="ldr umob_ldr">

									</span>		
									<p class="umob_suc avail_er suc">Available</p>
									<p class="umob_err avail_er err">Already Exists</p>
									<?php echo form_error('mobile','<span class="help-block form-error">', '</span>'); ?>
								</div>

								<div class="col-md-3 pull-left row form-group">
									<span class="input input--nao">
										<input class="input__field input__field--nao" type="number" id="user_profile_secondary_phoneno"
										  name="alter_mob" value="<?php echo set_value('alter_mob'); ?>"
										  data-validation="required length alphanumeric"
											data-validation-ignore="+ " 
											data-validation-length="10-12"
											data-validation-optional="true"
										  />
										<label class="input__label input__label--nao" for="user_profile_secondary_phoneno">
											<span class="input__label-content input__label-content--nao">Secondary Phone No</span>
										</label>
										<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
											<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
										</svg>
									</span>		
									<?php echo form_error('alter_mob','<span class="help-block form-error">', '</span>'); ?>
								</div>
							</div>
						</div>
						<div class="col-md-2 pull-left">
							<div class="col-12 user_module pkfile" id="user_access_pofile_image">
	                            <div class="toppkfile" style="float:left">	                                
	                                <div class="user_profile_default_image">
	                                    <img src="<?php echo base_url('assets/'); ?>images/user_photo.png" class="img-responsive user_profile_image">
	                                </div>
	                            </div>
	                        </div>
	                            <input type="file"  name="file" id="user_profile_image" class="form-control img-responsive">
						</div>
					</div>
					<p class="bank_add_details"><span><i class="fa fa-user" aria-hidden="true"></i></span>Nominee Details</p>
					<div class="row full_width">
						<div class="col-md-3 pull-left form-group">
							<span class="input input--nao">
								<input class="input__field input__field--nao" type="text" id="user_nominee_name"
								 name="n_name" value="<?php echo set_value('n_name'); ?>" 
								data-validation="required custom length"
								data-validation-length="2-100"
								data-validation-regexp="^[A-Za-z][A-Za-z ]+$"
								data-validation-error-msg-custom="Please provide valid nominee name"
								/>
								<label class="input__label input__label--nao" for="user_nominee_name">
									<span class="input__label-content input__label-content--nao">Nominee Name</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></path>
								</svg>
							</span>		
							<?php echo form_error('n_name','<span class="help-block form-error">', '</span>'); ?>
						</div>
						<div class="col-md-2 pull-left form-group">
							<span class="input input--nao" id="sandbox-container">
								<input type="text" class="input__field input__field--nao form-control"  name="n_dob" value="<?php if(set_value('n_dob') != null){echo set_value('n_dob');} ?>"
							
								>
								<!-- data-validation="birthdate" 
								data-validation-format="yyyy/mm/dd"
								data-validation-optional="true" -->
								<label class="input__label input__label--nao" for="input-1">
									<span class="input__label-content input__label-content--nao">Date of Birth </span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
							</span>	
							<?php echo form_error('n_dob','<span class="help-block form-error">', '</span>'); ?>						
						</div>

						<div class="col-md-2 pull-left">
							<label for="id_label_single" class="input input--nao">
								<select class="js-example-basic-single select2_style relation" name="n_relationship"
								data-validation="required"
								>
									<option value="Father" <?php  if(set_value('n_relationship') != null){ if(set_value('n_relationship') == 'Father') { echo"checked";} }?>>Father</option>
									<option value="Mother" <?php  if(set_value('n_relationship') != null){ if(set_value('n_relationship') == 'Mother') { echo"checked";} }?>>Mother</option>
									<option value="Wife" <?php  if(set_value('n_relationship') != null){ if(set_value('n_relationship') == 'Wife') { echo"checked";} }?>>Wife</option>
									<option value="Son" <?php  if(set_value('n_relationship') != null){ if(set_value('n_relationship') == 'Son') { echo"checked";} }?>>Son</option>
									<option value="Daughter" <?php  if(set_value('n_relationship') != null){ if(set_value('n_relationship') == 'Daughter') { echo"checked";} }?>>Daughter</option>
									<option value="Brother" <?php  if(set_value('n_relationship') != null){ if(set_value('n_relationship') == 'Brother') { echo"checked";} }?>>Brother</option>
									<option value="Sister" <?php  if(set_value('n_relationship') != null){ if(set_value('n_relationship') == 'Sister') { echo"checked";} }?>>Sister</option>
								</select>
							</label>		
							<?php echo form_error('n_relationship','<span class="help-block form-error">', '</span>'); ?>	
						</div>

						<div class="col-md-2 pull-left form-group">
							<span class="input input--nao">
								<input class="input__field input__field--nao" type="number" id="user_nominee_phoneno"
								 name="n_mob" value="<?php echo set_value('n_mob'); ?>" 
								  data-validation="required number length"
								  data-validation-ignore="+-" 
		                          data-validation-length="10-12"
								 />
								<label class="input__label input__label--nao" for="user_nominee_phoneno">
									<span class="input__label-content input__label-content--nao">Phone No</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
							</span>		
								<?php echo form_error('n_mob','<span class="help-block form-error">', '</span>'); ?>
						</div>
						<div class="col-md-3 pull-left row mt-5 height_30px">
							<div class="form-group radio_box">
								<span class="usermodulegender">Gender</span>
								<label class="chkbo" style="display:inline">M
									<input type="radio" id="radio" name="n_gender" value="1" <?php if(set_value('n_gender') != null){if(set_radio('n_gender', '1')) { echo"checked";}}?> data-validation="required">
									<label for="radio"></label>
								</label>
								<label class="chkbo" style="display:inline"> F
									<input type="radio" id="radio2" name="n_gender"  value="0" <?php if(set_value('n_gender') != null){if(set_radio('n_gender', '0')) { echo"checked";}} ?> data-validation="required">
									<label for="radio2"></label>
								</label>
								<label class="chkbo" style="display:inline">O
									<input type="radio" id="radio3"  name="n_gender"  value="2" <?php if(set_value('n_gender') != null){if(set_radio('n_gender', '2')) { echo"checked";}} ?> data-validation="required">
									<label for="radio3"></label>
								</label>
							</div>
								<?php echo form_error('n_gender','<span class="help-block form-error">', '</span>'); ?>
						</div>
					</div>
					<p class="bank_add_details"><span><i class="fa fa-user" aria-hidden="true"></i></span> Communication Details</p>
					<div class="row full_width">
						<div class="col-md-4 pull-left">
							<span class="input input--nao">
								<input class="input__field input__field--nao" type="text" id="user_communication_full_address"
								 name="full_address" value="<?php echo set_value('full_address'); ?>" 
								data-validation="length"
								data-validation-optional="true"
								data-validation-length="min10"
								data-validaton-optional="true"
								 />
								<label class="input__label input__label--nao" for="user_communication_full_address">
									<span class="input__label-content input__label-content--nao">Full Address</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
							</span>		
							<?php echo form_error('full_address','<span class="help-block form-error">', '</span>'); ?>
						</div>
						<div class="col-md-2 pull-left">
							<span class="input input--nao">
								<input class="input__field input__field--nao pincode_no" type="number" id="user_communication_pincode" name="pincode_no" value="<?php echo set_value('pincode_no'); ?>" 
								data-validation="length required"
								data-validation-length="4-6"
								/>
								<label class="input__label input__label--nao" for="user_communication_pincode">
									<span class="input__label-content input__label-content--nao">Pincode</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
							</span>		
							<?php echo form_error('pincode_no','<span class="help-block form-error">', '</span>'); ?>
						</div>
						<div class="col-md-2 pull-left form-group">
							<span class="input input--nao district_lbl">
								<input class="input__field input__field--nao district" type="text" id="user_communication_district"
								 name="district" value="<?php echo set_value('district'); ?>"
								 	data-validation="required custom length" 
 									data-validation-length="2-100"
									data-validation-regexp="^[A-Za-z][A-Za-z0-9-_\ .&amp;]+$"
									data-validation-error-msg-custom="Provide valid district name"
								 />
								<label class="input__label input__label--nao" for="user_communication_district">
									<span class="input__label-content input__label-content--nao">District</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
							</span>		
							<?php echo form_error('district','<span class="help-block form-error">', '</span>'); ?>
						</div>
						<div class="col-md-2 pull-left form-group">
							<span class="input input--nao state_lbl">
								<input class="input__field input__field--nao state" type="text" id="user_communication_state"
								 name="state" value="<?php echo set_value('state'); ?>" 
								data-validation="required custom length" 
								data-validation-length="2-100"
								data-validation-regexp="^[A-Za-z][A-Za-z0-9-_\ .&amp;]+$"
								data-validation-error-msg-custom="please provide valid state"
								 />
								<label class="input__label input__label--nao" for="user_communication_state">
									<span class="input__label-content input__label-content--nao">State</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
							</span>		
							<?php echo form_error('state','<span class="help-block form-error">', '</span>'); ?>
						</div>
						<div class="col-md-2 pull-left cntry">
							<select class="js-example-basic-single select2_style ad_usr country" name="country_id" id="combobox">
								<?php foreach ($country_list as $list) {?>
									<option value="<?php echo $list->getCountryId();  ?>" <?php if(set_value('country_id') == $list->getCountryId()){echo "selected='selected'";} ?>><?php echo $list->getCountryName();  ?></option>
								<?php  } ?>
							</select>
							<?php echo form_error('country_id','<span class="help-block form-error">', '</span>'); ?>	
						</div>
					</div>

					<p class="bank_add_details"><span><i class="fa fa-user" aria-hidden="true"></i></span> Identity Details</p>
					<div class="row full_width">
						<!-- <div class="col-md-6 pull-left form-group">
							<span class="input input--nao">
								<input class="input__field input__field--nao pan" type="number" id="user_identity_pancardnumber" name="pan" value="<?php echo set_value('pan'); ?>" 
								 data-validation="required custom length"
								 data-validation-length="min3"
								 data-validation-regexp="^[a-zA-Z0-9]*$"
								 data-validation-error-msg-custom="Please provide valid pancard no"
								/>
								<label class="input__label input__label--nao" for="user_identity_pancardnumber">
									<span class="input__label-content input__label-content--nao">Pancard Number</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></path>
								</svg>
							</span>		
							<?php echo form_error('pan','<span class="help-block form-error">', '</span>'); ?>
						</div>
						<div class="col-md-6 pull-left form-group">
							<span class="input input--nao">
								<input class="input__field input__field--nao" type="number" id="user_identity_aadharcardnumber"
								 name="aadhar" value="<?php echo set_value('aadhar'); ?>" 
								 data-validation="custom length"
								 data-validation-length="min3"
								 data-validation-regexp="^[a-zA-Z0-9]*$"
								 data-validation-error-msg-custom="Please provide valid Aadharcard no"
								 data-validation-optional="true"
								 />
								<label class="input__label input__label--nao" for="user_identity_aadharcardnumber">
									<span class="input__label-content input__label-content--nao">Aadharcard Number</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
							</span>		
							<?php echo form_error('aadhar','<span class="help-block form-error">', '</span>'); ?>
						</div> -->
					</div>


					<div class="row">
						<!--  -->
						<div class="tab-contentt">
							<div class="s_attachment_detail pull-left">
								<div class="col-md-4 pull-left row ">
									<span class="input input--nao mar_lef_5">
										<input class="input__field input__field--nao" type="text" id="input-1" name="pan_no"  value="<?php echo set_value('pan_no'); ?>" >
										<label class="input__label input__label--nao" for="input-1">
											<span class="input__label-content input__label-content--nao">Pancard No </span>
										</label>
										<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
											<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
										</svg>
									</span>	
									<?php echo form_error('pan_no','<span class="help-block form-error">', '</span>'); ?>						
								</div>
								<div class="col-md-4 pull-left row">
									<span class="input input--nao mar_lef_5">
										<input class="input__field input__field--nao" type="text" id="input-1" name="aadhar_no"  value="<?php echo set_value('aadhar_no'); ?>" >
										<label class="input__label input__label--nao" for="input-1">
											<span class="input__label-content input__label-content--nao">Aadhar Card No </span>
										</label>
										<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
											<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
										</svg>
									</span>		
									<?php echo form_error('aadhar_no','<span class="help-block form-error">', '</span>'); ?>					
								</div>
								<div class="col-md-4 pull-left row">
									<span class="input input--nao mar_lef_5">
										<input class="input__field input__field--nao" type="text" id="input-1" name="pass_no"  value="<?php echo set_value('pass_no'); ?>" >
										<label class="input__label input__label--nao" for="input-1">
											<span class="input__label-content input__label-content--nao">Bank Passbook </span>
										</label>
										<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
											<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
										</svg>
									</span>	
									<?php echo form_error('pass_no','<span class="help-block form-error">', '</span>'); ?>							
								</div>
								<div class="col-md-4 pull-left row pkfile">
									  <div class="col-6 toppkfile form-group row">
										  <input type="file" name="pan_front" id="filesx" class="form-control img-responsive filesx" accept='image/*'> 
										 
										  <div class="width-60px width-6px ml-2 pan_front_img"></div>
									  </div>
									  <div class="col-6 toppkfile form-group row">
										  <input type="file" name="pan_back" id="filesx" class="form-control img-responsive filesx"  accept='image/*'> 
										 
										  <div class="width-60px width-6px ml-2 pan_back_img"></div>
									  </div>
								</div>
								<div class="col-md-4 pull-left row pkfile">
									  <div class="col-6 toppkfile form-group row">
										  <input type="file" name="aadhar_front" id="filesx" class="form-control img-responsive filesx"  accept='image/*'> 
										 
										  <div class="width-60px width-6px ml-2 aadhar_front_img"></div>
									  </div>
									  <div class="col-6 toppkfile form-group row">
										  <input type="file" name="aadhar_back" id="filesx" class="form-control img-responsive filesx" accept='image/*' > 
										 
										  <div class="width-60px width-6px ml-2 aadhar_back_img"></div>
									  </div>
								</div>
								<div class="col-md-4 pull-left row pkfile">
									  <div class="col-6 toppkfile form-group row">
										  <input type="file" name="pass_front" id="filesx" class="form-control img-responsive filesx" accept='image/*'> 
										 
										  <div class="width-60px width-6px ml-2 pass_front_img"></div>
									  </div>
									  <div class="col-6 toppkfile form-group row">
										  <input type="file" name="pass_back" id="filesx" class="form-control img-responsive filesx" accept='image/*'> 
										 
										  <div class="width-60px width-6px ml-2 pass_back_img"></div>
									  </div>
								</div>
								<div class="rr">
									<div class="col-md-4 pull-left row ">
										<div class="dkinput-txtflds mar_lef_5">
											<label class="chkbo ">
												<input type="checkbox" name="pan_verified" value="1">
												<span class="checkbox-material">
													<span class="check"></span>
												</span>  Verified
											</label>
										</div>
									</div>
									<div class="col-md-4 pull-left row ">
										<div class="dkinput-txtflds mar_lef_5">
											<label class="chkbo ">
												<input type="checkbox" name="aadhar_verified" value="1">
												<span class="checkbox-material">
													<span class="check"></span>
												</span>  Verified
											</label>
										</div>
									</div>
									<div class="col-md-4 pull-left row ">
										<div class="dkinput-txtflds mar_lef_5">
											<label class="chkbo ">
												<input type="checkbox" name="bank_verified" value="1">
												<span class="checkbox-material">
													<span class="check"></span>
												</span>  Verified
											</label>
										</div>
									</div>
								</div>



								<div class="rr usr_ad_idnty">
									<div class="col-md-2 pull-left ">
										<div class="dkinput-txtflds mar_lef_miuns10">
											<label class="chkbo ">
												<input type="checkbox" name="skip" value="1" class="skip_cbox">
												<span class="checkbox-material">
													<span class="check"></span>
												</span>  Skip Now
											</label>
										</div>
									</div>
									<div class="col-md-10 pull-left attch_documt_date">
										<p class="color we_need">We need to document with in <?php echo date('d-m-Y', strtotime("+7 days")); ?></p>
									</div>
								</div>
							</div>
							
						</div>
						<!--  -->
					</div>


					<p class="bank_add_details"><span><i class="fa fa-user" aria-hidden="true"></i></span> Password Details</p>
					<!-- <input type="text" class="form-control" id="pwd" name="pass_confirmation" data-validation="length" data-validation-length="min8">
					<input type="text" class="form-control" id="cpwd" name="pass" data-validation="confirmation"> -->
					<div class="row full_width">
						<div class="col-md-4 pull-left form-group">
							<span class="input input--nao">
								<input class="input__field input__field--nao" type="password" id="user_password"
								 name="password" value="<?php echo set_value('password'); ?>" 
								 data-validation="required length custom" 
								 data-validation-length="min8"
								 data-validation-regexp="^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$" 
								 />
								<label class="input__label input__label--nao" for="user_password">
									<span class="input__label-content input__label-content--nao">Password</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
							</span>		
							<?php echo form_error('password','<span class="help-block form-error">', '</span>'); ?>
						</div>
						<div class="col-md-4 pull-left form-group">
							<span class="input input--nao">
								<input class="input__field input__field--nao" type="password" id="user_confirm_password"
								 name="r_password" value="<?php echo set_value('r_password'); ?>" 
								 data-validation="re_pass" data-confirm-with="user_password"
								 />
								<label class="input__label input__label--nao" for="user_confirm_password">
									<span class="input__label-content input__label-content--nao">Confirm Password</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
							</span>		
							<?php echo form_error('r_password','<span class="help-block form-error">', '</span>'); ?>
						</div>
						<!-- <input name="pass_confirmation" data-validation="length" data-validation-length="min8">
  						<input name="pass" data-validation="confirmation"> -->
					</div>

					<p class="bank_add_details"><span><i class="fa fa-user" aria-hidden="true"></i></span> Role Details</p>
					<div class="row full_width">

						<div class="col-md-5 pull-left row">

							<label for="exampleSelect1" class="role_select"><h4 class="userrole_addaccess">Role</h4>
						    <select class="form-control slt_type" id="exampleSelect1" name="">
						      	<option data-id="0" value="0">--Select--</option>
								<?php foreach ($role_lists as $r_list) { 
									$r_id=$r_list->getUserRoleId();
									$r_lit=$r_list->getUserRoleAccessIds();
									$r_name=$r_list->getUserRoleName();
									?>
									<option data-id="<?php echo $r_id; ?>" value="<?php if($r_lit != null){echo $r_lit;} ?>" <?php if(set_value('hid_role_id') != null){ if(set_value('hid_role_id') == $r_id){echo "selected='selected'";} } ?>><?php echo $r_name; ?></option>
								<?php } ?>
						    </select>
						   	</label>
								
						</div>

						<div class="col-md-7 pull-left mt-5 ">
							
							<h4 class="userrole_addaccess">Access</h4>
							<?php if($module_list != null){  ?>
								<?php if(count($module_list)>0 && count($access_list)>0){ ?>	
									<?php foreach ($module_list as $m_list) { ?>
										<div class="row rr mt-3 ml-2">
											<div class="checkboxFive">
					                        	<input type="checkbox" id="<?php echo $m_list->getModuleId(); ?>" onchange="mul(<?php echo $m_list->getModuleId(); ?>)" class="role-<?php echo $m_list->getModuleId(); ?>" >  
					                        	<label for="<?php echo $m_list->getModuleId(); ?>"></label>
					                        	<span class="checkbox-material ml-4">
					                        		<span class="check"></span> <?php if(isset($m_list)){if($m_list->getModuleName() != null){echo $m_list->getModuleName();}} ?>
					                        	</span>
					                        </div>
					                    </div>
				                      	<div class="row rr mt-3 ml-2">
				                      		<?php foreach ($access_list as $a_list) { 
							                    if($a_list->getModuleId()->getModuleId() == $m_list->getModuleId()){
							               	?>
							               		<div class="checkboxFive pull-left pr-3">
							               			<input type="checkbox"  data-role="<?php echo $m_list->getModuleId(); ?>" class="access-<?php echo $a_list->getAccessId(); ?>" value="<?php echo $a_list->getAccessId(); ?>" id="<?php echo $a_list->getAccessId();?>-<?php echo $a_list->getAccessName();?>" onchange="access_change(<?php echo $a_list->getAccessId(); ?>)" <?php if(set_value('hid_access_id') != null){$ar = explode(',',set_value('hid_access_id')); if(in_array($a_list->getAccessId(), $ar)){echo"checked='checked'";}} ?>>
						                        	<label for="<?php echo $a_list->getAccessId();?>-<?php echo $a_list->getAccessName();?>"></label>
						                        	<span class="checkbox-material ml-4">
						                        		<span class="check"></span> <?php echo $a_list->getAccessName(); ?>
						                        	</span>
						                        </div>
					                        <?php }} ?>
					                   	</div>
					                <?php } ?>
				                <?php } ?>
			                <?php } ?>
						</div>
					</div>
					<input type="hidden" class="hid_access_id" name="hid_access_id" value="<?php echo set_value('hid_access_id'); ?>">
					<input type="hidden" class="img_list" name="img_list" value="">
					<input type="hidden" class="hid_role_id" name="hid_role_id" value="<?php echo set_value('hid_role_id'); ?>">
					<input type="hidden" name="usr" value="asdf">
					<div class="country_width_100 col-12 mt-5">
						<div class="country_width_100">
							<div class="butt_sec_width mt-3 mb-3">
						      	<button type="submit" name="country_submit" class="country_button mr-2" >SUBMIT <i class="fa fa-paper-plane" aria-hidden="true"></i></button>		
							    <button type="reset" name="country_reset" class="country_button" >RESET <i class="fa fa-refresh" aria-hidden="true"></i></button>
							</div>
				      	</div>
				      	
				        <div class="country_width_100 mt-3 mb-3">					      		  
						    <div class="country-right">
						      	<a href="<?php echo base_url('index.php/user/user_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button></a>
						    </div>	
						</div>
					</div>

				</form>
			</section>
		</div>
	</div>
</div>
<?php 
	$pincode_array = array();
	foreach ($pincode_list as $list) {  
		array_push($pincode_array, $list->getPincodeNo());
	}
	function js_str($s) { return '"' . addcslashes($s, "\0..\37\"\\") . '"'; }
	function js_array($array) { 
		$temp = array_map('js_str', $array);
		return '[' . implode(',', $temp) . ']';
	}
?>
<script type="text/javascript">	
	$.validate({
		lang: 'en',
	    form:'#user_add_form',
	    //modules :'security'
	    modules :'logic,security,date',
	    onModulesLoaded : function() {
	      console.log('Modules are loaded successfully');
	  	}
	});

	$(document).ready(function() { 
		$(".js-example-basic-single").select2({ 
			placeholder: "Select a Role",
  			allowClear: true
  		});

		<?php if(set_value('n_relationship') != null){ ?>

			$(".relation").val('<?php echo set_value('n_relationship'); ?>').trigger('change');
			  
		<?php }else{ ?>
			$(".relation").select2({ 
				placeholder: "Select a Relationship",
	  			allowClear: true
	  		});  
		<?php } ?>
  		

  		
	});
	$( function() { 
      	$( ".pincode_no" ).autocomplete({
        	source: <?php echo  js_array($pincode_array);?>
      	});
    });
</script>
<script type="text/javascript">
    function handleFileSelect(evt) 
    { 
        //console.log(evt.target.id);
        var files = evt.target.files; // FileList object // Loop through the FileList and render image files as thumbnails. 
        for (var i = 0, f; f = files[i]; i++) 
        { 
         	if (!f.type.match('image.*')) 
            { 
                continue; 
            } 
            var reader = new FileReader(); // Closure to capture the file information. 
            reader.onload = (function(theFile) 
            { 
                console.log(evt.target.id);
                return function(e) 
                { 
                    if(evt.target.id=="user_profile_image")
                    {
                        $('#user_access_pofile_image #user_profile_image').remove();
                        $('#user_access_pofile_image .user_profile_image').remove();
                        $('#user_access_pofile_image .col-12.p1-0.mr-2.mb-2.bdrstyle_hover').remove();
                        $('#user_access_pofile_image').prepend('<div class="col-12 p1-0 pl-0 pr-0 mr-2 mb-2 bdrstyle_hover" style="float:left"><img class="width-60px bdrstyle" style="width:100%;height:auto" src="'+e.target.result+'"><span>X</span></div>'); 
                        $('.bdrstyle_hover>span').on('click',function(){
                            $(this).parent().remove(); 
                            $('.toppkfile').prepend('<input type="file" name="user_profile_image[]" id="user_profile_image" class="form-control img-responsive">');
                            $('#user_access_pofile_image .user_profile_default_image').prepend('<img src="<?php echo base_url('assets/'); ?>images/user_photo.png" class="img-responsive user_profile_image">');
                                            
                        }); 
                    }
                    
                }; 
            })(f); 
            reader.readAsDataURL(f); 
        } 
    } 
    $('.bdrstyle_hover>span').on('click',function(){ $(this).parent().remove(); });
    document.getElementById('user_profile_image').addEventListener('change', handleFileSelect, false); 
</script>
<style type="text/css">
	.page-content section.content.bgcolor-1 form span.select2.select2-container.select2-container--default.select2-container--focus, span.select2.select2-container.select2-container--default.select2-container--below, span.select2.select2-container.select2-container--default {
	    width: 170px !important;
	    margin: 30px auto !important;
	    margin-top: 15px !important;
	    text-align: left;
	}
	.page-content section.content.bgcolor-1 form  .cntry span.select2.select2-container.select2-container--default.select2-container--focus, .page-content section.content.bgcolor-1 form  .cntry span.select2.select2-container.select2-container--default.select2-container--below, s.page-content section.content.bgcolor-1 form  .cntry pan.select2.select2-container.select2-container--default {
	    width: 170px !important;
	    margin-top: 45px !important;
	    text-align: left;
	}
	 .cntry span.select2.select2-container.select2-container--default{
	 	 margin-top: 45px !important;
	 }
</style>

<script type="text/javascript">
	$( function() { 
		$('.we_need').hide();
      	//$('.district,.state,.country').attr('disabled', 'disabled');
      	$('.uname_ldr,.umail_ldr,.umob_ldr').hide();
		$('.uname_suc,.uname_err,.umail_suc,.umail_err,.umob_suc,.umob_err').hide();
    });
    //Pincode Ajax Start
	    $('.pincode_no').blur(function()  {
	  		var pincode = $('.pincode_no').val();
	      	if(pincode.length > 0){
		      	$.ajax({
		        type: "POST",
		        url: "<?php echo base_url(); ?>index.php/Common_controller/ajax_pincode",
		        data: {'pincode':pincode},
	          	success:  function (data) {
	          		if(data != 0){
			            var option = JSON.parse(data);
			            console.log(option);
			            if(option[2]['pincode_district'] == false){
			              $('.district').val("");
			              $('.district').attr('disabled', 'disabled');
			            }else{
			              $('.district_lbl').addClass('input--filled');
			              $('.district').val(option[2]['pincode_district']).removeAttr('disabled');
			              $('.district').attr('disabled', false);
			            }
			            if(option[3]['pincode_state'] == false){
			              $('.state').val("");
			              $('.state').attr('disabled', 'disabled');
			            }else{
			              $('.state_lbl').addClass('input--filled');
			              $('.state').attr('disabled', 'false');
			              $('.state').val(option[3]['pincode_state']).removeAttr('disabled');
			            }
			            if(option[6]['pincode_country'] == false){
			              $('.country').val("");
			              $('.country').attr('disabled', 'disabled');
			            }else{
			             	$('.country_lbl').addClass('input--filled');
		              		$('.country').val(option[6]['country_id']).trigger('change');  
			            }
			        }else{
				      	 $('.district,.state,.country').val('');
			        	$('.state_label,.district_label,.country_label').removeClass('input--filled');
				      	 $('.district,.state,.country').attr('disabled', 'disabled');
			        }
	          	}
	      	});
	      	}else{
	      		$('.state_label,.district_label,.country_label').removeClass('input--filled');
	      	 	$('.district,.state,.country').attr('disabled', 'disabled');
	      	 	$('.district,.state,.country').val('');
	      	}
	    });
  	//Pincode Ajax End
	
  	$('.user_name').focus(function()  {
      var a = $('.user_name').val();
      if(a.length>0){
        cc_name = a;
      }else{
        cc_name="";
      }
    })
    $('.user_name').blur(function()  {
      var user_name = $('.user_name').val();
      if(user_name.length>2 && cc_name != user_name){
        $('.uname_ldr').show();
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>index.php/Common_controller/user_name_available",
          data: {'field_value':user_name},
            success:  function (data) { 
              $('.uname_ldr').hide();
              if(data == 1){
              	$('.uname_suc').show();
              	$('.uname_err').hide();
              }else{
              	$('.uname_suc').hide();
              	$('.uname_err').show();
              }
            }
        })
      }else if(user_name.length == 0 && cc_name.length > 2){
       $('.uname_suc,.uname_err').hide();
      }
    })

    $('.user_email').focus(function()  {
      var a = $('.user_email').val();
      if(a.length>0){
        cc_mail = a;
      }else{
        cc_mail="";
      }
    })
    $('.user_email').blur(function()  {
      var user_email = $('.user_email').val();
      	if(validateEmail(user_email)){
	      if(user_email.length>5 && cc_mail != user_email){
	        $('.uname_ldr').show();
	        $.ajax({
	          type: "POST",
	          url: "<?php echo base_url(); ?>index.php/Common_controller/user_email_available",
	          data: {'field_value':user_email},
	            success:  function (data) { 
	              $('.uname_ldr').hide();
	              if(data == 1){
	              	$('.umail_suc').show();
	              	$('.umail_err').hide();
	              }else{
	              	$('.umail_suc').hide();
	              	$('.umail_err').show();
	              }
	            }
	        })
	      }else if(user_email.length == 0 && cc_mail.length > 2){
	       $('.umail_suc,.umail_err').hide();
	      }
	  }else{
       $('.umail_suc,.umail_err').hide();
      }
    })

    $('.mobile').focus(function()  {
      var a = $('.mobile').val();
      if(a.length>0){
        cc_mob = a;
      }else{
        cc_mob="";
      }
    })
    $('.mobile').blur(function()  {
      var mobile = $('.mobile').val();
      if(mobile.length>=10 && mobile.length<=12 && cc_mob != mobile){
        $('.umob_ldr').show();
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>index.php/Common_controller/user_mobile_available",
          data: {'field_value':mobile},
            success:  function (data) { 
              $('.umob_ldr').hide();
              if(data == 1){
              	$('.umob_suc').show();
              	$('.umob_err').hide();
              }else{
              	$('.umob_suc').hide();
              	$('.umob_err').show();
              }
            }
        })
      }else{
       $('.umob_suc,.umob_err').hide();
      }
    })
    function validateEmail(email) {
	  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	  return re.test(email);
	}

</script>

<script>
	$('.txtarea').blur(function(){
		if($('.txtarea').val().length > 0){
			$('.descri').addClass('input--filled');	
		
		}else{$('.descri').removeClass('input--filled');	}
	})
  $(document).ready(function(){
    //$('.hid_role_id').val();
  });
  function access_change(id){ 
    var val = $('.access-'+id).attr('data-role');
    $('.access-'+id).addClass('sss-'+val);
    ac_id =$('.hid_access_id').val();
    var result=ac_id.split(',');
    var checked = $('.access-'+id+':checked').val();
    if($('.access-'+id). prop("checked") == true){
      var count = $('.sss-'+val).length;
      var cc = $("[data-role='"+val+"'][data-cls='View']" ).val();
      if($("[data-role='"+val+"']").length == $('.sss-'+val).length){ 
        $('.role-'+val).click(); 
      }
      if(result.indexOf(checked)==-1){
        result.push(checked);
        if(result.indexOf(cc)==-1){
          result.push(cc);
        }
      }
      $('.hid_access_id').val(array_splice(result));
    }else{
      var val = $('.access-'+id).attr('data-role');
      if($(".sss-"+val).length == 1){
        if($('.role-'+val). prop("checked") == true){
            $('.role-'+val).attr('checked', true); 
        }
      }
      $('.access-'+id).removeClass('sss-'+val);
      if($("[data-role='"+val+"']").length-1 == $('.sss-'+val).length){
        $('.role-'+val).attr('checked', false); 
      }
      var remove_value_index = result.indexOf(""+id);
      result.splice( remove_value_index, 1 );
      $('.hid_access_id').val(array_splice(result));
      if($('.sss-'+val).length == 0){
        $("[data-role='"+val+"'][data-cls='View']" ).attr('checked',false);
      }
      if($("[data-role='"+val+"'][data-cls='View']" ).hasClass('sss-'+val)){
        $("[data-role='"+val+"'][data-cls='View']" ).attr('checked',true);
      }else{
        $("[data-role='"+val+"'][data-cls='View']" ).attr('checked',false);
      }
    }
  }
  function mul(id){
    $('[data-role='+id+']').each(function(){
      ac_id =$('.hid_access_id').val();
      var result=ac_id.split(',');
      if($('.role-'+id).prop("checked") == true){
        $("[data-role='"+id+"']").addClass('sss-'+id);
        // $('data-role'+id).addClass('sss-'+val);
        if(result.indexOf($(this).val())==-1){
          result.push($(this).val());
        }
        $('.hid_access_id').val(array_splice(result));
        $(this).prop('checked',true);
      }
      if($('.role-'+id).prop("checked") == false){
        $("[data-role='"+id+"']").removeClass('sss-'+id);
        var remove_value_index = result.indexOf(""+$(this).val());
        result.splice( remove_value_index, 1 );
        $('.hid_access_id').val(array_splice(result));
        $(this).prop('checked',false);
      }
    })
  }
  	function role_fuc(id){
	    $('.hid_role_id ').val(id);
	    ac_id =$('#role-'+id).val();
	    var result=ac_id.split(',');
	    var len = result.length;
	    $("input[type='checkbox']").prop('checked',false);
	      
	    for(i=0;i<len;i++){
	      $('.access-'+result[i]).prop('checked',true);
	    }
	    $('.hid_access_id').val(array_splice(result));
  	}

  	$(function(){
  		id = $('.slt_type').val();
  		idd = $('.slt_type option').attr('data-id');
	  	$('.hid_role_id ').val(idd);
	  	ac_id =id;
	    var result=ac_id.split(',');
	    var len = result.length;
	    $("input[type='checkbox']").prop('checked',false);
	      
	    for(i=0;i<len;i++){
	      $('.access-'+result[i]).prop('checked',true);
	    }
	    $('.hid_access_id').val(array_splice(result));
  	})

  	$('.slt_type').change(function(){
	  	id = $(this).val();
	  	//if(id != 0){
		  	// var idd = $('option:selected', this).attr('data_role-id');alert(id)
		  	var idd = $('option:selected', this).attr('data-id');
	  		$('.hid_role_id').val(idd);
		  	ac_id =id;
		    var result=ac_id.split(',');
		    var len = result.length;
		    $("input[type='checkbox']").prop('checked',false);
		      
		    for(i=0;i<len;i++){
		      $('.access-'+result[i]).prop('checked',true);
		    }
		    $('.hid_access_id').val(array_splice(result));
		//}
  	})

  	function array_splice(ar){
     	for (var i = 0; i < ar.length; i++) {
	      	if (ar[i] == "") {         
	        	ar.splice(i, 1);
	      	}
      		return ar;
		}
  	}
  	/*----------------------*/  	
</script>
<script>
	  	function readURL(input,class_name) {
		    if (input.files && input.files[0]) {
			        var reader = new FileReader();
			        reader.onload = function (e) {
			            $('.'+class_name+'_img').prepend('<div class="p1-0 mr-2 mb-2 bdrstyle_hover" style="float:left"><img class="width-60px bdrstyle" src="'+e.target.result+'"><span>X</span></div>');
			        }
			        reader.readAsDataURL(input.files[0]);
		    }
		}

		$(".filesx").change(function(){
		    readURL(this,$(this).attr('name'));
		});
		$('.bdrstyle_hover>span').on('click',function(){
			$(this).parent().remove();
		});	
		$('.skip_cbox').click(function(){
			if($(this).is(':checked')){
				$('.we_need').show();
			}else{$('.we_need').hide()}
		})
</script>
<style type="text/css">
	.pkfile input.form-control {
	    opacity: 0;
	    height: 90px;
	    position: absolute;
	    width: 95px;
	}
	img.width-60px.bdrstyle {
	    width: 90px;
	    height: 80px;
	    padding: 2px;
	    border: 1px solid #d7d7d7;
	    margin-left: -15px;
	}
	.p1-0.mr-2.mb-2.bdrstyle_hover {
	    position: absolute;
	    top: 0;
	}
	.bdrstyle_hover span {
	    right: 0px;
	    top: -6px;
	}
</style>