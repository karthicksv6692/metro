<div class="dkbody">
  <div class="container-fluid">
    <div class="page-header gj_new_pack">
      <h3>User Module</h3>
    </div>
    <p class="gj_package_dets"><span><i class="fa fa-cubes" aria-hidden="true"></i></span> Personal Details</p>
    <div class="page-content gj_user_all">
      <section class="gj_user_sec">
        <form class="gj_form">
          <div class="row rr">
            <div class="col-lg-10 pull-left pl-0 pr-0">
              <div class="col-lg-3 col-md-6 pull-left">
                <span class="input input--nao">
                  <input class="input__field input__field--nao" type="text" id="product_add_product_quantity" data-validation="required"/>
                  <label class="input__label input__label--nao" for="product_add_product_quantity">
                    <span class="input__label-content input__label-content--nao">User Name</span>
                  </label>
                  <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                  </svg>
                </span>
              </div>
              <div class="col-lg-3 col-md-6 pull-left">
                <span class="input input--nao">
                  <input class="input__field input__field--nao" type="text" id="product_add_product_quantity" data-validation="required"/>
                  <label class="input__label input__label--nao" for="product_add_product_quantity">
                    <span class="input__label-content input__label-content--nao">First Name</span>
                  </label>
                  <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                  </svg>
                </span>
              </div>
              <div class="col-lg-3 col-md-6 pull-left">
                <span class="input input--nao">
                  <input class="input__field input__field--nao" type="text" id="product_add_product_quantity"
                   />
                  <label class="input__label input__label--nao" for="product_add_product_quantity">
                    <span class="input__label-content input__label-content--nao">Last Name</span>
                  </label>
                  <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                  </svg>
                </span>
              </div>
              <div class="col-lg-3 col-md-6 pull-left">
                <span class="input input--nao">
                  <input class="input__field input__field--nao" type="email" id="product_add_product_quantity"
                   />
                  <label class="input__label input__label--nao" for="product_add_product_quantity">
                    <span class="input__label-content input__label-content--nao">E-Mail ID</span>
                  </label>
                  <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                  </svg>
                </span>
              </div>
            </div>
            <div class="col-lg-2 gj_user_up_div pull-left">
              <p class="gj_pack_title">upload Image</p>
              <div class=" pkfile gj_file gj_userfile">
                <div class=" toppkfile" style="float:left">
                  <input type="file"  name="files[]" id="files" class="form-control img-responsive">
                  <div class="width-60px width-6px">

                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row rr">
            <div class="col-lg-2 col-md-6 pull-left">
              <span class="input input--nao">
                <input class="input__field input__field--nao" type="date"  id="gj_user_dob"/>
                <label class="input__label input__label--nao" for="gj_user_dob">
                  <span class="input__label-content input__label-content--nao">D.O.B</span>
                </label>
                <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                  <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                </svg>
              </span>
            </div>
            <div class="col-lg-3 col-md-6 pull-left">
              <span class="input input--nao">
                <input class="input__field input__field--nao" type="number" id="product_add_product_quantity"
                 />
                <label class="input__label input__label--nao" for="product_add_product_quantity">
                  <span class="input__label-content input__label-content--nao">Mobile Number</span>
                </label>
                <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                  <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                </svg>
              </span>
            </div>
            <div class="col-lg-3 col-md-6 pull-left">
              <span class="input input--nao">
                <input class="input__field input__field--nao" type="number" id="product_add_product_quantity"
                 />
                <label class="input__label input__label--nao" for="product_add_product_quantity">
                  <span class="input__label-content input__label-content--nao">Alter Mobile Number</span>
                </label>
                <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                  <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                </svg>
              </span>
            </div>
            <div class="col-lg-4 col-md-6 pull-left">
              <div class="gj_gender_det">
                <span>Gender : </span>
                <input type="radio" id="test1" name="radio-group" checked>
                <label for="test1">Male</label>
                <input type="radio" id="test2" name="radio-group">
                <label for="test2">Female</label>
                <input type="radio" id="test3" name="radio-group">
                <label for="test3">Others</label>
              </div>
            </div>
          </div>

          <div class="row rr">
            <div class="col-lg-12">
              <p class="gj_package_dets gj_details_hd"><span><i class="fa fa-dropbox" aria-hidden="true"></i></span> Identity</p>
            </div>
            <div class="col-lg-4 col-md-6 pull-left">
              <span class="input input--nao">
                <input class="input__field input__field--nao" type="number"  id="gj_user_dob"/>
                <label class="input__label input__label--nao" for="gj_user_dob">
                  <span class="input__label-content input__label-content--nao">A/C Number</span>
                </label>
                <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                  <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                </svg>
              </span>
            </div>
            <div class="col-lg-4 col-md-6 pull-left">
              <span class="input input--nao">
                <input class="input__field input__field--nao" type="text" id="product_add_product_quantity"
                 />
                <label class="input__label input__label--nao" for="product_add_product_quantity">
                  <span class="input__label-content input__label-content--nao">PanCard Number</span>
                </label>
                <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                  <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                </svg>
              </span>
            </div>
            <div class="col-lg-4 col-md-6 pull-left">
            </div>
          </div>
          <div class="row rr">
            <div class="col-lg-4 col-md-6 pull-left">
              <div class="ifile gj_file gj_filess gj_user_file">
                <div class="toppkfile gj_relative" style="float:left">
                  <input type="file"  name="gj_file1" accept="image/jpeg" id="gj_file11" class="form-control gj_finput img-responsive gj_finput" title="Select Pan Card">
                  <div class="width-60px width-6px">
                    <span class="gj_file_cmd">Attach Pan Card Image</span>
                  </div>
                </div>
              </div>  
              <div class="row gj_rr">
                <div class="gj_user_chk_div gj_verfid">
                  <input type="checkbox" id="pan_verf" />
                  <label for="pan_verf">Verified</label>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 pull-left">
              <div class="yfile gj_file gj_filess gj_user_file">
                <div class="toppkfile gj_relative" style="float:left">
                  <input type="file"  name="gj_file4" id="gj_file44" class="form-control gj_finput img-responsive" title="Select Bank Passbook">
                  <div class="width-60px width-6px">
                    <span class="gj_file_cmd">Attach ID Card Image</span>
                  </div>
                </div>
              </div>  
              <div class="row gj_rr">
                <div class="gj_user_chk_div gj_verfid">
                  <input type="checkbox" id="id_verf" />
                  <label for="id_verf">Verified</label>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 pull-left">
              <div class="xfile gj_file gj_filess gj_user_file">
                <div class="toppkfile gj_relative" style="float:left">
                  <input type="file"  name="gj_file5" id="gj_file55" class="form-control gj_finput img-responsive" title="Select Bank Passbook">
                <div class="width-60px width-6px">
                  <span class="gj_file_cmd">Attach Bank Passbook Image</span>
                </div>
                </div>
              </div>  
              <div class="row gj_rr">
                <div class="gj_user_chk_div gj_verfid">
                  <input type="checkbox" id="bank_verf" />
                  <label for="bank_verf">Verified</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row rr">
            <div class="col-lg-12">
              <div class="gj_user_chk_div">
                <input type="checkbox" id="skip" />
                <label for="skip">Skip Now</label>
              </div>
            </div>
          </div>

          <div class="row rr">
            <div class="col-lg-12">
              <p class="gj_package_dets gj_details_hd"><span><i class="fa fa-cubes" aria-hidden="true"></i></span> Bank Details</p>
            </div>
            <div class="col-lg-4 col-md-6 pull-left">
              <span class="input input--nao">
                <input class="input__field input__field--nao" type="text"  id="gj_user_dob"/>
                <label class="input__label input__label--nao" for="gj_user_dob">
                  <span class="input__label-content input__label-content--nao">IFSC</span>
                </label>
                <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                  <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                </svg>
              </span>
            </div>
            <div class="col-lg-4 col-md-6 pull-left">
              <span class="input input--nao">
                <input class="input__field input__field--nao" type="text" id="product_add_product_quantity"
                 />
                <label class="input__label input__label--nao" for="product_add_product_quantity">
                  <span class="input__label-content input__label-content--nao">Bank Number</span>
                </label>
                <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                  <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                </svg>
              </span>
            </div>
            <div class="col-lg-4 col-md-6 pull-left">
              <span class="input input--nao">
                <input class="input__field input__field--nao" type="text" id="product_add_product_quantity"
                 />
                <label class="input__label input__label--nao" for="product_add_product_quantity">
                  <span class="input__label-content input__label-content--nao">Branch</span>
                </label>
                <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                  <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                </svg>
              </span>
            </div>
          </div>

          <div class="row rr">
            <div class="col-lg-12">
              <p class="gj_package_dets gj_details_hd"><span><i class="fa fa-dropbox" aria-hidden="true"></i></span> Communication Details</p>
            </div>
            <div class="col-lg-4 col-md-12 pull-left">
              <span class="input input--nao gj_desc_wid">
                <textarea class="input__field input__field--nao" rows="3" id="product_add_product_package_short_description"></textarea>
                <label class="input__label input__label--nao" for="product_add_product_package_short_description">
                  <span class="input__label-content input__label-content--nao">Address</span>
                </label>
                <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                  <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                </svg>
              </span> 
            </div>
            <div class="col-lg-8 col-md-12 pull-left gj_user_marg">
              <div class="col-lg-3 col-md-6 pull-left">
                <span class="input input--nao">
                  <select id="gj_sel2_pincode" class="input__field--nao gj_user_pin">
                    <option value="repurchase">629168</option>
                    <option value="register">629165</option>
                    <option value="register">629001</option>
                  </select>
                </span>
              </div>
              <div class="col-lg-3 col-md-6 pull-left">
                <span class="input input--nao">
                  <input class="input__field input__field--nao" type="text" id="product_add_product_quantity"
                   />
                  <label class="input__label input__label--nao" for="product_add_product_quantity">
                    <span class="input__label-content input__label-content--nao">District</span>
                  </label>
                  <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                  </svg>
                </span>
              </div>
              <div class="col-lg-3 col-md-6 pull-left">
                <span class="input input--nao">
                  <input class="input__field input__field--nao" type="text" id="product_add_product_quantity"
                   />
                  <label class="input__label input__label--nao" for="product_add_product_quantity">
                    <span class="input__label-content input__label-content--nao">State</span>
                  </label>
                  <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                  </svg>
                </span>
              </div>
              <div class="col-lg-3 col-md-6 pull-left">
                <span class="input input--nao">
                  <select id="gj_sel2_country" class="input__field--nao gj_user_pin">
                    <option value="repurchase">India</option>
                    <option value="register">Japan</option>
                    <option value="register">Dubai</option>
                  </select>
                </span>
              </div>
            </div>
          </div>

          <div class="row rr">
            <div class="col-lg-12">
              <p class="gj_package_dets gj_details_hd"><span><i class="fa fa-cubes" aria-hidden="true"></i></span> Nominee Details</p>
            </div>
            <div class="col-lg-2 col-md-6 pull-left gj_user_nominee">
              <span class="input input--nao">
                <input class="input__field input__field--nao" type="text" id="product_add_product_quantity"
                 />
                <label class="input__label input__label--nao" for="product_add_product_quantity">
                  <span class="input__label-content input__label-content--nao">Nominee Name</span>
                </label>
                <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                  <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                </svg>
              </span>
            </div>
            <div class="col-lg-2 col-md-6 pull-left">
                <span class="input input--nao gj_user_mobno">
                  <input class="input__field input__field--nao" type="number" id="product_add_product_quantity"
                   />
                  <label class="input__label input__label--nao" for="product_add_product_quantity">
                    <span class="input__label-content input__label-content--nao">Mobile Number</span>
                  </label>
                  <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                  </svg>
                </span>
          </div>
            <div class="col-lg-2 col-md-6 pull-left">
                <span class="input input--nao">
                  <select id="gj_sel2_relation" class="input__field--nao gj_user_pin">
                    <option value="repurchase">Father</option>
                    <option value="register">Mother</option>
                    <option value="register">Brother</option>
                  </select>
                </span>
            </div>
            <div class="col-lg-2 col-md-6 pull-left">
                <span class="input input--nao">
                  <input class="input__field input__field--nao" type="date" id="product_add_product_quantity"
                   />
                  <label class="input__label input__label--nao" for="product_add_product_quantity">
                    <span class="input__label-content input__label-content--nao">D.O.B</span>
                  </label>
                  <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                  </svg>
                </span>
            </div>
            <div class="col-lg-4 col-md-6 pull-left">
                <div class="gj_genr_dets">
                  <span>Gender : </span>
                  <input type="radio" id="male" name="sex" checked>
                  <label for="male">Male</label>
                  <input type="radio" id="female" name="sex">
                  <label for="female">Female</label>
                  <input type="radio" id="others" name="sex">
                  <label for="others">Others</label>
                </div>
            </div>
          </div>
          
          <div class="row rr">
            <div class="col-lg-12">
              <p class="gj_package_dets gj_details_hd"><span><i class="fa fa-dropbox" aria-hidden="true"></i></span> Password Details</p>
            </div>
            <div class="col-lg-4 col-md-6 pull-left">
              <span class="input input--nao">
                <input class="input__field input__field--nao" type="text" id="product_add_product_quantity" data-validation="required"/>
                <label class="input__label input__label--nao" for="product_add_product_quantity">
                  <span class="input__label-content input__label-content--nao">Password</span>
                </label>
                <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                  <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                </svg>
              </span>
              <div class="gj_cus_pwd">
                <span class="input input--nao">
                  <input class="input__field input__field--nao" type="text" id="product_add_product_quantity" data-validation="required"/>
                  <label class="input__label input__label--nao" for="product_add_product_quantity">
                    <span class="input__label-content input__label-content--nao">Conform Password</span>
                  </label>
                  <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                  </svg>
                </span>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 pull-left">
              <!-- <p class="gj_pin_store_hd">Pin Store Password</p> -->
              <span class="input input--nao">
                <input class="input__field input__field--nao" type="text" id="product_add_product_quantity" data-validation="required"/>
                <label class="input__label input__label--nao" for="product_add_product_quantity">
                  <span class="input__label-content input__label-content--nao">Pin Store Password</span>
                </label>
                <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                  <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                </svg>
              </span>
            </div>
            <div class="col-lg-4 col-md-6 pull-left">
            </div>
          </div>
          
          <div class="country_width_100 col-12 mt-5">
            <div class="country_width_100">
              <div class="butt_sec_width mt-3 mb-3">
                <button type="submit" name="moduleadd_submit" class="country_button mr-2" >SUBMIT <i class="fa fa-paper-plane" aria-hidden="true"></i></button>   
                  <button type="reset" name="moduleadd_reset" class="country_button">RESET <i class="fa fa-refresh" aria-hidden="true"></i></button>
                  
              </div>
            </div>
                
            <div class="country_width_100 mt-3 mb-3">                     
              <div class="country-right">
                <button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button>
              </div>  
            </div>
          </div>
          
        </form>
      </section>
    </div>
  </div>
</div>


<style type="text/css">
  .pkfile.gj_file.gj_userfile img.width-60px.bdrstyle {
    position: absolute;
    z-index: 99;
    left: 8px;
}
.pkfile.gj_file.gj_userfile span {
    right: -95px;
    z-index: 99;
}
input::-webkit-calendar-picker-indicator,
input::-webkit-inner-spin-button {
    display: none;
}
/* gender radio button style */
.gj_gender_det {
    margin-top: 38px;
}
.gj_gender_det span {
    font-weight: bold;
    color: dimgrey;
    vertical-align: text-bottom;
}
.gj_gender_det [type="radio"]:checked,
.gj_gender_det [type="radio"]:not(:checked) {
    position: absolute;
    left: -9999px;
}
.gj_gender_det [type="radio"]:checked + label,
.gj_gender_det [type="radio"]:not(:checked) + label
{
    position: relative;
    padding-left: 23px;
    cursor: pointer;
    margin-right: 5px;
    line-height: 20px;
    display: inline-block;
    color: #666;
}
.gj_gender_det [type="radio"]:checked + label:before,
.gj_gender_det [type="radio"]:not(:checked) + label:before {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    width: 18px;
    height: 18px;
    border: 1px solid #ddd;
    border-radius: 100%;
    background: #fff;
}
.gj_gender_det [type="radio"]:checked + label:after,
.gj_gender_det [type="radio"]:not(:checked) + label:after {
    content: '';
    width: 12px;
    height: 12px;
    background: #18a9e7;
    position: absolute;
    top: 3px;
    left: 3px;
    border-radius: 100%;
    -webkit-transition: all 0.2s ease;
    transition: all 0.2s ease;
}
.gj_gender_det [type="radio"]:not(:checked) + label:after {
    opacity: 0;
    -webkit-transform: scale(0);
    transform: scale(0);
}
.gj_gender_det [type="radio"]:checked + label:after {
    opacity: 1;
    -webkit-transform: scale(1);
    transform: scale(1);
}
/* gender radio button style */
p.gj_package_dets.gj_details_hd {
    width: 100%;
    float: left;
}
.gj_user_all .gj_package_dets>span {
    margin-right: 5px;
}
.gj_pan1file {
    width: 100px;
    opacity: 0;
    height: 100px;
    position: absolute;
    z-index: 99;
    left: 42px;
}
.gj_f_div {
    margin: auto;
    width: 100px;
    height: 100px;
    padding: 2px;
    border: 1px solid #d7d7d7;
    box-shadow: 4px 1px 2px 0px #c1c1c1;
    position: relative;
}
.gj_f_div:before {
    content: "+";
    font-size: 60px;
    color: #efefef;
    line-height: 115px;
    padding-left: 33%;
}
.gj_file_img {
    color: #787878;
    position: absolute;
    top: 0;
    left: 0;
    width: 100px;
    height: 100px;
}
.gj_user_file span.gj_file_cmd {
    position: absolute;
    top: 50%;
    margin-top: -33px;
    left: 20px;
    text-align: center;
    font-weight: bold;
    color: #787878;
    padding: 0px 3px;
}
span.gj_file_close {
    position: absolute;
    z-index: 99;
    top: -7px;
    left: -5px;
    background-color: #16a9e7;
    color: #fff;
    border-radius: 50%;
    width: 20px;
    height: 20px;
    padding: 2px 4px;
    cursor: pointer;
}
p.gj_need{
  color: #2bb0e9;
  font-weight: bold;
}
.gj_user_file .gj_finput {
    width: 250px !important;
    height: 175px !important;
    opacity: 0;
    position: absolute;
    z-index: 9;
}
.gj_relative{
    position: relative;
}
.gj_absolute{
    position: absolute;
    z-index: 99;
}
.gj_relative .width-60px {
    width: 250px;
    height: 175px;
    box-shadow: 1px 1px 2px 0px #c1c1c1;
}
.pkfile.gj_file.gj_userfile img.width-60px.bdrstyle {
    width: 80px;
    height: 80px;
}
.gj_user_file img.width-60px.bdrstyle {
    width: 250px;
    height: 175px;
}
.gj_filess .width-6px:before {
    content: "+";
    font-size: 60px;
    color: #e6e4e4;
    padding-left: 33%;
    line-height: 110px;
}
.gj_rr {
    margin-left: 0px !important;
    margin-right: 0px !important;
}
/* custom check box */
.gj_user_chk_div [type="checkbox"]:not(:checked),
.gj_user_chk_div [type="checkbox"]:checked {
  display: none;
}
.gj_user_chk_div [type="checkbox"]:not(:checked) + label,
.gj_user_chk_div [type="checkbox"]:checked + label {
  position: relative;
      padding-left: 1.95em;
    cursor: pointer;
    font-weight: bold;
  color: #787878;
}
.gj_user_chk_div [type="checkbox"]:checked + label {
  color: #2bb0e9;
}

/* checkbox aspect */
.gj_user_chk_div [type="checkbox"]:not(:checked) + label:before,
.gj_user_chk_div [type="checkbox"]:checked + label:before {
    content: '';
  position: absolute;
    left: 0; top: 0;
    width: 1.25em; height: 1.25em;
    border: 2px solid #2bb0e9;
    background: #fff;
}
/* checked mark aspect */
.gj_user_chk_div [type="checkbox"]:not(:checked) + label:after,
.gj_user_chk_div [type="checkbox"]:checked + label:after {
  content: '✔';
    position: absolute;
    top: 2.5px;
    left: 3px;
    font-size: 18px;
    line-height: 0.8;
    color: #2bb0e9;
    transition: all .2s;
}
/* checked mark aspect changes */
.gj_user_chk_div [type="checkbox"]:not(:checked) + label:after {
    opacity: 0;
    transform: scale(0);
}
.gj_user_chk_div [type="checkbox"]:checked + label:after {
    opacity: 1;
    transform: scale(1);
}
/* custom check box */
.gj_user_file {
    width: 100%;
    overflow: hidden;
    margin-bottom: 15px;
    padding: 0px 15px;
}
.gj_user_chk_div.gj_verfid {
    padding: 0px 15px;
    margin-bottom: 15px;
}
.gj_user_pin+.select2-container {
    width: 100% !important;
    margin-top: 13px;
}
.gj_user_pin+.select2-container span.select2-selection {
    border-radius: 0px;
    border: 0px;
    border-bottom: 1px solid #b9bdc1;
}
.gj_user_pin+.select2-container span.select2-selection .select2-selection__rendered {
    padding-left: 0px !important;
}
.gj_user_pin {
    outline: 0 !important;
}
.gj_user_marg {
    margin-top: 45px;
}
.gj_user_mobno {
    margin: 16px 0px;
}
/* gender radio button style */
.gj_genr_dets {
    margin-top: 40px;
}
.gj_genr_dets span {
    font-weight: bold;
    color: dimgrey;
    vertical-align: text-bottom;
}
.gj_genr_dets [type="radio"]:checked,
.gj_genr_dets [type="radio"]:not(:checked) {
    position: absolute;
    left: -9999px;
}
.gj_genr_dets [type="radio"]:checked + label,
.gj_genr_dets [type="radio"]:not(:checked) + label
{
    position: relative;
    padding-left: 23px;
    cursor: pointer;
    margin-right: 5px;
    line-height: 20px;
    display: inline-block;
    color: #666;
}
.gj_genr_dets [type="radio"]:checked + label:before,
.gj_genr_dets [type="radio"]:not(:checked) + label:before {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    width: 18px;
    height: 18px;
    border: 1px solid #ddd;
    border-radius: 100%;
    background: #fff;
}
.gj_genr_dets [type="radio"]:checked + label:after,
.gj_genr_dets [type="radio"]:not(:checked) + label:after {
    content: '';
    width: 12px;
    height: 12px;
    background: #18a9e7;
    position: absolute;
    top: 3px;
    left: 3px;
    border-radius: 100%;
    -webkit-transition: all 0.2s ease;
    transition: all 0.2s ease;
}
.gj_genr_dets [type="radio"]:not(:checked) + label:after {
    opacity: 0;
    -webkit-transform: scale(0);
    transform: scale(0);
}
.gj_genr_dets [type="radio"]:checked + label:after {
    opacity: 1;
    -webkit-transform: scale(1);
    transform: scale(1);
}
/* gender radio button style */
p.gj_pin_store_hd {
    margin-bottom: -24px;
    font-weight: bold;
}
p.gj_pack_title {
    font-weight: bold;
    color: #787878;
}

/* Responsive */
@media (min-width: 992px) and (max-width: 1199px) {
  p.gj_pack_title {
      font-size: 13px;
  }
  .gj_user_file .gj_finput {
      width: 190px !important;
      height: 150px !important;
  }
  .gj_relative .width-60px {
      width: 190px !important;
      height: 150px !important;
  }
  .gj_user_file img.width-60px.bdrstyle {
      width: 190px;
      height: 150px;
  }
  .pkfile.gj_file.gj_userfile .toppkfile input#files {
      padding: 0;
  }
  .gj_gender_det {
      margin-top: 25px;
  }
  .gj_genr_dets {
      width: 100%;
  }
}

@media (min-width: 768px) and (max-width: 991px) {
  .gj_user_up_div {
      padding: 0px 30px !important;
  }
  .gj_user_file .gj_finput {
      width: 190px !important;
      height: 150px !important;
  }
  .gj_relative .width-60px {
      width: 190px !important;
      height: 150px !important;
  }
  .gj_user_file img.width-60px.bdrstyle {
      width: 190px;
      height: 150px;
  }
  .gj_user_marg {
      margin-top: 0px;
  }
  section.gj_user_sec span.input.input--nao.gj_desc_wid {
      max-width: 100%;
  }
  .gj_user_pin+.select2-container {
      margin-bottom: 3px;
  }
  .gj_user_nominee span.input.input--nao {
      margin: 0px 30px 0px 15px;
      max-width: 100%;
  }
  .gj_user_pin+.select2-container {
      margin-top: 40px;
  }
  .gj_genr_dets {
      width: 100%;
      margin: 30px auto 0px;
  }
}
@media (max-width: 767px) {
  .gj_genr_dets {
      width: 100%;
  }
}
/* Responsive */
.page-content section.content.bgcolor-1 form span.select2.select2-container.select2-container--default.select2-container--focus, span.select2.select2-container.select2-container--default.select2-container--below, span.select2.select2-container.select2-container--default {
    width: 125px !important;
    margin: 15px auto !important;
    text-align: left;
}
.toppkfile {
    margin-left: 0px;
}
</style>

<script type="text/javascript">
  $(document).ready(function() {
    $("#gj_sel2_country").select2({
      placeholder: "Country",
    });
  });
  $("#gj_sel2_country").val('').trigger('change');

  $(document).ready(function() {
    $("#gj_sel2_pincode").select2({
      placeholder: "Pincode",
    });
  });
  $("#gj_sel2_pincode").val('').trigger('change');

  $(document).ready(function() {
      $("#gj_sel2_relation").select2({
        placeholder: "Relation",
      });
  });
  $("#gj_sel2_relation").val('').trigger('change');

  $(document).on('blur','input',function(){
    if($(this).val().length>=1){
      $(this).parent().addClass('input--filled');
    }
  });
</script>



<script> /* image upload */
  // function handleFileSelect(evt) {
  //   var files = evt.target.files; // FileList object

  //   // Loop through the FileList and render image files as thumbnails.
  //   for (var i = 0, f; f = files[i]; i++) {

  //     // Only process image files.
  //     if (!f.type.match('image.*')) {
  //       continue;
  //     }

  //     var reader = new FileReader();

  //     // Closure to capture the file information.
  //     reader.onload = (function(theFile) {
  //       return function(e) {
  //         // Render thumbnail.
  //        $('.pkfile').prepend('<div class="p1-0 mr-2 mb-2 bdrstyle_hover" style="float:left"><img class="width-60px bdrstyle" src="'+e.target.result+'"><span>X</span></div>');
  //        $('.bdrstyle_hover>span').on('click',function(){
  //         $(this).parent().remove();
  //       }); 
  //       };
  //     })(f);

  //     // Read in the image file as a data URL.
  //     reader.readAsDataURL(f);
  //   }
  // }

  document.getElementById('files').addEventListener('change', handleFileSelect, false);
</script>

<script> /* Identity uploads */
  function handleFileSelect(evt) {
      var files = evt.target.files; // FileList object
      // Loop through the FileList and render image files as thumbnails.
      for (var i = 0, f; f = files[i]; i++) {
          // Only process image files.
          if (!f.type.match('image.*')) {
            continue;
          }
        var reader = new FileReader();
          // Closure to capture the file information.
          reader.onload = (function(theFile) {
        console.log(evt.target.id);
            return function(e) {
                // Render thumbnail.
                if(evt.target.id=="gj_file11"){
                  $('.ifile').prepend('<div class="gj_absolute p1-0 mr-2 mb-2 bdrstyle_hover" style="float:left"><img class="width-60px bdrstyle" src="'+e.target.result+'"><span>X</span></div>');

                    $('.bdrstyle_hover>span').on('click',function(){
                        $(this).parent().remove();
                  }); 
                }else  if(evt.target.id=="gj_file44"){
                  $('.yfile').prepend('<div class="gj_absolute p1-0 mr-2 mb-2 bdrstyle_hover" style="float:left"><img class="width-60px bdrstyle" src="'+e.target.result+'"><span>X</span></div>');

                    $('.bdrstyle_hover>span').on('click',function(){
                        $(this).parent().remove();
                  }); 
                }else  if(evt.target.id=="gj_file55"){
                  $('.xfile').prepend('<div class="gj_absolute p1-0 mr-2 mb-2 bdrstyle_hover" style="float:left"><img class="width-60px bdrstyle" src="'+e.target.result+'"><span>X</span></div>');

                    $('.bdrstyle_hover>span').on('click',function(){
                        $(this).parent().remove();
                  }); 
                }
            };
          })(f);
          // Read in the image file as a data URL.
          reader.readAsDataURL(f);
      }
    }

   $('.bdrstyle_hover>span').on('click',function(){
       $(this).parent().remove();
    }); 

    document.getElementById('gj_file11').addEventListener('change', handleFileSelect, false);
    document.getElementById('gj_file44').addEventListener('change', handleFileSelect, false);
    document.getElementById('gj_file55').addEventListener('change', handleFileSelect, false);

</script>

<script type="text/javascript">
  $.validate({
    form:'.gj_form',
  });
</script>

<style type="text/css">
  .input--nao {
    margin: 0px 0px 15px;
    max-width: 100%;
  }
</style>