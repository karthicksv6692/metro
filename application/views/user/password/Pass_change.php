<div class="dk-body">
<div class="container-fluid">
	<div class="user-header">
		<!-- <h3>Password Change</h3> -->
	</div>
	<form method="post" action="<?php echo base_url('index.php/login/change_password');?>">
		<div class="rowr" style="margin-top: 1.5em;">
			<div class="col-md-4 pull-left">
				<span class="input input--nao">
              		<input class="input__field input__field--nao" type="password" id="input-1" name="pwd" value="admin123">
					<label class="input__label input__label--nao" for="input-1">
						<span class="input__label-content input__label-content--nao">Password </span>
					</label>
					<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
						<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
					</svg>
				</span>	
				<?php echo form_error('pwd','<span class="help-block form-error">', '</span>'); ?>
			</div>
		</div>		

		<div class="rowr">
			<div class="col-md-4 pull-left">
				<span class="input input--nao">
              		<input class="input__field input__field--nao" type="password" id="input-1" name="n_pwd" value="admin123">
					<label class="input__label input__label--nao" for="input-1">
						<span class="input__label-content input__label-content--nao">New Password </span>
					</label>
					<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
						<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
					</svg>
				</span>	
				<?php echo form_error('n_pwd','<span class="help-block form-error">', '</span>'); ?>
			</div>
		</div>
		<div class="rowr">
			<div class="col-md-4 pull-left">
				<span class="input input--nao">
              		<input class="input__field input__field--nao" type="password" id="input-1" name="rn_pwd" value="admin123">
					<label class="input__label input__label--nao" for="input-1">
						<span class="input__label-content input__label-content--nao">Repeat New Password </span>
					</label>
					<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
						<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
					</svg>
				</span>	
				<?php echo form_error('rn_pwd','<span class="help-block form-error">', '</span>'); ?>
			</div>
		</div>

			<div class="security_submit text-center">
				<button type="submit" class="btn btn_form" id="submit"> SUBMIT <i class="fa fa-paper-plane-o fa_form"></i></button>
			</div>
		</div>
	</form>
</div>
</div>

<style type="text/css">
	.rowr{
		width: 100%;
		clear: both;
	}
</style>