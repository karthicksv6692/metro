<?php if(isset($obj)){ ?>
	<?php if(count($obj)>0){ ?>
		<div class="dkbody">
			<div class="container-fluid">
				<div class="page-header">
					<h3>Access Details</h3>
				</div>
				<div class="page-content">
					<section class="content bgcolor-1">
						<form method="post" action="<?php echo base_url('index.php/user/stock_pointer/edit'); ?>" id="access_add">

							<div class="col-md-5 row form-group">
								<h4 style="width: 100%;">User Id : <?php echo $obj['user_id'] ?></h4><br><br>
								<h4 style="width: 100%;">User Name :<?php echo $obj['user_name'] ?></h4><br><br>
								<h4 style="width: 100%;">Available Amount :<?php echo $obj['stock_amount'] ?></h4>
							</div>	
							<input type="hidden" name="user_id" value="<?php echo $obj['user_id'] ?>">

							<div class="col-md-5 row form-group">
								
							</div>	


							
							<div class="country_width_100 col-12 mt-5">
								<div class="country_width_100">
									<div class="butt_sec_width mt-3 mb-3">
								      	<button type="submit" name="moduleadd_submit" class="country_button mr-2" >EDIT <i class="fa fa-paper-plane" aria-hidden="true"></i></button>		
									    <button type="reset" name="moduleadd_reset" class="country_button">RESET <i class="fa fa-refresh" aria-hidden="true"></i></button>
							      	
									</div>
						      	</div>
						      	
						        <div class="country_width_100 mt-3 mb-3">					      		  
								    <div class="country-right">
								      	<a href="<?php echo base_url('index.php/user/stock_pointer'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button>
								    </div>	</a>
								    </div>	
								</div>
							</div>				

						</form>
					</section>
				</div>
			</div>
		</div>
		<style type="text/css">
			.page-content section.content.bgcolor-1 form span.select2.select2-container.select2-container--default.select2-container--focus, span.select2.select2-container.select2-container--default.select2-container--below, span.select2.select2-container.select2-container--default {
			    width: 300px !important;
			    margin: 15px auto !important;
			    text-align: left;
			}
			.r_style{
				padding-top: 1.5em;
			    margin-left: -23px;
			    font-size: 25px;
			    color: #968585;
			}
		</style>
	<?php }else{ ?>
		<div class="dkbody">
			<div class="country_width_100 mt-3 mb-3">					      		  
			    <div class="country-right">
			      	<a href="<?php echo base_url('index.php/user/stock_pointer'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button>
			    </div>	</a>
			    </div>	
			</div>
		</div>

	<?php } ?>
<?php }else{ ?>
		<div class="dkbody">
			<div class="country_width_100 mt-3 mb-3">					      		  
			    <div class="country-right">
			      	<a href="<?php echo base_url('index.php/user/stock_pointer'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button>
			    </div>	</a>
			    </div>	
			</div>
		</div>

<?php } ?>

