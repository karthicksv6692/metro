<div class="dkbody">
	<div class="container-fluid">
		<div class="page-header">
			<h3>Access Details</h3>
		</div>
		<div class="page-content">
			<section class="content bgcolor-1">
				<form method="post" action="<?php echo base_url('index.php/user/stock_pointer/add'); ?>" id="access_add">

					<div class="col-md-5 row form-group">
						<label for="id_label_single" class="input input--nao">
							<select class="js-example-basic-single select2_style user_id" name="user_id"
							 data-validation="required number"
							>
								
							</select>
						</label>
					</div>	

					<div class="col-md-1 row form-group"><p class="r_style">(or)</p></div>

					<div class="col-md-5 row form-group">
						<label for="id_label_single" class="input input--nao">
							<select class="js-example-basic-single select2_style user_name" name="user_id"
							 data-validation="required number"
							>
								
							</select>
						</label>
					</div>	


					<div class="rr">
						<div class="col-md-5 row form-group">
							<span class="input input--nao ">
								<input class="input__field input__field--nao" type="number" 
								  name="stock_amount" value="<?php echo set_value('stock_amount'); ?>" 
								   data-validation="required" />
								<label class="input__label input__label--nao" for="access_add_name">
									<span class="input__label-content input__label-content--nao">Stock Amount</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
							</span>		
						</div>
					</div>

					<input type="hidden" name="acc" value="asdf">
					<div class="country_width_100 col-12 mt-5">
						<div class="country_width_100">
							<div class="butt_sec_width mt-3 mb-3">
						      	<button type="submit" name="moduleadd_submit" class="country_button mr-2" >SUBMIT <i class="fa fa-paper-plane" aria-hidden="true"></i></button>		
							    <button type="reset" name="moduleadd_reset" class="country_button">RESET <i class="fa fa-refresh" aria-hidden="true"></i></button>
					      	
							</div>
				      	</div>
				      	
				        <div class="country_width_100 mt-3 mb-3">					      		  
						    <div class="country-right">
						      	<a href="<?php echo base_url('index.php/user/stock_pointer'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button>
						    </div>	</a>
						    </div>	
						</div>
					</div>				

				</form>
			</section>
		</div>
	</div>
</div>

<script type="text/javascript">
	
	$('.user_id').select2({
	    placeholder: 'Select User Name',
	    ajax: {
	      url: '<?php echo base_url("index.php/common_controller/get_all_user_name_for_stockpointer") ?>',
	      dataType: 'json',
	      delay: 0,
	      data: function (params) {console.log(params);
	            return { q: params.term // search term
	            };
	      },
	      processResults: function (data) { return { results: data }; },
	      cache: true
	    },
  	});
  	$('.user_name').select2({
	    placeholder: 'Select User Id',
	    ajax: {
	      url: '<?php echo base_url("index.php/common_controller/get_all_user_id_for_stockpointer") ?>',
	      dataType: 'json',
	      delay: 0,
	      data: function (params) {console.log(params);
	            return { q: params.term // search term
	            };
	      },
	      processResults: function (data) { return { results: data }; },
	      cache: true
	    },
  	});
</script>
<style type="text/css">
	.page-content section.content.bgcolor-1 form span.select2.select2-container.select2-container--default.select2-container--focus, span.select2.select2-container.select2-container--default.select2-container--below, span.select2.select2-container.select2-container--default {
	    width: 300px !important;
	    margin: 15px auto !important;
	    text-align: left;
	}
	.r_style{
		padding-top: 1.5em;
	    margin-left: -23px;
	    font-size: 25px;
	    color: #968585;
	}
</style>

