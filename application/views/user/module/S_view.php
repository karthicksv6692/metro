<?php if(isset($module_obj)){ 
	if($module_obj->getModuleId() != null)	{
		$module_id = $module_obj->getModuleId();
	}else{$module_id = "";}

	if($module_obj->getModuleName() != null)	{
		$module_name = $module_obj->getModuleName();
	}else{$module_name = "";}

	if($module_obj->getModuleDesc() != null)	{
		$module_desc = $module_obj->getModuleDesc();
	}else{$module_desc = "";}
?>
	<div class="dkbody">
		<div class="container-fluid">
			<div class="page-header">
				<h3>Module Details</h3>
			</div>
			<form method="post" action="<?php echo base_url('index.php/user/module_crud/edit'); ?>">
				<div class="page-content">
					<section class="content bgcolor-1">
						<div class="col-md-6 pull-left table-responsive">
							<table style="width: 100%" class="single_view_table">
								<tr>
									<td class="single_table_view_head">Module Name</td>
									<td class="single_table_view_head">:</td>
									<td class="single_table_view_data"><?php echo $module_name; ?></td>
								</tr>
								<tr>
									<td class="single_table_view_head">description</td>
									<td class="single_table_view_head">:</td>
									<td class="single_table_view_data"><?php echo $module_desc; ?></td>
								</tr>
								
							</table>
						</div>
						<input type="hidden" name="module_id" value="<?php echo $module_id; ?>">
						<div class="country_width_100 col-12 mt-5">
								<div class="country_width_100">
									<div class="butt_sec_width mt-3 mb-3">
								      	<button type="submit" name="country_submit" class="country_button mr-2" >EDIT <i class="fa fa-pencil" aria-hidden="true"></i></button>		
									   	<a href="<?php echo base_url('index.php/user/module_crud'); ?>"> <button type="reset" name="country_reset" class="country_button">CANCEL <i class="fa fa-trash" aria-hidden="true"></i></button></a>
							      	
									</div>
						      	</div>
						      	
						        <div class="country_width_100 mt-3 mb-3">					      		  
								    <div class="country-right">
								      		<a href="<?php echo base_url('index.php/user/module_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button></a>
								    </div>	
								</div>
							</div>
					</section>
				</div>
			</form>
		</div>
	</div>
<?php }else{ ?>

	<div class="dkbody">
	<div class="container-fluid">
		<div class="page-header">
			<h3>Select</h3>
		</div>
		<?php echo $search; ?>
	</div>
</div>


<script type="text/javascript">
	$("#combobox").select2();
</script>

<?php }  ?>