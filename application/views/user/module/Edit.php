<?php if(isset($module_obj) && count($module_obj)>0){ 

	if($module_obj->getModuleId() != null)	{
		$module_id = $module_obj->getModuleId();
	}else{$module_id = "";}

	if($module_obj->getModuleName() != null)	{
		$module_name = $module_obj->getModuleName();
	}else{$module_name = "";}

	if($module_obj->getModuleDesc() != null)	{
		$module_desc = $module_obj->getModuleDesc();
	}else{$module_desc = "";}

?>

	<div class="dkbody">
		<div class="container-fluid">
			<div class="page-header">
				<h3>Module Details</h3>
			</div>
			<form method="post" action="<?php echo base_url('index.php/user/module_crud/edit'); ?>">
				<div class="page-content">
					<section class="content bgcolor-1">
						<form id='bankadd'>
							<div class="col-md-5 row form-group">
								<span class="input input--nao">
									<input class="input__field input__field--nao module_name" type="text" id="module_add_name"
									 name="module_name" value="<?php echo $module_name; ?>" 
									 data-validation="required length alphanumeric"
			                        data-validation-length="min3"
			                        data-validation-ignore="-_ "
									 />
									<label class="input__label input__label--nao" for="module_add_name">
										<span class="input__label-content input__label-content--nao">Module Name</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></path>
									</svg>
									<img src="<?php echo base_url('assets/images/loader/') ?>default.gif" class="ldr pin_ldr">

								</span>		
								<p class="pin_suc avail_er suc">Available</p>
								<p class="pin_err avail_er err">Already Exists</p>
								<?php echo form_error('module_name','<span class="help-block form-error">', '</span>'); ?>
							</div>
							<div class="col-md-5 row pull-left">
								<span class="input input--nao">
									
									 <textarea class="input__field input__field--nao" rows="3" id="module_add_description" name="module_desc"> <?php echo $module_desc; ?> </textarea >
									<label class="input__label input__label--nao" for="module_add_description">
										<span class="input__label-content input__label-content--nao">Description</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></path>
									</svg>
									

								</span>	
								<?php echo form_error('module_desc','<span class="help-block form-error">', '</span>'); ?>	
								
							</div>
							
		                    <input type="hidden" name="edit_module_id" value="<?php echo $module_id; ?>">
							<div class="country_width_100 col-12 mt-5">
								<div class="country_width_100">
									<div class="butt_sec_width mt-3 mb-3">
								      	<button type="submit" name="moduleadd_submit" class="country_button mr-2" >SUBMIT <i class="fa fa-paper-plane" aria-hidden="true"></i></button>		
									    <button type="reset" name="moduleadd_reset" class="country_button">RESET <i class="fa fa-refresh" aria-hidden="true"></i></button>
							      	
									</div>
						      	</div>
						      	
						        <div class="country_width_100 mt-3 mb-3">					      		  
								    <div class="country-right">
								      	<a href="<?php echo base_url('index.php/user/module_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button></a>
								    </div>	
								</div>
							</div>
						</form>
					</section>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
		$(function(){
			$('.pin_suc,.pin_err,.pin_ldr').hide();
		})
		$('.module_name').focus(function()  {
		  var a = $('.module_name').val();
		  if(a.length>0){
		    cc_name = a;
		  }else{
		    cc_name="";
		  }
		})
		$('.module_name').blur(function()  {
		    var module_name = $('.module_name').val();
		    if(module_name.length>4 && cc_name != module_name){
		      $('.pin_ldr').show();
		      $.ajax({
		        type: "POST",
		        url: "<?php echo base_url(); ?>index.php/Common_controller/module_name_available_on_update",
		        data: {'field_value':module_name},
		          success:  function (data) { 
		            $('.pin_ldr').hide();
		            if(data == 1){
		            	$('.pin_suc').show();
		            	$('.pin_err').hide();
		            }else{
		            	$('.pin_suc').hide();
		            	$('.pin_err').show();
		            }
		          }
		      })
		    }else if(module_name.length == 0 && cc_name.length > 2){
		     $('.pin_suc,.pin_err').hide();
		    }
	  	})
	  	$(document).on("click", "button[type='reset']", function(){
		 $('.pin_suc,.pin_err,.pin_ldr').hide();
		});
	</script>
<?php }else{ ?>

	<div class="dkbody">
	<div class="container-fluid">
		<div class="page-header">
			<h3>Select</h3>
		</div>
		<?php echo $search; ?>
	</div>
</div>


<script type="text/javascript">
	$("#combobox").select2();
</script>

<?php }  ?>
