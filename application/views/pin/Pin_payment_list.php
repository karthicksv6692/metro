
<input type="hidden" class="package_id" value="<?php echo $details['package_id'] ?>">
<input type="hidden" class="from" value="<?php echo $details['from'] ?>">
<input type="hidden" class="to" value="<?php echo $details['to'] ?>">

<div class="dkbody">
  <div class="container-fluid">
    <div class="page-header">
      <h3>Payout-Pin Wise</h3>
    </div>
  
    <div class="page-content" style="margin-bottom: 2em">                   
      <div class="container-fluid">
        <table  id="gj_tree_struct_vw" class="table table-striped table-bordered table-responsive data_table_style" cellspacing="10" width="100%">
          <thead>
            <tr>
              <th class="text-center" style="min-width: 35px">#</th>
              <th class="text-center" style="min-width: 80px">PIN NO</th>
              <th class="text-center" style="min-width: 30px">USER ID</th>
              <th class="text-center" style="min-width: 80px">USER NAME</th>
              <th class="text-center" style="min-width: 80px">FIRST NAME</th>
              <th class="text-center" style="min-width: 80px">PACKAGE</th>
              <th class="text-center" style="min-width: 40px">SUB PACKAGE</th>
              <th class="text-center" style="min-width: 60px">PRICE</th>
              <th class="text-center" style="min-width: 100px">DATE</th>
            </tr>
          </thead>
          <tbody style="text-align: center;">
          </tbody>
          <tfoot>
            <tr>
              <td colspan="6"></td>
              <td><h4>Total:</h4></td>
              <td><h4 class="tot"></h4></td>
              <td colspan="1"></td>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>


  </div>
</div>

<script type="text/javascript">
  $(function(){
    $('.tot').text('00.00');
  })
  var fr = $('.from').val()
  var to = $('.to').val()
  var id = $('.package_id').val()
  $('#gj_tree_struct_vw').DataTable({
    "processing": true,
    "serverSide": true,
    "bInfo": false,
    "ajax": {
      "url": '<?php echo base_url(); ?>index.php/pin/pin_crud/datatable_pin_payment_list',
      "data": {'id':id,'fr':fr,'to':to},
    }
  });
  $.ajax({
    type  :   'post',
    url   :   "<?php echo base_url('index.php/pin/pin_crud/get_total_count_package_price'); ?>",
    data  :  {'id':id,'dt1':fr,'dt2':to},
    success: function(data){
      if(data != 0){
        $('.tot').text(data);
      }else{
        $('.tot').text('00.00');
      }
    }
  })
</script>