
<div class="dkbody">
  <div class="container-fluid">

    <div class="col-md-12">
      <?php if($_SESSION['role_id'] == RT::$default_stock_pointer_id){ ?>
        <a href="<?php echo base_url('index.php/shopping/shop_pin_requests'); ?>" target="new">
        <div class="col-md-3" style="margin-bottom: 2em">
          <div class="s_box1">
            <h3 class="shp">Shopping Pin Requests</h3>
          </div>
        </div>
      </a>
      <?php } ?>

      <a href="<?php echo base_url('index.php/pin/pin_store/used'); ?>" target="new">
        <div class="col-md-2">
          <div class="s_box">
            <h3 class="usd">Used Pins</h3>
          </div>
        </div>
      </a>

      <a href="<?php echo base_url('index.php/pin/pin_store/un_used'); ?>" target="new">
        <div class="col-md-2">
          <div class="s_box">
            <h3 class="unusd">Un Used Pins</h3>
          </div>
        </div>
      </a>

      <a href="<?php echo base_url('index.php/pin/pin_store/pin_transfer'); ?>" target="new">
        <div class="col-md-2">
          <div class="s_box">
            <h3 class="pntr">Pin Transfer</h3>
          </div>
        </div>
      </a>

      <a href="<?php echo base_url('index.php/pin/pin_store/pin_transfer_history'); ?>" target="new">
        <div class="col-md-3" style="margin-bottom: 2em">
          <div class="s_box1">
            <h3 class="pntrh">Pin Transfer History</h3>
          </div>
        </div>
      </a>
      <style type="text/css">
        .s_box {
            background: #2bb1e9;
            text-align: center;
            height: 45px;
        }
        .s_box1 {
            background: #2bb1e9;
            text-align: center;
            height: 45px;
        }
        .s_box h3{
            padding-top: 9%;
            font-size: 19px !important;
            border: 1px solid #2bb1e9;
            background: #fff;
            color: #2bb1e9;
            padding-bottom: 8%;
        }
        .s_box1 h3{
          padding-top: 6%;
          font-size: 19px !important;
          border: 1px solid #2bb1e9;
          background: #fff;
          color: #2bb1e9;
          padding-bottom: 4%;
        }
        .s_box1.active h3,.s_box.active h3,.s_box1 h3:hover,.s_box h3:hover{
          background: #2bb1e9;
          color: #fff;
        }
        h3.pntr{
          background: #2bb1e9;
          color: #fff;
        }
      </style>
    </div>



    <!-- <div class="page-header">
      <h3>Pin  Transfer</h3>
    </div> -->
    <form method="post" action="<?php echo base_url('index.php/pin/pin_store/pin_transfer'); ?>">
      <div class="rr">
        <div class="col-md-4">
          <div style="">
            <select class="user form-control" style="width:500px" name="user"></select>
          </div>
          <?php echo form_error('user','<p class="error">', '</p>'); ?>
        </div>
      </div>
      <div class="rr">
        <div class="col-md-8 text-center" style="margin-top: 16px;margin-left: -40px;">
            <div class="butt_sec_width mt-3 mb-3 pull-left">
                  <button type="submit" name="country_submit" class="country_button mr-2" style="width: 100px;" >TRANSFER <i class="fa fa-paper-plane" aria-hidden="true"></i></button>   
                  <a href="<?php echo base_url('index.php/pin/pin_store/pin_store_viewx'); ?>"><button type="button" name="country_reset" class="country_button" >BACK <i class="fa fa-refresh" aria-hidden="true"></i></button></a>
            </div>
        </div>
      </div>
      <div class="rr" style="margin-bottom: 2em">
        <table id="bank_request" class="data_table_style">
          <thead>
             <tr role="row">
                <th>#</th>
                <th>PIN NO</th>
                <th>REQUEST FOR</th>
                <th>PACKAGE</th>
                <th>SUB PACKAGE</th>
                <th>ACTIONS</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <input type="hidden" value="0" class="hid_ar" name="hid_ar">
      <input type="hidden" value="ahid_frmsd" name="hid_frm">
    </form>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
      var t = $('#bank_request').DataTable( {
          "processing": true,
          "serverSide": true,
          "ajax": '<?php echo base_url(); ?>index.php/pin/pin_store/datatable_ajax_unused_pins_for_transfer',
          "order": [[ 0, "asc" ]],
          "pagingType": "full_numbers"
      });

      t.on( 'order.dt search.dt', function () {
          t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              cell.innerHTML = i+1;
          } );
      } ).draw();
  });
  
  $('.user').select2({
    placeholder: 'Select a User',
    ajax: {
      url: '<?php echo base_url("index.php/common_controller/get_all_active_users") ?>',
      dataType: 'json',
      delay: 1,
      data: function (params) {console.log(params);
            return { q: params.term // search term
            };
      },
      processResults: function (data) { return { results: data }; },
      cache: true
    },
  });
  function func(id){
    var ar = $('.hid_ar').val();
    var res = ar.split(",");

    if(res.indexOf(""+id) == -1){
      res.push(id);
    }else{
      res.splice( res.indexOf(""+id), 1 );
    }
    if(res.indexOf("0") >= 0){
      res.splice( res.indexOf("0"), 1 );
    }
    res.join(',');
    $('.hid_ar').val(res);
    
  }
</script>