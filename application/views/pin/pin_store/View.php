<div class="dkbody">
  <div class="container-fluid">
    <div class="page-header">
      <h3>Pin-Store All Pins</h3>
    </div>
    <div class="page-content">                   
        <div class="container-fluid">
          <table id="bank_request" class="data_table_style">
              <thead>
                 <tr role="row">
                    <th>#</th>
                    <th>PIN NO</th>
                    <th>REQUEST FOR</th>
                    <th>PACKAGE</th>
                    <th>SUB PACKAGE</th>
                    <th>ACTIONS</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>
         <?php //if($add != null){ ?>
         <div class="per_rw btn-click pr-5">
              <a href="<?php echo base_url('index.php/pin/pin_crud/add'); ?>"><button type="button" class="btn btn_row1  btn-7 btn-7a icon-truck mb-5" id="row1"> <span>ADD NEW</span></button></a>
          </div>
        <?php //} ?>
      </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
   var t =  $('#bank_request').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": '<?php echo base_url(); ?>index.php/pin/pin_store/datatable_ajax',
        "order": [[ 0, "asc" ]],
        "pagingType": "full_numbers"
    });
   t.on( 'order.dt search.dt', function () {
          t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              cell.innerHTML = i+1;
          } );
      } ).draw();
});
</script>
