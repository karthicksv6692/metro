<?php if(count($obj) != null){ ?>
  <input type="hidden" value="<?php echo $obj['id'] ?>" class="id">
  <input type="hidden" value="<?php echo $obj['pack_id'] ?>" class="pack_id">
  <input type="hidden" value="<?php echo $obj['type'] ?>" class="type">
  <input type="hidden" value="<?php echo $obj['status'] ?>" class="status">
<?php } ?>

<div class="dkbody">
  <div class="container-fluid">
    <div class="page-header">
      <h3>Pin Lists</h3>
    </div>
    <div class="page-content">                   
        <div class="container-fluid">
          <table id="bank_request" class="data_table_style">
              <thead>
                 <tr role="row">
                    <th>#</th>
                    <th>PIN NO</th>
                    <th>DATE</th>
                    <th>PACKAGE</th>
                    <th>SUB-PACKAGE</th>
                    <th>PRICE</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>
         
      </div>
  </div>
</div>



<script type="text/javascript">
$(document).ready(function() {
  var id = $('.id').val();
  var pack_id = $('.pack_id').val();
  var type = $('.type').val();
  var status = $('.status').val();
     $('#bank_request').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax":{
          "url" : '<?php echo base_url(); ?>index.php/pin/pin_store/datatable_particular_pins_list',
          "data": {'id':id,'pack_id':pack_id,'type':type,'status':status},
        },
        
        "order": [[ 0, "asc" ]],
        "pagingType": "full_numbers"
    });
    
});
</script>
