<div class="dkbody">
  <div class="container-fluid">
    <?php if($_SESSION['user_id'] == 2){ ?>
      <div class="col-md-12">
        <a href="<?php echo base_url('index.php/pin/pin_store/pin_transfer'); ?>" target="new">
          <div class="col-md-3">
            <div class="s_box">
              <h3 style="padding-top: 5%;padding-bottom: 5%;" class="transfer">Pin Transfer</h3>
            </div>
          </div>
        </a>
        <a href="<?php echo base_url('index.php/pin/pin_crud/admin_transfer_pin_view'); ?>" target="new">
          <div class="col-md-3" style="margin-bottom: 2em">
            <div class="s_box1">
              <h3 class="records">Pin Records</h3>
            </div>
          </div>
        </a>
        <a href="<?php echo base_url('index.php/shopping/shop_pin_requests'); ?>" target="new">
          <div class="col-md-3" style="margin-bottom: 2em">
            <div class="s_box1">
              <h3 class="req">Shopping Pin Requests</h3>
            </div>
          </div>
        </a>
        <a href="<?php echo base_url('index.php/pin/pin_store/pin_transfer_history'); ?>" target="new">
          <div class="col-md-3" style="margin-bottom: 2em">
            <div class="s_box1">
              <h3 class="his">Pin Transfer History</h3>
            </div>
          </div>
        </a>
          <!-- <a href="<?php echo base_url('index.php/pin/pin_store/un_used'); ?>" target="new">
                <div class="col-md-3" style="margin-bottom: 2em">
                  <div class="s_box1">
                    <h3 class="unu">Un Used</h3>
                  </div>
                </div>
              </a> -->
        <style type="text/css">
          .s_box {
              background: #2bb1e9;
              text-align: center;
              height: 45px;
          }
          .s_box1 {
              background: #2bb1e9;
              text-align: center;
              height: 45px;
          }
          .s_box h3{
              padding-top: 9%;
              font-size: 19px !important;
              border: 1px solid #2bb1e9;
              background: #fff;
              color: #2bb1e9;
              padding-bottom: 8%;
          }
          .s_box1 h3{
            padding-top: 6%;
            font-size: 19px !important;
            border: 1px solid #2bb1e9;
            background: #fff;
            color: #2bb1e9;
            padding-bottom: 4%;
          }
          .s_box1.active h3,.s_box.active h3,.s_box1 h3:hover,.s_box h3:hover{
            background: #2bb1e9;
            color: #fff;
          }
          h3.his{
            background: #2bb1e9;
            color: #fff;
          }
        </style>
      </div>
    <?php }else{ ?>
      <div class="col-md-12">
      <?php if($_SESSION['role_id'] == RT::$default_stock_pointer_id){ ?>
        <a href="<?php echo base_url('index.php/shopping/shop_pin_requests'); ?>" target="new">
        <div class="col-md-3" style="margin-bottom: 2em">
          <div class="s_box1">
            <h3 class="shp">Shopping Pin Requests</h3>
          </div>
        </div>
      </a>
      <?php } ?>

      <a href="<?php echo base_url('index.php/pin/pin_store/used'); ?>" target="new">
        <div class="col-md-2">
          <div class="s_box">
            <h3 class="usd">Used Pins</h3>
          </div>
        </div>
      </a>

      <a href="<?php echo base_url('index.php/pin/pin_store/un_used'); ?>" target="new">
        <div class="col-md-2">
          <div class="s_box">
            <h3 class="unusd">Un Used Pins</h3>
          </div>
        </div>
      </a>

      <a href="<?php echo base_url('index.php/pin/pin_store/pin_transfer'); ?>" target="new">
        <div class="col-md-2">
          <div class="s_box">
            <h3 class="pntr">Pin Transfer</h3>
          </div>
        </div>
      </a>

      <a href="<?php echo base_url('index.php/pin/pin_store/pin_transfer_history'); ?>" target="new">
        <div class="col-md-3" style="margin-bottom: 2em">
          <div class="s_box1">
            <h3 class="pntrh">Pin Transfer History</h3>
          </div>
        </div>
      </a>
      <style type="text/css">
        .s_box {
            background: #2bb1e9;
            text-align: center;
            height: 45px;
        }
        .s_box1 {
            background: #2bb1e9;
            text-align: center;
            height: 45px;
        }
        .s_box h3{
            padding-top: 9%;
            font-size: 19px !important;
            border: 1px solid #2bb1e9;
            background: #fff;
            color: #2bb1e9;
            padding-bottom: 8%;
        }
        .s_box1 h3{
          padding-top: 6%;
          font-size: 19px !important;
          border: 1px solid #2bb1e9;
          background: #fff;
          color: #2bb1e9;
          padding-bottom: 4%;
        }
        .s_box1.active h3,.s_box.active h3,.s_box1 h3:hover,.s_box h3:hover{
          background: #2bb1e9;
          color: #fff;
        }
        h3.pntrh{
          background: #2bb1e9;
          color: #fff;
        }
      </style>
    </div>
    <?php } ?>
  

    <div class="rr">
        <div class="col-md-3 pull-left form-group">
          <span class="input input--nao" id="sandbox-container">
            <input type="text" class="input__field input__field--nao form-control dtt1"  name="dob" value="" 
             data-validation="date" />
             <!-- data-validation-format="yyyy-mm-dd" -->
            <label class="input__label input__label--nao" for="input-1">
              <span class="input__label-content input__label-content--nao">From</span>
            </label>
            <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
              <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
            </svg>
          </span> 
        </div>

        <div class="col-md-2 pull-left row">
            <select class="form-control slt_type pos1" id="exampleSelect1" name="">
                <option value="0">First Cutoff</option>
                <option value="1">Second Cutoff</option>
                <option value="2">All</option>
            </select>
            </label>
        </div>

        <div class="col-md-3 pull-left form-group">
          <span class="input input--nao" id="sandbox-container">
            <input type="text" class="input__field input__field--nao form-control dtt2"  name="dob" value="" 
             data-validation="date" />
             <!-- data-validation-format="yyyy-mm-dd" -->
            <label class="input__label input__label--nao" for="input-1">
              <span class="input__label-content input__label-content--nao">To</span>
            </label>
            <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
              <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
            </svg>
          </span> 
        </div>

        <div class="col-md-2 pull-left row">
            <select class="form-control slt_type pos2" id="exampleSelect1" name="">
                <option value="0">First Cutoff</option>
                <option value="1">Second Cutoff</option>
            </select>
            </label>
        </div>

        
        <button type="button" class="btn btn_row1  btn-7 btn-7a icon-truck mb-5" id="date-search"> <i class="fa fa-search" aria-hidden="true"></i></button>
    </div>



    <div class="page-content">                   
        <div class="asd">
          <table id="bank_request" class="data_table_style">
              <thead>
                 <tr role="row">
                    <th>#</th>
                    <th>PIN NO</th>
                    <th>REQUEST FOR</th>
                    <th>PACKAGE</th>
                    <th>SUB-PACKAGE</th>
                    <th>CREATED BY</th>
                    <th>TRASFER DATE</th>
                    <th>TRASFER ID</th>
                    <th>STATUS</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>
        <div class="per_rw btn-click pr-5">
              <a href="<?php echo base_url('index.php/pin/pin_store/pin_store_viewx'); ?>"><button type="button" class="btn btn_row1  btn-7 btn-7a icon-truck mb-5" id="row1"> <span>BACK</span></button></a>
          </div>
      </div>
  </div>
</div>

<script type="text/javascript">
  var tab = "";
$(document).ready(function() {
    tab =  $('#bank_request').DataTable();
     // {
     //    "processing": true,
     //    "serverSide": true,
     //    "ajax": '<?php echo base_url(); ?>index.php/pin/pin_store/datatable_ajax_tranfer_pins',
     //    "order": [[ 0, "asc" ]],
     //    "pagingType": "full_numbers"
     //  }
    
});
document.querySelector("#date-search").addEventListener('click',function(){
  dt1 = $('.dtt1').val();
  pos1 = $('.pos1').val();

  dt2 = $('.dtt2').val();
  pos2 = $('.pos2').val();
  if(dt1.length>0){
    tab.destroy();
    tab = $('#bank_request').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax": {
        "url": '<?php echo base_url(); ?>index.php/pin/pin_store/datatable_ajax_tranfer_pins_search',
        "data": {'dt1':dt1,'pos1':pos1,'dt2':dt2,'pos2':pos2},
      },
    })
  }
})
</script>

<style type="text/css">
  select.form-control:not([size]):not([multiple]) {
    height: 100%;
    margin-top: 40px;
  }
  button#date-search {
    height: 50px;
    width: 60px;
    margin-left: 20px;
  }
  i.fa.fa-search {
    font-size: 25px;
  }
</style>
