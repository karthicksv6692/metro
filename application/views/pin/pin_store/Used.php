<div class="dkbody">
  <div class="container-fluid">
    <div class="col-md-12">
      <?php if($_SESSION['role_id'] == RT::$default_stock_pointer_id){ ?>
        <a href="<?php echo base_url('index.php/shopping/shop_pin_requests'); ?>" target="new">
        <div class="col-md-3" style="margin-bottom: 2em">
          <div class="s_box1">
            <h3 class="shp">Shopping Pin Requests</h3>
          </div>
        </div>
      </a>
      <?php } ?>

      <a href="<?php echo base_url('index.php/pin/pin_store/used'); ?>" target="new">
        <div class="col-md-2">
          <div class="s_box">
            <h3 class="usd">Used Pins</h3>
          </div>
        </div>
      </a>

      <a href="<?php echo base_url('index.php/pin/pin_store/un_used'); ?>" target="new">
        <div class="col-md-2">
          <div class="s_box">
            <h3 class="unusd">Un Used Pins</h3>
          </div>
        </div>
      </a>

      <a href="<?php echo base_url('index.php/pin/pin_store/pin_transfer'); ?>" target="new">
        <div class="col-md-2">
          <div class="s_box">
            <h3 class="pntr">Pin Transfer</h3>
          </div>
        </div>
      </a>

      <a href="<?php echo base_url('index.php/pin/pin_store/pin_transfer_history'); ?>" target="new">
        <div class="col-md-3" style="margin-bottom: 2em">
          <div class="s_box1">
            <h3 class="pntrh">Pin Transfer History</h3>
          </div>
        </div>
      </a>
      <style type="text/css">
        .s_box {
            background: #2bb1e9;
            text-align: center;
            height: 45px;
        }
        .s_box1 {
            background: #2bb1e9;
            text-align: center;
            height: 45px;
        }
        .s_box h3{
            padding-top: 9%;
            font-size: 19px !important;
            border: 1px solid #2bb1e9;
            background: #fff;
            color: #2bb1e9;
            padding-bottom: 8%;
        }
        .s_box1 h3{
          padding-top: 6%;
          font-size: 19px !important;
          border: 1px solid #2bb1e9;
          background: #fff;
          color: #2bb1e9;
          padding-bottom: 4%;
        }
        .s_box1.active h3,.s_box.active h3,.s_box1 h3:hover,.s_box h3:hover{
          background: #2bb1e9;
          color: #fff;
        }
        h3.usd{
          background: #2bb1e9;
          color: #fff;
        }
      </style>
    </div>
    <div class="page-content">                   
        <div class="container-fluidasd">
          <table id="bank_request" class="data_table_style">
              <thead>
                 <tr role="row">
                    <th>#</th>
                    <th>PIN NO</th>
                    <th>USED USER ID</th>
                    <th>TYPE</th>
                    <th>PACKAGE</th>
                    <th>SUB-PACKAGE</th>
                    <th>USED TIME</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>
         <?php //if($add != null){ ?>
         <div class="per_rw btn-click pr-5">
              <a href="<?php echo base_url('index.php/pin/pin_store/pin_store_viewx'); ?>"><button type="button" class="btn btn_row1  btn-7 btn-7a icon-truck mb-5" id="row1"> <span>BACK</span></button></a>
          </div>
        <?php //} ?>
      </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
     $('#bank_request').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": '<?php echo base_url(); ?>index.php/pin/pin_store/datatable_ajax_used_pins',
        "order": [[ 0, "asc" ]],
        "pagingType": "full_numbers"
    });
   
});
</script>
