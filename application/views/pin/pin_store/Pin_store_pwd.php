<div class="dk_body container-fluid">
	<div class="s_box">
		<p></p>
		<form method="post" action="<?php echo base_url('index.php/pin/pin_store/pin_store_view'); ?>">
			<div class="rowrr text-center">	
				<div class="col-md-4 pull-left row" style="margin-top: 1em">
					<span class="input input--nao">
						<input class="input__field input__field--nao package" name="pwd" type="password" value="">
						<label class="input__label input__label--nao" for="input-1">
							<span class="input__label-content input__label-content--nao">Enter Pin Store Password</span>
						</label>
						<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
							<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
						</svg>
					</span>	
					<input type="hidden" class="package_hid_id"  name="package_id">
					<input type="hidden" class="package_pack_price" name="">
				</div>
			</div>
			<div class="country_width_100 col-12 mt-5" style="margin-bottom: 2em">
				<div class="country_width_100">
					<div class="butt_sec_width mt-3 mb-3">
				      	<button type="submit" name="country_submit" class="country_button mr-2" >SUBMIT <i class="fa fa-paper-plane" aria-hidden="true"></i></button>		
					    <button type="reset" name="country_reset" class="country_button" onclick="clear_form()">RESET <i class="fa fa-refresh" aria-hidden="true"></i></button>
					</div>
		      	</div>
	      	</div>
	    </form>
	</div>
</div>