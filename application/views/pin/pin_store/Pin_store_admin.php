<?php //print_r($obj); ?>

<div class="dkbody container-fluid">
  <div class="hole_body">
    <div class="rr">
        <a href="<?php echo base_url('index.php/pin/pin_store/pin_transfer'); ?>" target="new">
          <div class="col-md-3">
            <div class="s_box">
              <h3>Pin Transfer</h3>
            </div>
          </div>
        </a>
  <a href="<?php echo base_url('index.php/pin/pin_crud/admin_transfer_pin_view'); ?>" target="new">
    <div class="col-md-3" style="margin-bottom: 2em">
      <div class="s_box1">
        <h3>Pin Records</h3>
      </div>
    </div>
  </a>
  <a href="<?php echo base_url('index.php/shopping/shop_pin_requests'); ?>" target="new">
        <div class="col-md-3" style="margin-bottom: 2em">
          <div class="s_box1">
            <h3>Shopping Pin Requests</h3>
          </div>
        </div>
      </a>
        <!-- <a href="<?php //echo base_url('index.php/pin/pin_store/used'); ?>" target="new">
        <div class="col-md-2">
          <div class="s_box">
            <h3>Used Pins</h3>
          </div>
        </div>
      </a>

      <a href="<?php //echo base_url('index.php/pin/pin_store/un_used'); ?>" target="new">
        <div class="col-md-2">
          <div class="s_box">
            <h3>Un Used Pins</h3>
          </div>
        </div>
      </a> -->

      <a href="<?php echo base_url('index.php/pin/pin_store/pin_transfer_history'); ?>" target="new">
        <div class="col-md-3" style="margin-bottom: 2em">
          <div class="s_box1">
            <h3>Pin Transfer History</h3>
          </div>
        </div>
      </a>
      

      
    </div>
    <div class="rr">
      <div class="col-md-12 col-lg-12 gj_tree_struct">
          <div class="gj_tree_div text-center">
              <div class="rowx rrx" style="margin-bottom: 1em">
                <div class="col-lg-4 col-md-4 pull-left pr-0 pl-0">
                  <div style="">
                    <select class="user_id form-control" style="" name="user_id"></select>
                  </div>
                </div>

                <div class="col-lg-4 col-md-4 pull-left pr-0 pl-0">
                  <div style="">
                    <select class="user_name form-control" style="" name="user_name"></select>
                  </div>
                </div>

                <div class="col-lg-3 col-md-3 pull-left pr-0 text-left">
                  <button type="submit" id="gj_dist_search" class="gj_dist_view" style="margin-top: 25px"><i class="fa fa-search gj_icon_srh"></i> SEARCH</button>    
                </div>
              </div>
          </div>
      </div>
    </div>

    <div class="rr">
      <div class="table_cnt">
        <table class="table table-bordered text-center table-responsive">
          <tr>
              <th>#</th>
              <th>PACKAGE TYPE</th>
              <th>PACKAGE NAME</th>
              <th>USED PINS</th>
              <th>UN USED PINS</th>
          </tr>
          <tbody class="tbdy">

          </tbody>
        </table>
      </div>
    </div>
    
  </div>
</div>
<form method="post" target="_blank" action="<?php echo base_url('index.php/pin/pin_store/get_pin_whole_details'); ?>">
  <input type="hidden" name="iid" class="iid">
  <input type="hidden" name="pack_id" class="pack_id">
  <input type="hidden" name="type" class="type">
  <input type="hidden" name="status" class="status">
  <input type="submit" style="opacity: 0" class="cus_but">
</form>


<script type="text/javascript">
  $('.user_id').select2({
    placeholder: 'Search With User Id',
    allowClear: true,
    ajax: {
      url: '<?php echo base_url("index.php/common_controller/get_all_user_id") ?>',
      dataType: 'json',
      delay: 1,
      data: function (params) {
            return { q: params.term 
            };
      },
      processResults: function (data) { return { results: data }; },
      cache: true
    },
  });
  $('.user_name').select2({
    placeholder: 'Search With User Name',
    allowClear: true,
    ajax: {
      url: '<?php echo base_url("index.php/common_controller/get_all_user_name") ?>',
      dataType: 'json',
      delay: 1,
      data: function (params) {
            return { rs:1,q: params.term 
            };
      },
      processResults: function (data) { return { results: data }; },
      cache: true
    },
  });
  $('#gj_dist_search').click(function(){
    var user_id = $('.user_id').val();
    var user_name = $('.user_name').val();
    if(user_id || user_name){
      if(user_id && !user_name){
        $.ajax({
          type : 'post',
          url  : "<?php echo base_url('index.php/pin/pin_store/get_pin_home'); ?>",
          data : {'id':user_id},
          success : function(data){
            if(data != 0){
              var res = JSON.parse(data);
              get_htm(res);
            }else{
              $('.tbdy').html('');
              $('.tbdy').append('<tr><td colspan="5">Pin List is Empty</td></tr>');
            }
          }
        })
      }else if(!user_id && user_name){
        $.ajax({
          type : 'post',
          url  : "<?php echo base_url('index.php/pin/pin_store/get_pin_home'); ?>",
          data : {'id':user_name},
          success : function(data){
            if(data != 0){
              var res = JSON.parse(data);
              get_htm(res);
            }else{
              $('.tbdy').html('');
              $('.tbdy').append('<tr><td colspan="5">Pin List is Empty</td></tr>');
            }
          }
        })
      }else{
        $.ajax({
          type : 'post',
          url  : "<?php echo base_url('index.php/pin/pin_store/get_pin_home'); ?>",
          data : {'id':user_id},
          success : function(data){
            if(data != 0){
              var res = JSON.parse(data); console.log(res);
              if(res['reg'].length > 0 || res['rep'].length > 0){
               get_htm(res);
              }else{
                $('.tbdy').html('');
                $('.tbdy').append('<tr><td colspan="5">Pin List is Empty</td></tr>');
              }
            }else{
              $('.tbdy').html('');
              $('.tbdy').append('<tr><td colspan="5">Pin List is Empty</td></tr>');
            }
          }
        })
      }
    }
  })

  function get_htm(res){
    $('.tbdy').html('');
    if(res['reg'].length > 0){
      for(i=0;i<res['reg'].length;i++){
        $('.tbdy').append('<tr><td>'+(i+1)+'</td><td>'+res['reg'][i]['for']+'</td><td>'+res['reg'][i]['package_name']+'</td><td  style="cursor:pointer;" onclick="get_used_pins_reg('+res['reg'][i]['id']+','+res['reg'][i]['used']+','+res['reg'][i]['package_id']+')">'+res['reg'][i]['used']+'</td><td  style="cursor:pointer;" onclick="get_unused_pins_reg('+res['reg'][i]['id']+','+res['reg'][i]['un_used']+','+res['reg'][i]['package_id']+')">'+res['reg'][i]['un_used']+'</td></tr>');
        if(i==res['reg'].length){
          if(res['rep'].length > 0){
            for(j=0;j<res['rep'].length;j++){
              $('.tbdy').append('<tr><td>'+(i+j+1)+'</td><td>'+res['rep'][j]['for']+'</td><td>'+res['rep'][j]['package_name']+'</td><td  style="cursor:pointer;" onclick="get_used_pins_rep('+res['rep'][j]['id']+','+res['rep'][j]['used']+','+res['rep'][i]['package_id']+')">'+res['rep'][j]['used']+'</td><td  style="cursor:pointer;" onclick="get_unused_pins_rep('+res['rep'][i]['id']+','+res['rep'][i]['un_used']+','+res['rep'][i]['package_id']+')">'+res['rep'][j]['un_used']+'</td></tr>');
            }
          }
        }
        
      }
    }else{
      if(res['rep'].length > 0){
        for(j=0;j<res['rep'].length;j++){
          $('.tbdy').append('<tr><td>'+(j+1)+'</td><td>'+res['rep'][j]['for']+'</td><td>'+res['rep'][j]['package_name']+'</td><td  style="cursor:pointer;" onclick="get_used_pins_rep('+res['rep'][j]['id']+','+res['rep'][j]['used']+','+res['rep'][i]['package_id']+')">'+res['rep'][j]['used']+'</td><td  style="cursor:pointer;" onclick="get_unused_pins_rep('+res['rep'][j]['id']+','+res['rep'][j]['un_used']+','+res['rep'][i]['package_id']+')">'+res['rep'][j]['un_used']+'</td></tr>');
        }
      }else{
        $('.tbdy').html('');
        $('.tbdy').append('<tr><td colspan="5">Pin List is Empty</td></tr>');
      }
    }
  }

  //registration -   TYPE = 0;
    //used  status = 0
    //unused  status = 1


  //repurchase   -  TYPE = 1;
    //used  status = 0
    //unused  status = 1

  function get_used_pins_reg(id,val,pack_id){
    if(val > 0){
      $('.iid').val(id);
      $('.pack_id').val(pack_id);
      $('.type').val(0);
      $('.status').val(0);
      $('.cus_but').click();
    }
  }
  function get_used_pins_rep(id,val,pack_id){
    if(val > 0){
      $('.iid').val(id);
      $('.pack_id').val(pack_id);
      $('.type').val(1);
      $('.status').val(0);
      $('.cus_but').click();
    }
  }
  function get_unused_pins_reg(id,val,pack_id){
    if(val > 0){
      $('.iid').val(id);
      $('.pack_id').val(pack_id);
      $('.type').val(0);
      $('.status').val(1);
      $('.cus_but').click();
    }
  }
  function get_unused_pins_rep(id,val,pack_id){
    if(val > 0){
      $('.iid').val(id);
      $('.pack_id').val(pack_id);
      $('.type').val(1);
      $('.status').val(1);
      $('.cus_but').click();
    }
  }
</script>


<style type="text/css">
  .rr{width: 100% !important;float: left;}
  .table_cnt table.table.table-bordered.text-center.table-responsive {
    width: 50%;
    margin: 0 auto;
  }
  .table_cnt table tr th{
    background: #2bb1e9;
    color: #fff;
  }
  .hole_body {
      margin: 3em 0em;
  }
  .s_box {
      background: #2bb1e9;
      text-align: center;
      height: 45px;
  }
  .s_box1 {
      background: #2bb1e9;
      text-align: center;
      height: 45px;
  }
  .s_box h3{
      padding-top: 9%;
      font-size: 19px !important;
      border: 1px solid #2bb1e9;
      background: #fff;
      color: #2bb1e9;
      padding-bottom: 8%;
  }
  .s_box1 h3{
    padding-top: 6%;
    font-size: 19px !important;
    border: 1px solid #2bb1e9;
    background: #fff;
    color: #2bb1e9;
    padding-bottom: 4%;
  }
  .s_box1.active h3,.s_box.active h3,.s_box1 h3:hover,.s_box h3:hover{
    background: #2bb1e9;
    color: #fff;
  }
</style>