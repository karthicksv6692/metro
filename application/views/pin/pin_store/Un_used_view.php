<div class="dkbody">
  <div class="container-fluid">
    <div class="page-header">
      <h3>Un Used Pin Lists</h3>
      <a href="<?php echo base_url('index.php/pin/pin_store/un_used'); ?>"><button type="button" class="btn btn_row1  btn-7 btn-7a icon-truck mb-5 pull-right" id="row1"> <span>BACK</span></button></a>
    </div>
    <div class="page-content">                   
        <div class="container-fluidasd">
          <h2 style="color: rgba(22, 169, 231, 0.91);">USER NAME :   <span style="color: #f5105e;"><?php echo $user; ?></span></h2>
          <table id="bank_request" class="data_table_style">
              <thead>
                 <tr role="row">
                    <th>#</th>
                    <th>Pin No</th>
                    <th>Package</th>
                    <th>Sub Package</th>
                    <th>Price</th>
                    <th>Created</th>
                </tr>
              </thead>
              <tbody>
                <?php $i=1; $tot = 0;foreach ($res as $rr) { ?>
                  <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $rr['pin_no']; ?></td>
                    <td><?php echo $rr['package_name']; ?></td>
                    <td><?php echo $rr['package_product_name']; ?></td>
                    <td><?php echo $rr['package_product_display_price']; ?></td>
                    <td><?php echo $rr['created_at']->format('Y-m-d H:i:s'); ?></td>
                  </tr>
                <?php $tot+=$rr['package_product_display_price'];} ?>
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="4" class="text-right">Total</td>
                  <td colspan="2"><?php echo $tot; ?></td>
                </tr>
              </tfoot>
            </table>
        </div>
         <?php //if($add != null){ ?>
         <div class="per_rw btn-click pr-5">
              <a href="<?php echo base_url('index.php/pin/pin_store/pin_store_viewx'); ?>"><button type="button" class="btn btn_row1  btn-7 btn-7a icon-truck mb-5" id="row1"> <span>BACK</span></button></a>
          </div>
        <?php //} ?>
      </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $('#bank_request').DataTable();
     
});
</script>
