<?php if(isset($pin_obj)){ ?>
  <?php if($pin_obj != null){

    $pin_no = $pin_obj->getPinNo();
    $status = "Not-Used";
    if($pin_obj->getPinStatus() == 0){
      $status = "Used";
    }
    $package_name = $pin_obj->getPinRequestPackageId()->getPackageName();
    $package_product_name = $pin_obj->getPinRequestPackageProductId()->getPackageProductName();
    $for = "Registration";
    if($pin_obj->getPinRequestFor() == 1){
      $for = "Repurchase";
    }

  }else{

  }?>
  <div class="dkbody">
      <div class="container-fluid">
        <div class="page-header">
          <h3>Pin Details</h3>
        </div>
      </div>
      <div class="bdy container">
        <table style="width: 100%" class="single_view_table ">
          <tr>
            <td class="single_table_view_head">Pin Number</td>
            <td class="single_table_view_head">:</td>
            <td class="single_table_view_data"><?php echo $pin_no; ?></td>
          </tr>
          <tr>
            <td class="single_table_view_head">Status</td>
            <td class="single_table_view_head">:</td>
            <td class="single_table_view_data"><?php echo $status; ?></td>
          </tr>
          <tr>
            <td class="single_table_view_head">Package Name</td>
            <td class="single_table_view_head">:</td>
            <td class="single_table_view_data"><?php echo $package_name; ?></td>
          </tr>
          <tr>
            <td class="single_table_view_head">Sub Package Name</td>
            <td class="single_table_view_head">:</td>
            <td class="single_table_view_data"><?php echo $package_product_name; ?></td>
          </tr>
          <tr>
            <td class="single_table_view_head">Pin Request For</td>
            <td class="single_table_view_head">:</td>
            <td class="single_table_view_data"><?php echo $for; ?></td>
          </tr>
          
          
        </table>
        <div class="country_width_100 col-12 mt-5">
            <div class="country_width_100 mt-3 mb-3">                     
                <div class="country-right">
                    <a href="<?php echo base_url('index.php/pin/pin_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button></a>
                </div>  
            </div>
        </div>
      </div>
  </div>

<?php }else{ ?>
  <div class="dkbody">
    <div class="container-fluid">
      <div class="page-header">
        <h3>Select</h3>
      </div>
      <?php echo $search; ?>
    </div>
  </div>
  <script type="text/javascript">
    $("#combobox").select2();
  </script>
<?php }  ?>