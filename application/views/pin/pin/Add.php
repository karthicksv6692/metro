<div class="dkbody">
	<div class="container-fluid">
		<div class="page-header">
			<h3>Pin Creation</h3>
		</div>
		<form method="post" acion="<?php echo base_url('index.php/pin/pin_crud/add'); ?>" id="new_pin_add">	
			<?php if($_SESSION['role_id'] == RT::$default_stock_pointer_id){ ?>
				<div class="row">
					<h4 style="text-align: center; width: 100%; color: #0f9bd6; font-size: 25px;">AVAILABLE BALANCE : <?php echo $avail_amount; ?></h4>
				</div>
			<?php } ?>
			<div class="row">	
				<div class="col-md-3 pull-left row">
          <div style="width:230px;margin-left: 10px;">
            <select class="itemName form-control" style="width:230px" name="itemName"></select>
          </div>
          <?php echo form_error('package_id','<span class="help-block form-error">', '</span>'); ?>  
        </div>

        <div class="col-md-3 pull-left row">
          <div style="width:230px;margin-left: 10px;">
            <select class="pck_prd form-control" style="width:230px" name="pck_prd"></select>
          </div>
          <?php echo form_error('pck_prd','<span class="help-block form-error">', '</span>'); ?>  
        </div>

        <input type="hidden" class="package_hid_id"  name="package_id">
        <input type="hidden" class="package_pack_price" name="">
        <input type="hidden" class="package_prd_hid_id" name="">
				<div class="col-md-4 pull-left">
					<p class="prd_prc_p">Price : <span class="prd_prc">0.00</span></p>
				</div>
			</div>
			<div class="row">	
				<div class="col-md-3 pull-left row">
					<span class="input input--nao">
						<input class="input__field input__field--nao nop" type="number" value="" name="nop">
						<label class="input__label input__label--nao" for="input-1">
							<span class="input__label-content input__label-content--nao">No.of Pins</span>
						</label>
						<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
							<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
						</svg>
					</span>	
				</div>
				<div class="col-md-4 pull-left">
					<p class="tot_prd_prc_p">Total Price : <span class="tot_prd_prc">0.00</span></p>
				</div>
			</div>
			<div class="row">	
				<div class="col-md-3 pull-left">
					<label for="exampleSelect1" class="role_select">Payment By
					    <select class="form-control slt_type" id="exampleSelect1" name="payment">
					    		<?php if($_SESSION['role_id'] == RT::$default_stock_pointer_id){ ?>
								<option value="2">Stockpoint Wallet</option>
								<?php }else{ ?>
								<option value="1">Cash</option>
								<?php } ?>
					    </select>
				   	</label>
				</div>
				<div class="col-md-3 pull-left">
					<label for="exampleSelect1" class="role_select">Request For
					    <select class="form-control slt_type" id="exampleSelect1" name="for">
								<option value="0">Registration</option>
								<option value="1">Repurchase</option>
					    </select>
				   	</label>
				</div>
			</div>
			<input type="hidden" name="pin_req" value="pin_req_id"> 
			<div class="country_width_100 col-12 mt-5">
				<div class="country_width_100">
					<div class="butt_sec_width mt-3 mb-3">
				      	<button type="submit" name="country_submit" class="country_button mr-2" >SUBMIT <i class="fa fa-paper-plane" aria-hidden="true"></i></button>		
					    <button type="reset" name="country_reset" class="country_button" onclick="clear_form()">RESET <i class="fa fa-refresh" aria-hidden="true"></i></button>
					</div>
		      	</div>
		      	
		        <div class="country_width_100 mt-3 mb-3">					      		  
				    <div class="country-right">
				      	<a href="<?php echo base_url('index.php/pin/pin_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button></a>
				    </div>	
				</div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">

      $('.pck_prd').select2();
      $('.itemName').select2({
        placeholder: 'Select an item',
        ajax: {
          url: '<?php echo base_url("index.php/common_controller/get_package_names_aj") ?>',
          dataType: 'json',
          delay: 1,
          data: function (params) {console.log(params);
                return { q: params.term // search term
                };
          },
          processResults: function (data) { return { results: data }; },
          cache: true
        },
      });
      $('.itemName').on("select2:selecting", function(e) { 
        // console.log(e);
         //var price = e['params']['args']['data']['price'];
         //$('.prd_prc').text(price+'.00');
         //$('.package_pack_price').val(price);
         $('.package_hid_id').val(e['params']['args']['data']['id']);
         
      }).on("select2:close", function(e) {
        var pck_id = $('.package_hid_id').val();
        if(pck_id){
          $('.pck_prd').select2({
            placeholder: 'Select an item',
            ajax: {
            url: '<?php echo base_url("index.php/common_controller/get_package_prd_names_aj") ?>',
            dataType: 'json',
            delay: 1,
            data: function (params) {console.log(params);
                  return { id:pck_id,q: params.term // search term
                  };
            },
            processResults: function (data) {return { results: data }; },
            cache: true
          },
          })
        }
      }).on("select2:opening", function(e) {
        $('.pck_prd').empty()
      })

      $('.pck_prd').on("select2:selecting", function(e) {
        var price = e['params']['args']['data']['price'];
        $('.prd_prc').text(price+'.00');
        $('.package_pack_price').val(price);
      })

      
      
</script>

<style type="text/css">
	p.prd_prc_p,p.tot_prd_prc_p {
	    padding-top: 3em;
	    padding-left: 2em;
	}
	label.role_select {
		width: 100%;
	    margin-top: 1em;
	    margin-left: 0em;
	    border-radius: 0 !important;
	    color: #696969;
	}
	select#exampleSelect1{
		margin-top: 0;
		padding-left: 0;
		border-radius: 0 !important;
	}
  .page-content section.content.bgcolor-1 form span.select2.select2-container.select2-container--default.select2-container--focus, span.select2.select2-container.select2-container--default.select2-container--below, span.select2.select2-container.select2-container--default {
      width: 230px !important;
      margin: 30px auto !important;
      text-align: left;
  }
  select.form-control:not([size]):not([multiple]) {
      height: 40px;
  }
</style>
<script type="text/javascript">
	$(function(){
		$('.package').autocomplete();
	})

 	$('.nop').keyup(function(){
 		var nop = $(this).val();
 		var price = $('.package_pack_price').val();
 		if(nop && price){
 			$('.tot_prd_prc').text(parseInt(price)*parseInt(nop)+'.00')
 		}else{
 			$('.tot_prd_prc').text('0.00')
 		}
 	})
 	function clear_form(){
 		$('.prd_prc').text('0.00');
 		$('.tot_prd_prc').text('0.00')
 	}
</script>

