<div class="dkbody">
  <div class="container-fluid">
    <div class="page-header">
      <h3>Un Used Pins</h3>
    </div>
    <div class="rr" style="overflow: hidden;margin-top: -40px;">
      <div class="col-lg-2 col-md-2 pull-left pr-0 pl-0">
        <div style="">
          <select class="user_id form-control" style="" name="user_id"></select>
        </div>
      </div>
      <div class="col-lg-2 col-md-2 pull-left pr-0 pl-0">
        <div style="">
          <select class="user_name form-control" style="" name="user_name"></select>
        </div>
      </div>
      <div class="col-md-2 pull-left form-group">
        <span class="input input--nao" id="sandbox-container">
          <input type="text" class="input__field input__field--nao form-control dtt1"  name="dob" value="" 
           data-validation="date" />
           <!-- data-validation-format="yyyy-mm-dd" -->
          <label class="input__label input__label--nao" for="input-1">
            <span class="input__label-content input__label-content--nao">From</span>
          </label>
          <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
          </svg>
        </span> 
      </div>
      <div class="col-md-1 pull-left pad_zero">
        <select class="slt_type pos1" id="exampleSelect1" name="" style="margin-top: 50px;width: 60%;">
            <option value="0">2 AM</option>
            <option value="1">2 PM</option>
            <option value="2">All</option>
        </select>
      </div>
      <div class="col-md-2 pull-left form-group pad_zero">
        <span class="input input--nao" id="sandbox-container">
          <input type="text" class="input__field input__field--nao form-control dtt2"  name="dob" value="" 
           data-validation="date" />
           <!-- data-validation-format="yyyy-mm-dd" -->
          <label class="input__label input__label--nao" for="input-1">
            <span class="input__label-content input__label-content--nao">To</span>
          </label>
          <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
          </svg>
        </span> 
      </div>
      <div class="col-md-1 pull-left pad_zero">
        <select class="slt_type pos2" id="exampleSelect1" name="" style="margin-top: 50px;width: 60%;">
          <option value="0">2 AM</option>
          <option value="1">2 PM</option>
        </select>
      </div>
      <div class="col-md-2 pull-left pad_zero">
          <select class="slt_type pack_type" id="exampleSelect1" name="" style="margin-top: 50px;">
              <option value="0">All</option>
              <option value="1">Registration</option>
              <option value="2">Repurchase</option>
          </select>
          <button type="button" class="btn btn_row1  btn-7 btn-7a icon-truck mb-5" id="date-search" style=" margin-top: -24px;  margin-bottom: 0px !important; margin-left: 4px; width: 50px;"> <i class="fa fa-search" aria-hidden="true" style="font-size: 25px;margin-left: -5px;"></i></button>
      </div>
    </div>

    <div class="page-content">                   
        <div class="container-fluid" style="margin-bottom: 2em">
        
        <table  id="gj_tree_struct_vw" class="data_table_style">
          <thead>
            <tr>
              <th class="text-center col-md-1">#</th>
              <th class="text-center">Generated Date</th>
              <th class="text-center" >Pin</th>
              <th class="text-center" >Package</th>
              <th class="text-center" >Sub.Pack</th>
              <th class="text-center" >UserId</th>
              <th class="text-center" >UserName</th>
              <th class="text-center" >Type</th>
              <th class="text-center" >Price</th>
            </tr>
          </thead>
          <tbody></tbody>
          <tfoot>
            <tr>
              <td colspan="8"><h4 style="float: right;">Total : </h4></td>
              <td><h4 class="tot_price">00.00</h4></td>
            </tr>
          </tfoot>
        </table>
        </div>
    </div>
  </div>
</div>
<style type="text/css">
  .pad_zero{
    padding: 0px !important;
  }
  .page-content section.content.bgcolor-1 form span.select2.select2-container.select2-container--default.select2-container--focus, span.select2.select2-container.select2-container--default.select2-container--below, span.select2.select2-container.select2-container--default {
      width: 160px !important;
      margin: 45px auto !important;
      text-align: left;
  }
</style>
<script type="text/javascript">
  $('.user_id').select2({
    placeholder: 'User Id',
    allowClear: true,
    ajax: {
      url: '<?php echo base_url("index.php/common_controller/get_all_user_id") ?>',
      dataType: 'json',
      delay: 1,
      data: function (params) {
            return { q: params.term 
            };
      },
      processResults: function (data) { return { results: data }; },
      cache: true
    },
  });
  $('.user_name').select2({
    placeholder: 'User Name',
    allowClear: true,
    ajax: {
      url: '<?php echo base_url("index.php/common_controller/get_all_user_name") ?>',
      dataType: 'json',
      delay: 1,
      data: function (params) {
            return { rs:1,q: params.term 
            };
      },
      processResults: function (data) { return { results: data }; },
      cache: true
    },
  });
  $(document).ready(function() {
    var tab = "";
    tab = $('#gj_tree_struct_vw').DataTable({});
    
    document.querySelector("#date-search").addEventListener('click',function(){
      dt1 = $('.dtt1').val();
      pos1 = $('.pos1').val();

      dt2 = $('.dtt2').val();
      pos2 = $('.pos2').val();
      pack_type = $('.pack_type').val();

      user_name = $('.user_name').val();
      user_id = $('.user_id').val();

      stat = $('.stat').val();
      if(dt1.length>0 ||user_name!=null || user_id != null ){
        tab.destroy();
        tab = $('#gj_tree_struct_vw').DataTable({
          "processing": true,
          "serverSide": true,
          "bInfo" : false,
          "ajax": {
            "url": '<?php echo base_url(); ?>index.php/pin/pin_crud/datatable_admin_unused_pin_view',
             "data": {'dt1':dt1,'pos1':pos1,'dt2':dt2,'pos2':pos2,'stat':stat,'user_name':user_name,'user_id':user_id,'pack_type':pack_type},
          },
        });
      }
      stat = $('.stat').val();
      $.ajax({
        type: "POST",
        url : '<?php echo base_url('index.php/pin/pin_crud/get_admin_unused_pin_total') ?>',
         "data": {'dt1':dt1,'pos1':pos1,'dt2':dt2,'pos2':pos2,'stat':stat,'user_name':user_name,'user_id':user_id,'pack_type':pack_type},
        success:  function (data) { console.log(data);
          if(data != 0){
            if(data != "" ){
              $('.tot_price').text(data);
            }else{ $('.tot_price').text('00.00'); }
          }else{$('.tot_price').text('00.00');}
        }
      })
    })
  });
</script>
