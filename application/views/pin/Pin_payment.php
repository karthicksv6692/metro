<div class="dkbody">
  <div class="container-fluid">
    <div class="page-header">
      <h3>Payout-Pin Wise</h3>
    </div>
    <div class="row">
      <div class="col-md-3 pull-left form-group">
        <span class="input input--nao" id="sandbox-container">
          <input type="text" class="input__field input__field--nao form-control dtt1"  name="dob" value="" 
           data-validation="date" />
          <label class="input__label input__label--nao" for="input-1">
            <span class="input__label-content input__label-content--nao">From Date</span>
          </label>
          <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
          </svg>
        </span> 
      </div>

      <div class="col-md-2 pull-left row">
            <select class="form-control slt_type pos1" id="exampleSelect1" name="">
                <option value="0">First Cutoff</option>
                <option value="1">Second Cutoff</option>
                <option value="2">All</option>
            </select>
            </label>
        </div>

      <div class="col-md-3 pull-left form-group">
        <span class="input input--nao" id="sandbox-container">
          <input type="text" class="input__field input__field--nao form-control dtt2"  name="dob" value="" 
           data-validation="date" />
          <label class="input__label input__label--nao" for="input-1">
            <span class="input__label-content input__label-content--nao">To Date</span>
          </label>
          <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
          </svg>
        </span> 
      </div>

      <div class="col-md-2 pull-left row">
            <select class="form-control slt_type pos2" id="exampleSelect1" name="">
                <option value="0">First Cutoff</option>
                <option value="1">Second Cutoff</option>
            </select>
            </label>
        </div>
  
      <button type="button" class="btn btn_row1  btn-7 btn-7a icon-truck mb-5" id="date-search"> <i class="fa fa-search" aria-hidden="true"></i></button>


    </div>
    <div class="page-content">                   
        <div class="container-fluid">
          <div class="loader"></div>
          <table  id="" class="table table-striped table-bordered table-responsive data_table_style" cellspacing="10" width="100%"> 
            <thead>
                
                <tr>
                  <th class="text-center">#</th>
                  <th class="text-center">PACKAGE NAME</th>
                  <th class="text-center">COUNT</th>
                  <th class="text-center">ACTION</th>
                  
                </tr>
            </thead>
            <tbody style="text-align: center;" class="tbdy">
            </tbody>
          </table>
        </div>

     
      </div>
  </div>
</div>

<form method="post" target="_blank" action="<?php echo base_url('index.php/pin/pin_crud/get_pin_details'); ?>">
  <input type="hidden" name="package_id" class="package_id">
  <input type="hidden" name="from" class="from">
  <input type="hidden" name="to" class="to">
  <input type="submit" class="sub_but" style="display: none">
</form>

<script type="text/javascript">

  $(document).ready(function() {

    $('.loader').css({'display':'none'});
    var tab = "";
   
    document.querySelector("#date-search").addEventListener('click',function(){
      $('.tbdy').html('');
      $('.loader').css({'display':'block'});
      dt1 = $('.dtt1').val();
      pos1 = $('.pos1').val();
      
      dt2 = $('.dtt2').val();
      pos2 = $('.pos2').val();
     
      $.ajax({
        type :  'POST',
        url  :  "<?php echo base_url('index.php/pin/pin_crud/get_total_pin_report'); ?>",
        data :  {'dt1':dt1,'dt2':dt2,'pos1':pos1,'pos2':pos2},
        success : function(data){
          if(data != 0){
            var res = JSON.parse(data);
            console.log(res);
            
            for(i=0;i<res.length;i++){
              $('.tbdy').append('<tr><td>'+(i+1)+'</td><td>'+res[i]['package_name']+'</td><td>'+res[i]['count']+'</td><td onclick="get_tot_pins('+res[i]['package_id']+',`'+res[i]['from']+'`,`'+res[i]['to']+'`)"><span><i class="fa fa-search" aria-hidden="true"></i></span></td></tr>');
            }
            $('.loader').css({'display':'none'});
          }else{
            $('.tbdy').html('');
            $('.tbdy').append('<tr><td colspan="6">Pin List Empty</td></tr>');
          }
        }
      })

    })
  });

  function get_tot_pins(id,fr,to){
    $('.package_id').val(id);
    $('.from').val(fr);
    $('.to').val(to);
    $('.sub_but').click();
  }
</script>
<style type="text/css">
  
 .loader {
    position: absolute;
    left: 29%;
    top: 90%;
}

.loader {
    border: 3px solid #f3f3f3;
    border-top: 3px solid #3498db;
    border-radius: 50%;
    width: 40px;
    height: 40px;
    animation: spin 1s linear infinite;
}

  @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
  }


  select.form-control:not([size]):not([multiple]) {
      height: calc(2.25rem + 30px);
  }
  button#date-search {
      height: 50px;
      width: 60px;
      margin-left: 20px;
  }
  i.fa.fa-search {
      font-size: 25px;
  }
  .data_table_style thead tr th{
    background-color: rgba(22, 169, 231, 0.91) !important;
    color: #fff;
    font-size: 14px;
    padding: 7px;
    border: 1px solid;
    min-width: 155px;
  }
  .box{
    width: 30%;
    float: right;
  }
  .f12{
    width: 100%;
  }
  .f6{
    width: 50%;
    float: left;
  }
  .hd{
    color: #088bc1 !important;
  }
  .rs{
        color: #ff4081;
  }
 .container-fluid{
    margin-bottom: 2em;
  }
</style>