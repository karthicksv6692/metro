<?php  //echo"<pre>";print_r($obj);exit(); ?>

<div class="dkbody">
  <div class="container-fluid">
    <div class="page-header gj_new_pack">
      <h3>User Module</h3>
    </div>
      <p class="gj_package_dets"><span><i class="fa fa-cubes" aria-hidden="true"></i></span> Personal Details
        <input type="button" value="Update" class="pesonal_but_x aj_but">
      </p>
      <div class="page-content gj_user_all">
        <section class="gj_user_sec">
          <form class="gj_form">
            <div class="xx" style="margin-top: 15px;margin-left: -15px;">
              <div class="gj_user_chk_div gj_verfid">
                <input type="checkbox" id="is_active" name="is_active" <?php if ($obj->getIsActive() == 1) {echo "checked";}?> value="1"/>
                <label for="is_active">Is Active</label>
              </div>
            </div>
            <div class="row rr">
              <div class="col-lg-10 pull-left pl-0 pr-0">
                <div class="col-lg-3 col-md-6 pull-left">
                  <span class="input input--nao">
                    <input class="input__field input__field--nao" type="text" id="product_add_product_quantity"  data-validation="required length" name="user_name" data-validation-length="min3" value="<?php echo $obj->getUserName() ?>" readonly/>
                    <label class="input__label input__label--nao" for="product_add_product_quantity">
                      <span class="input__label-content input__label-content--nao">User Name</span>
                    </label>
                    <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                      <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                    </svg>
                  </span>
                </div>
                <div class="col-lg-3 col-md-6 pull-left">
                  <span class="input input--nao">
                    <input class="input__field input__field--nao first_name" type="text" id="product_add_product_quantity" data-validation="required" name="first_name" value="<?php echo $obj->getFirstName(); ?>" />
                    <label class="input__label input__label--nao" for="product_add_product_quantity">
                      <span class="input__label-content input__label-content--nao">First Name</span>
                    </label>
                    <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                      <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                    </svg>
                  </span>
                  <p class="error first_name"></p>
                </div>
                <div class="col-lg-3 col-md-6 pull-left">
                  <span class="input input--nao">
                    <input class="input__field input__field--nao last_name" type="text" id="product_add_product_quantity"
                     name="last_name" value="<?php echo $obj->getLastName(); ?>"/>
                    <label class="input__label input__label--nao" for="product_add_product_quantity">
                      <span class="input__label-content input__label-content--nao">Last Name</span>
                    </label>
                    <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                      <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                    </svg>
                  </span>
                </div>
                <div class="col-lg-3 col-md-6 pull-left">
                  <span class="input input--nao">
                    <input class="input__field input__field--nao user_email" type="email" id="product_add_product_quantity"
                     name="user_email" value="<?php echo $obj->getUserEmail(); ?>"/>
                    <label class="input__label input__label--nao" for="product_add_product_quantity">
                      <span class="input__label-content input__label-content--nao">E-Mail ID</span>
                    </label>
                    <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                      <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                    </svg>
                  </span>
                </div>
              </div>
              <div class="col-lg-2 gj_user_up_div pull-left">
                <p class="gj_pack_title">upload Image</p>
                <div class=" pkfile gj_file gj_userfile">
                  <?php if($obj->getProfilePictureUrl() != null && !is_numeric($obj->getProfilePictureUrl()) ){ ?>
                        <div class="p1-0 mr-2 mb-2 bdrstyle_hover" style="float:left">
                          <img src="<?php echo base_url(); ?><?php echo $obj->getProfilePictureUrl(); ?>" alt="image" class="width-60px bdrstyle" style="z-index: 1">
                        </div>
                        
                      <?php } ?>
                  <div class=" toppkfile" style="float:left">
                    <input type="file"  name="files" id="files" class="form-control img-responsive" style="z-index: 2">
                    <div class="width-60px width-6px">
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <input type="hidden" class="edit_user_id" name="edit_user_id" value="<?php echo $obj->getUserId(); ?>">
          

            <div class="row rr">
              <div class="col-lg-2 col-md-6 pull-left">
                <span class="input input--nao">
                  <input class="input__field input__field--nao dob" type="date"  id="gj_user_dob" name="dob" value="<?php echo $obj->getDob(); ?>"/>
                  <label class="input__label input__label--nao" for="gj_user_dob">
                    <span class="input__label-content input__label-content--nao">D.O.B</span>
                  </label>
                  <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                  </svg>
                </span>
              </div>
              <div class="col-lg-3 col-md-6 pull-left">
                <span class="input input--nao">
                  <input class="input__field input__field--nao primary_mobile" type="number" id="product_add_product_quantity"
                   name="primary_mobile" value="<?php echo $obj->getPrimaryMobile(); ?>"/>
                  <label class="input__label input__label--nao" for="product_add_product_quantity">
                    <span class="input__label-content input__label-content--nao">Mobile Number</span>
                  </label>
                  <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                  </svg>
                </span>
              </div>
              <div class="col-lg-3 col-md-6 pull-left">
                <span class="input input--nao">
                  <input class="input__field input__field--nao secondary_mobile" type="number" id="product_add_product_quantity"
                   name="secondary_mobile" value="<?php echo $obj->getSecondaryMobile(); ?>"/>
                  <label class="input__label input__label--nao" for="product_add_product_quantity">
                    <span class="input__label-content input__label-content--nao">Alter Mobile Number</span>
                  </label>
                  <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                  </svg>
                </span>
              </div>
              <div class="col-lg-4 col-md-6 pull-left">
                <div class="gj_gender_det">
                  <span>Gender : </span>
                  <input type="radio" id="test1"  name="gender" value="1" <?php if ($obj->getGender() == 1) {echo "Checked";}?>>
                  <label for="test1">Male</label>
                  <input type="radio" id="test2"  name="gender" value="0"  <?php if ($obj->getGender() == 0) {echo "Checked";}?>>
                  <label for="test2">Female</label>
                  <input type="radio" id="test3"  name="gender" value="2"  <?php if ($obj->getGender() == 2) {echo "Checked";}?>>
                  <label for="test3">Others</label>
                </div>
              </div>
            </div>

          </form>

            <div class="row rr">
              <div class="col-lg-12">
                <p class="gj_package_dets gj_details_hd"><span><i class="fa fa-dropbox" aria-hidden="true"></i></span> Identity <input type="button" value="Update" class="identity_but_x aj_but"></p>
            <form class="gj_identity">
              <input type="hidden" class="edit_user_id" name="ed_user_id" value="<?php echo $obj->getUserId(); ?>">
              </div>
                <div class="col-lg-4 col-md-6 pull-left">
                  <span class="input input--nao">
                    <input class="input__field input__field--nao aadhar_no" type="number"  id="gj_user_dob" name="aadhar_no" value="<?php echo $obj->getAadharNo(); ?>" />
                    <label class="input__label input__label--nao" for="gj_user_dob">
                      <span class="input__label-content input__label-content--nao">Identity Number</span>
                    </label>
                    <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                      <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                    </svg>
                  </span>
                </div>
                <div class="col-lg-4 col-md-6 pull-left">
                  <span class="input input--nao">
                    <input class="input__field input__field--nao pan_no" type="text" id="product_add_product_quantity"
                     name="pan_no" value="<?php echo $obj->getPanNo(); ?>" />
                    <label class="input__label input__label--nao" for="product_add_product_quantity">
                      <span class="input__label-content input__label-content--nao">PanCard Number</span>
                    </label>
                    <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                      <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                    </svg>
                  </span>
                </div>
                <div class="col-lg-4 col-md-6 pull-left">
                </div>
              </div>
              <div class="row rr">
                <div class="col-lg-4 col-md-6 pull-left">

                  <?php if($obj->getPanImageUrl() != null && !is_numeric($obj->getPanImageUrl()) ){ ?>
                    <div class="ifile gj_file gj_filess gj_user_file">
                      <div class="gj_absolute p1-0 mr-2 mb-2 bdrstyle_hover" style="float:left">
                        <img class="width-60px bdrstyle" src="<?php echo base_url(); ?><?php echo $obj->getPanImageUrl(); ?>"><span>X</span></div>
                        <div class="toppkfile gj_relative input--filled" style="float:left">
                          <input type="file" name="gj_file1" accept="image/jpeg" id="gj_file11" class="form-control gj_finput img-responsive gj_finput" title="Select Pan Card">
                          <div class="width-60px width-6px">
                            <span class="gj_file_cmd">Attach Pan Card Image</span>
                          </div>
                      </div>
                    </div>
                  <?php }else{ ?>
                    <div class="ifile gj_file gj_filess gj_user_file">
                      <div class="toppkfile gj_relative" style="float:left">
                        <input type="file"  name="gj_file1" accept="image/jpeg" id="gj_file11" class="form-control gj_finput img-responsive gj_finput" title="Select Pan Card">
                        <div class="width-60px width-6px">
                          <span class="gj_file_cmd">Attach Pan Card Image</span>
                        </div>
                      </div>
                    </div>
                  <?php } ?>
                  <div class="row gj_rr">
                    <div class="gj_user_chk_div gj_verfid">
                      <input type="checkbox" id="pan_verf" name="pan_verified" <?php if ($obj->getPanVerified() == 1) {echo "checked";}?> value="1"/>
                      <label for="pan_verf">Pan Verified</label>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 col-md-6 pull-left">
                  <?php if($obj->getAadharImageUrl() != null && !is_numeric($obj->getAadharImageUrl()) ){ ?>
                    <div class="yfile gj_file gj_filess gj_user_file">
                      <div class="gj_absolute p1-0 mr-2 mb-2 bdrstyle_hover" style="float:left">
                        <img class="width-60px bdrstyle" src="<?php echo base_url(); ?><?php echo $obj->getAadharImageUrl(); ?>"><span>X</span></div>
                        <div class="toppkfile gj_relative" style="float:left">
                          <input type="file"  name="gj_file4" id="gj_file44" class="form-control gj_finput img-responsive" title="Select Bank Passbook">
                          <div class="width-60px width-6px">
                            <span class="gj_file_cmd">Attach ID Card Image</span>
                          </div>
                        </div>
                    </div>
                  <?php }else{ ?>
                    <div class="yfile gj_file gj_filess gj_user_file">
                      <div class="toppkfile gj_relative" style="float:left">
                        <input type="file"  name="gj_file4" id="gj_file44" class="form-control gj_finput img-responsive" title="Select Bank Passbook">
                        <div class="width-60px width-6px">
                          <span class="gj_file_cmd">Attach ID Card Image</span>
                        </div>
                      </div>
                    </div>
                  <?php } ?>
                  <div class="row gj_rr">
                    <div class="gj_user_chk_div gj_verfid">
                      <input type="checkbox" name="id_verified" id="id_verf" <?php if ($obj->getAadharVerified() == 1) {echo "checked";}?> value="1"/>
                      <label for="id_verf">Id Verified</label>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 col-md-6 pull-left">
                  <?php if($obj->getPassImageUrl() != null && !is_numeric($obj->getPassImageUrl()) ){ ?>
                    <div class="xfile gj_file gj_filess gj_user_file">
                      <div class="gj_absolute p1-0 mr-2 mb-2 bdrstyle_hover" style="float:left">
                        <img class="width-60px bdrstyle" src="<?php echo base_url(); ?><?php echo $obj->getPassImageUrl(); ?>"><span>X</span></div>
                        <div class="toppkfile gj_relative" style="float:left">
                          <input type="file"  name="gj_file5" id="gj_file55" class="form-control gj_finput img-responsive" title="Select Bank Passbook">
                          <div class="width-60px width-6px">
                            <span class="gj_file_cmd">Attach Bank Passbook Image</span>
                          </div>
                        </div>
                    </div>
                  <?php }else{ ?>
                    <div class="xfile gj_file gj_filess gj_user_file">
                      <div class="toppkfile gj_relative" style="float:left">
                        <input type="file"  name="gj_file5" id="gj_file55" class="form-control gj_finput img-responsive" title="Select Bank Passbook">
                        <div class="width-60px width-6px">
                          <span class="gj_file_cmd">Attach Bank Passbook Image</span>
                        </div>
                      </div>
                    </div>
                  <?php } ?>
                  <div class="row gj_rr">
                    <div class="gj_user_chk_div gj_verfid">
                      <input type="checkbox" name="pass_verified" id="bank_verf" <?php if ($obj->getPassVerified() == 1) {echo "checked";}?> value="1"/>
                      <label for="bank_verf">PassBook Verified</label>
                    </div>
                  </div>
                </div>
                <!-- <div class="row gj_rr">
                  <div class="gj_user_chk_div gj_verfid">
                    <input type="checkbox" id="kyc_verified" name="kyc_status" <?php if ($obj->getKycStatus() == 1) {echo "checked";}?> value="1"/>
                    <label for="kyc_verified">KYC Verified</label>
                  </div>
                </div> -->
              </div>
            </form>
            <div class="row rr">
              <div class="col-lg-12">
                <p class="gj_package_dets gj_details_hd"><span><i class="fa fa-cubes" aria-hidden="true"></i></span> Bank Details <input type="button" value="Update" class="bank_but aj_but"></p>
              </div>
              <div class="col-lg-3 col-md-6 pull-left">
                <span class="input input--nao">
                  <input class="input__field input__field--nao account_no" type="text" id="product_add_product_quantity"
                   name="account_no" value="<?php echo $obj->getAccountNo(); ?>"/>
                  <label class="input__label input__label--nao" for="product_add_product_quantity">
                    <span class="input__label-content input__label-content--nao">Account Number</span>
                  </label>
                  <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                  </svg>
                </span>
              </div>
              <div class="col-lg-3 col-md-6 pull-left">
                <span class="input input--nao">
                  <input class="input__field input__field--nao bank_ifsc" type="text"  id="gj_user_dob" name="bank_ifsc" value="<?php echo $obj->getBankIfsc(); ?>" />
                  <label class="input__label input__label--nao" for="gj_user_dob">
                    <span class="input__label-content input__label-content--nao">IFSC</span>
                  </label>
                  <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                  </svg>
                </span>
                <input type="hidden" class="bank_ifsc_hid" value="0">
              </div>
              <div class="col-lg-3 col-md-6 pull-left">
                <span class="input input--nao">
                  <input class="input__field input__field--nao bank_name" type="text" id="product_add_product_quantity"
                   name="bank_name" value="<?php echo $obj->getBankName(); ?>"/>
                  <label class="input__label input__label--nao" for="product_add_product_quantity">
                    <span class="input__label-content input__label-content--nao">Bank Name</span>
                  </label>
                  <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                  </svg>
                </span>
              </div>
              <div class="col-lg-3 col-md-6 pull-left">
                <span class="input input--nao">
                  <input class="input__field input__field--nao bank_branch" type="text" id="product_add_product_quantity"
                   name="bank_branch" value="<?php echo $obj->getBankBranch(); ?>"/>
                  <label class="input__label input__label--nao" for="product_add_product_quantity">
                    <span class="input__label-content input__label-content--nao">Branch</span>
                  </label>
                  <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                  </svg>
                </span>
              </div>
            </div>

            <div class="row rr">
              <div class="col-lg-12">
                <p class="gj_package_dets gj_details_hd"><span><i class="fa fa-dropbox" aria-hidden="true"></i></span> Communication Details <input type="button" value="Update" class="communication_but aj_but"></p>
              </div>
              <div class="col-lg-4 col-md-12 pull-left">
                <span class="input input--nao gj_desc_wid">
                  <textarea class="input__field input__field--nao full_address" rows="3" id="product_add_product_package_short_description" name="full_address"><?php echo $obj->getFullAddress(); ?></textarea>
                  <label class="input__label input__label--nao" for="product_add_product_package_short_description">
                    <span class="input__label-content input__label-content--nao">Address</span>
                  </label>
                  <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                  </svg>
                </span>
              </div>
              <div class="col-lg-8 col-md-12 pull-left gj_user_marg">
                <div class="col-lg-3 col-md-6 pull-left">
                  <span class="input input--nao">
                      <input class="input__field input__field--nao pincode_id_hid_no pincode_no" type="number" id="tags" name="pincode_no"  value="<?php echo $obj->getPincode(); ?>">
                      <label class="input__label input__label--nao" for="r_password">
                        <span class="input__label-content input__label-content--nao"> Pincode </span>
                      </label>
                      <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                      <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></path></svg>
                  </span>
                  <input type="hidden" name="pincode_id_hid" class="pincode_id_hid">
                </div>
                <div class="col-lg-3 col-md-6 pull-left">
                  <span class="input input--nao">
                    <input class="input__field input__field--nao district" type="text" id="input-1" name="district" value="<?php echo $obj->getDistrict(); ?>"/>
                    <label class="input__label input__label--nao " for="input-1">
                      <span class="input__label-content input__label-content--nao">District </span>
                    </label>
                    <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                      <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                    </svg>
                  </span>
                </div>
                <div class="col-lg-3 col-md-6 pull-left">
                  <span class="input input--nao">
                    <input class="input__field input__field--nao state" type="text" id="input-1" name="state" value="<?php echo $obj->getState(); ?>"
                        data-validation-length="2-100"
                        data-validation-regexp="^[A-Za-z][A-Za-z0-9-_\ .&amp;]+$"
                        data-validation-error-msg-custom="please provide valid state"/>
                    <label class="input__label input__label--nao" for="input-1">
                      <span class="input__label-content input__label-content--nao">State </span>
                    </label>
                    <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                      <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                    </svg>
                  </span>
                </div>
                <div class="col-lg-3 col-md-6 pull-left">
                  <select class="js-example-basic-single select2_style ad_usr country" name="country_id" id="combobox">
                      <?php if ($obj->getCountryId() != 0) {?>
                          <option value="<?php echo $obj->getCountryId(); ?>"><?php echo $obj->getCountryName(); ?></option>
                      <?php }?>
                  </select>
                <input type="hidden" name="user_page" value="asdf">
                </div>
              </div>
            </div>

            <div class="row rr">
              <div class="col-lg-12">
                <p class="gj_package_dets gj_details_hd"><span><i class="fa fa-cubes" aria-hidden="true"></i></span> Nominee Details <input type="button" value="Update" class="nominee_but aj_but"></p>
              </div>
              <div class="col-lg-2 col-md-6 pull-left gj_user_nominee">
                <span class="input input--nao">
                  <input class="input__field input__field--nao nominee_name" type="text" id="product_add_product_quantity"
                   name="nominee_name" value="<?php echo $obj->getNName(); ?>" />
                  <label class="input__label input__label--nao" for="product_add_product_quantity">
                    <span class="input__label-content input__label-content--nao">Nominee Name</span>
                  </label>
                  <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                  </svg>
                </span>
              </div>
              <div class="col-lg-2 col-md-6 pull-left">
                  <span class="input input--nao gj_user_mobno">
                    <input class="input__field input__field--nao n_mobile" type="number" id="product_add_product_quantity"
                     name="n_mobile" value="<?php echo $obj->getNMobile(); ?>"/>
                    <label class="input__label input__label--nao" for="product_add_product_quantity">
                      <span class="input__label-content input__label-content--nao">Mobile Number</span>
                    </label>
                    <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                      <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                    </svg>
                  </span>
            </div>
              <div class="col-lg-2 col-md-6 pull-left">
                  <span class="input input--nao">
                    <?php $relationship = $obj->getNRelation(); ?>
                    <select id="gj_sel2_relation " class="input__field--nao gj_user_pin relation" name="relation">
                      <option value="Father" <?php if($relationship == "Father"){echo"selected='selected'";} ?>>Father</option>
                    <option value="Mother" <?php if($relationship == "Mother"){echo"selected='selected'";} ?>>Mother</option>
                    <option value="Son" <?php if($relationship == "Son"){echo"selected='selected'";} ?>>Son</option>
                    <option value="Daughter" <?php if($relationship == "Daughter"){echo"selected='selected'";} ?>>Daughter</option>
                    <option value="Wife" <?php if($relationship == "Wife"){echo"selected='selected'";} ?>>Wife</option>
                    <option value="Wife" <?php if($relationship == "Husband"){echo"selected='selected'";} ?>>Husband</option>
                    <option value="Brother" <?php if($relationship == "Brother"){echo"selected='selected'";} ?>>Brother</option>
                    <option value="Sister" <?php if($relationship == "Sister"){echo"selected='selected'";} ?>>Sister</option>
                  </select>
                    </select>
                  </span>
              </div>
              <div class="col-lg-2 col-md-6 pull-left">
                  <span class="input input--nao">
                    <input class="input__field input__field--nao n_dob" type="date" id="product_add_product_quantity"
                     name="n_dob" value="<?php echo $obj->getNDob(); ?>"/>
                    <label class="input__label input__label--nao" for="product_add_product_quantity">
                      <span class="input__label-content input__label-content--nao">D.O.B</span>
                    </label>
                    <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                      <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                    </svg>
                  </span>
              </div>
              <div class="col-lg-4 col-md-6 pull-left">
                  <div class="gj_genr_dets">
                    <span>Gender : </span>
                    <input type="radio" id="male" name="n_gender" value="1" <?php if ($obj->getNGender() == 1) {echo "Checked";}?>>
                    <label for="male">Male</label>
                    <input type="radio" id="female" name="n_gender" value="0" <?php if ($obj->getNGender() == 0) {echo "Checked";}?>>
                    <label for="female">Female</label>
                    <input type="radio" id="others" name="n_gender" value="2" <?php if ($obj->getNGender() == 2) {echo "Checked";}?>>
                    <label for="others">Others</label>
                  </div>
              </div>
            </div>

            <div class="row rr">
              <div class="col-lg-12">
                <p class="gj_package_dets gj_details_hd"><span><i class="fa fa-dropbox" aria-hidden="true"></i></span> Password Details
                  <input type="button" value="Update" class="pass_but aj_but">
                </p>
              </div>
              <div class="col-lg-4 col-md-6 pull-left">
                <span class="input input--nao">
                  <input class="input__field input__field--nao pwd" type="text" id="product_add_product_quantity" data-validation="required" name="pwd" value="<?php echo $obj->getPassword(); ?>" />
                  <label class="input__label input__label--nao" for="product_add_product_quantity">
                    <span class="input__label-content input__label-content--nao">Password</span>
                  </label>
                  <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                  </svg>
                </span>

              </div>
              <div class="col-lg-4 col-md-6 pull-left">
                <!-- <p class="gj_pin_store_hd">Pin Store Password</p> -->
                <span class="input input--nao">
                  <input class="input__field input__field--nao pin_pwd" type="text" id="product_add_product_quantity" data-validation="required" name="pin_pwd" value="<?php echo $obj->getPinPassword(); ?>"/>
                  <label class="input__label input__label--nao" for="product_add_product_quantity">
                    <span class="input__label-content input__label-content--nao">Pin Store Password</span>
                  </label>
                  <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                  </svg>
                </span>
              </div>
              <div class="col-lg-4 col-md-6 pull-left">
              </div>
            </div>

            

            <div class="country_width_100 col-12 mt-5">
              <div class="country_width_100">
                <div class="butt_sec_width mt-3 mb-3">
                  <!-- <button type="submit" name="moduleadd_submit" class="country_button mr-2" >SUBMIT <i class="fa fa-paper-plane" aria-hidden="true"></i></button> -->
                    <button type="reset" name="moduleadd_reset" class="country_button">RESET <i class="fa fa-refresh" aria-hidden="true"></i></button>

                </div>
              </div>

              <div class="country_width_100 mt-3 mb-3">
                <div class="country-right">
                  <a href="<?php echo base_url('index.php/user/user_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button></a>
                </div>
              </div>
            </div>

          </form>
        </section>
      </div>
  </div>
</div>


<style type="text/css">
  .pkfile.gj_file.gj_userfile img.width-60px.bdrstyle {
      position: absolute;
      z-index: 99;
      left: 8px;
  }
  .pkfile.gj_file.gj_userfile span {
      right: -95px;
      z-index: 99;
  }
  input::-webkit-calendar-picker-indicator,
  input::-webkit-inner-spin-button {
      display: none;
  }
  /* gender radio button style */
  .gj_gender_det {
      margin-top: 38px;
  }
  .gj_gender_det span {
      font-weight: bold;
      color: dimgrey;
      vertical-align: text-bottom;
  }
  .gj_gender_det [type="radio"]:checked,
  .gj_gender_det [type="radio"]:not(:checked) {
      position: absolute;
      left: -9999px;
  }
  .gj_gender_det [type="radio"]:checked + label,
  .gj_gender_det [type="radio"]:not(:checked) + label
  {
      position: relative;
      padding-left: 23px;
      cursor: pointer;
      margin-right: 5px;
      line-height: 20px;
      display: inline-block;
      color: #666;
  }
  .gj_gender_det [type="radio"]:checked + label:before,
  .gj_gender_det [type="radio"]:not(:checked) + label:before {
      content: '';
      position: absolute;
      left: 0;
      top: 0;
      width: 18px;
      height: 18px;
      border: 1px solid #ddd;
      border-radius: 100%;
      background: #fff;
  }
  .gj_gender_det [type="radio"]:checked + label:after,
  .gj_gender_det [type="radio"]:not(:checked) + label:after {
      content: '';
      width: 12px;
      height: 12px;
      background: #18a9e7;
      position: absolute;
      top: 3px;
      left: 3px;
      border-radius: 100%;
      -webkit-transition: all 0.2s ease;
      transition: all 0.2s ease;
  }
  .gj_gender_det [type="radio"]:not(:checked) + label:after {
      opacity: 0;
      -webkit-transform: scale(0);
      transform: scale(0);
  }
  .gj_gender_det [type="radio"]:checked + label:after {
      opacity: 1;
      -webkit-transform: scale(1);
      transform: scale(1);
  }
  /* gender radio button style */
  p.gj_package_dets.gj_details_hd {
      width: 100%;
      float: left;
  }
  .gj_user_all .gj_package_dets>span {
      margin-right: 5px;
  }
  .gj_pan1file {
      width: 100px;
      opacity: 0;
      height: 100px;
      position: absolute;
      z-index: 99;
      left: 42px;
  }
  .gj_f_div {
      margin: auto;
      width: 100px;
      height: 100px;
      padding: 2px;
      border: 1px solid #d7d7d7;
      box-shadow: 4px 1px 2px 0px #c1c1c1;
      position: relative;
  }
  .gj_f_div:before {
      content: "+";
      font-size: 60px;
      color: #efefef;
      line-height: 115px;
      padding-left: 33%;
  }
  .gj_file_img {
      color: #787878;
      position: absolute;
      top: 0;
      left: 0;
      width: 100px;
      height: 100px;
  }
  .gj_user_file span.gj_file_cmd {
      position: absolute;
      top: 50%;
      margin-top: -33px;
      left: 20px;
      text-align: center;
      font-weight: bold;
      color: #787878;
      padding: 0px 3px;
  }
  span.gj_file_close {
      position: absolute;
      z-index: 99;
      top: -7px;
      left: -5px;
      background-color: #16a9e7;
      color: #fff;
      border-radius: 50%;
      width: 20px;
      height: 20px;
      padding: 2px 4px;
      cursor: pointer;
  }
  p.gj_need{
    color: #2bb0e9;
    font-weight: bold;
  }
  .gj_user_file .gj_finput {
      width: 250px !important;
      height: 175px !important;
      opacity: 0;
      position: absolute;
      z-index: 9;
  }
  .gj_relative{
      position: relative;
  }
  .gj_absolute{
      position: absolute;
      z-index: 99;
  }
  .gj_relative .width-60px {
      width: 250px;
      height: 175px;
      box-shadow: 1px 1px 2px 0px #c1c1c1;
  }
  .pkfile.gj_file.gj_userfile img.width-60px.bdrstyle {
      width: 80px;
      height: 80px;
  }
  .gj_user_file img.width-60px.bdrstyle {
      width: 250px;
      height: 175px;
  }
  .gj_filess .width-6px:before {
      content: "+";
      font-size: 60px;
      color: #e6e4e4;
      padding-left: 33%;
      line-height: 110px;
  }
  .gj_rr {
      margin-left: 0px !important;
      margin-right: 0px !important;
  }
  /* custom check box */
  .gj_user_chk_div [type="checkbox"]:not(:checked),
  .gj_user_chk_div [type="checkbox"]:checked {
    display: none;
  }
  .gj_user_chk_div [type="checkbox"]:not(:checked) + label,
  .gj_user_chk_div [type="checkbox"]:checked + label {
    position: relative;
        padding-left: 1.95em;
      cursor: pointer;
      font-weight: bold;
    color: #787878;
  }
  .gj_user_chk_div [type="checkbox"]:checked + label {
    color: #2bb0e9;
  }

  /* checkbox aspect */
  .gj_user_chk_div [type="checkbox"]:not(:checked) + label:before,
  .gj_user_chk_div [type="checkbox"]:checked + label:before {
      content: '';
    position: absolute;
      left: 0; top: 0;
      width: 1.25em; height: 1.25em;
      border: 2px solid #2bb0e9;
      background: #fff;
  }
  /* checked mark aspect */
  .gj_user_chk_div [type="checkbox"]:not(:checked) + label:after,
  .gj_user_chk_div [type="checkbox"]:checked + label:after {
    content: '✔';
      position: absolute;
      top: 2.5px;
      left: 3px;
      font-size: 18px;
      line-height: 0.8;
      color: #2bb0e9;
      transition: all .2s;
  }
  /* checked mark aspect changes */
  .gj_user_chk_div [type="checkbox"]:not(:checked) + label:after {
      opacity: 0;
      transform: scale(0);
  }
  .gj_user_chk_div [type="checkbox"]:checked + label:after {
      opacity: 1;
      transform: scale(1);
  }
  /* custom check box */
  .gj_user_file {
      width: 100%;
      overflow: hidden;
      margin-bottom: 15px;
      padding: 0px 15px;
  }
  .gj_user_chk_div.gj_verfid {
      padding: 0px 15px;
      margin-bottom: 15px;
  }
  .gj_user_pin+.select2-container {
      width: 100% !important;
      margin-top: 13px;
  }
  .gj_user_pin+.select2-container span.select2-selection {
      border-radius: 0px;
      border: 0px;
      border-bottom: 1px solid #b9bdc1;
  }
  .gj_user_pin+.select2-container span.select2-selection .select2-selection__rendered {
      padding-left: 0px !important;
  }
  .gj_user_pin {
      outline: 0 !important;
  }
  .gj_user_marg {
      margin-top: 45px;
  }
  .gj_user_mobno {
      margin: 16px 0px;
  }
  /* gender radio button style */
  .gj_genr_dets {
      margin-top: 40px;
  }
  .gj_genr_dets span {
      font-weight: bold;
      color: dimgrey;
      vertical-align: text-bottom;
  }
  .gj_genr_dets [type="radio"]:checked,
  .gj_genr_dets [type="radio"]:not(:checked) {
      position: absolute;
      left: -9999px;
  }
  .gj_genr_dets [type="radio"]:checked + label,
  .gj_genr_dets [type="radio"]:not(:checked) + label
  {
      position: relative;
      padding-left: 23px;
      cursor: pointer;
      margin-right: 5px;
      line-height: 20px;
      display: inline-block;
      color: #666;
  }
  .gj_genr_dets [type="radio"]:checked + label:before,
  .gj_genr_dets [type="radio"]:not(:checked) + label:before {
      content: '';
      position: absolute;
      left: 0;
      top: 0;
      width: 18px;
      height: 18px;
      border: 1px solid #ddd;
      border-radius: 100%;
      background: #fff;
  }
  .gj_genr_dets [type="radio"]:checked + label:after,
  .gj_genr_dets [type="radio"]:not(:checked) + label:after {
      content: '';
      width: 12px;
      height: 12px;
      background: #18a9e7;
      position: absolute;
      top: 3px;
      left: 3px;
      border-radius: 100%;
      -webkit-transition: all 0.2s ease;
      transition: all 0.2s ease;
  }
  .gj_genr_dets [type="radio"]:not(:checked) + label:after {
      opacity: 0;
      -webkit-transform: scale(0);
      transform: scale(0);
  }
  .gj_genr_dets [type="radio"]:checked + label:after {
      opacity: 1;
      -webkit-transform: scale(1);
      transform: scale(1);
  }
  /* gender radio button style */
  p.gj_pin_store_hd {
      margin-bottom: -24px;
      font-weight: bold;
  }
  p.gj_pack_title {
      font-weight: bold;
      color: #787878;
  }

  /* Responsive */
  @media (min-width: 992px) and (max-width: 1199px) {
    p.gj_pack_title {
        font-size: 13px;
    }
    .gj_user_file .gj_finput {
        width: 190px !important;
        height: 150px !important;
    }
    .gj_relative .width-60px {
        width: 190px !important;
        height: 150px !important;
    }
    .gj_user_file img.width-60px.bdrstyle {
        width: 190px;
        height: 150px;
    }
    .pkfile.gj_file.gj_userfile .toppkfile input#files {
        padding: 0;
    }
    .gj_gender_det {
        margin-top: 25px;
    }
    .gj_genr_dets {
        width: 100%;
    }
  }

  @media (min-width: 768px) and (max-width: 991px) {
    .gj_user_up_div {
        padding: 0px 30px !important;
    }
    .gj_user_file .gj_finput {
        width: 190px !important;
        height: 150px !important;
    }
    .gj_relative .width-60px {
        width: 190px !important;
        height: 150px !important;
    }
    .gj_user_file img.width-60px.bdrstyle {
        width: 190px;
        height: 150px;
    }
    .gj_user_marg {
        margin-top: 0px;
    }
    section.gj_user_sec span.input.input--nao.gj_desc_wid {
        max-width: 100%;
    }
    .gj_user_pin+.select2-container {
        margin-bottom: 3px;
    }
    .gj_user_nominee span.input.input--nao {
        margin: 0px 30px 0px 15px;
        max-width: 100%;
    }
    .gj_user_pin+.select2-container {
        margin-top: 40px;
    }
    .gj_genr_dets {
        width: 100%;
        margin: 30px auto 0px;
    }
  }
  @media (max-width: 767px) {
    .gj_genr_dets {
        width: 100%;
    }
  }
  /* Responsive */
  .page-content section.content.bgcolor-1 form span.select2.select2-container.select2-container--default.select2-container--focus, span.select2.select2-container.select2-container--default.select2-container--below, span.select2.select2-container.select2-container--default {
      width: 125px !important;
      margin: 15px auto !important;
      text-align: left;
  }
  .toppkfile {
      margin-left: 0px;
  }
</style>

<script type="text/javascript">
  $(document).ready(function() {
    $(".js-example-basic-single.select2_style.ad_usr.country").select2({
      placeholder: "Country",
    });
  });
  $("#gj_sel2_country").val('').trigger('change');

  $(document).ready(function() {
    $("#gj_sel2_pincode").select2({
      placeholder: "Pincode",
    });
  });
  $("#gj_sel2_pincode").val('').trigger('change');

  $(document).ready(function() {
      $("#gj_sel2_relation").select2({
        placeholder: "Relation",
      });
  });
  $("#gj_sel2_relation").val('').trigger('change');

  $(document).on('blur','input',function(){
    if($(this).val().length>=1){
      $(this).parent().addClass('input--filled');
    }
  });
</script>
<script>
  document.getElementById('files').addEventListener('change', handleFileSelect, false);
</script>

<script> /* Identity uploads */
  function handleFileSelect(evt) {
      var files = evt.target.files; // FileList object
      // Loop through the FileList and render image files as thumbnails.
      for (var i = 0, f; f = files[i]; i++) {
          // Only process image files.
          if (!f.type.match('image.*')) {
            continue;
          }
        var reader = new FileReader();
          // Closure to capture the file information.
          reader.onload = (function(theFile) {
        console.log(evt.target.id);
            return function(e) {
                // Render thumbnail.
                if(evt.target.id=="gj_file11"){
                  $('.ifile').prepend('<div class="gj_absolute p1-0 mr-2 mb-2 bdrstyle_hover" style="float:left"><img class="width-60px bdrstyle" src="'+e.target.result+'"><span>X</span></div>');

                    $('.bdrstyle_hover>span').on('click',function(){
                        $(this).parent().remove();
                  });
                }else  if(evt.target.id=="gj_file44"){
                  $('.yfile').prepend('<div class="gj_absolute p1-0 mr-2 mb-2 bdrstyle_hover" style="float:left"><img class="width-60px bdrstyle" src="'+e.target.result+'"><span>X</span></div>');

                    $('.bdrstyle_hover>span').on('click',function(){
                        $(this).parent().remove();
                  });
                }else  if(evt.target.id=="gj_file55"){
                  $('.xfile').prepend('<div class="gj_absolute p1-0 mr-2 mb-2 bdrstyle_hover" style="float:left"><img class="width-60px bdrstyle" src="'+e.target.result+'"><span>X</span></div>');

                    $('.bdrstyle_hover>span').on('click',function(){
                        $(this).parent().remove();
                  });
                }
            };
          })(f);
          // Read in the image file as a data URL.
          reader.readAsDataURL(f);
      }
    }

   $('.bdrstyle_hover>span').on('click',function(){
       $(this).parent().remove();
    });

    document.getElementById('gj_file11').addEventListener('change', handleFileSelect, false);
    document.getElementById('gj_file44').addEventListener('change', handleFileSelect, false);
    document.getElementById('gj_file55').addEventListener('change', handleFileSelect, false);

</script>
<script type="text/javascript">
  $( function() {
    $('.bank_ifsc').keyup();
  });
  $( function() {
    $('.pincode_no').keyup();
  });

  $('.bank_ifsc').keyup(function(){
    var id=$(this).val();
    $.post("<?php echo base_url('index.php/common_controller/get_ifsc'); ?>",
    {key:id},
    function(data,status){
      if(status=="success"){
        if(data==''){
          console.log(data)
        }else{
          var item=JSON.parse(data);//console.log(item)
          $(".bank_ifsc").autocomplete({
            source: item,
            minLength: 1,
          create: function () {
              $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
                return $('<li>')
                  .append('<a>'+ item.label +'</a>')
                  .appendTo(ul);
              };
          },
          select:function(event,ui){
                event.preventDefault();
                $(".bank_ifsc").val(ui.item.data_id);
                $('.bank_ifsc').val(ui.item.value).trigger('change');
                $('.bank_ifsc_hid').val(ui.item.data_id);
             }
            });
        }
        }
    });
  });
  $(".bank_ifsc").blur(function(){
    id = $('.bank_ifsc_hid').val()
    if(id != 0 || id != null){
      $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>index.php/Common_controller/get_bank_details_from_bank",
          data: {'id':id},
            success:  function (data) {
              if(data != null){
                var option = JSON.parse(data);
                //console.log(option);
                $('.bank_name').val(option['bank_name']);
                $('.bank_branch').val(option['bank_branch']);
                $('.bank_branch_lbl').addClass('input--filled');
              }
            }
        })
    }
  })

  $('.pincode_no').keyup(function(){
    var id=$(this).val();
    $.post("<?php echo base_url('index.php/common_controller/get_pincode'); ?>",
    {key:id},
    function(data,status){
      if(status=="success"){
        if(data==''){
          console.log(data)
        }else{
          var item=JSON.parse(data);//console.log(item)
          $(".pincode_no").autocomplete({
            source: item,
            minLength: 1,
          create: function () {
              $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
                return $('<li>')
                  .append('<a>'+ item.label +'</a>')
                  .appendTo(ul);
              };
          },
          select:function(event,ui){
                event.preventDefault();
                $(".pincode_no").val(ui.item.data_id);
                $('.pincode_no').val(ui.item.value).trigger('change');
                $('.pincode_id_hid').val(ui.item.data_id);
             }
            });
        }
        }
    });
  });
  $(".pincode_no").blur(function(){
    id = $(this).val()
    if(id > 0){
      $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>index.php/Common_controller/get_pincode_details",
          data: {'id':id},
            success:  function (data) {
              if(data != 0){
                if(data != null){
                  var option = JSON.parse(data);
                  //console.log(option);
                  $('.district').val(option['district']);
                  $('.state').val(option['state']);
                }else{
                  $('.pincode_id_hid').val(0);
                  $('.district').val('');
                  $('.state').val('');
                }
              }else{
                $('.pincode_id_hid').val(0);
                $('.district').val('');
                $('.state').val('');
              }
              
            }
        })
    }
  })

</script>

<script type="text/javascript">
  $.validate({
      form:'.gj_form',
    });

  $('.pesonal_but').click(function(){
    gender = $('input[name=gender]:checked').val();
    first_name = $('.first_name').val();
    last_name = $('.last_name').val();
    user_email = $('.user_email').val();
    dob = $('.dob').val();
    primary_mobile = $('.primary_mobile').val();
    secondary_mobile = $('.secondary_mobile').val();
    id = $('.edit_user_id').val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url('index.php/common_controller/update_user_from_pers_but'); ?>",
      data: {'first_name':first_name,'last_name':last_name,'user_email':user_email,'gender':gender,'dob':dob,'primary_mobile':primary_mobile,'secondary_mobile':secondary_mobile,'id':id},
      success: function(data) {
        if(data != 0){
          res = JSON.parse(data)
          if(res['status'] == 1){
            toast({
              message: "Personal Details Success",
              displayLength: 3000,
              className: 'success',
            });
          }else if(res['status'] == -1){
            toast({
              message: "User Fields Already Exists",
              displayLength: 3000,
              className: 'error',
            });
          }else{
            console.log(res);
            for (var key in res['array']) {
              $('.error.'+key).text(res['array'][key]);
              // console.log("key " + key + " has value " + res['array'][key]);
            }
          }
        }else{
          toast({
            message: "Invalid User",
            displayLength: 3000,
            className: 'error',
          });
        }
      }
    })
  })
  $('.bank_but').click(function(){
    account_no = $('.account_no').val();
    bank_ifsc = $('.bank_ifsc').val();
    bank_name = $('.bank_name').val();
    bank_branch = $('.bank_branch').val();
    id = $('.edit_user_id').val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url('index.php/common_controller/update_bank_details_from_bk_but'); ?>",
      data: {'account_no':account_no,'bank_ifsc':bank_ifsc,'bank_name':bank_name,'bank_branch':bank_branch,'id':id},
      success: function(data) {
        if(data != 0){
          res = JSON.parse(data)
          if(res['status'] == 1){
            toast({
              message: "Successfully Updated",
              displayLength: 3000,
              className: 'success',
            });
          }else if(res['status'] == -1){
            toast({
              message: "Update Failed",
              displayLength: 3000,
              className: 'error',
            });
          }else{
            console.log(res);
            for (var key in res['array']) {
              $('.error.'+key).text(res['array'][key]);
              // console.log("key " + key + " has value " + res['array'][key]);
            }
          }
        }else{
          toast({
            message: "Invalid Bank",
            displayLength: 3000,
            className: 'error',
          });
        }
      }
    })
  })
  $('.communication_but').click(function(){
    full_address = $('.full_address').val();
    pincode_no = $('.pincode_no').val();
    district = $('.district').val();
    state = $('.state').val();
    country = $('.country').val();
    id = $('.edit_user_id').val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url('index.php/common_controller/update_communication_details_from_cm_but'); ?>",
      data: {'full_address':full_address,'pincode_no':pincode_no,'district':district,'state':state,'country':country,'id':id},
      success: function(data) {
        if(data != 0){
          res = JSON.parse(data)
          if(res['status'] == 1){
            toast({
              message: "Successfully Updated",
              displayLength: 3000,
              className: 'success',
            });
          }else if(res['status'] == -1){
            toast({
              message: "Update Failed",
              displayLength: 3000,
              className: 'error',
            });
          }else{
            console.log(res);
            for (var key in res['array']) {
              $('.error.'+key).text(res['array'][key]);
              // console.log("key " + key + " has value " + res['array'][key]);
            }
          }
        }else{
          toast({
            message: "Invalid Bank",
            displayLength: 3000,
            className: 'error',
          });
        }
      }
    })
  })
  $('.nominee_but').click(function(){
    nominee_name = $('.nominee_name').val();
    n_mobile = $('.n_mobile').val();
    relation = $('.relation').val();
    n_dob = $('.n_dob').val();
    n_gender = $('input[name=n_gender]:checked').val();
    id = $('.edit_user_id').val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url('index.php/common_controller/update_nominee_details_from_no_but'); ?>",
      data: {'nominee_name':nominee_name,'n_mobile':n_mobile,'relation':relation,'n_dob':n_dob,'n_gender':n_gender,'id':id},
      success: function(data) {
        if(data != 0){
          res = JSON.parse(data)
          if(res['status'] == 1){
            toast({
              message: "Successfully Updated",
              displayLength: 3000,
              className: 'success',
            });
          }else if(res['status'] == -1){
            toast({
              message: "Update Failed",
              displayLength: 3000,
              className: 'error',
            });
          }else{
            console.log(res);
            for (var key in res['array']) {
              $('.error.'+key).text(res['array'][key]);
              // console.log("key " + key + " has value " + res['array'][key]);
            }
          }
        }else{
          toast({
            message: "Invalid Bank",
            displayLength: 3000,
            className: 'error',
          });
        }
      }
    })
  })
  $('.pass_but').click(function(){
    id = $('.edit_user_id').val();
    pwd = $('.pwd').val();
    pin_pwd = $('.pin_pwd').val();
    if(pwd.length>4 && pin_pwd.length>4){
      $.ajax({
        type: "POST",
        url: "<?php echo base_url('index.php/common_controller/update_pass_details_from_pas_but'); ?>",
        data: {'pwd':pwd,'pin_pwd':pin_pwd,'id':id},
        success: function(data) {
          if(data != 0){
            res = JSON.parse(data)
            if(res['status'] == 1){
              toast({
                message: "Successfully Updated",
                displayLength: 3000,
                className: 'success',
              });
            }else if(res['status'] == -1){
              toast({
                message: "Update Failed",
                displayLength: 3000,
                className: 'error',
              });
            }else{
              console.log(res);
              for (var key in res['array']) {
                $('.error.'+key).text(res['array'][key]);
                // console.log("key " + key + " has value " + res['array'][key]);
              }
            }
          }else{
            toast({
              message: "Invalid Bank",
              displayLength: 3000,
              className: 'error',
            });
          }
        }
      })
    }else{
      toast({
        message: "Min Length 4",
        displayLength: 2000,
        className: 'error',
      });
    }
  })

  $(".pesonal_but_x").on("click", function() {
        $(".gj_form").submit();
  });
  $(".identity_but_x").on("click", function() {
        $(".gj_identity").submit();
  });

  $('.gj_form').on('submit',(function(e) {
    e.preventDefault()
     var formData = new FormData(this);
    $.ajax({
        type:'POST',
        url: "<?php echo base_url('index.php/common_controller/update_user_aj_with_prof'); ?>",
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        success:function(data){
            if(data != 0){
            res = JSON.parse(data)
            if(res['status'] == 1){
              toast({
                message: "Successfully Updated",
                displayLength: 3000,
                className: 'success',
              });
            }else if(res['status'] == -1){
              toast({
                message: "Update Failed",
                displayLength: 3000,
                className: 'error',
              });
            }else{
              console.log(res);
              for (var key in res['array']) {
                $('.error.'+key).text(res['array'][key]);
                // console.log("key " + key + " has value " + res['array'][key]);
              }
            }
          }else{
            toast({
              message: "Invalid Bank",
              displayLength: 3000,
              className: 'error',
            });
          }
        },
       
    });
}));

  $('.gj_identity').on('submit',(function(e) {
    e.preventDefault()
     var formData = new FormData(this);
    $.ajax({
        type:'POST',
        url: "<?php echo base_url('index.php/common_controller/update_attach_aj_with'); ?>",
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        success:function(data){
            if(data != 0){
            res = JSON.parse(data)
            if(res['status'] == 1){
              toast({
                message: "Successfully Updated",
                displayLength: 3000,
                className: 'success',
              });
            }else if(res['status'] == -1){
              toast({
                message: "Update Failed",
                displayLength: 3000,
                className: 'error',
              });
            }else{
              console.log(res);
              for (var key in res['array']) {
                $('.error.'+key).text(res['array'][key]);
                // console.log("key " + key + " has value " + res['array'][key]);
              }
            }
          }else{
            toast({
              message: "Invalid Bank",
              displayLength: 3000,
              className: 'error',
            });
          }
        },
       
    });
}));
</script>

<style type="text/css">
  .input--nao {
    margin: 0px 0px 15px;
    max-width: 100%;
  }
  .aj_but{
    float: right;
    margin-right: 15px;
    margin-top: -2px;
    background-color: white;
    color: #2bb1e9;
    border: 0;
    padding: 2px 15px;
    font-weight: bold;
  }
</style>