<?php 
	if ($this->session->userdata('user_account_details') != null) {
		$account_holder_name = $_SESSION['user_account_details']['account_holder_name'];
		$account_no = $_SESSION['user_account_details']['account_no'];
		$account_type = $_SESSION['user_account_details']['account_type'];

		$bank_ifsc = $_SESSION['user_account_details']['bank_ifsc'];

		$bank_obj = unserialize($_SESSION['user_account_details']['bank_obj']);
		$bank_name = $bank_obj->getBankName();	
		$bank_branch = $bank_obj->getBankBranch();	
	}else{
		$account_holder_name='';$account_no='';
		$account_type='';$bank_ifsc='';
		$bank_name='';$bank_branch='';
	}
?>
<form method="post" action="<?php echo base_url('index.php/registration/add_account_details'); ?>" enctype="multipart/form-data">
	<div class="container-fluid">
		<div class="user-header">
			<h3>Add Account Details</h3>
		</div>



		<ul class="nav  navbar-inline navbar-right tab_menuref">
			<li><a href="s_refferal_detail.php"><i class="fa  fa-paper-plane-o font_icon"></i> Refferal Details</a></li>
			<li><a href="s_product_detail.php"><i class="fa fa-angle-double-down font_icon"></i> Product Details</a></li>
			<li><a href="#"><i class="fa fa-user font_icon"></i>Personal Details</a></li>
			<li class="active"><a href="s_bank_details.php"><i class="fa fa-building font_icon"></i> Bank Details</a></li>
			<li><a href="#"><i class="fa  fa-paperclip font_icon"></i> Attachments</a></li>
			<li><a href="#"><i class="fa   fa-lock font_icon"></i> Security</a></li>
		</ul>
		<div class="tab-contentt">
			<div id="#" class="tab-panee">
				<!-- <p class="refferal_tab_head"><i class="fa fa-bank fon_refeer"></i>Account Holder Details</p> -->
				<!-- <div class="col-md-5 pull-left row">
					<span class="input input--nao">
						<input class="input__field input__field--nao" type="text" id="input-1" name="account_holder_name"  value="<?php if(set_value('account_holder_name') != null){echo set_value('account_holder_name');}else{echo $account_holder_name;} ?>">
						<label class="input__label input__label--nao" for="input-1">
							<span class="input__label-content input__label-content--nao">Account Holder Name </span>
						</label>
						<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
							<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
						</svg>
					</span>	
					<?php echo form_error('account_holder_name','<span class="help-block form-error">', '</span>'); ?>							
				</div> -->
				<input type="hidden" name="account_holder_name" value="account_holder_name">
				<div class="col-md-3 pull-left row">
					<span class="input input--nao">
						<input class="input__field input__field--nao" type="text" id="input-1" name="account_no"  value="<?php if(set_value('account_no') != null){echo set_value('account_no');}else{echo $account_no;} ?>">
						<label class="input__label input__label--nao" for="input-1">
							<span class="input__label-content input__label-content--nao">Account No </span>
						</label>
						<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
							<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
						</svg>
					</span>		
					<?php echo form_error('account_no','<span class="help-block form-error">', '</span>'); ?>						
				</div>
				<div class="col-md-4 pull-left row">
					<div class="dkinput-txtflds">
						<span class="input input--nao input_spn">
							<select class="slt_type" type="text" name="account_type">
								<option> Account Type</option>
								<option value="0" <?php if(set_value('account_type') != null){ echo (set_value('account_type')==0)?" selected=' selected'":"";}elseif($account_type == 0){echo"selected";} ?>  >Savings Bank</option>
								<option value="1" <?php if(set_value('account_type') != null){ echo (set_value('account_type')==1)?" selected=' selected'":"";}elseif($account_type == 1){echo"selected";} ?>>Fixed</option>
							</select>
						</span>				
					</div>
				</div>
			</div>
		</div>
		<section>
			<div class="s_bank_details1 pull-left">
				<p class="refferal_tab_head"><i class="fa fa-bank fon_refeer"></i>Account Holder Details</p>
				<div class="col-md-3 pull-left row">
					<span class="input input--nao">
						<input class="input__field input__field--nao auto" type="text" id="input-1" value="" name="bank_ifsc">
            <!-- <?php //if(set_value('bank_ifsc') != null){echo set_value('bank_ifsc');}else{echo $bank_ifsc;} ?> -->
						<label class="input__label input__label--nao" for="input-1">
							<span class="input__label-content input__label-content--nao">IFSC Code </span>
						</label>
						<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
							<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
						</svg>
					</span>	
					<!-- <input type="hidden" class="bank_ifsc_hid"   name="bank_ifsc"> -->
					<?php echo form_error('bank_ifsc','<span class="help-block form-error">', '</span>'); ?>							
				</div>
				<div class="col-md-3 pull-left row">
					<span class="input input--nao">
						<input class="input__field input__field--nao bank_name" type="text" id="input-1"  name="bank_name" value="<?php if(set_value('bank_name') != null){echo set_value('bank_name');}else{echo $bank_name;} ?>">
						<label class="input__label input__label--nao" for="input-1">
							<span class="input__label-content input__label-content--nao">Bank Name </span>
						</label>
						<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
							<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
						</svg>
					</span>	
					<?php echo form_error('bank_name','<span class="help-block form-error">', '</span>'); ?>							
				</div>
				<div class="col-md-3 pull-left row">
					<span class="input input--nao bank_branch_lbl">
						<input class="input__field input__field--nao bank_branch" type="text" id="input-1"  name="bank_branch" value="<?php if(set_value('bank_branch') != null){echo set_value('bank_branch');}else{echo $bank_branch;} ?>">
						<label class="input__label input__label--nao" for="input-1">
							<span class="input__label-content input__label-content--nao">Branch Name </span>
						</label>
						<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
							<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
						</svg>
					</span>	
					<?php echo form_error('bank_branch','<span class="help-block form-error">', '</span>'); ?>							
				</div>
			</div>
			<input type="hidden" name="acc_page" value="acc_page">
		</section>
		<div class="text-center arrow_sectt">
			<div class="next_but"><a href="#"><i class="fa fa-arrow-circle-o-left font_arrow"></i>Prev </a></div>
			<div class="next_but"><a onclick="$(this).closest('form').submit()">Next <i class="fa fa-arrow-circle-o-right font_arrow"></i></a></div>
		</div>
	</div>
</form>


<script type="text/javascript">
	$( function() {
		$('.auto').keyup();
	});

	$('.auto').keyup(function(){
		var id=$(this).val();
		$.post("<?php echo base_url('index.php/common_controller/get_ifsc'); ?>",
		{key:id},
	 	function(data,status){
  		if(status=="success"){
		    if(data==''){
		   		console.log(data)
		    }else{
   				var item=JSON.parse(data);//console.log(item)
    			$(".auto").autocomplete({
				    source: item,
				    minLength: 1,
				 	create: function () {
				  		$(this).data('ui-autocomplete')._renderItem = function (ul, item) {
					   		return $('<li>')
					      	.append('<a>'+ item.label +'</a>')
					      	.appendTo(ul);
					  	};
				 	},
				 	select:function(event,ui){
			      		event.preventDefault();
			      		$(".auto").val(ui.item.data_id);
			      		$('.auto').val(ui.item.value).trigger('change');
			      		$('.bank_ifsc_hid').val(ui.item.value);
				     }
				    });
    		}
      	}
    });
 	});
  $(".auto").blur(function(){
    id = $('.bank_ifsc_hid').val()
    if(id != 0 || id != null){
      $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>index.php/Common_controller/get_bank_details_from_bank",
          data: {'id':id},
            success:  function (data) {
              if(data != null){
                var option = JSON.parse(data);
                //console.log(option);
                $('.bank_name').val(option['bank_branch']);
                $('.bank_branch').val(option['bank_branch']);
                $('.bank_branch_lbl').addClass('input--filled');
              }
            }
        })
    }
  })
</script>

<style type="text/css">
  ul.nav.navbar-inline.navbar-right.tab_menuref {
      width: 100%;
      margin: 0;
  }
  .nav>li>a:focus, .nav>li>a:hover {
      text-decoration: none;
      background-color: #2bb1e9;
  }
</style>