<?php 
  if ($this->session->userdata('user_id_details') != null) {
    $pan_no = $_SESSION['user_id_details']['pan_no'];
    $aadhar_no = $_SESSION['user_id_details']['aadhar_no'];
    $pass_no = $_SESSION['user_id_details']['pass_no'];
  }else{
    $pan_no='';$aadhar_no='';$pass_no='';
  }
?>
<form method="post" action="<?php echo base_url('index.php/registration/add_identity_card_details') ?>" enctype="multipart/form-data">
  <div class="container-fluid">
    <div class="user-header">
      <h3>User Module</h3>
    </div>
      <ul class="nav  navbar-inline navbar-right tab_menuref">
      <li><a href="s_refferal_detail.php"><i class="fa  fa-paper-plane-o font_icon"></i> Refferal Details</a></li>
      <li><a href="s_product_detail.php"><i class="fa fa-angle-double-down font_icon"></i> Product Details</a></li>
      <li><a href="#"><i class="fa fa-user font_icon"></i>Personal Details</a></li>
      <li><a href="#"><i class="fa fa-building font_icon"></i> Bank Details</a></li>
      <li class="active"><a href="#"><i class="fa  fa-paperclip font_icon"></i> Attachments</a></li>
      <li><a href="#"><i class="fa   fa-lock font_icon"></i> Security</a></li>
    </ul>
    <div class="tab-contentt">
      <div class="s_attachment_detail pull-left">
        <div class="col-md-4 pull-left row ">
          <span class="input input--nao">
            <input class="input__field input__field--nao" type="text" id="input-1" name="pan_no"  value="<?php if(set_value('pan_no') != null){echo set_value('pan_no');}else{echo $pan_no;} ?>" >
            <label class="input__label input__label--nao" for="input-1">
              <span class="input__label-content input__label-content--nao">Pancard No </span>
            </label>
            <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
              <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
            </svg>
          </span> 
          <?php echo form_error('pan_no','<span class="help-block form-error">', '</span>'); ?>           
        </div>
        <div class="col-md-4 pull-left row">
          <span class="input input--nao">
            <input class="input__field input__field--nao" type="text" id="input-1" name="aadhar_no"  value="<?php if(set_value('aadhar_no') != null){echo set_value('aadhar_no');}else{echo $aadhar_no;} ?>" >
            <label class="input__label input__label--nao" for="input-1">
              <span class="input__label-content input__label-content--nao">Aadhar Card No </span>
            </label>
            <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
              <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
            </svg>
          </span>   
          <?php echo form_error('aadhar_no','<span class="help-block form-error">', '</span>'); ?>          
        </div>
        <div class="col-md-4 pull-left row">
          <span class="input input--nao">
            <input class="input__field input__field--nao" type="text" id="input-1" name="pass_no"  value="<?php if(set_value('pass_no') != null){echo set_value('pass_no');}else{echo $pass_no;} ?>" >
            <label class="input__label input__label--nao" for="input-1">
              <span class="input__label-content input__label-content--nao">Bank Passbook </span>
            </label>
            <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
              <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
            </svg>
          </span> 
          <?php echo form_error('pass_no','<span class="help-block form-error">', '</span>'); ?>              
        </div>
        <div class="col-md-4 pull-left row pkfile">
            <div class="col-6 toppkfile form-group row">
              <input type="file" name="pan_front" id="filesx" class="form-control img-responsive filesx" > 
             
              <div class="width-60px width-6px ml-2 pan_front_img"></div>
            </div>
            <div class="col-6 toppkfile form-group row">
              <input type="file" name="pan_back" id="filesx" class="form-control img-responsive filesx" > 
             
              <div class="width-60px width-6px ml-2 pan_back_img"></div>
            </div>
        </div>
        <div class="col-md-4 pull-left row pkfile">
            <div class="col-6 toppkfile form-group row">
              <input type="file" name="aadhar_front" id="filesx" class="form-control img-responsive filesx" > 
             
              <div class="width-60px width-6px ml-2 aadhar_front_img"></div>
            </div>
            <div class="col-6 toppkfile form-group row">
              <input type="file" name="aadhar_back" id="filesx" class="form-control img-responsive filesx" > 
             
              <div class="width-60px width-6px ml-2 aadhar_back_img"></div>
            </div>
        </div>
        <div class="col-md-4 pull-left row pkfile">
            <div class="col-6 toppkfile form-group row">
              <input type="file" name="pass_front" id="filesx" class="form-control img-responsive filesx"> 
             
              <div class="width-60px width-6px ml-2 pass_front_img"></div>
            </div>
            <div class="col-6 toppkfile form-group row">
              <input type="file" name="pass_back" id="filesx" class="form-control img-responsive filesx"> 
             
              <div class="width-60px width-6px ml-2 pass_back_img"></div>
            </div>
        </div>
        

        <div class="rr">
                  <div class="col-md-4 pull-left row ">
                    <div class="dkinput-txtflds mar_lef_5">
                      <label class="chkbo ">
                        <input type="checkbox" name="pan_verified" value="1" class="pan_front_check pan_back_check" >
                        <span class="checkbox-material">
                          <span class="check"></span>
                        </span>  Pan Verified
                      </label>
                    </div>
                  </div>
                  <div class="col-md-4 pull-left row ">
                    <div class="dkinput-txtflds mar_lef_5">
                      <label class="chkbo ">
                        <input type="checkbox" name="aadhar_verified" class="aadhar_front_check aadhar_back_check" value="1" >
                        <span class="checkbox-material">
                          <span class="check"></span>
                        </span>  Id Verified
                      </label>
                    </div>
                  </div>
                  <div class="col-md-4 pull-left row ">
                    <div class="dkinput-txtflds mar_lef_5">
                      <label class="chkbo ">
                        <input type="checkbox" name="pass_verified" value="1" class="pass_front_check pass_back_check">
                        <span class="checkbox-material">
                          <span class="check"></span>
                        </span>  PassBook Verified
                      </label>
                    </div>
                  </div>
                </div>
 
                <div class="rr usr_ad_idnty">
                  <div class="col-md-2 pull-left ">
                    <div class="dkinput-txtflds mar_lef_miuns10">
                      <label class="chkbo ">
                        <input type="checkbox" name="skip" value="1" class="skip_cbox">
                        <span class="checkbox-material">
                          <span class="check"></span>
                        </span>  Skip Now
                      </label>
                    </div>
                  </div>
                  <div class="col-md-10 pull-left attch_documt_date">
                    <p class="color we_need" style="font-weight: bold;">
                      You can't Receive money untill KYC is Updated <?php //echo date('d-m-Y', strtotime("+90 days")); ?>
                        
                      </p>
                  </div>
                </div>

                <div class="rr ">
                  <div class="col-md-2 pull-left ">
                    <div class="dkinput-txtflds mar_lef_miuns10">
                      <label class="chkbo ">
                        <input type="checkbox" name="kyc_status" value="1" class=""  >
                        <span class="checkbox-material">
                          <span class="check"></span>
                        </span>  KYC Update
                      </label>
                    </div>
                  </div>
                </div>

        <div class="col-md-10 pull-left attch_documt_date">
          <!-- <a href="#">We need to document with in 18-02-2017</a> -->
        </div>
      </div>
      <div class="text-center arrow_sect">
        <div class="next_but"><a href="#"><i class="fa fa-arrow-circle-o-left font_arrow"></i>Prev </a></div>
        <div class="next_but"><a onclick="$(this).closest('form').submit();">Next <i class="fa fa-arrow-circle-o-right font_arrow"></i></a></div>
      </div>
    </div>
  </div>  
  <input type="hidden" name="id_page" value="id_page">
</form>
<script>
  $(document).ready(function() { 
      $('.we_need').hide();
  });
  $('.skip_cbox').click(function(){
    if($(this).is(':checked')){
      $('.we_need').show();
    }else{$('.we_need').hide()}
  })
      function readURL(input,class_name) {
        if (input.files && input.files[0]) {
              var reader = new FileReader();
              reader.onload = function (e) {
                  $('.'+class_name+'_img').prepend('<div class="p1-0 mr-2 mb-2 bdrstyle_hover" style="float:left"><img class="width-60px bdrstyle" src="'+e.target.result+'"><span>X</span></div>');
              }
              reader.readAsDataURL(input.files[0]);
        }
    }

    $(".filesx").change(function(){
        readURL(this,$(this).attr('name'));
    });
    $('.bdrstyle_hover>span').on('click',function(){
      $(this).parent().remove();
    }); 
</script>
<style type="text/css">
  .pkfile input.form-control {
      opacity: 0;
      height: 90px;
      position: absolute;
      width: 95px;
  }
  img.width-60px.bdrstyle {
      width: 90px;
      height: 80px;
      padding: 2px;
      border: 1px solid #d7d7d7;
      margin-left: -15px;
  }
  .p1-0.mr-2.mb-2.bdrstyle_hover {
      position: absolute;
      top: 0;
  }
  .navbar-right{
      float: none !important;
      margin-right: -15px !important;
    }
    .nav>li>a:focus, .nav>li>a:hover {
      text-decoration: none;
      background-color: #2bb1e9;
  }
</style>