<?php 
  if ($this->session->userdata('user_personal_details') != null) {
    //$user_name = $_SESSION['user_personal_details']['user_name'];
    $user_email = $_SESSION['user_personal_details']['user_email'];
    $first_name = $_SESSION['user_personal_details']['first_name'];
    $last_name = $_SESSION['user_personal_details']['last_name'];
    $gender = $_SESSION['user_personal_details']['gender'];
    if($_SESSION['user_personal_details']['dob'] != null){
      $dob = $_SESSION['user_personal_details']['dob']->format('Y-m-d'); 
    }else{$dob=null;}
    $n_name = $_SESSION['user_personal_details']['n_name'];
    if($_SESSION['user_personal_details']['n_dob'] != null){
      $n_dob = $_SESSION['user_personal_details']['n_dob']->format('Y-m-d'); 
    }else{$n_dob=null;}
    $n_relationship = $_SESSION['user_personal_details']['n_relationship'];
    $n_phoneno = $_SESSION['user_personal_details']['n_phoneno'];
    $n_gender = $_SESSION['user_personal_details']['n_gender'];
    $mobile = $_SESSION['user_personal_details']['mobile'];
    $alter_mobile = $_SESSION['user_personal_details']['alter_mobile'];
    $full_address = $_SESSION['user_personal_details']['full_address'];
    $pincode_no = $_SESSION['user_personal_details']['pincode_no'];
    $pincode_obj = unserialize($_SESSION['user_personal_details']['pincode_obj']);
    if($pincode_obj != null){
      $district = $pincode_obj->getPincodeDistrict();
      $state = $pincode_obj->getPincodeState();
      $country = $pincode_obj->getPincodeCountryId()->getCountryName();
    }else{
      $district = "";
      $state = "";
      $country = "";
    }
    
  }else{$user_name = "";$user_email = "";$first_name = "";$last_name = "";$gender = "";$dob = "";$n_name = "";$n_dob = "";
  $n_relationship = "";$n_phoneno = "";$n_gender = "";$mobile = "";$alter_mobile = "";$full_address = "";$pincode_no = "";$district = "";$state = "";$country = "";}
?>


<div class="container-fluid">
  <div class="user-header">
    <h3>User Module</h3>
  </div>
  <ul class="nav  navbar-inline navbar-right tab_menuref">
    <li><a href="s_refferal_detail.php"><i class="fa  fa-paper-plane-o font_icon"></i> Refferal Details</a></li>
    <li><a href="s_product_detail.php"><i class="fa fa-angle-double-down font_icon"></i> Product Details</a></li>
    <li class="active"><a href="#"><i class="fa fa-user font_icon"></i>Personal Details</a></li>
    <li><a href="#"><i class="fa fa-building font_icon"></i> Bank Details</a></li>
    <li><a href="#"><i class="fa fa-paperclip font_icon"></i> Attachments</a></li>
    <li><a href="#"><i class="fa fa-lock font_icon"></i> Security</a></li>
  </ul> <div class="tab-contentt">
    <form method="post" action="<?php echo base_url('index.php/registration/add_user') ?>" enctype="multipart/form-data" class="wpc_contact" name="frm" id="user_form">
      <div class="s_personal_detal">
        <div class="col-md-10 row pull-left">
          
          <!-- <div class="col-md-3 pull-left">
            <span class="input input--nao">
              <input class="input__field input__field--nao user_name" type="text" id="input-1" name="user_name" value="<?php if(set_value('user_name') != null){echo set_value('user_name');}else{echo $user_name;} ?>" 
              data-validation="required custom length"
                    data-validation-length="1-100"
                    
                    data-validation-error-msg-custom="Please provide a valid user name"
                     />
              <label class="input__label input__label--nao" for="input-1">
                <span class="input__label-content input__label-content--nao">User Name </span>
              </label>
              <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
              </svg>
              <img src="<?php echo base_url('assets/images/loader/'); ?>default.gif" class="ldr uname_ldr">
            </span> 
            <p class="uname_suc avail_er suc">Available</p>
            <p class="uname_err avail_er err">Already Exists</p>
            <?php echo form_error('user_name','<span class="help-block form-error">', '</span>'); ?>            
          </div> -->
          
          <div class="col-md-3 pull-left ">
            <span class="input input--nao">
              <input class="input__field input__field--nao" type="text" id="input-1" name="first_name" value="<?php if(set_value('first_name') != null){echo set_value('first_name');}else{echo $first_name;} ?>" data-validation=" required length alphanumeric"
                      data-validation-ignore=". "
                      data-validation-length="min3"/>
              <label class="input__label input__label--nao" for="input-1">
                <span class="input__label-content input__label-content--nao">First Name </span>
              </label>
              <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
              </svg>
            </span> 
            <?php echo form_error('first_name','<span class="help-block form-error">', '</span>'); ?>           
          </div>
          <div class="col-md-3 pull-left ">
            <span class="input input--nao">
              <input class="input__field input__field--nao" type="text" id="input-1" name="last_name" value="<?php if(set_value('last_name') != null){echo set_value('last_name');}else{echo $last_name;} ?>" data-validation="length alphanumeric"
                      data-validation-ignore="."
                      data-validation-length="min3"
                      data-validation-optional="true"/>
              <label class="input__label input__label--nao" for="input-1">
                <span class="input__label-content input__label-content--nao">Last Name </span>
              </label>
              <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
              </svg>
            </span>
            <?php echo form_error('last_name','<span class="help-block form-error">', '</span>'); ?>              
          </div>
          <div class="col-md-3 pull-left person_phone_nub">
            <span class="input input--nao">
              <input class="input__field input__field--nao user_email" type="email" id="input-1" name="user_email" value="<?php if(set_value('user_email') != null){echo set_value('user_email');}else{echo $user_email;} ?>"/>
              <label class="input__label input__label--nao" for="input-1">
                <span class="input__label-content input__label-content--nao">Email ID </span>
              </label>
              <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
              </svg>
              <img src="<?php echo base_url('assets/images/loader/'); ?>default.gif" class="ldr umail_ldr">
            </span>   
            <p class="umail_suc avail_er suc">Available</p>
            <p class="umail_err avail_er err">Already Exists</p>
            <?php echo form_error('user_email','<span class="help-block form-error">', '</span>'); ?>         
          </div>

          <div class="col-md-3 pull-left">
            <span class="input input--nao" id="sandbox-container">
              <input type="text" class="input__field input__field--nao form-control"  name="dob" value="<?php if(set_value('dob') != null){echo set_value('dob');}else{echo $dob;} ?>">
              <label class="input__label input__label--nao" for="input-1">
                <span class="input__label-content input__label-content--nao">Date of Birth </span>
              </label>
              <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
              </svg>
            </span> 
            <?php echo form_error('dob','<span class="help-block form-error">', '</span>'); ?>            
          </div>
          <div class="col-md-3 pull-left person_phone_nub">
            <span class="input input--nao">
              <input class="input__field input__field--nao mobile" type="number" id="input-1" name="mobile" value="<?php if(set_value('mobile') != null){echo set_value('mobile');}else{echo $mobile;} ?>" data-validation="required length alphanumeric"
                      data-validation-ignore="+ " 
                      data-validation-length="10-12"/>
              <label class="input__label input__label--nao" for="input-1">
                <span class="input__label-content input__label-content--nao">Phone No </span>
              </label>
              <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
              </svg>
              <img src="<?php echo base_url('assets/images/loader/'); ?>default.gif" class="ldr umob_ldr">
            </span> 
            <p class="umob_suc avail_er suc">Available</p>
            <p class="umob_err avail_er err">Already Exists</p>
            <?php echo form_error('mobile','<span class="help-block form-error">', '</span>'); ?>           
          </div>
          <div class="col-md-3 pull-left person_phone_nub">
            <span class="input input--nao">
              <input class="input__field input__field--nao" type="number" id="input-1"  name="alter_mobile"  value="<?php if(set_value('alter_mobile') != null){echo set_value('alter_mobile');}else{echo $alter_mobile;} ?>"  data-validation="required length alphanumeric"
                      data-validation-ignore="+ " 
                      data-validation-length="10-12"
                      data-validation-optional="true"/>
              <label class="input__label input__label--nao" for="input-1">
                <span class="input__label-content input__label-content--nao">Alter Mobile </span>
              </label>
              <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
              </svg>
            </span> 
            <?php echo form_error('alter_mobile','<span class="help-block form-error">', '</span>'); ?>           
          </div>
          <div class="col-md-3 pull-left">
            <div class="checkbutvew">
              <label>Gender</label> 
              <label class="chkbo" style="display:inline">M
                <input type="radio" id="radio" name="gender" value="1" <?php if(set_value('gender') != null){if(set_radio('gender', '1')) { echo"checked";}}elseif($gender == 1){echo"checked";} ?> data-validation="required" >
                <label for="radio"></label>
              </label>
              <label class="chkbo" style="display:inline" > F
                <input type="radio" id="radio2" name="gender"  value="0" <?php if(set_value('gender') != null){if(set_radio('gender', '0')) { echo"checked";}}elseif($gender == 0){echo"checked";}  ?> data-validation="required" >
                <label for="radio2"></label>
              </label>
              <label class="chkbo" style="display:inline"  >O
                <input type="radio" id="radio3"  name="gender"  value="2" <?php if(set_value('gender') != null){if(set_radio('gender', '2')) { echo"checked";}}elseif($gender == 2){echo"checked";}  ?> data-validation="required" >
                <label for="radio3"></label>
              </label>
            </div>
          </div>
        </div>
        <div class="col-md-2 pull-left">
          <img class="profile-pic img-fluid" src="<?php echo base_url('assets/images'); ?>/user_photo.png" />
          <input class="file-upload up_file" type="file" name="file" accept="image/*"/>
        </div>
      </div>
      <!-- nominee details-->
      <div class="s_personal_detal pull-left">
        <p class="refferal_tab_head"><i class="fa fa-user-plus fon_refeer"></i>Nominee Details</p>
        <div class="col-md-3 pull-left ">
          <span class="input input--nao">
            <input class="input__field input__field--nao" type="text" id="input-1" name="n_name" value="<?php if(set_value('n_name') != null){echo set_value('n_name');}else{echo $n_name;} ?>" data-validation="required custom length"
                data-validation-length="2-100"
                data-validation-regexp="^[A-Za-z][A-Za-z ]+$"
                data-validation-error-msg-custom="Please provide valid nominee name"/>
            <label class="input__label input__label--nao" for="input-1">
              <span class="input__label-content input__label-content--nao">Nominee Name </span>
            </label>
            <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
              <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
            </svg>
          </span>
          <?php echo form_error('n_name','<span class="help-block form-error">', '</span>'); ?>             
        </div>
        <div class="col-md-2 pull-left">
            <span class="input input--nao" id="sandbox-container">
              <input type="text" class="input__field input__field--nao form-control" name="n_dob"  value="<?php if(set_value('n_dob') != null){echo set_value('n_dob');}else{echo $n_dob;} ?>" 
              >
              <label class="input__label input__label--nao" for="input-1"  >
                <span class="input__label-content input__label-content--nao">DOB </span>
              </label>
              <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
              </svg>
            </span>   
            <?php echo form_error('n_dob','<span class="help-block form-error">', '</span>'); ?>          
          </div>
        <div class="col-md-2 pull-left pad_zero">
          <label for="id_label_single" class="input input--nao">
            <select class="js-example-basic-single relation select2_style" name="n_relationship"
            data-validation="required"
            >
              <option value="Father" <?php  if(set_value('n_relationship') != null){ if(set_value('n_relationship') == 'Father') { echo"checked";} }?>>Father</option>
              <option value="Mother" <?php  if(set_value('n_relationship') != null){ if(set_value('n_relationship') == 'Mother') { echo"checked";} }?>>Mother</option>
              <option value="Husband" <?php  if(set_value('n_relationship') != null){ if(set_value('n_relationship') == 'Husband') { echo"checked";} }?>>Husband</option>
              <option value="Wife" <?php  if(set_value('n_relationship') != null){ if(set_value('n_relationship') == 'Wife') { echo"checked";} }?>>Wife</option>
              <option value="Son" <?php  if(set_value('n_relationship') != null){ if(set_value('n_relationship') == 'Son') { echo"checked";} }?>>Son</option>
              <option value="Daughter" <?php  if(set_value('n_relationship') != null){ if(set_value('n_relationship') == 'Daughter') { echo"checked";} }?>>Daughter</option>
              <option value="Brother" <?php  if(set_value('n_relationship') != null){ if(set_value('n_relationship') == 'Brother') { echo"checked";} }?>>Brother</option>
              <option value="Sister" <?php  if(set_value('n_relationship') != null){ if(set_value('n_relationship') == 'Sister') { echo"checked";} }?>>Sister</option>

            </select>
          </label>    
          <?php echo form_error('n_relationship','<span class="help-block form-error">', '</span>'); ?> 
        </div>
        <div class="col-md-2 pull-left">
          <span class="input input--nao">
            <input class="input__field input__field--nao" type="number" id="input-1" name="n_phoneno" value="<?php if(set_value('n_phoneno') != null){echo set_value('n_phoneno');}else{echo $n_phoneno;} ?>"  data-validation="required number length"
                  data-validation-ignore="+-" 
                              data-validation-length="10-12"/>
            <label class="input__label input__label--nao" for="input-1">
              <span class="input__label-content input__label-content--nao">Phone No</span>
            </label>
            <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
              <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
            </svg>
          </span>
          <?php echo form_error('n_phoneno','<span class="help-block form-error">', '</span>'); ?>              
        </div>
        <div class="col-md-3 pull-left">
          <div class="checkbutvew">
            <label>Gender</label> 
            <label class="chkbo" style="display:inline">M
              <input type="radio" id="radio" name="n_gender" value="1" <?php if(set_value('gender') != null){if(set_radio('gender', '1')) { echo"checked";}}elseif($gender == 1){echo"checked";} ?>  data-validation="required">
              <label for="radio"></label>
            </label>
            <label class="chkbo" style="display:inline" > F
              <input type="radio" id="radio2" name="n_gender"  value="0" <?php if(set_value('gender') != null){if(set_radio('gender', '0')) { echo"checked";}}elseif($gender == 0){echo"checked";}  ?>  data-validation="required">
              <label for="radio2"></label>
            </label>
            <label class="chkbo" style="display:inline"  >O
              <input type="radio" id="radio3"  name="n_gender"  value="2" <?php if(set_value('gender') != null){if(set_radio('gender', '2')) { echo"checked";}}elseif($gender == 2){echo"checked";}  ?>  data-validation="required">
              <label for="radio3"></label>
            </label>
          </div>
        </div>
      </div>
      <!-- communication details-->
      <div class="s_personal_detal pull-left">
        <p class="refferal_tab_head"><i class="fa fa-home fon_refeer"></i>Communication Details</p>
        <div class="col-md-4 pull-left">
          <span class="input input--nao">
            <input class="input__field input__field--nao" type="text" id="input-1" name="full_address" value="<?php if(set_value('full_address') != null){echo set_value('full_address');}else{echo $full_address;} ?>" data-validation="length"
                data-validation-length="min10"
                />
            <label class="input__label input__label--nao" for="input-1">
              <span class="input__label-content input__label-content--nao">Full Address </span>
            </label>
            <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
              <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
            </svg>
          </span>   
          <?php echo form_error('full_address','<span class="help-block form-error">', '</span>'); ?>
        </div>
        <div class="col-md-2 pull-left">

          <span class="input input--nao dkinput-txtflds">
                        <input class="input__field input__field--nao pincode_no" type="text" id="tags" name="pincode_no"  value="<?php if(set_value('pincode_no') != null){echo set_value('pincode_no');}else{echo $pincode_no;} ?>" 
                data-validation-length="4-6" data-validation="length required">
                        <label class="input__label input__label--nao" for="r_password">
                          <span class="input__label-content input__label-content--nao"> Pincode </span>
                        </label>
                        <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                        <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></path></svg>
                    </span>
          <?php echo form_error('pincode_no','<span class="help-block form-error">', '</span>'); ?>

        </div>
        <div class="col-md-2 pull-left ">
          <span class="input input--nao district_label">
            <input class="input__field input__field--nao district" type="text" id="input-1" name="district" value="<?php if(set_value('district') != null){echo set_value('district');}else{echo $district;} ?>" data-validation="required custom length" 
                  data-validation-length="2-100"
                  data-validation-regexp="^[A-Za-z][A-Za-z0-9-_\ .&amp;]+$"
                  data-validation-error-msg-custom="Provide valid district name"/>
            <label class="input__label input__label--nao " for="input-1">
              <span class="input__label-content input__label-content--nao">District </span>
            </label>
            <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
              <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
            </svg>
          </span>   
          <?php echo form_error('district','<span class="help-block form-error">', '</span>'); ?>         
        </div>
        <div class="col-md-2 pull-left ">
          <span class="input input--nao state_label">
            <input class="input__field input__field--nao state" type="text" id="input-1" name="state" value="<?php if(set_value('state') != null){echo set_value('state');}else{echo $state;} ?>" data-validation="required custom length" 
                data-validation-length="2-100"
                data-validation-regexp="^[A-Za-z][A-Za-z0-9-_\ .&amp;]+$"
                data-validation-error-msg-custom="please provide valid state"/>
            <label class="input__label input__label--nao" for="input-1">
              <span class="input__label-content input__label-content--nao">State </span>
            </label>
            <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
              <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
            </svg>
          </span> 
          <?php echo form_error('state','<span class="help-block form-error">', '</span>'); ?>            
        </div>
        <div class="col-md-2 pull-left country">
          <select class="js-example-basic-single select2_style ad_usr country" name="country_id" id="combobox">
            <?php foreach ($country_list as $list) {?>
              <option value="<?php echo $list->getCountryId();  ?>" <?php if(set_value('country_id') == $list->getCountryId()){echo "selected='selected'";} ?>><?php echo $list->getCountryName();  ?></option>
            <?php  } ?>
          </select>
          <?php echo form_error('country_id','<span class="help-block form-error">', '</span>'); ?> 
        </div>
        <input type="hidden" name="user_page" value="asdf">
        <div class="text-center arrow_sect">
          <div class="next_but"><a onclick="goBack()"><i class="fa fa-arrow-circle-o-left font_arrow"></i>Prev </a></div>
          <div class="next_but"><a onclick="$(this).closest('form').submit()">Next <i class="fa fa-arrow-circle-o-right font_arrow"></i></a></div>
        </div>
      </div>
    </form>
  </div>
</div>
<script>
  $.validate({
    lang: 'en',
      form:'#user_form',
      //modules :'security'
      modules :'logic,security,date',
      onModulesLoaded : function() {
        console.log('Modules are loaded successfully');
      }
  });
  function goBack() {
      window.history.go(-2);
  }
</script>
<?php 
  $pincode_array = array();
  foreach ($pincode_list as $list) {  
    array_push($pincode_array, $list->getPincodeNo());
  }
  function js_str($s) { return '"' . addcslashes($s, "\0..\37\"\\") . '"'; }
  function js_array($array) { 
    $temp = array_map('js_str', $array);
    return '[' . implode(',', $temp) . ']';
  }
?>
<script type="text/javascript">
  $(document).ready(function() { 
    $(".js-example-basic-single").select2({ 
      placeholder: "Select a COuntry",
        allowClear: true
      });
  });
  $( function() { 
        $( "#tags" ).autocomplete({
          source: <?php echo  js_array($pincode_array);?>
        });
        // $('.district,.state,.country').attr('disabled', 'disabled');
        $('.uname_ldr,.umail_ldr,.umob_ldr').hide();
    $('.uname_suc,.uname_err,.umail_suc,.umail_err,.umob_suc,.umob_err').hide();
    $(".relation").select2({ 
      placeholder: "Select a Relation",
        allowClear: true
      }); 
      $('.relation').val('').trigger('change');  
    });
  
  //Pincode Ajax Start
      $('.pincode_no').blur(function()  {
        var pincode = $('.pincode_no').val();
          if(pincode.length > 0){
            $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/Common_controller/ajax_pincode",
            data: {'pincode':pincode},
              success:  function (data) {

              if(data != 0){
                  var option = JSON.parse(data);
                  console.log(option);
                  if(option[2]['pincode_district'] == false){
                    $('.district').val("");
                    $('.district').attr('disabled', 'disabled');
                  }else{
                    $('.district_label').addClass('input--filled');
                    $('.district').val(option[2]['pincode_district']).removeAttr('disabled');
                    $('.district').attr('disabled', false);
                  }
                  if(option[3]['pincode_state'] == false){
                    $('.state').val("");
                    $('.state').attr('disabled', 'disabled');
                  }else{
                    $('.state_label').addClass('input--filled');
                    $('.state').attr('disabled', 'false');
                    $('.state').val(option[3]['pincode_state']).removeAttr('disabled');
                  }
                  if(option[4]['pincode_country'] == false){
                    $('.country').val("");
                    $('.country').attr('disabled', 'disabled');
                  }else{
                    $('.country_label').addClass('input--filled');
                    $('.country').val(option[4]['pincode_country']).removeAttr('disabled');
                    $('.country').attr('disabled', 'false');
                  }
              }else{
                 // $('.district,.state,.country').val('');
                  // $('.state_label,.district_label,.country_label').addClass('sinput--filled');
                 // $('.district,.state,.country').attr('disabled', 'fasle');
              }
              }
          });
          }else{
            // $('.state_label,.district_label,.country_label').addClass('input--filled');
           //   $('.district,.state,.country').attr('disabled', 'fasle');
           //   $('.district,.state,.country').val('');
          }
      });
    //Pincode Ajax End

    $('.user_name').focus(function()  {
      var a = $('.user_name').val();
      if(a.length>0){
        cc_name = a;
      }else{
        cc_name="";
      }
    })
    $('.user_name').blur(function()  {
      var user_name = $('.user_name').val();
      if(user_name.length>0 && cc_name != user_name){
        $('.uname_ldr').show();
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>index.php/Common_controller/user_name_available",
          data: {'field_value':user_name},
            success:  function (data) { 
              $('.uname_ldr').hide();
              if(data == 1){
                $('.uname_suc').show();
                $('.uname_err').hide();
              }else{
                $('.uname_suc').hide();
                $('.uname_err').show();
              }
            }
        })
      }else if(user_name.length == 0 && cc_name.length > 2){
       $('.uname_suc,.uname_err').hide();
      }
    })

    $('.user_email').focus(function()  {
      var a = $('.user_email').val();
      if(a.length>0){
        cc_mail = a;
      }else{
        cc_mail="";
      }
    })
    $('.user_email').blur(function()  {
      var user_email = $('.user_email').val();
        if(validateEmail(user_email)){
        if(user_email.length>5 && cc_mail != user_email){
          $('.uname_ldr').show();
          $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/Common_controller/user_email_available",
            data: {'field_value':user_email},
              success:  function (data) { 
                $('.uname_ldr').hide();
                if(data == 1){
                  $('.umail_suc').show();
                  $('.umail_err').hide();
                }else{
                  $('.umail_suc').hide();
                  $('.umail_err').show();
                }
              }
          })
        }else if(user_email.length == 0 && cc_mail.length > 2){
         $('.umail_suc,.umail_err').hide();
        }
    }else{
       $('.umail_suc,.umail_err').hide();
      }
    })

    $('.mobile').focus(function()  {
      var a = $('.mobile').val();
      if(a.length>0){
        cc_mob = a;
      }else{
        cc_mob="";
      }
    })
    $('.mobile').blur(function()  {
      var mobile = $('.mobile').val();
      if(mobile.length>=10 && mobile.length<=12 && cc_mob != mobile){
        $('.umob_ldr').show();
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>index.php/Common_controller/user_mobile_available",
          data: {'field_value':mobile},
            success:  function (data) { 
              $('.umob_ldr').hide();
              if(data == 1){
                $('.umob_suc').show();
                $('.umob_err').hide();
              }else{
                $('.umob_suc').hide();
                $('.umob_err').show();
              }
            }
        })
      }else{
       $('.umob_suc,.umob_err').hide();
      }
    })
    function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
</script>
<style type="text/css">
  .page-content section.content.bgcolor-1 form span.select2.select2-container.select2-container--default.select2-container--focus, span.select2.select2-container.select2-container--default.select2-container--below, span.select2.select2-container.select2-container--default {
      width: 300px !important;
      margin: 10px auto !important;
      text-align: left;
        font-size: 13px;
        color: #696969;
        font-weight: bold;
  }
  .country span.select2.select2-container.select2-container--default.select2-container--focus,span.select2.select2-container.select2-container--default.select2-container--below, span.select2.select2-container.select2-container--default{
     width: 140px !important;
  }
  .country{    padding-top: 44px;}
  .navbar-right{
      float: none !important;
      margin-right: -15px !important;
    }
    .nav>li>a:focus, .nav>li>a:hover {
      text-decoration: none;
      background-color: #2bb1e9;
  }
  input.file-upload.up_file{
    width: 50px;
    visibility: hidden;
  }
  .ui-autocomplete {
    max-height: 100px;
    overflow-y: auto;   /* prevent horizontal scrollbar */
    overflow-x: hidden; /* add padding to account for vertical scrollbar */
    z-index:1000 !important;
  }
</style>