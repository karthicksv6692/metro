<?php 
	if ($this->session->userdata('pin_obj') != null) {
		$obj = unserialize($_SESSION['pin_obj']);
		$pin_number = $obj->getPinNo();
	}else{$pin_number = "";}
?>
<div class="container-fluid">
		<div class="page-header">
			<h3>Enter The Pin - Registration</h3>
		</div>
		<div class="page-content">
			<section class="content bgcolor-1">
				<form method="POST" action="" class="common_form">

					<div class="dkinput-txtflds simple_form svk_simple_form">
						<span class="input input--nao">
							<input type="text" class="input__field input__field--nao" name="pin_no" value="<?php if(set_value('pin_no') != null){echo set_value('pin_no');}else{echo $pin_number;} ?>">
							<label class="input__label input__label--nao" for="input-1">
								<span class="input__label-content input__label-content--nao">Enter The Pin Number</span>
							</label>
							<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
								<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
							</svg>
						</span>		
						<?php echo form_error('pin_no','<span class="help-block form-error">', '</span>'); ?>				
					</div>
					<input type="hidden" name="pin_page" value="asdf">
					<div class="row bottom_buttons">
						<div class="next_buton text-center pull-left"><a  onclick="$(this).closest('form').submit()">Next <i class="fa fa-arrow-circle-o-right font_arrow"></i></a></div>
					</div>
				</form>
			</section>
		</div>
	</div>