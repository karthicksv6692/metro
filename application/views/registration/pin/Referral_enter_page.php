<?php  //print_r($empty_obj[0]->getUserPurchaseId());

	if( isset($user) ){
		$u_name = $user->getUserName();
	}else{$u_name = "";}

	if( isset($user) ){
		$f_name = $user->getFirstName();
	}else{$f_name = "";}

?>
<?php 
	if ($this->session->userdata('user_purchase_obj') != null) {
		if(! is_integer($_SESSION['user_purchase_obj']['user_purchase_sponcer_user_purchase_id'])){
			$user_obj = unserialize($_SESSION['user_purchase_obj']['user_purchase_sponcer_user_purchase_id']);
			if($user_obj != null){
				$user_name = $user_obj->getUserName();
				$first_name = $user_obj->getFirstName();
			}else{$user_name = "";$first_name = "";}
		}else{$user_name = "";$first_name = "";}
	}else{$user_name = "";$first_name = "";}
?>

<div class="container-fluid">
	<div class="user-header">
		<h3>Sponcer Details</h3>
	</div>
	<ul class="nav nav-tabs user_module_tab">
		<li class="active"><a data-toggle="tab" href="s_refferal_detail.php"><i class="fa  fa-paper-plane-o font_icon"></i> Refferal Details</a></li>
		<li><a data-toggle="tab" href="s_product_detail.php"><i class="fa fa-angle-double-down font_icon"></i> Product Details</a></li>
		<li><a data-toggle="tab" href="#"><i class="fa fa-user font_icon"></i>Personal Details</a></li>
		<li><a data-toggle="tab" href="#"><i class="fa fa-building font_icon"></i> Bank Details</a></li>
		<li><a data-toggle="tab" href="#"><i class="fa  fa-paperclip font_icon"></i> Attachments</a></li>
		<li><a data-toggle="tab" href="#"><i class="fa   fa-lock font_icon"></i> Security</a></li>
	</ul>
	<form method="post" action="<?php echo base_url('index.php/Registration/ref_detail'); ?>">
		<input name="obj" type="hidden" value="<?php if(isset($user)){ echo json_encode($user); } ?>">
		<div class="tab-contentt">
				<div id="#" class="tab-panee">
					<p class="refferal_tab_head"><i class="fa fa-pencil fon_refeer"></i>Refferal Details</p>
				</div>
				
				<form id="formstreet" class="has-validation-callback">
					<div class="referal_tab_detail1">
							<div class="col-md-6 col-sm-6 ref_col_id pull-left row">
								<p class="refeeral_id">Sponcer User Name : <a><?php echo $u_name ?></a></p>
							</div>
							<div class="col-md-6 col-sm-6 ref_col pull-left row">
								<span class="input input--nao dkinput-txtflds">
									<input class="input__field input__field--nao" type="text" value="<?php echo $f_name; ?>" disabled>
									<label class="input__label input__label--nao" for="input-1">
										<span class="input__label-content input__label-content--nao">Sponcer First Name </span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
									</svg>
								</span>							
							</div>
							<div class="col-md-6 col-sm-6 ref_col pull-left row">
								<!-- <div class="dkinput-txtflds n_max-width form-group"> -->
									<span class="input input--nao dkinput-txtflds">
			                            <input class="input__field input__field--nao user_name" type="text" id="tags" name="user_name"  value="<?php if(set_value('user_name') != null){echo set_value('user_name');}else{echo $user_name;} ?>">
			                            <label class="input__label input__label--nao" for="r_password">
			                            	<span class="input__label-content input__label-content--nao"> Placement ID </span>
			                            </label>
			                            <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
			                            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></path></svg>
			                        </span>
									<?php echo form_error('user_name','<span class="help-block form-error">', '</span>'); ?>
								<!-- </div> -->
							</div>
							<div class="col-md-6 col-sm-6 ref_col pull-left row ">
								<span class="input input--nao dkinput-txtflds">
									<input class="input__field input__field--nao fname" type="text" id="input-1" value="<?php if(set_value('f_name') != null){echo set_value('f_name');}else{echo $first_name;} ?>">
									<label class="input__label input__label--nao fname_lbl active" for="input-1">
										<span class="input__label-content input__label-content--nao">Placement Holder Name </span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
									</svg>
								</span>							
							</div>

							<div class="col-md-12 col-lg-12 col-sm-12 j_frm_pad pull-left row">	
								<div class="form-group forge_reg_radio ">
									<label> Position</label>
									<label class="radio-inline radio-in">
									  <input type="radio" class="radio_but" name="optradio" value="0">Left
									</label>
									<label class="radio-inline radio-in">
									  <input type="radio" class="radio_but" name="optradio" value="1">Right
									</label>
									<label class="radio-inline">
									  <input type="radio" class="radio_but" name="optradio"  value="2" checked>Auto
									</label>
								</div>
							</div>
							<input type="hidden" name="ref_page" value="asdf">
					</div>
					<div class="next_buton text-center pull-left"><a onclick="$(this).closest('form').submit();">Next <i class="fa fa-arrow-circle-o-right font_arrow"></i></a></div>
				</form>
		</div>
	</form>
</div>
<?php 
	//print_r($empty_obj);
	$pincode_array = array();
	$fname_array = array();
	foreach ($empty_obj as $list) {  
		array_push($pincode_array, $list->getUserId()->getUserName());

		$a[$list->getUserId()->getUserName()] = $list->getUserId()->getFirstName();

		if(!in_array($a,$fname_array)){
			array_push($fname_array, $a);
		}
	}
	function js_str($s) { return '"' . addcslashes($s, "\0..\37\"\\") . '"'; }
	function js_array($array) { 
	$temp = array_map('js_str', $array);
	return '[' . implode(',', $temp) . ']';
	}
	$name = array_pop($fname_array);
?>


<script>
  $( function() {
    $( "#tags" ).autocomplete({
      source: <?php echo  js_array($pincode_array);?>
    });
  } );
  $('.user_name').blur(function(){
  	var uname = $(this).val();

  	if(uname != null){
  		$.ajax({
	  		type: "POST",
	        url: "<?php echo base_url(); ?>index.php/Registration/get_fname",
	        data: {'uname':uname},
	        success:  function (data) {
	        	if(data != 0){
              $('.fname_lbl').addClass('active');
              $('.fname').val(data);
	        	}
	      	}
	  	})
  	}
  	
  })
</script>