
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style_shopping.css'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/flexslider.css'); ?>">
  
  <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.flexslider.js'); ?>"></script>
 
  <!--Social link-->
  <section>
    <div class="dhinatechnologies_metrokings_socialbg">
      <div class="container">
        <div class="row">
          <div class="col-lg-7 col-md-6 col-sm-12 pl0 pr0 padd">
            <div class="cent">
              <span class="dhinatechnologies_metrokings_social_ph"> <a href="tel:4651264811" class="faa-parent animated-hover"> <i class="fa fa-phone social_ico faa-horizontal"></i> 4651264811 </a> </span><span class="dhinatechnologies_metrokings_social_ph"> <a href="mailto:support@metrokings.biz" class="faa-parent animated-hover"> <i class="fa fa-envelope social_ico env faa-horizontal"></i> support@metrokings.biz </a> </span>
            </div>
          </div>
          <div class="col-lg-5 col-md-6 col-sm-12 pl0 pr0 padd">
            <div  class="pull-right soc cent">
              <span class="dhinatechnologies_metrokings_social_login"> <a href="<?php echo base_url('index.php'); ?>"> Login </a> </span>
              <span class="dhinatechnologies_metrokings_social_divider"></span>
              <span class="dhinatechnologies_metrokings_social_register"> <a href="<?php echo base_url('index.php'); ?>"> Enroll </a> </span>
              <span class="dhinatechnologies_metrokings_social_divider"></span>
              <span><a href="#"><i class="fa fa-facebook dhinatechnologies_metrokings_social_icon fb"></i></a></span>
              <span><a href="#"><i class="fa fa-google-plus dhinatechnologies_metrokings_social_icon"></i></a></span>
              <span><a href="#"><i class="fa fa-youtube dhinatechnologies_metrokings_social_icon"></i></a></span>
              <span><a href="#"><i class="fa fa-twitter dhinatechnologies_metrokings_social_icon"></i></a></span>
              <span class="gj_mnu_cart">
                <?php $tot_count = 0; if(isset($_SESSION)){ ?>  
                  <?php if(isset($_SESSION['cart_items'])){ ?>  
                    <?php if($_SESSION['cart_items'] != null){ ?> 
                        <a href="<?php echo base_url('index.php/shopping/cart'); ?>" class="gj_socialicons gj_bad_pos"><i class="fa fa-shopping-bag gj_soc_ico"></i><span class="gj_badge"><?php echo count($_SESSION['cart_items']); ?></span></a>
                    <?php } else{?>
                        <a href="<?php echo base_url('index.php/shopping/cart'); ?>" class="gj_socialicons gj_bad_pos"><i class="fa fa-shopping-bag gj_soc_ico"></i><span class="gj_badge">0</span></a>
                    <?php } ?>
                  <?php } else{?>
                      <a href="<?php echo base_url('index.php/shopping/cart'); ?>" class="gj_socialicons gj_bad_pos"><i class="fa fa-shopping-bag gj_soc_ico"></i><span class="gj_badge">0</span></a>
                  <?php } ?>
                <?php } else{?>
                    <a href="<?php echo base_url('index.php/shopping/cart'); ?>" class="gj_socialicons gj_bad_pos"><i class="fa fa-shopping-bag gj_soc_ico"></i><span class="gj_badge">0</span></a>
                <?php } ?>
               <?php //echo $_SESSION['cart_items'][$ct]['cart_tot_pv'] ?>
                <div class="gj_cart_det table-responsive">
                  <table id="gj_cart_det_tl" class="table table-striped table-bordered">
                      <thead>
                          <tr class="gj_sp_ct">
                            <th colspan="3" class="text-left"><span>Shopping Cart</span>
                              <!-- <button type="button" class="gj_ety_ct" onclick="remove_all_from_cart()">Empty Cart</button> -->
                            </th>
                            <th colspan="2" class="text-left"><span>Total PV :</span>
                             

                                <?php $tot_price = 0; if(isset($_SESSION)){ ?>  
                                  <?php $vv =0; if(isset($_SESSION['cart_items'])){ ?>  
                                    <?php if($_SESSION['cart_items'] != null){ ?> 
                                        <?php foreach($_SESSION['cart_items'] as $cart){?>
                                            <?php $vv += $cart['user_pv'] ?>
                                        <?php } ?>
                                        <span class="tot_pv_tp"><?php echo $vv; ?> PV</span>
                                    <?php }else{?> 
                                      <span class="tot_pv_tp"><?php echo $vv; ?> PV</span>
                                    <?php } ?>
                                  <?php }else{?> 
                                    <span class="tot_pv_tp"><?php echo $vv; ?> PV</span>
                                  <?php } ?>
                                <?php }else{?> 
                                  <span class="tot_pv_tp">0 PV</span>
                                <?php } ?>

                                  
                              <!-- <button type="button" class="gj_ety_ct" onclick="remove_all_from_cart()">Empty Cart</button> -->
                            </th>
                          </tr>
                          <tr>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Action</th>
                          </tr>
                      </thead>
                      <tbody class="cart_items_body">
                      <?php $tot_price = 0; if(isset($_SESSION)){ ?>  
                        <?php if(isset($_SESSION['cart_items'])){ ?>  
                          <?php if($_SESSION['cart_items'] != null){ ?> 
                              <?php foreach($_SESSION['cart_items'] as $cart){$tot_price += $cart['user_price'];  ?>
                                <tr class="crt_prd_<?php echo $cart['product_id'] ?>">
                                  <td><?php echo $cart['product_code'] ?></td>
                                  <td><?php echo $cart['product_name'] ?></td>
                                  <td class="prcqty_<?php echo $cart['product_id'] ?>"><?php echo $cart['user_quantity'] ?></td>
                                  <td class="prc_<?php echo $cart['product_id'] ?>"><?php echo $cart['user_price'] ?></td>
                                  <td><button type="button" class="gj_cart_close" onclick="remove_from_cart(<?php echo $cart['product_id'] ?>,<?php echo $cart['user_quantity'] ?>)"><i class="fa fa-close"></i></button></td>
                                </tr>
                                
                              <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr class="top_crt_tot">
                                  <td colspan="4" style="text-align: left;">
                                    <a href="<?php echo base_url('index.php/shopping/cart'); ?>"><input type="button" value="Checkout"></a>
                                  </td>
                                  <td colspan="" class="text-right"><span class="gj_c_total">Total :</span> <span class="tp_ct_tt"><?php echo $tot_price; ?></span>.00</td>
                                </tr>
                                </tfoot>

                            <?php }else{?>
                            <tr class="empty_value">
                              <td  colspan="5">Cart is Empty</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr class="top_crt_tot">
                                  <td colspan="4" style="text-align: left;">
                                    <a href="<?php echo base_url('index.php/shopping/cart'); ?>"><input type="button" value="Checkout"></a>
                                  </td>
                                  <td colspan="" class="text-right"><span class="gj_c_total">Total : </span> <?php echo $tot_price; ?><span class="tp_ct_tt"></span>.00</td>
                                </tr>
                        </tfoot>
                            <?php } ?>
                                
                        <?php } ?>
                      <?php } ?>
                      
                  </table>
                </div>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--MENU-->
  <section>
    <nav class="navbar navbar-inverse mnu_cr">
      <div class="container">
        <div class="row">
          <div class="navbar-header col-lg-4 col-md-4 col-sm-12  col-xs-12 pl0 pr0">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>                        
            </button>
            <div class="gj_logo_div">
              <a href="index.html"><img src="<?php echo base_url('assets/images/dhinatechnologies_metrokings_logo.png'); ?>" class="img-responsive gj_logo_img"></a>
            </div>
          </div>
          <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12  pl0 pr0 high">
            <div class="collapse navbar-collapse" id="myNavbar">
              <ul class="nav navbar-nav">
                <li><a href="http://metrokings.biz/"> Home </a></li>
                <li class="active"><a href="#"> Products </a></li>
                <li><a href="http://metrokings.biz/events"> Events </a></li>
                <li><a href="http://metrokings.biz/blogs"> Blogs </a></li>
                <li><a href="http://metrokings.biz/magazine"> Magazine </a></li>
                <li><a href="http://metrokings.biz/about-us"> About Us </a></li>
                <li><a href="http://metrokings.biz/contact-us"> Contact Us </a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      </nav>
  </section>

    <!-- CAROSEL -->
  <section>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">

        <div class="item active">
          <img src="<?php echo base_url('assets/images/dhinatechnologies_metrokings_Product_banner.jpg'); ?>" alt="banner" class="img-responsive wow zoomInRight">
        </div>
  
      </div>
      <!-- <a class="left carousel-control gj_left_icon" href="#myCarousel" data-slide="prev">
           <span class="fa fa-chevron-left"></span>
           <span class="sr-only">Previous</span>
         </a>
         <a class="right carousel-control gj_right_icon" href="#myCarousel" data-slide="next">
           <span class="fa fa-chevron-right"></span>
           <span class="sr-only">Next</span>
        </a> -->
    </div>
  </section>