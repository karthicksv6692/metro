
	<script type="text/javascript" src="<?php echo base_url('assets/')?>js/wow_and_metismenu.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/')?>js/dkscript.js"></script>
	<script type="text/javascript">
       	new WOW().init();
			$(function () {
			   $('#menu').metisMenu();
			});
			</script>			
			<script type="text/javascript" src="<?php echo base_url('assets/')?>js/(form)jquery.min.js">				
			</script>		
			<script type="text/javascript" src="<?php echo base_url('assets/')?>js/form_validator/jquery.form-validator.js"></script>
			<script type="text/javascript">
			$.noConflict();
			jQuery( document ).ready(function( $ ) {
			$.validate({
				form:'#formstreet',
				modules :'logic,security,date',
			    onModulesLoaded : function() {
			      console.log('Modules are loaded successfully');
			  	}
			});
		});
	</script>
	<script type="text/javascript" src="<?php echo base_url('assets/')?>js/toast.js"></script>
	
<script>
	$(document).ready(function() {
	    var readURL = function(input) {
	        if (input.files && input.files[0]) {
	            var reader = new FileReader();

	            reader.onload = function (e) {
	                $('.profile-pic').attr('src', e.target.result);
	            }
	    
	            reader.readAsDataURL(input.files[0]);
	        }
	    }

	    $(".file-upload").on('change', function(){
	        readURL(this);
	    });
	    
	    $(".profile-pic").on('click', function() {
	       $(".file-upload").click();
	    });
	});
	$('#sandbox-container input').datepicker({
		format: 'yyyy/mm/dd',
	});
</script>
</body>
</html>
