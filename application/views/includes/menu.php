<div class="menu-wrap">
  <nav class="sidebar-nav">
        <ul class="metismenu" id="menu">
        <li class="bold user_crud">
            <a class="has-arrow ripple" href="#" aria-expanded="false">
                <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                <span class="sidebar-nav-item ripple">My Account</span>
            </a>
            <ul aria-expanded="false" class="collapse" style="height: 0px;">
              <li class="wow fadeInLeft ripple edit" data-wow-duration="0.2s" style="visibility: visible; animation-duration: 0.2s; animation-name: fadeInLeft;">
                <a href="<?php echo base_url('index.php/login/dashboard'); ?>">Home</a>
              </li>
              <li class="wow fadeInLeft ripple delete" data-wow-duration="0.2s" style="visibility: visible; animation-duration: 0.2s; animation-name: fadeInLeft;">
                  <a href="<?php echo base_url('index.php/login/login_user'); ?>">Profile</a>
              </li>
              <li class="wow fadeInLeft ripple s_view" data-wow-duration="0.2s" style="visibility: visible; animation-duration: 0.2s; animation-name: fadeInLeft;">
                  <a href="<?php echo base_url('index.php/login/change_password'); ?>">Change Password</a>
              </li>
                 
            </ul>
          </li>

            <?php if($_SESSION['role_id'] == 7){ ?>
              <li class="bold user_crud">
                <a class="has-arrow ripple" href="<?php echo base_url('index.php/package_purchase/tree'); ?>" aria-expanded="false">
                    <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                    <span class="sidebar-nav-item ripple">Geanology</span>
                </a>
              </li>
              <li class="bold user_crud">
                  <a class="has-arrow ripple" href="#" aria-expanded="false">
                    <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                    <span class="sidebar-nav-item ripple">Income</span>
                  </a>
                  <ul aria-expanded="false" class="collapse" style="height: 0px;">
                    <li class="wow fadeInLeft ripple edit" data-wow-duration="0.2s" style="visibility: visible; animation-duration: 0.2s; animation-name: fadeInLeft;">
                      <a href="<?php echo base_url('index.php/level_income'); ?>">Level Income</a>
                    </li>
                   
                  </ul>
              </li>
            <?php }else{ ?>
              <li class="bold user_crud">
                <!-- <a class="has-arrow ripple" href="<?php echo base_url('index.php/package_purchase/tree'); ?>" aria-expanded="false"> -->
                <a class="has-arrow ripple" href="#" aria-expanded="false">
                    <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                    <span class="sidebar-nav-item ripple">Geanology</span>
                </a>
                <ul aria-expanded="false" class="collapse" style="height: 0px;">
                  <li class="wow fadeInLeft ripple edit" data-wow-duration="0.2s" style="visibility: visible; animation-duration: 0.2s; animation-name: fadeInLeft;">
                    <a href="<?php echo base_url('index.php/package_purchase/tree'); ?>">Geanology</a>
                  </li>
                  <li class="wow fadeInLeft ripple delete" data-wow-duration="0.2s" style="visibility: visible; animation-duration: 0.2s; animation-name: fadeInLeft;">
                      <a href="<?php echo base_url('index.php/package_purchase/downlist'); ?>">Downlist</a>
                  </li>
                  
                </ul>
              </li>
            <?php } ?>

          <?php if($_SESSION['role_id'] == 7){ ?>
            <li class="bold user_crud">
              <a class="has-arrow ripple" href="#" aria-expanded="false">
                  <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                  <span class="sidebar-nav-item ripple">Distributor</span>
              </a>
              <ul aria-expanded="false" class="collapse" style="height: 0px;">
                <li class="wow fadeInLeft ripple edit" data-wow-duration="0.2s" style="visibility: visible; animation-duration: 0.2s; animation-name: fadeInLeft;">
                  <a href="<?php echo base_url('index.php/package_purchase/distributor'); ?>">Distributor List</a>
                </li>
                <li class="wow fadeInLeft ripple edit" data-wow-duration="0.2s" style="visibility: visible; animation-duration: 0.2s; animation-name: fadeInLeft;">
                  <a href="<?php echo base_url('index.php/common_controller/user_search_with_ad_details'); ?>">Distributor Search</a>
                </li>
              </ul>
            </li>
          <?php  } ?>

          <li class="bold user_crud">
            <a class="has-arrow ripple" href="<?php echo base_url('index.php/pin/pin_store/pin_store_view'); ?>" aria-expanded="false">
                <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                <span class="sidebar-nav-item ripple">Pin Store</span>
            </a>
          </li>


           <?php if($_SESSION['role_id'] == 7){ ?>
            <li class="bold user_crud">
              <a class="has-arrow ripple" href="<?php echo base_url('index.php/user/stock_pointer'); ?>" aria-expanded="false">
                  <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                  <span class="sidebar-nav-item ripple">Stock Pointer</span>
              </a>
            </li>
            <li class="bold user_crud">
              <a class="has-arrow ripple" href="<?php echo base_url('index.php/payout_demo/all_payout'); ?>" aria-expanded="false">
                  <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                  <span class="sidebar-nav-item ripple">Latest Payouts</span>
              </a>
            </li>
            <li class="bold user_crud">
              <a class="has-arrow ripple" href="#" aria-expanded="false">
                  <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                  <span class="sidebar-nav-item ripple">Messages</span>
              </a>
                <ul aria-expanded="false" class="collapse" style="height: 0px;">
                  <li class="wow fadeInLeft ripple edit" data-wow-duration="0.2s" style="visibility: visible; animation-duration: 0.2s; animation-name: fadeInLeft;">
                    <a href="<?php echo base_url('index.php/common/messages_crud/mobile_message'); ?>">Mobile Message</a>
                  </li>
                  <li class="wow fadeInLeft ripple edit" data-wow-duration="0.2s" style="visibility: visible; animation-duration: 0.2s; animation-name: fadeInLeft;">
                    <a href="<?php echo base_url('index.php/common/messages_crud'); ?>">Message</a>
                  </li>
                </ul>

            </li>
           <?php } ?>
           <?php if($_SESSION['role_id'] != 7){ ?>

            <li class="bold user_crud">
              <a class="has-arrow ripple" href="<?php echo base_url('index.php/common_controller/invoice'); ?>" aria-expanded="false">
                  <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                  <span class="sidebar-nav-item ripple">Invoice</span>
              </a>
            </li>
            <li class="bold user_crud">
              <a class="has-arrow ripple" href="<?php echo base_url('index.php/common/messages_crud'); ?>" aria-expanded="false">
                  <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                  <span class="sidebar-nav-item ripple">Messages</span>
              </a>
            </li>

            <?php } ?>
           
           

          <li class="bold user_crud">
            <a class="has-arrow ripple" href="#" aria-expanded="false">
                <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                <span class="sidebar-nav-item ripple">Payout</span>
            </a>
            <ul aria-expanded="false" class="collapse" style="height: 0px;">
              <?php if($_SESSION['role_id'] != 7){ ?>
              <li class="wow fadeInLeft ripple edit" data-wow-duration="0.2s" style="visibility: visible; animation-duration: 0.2s; animation-name: fadeInLeft;">
                <a href="<?php echo base_url('index.php/payout_demo/payout_view'); ?>">Current Payout</a>
              </li>
              <?php  } ?>
             <?php if($_SESSION['role_id'] == 7){ ?>
                <li class="wow fadeInLeft ripple delete" data-wow-duration="0.2s" style="visibility: visible; animation-duration: 0.2s; animation-name: fadeInLeft;">
                    <a href="<?php echo base_url('index.php/payout_demo/final_abstract'); ?>">Final Payout Abstract</a>
                </li>
                <li class="wow fadeInLeft ripple delete" data-wow-duration="0.2s" style="visibility: visible; animation-duration: 0.2s; animation-name: fadeInLeft;">
                    <a href="<?php echo base_url('index.php/payout_demo/payout_date_wise_admin'); ?>">Payout Abstract</a>
                </li>
              <?php  }else{ ?>

              <li class="wow fadeInLeft ripple delete" data-wow-duration="0.2s" style="visibility: visible; animation-duration: 0.2s; animation-name: fadeInLeft;">
                  <a href="<?php echo base_url('index.php/payout_demo/payout_date_wise'); ?>">Payout Abstract</a>
              </li>
              <li class="wow fadeInLeft ripple s_view" data-wow-duration="0.2s" style="visibility: visible; animation-duration: 0.2s; animation-name: fadeInLeft;">
                  <a href="<?php echo base_url('index.php/payout_demo/payout_result_date_wise'); ?>">Payout Sales </a>
              </li>

              <?php } ?>

              
               
            </ul>
          </li>
        
            <?php if($_SESSION['role_id'] == 7){?>
            <li>
                <a class="has-arrow ripple" href="<?php echo base_url('index.php/Registration/enter_pin') ?>">
                    <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                    <span class="sidebar-nav-item ripple">Registration</span>
                </a>
            </li>
            <?php } ?>
             <li>
                <a class="has-arrow ripple" href="<?php echo base_url('index.php/purchase/enter_pin') ?>">
                    <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                    <span class="sidebar-nav-item ripple">Repurchase</span>
                </a>
            </li>
             
            <li>
                <a class="has-arrow ripple" href="<?php echo base_url('index.php/shopping') ?>">
                    <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                    <span class="sidebar-nav-item ripple">Shopping</span>
                </a>
            </li>

            <?php if($_SESSION['role_id'] == 7){?>
            <li>
                <a class="has-arrow ripple" href="#">
                    <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                    <span class="sidebar-nav-item ripple">Pin</span>
                </a>
                <ul >
                  <li class="wow fadeInLeft ripple delete" data-wow-duration="0.2s" style="visibility: visible; animation-duration: 0.2s; animation-name: fadeInLeft;">
                      <a href="<?php echo base_url('index.php/pin/pin_crud/pin_abstract'); ?>">Pin Payment List</a>
                  </li>
                    <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                        <a href="<?php echo base_url('index.php/pin/pin_crud') ?>">  View  </a>
                    </li>
                    <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                        <a href="<?php echo base_url('index.php/pin/pin_crud/add') ?>">  Add  </a>
                    </li>
                    <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                        <a href="<?php echo base_url('index.php/pin/pin_crud/used') ?>">  Used  </a>
                    </li>
                    <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                        <a href="<?php echo base_url('index.php/pin/pin_crud/un_used') ?>">  Un-used  </a>
                    </li>
                    
                </ul>
            </li>
            <?php }elseif($_SESSION['role_id'] == RT::$default_stock_pointer_id){ ?>
              <li>
                <a class="has-arrow ripple" href="#">
                    <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                    <span class="sidebar-nav-item ripple">Pin</span>
                </a>
                <ul >
                  <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                        <a href="<?php echo base_url('index.php/pin/pin_crud/add') ?>">  Add  </a>
                    </li>
                </ul>
              </li>
            <?php } ?>
            
            
            <?php if($_SESSION['role_id'] == 7){?>
              <li>
                  <a class="has-arrow ripple" href="#">
                      <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                      <span class="sidebar-nav-item ripple">Pincode</span>
                  </a>
                  <ul >
                      <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                          <a href="<?php echo base_url('index.php/common/pincode_crud') ?>">  View  </a>
                      </li>
                      <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                          <a href="<?php echo base_url('index.php/common/pincode_crud/add') ?>">  Add  </a>
                      </li>
                      <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                          <a href="<?php echo base_url('index.php/common/pincode_crud/edit') ?>">  Edit  </a>
                      </li>
                      <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                          <a href="<?php echo base_url('index.php/common/pincode_crud/s_view') ?>">  S_view  </a>
                      </li>
                      <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                          <a href="<?php echo base_url('index.php/common/pincode_crud/delete') ?>">  Delete  </a>
                      </li>
                  </ul>
              </li>
            <?php } ?>
            <?php if($_SESSION['role_id'] == 7){?>
             <li>
                <a class="has-arrow ripple" href="#">
                    <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                    <span class="sidebar-nav-item ripple">Country</span>
                </a>
                <ul >
                    <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                        <a href="<?php echo base_url('index.php/common/country_crud') ?>">  View  </a>
                    </li>
                    <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                        <a href="<?php echo base_url('index.php/common/country_crud/add') ?>">  Add  </a>
                    </li>
                    <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                        <a href="<?php echo base_url('index.php/common/country_crud/edit') ?>">  Edit  </a>
                    </li>
                    <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                        <a href="<?php echo base_url('index.php/common/country_crud/s_view') ?>">  S_view  </a>
                    </li>
                    <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                        <a href="<?php echo base_url('index.php/common/country_crud/delete') ?>">  Delete  </a>
                    </li>
                </ul>
            </li>
            <?php } ?>
            
            


            <!-- cus_mesnu STart -->
            <?php if($_SESSION['role_id'] == 7){?>
                <li>
                    <a class="has-arrow ripple" href="#">
                        <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                        <span class="sidebar-nav-item ripple">Bank</span>
                    </a>
                    <ul >
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/account_details/bank_crud') ?>">  View  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/account_details/bank_crud/add') ?>">  Add  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/account_details/bank_crud/edit') ?>">  Edit  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/account_details/bank_crud/s_view') ?>">  S_view  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/account_details/bank_crud/delete') ?>">  Delete  </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a class="has-arrow ripple" href="#">
                        <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                        <span class="sidebar-nav-item ripple">Module</span>
                    </a>
                    <ul >
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/user/Module_crud') ?>">  View  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/user/Module_crud/add') ?>">  Add  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/user/Module_crud/edit') ?>">  Edit  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/user/Module_crud/s_view') ?>">  S_view  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/user/Module_crud/delete') ?>">  Delete  </a>
                        </li>
                    </ul>
                </li>
               
                <li>
                    <a class="has-arrow ripple" href="#">
                        <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                        <span class="sidebar-nav-item ripple">Access</span>
                    </a>
                    <ul >
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/user/Access_crud') ?>">  View  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/user/Access_crud/add') ?>">  Add  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/user/Access_crud/edit') ?>">  Edit  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/user/Access_crud/s_view') ?>">  S_view  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/user/Access_crud/delete') ?>">  Delete  </a>
                        </li>
                    </ul>
                </li>
               
                <li>
                    <a class="has-arrow ripple" href="#">
                        <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                        <span class="sidebar-nav-item ripple">User</span>
                    </a>
                    <ul >
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/user/User_crud') ?>">  View  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/user/User_crud/add') ?>">  Add  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/user/User_crud/edit') ?>">  Edit  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/user/User_crud/s_view') ?>">  S_view  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/user/User_crud/delete') ?>">  Delete  </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="has-arrow ripple" href="#">
                        <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                        <span class="sidebar-nav-item ripple">User Role</span>
                    </a>
                    <ul >
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/user/User_role_crud') ?>">  View  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/user/User_role_crud/add') ?>">  Add  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/user/User_role_crud/edit') ?>">  Edit  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/user/User_role_crud/s_view') ?>">  S_view  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/user/User_role_crud/delete') ?>">  Delete  </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a class="has-arrow ripple" href="#">
                        <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                        <span class="sidebar-nav-item ripple">Category</span>
                    </a>
                    <ul >
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/products/category_crud') ?>">  View  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/products/category_crud/add') ?>">  Add  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/products/category_crud/edit') ?>">  Edit  </a>
                        </li>
                        
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/products/category_crud/delete') ?>">  Delete  </a>
                        </li>
                    </a>
                   </ul>
                </li>

                <li>
                    <a class="has-arrow ripple" href="#">
                        <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                        <span class="sidebar-nav-item ripple">Vendor</span>
                    </a>
                    <ul >
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/products/Vendor_crud') ?>">  View  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/products/Vendor_crud/add') ?>">  Add  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/products/Vendor_crud/edit') ?>">  Edit  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/products/Vendor_crud/s_view') ?>">  S_view  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/products/Vendor_crud/delete') ?>">  Delete  </a>
                        </li>
                    </a>
                   </ul>
                </li>

                
               <li>
                    <a class="has-arrow ripple" href="#">
                        <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                        <span class="sidebar-nav-item ripple">Packages</span>
                    </a>
                    <ul >
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/products/Packages_crud') ?>">  View  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/products/Packages_crud/add') ?>">  Add  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/products/Packages_crud/edit') ?>">  Edit  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/products/Packages_crud/s_view') ?>">  S_view  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/products/Packages_crud/delete') ?>">  Delete  </a>
                        </li>
                    </ul>
                </li>

                  <li>
                    <a class="has-arrow ripple" href="#">
                        <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                        <span class="sidebar-nav-item ripple">Product</span>
                    </a>
                    <ul >
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/products/Product_crud') ?>">  View  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/products/Product_crud/add') ?>">  Add  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/products/Product_crud/edit') ?>">  Edit  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/products/Product_crud/s_view') ?>">  S_view  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/products/Product_crud/delete') ?>">  Delete  </a>
                       </li>
                    </a>
                   </ul>
                </li>

                <li>
                    <a class="has-arrow ripple" href="#">
                        <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                        <span class="sidebar-nav-item ripple">Package Product</span>
                    </a>
                    <ul >
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/products/Package_product_crud') ?>">  View  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/products/Package_product_crud/add') ?>">  Add  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/products/Package_product_crud/edit') ?>">  Edit  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/products/Package_product_crud/s_view') ?>">  S_view  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/products/Package_product_crud/delete') ?>">  Delete  </a>
                        </li>
                    </a>
                   </ul>
                </li>
                <li>
                    <a class="has-arrow ripple" href="<?php echo base_url('index.php/payout_demo') ?>">
                        <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                        <span class="sidebar-nav-item ripple">Payout</span>
                    </a>
                </li>
                <li>
                    <a class="has-arrow ripple" href="#">
                        <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                        <span class="sidebar-nav-item ripple">Purchase Request</span>
                    </a>
                    <ul >
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/stock/Purchase_request_crud') ?>">  View  </a>
                        </li>
                        <li class="wow fadeInLeft ripple" data-wow-duration="0.2s">
                            <a href="<?php echo base_url('index.php/stock/Purchase_request_crud/add') ?>">  Add  </a>
                        </li>
                        
                   </ul>
                </li>
            <?php } ?>
            <!-- cus_menu_end  -->
           <?php 
                // $utill_obj = new Common_utill();
                // $menu = $utill_obj->create_menu_html($_SESSION['user_id']);

                // if($menu != null){
                    // echo $menu; 
                // }
            ?>
             <!--  <li> 
                <a class="has-arrow ripple" href="<?php echo base_url('index.php/common/messages_crud'); ?>">
                    <span class="sidebar-nav-item-icon fa fa-user ripple"></span>
                    <span class="sidebar-nav-item ripple">Messages</span>
                </a>
            </li> -->
        </ul>
    </nav>
  <button class="close-button" id="close-button">Close Menu</button>
  <div class="morph-shape" id="morph-shape" data-morph-open="M-1,0h101c0,0,0-1,0,395c0,404,0,405,0,405H-1V0z">
    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 100 800" preserveAspectRatio="none">
      <path d="M-1,0h101c0,0-97.833,153.603-97.833,396.167C2.167,627.579,100,800,100,800H-1V0z"/>
    </svg>
  </div>
</div>