<!DOCTYPE html>
<html lang="en" class="no-js">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
    <title>Admin Template</title>
    <meta name="description" content="Admin Template Designed by Dhina Technologies Developers" />
    <meta name="keywords" content="dhinatechnologies.com | Admin Template" />
    <meta name="author" content="dhaneesh kumar" />
    <!--[if IE]>
      <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/')?>css/bootstrap_font-awesome_animate.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/')?>css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/')?>css/validator-theme-default.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/')?>css/admin-style.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/')?>css/toast.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/')?>css/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/')?>css/s_style.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/')?>css/svk_style.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/')?>css/bootstrap-datepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/')?>css/jquery.dataTables.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/')?>css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/')?>css/gj_jssor.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/')?>css/editor.css" />

  <!-- user only in live server <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->

  <script type="text/javascript" src="<?php echo base_url('assets/')?>js/snap.svg-min.js"></script>
  <script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
  
  <!-- <script type="text/javascript" src="<?php //echo base_url('assets/js/jquery-1.11.2.min.js')?>"></script> -->

  <script type="text/javascript" src="<?php echo base_url('assets/')?>js/tether.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/')?>js/bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/')?>js/jquery-ui.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/')?>js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/')?>js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/')?>js/select2.min.js"></script>
    <!-- From godwin -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/gj_appscript.js')?>"></script>
    
    <script type="text/javascript" src="<?php echo base_url('assets/js/jssor.slider-23.1.5.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/gj_jssor.js')?>"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/')?>css/package_styles.css">
    <!--  form validator  -->
    <script type="text/javascript" src="<?php echo base_url('assets/')?>js/form_validator/jquery.form-validator.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/')?>js/form_validator/logic.js"></script>
    
   
    <script type="text/javascript" src="<?php echo base_url('assets/')?>js/editor.js"></script>
    <!--  -->
  </head>
      <?php  if(isset($_SESSION['success'])){ ?>
            <script type="text/javascript">
                $(document).ready(function(){
                    toast({
                      message: "<?php echo $_SESSION['success'];?>",
                      displayLength: 3000,
                      className: 'success',
                    });
                })
            </script>
        <?php } ?>
        <?php  if(isset($_SESSION['error'])){ ?>
            <script type="text/javascript">
                $(document).ready(function(){
                    toast({
                      message: "<?php echo $_SESSION['error'];?>",
                      displayLength: 30000,
                      className: 'error',
                    });
                })
            </script>
        <?php } ?>
        <?php  if(isset($_SESSION['warning'])){ ?>
            <script type="text/javascript">
                $(document).ready(function(){
                    toast({
                      message: "<?php echo $_SESSION['warning'];?>",
                      displayLength: 3000,
                      className: 'error',
                    });
                })
            </script>
        <?php } ?>
    