<!-- Footer -->
	<section> 
		<div class="dhinatechnologies_metrokings_Footer_bg1">
			<div class="container">
				<div class="row foo_block">
					<div class="col-lg-3 col-md-6 col-sm-6 pull-left">
						<div class="dhinatechnologies_metrokings_Footer_mar2 wow slideInDown">
							<p class="dhinatechnologies_metrokings_Footer_hd"> Navigation </p>
							<p class="navig"> <a href="#"> Home </a> </p>
							<p class="navig"> <a href="#"> About Us </a> </p> 
							<p class="navig"> <a href="#"> Certification </a> </p>
							<p class="navig"> <a href="#"> Products </a> </p>
							<p class="navig"> <a href="#"> Biz.plan </a> </p>
							<p class="navig"> <a href="#"> Contact Us </a> </p>
							<p class="navig"> <a href="#"> Register </a> </p>
							<p class="navig"> <a href="#"> Login </a> </p>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6 pull-left">
						<div class="dhinatechnologies_metrokings_Footer_mar2 wow slideInUp">
							<p class="dhinatechnologies_metrokings_Footer_hd"> Our Address </p>
							<p class="dhinatechnologies_metrokings_Footer_admin"> Registered office: </p>
							<p class="dhinatechnologies_metrokings_Footer_content"> 15/64,Vattavilai, </p>
							<p class="dhinatechnologies_metrokings_Footer_content"> Pacode - 629 168, </p>
							<p class="dhinatechnologies_metrokings_Footer_content dhinatechnologies_metrokings_Footer_mar1"> Kanyakumari District </p>
							<p class="dhinatechnologies_metrokings_Footer_admin"> Administrative Office: </p>
							<p class="dhinatechnologies_metrokings_Footer_content"> Kamal Building, </p>
							<p class="dhinatechnologies_metrokings_Footer_content"> Kuzhithurai – 629163 </p>
							<p class="dhinatechnologies_metrokings_Footer_content"> Kanyakumari District </p>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6 pull-left">
						<div class="dhinatechnologies_metrokings_Footer_mar2 foo_float wow slideInDown">
							<p class="dhinatechnologies_metrokings_Footer_hd"> Our Blogs </p>
							<p class="dhinatechnologies_metrokings_Footer_content"> MetroKings Connections </p>
							<p class="dhinatechnologies_metrokings_Footer_content"> Achieve Magazine </p>
							<p class="dhinatechnologies_metrokings_Footer_content"> MetroKings Insider </p>
							<p class="dhinatechnologies_metrokings_Footer_content"> MetroKings One by One </p>
							<p class="dhinatechnologies_metrokings_Footer_content"> MetroKings.net </p>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6 pull-left">
						<div class="dhinatechnologies_metrokings_Footer_mar2 foo_float wow slideInUp">
							<p class="dhinatechnologies_metrokings_Footer_hd"> Connect with Us </p>
							<p class="navig"><a href="#"> Facebook </a> </p>
							<p class="navig"><a href="#"> Twitter </a> </p>
							<p class="navig"><a href="#"> Linkedin </a> </p>
							<p class="navig"><a href="#"> Instagram </a> </p>
							<p class="navig"><a href="#"> Google+ </a> </p>
							<p class="navig"><a href="#"> Skype </a> </p>
						</div>
					</div>
				</div>
				<div class="row foo_none wow slideInUp">
					<div class="col-xs-12 pull-left">
						<div class="dhinatechnologies_metrokings_Footer_mar2">
							<p class="dhinatechnologies_metrokings_Footer_hd"> Our Address </p>
							<p class="dhinatechnologies_metrokings_Footer_admin"> Registered office: </p>
							<p class="dhinatechnologies_metrokings_Footer_content"> 15/64,Vattavilai, </p>
							<p class="dhinatechnologies_metrokings_Footer_content"> Pacode - 629 168, </p>
							<p class="dhinatechnologies_metrokings_Footer_content dhinatechnologies_metrokings_Footer_mar1"> Kanyakumari District </p>
							<p class="dhinatechnologies_metrokings_Footer_admin"> Administrative Office: </p>
							<p class="dhinatechnologies_metrokings_Footer_content"> Kamal Building, </p>
							<p class="dhinatechnologies_metrokings_Footer_content"> Kuzhithurai – 629163 </p>
							<p class="dhinatechnologies_metrokings_Footer_content"> Kanyakumari District </p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6 col-md-12 pull-left">
						<div class="dhinatechnologies_metrokings_Footer_mar3 wow slideInDown foo_marg1">
							<p class="dhinatechnologies_metrokings_Footer_content copysize"> &copy; 2017 MetroKings. Use of MetroKings.com is  subject to our <span class="ter"> <a href="#"> Terms of Service </a> </span> &nbsp; | &nbsp; <span class="ter"> <a href="#"> Privacy Policy </a> </span> </p>
						</div>
					</div>
					<div class="col-lg-6 col-md-12  pull-left foo_blo">
						<div class="dhinatechnologies_metrokings_Footer_mar3 foo_mar foo_icomar wow slideInUp">
							<span> <img src="product_metro/images/dhinatechnologies_metrokings_quality.png" class="img-responsive dhinatechnologies_metrokings_Footer_img1 foo_client1"> </span>
							<span> <img src="product_metro/images/dhinatechnologies_metrokings_soc.png" class="img-responsive dhinatechnologies_metrokings_Footer_img1 foo_client1"> </span>
							<span> <img src="product_metro/images/dhinatechnologies_metrokings_bbb.png" class="img-responsive dhinatechnologies_metrokings_Footer_img1 foo_client2"> </span>
							<span> <img src="product_metro/images/dhinatechnologies_metrokings_nsf.png" class="img-responsive dhinatechnologies_metrokings_Footer_img1 foo_client1"> </span>
							<span> <img src="product_metro/images/dhinatechnologies_metrokings_safe.png" class="img-responsive foo_client2"> </span>
						</div>
					</div>
				</div>
				<div class="row foo_blo">
					<div class="col-lg-6 col-md-12 pull-left">
						<div>
							<p class="dhinatechnologies_metrokings_Footer_content wow slideInLeft"> This website gives a general view of MetroKings.The benefits and prices described are not available in all state and Canadian provinces. See specific details on terms,coverage,pricing,condition and exclusions in the MetroKings website. </p>
						</div>
					</div>
					<div class="col-lg-6 col-md-12 pull-left">
						<div>
							<p class="dhinatechnologies_metrokings_Footer_content wow slideInRight"> MetroKings provides access to legal services offered by a network of provider law firms to MetroKings members and their covered family member through membership based par-ticipation. Neither MetroKings nor its officers, employees or sales associates directly or indirectly provide legal services, representation or advice. </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> 
	<div class="scrolltop">
		<a href="#top"><img src="product_metro/images/dhinatechnologies_metrokings_arrow.png" class="img-responsive"></a>
	</div>
	<script>
		$("a[href='#top']").click(function() {
			$("html, body").animate({ scrollTop: 0 }, "slow");
			return false;
		});
	</script>
  
	<script>
		$(window).scroll(function() { 
			var scroll = $(window).scrollTop();
			if(scroll>=57){
				$(".mnu_cr").addClass("mnu_clr");
				if($( "span.gj_mnu_cart" ).length <= 1 ){
					$( "span.gj_mnu_cart" ).clone().appendTo( "ul.nav.navbar-nav" );
					$( ".gj_soc_ico" ).css({'font-size':'20px','color':"#000"});
					$( ".gj_mnu_cart" ).css({'top':"16px"});
					$( "span.gj_badge" ).css( { 'position': 'absolute',
						'top': '0px',
						'right': '20px',
						'width': '20px',
						'height': '20px',
						'padding': '2px 3px',
						'background-color': '#000',
						'border-radius': '50%',
						'color': '#fff',
						'text-align': 'center',
						'font-size': '12px'
					});

					
				}
				//$('ul.nav.navbar-nav span.gj_mnu_cart').remove()
			}
			else{
				$(".mnu_cr").removeClass("mnu_clr");
				
				$('ul.nav.navbar-nav span.gj_mnu_cart').remove()
				$( ".gj_soc_ico" ).css({'font-size':'20px','color':"#fff"});
					$( ".gj_mnu_cart" ).css({'top':"16px"});
					$( "span.gj_badge" ).css( { 'position': 'absolute',
						'top': '-5px',
						'right': '13px',
						'width': '23px',
						'height': '23px',
						'padding': '2px 3px',
						'background-color': '#fff',
						'border-radius': '50%',
						'color': '#0688d4',
						'text-align': 'center',
						'font-size': '12px'
					});
				$( ".gj_mnu_cart" ).css({'top':"0px"});
			} 
		});
	</script>

	<script type="text/javascript">/*add cart script*/
		$(document).ready(function() {

      $(document).on('click','.gj_cart_but',function(){
			// $( '.gj_cart_but' ).click(function() {
				el=$(this);
				// alert(el.attr('class'));
				if(el.attr('class') == "gj_cart_but btn-danger"){
					el.removeClass('btn-danger');
					el.addClass('btn-success',500);
					el.prev().fadeOut('slow');
					el.html('<i class="fa fa-check"></i> Add Cart'); 
				}else{
					el.removeClass('btn-success');
					el.addClass('btn-danger',500);
					el.prev().fadeIn('slow');
					el.html('<i class="fa fa-close"></i> Remove');
				}
			});
		});
	</script>

	   <script type="text/javascript">/*product carousel*/
		    $(window).load(function(){
		      	$('#carousel').flexslider({
			        animation: "slide",
			        controlNav: false,
			        animationLoop: false,
			        slideshow: false,
			        itemWidth: 162,
			        itemMargin: 10,
			        asNavFor: '#slider'
		      	});

		      	$('#slider').flexslider({
		        	animation: "slide",
			        controlNav: false,
			        animationLoop: false,
			        slideshow: false,
			        sync: "#carousel",
		      	});
		    });
	  </script>

	  <script>     
	  	function common_add_remove(obj,id,$qty){
		  	if( $(obj).attr('class') == 'gj_cart_but btn-success'){
		  		add_to_cart(id,$qty);
		  	}else{
		  		remove_from_cart(id)
		  	}
	  	}
	  	function common_add_remove_from_s_view(obj,id,$qty){
	  		if( $(obj).attr('class') == 'gj_cart_but btn-success'){
		  		add_to_cart(id,$('input#quantity').val());
		  	}else{
		  		remove_from_cart(id)
		  	}
	  	}
		function add_to_cart(id,qty){//alert(id)
			$.ajax({
				type	: 	'post',
				url		: 	'<?php echo base_url('index.php/common_controller/add_to_cart'); ?>',
				data	: 	{'id':id,'quantity':qty},
				success : 	function(data){
					var option = JSON.parse(data);
					if(option == 1){
						console.log("success");
					}else if(option != 0){	console.log(option);
						$('.empty_value_head').show();
						$('tr.empty_value').hide();
						var ap = '<tr class="crt_prd_'+option.product_id+'"><td>'+option.product_code+'</td><td>'+option.product_name+'</td><td>'+option.user_quantity+'</td><td class="prc_'+option.product_id+'">'+option.user_price+'</td><td><button type="button" class="gj_cart_close" onclick="remove_from_cart('+option.product_id+')"><i class="fa fa-close"></i></button></td></tr>';

						$('tbody.cart_items_body').append(ap);
						$('span.gj_badge').text(option.tot);
						$('span.tp_ct_tt').text("");
            $('span.tp_ct_tt').text(option.cart_tot_price);
						$('.tot_pv_tp').html(option.cart_tot_pv+' PV');
					}else{
						console.log("Add To Cart Failed");
					}
				}
			})
		}
		function remove_from_cart(id,$qty){
			$.ajax({
				type	: 	'post',
				url		: 	'<?php echo base_url('index.php/common_controller/remove_from_cart'); ?>',
				data	: 	{'id':id,'quantity':$qty},
				success : 	function(data){
					
					var option = JSON.parse(data);
					console.log(option);
					if(option.status == 1){
            $('.tp_ct_tt').text(option.cart_tot_price);
            $('.k_prd_pg').html('<i class="fa fa-inr"></i>'+option.cart_tot_price)+'.00';
            $('.k_prd_pv').html(option.cart_tot_pv+" PV");
						$('.tot_pv_tp').html(option.cart_tot_pv+" PV");
						$('tr.crt_prd_'+id).remove();
						$('span.gj_badge').text(option.tot);
						el = $('#bt_'+id);
						el.removeClass('btn-danger');
						el.addClass('btn-success',500);
						el.prev().fadeOut('slow');
						el.html('<i class="fa fa-check"></i> Add Cart'); 
					}

				}
			})
		}
	</script>

	
	<script type="text/javascript">/*product carousel*/
		    $(window).load(function(){
		      	$('#carousel').flexslider({
			        animation: "slide",
			        controlNav: false,
			        animationLoop: false,
			        slideshow: false,
			        itemWidth: 162,
			        itemMargin: 10,
			        asNavFor: '#slider'
		      	});

		      	$('#slider').flexslider({
		        	animation: "slide",
			        controlNav: false,
			        animationLoop: false,
			        slideshow: false,
			        sync: "#carousel",
		      	});
		    });
	  </script>