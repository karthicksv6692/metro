 </div>

<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	$this->view('includes/footer');
?>

<footer>
 <div class="footer_postion" id="footer">
  <div class="container-fluid foot_copyright">
   <div class="col-md-6 pull-left">
    <p>&copy;metrokings.biz.All rights reserved</p>
   </div>
   <div class="col-md-6 pull-right text-right">
    <p>Powered By Dhina Technologies</p>
   </div>
  </div>
 </div>
</footer>

<script type="text/javascript">
	if($('#container').length>=1){
		document.addEventListener("DOMContentLoaded", function (event) {
		    var element = document.getElementById('container');
		    var height = element.offsetHeight;
		    if (height < screen.height) {
		        document.getElementById("footer").classList.add('stikybottom');
		    }
		}, false);
	}
</script>