<link href="<?php echo base_url('assets/css/owl.carousel.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/owl.theme.css'); ?>" rel="stylesheet">

<!--Ceiling Achievers-->
  <section class="ceiling_sec">
    <div class="dhinatechnologies_metrokings_Ceiling_bg1">
      <div class="container">
        <div class = "row">
          <div class = "col-md-12">
            <div>
              <p class="text-center dhinatechnologies_metrokings_Ceiling_hd"> Ceiling Achievers </p>
              <hr class="dhinatechnologies_metrokings_Ceiling_hr">  
            </div>
            <!--carousel-->
            <div class="gj_ceil_carousel">
              <div id="owl-demo2" class="owl-carousel ">

              <?php if(isset($user)){ ?>
                <?php if(! is_int($user)){ ?>
                  <?php foreach ($user as $us) { ?>
                        <div class="item wow zoomIn">
                          <div class="gj_clipimg_div text-center">
                            <div class="dhinatechnologies_metrokings_gjCeiling_img">
                              <?php if(! is_int($us['img_url'])){ ?>
                                <img src="<?php echo base_url($us['img_url']); ?>" alt="clip1" class="img-responsive">
                              <?php }else{ ?>
                                <img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="clip1" class="img-responsive">
                              <?php } ?>
                            </div>
                              <p class="acheiver"> <?php echo $us['name']; ?> </p>
                          </div>
                        </div>  
                  <?php } ?>
                <?php }else{echo"Empty";} ?>
              <?php } ?>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<script src="<?php echo base_url('assets/js/owl.carousel.js'); ?>"></script>
  <script> /* Ceiling Achivers Carousel */

    $(document).ready(function(){
      $("#owl-demo2").owlCarousel({
        items:3,
        loop:true,
        margin:10,
        autoplay:true,
        autoplayTimeout:2000,
          dots:false,
        nav:true,
        responsive:{
          0:{
            items:1
          },
          400:{
            items:1
          },
          550:{
            items:2
          },
          700:{
            items:2
          },
          991:{
            items:3
          },
          1200:{
             items:3
          }
        }
      });
      $( ".owl-next").html('<i class="fa fa-chevron-right gj_right_move"></i>');
      $( ".owl-prev").html('<i class="fa fa-chevron-left gj_left_move"></i>');
    });
  </script>

  <style type="text/css">
    /*-- Ceiling Acheivers --*/
.dhinatechnologies_metrokings_gjCeiling_img {
    width: 210px;
    height: 200px;
    overflow: hidden;
    background-color: transparent;
    margin: 10px auto;
}
.dhinatechnologies_metrokings_Ceiling_bg1 {
  background-color: #0688d4;
  color: white;
  padding: 40px 0;
}
.dhinatechnologies_metrokings_Ceiling_hd {
  font-size: 28px;
}
.dhinatechnologies_metrokings_Ceiling_hr {
  margin-top: -10px;
  margin-bottom: 40px;
  border-top: 2px solid #6dbbe5;
  width: 250px;
}
.acheiver{
  font-size: 20px;
  text-transform: capitalize;
}
section.ceiling_sec {
    margin-bottom: 3px;
}
.gj_left_move {
    position: absolute;
    top: 40%;
    left: 0;
    font-size: 20px;
}
.gj_right_move {
    position: absolute;
    top: 40%;
    right: 0;
    font-size: 20px;
}
  </style>