<?php if(isset($contact_obj) && $contact_obj != null){

		if($contact_obj->getContactName() != null)	{
			$contact_name = $contact_obj->getContactName();
		}else{$contact_name = "";}

		if($contact_obj->getContactDesignation() != null)	{
			$desig = $contact_obj->getContactDesignation();
		}else{$desig = "";}

		if($contact_obj->getContactMobilePrimary() != null)	{
			$mob = $contact_obj->getContactMobilePrimary();
		}else{$mob = "";}

		if($contact_obj->getContactMobileSecondary() != null)	{
			$smob = $contact_obj->getContactMobileSecondary();
		}else{$smob = "";}

		if($contact_obj->getContactEmailId() != null)	{
			$mail = $contact_obj->getContactEmailId();
		}else{$mail = "";}

?>
<div class="dkbody">
	<div class="container-fluid">
		<div class="page-header">
			<h3>Contact Details</h3>
		</div>
		<div class="page-content">
			<form method="post" action="<?php echo base_url('index.php/common/contact_crud/edit'); ?>">
				<section class="content bgcolor-1">
					<div class="col-md-6 pull-left">
						<table style="width: 100%" class="single_view_table table-responsive">
							<tr>
								<td class="single_table_view_head">Contact Name</td>
								<td class="single_table_view_head coln">:</td>
								<td class="single_table_view_data"><?php echo $contact_name; ?></td>
							</tr>
							<tr>
								<td class="single_table_view_head">Designation</td>
								<td class="single_table_view_head coln">:</td>
								<td class="single_table_view_data"><?php echo $desig; ?></td>
							</tr>
							<tr>
								<td class="single_table_view_head">Mobile</td>
								<td class="single_table_view_head coln">:</td>
								<td class="single_table_view_data"><?php echo $mob; ?></td>
							</tr>
							<tr>
								<td class="single_table_view_head">Alter Mobile</td>
								<td class="single_table_view_head coln">:</td>
								<td class="single_table_view_data"><?php echo $smob; ?></td>
							</tr>
							<tr>
								<td class="single_table_view_head">Email</td>
								<td class="single_table_view_head coln">:</td>
								<td class="single_table_view_data"><?php echo $mail; ?></td>
							</tr>
							
						</table>
					</div>
					<div class="country_width_100 col-12 mt-5">
							<div class="country_width_100">
								<!-- <div class="butt_sec_width mt-3 mb-3">
							      	<button type="submit" name="country_submit" class="country_button mr-2" >EDIT <i class="fa fa-pencil" aria-hidden="true"></i></button>		
								    <a href="<?php echo base_url('index.php/common/country_crud'); ?>"><button type="button" name="country_reset" class="country_button">CANCEL <i class="fa fa-trash" aria-hidden="true"></i></button></a>
						      	
								</div> -->
					      	</div>
					      	
					        <div class="country_width_100 mt-3 mb-3">					      		  
							    <div class="country-right">
							      	<a href="<?php echo base_url('index.php/common/contact_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button></a>
							    </div>	
							</div>
						</div>
				</section>
			</form>
		</div>
	</div>
</div>
<?php }else{ ?>
	<div class="dkbody">
		<div class="container-fluid">
			<div class="page-header">
				<h3>Contact</h3>
			</div>
			<?php echo $search; ?>
		</div>
	</div>
	<script type="text/javascript">
		$("#combobox").select2();
	</script>
<?php }  ?>