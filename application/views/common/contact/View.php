<div class="dkbody">
	<div class="container-fluid">
		<div class="page-header">
			<h3>Contact</h3>
		</div>
		<div class="page-content">                   
        <div class="container-fluid">
          <table id="bank_request" class="data_table_style">
              <thead>
                 <tr role="row">
                    <th>CONTACT NAME</th>
                    <th>DESIGNATION</th>
                    <th>MOBILE</th>
                    <th>EMAIL</th>
                    <th>ACTIONS</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>
        
     	</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $('#bank_request').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": '<?php echo base_url(); ?>index.php/common/Contact_crud/datatable_ajax',
        "order": [[ 0, "asc" ]],
        "pagingType": "full_numbers"
    });
});
</script>


<div id="dialog-confirm" title="DELETE">
     <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>Are you sure?</p>
</div>

<script>
	$('.dkbody').on('click','.btn_delete',function(){
	  var id = $(this).attr('data-value');
	  var tr = $(this).closest("tr");
	  var aa = $(this);
	   $( "#dialog-confirm" ).dialog({
	     resizable: true,
	     height: "auto",
	     width: 400,
	     modal: true,
	     buttons: {
	       "Delete": function() {
	          $.ajax({
	            type: "POST",
	            url: "<?php echo base_url(); ?>common/Country_crud/ajax_delete",
	            data: {'delete_country_id': id },
	              success:  function (data) {
	                if(data == 1){
	                  tr.fadeOut(1000,function(){tr.remove()});
	                  toast({
	                    message: "Successfully Deleted",
	                    displayLength: 3000,
	                    className: 'success',
	                  });
	                  $('span.ui-button-icon.ui-icon.ui-icon-closethick').click();
	                }else{
	                  toast({
	                    message: "Deleted Failed",
	                    displayLength: 3000,
	                    className: 'error',
	                  });
	                  $('span.ui-button-icon.ui-icon.ui-icon-closethick').click();
	                }
	              }
	          })
	        },
	        Cancel: function() {
	          $(this).dialog( "close" );
	        }
	     }
	   });
	 });
</script>
<style>
#dialog-confirm{
 display:none;
}
</style>