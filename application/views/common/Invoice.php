<?php //print_r($obj); ?>

<div class="dkbody">
        <div class="container-fluid">
            <div class="page-header">
                <h3>Invoice</h3>
            </div>

                <table align="center" cellpadding="3" cellspacing="0" border="0" width="500px">
                    <tbody><tr>
                        <td style="background: #d0ecfc;">
                            
                    <style type="text/css">
                        .companybanner
                        {
                            font-size: 11px;
                            color: Black;
                        }
                        .companybanner td
                        {
                            text-align: right;
                        }
                        .style1
                        {
                            width: 62px;
                        }
                    </style>
                    <table cellpadding="0" cellspacing="0" class="companybanner" width="100%" align="center">
                        <tbody><tr>
                            <td rowspan="6" class="style1">
                                
                            </td>
                            <td>
                                <div style="font-size: 14px; font-weight: bold; text-align: right;">
                                    Metrokings Marketing Private Limited</div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span id="lblCPYAddress1">Vattavilai,</span>
                                <span id="lblCPYAddress2">Pacode,</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span id="lblCPYCity">KANYAKUMARI - 629 168.</span>
                                <span id="lblCPYContact"> Phone - 04651263811</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span id="lblCPYEmail">Email : contact.metrokings@gmail.com</span>
                                <br>
                                <span id="lblCPYWebSite">Website : http://www.metrokings.biz</span>
                            </td>
                        </tr>
                    </tbody></table>

                                            </td>
                    </tr>
                    <tr>
                        <td style="background-color: #f7f7f7;">
                            <table align="center" cellpadding="3" cellspacing="0" class="simpleTable">
                                <tbody><tr>
                                    <td align="center">
                                        <div align="center">
                                            <span style="font-size: 20px; font-weight: bold">Welcome Letter</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div>
                                            <br>
                                            <table class="style2" align="center">
                                                <tbody><tr>
                                                    <td>
                                                        <span style="font-weight: bold;">Join Date</span>
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td>
                                                        <span id="lblJoinDate" class="displaytext">
                                                            <?php echo $obj['distributor_created'] ?>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span id="lblMarketerIDLabel">Distributor ID</span>
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td>
                                                        <span id="lblMarketerID" class="displaytext">
                                                            <?php echo $obj['distributor_id'] ?>
                                                                
                                                            </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Name
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td>
                                                        <span id="lblFullName" class="displaytext">
                                                        <?php echo $obj['distributor_name'] ?>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Placement ID
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td>
                                                        <span id="lblSponsorID" class="displaytext">
                                                            <?php echo $obj['placement_id'] ?>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Name
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td>
                                                        <span id="lblSponsorName" class="displaytext">
                                                            <?php echo $obj['placement_name'] ?>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Position
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td>
                                                        <span id="lblPosition" class="displaytext">
                                                            <?php echo $obj['dis_pos'] ?>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Package Name
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td>
                                                        <span id="lblPackage" class="displaytext">
                                                        <?php echo $obj['package_name'] ?>
                                                        </span>
                                                    </td>
                                                </tr>
                                              
                                                <tr>
                                                    <td>
                                                        Amount
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td>
                                                        <span id="lblPinValue" class="displaytext">
                                                        <?php echo $obj['package_price'] ?>
                                                        </span>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        * This is a Computer Generated Certificate hence Signature is not Required.
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <b>
                                            <h2>
                                                Thank You</h2>
                                        </b>
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                    <tr>
                        <td height="25px" style="background-color: #d0ecfc;">
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <div id="printbutt">
                                <br>
                                <input type="submit" name="cmdPrint" value="Print" onclick="javascript:window.print();return false;" id="cmdPrint" class="print" style="margin-bottom: 2em;">
                            </div>
                        </td>
                    </tr>
                </tbody></table>
        </div>
</div>