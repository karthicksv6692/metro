

<div class="dkbody">
  <div class="container-fluid">
    <div class="page-header">
      <h3>Payout-Sales</h3>
    </div>
    <div class="rr">
      <div class="col-md-12 col-lg-12 gj_tree_struct">
          <div class="gj_tree_div text-center">
                <div class="col-lg-3 col-md-4 pull-left pr-0 pl-0">
                  <div style="">
                    <select class="user_id form-control" style="" name="user_id"></select>
                  </div>
                </div>

                <div class="col-lg-3 col-md-4 pull-left pr-0 pl-0">
                  <div style="">
                    <select class="user_name form-control" style="" name="user_name"></select>
                  </div>
                </div>

                <div class="col-md-2 pull-left form-group">
                  <span class="input input--nao" id="sandbox-container">
                    <input type="text" class="input__field input__field--nao form-control dtt1"  name="dob" value="" 
                     data-validation="date" />
                     <!-- data-validation-format="yyyy-mm-dd" -->
                    <label class="input__label input__label--nao" for="input-1">
                      <span class="input__label-content input__label-content--nao">From</span>
                    </label>
                    <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                      <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                    </svg>
                  </span> 
                </div>

                <div class="col-md-2 pull-left form-group">
                  <span class="input input--nao" id="sandbox-container">
                    <input type="text" class="input__field input__field--nao form-control dtt2"  name="dob" value="" 
                     data-validation="date" />
                     <!-- data-validation-format="yyyy-mm-dd" -->
                    <label class="input__label input__label--nao" for="input-1">
                      <span class="input__label-content input__label-content--nao">To</span>
                    </label>
                    <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                      <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                    </svg>
                  </span> 
                </div>
                 <div class="col-md-1pull-left form-group">
                <button type="button" class="btn btn_row1  btn-7 btn-7a icon-truck mb-5" id="date-search"> <i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
  
          </div>
      </div>
    </div>
    <div class="rr">
        

        
    </div>

    <div class="page-content">                   
        <div class="container-fluidx" style="margin-bottom: 2em">
          <table  id="gj_tree_struct_vw" class="table table-striped table-bordered data_table_style">
            <thead>
                
                <tr>
                  <th class="text-center col-md-1">#</th>
                  <th class="text-center" >Id</th>
                  <th class="text-center" >User-Name</th>
                  <th class="text-center" >Name</th>
                  <th class="text-center" >Amount</th>
                  <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody style="text-align: center;" class="my_tab_bdy">
              <!-- <tr><td>Karthick</td><td>Karthick</td><td>Karthick</td><td>Karthick</td></tr> -->
            </tbody>
            <tfoot>
              <tr>
                <td colspan="4" class="text-right"><h4>Total:</h4></td>
                <td><h4 class="tot"></h4></td>
                <td></td>
              </tr>
            </tfoot>
          </table>
        </div>
         
      </div>
  </div>
</div>
<button type="button" class="btn btn-info btn-lg mod" data-toggle="modal" data-target="#myModal" style="opacity: 0">Open Modal</button>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
      </div>
      <div class="modal-body">
          <table class="table table-striped mob_tb">
            <thead>
              <tr>
                <th>#</th>
                <th>User Id</th>
                <th>User Name</th>
                <th>Name</th>
                <th>Amount</th>
                <th>Date</th>
              </tr>
            </thead>
            <tbody class="tbdy">
            </tbody>
          </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
<script type="text/javascript">

  $('.user_id').select2({
    placeholder: 'Search With User Id',
    allowClear: true,
    ajax: {
      url: '<?php echo base_url("index.php/common_controller/get_all_user_id") ?>',
      dataType: 'json',
      delay: 1,
      data: function (params) {
            return { q: params.term 
            };
      },
      processResults: function (data) { return { results: data }; },
      cache: true
    },
  });
  $('.user_name').select2({
    placeholder: 'Search With User Name',
    allowClear: true,
    ajax: {
      url: '<?php echo base_url("index.php/common_controller/get_all_user_name") ?>',
      dataType: 'json',
      delay: 1,
      data: function (params) {
            return { rs:1,q: params.term 
            };
      },
      processResults: function (data) { return { results: data }; },
      cache: true
    },
  });

  $(document).ready(function() {

    var tab = "";
    tab = $('#gj_tree_struct_vw').DataTable({
      "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    });
    
    // && dt2.length>0
   
    
    document.querySelector("#date-search").addEventListener('click',function(){
          dt1 = $('.dtt1').val();
          dt2 = $('.dtt2').val();

          user_name = $('.user_name').val();
          user_id = $('.user_id').val();

          if(dt1.length>0 ||user_name!=null || user_id != null ){
            tab.destroy();
            tab = $('#gj_tree_struct_vw').DataTable({
              
              "processing": true,
              "serverSide": true,
              "ajax": {
                "url": '<?php echo base_url(); ?>index.php/level_income/datatable',
                 "data": {'dt1':dt1,'dt2':dt2,'user_name':user_name,'user_id':user_id},
              },
              
            });
            
          }

          

          // $.ajax({
          //   type: "POST",
          //   url : '<?php echo base_url('index.php/level_income/total_level_income') ?>',
          //    "data": {'dt1':dt1,'dt2':dt2,'user_name':user_name,'user_id':user_id},
          //   success:  function (data) {
          //     if(data != 0){
          //       var option = JSON.parse(data);
          //       if(option['tot'] != "" ){
          //         $('.tot').text(option['tot']);
          //       }else{
          //         $('.tot').text('00.00');
          //       }
          //     }else{ $('.tot').text('');  }
          //   }
          // })
    })
  });

  function get_income_details(id){
    $('.mod').click();
    $.ajax({
      type: "POST",
      url : '<?php echo base_url('index.php/level_income/level_income_details') ?>',
      "data": {'id':id},
      success:  function (data) {
        if(data != 0){
          var option = JSON.parse(data);
          $('.tbdy').html(option);
        }else{ $('.tbdy').html('');  }
      }
    })
  }

</script>
<style type="text/css">

table#gj_tree_struct_vwee thead tr th {
    background: #2bb1e9;
    color: #fff;
}
div#myModal {
    padding-top: 11%;
}
.modal-dialog {
    width: 80%;
    margin: 30px auto;
    max-width: 100%;
}
a.dt-button.buttons-excel.buttons-html5{
  opacity: 0;
  display: none;
}
  .dt-buttons a {
      padding: 10px 25px;
      margin: 10px;
      background: #2bb1e9;
      color: #fff;
  }
  a.dt-button.buttons-excel.buttons-html5 {
      position: absolute;
      left: 7em;
  }
  a.dt-button.buttons-pdf.buttons-html5 {
      position: absolute;
      left: -9px;
  }
  .dt-buttons {
      padding-bottom: 20px;
      position: absolute;
      left: 170px;
      top: -20px;
  }

  select.form-control:not([size]):not([multiple]) {
      height: calc(2.25rem + 30px);
  }
  button#date-search {
      height: 50px;
      width: 60px;
      margin-left: 20px;
  }
  i.fa.fa-search {
      font-size: 25px;
  }
  .data_table_style thead tr th{
    background-color: rgba(22, 169, 231, 0.91) !important;
    color: #fff;
    font-size: 14px;
    padding: 7px;
    border: 1px solid;
    /*min-width: 160px;*/
  }
  .box{
    width: 30%;
    float: right;
  }
  .f12{
    width: 100%;
  }
  .f6{
    width: 50%;
    float: left;
  }
  .hd{
    color: #088bc1 !important;
  }
  .rs{
        color: #ff4081;
  }
  input.cus_but {
    background: #2bb1e9;
    padding: 7px 25px;
    font-size: 15px;
    color: #fff;
    border: 0;
    position: absolute;
    left: 16%;
    z-index: 2;
}
  table.display {
    margin: 0 auto;
    width: 100%;
    clear: both;
    border-collapse: collapse;
    table-layout: fixed;         // add this 
    word-wrap:break-word;        // add this 
  }
  a.dt-button.buttons-pdf.buttons-html5 {
    z-index: 1;
    opacity: 0;
}
.page-content section.content.bgcolor-1 form span.select2.select2-container.select2-container--default.select2-container--focus, span.select2.select2-container.select2-container--default.select2-container--below, span.select2.select2-container.select2-container--default {
    width: 230px !important;
    margin: 45px auto !important;
    text-align: left;
}
.mob_tb thead {
    background: #2bb1e9;
    color: #fff;
}
/*#tbl_qunatity { table-layout: fixed; }*/

</style>