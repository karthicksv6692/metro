<?php if(isset($address_obj) && $address_obj != null){

		if($address_obj->get_Address_full_address() != null)	{
			$address = $address_obj->get_Address_full_address();
		}else{$address = "";}


		if($address_obj->get_Address_pincode_id() != null)	{
			$pincode = $address_obj->get_Address_pincode_id()->getPincodeNo();
		}else{$pincode = "";}

		if($address_obj->get_Address_pincode_id() != null)	{
			$place = $address_obj->get_Address_pincode_id()->getPincodePlace();
		}else{$place = "";}

		if($address_obj->get_Address_pincode_id() != null)	{
			$district = $address_obj->get_Address_pincode_id()->getPincodeDistrict();
		}else{$district = "";}

		if($address_obj->get_Address_pincode_id() != null)	{
			$state = $address_obj->get_Address_pincode_id()->getPincodeState();
		}else{$state = "";}

		if($address_obj->get_Address_pincode_id()->getPincodeCountryId() != null)	{
			$country = $address_obj->get_Address_pincode_id()->getPincodeCountryId()->getCountryName();
		}else{$country = "";}


	?>
	<div class="dkbody">
		<div class="container-fluid">
			<div class="page-header">
				<h3>Address Details</h3>
			</div>
			<div class="page-content">
				<form method="post" action="<?php echo base_url('index.php/common/pincode_crud/edit'); ?>">
					<section class="content bgcolor-1">
						<div class="col-md-6 pull-left">
							<table style="width: 100%" class="single_view_table table-responsive">
								<tr>
									<td class="single_table_view_head">Full Address</td>
									<td class="single_table_view_head coln">:</td>
									<td class="single_table_view_data"><?php echo $address; ?></td>
								</tr>
								<tr>
									<td class="single_table_view_head">Pincode</td>
									<td class="single_table_view_head coln">:</td>
									<td class="single_table_view_data"><?php echo $pincode; ?></td>
								</tr>
								<tr>
									<td class="single_table_view_head">Place</td>
									<td class="single_table_view_head coln">:</td>
									<td class="single_table_view_data"><?php echo $place; ?></td>
								</tr>
								<tr>
									<td class="single_table_view_head">District</td>
									<td class="single_table_view_head coln">:</td>
									<td class="single_table_view_data"><?php echo $district; ?></td>
								</tr>
								<tr>
									<td class="single_table_view_head">State</td>
									<td class="single_table_view_head coln">:</td>
									<td class="single_table_view_data"><?php echo $state; ?></td>
								</tr>
								<tr>
									<td class="single_table_view_head">Country</td>
									<td class="single_table_view_head coln">:</td>
									<td class="single_table_view_data"><?php echo $country; ?></td>
								</tr>
								
							</table>
						</div>
						<div class="country_width_100 col-12 mt-5">
								<div class="country_width_100">
									<!-- <div class="butt_sec_width mt-3 mb-3">
								      	<button type="submit" name="country_submit" class="country_button mr-2" >EDIT <i class="fa fa-pencil" aria-hidden="true"></i></button>		
									    <a href="<?php echo base_url('index.php/common/country_crud'); ?>"><button type="button" name="country_reset" class="country_button">CANCEL <i class="fa fa-trash" aria-hidden="true"></i></button></a>
							      	
									</div> -->
						      	</div>
						      	
						        <div class="country_width_100 mt-3 mb-3">					      		  
								    <div class="country-right">
								      	<a href="<?php echo base_url('index.php/common/address_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button></a>
								    </div>	
								</div>
							</div>
					</section>
				</form>
			</div>
		</div>
	</div>
	<?php }else{ ?>
		<div class="dkbody">
			<div class="container-fluid">
				<div class="page-header">
					<h3>Select</h3>
				</div>
				<?php echo $search; ?>
			</div>
		</div>
		<script type="text/javascript">
			$("#combobox").select2();
		</script>
	<?php }  ?>