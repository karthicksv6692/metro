<div class="dkbody">
	<div class="container-fluid">
		<div class="page-header">
			<h3>Country Add</h3>
		</div>
		<div class="page-content">
			<section class="content bgcolor-1">
				<form method="POST" action="<?php echo base_url('index.php/common/country_crud/add'); ?>"  id="country_add">
					<div class="row rr">
						<div class="col-md-5 pull-left row form-group">
							<span class="input input--nao">
								<input class="input__field input__field--nao country_name" type="text" id="country-name"
								 	name="country_name" value="<?php echo set_value('country_name'); ?>" 
								  	 data-validation="required custom length"
									 data-validation-length="2-100"
									 data-validation-regexp="^[A-Za-z][A-Za-z0-9-_\ .&amp;]+$"
									 data-validation-error-msg-custom="Starts with alphabet,No special chars" />
								<label class="input__label input__label--nao" for="country-name">
									<span class="input__label-content input__label-content--nao">Country Name</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
								<img src="<?php echo base_url('assets/images/loader'); ?>/default.gif" class="ldr cname_ldr">

							</span>		
							<p class="cname_suc avail_er suc">Available</p>
							<p class="cname_err avail_er err">Already Exists</p>
							<?php echo form_error('country_name','<span class="help-block form-error">', '</span>'); ?>	
						</div>
								
						<div class="col-md-5 pull-left row form-group">
							<span class="input input--nao">
								<input class="input__field input__field--nao country_code" type="text" id="country-code"
								  name="country_code" value="<?php echo set_value('country_code'); ?>" 
								  data-validation="required length number"
		                          data-validation-length="1-5"
		                          data-validation-ignore="+"
								  />
								 <label class="input__label input__label--nao" for="country-code">
									<span class="input__label-content input__label-content--nao">Country Code</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
								<img src="<?php echo base_url('assets/images/loader'); ?>/default.gif" class="ldr ccode_ldr">

							</span>		
							<p class="ccode_suc avail_er suc">Available</p>
							<p class="ccode_err avail_er err">Already Exists</p>
							<?php echo form_error('country_code','<span class="help-block form-error">', '</span>'); ?>	
						</div>
					</div>
					<input type="hidden" name="country" value="asdf">
					<div class="country_width_100 col-12 mt-5">
						<div class="country_width_100">
							<div class="butt_sec_width mt-3 mb-3">
						      	<button type="submit" name="country_submit" class="country_button mr-2" >SUBMIT <i class="fa fa-paper-plane" aria-hidden="true"></i></button>		
							    <button type="reset" name="country_reset" class="country_button">RESET <i class="fa fa-refresh" aria-hidden="true"></i></button>
					      	
							</div>
				      	</div>
				      	
				        <div class="country_width_100 mt-3 mb-3">					      		  
						    <div class="country-right">
						      	<a href="<?php echo base_url('index.php/common/country_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button></a>
						    </div>	
						</div>
					</div>
				</form>
			</section>
		</div>
	</div>
</div>
<script type="text/javascript">
	$.validate({
	    form:'#country_add',
	});  
	$(function(){
		$('.cname_suc,.cname_err,.ccode_suc,.ccode_err,.cname_ldr,.ccode_ldr').hide();
	})
	$('.country_name').focus(function()  {
	  var a = $('.country_name').val();
	  if(a.length>0){
	    cc_name = a;
	  }else{
	    cc_name="";
	  }
	})
  	$('.country_name').blur(function()  {
  		var regex_name = /^[a-zA-Z][a-zA-Z0-9 ]{2,}$/;
	    var country_name = $('.country_name').val();
	    if(country_name.length>2 && cc_name != country_name && regex_name.test(country_name)){
	      $('.cname_ldr').show();
	      $.ajax({
	        type: "POST",
	        url: "<?php echo base_url(); ?>index.php/Common_controller/country_name_available",
	        data: {'field_value':country_name},
	          success:  function (data) { 
	            $('.cname_ldr').hide();
	            if(data == 1){
	            	$('.cname_suc').show();
	            	$('.cname_err').hide();
	            }else{
	            	$('.cname_suc').hide();
	            	$('.cname_err').show();
	            }
	          }
	      })
	    }else{
	     $('.cname_suc,.cname_err').hide();
	    }
  	})
  	$('.country_code').focus(function()  {
	  var b = $('.country_code').val();
	  if(b.length>0){
	    cc_code = b;
	  }else{
	    cc_code="";
	  }
	})
  	$('.country_code').blur(function()  {
  		var regex_code = /^[0-9+]{1,5}$/;
	    var country_code = $('.country_code').val();
		    if(country_code.length>0 && cc_code != country_code && regex_code.test(country_code)){
		    	if(!isNaN(country_code)){
			      $('.ccode_ldr').show();
			      $.ajax({
			        type: "POST",
			        url: "<?php echo base_url(); ?>index.php/Common_controller/country_code_available",
			        data: {'field_value':country_code},
			          success:  function (data) { 
			            $('.ccode_ldr').hide();
			            if(data == 1){
			            	$('.ccode_suc').show();
			            	$('.ccode_err').hide();
			            }else{
			            	$('.ccode_suc').hide();
			            	$('.ccode_err').show();
			            }
			          }
			      })
			    }else{
			    	$('.ccode_err').show();
			    	$('.ccode_suc').hide();
			    	$('.ccode_err').text('Enter Number Only');
			    }
		    }else if(country_code.length == 0 && cc_code.length > 2){
		     $('.cname_suc,.cname_err').hide();
		    }
		    else{
		    	$('.cname_suc,.cname_err').hide();
		    }
  	})

  	$(document).on("click", "button[type='reset']", function(){
	  $('.cname_suc,.cname_err,.ccode_suc,.ccode_err,.cname_ldr,.ccode_ldr').hide();
	  $('span.input.input--nao').removeClass('input--filled');
	});
</script>
