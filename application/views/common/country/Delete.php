<?php if(isset($country_obj)){

	if($country_obj->getCountryName() != null)	{
		$country_name = $country_obj->getCountryName();
	}else{$country_name = "";}

	if($country_obj->getCountryCode() != null)	{
		$country_code = $country_obj->getCountryCode();
	}else{$country_code = "";}

	if($country_obj->getCountryId	() != null)	{
		$country_id = $country_obj->getCountryId();
	}else{$country_id = "";}

?>
<div class="dkbody">
	<div class="container-fluid">
		<div class="page-header">
			<h3>Country Details</h3>
		</div>
		<div class="page-content">
			<form method="post" action="<?php echo base_url('index.php/common/country_crud/delete'); ?>">
				<section class="content bgcolor-1">
					<div class="col-md-6 pull-left">
						<table style="width: 100%" class="single_view_table table-responsive">
							<tr>
								<td class="single_table_view_head">Country Name</td>
								<td class="single_table_view_head">:</td>
								<td class="single_table_view_data"><?php echo $country_name; ?></td>
							</tr>
							<tr>
								<td class="single_table_view_head">Country Code</td>
								<td class="single_table_view_head">:</td>
								<td class="single_table_view_data"><?php echo $country_code; ?></td>
							</tr>
							
						</table>
					</div>
					<input type="hidden" name="delete_country_id" value="<?php echo $country_id; ?>">
					<div class="country_width_100 col-12 mt-5">
							<div class="country_width_100">
								<div class="butt_sec_width mt-3 mb-3">
							      	<button type="submit" name="country_submit" class="country_button mr-2" >DELETE <i class="fa fa-pencil" aria-hidden="true"></i></button>		
								    <a href="<?php echo base_url('index.php/common/country_crud'); ?>"><button type="button" name="country_reset" class="country_button">CANCEL <i class="fa fa-trash" aria-hidden="true"></i></button></a>
						      	
								</div>
					      	</div>
					      	
					        <div class="country_width_100 mt-3 mb-3">					      		  
							    <div class="country-right">
							      	<a href="<?php echo base_url('index.php/common/country_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button></a>
							    </div>	
							</div>
						</div>
				</section>
			</form>
		</div>
	</div>
</div>
<?php }else{ ?>

	<div class="dkbody">
	<div class="container-fluid">
		<div class="page-header">
			<h3>Select</h3>
		</div>
		<?php echo $search; ?>
	</div>
</div>


<script type="text/javascript">
	$("#combobox").select2();
</script>

<?php }  ?>