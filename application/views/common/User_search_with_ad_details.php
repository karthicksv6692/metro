<div class="dkbody">
  <div class="container-fluid">
    <div class="page-header">
      <h3>User Search</h3>
    </div>
    <div class="rr">
      <div class="col-lg-4 col-md-4 pull-left pr-0 pl-0">
        <div style="">
          <select class="search_id form-control" style="" name="search_id"></select>
        </div>
      </div>

      <div class="col-lg-4 col-md-4 pull-left pr-0 pl-0">
        <div style="">
          <select class="search_tool form-control" style="" name="search_tool">
            <option value="0">Pincode</option>
            <option value="1">Place</option>
            <option value="2">District</option>
          </select>
        </div>
      </div>

      <div class="col-md-2">
        <button type="button" class="btn btn_row1  btn-7 btn-7a icon-truck mb-5" id="date-search"> <i class="fa fa-search" aria-hidden="true"></i></button>
      </div>
    </div>

    <div class="page-content">                   
      <div class="xx" style="margin-bottom: 2em">
        <table  id="gj_tree_struct_vw" class="table table-striped table-bordered table-responsive data_table_style" cellspacing="10"  style="width: 100%;">
          <thead>
              <tr>
                <th class="text-center">#</th>
                <th class="text-center">UserId</th>
                <th class="text-center">UserName</th>
                <th class="text-center">Name</th>
                <th class="text-center">Mobile</th>
                <th class="text-center">Pincode</th>
                <th class="text-center">Place</th>
                <th class="text-center">District</th>
                <th class="text-center">Join Date</th>
              </tr>
          </thead>
          <tbody></tbody>
        </table>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
  $('.search_id').select2({
    placeholder: 'Search',
    allowClear: true,
    ajax: {
      url: '<?php echo base_url("index.php/common_controller/get_user_address_information") ?>',
      dataType: 'json',
      delay: 1,
      data: function (params) {
            return { q: params.term,search_tool:$('.search_tool').val(),
            };
      },
      processResults: function (data) { return { results: data }; },
      cache: true
    },
  });
  $(document).ready(function() {
    var tab = "";
    tab = $('#gj_tree_struct_vw').DataTable();

    document.querySelector("#date-search").addEventListener('click',function(){
      var key = $('.search_id').val();
      var search_tool = $('.search_tool').val();
      if(key){
        tab.destroy();
        tab = $('#gj_tree_struct_vw').DataTable({
          "processing": true,
          "serverSide": true,
          "ajax": {
            "url": '<?php echo base_url(); ?>index.php/common_controller/datatable_get_user_address_information',
             "data": {'key':key,'search_tool':search_tool},
          },
        });
      }
    })
  });
</script>

<style type="text/css">
  select.form-control:not([size]):not([multiple]) {
    height: auto;
  }
  .search_tool{
    margin-top: 2em;
  }
  .btn_row1 {
      background-color: #0290cc;
      color: white;
      border-radius: 0;
      padding: 1px 18px 8px 17px;
      font-size: 1.9rem;
      margin: 2.2rem 0rem;
  }
</style>