<?php if(isset($pincode_obj) && $pincode_obj != null){

		if($pincode_obj->getPincodeId() != null)	{
			$pincode_id = $pincode_obj->getPincodeId();
		}else{$pincode_id = "";}

		if($pincode_obj->getPincodeNo() != null)	{
			$pincode_no = $pincode_obj->getPincodeNo();
		}else{$pincode_no = "";}

		if($pincode_obj->getPincodeId() != null)	{
			$pincode_id = $pincode_obj->getPincodeId();
		}else{$pincode_id = "";}

		if($pincode_obj->getPincodePlace() != null)	{
			$place = $pincode_obj->getPincodePlace();
		}else{$place = "";}

		if($pincode_obj->getPincodeDistrict() != null)	{
			$district = $pincode_obj->getPincodeDistrict();
		}else{$district = "";}

		if($pincode_obj->getPincodeState() != null)	{
			$state = $pincode_obj->getPincodeState();
		}else{$state = "";}

		if($pincode_obj->getPincodeCountryId() != null)	{
			$country_id = $pincode_obj->getPincodeCountryId()->getCountryName();
		}else{$country_id = "";}

	?>
	<div class="dkbody">
		<div class="container-fluid">
			<div class="page-header">
				<h3>Pincode Delete</h3>
			</div>
			<div class="page-content">
				<form method="post" action="<?php echo base_url('index.php/common/pincode_crud/delete'); ?>">
					<section class="content bgcolor-1">
						<div class="col-md-6 pull-left">
							<table style="width: 100%" class="single_view_table table-responsive">
								<tr>
									<td class="single_table_view_head">Pincode</td>
									<td class="single_table_view_head">:</td>
									<td class="single_table_view_data"><?php echo $pincode_no; ?></td>
								</tr>
								<tr>
									<td class="single_table_view_head">Place</td>
									<td class="single_table_view_head">:</td>
									<td class="single_table_view_data"><?php echo $place; ?></td>
								</tr>
								<tr>
									<td class="single_table_view_head">District</td>
									<td class="single_table_view_head">:</td>
									<td class="single_table_view_data"><?php echo $district; ?></td>
								</tr>
								<tr>
									<td class="single_table_view_head">State</td>
									<td class="single_table_view_head">:</td>
									<td class="single_table_view_data"><?php echo $state; ?></td>
								</tr>
								<tr>
									<td class="single_table_view_head">Country</td>
									<td class="single_table_view_head">:</td>
									<td class="single_table_view_data"><?php echo $country_id; ?></td>
								</tr>
								
							</table>
						</div>
						<input type="hidden" name="delete_pincode_id" value="<?php echo $pincode_id; ?>">
						<div class="country_width_100 col-12 mt-5">
								<div class="country_width_100">
									<div class="butt_sec_width mt-3 mb-3">
								      	<button type="submit" name="country_submit" class="country_button mr-2" >DELETE <i class="fa fa-pencil" aria-hidden="true"></i></button>		
									    <a href="<?php echo base_url('index.php/common/country_crud'); ?>"><button type="button" name="country_reset" class="country_button">CANCEL <i class="fa fa-trash" aria-hidden="true"></i></button></a>
							      	
									</div>
						      	</div>
						      	
						        <div class="country_width_100 mt-3 mb-3">					      		  
								    <div class="country-right">
								      	<a href="<?php echo base_url('index.php/common/country_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button></a>
								    </div>	
								</div>
							</div>
					</section>
				</form>
			</div>
		</div>
	</div>
	<?php }else{ ?>
		<div class="dkbody">
			<div class="container-fluid">
				<div class="page-header">
					<h3>Select</h3>
				</div>
				<?php echo $search; ?>
			</div>
		</div>
		<script type="text/javascript">
			$("#combobox").select2();
		</script>
	<?php }  ?>