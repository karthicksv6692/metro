<?php if(isset($country_obj) && count($country_obj)>0){ ?>
	
	<?php if(isset($pincode_obj) && $pincode_obj != null){

		if($pincode_obj->getPincodeNo() != null)	{
			$pincode_no = $pincode_obj->getPincodeNo();
		}else{$pincode_no = "";}

		if($pincode_obj->getPincodeId() != null)	{
			$pincode_id = $pincode_obj->getPincodeId();
		}else{$pincode_id = "";}

		if($pincode_obj->getPincodePlace() != null)	{
			$place = $pincode_obj->getPincodePlace();
		}else{$place = "";}

		if($pincode_obj->getPincodeDistrict() != null)	{
			$district = $pincode_obj->getPincodeDistrict();
		}else{$district = "";}

		if($pincode_obj->getPincodeState() != null)	{
			$state = $pincode_obj->getPincodeState();
		}else{$state = "";}

		if($pincode_obj->getPincodeCountryId() != null)	{
			$country_id = $pincode_obj->getPincodeCountryId()->getCountryId();
		}else{$country_id = "";}

	?>
	<div class="dkbody">
		<div class="container-fluid">
			<div class="page-header">
				<h3>Pincode</h3>
			</div>
			<div class="page-content">
				<section class="content bgcolor-1">
					<form id='pincode_edit' method="post" action="<?php echo base_url('index.php/common/pincode_crud/edit'); ?>">
						<div class="row rr">
							<div class="col-md-4 pull-left row form-group">
								<span class="input input--nao">
									<input class="input__field input__field--nao pincode_no" type="number" id="pincode_no"
									 name="pincode_no" value="<?php echo $pincode_no; ?>" 
									 data-validation=" required number length" 
                                     data-validation-length="4-6"
                                     data-validation-error-msg-required="Field should not be empty"
                                     data-validation-error-msg-custom="Enter Your correct pincode" />
									<label class="input__label input__label--nao" for="pincode-no">
										<span class="input__label-content input__label-content--nao">Pincode number</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
									<img src="<?php echo base_url('assets/images/loader/') ?>default.gif" class="ldr pin_ldr">
								</span>		
								<p class="pin_suc avail_er suc">Available</p>
								<p class="pin_err avail_er err">Already Exists</p>
								<?php echo form_error('pincode_no','<span class="help-block form-error">', '</span>'); ?>			
							</div>
									
							<div class="col-md-4 pull-left row form-group">
								<span class="input input--nao">
									<input class="input__field input__field--nao place" type="text" id="pincode-place"
									  value="<?php echo $place; ?>" name="place" 
									  data-validation="required custom length"
									 data-validation-length="2-100"
									 data-validation-regexp="^[A-Za-z][A-Za-z0-9-_\ .&amp;]+$"
									 data-validation-error-msg-custom="Starts with alphabet,No special chars"/>
									 <label class="input__label input__label--nao" for="pincode-place">
										<span class="input__label-content input__label-content--nao">Place</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
								</span>		
								<?php echo form_error('place','<span class="help-block form-error">', '</span>'); ?>			
							</div>
							<div class="col-md-4 pull-left row form-group">
								<span class="input input--nao">
									<input class="input__field input__field--nao district" type="text" id="pincode-district"
									  value="<?php echo $district; ?>" name="district" 
									  data-validation="required custom length"
									 data-validation-length="2-100"
									 data-validation-regexp="^[A-Za-z][A-Za-z0-9-_\ .&amp;]+$"
									 data-validation-error-msg-custom="Starts with alphabet,No special chars"/>
									 <label class="input__label input__label--nao" for="pincode-district">
										<span class="input__label-content input__label-content--nao">District</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
								</span>		
								<?php echo form_error('district','<span class="help-block form-error">', '</span>'); ?>			
							</div>
							<div class="col-md-4 pull-left row form-group">
								<span class="input input--nao">
									<input class="input__field input__field--nao state" type="text" id="pincode-state"
									 value="<?php echo $state; ?>" name="state" 
									 data-validation="required custom length"
									 data-validation-length="2-100"
									 data-validation-regexp="^[A-Za-z][A-Za-z0-9-_\ .&amp;]+$"
									 data-validation-error-msg-custom="Starts with alphabet,No special chars"/>
									 <label class="input__label input__label--nao" for="pincode-state">
										<span class="input__label-content input__label-content--nao">State</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
								</span>		
								<?php echo form_error('state','<span class="help-block form-error">', '</span>'); ?>			
							</div>
							<div class="col-md-4 pull-left row ">
								<select class="js-example-basic-single select2_style" name="country_id" id="combobox">
									<?php foreach ($country_obj as $list) {?>
										<option value="<?php echo $list->getCountryId();?>" <?php if($country_id == $list->getCountryId()){echo "selected='selected'";} ?>><?php echo $list->getCountryName();  ?></option>
									<?php  } ?>
								</select>
								<?php echo form_error('country_id','<span class="help-block form-error">', '</span>'); ?>			
							</div>
						</div>
						<div class="country_width_100 col-12 mt-5 ">
						<input type="hidden" name="edit_pincode_id" value="<?php echo $pincode_id; ?>">
							<div class="country_width_100">
								<div class="butt_sec_width mt-3 mb-3">
							      	<button type="submit" name="country_submit" class="country_button mr-2" >SUBMIT <i class="fa fa-paper-plane" aria-hidden="true"></i></button>		
								    <button type="reset" name="country_reset" class="country_button">RESET <i class="fa fa-refresh" aria-hidden="true"></i></button>
						      	
								</div>
					      	</div>
					      	
					        <div class="country_width_100 mt-3 mb-3">					      		  
							    <div class="country-right">
							      	<a href="<?php echo base_url('index.php/common/pincode_crud') ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button></a>
							    </div>	
							</div>
						</div>
					</form>
				</section>
			</div>
		</div>
	</div>
	<?php }else{ ?>
		<div class="dkbody">
			<div class="container-fluid">
				<div class="page-header">
					<h3>Select</h3>
				</div>
				<?php echo $search; ?>
			</div>
		</div>
		<script type="text/javascript">
			$("#combobox").select2();
		</script>
	<?php }  ?>



<?php }else{?>
	<a href="<?php echo base_url('index.php/common/country_crud/add') ?>"><button type="button" name="country_reset" class="country_button">ADD COUNTRY <i class="fa fa-refresh" aria-hidden="true"></i></button></a>
<?php } ?>

<script type="text/javascript">
	$("#combobox").select2();
	$.validate({
	    form:'#pincode_edit',
	});  
	$(function(){
		$('.pin_suc,.pin_err,.pin_ldr').hide();
	})
	$('.pincode_no').focus(function()  {
	  var a = $('.pincode_no').val();
	  if(a.length>0){
	    cc_name = a;
	  }else{
	    cc_name="";
	  }
	})
	$('.pincode_no').blur(function()  {
	    var pincode_no = $('.pincode_no').val();
	    if(pincode_no.length>4 && cc_name != pincode_no){
	      $('.pin_ldr').show();
	      $.ajax({
	        type: "POST",
	        url: "<?php echo base_url(); ?>index.php/Common_controller/pincode_available_on_update",
	        data: {'field_value':pincode_no,'pk':<?php echo $pincode_id; ?>},
	          success:  function (data) { 
	            $('.pin_ldr').hide();
	            if(data == 1){
	            	$('.pin_suc').show();
	            	$('.pin_err').hide();
	            }else{
	            	$('.pin_suc').hide();
	            	$('.pin_err').show();
	            }
	          }
	      })
	    }else if(pincode_no.length == 0 && cc_name.length > 2){
	     $('.pin_suc,.pin_err').hide();
	    }
  	})
  	$(document).on("click", "button[type='reset']", function(){
	 $('.pin_suc,.pin_err,.pin_ldr').hide();
	});
</script>
