<div class="dkbody">
	<div class="container-fluid">
		<div class="page-header gj_new_pack gj_msg_nw">
			<h3>New Message</h3>
		</div>
		<p class="gj_package_dets gj_msg_bot"><span><i class="fa fa-dropbox" aria-hidden="true"></i></span> Compose Message</p>

		<div class="page-content gj_msg_all">
			<section class="gj_msg_sec">
				<form method="post" action="<?php echo base_url('index.php/common/messages_crud/create_message'); ?>"  enctype="multipart/form-data">
					<div class="row rr">
						<div class="col-lg-12 col-md-12 pull-left">
							<p class="gj_s_frm">Tp <span class="gj_s_colon1">:</span><span class="gj_mail_addrs"><?php echo $user_name; ?></span></p>	
              <!-- <input type="hidden" name="user_name" value="<?php echo $user_name; ?>">   -->
							<input type="hidden" name="reply" value="<?php echo $to_id; ?>">	
						</div>
					</div>
					<div class="row rr">
						<div class="col-lg-12 col-md-12 pull-left pr-0 pl-0">
							<span class="input input--nao">
								<input class="input__field input__field--nao" type="text" id="subject"
								 name="subject"/>
								<label class="input__label input__label--nao" for="subject">
									<span class="input__label-content input__label-content--nao">Subject</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
							</span>		
						</div>
					</div>
					<div class="row rr">
						<div class="col-lg-12 col-md-12 pull-left">
							<div class="gj_text_edt nopadding">
								<p class="gj_com_msg">Message</p>
								<textarea id="txtEditor" name="message"></textarea> 
							</div>
						</div>
					</div>
					<hr>
					<p class="gj_pack_title">Attachments</p>
					<div class=" pkfile gj_file">
				      	<div class=" toppkfile" style="float:left">
				      		<input type="file"  name="files[]" id="files" class="form-control img-responsive"  multiple="multiple">
				     		 <div class="width-60px width-6px">

				     		 </div>
				      	</div>
			        </div>
					<div class="country_width_100 col-12 mt-5">
						<div class="country_width_100">
							<div class="butt_sec_width mt-3 mb-3">
						      	<button type="submit" name="moduleadd_submit" class="country_button mr-2" >send <i class="fa fa-paper-plane" aria-hidden="true"></i></button>		
							    <button type="reset" name="moduleadd_reset" class="country_button">Cancel <i class="fa fa-refresh" aria-hidden="true"></i></button>
					      	
							</div>
				      	</div>
				      	
				        <div class="country_width_100 mt-3 mb-3">					      		  
						    <div class="country-right">
						      	<a href="<?php echo base_url('index.php/common/messages_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button>
						    </div>	
						</div>
					</div>
					<input type="hidden" name="msg" value="msg">
				</form>
			</section>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$("#txtEditor").Editor();
	});
	
  	$( "form" ).submit(function() {
	 $('#txtEditor').val($('.Editor-editor').html())
	 $('#txtEditor').val($("#txtEditor").Editor("getText"))
	});
</script>
