<?php 
	if(isset($from_user)){
    if($from_user != null){
      $from_user = $from_user;
    }else{$from_user = "";}
  }else{$from_user = "";}
if(isset($from_user_id)){
		if($from_user_id != null){
			$from_user_id = $from_user_id;
		}else{$from_user_id = "";}
	}else{$from_user_id = "";}

  
  if(isset($date)){
    if($date != null){
      $date = $date->format('d-m-Y H:i:s');
    }else{$date = "";}
  }else{$date = "";}

	if(isset($to_user)){
		if($to_user != null){
			$to_user = $to_user;
		}else{$to_user = "";}
	}else{$to_user = "";} 

	if(isset($message)){
    if($to_user != null){
      $message = $message;
    }else{$message = "";}
  }else{$message="";}
if(isset($subject)){
		if($subject != null){
			$subject = $subject;
		}else{$subject = "";}
	}else{$subject="";}

	if(isset($id)){
		if($id != null){
			$id = $id;
		}else{$id = "";}
	}else{$id="";}

?>
<div class="dkbody">
	<div class="container-fluid">
		<div class="page-header gj_new_pack gj_msg_nw">
			<h3>Messages</h3>
		</div>
		<p class="gj_package_dets gj_msg_bot"><span><i class="fa fa-dropbox" aria-hidden="true"></i></span> Message View</p>

		<div class="page-content gj_msg_all">
			<section class="gj_msg_sec">
				<form method="post" action="<?php echo base_url('index.php/common/messages_crud/reply_message'); ?>">
					<div class="row rr">
						<div class="col-lg-12 col-md-12 pull-left">
							<p class="gj_s_frm">From <span class="gj_s_colon1">:</span><span class="gj_mail_addrs"><?php echo $from_user ?></span></p>	
							<input type="hidden" name="to_id" value="<?php echo $from_user_id ?>">	
						</div>
					</div>
					<div class="row rr">
            <div class="col-lg-12 col-md-12 pull-left">
              <p class="gj_s_frm">Date <span class="gj_s_colon2">:</span><span class="gj_mail_addrs">   <?php echo $date; ?></span></p>    
            </div>
          </div>
          <div class="row rr">
						<div class="col-lg-12 col-md-12 pull-left">
							<p class="gj_s_frm">Subject <span class="gj_s_colon2">:</span><span class="gj_mail_addrs"><?php echo $subject; ?></span></p>		
						</div>
					</div>
					<div class="row rr">
						<div class="col-lg-12 col-md-12 pull-left">
							<div class="gj_msg_bdy">
								<p class="gj_s_bdy">Message :</p>
								<p class="gj_s_msg_bdy"></p>
								<div class="full_msg">
									<?php echo $message; ?>
								</div>
							</div>
						</div>
					</div>
					<div class="row rr">
						<div class="col-lg-12 col-md-12 pull-left">
							<div class="gj_attach_div">
									<p class="gj_s_bdy">Attachment :</p>
                  <?php if(count($attachment > 0)){ ?>
                    <?php for($i=0;$i<count($attachment);$i++){ ?>
                          <?php if(! is_int($attachment[$i]->getAttachmentImageUrl())){ ?>
                            <?php if($attachment[$i]->getAttachmentImageUrl() != -1){ ?>
            									<div class="gj_msg_attachx">
                                <img src="<?php echo base_url($attachment[$i]->getAttachmentImageUrl()); ?>" alt="No Image Available" style="height: 190px;">
                                <a href="<?php echo base_url($attachment[$i]->getAttachmentImageUrl()); ?>" download style="width: 200px;float: left;">Click Here To Download</a>
            									</div>
                            <?php } ?>
                          <?php } ?>
                    <?php } ?>
									<?php } ?>
							</div>
						</div>
					</div>

          <?php //print_r($attachment); ?>

					<div class="country_width_100 col-12 mt-5">
						<div class="country_width_100">
							<div class="butt_sec_width mt-3 mb-3 gj_msg_reply">
						      	<button type="submit" name="moduleadd_submit" class="country_button mr-2" >Reply <i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>	
					</form>

					<form method="post" action="<?php echo base_url('index.php/common/messages_crud/delete_message'); ?>">	
						<input type="hidden" name="trash_id" value="<?php echo $id; ?>">
							    <button type="submit" name="moduleadd_reset" class="country_button">Delete <i class="fa fa-Trash-o" aria-hidden="true"></i></button>
					</form>
					      	
							</div>
				      	</div>
				      	
				        <div class="country_width_100 mt-3 mb-3">					      		  
						    <div class="country-right">
						      	<a href="<?php echo base_url('index.php/common/messages_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button>
						    </div>	
						</div>
					</div>
				
			</section>
		</div>
	</div>
</div>

<style type="text/css">
  .gj_msg_attachx {
    width: 20%;
    float: left;
}
</style>
