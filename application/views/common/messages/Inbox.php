<?php //print_r($messages); ?>
<div class="dkbody">
	<div class="container-fluid">
		<div class="page-header gj_new_pack gj_msg_nw">
			<h3>Messages</h3>
		</div>
		<p class="gj_package_dets gj_msg_bot"><span><i class="fa fa-dropbox" aria-hidden="true"></i></span> Inbox</p>

		<div class="page-content gj_msg_all">
			<section class="gj_msg_sec">
					<div class="row rr">
						<div class="col-lg-12 pull-left pl-0">
							<div class="gj_msg_head">
								<div class="col-lg-2 col-md-3 pull-left pr-0">
									<div class="gj_msg_comp">
										<p><a href="<?php echo base_url('index.php/common/Messages_crud/create_message'); ?>">Compose</a></p>
									</div>	
								</div>
								<div class="col-lg-10 col-md-9 pull-left pl-0 pr-0">
									<div class="gj_msg_act">
										<p>Action</p>
									</div>	
									<div class="gj_msg_from">
										<p>From</p>
									</div>
									<div class="gj_msg_subj">
										<p>Subject</p>
									</div>	
                  <div class="gj_msg_subj">
                    <p>Date</p>
                  </div>  
								</div>
							</div>
						</div>
						<div class="col-lg-2 col-md-3 pull-left pr-0">
							<div class="gj_msg_det">
								<ul class="gj_msg_ul">
									<li class="active"><a href="#">Inbox</a></li>
									<li><a href="<?php echo base_url('index.php/common/messages_crud/sent_items'); ?>">Sent</a></li>
									<!-- <li><a href="<?php echo base_url('index.php/common/messages_crud/draft'); ?>">Draft</a></li> -->
								</ul>
							</div>	
						</div>
						<div class="col-lg-10 col-md-9 pull-left pl-0">
							<div class="gj_msg_detts">
								<ul class="gj_msgs_ul">
									
									<?php if(count($messages) > 0){ ?>
										<?php foreach ($messages as $msg) { ?>
											<li class="gj_read">
												<form method="post" action="<?php echo base_url('index.php/common/messages_crud/view_inbox_message'); ?>">
												<a onclick="$(this).closest('form').submit();">
												<p class="gj_action_sel">
													<input type="checkbox" id="test1" class="msg_<?php echo $msg['user_messages_id']; ?>" value="<?php echo $msg['user_messages_id']; ?>" />
		    										<label for="test1"></label>
		    									</p>
												<p class="gj_from_adds"><?php echo $msg['user_email']; ?></p>
                        <p class="gj_msg_body"><?php if($msg['user_messages_subject'] != ''){echo $msg['user_messages_subject'];}else{echo'------------';} ?></p>
												<p class="gj_msg_bodyx"><?php echo $msg['user_messages_received_date']->format('d-m-Y H:i:s'); ?></p>
												<input type="hidden" name="hid_msg_id" value="<?php echo $msg['user_messages_id']; ?>">
												</a>
												</form>
											</li>
										<?php } ?>	
									<?php }else{ echo "Inbox Empty"; } ?>	

								</ul>
							</div>	

							<div class="country_width_100 mt-3 mb-3">  
						    <div class="country-right">
						      	<button type="button" name="back" class="country_button"><i class="fa fa-trash-o"></i> delete</button>
						    </div>	
						</div>

							<!-- <div class="gj_msg_pn text-center">
								<ul class="pagination">
								  	<li class="gj_nxt_pve"><a href="#"><i class="fa fa-angle-left"></i></a></li>
								  	<li class="active"><a href="#">1</a></li>
								  	<li><a href="#">2</a></li>
							  		<li><a href="#">3</a></li>
								  	<li><a href="#">4</a></li>
								  	<li><a href="#">5</a></li>
								  	<li class="gj_nxt_pve"><a href="#"><i class="fa fa-angle-right"></i></a></li>
								</ul>
							</div> -->
						</div>

						<div class="country_width_100 mt-3 mb-3 container-fluid">  
						    <div class="country-right">
						      	<button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button>
						    </div>	
						</div>
					</div>
			</section>
		</div>
	</div>
</div>
<style type="text/css">
  .gj_msg_bodyx{
    width: 16% !important;
    float: left;
  }
  .gj_msg_body{
    width: 32% !important;
  }
  .gj_msg_subj {
      width: 30%!important;
      margin-left: 1%;
      float: left;
  }
  .gj_msg_det ul.gj_msg_ul li.active, .gj_msg_det ul.gj_msg_ul li:hover {
      background-color: #ff4081;
      color: #fff !important;
      font-weight: bold;
  }
</style>