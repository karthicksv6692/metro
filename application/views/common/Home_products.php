<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/owl.theme.css'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/owl.carousel.css'); ?>">
<section class="gj_sec_pdts">
    <div class="container">
      <div class = "row">
        <div class = "col-md-12">
          <div>
            <p class="text-center dhinatechnologies_metrokings_headings pdt"> Our Products </p>
            <hr class="dhinatechnologies_metrokings_hr">  
          </div>
          <!--carousel-->
          <div>
            <div id="owl_demo_xx" class="owl-carousel ">
              <?php if(count($products) > 0){ ?>
                <?php foreach ($products as $prd) { ?>
                  <div class="item wow bounceInUp">
                    <div class="dhinatechnologies_metrokings_products_bg1">
                      <div class="dhinatechnologies_metrokings_products_bg2">
                        <p class="dhinatechnologies_metrokings_products_bundle"><?php echo $prd['product_name'] ?></p>
                      </div>
                      <div class="gj_pdts_imgs">
                        <?php if(!is_numeric($prd['product_image_url'])){ ?>
                          <img src="<?php echo base_url($prd['product_image_url']); ?>" class="img-responsive">
                        <?php }else{ ?>
                          <img src="<?php echo base_url('assets/images/no_image.jpg'); ?>" class="img-responsive">
                        <?php } ?>
                      </div>
                      <hr class="dhinatechnologies_metrokings_products_hr">
                      <div class="gj_pdts_dets">
                        <p class="gj_pdts_desc"><?php echo implode(',', $prd['product_desc']); ?></p>
                        <ol class="pad">
                          <li class="dhinatechnologies_metrokings_products_list2"> <span class="dhinatechnologies_metrokings_products_mrp"> MRP Rs.<s><?php echo $prd['product_price']; ?></s></span> <span class="pull-right dhinatechnologies_metrokings_products_dp"> DP Rs.<?php echo $prd['product_display_price']; ?> </span> </li>
                        </ol>
                      </div>
                    </div>
                  </div>
                <?php } ?>
                    
              <?php }else{echo"Product is Empty";} ?>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="text-right righ">
            <a href="<?php echo base_url('index.php/shopping'); ?>" target="_top" class="hvr-float-shadow"><input type="button" class="dhinatechnologies_metrokings_pdts_but1" value="VIEW MORE" id="view"/></a>
          </div>
        </div>
      </div>
    </div>
  </section>
 <script type="text/javascript" src="<?php echo base_url('assets/js/owl.carousel.js'); ?>"></script>
  <script> /* Product carousel */

    $(document).ready(function(){
      $("#owl_demo_xx").owlCarousel({
        items:4,
        loop:true,
        margin:10,
        autoplay:true,
        autoplayTimeout:2000,
          dots:false,
        nav:true,
        responsive:{
          0:{
            items:1
          },
          460:{
            items:2
          },
          760:{
            items:3
          },
          991:{
            items:4
          },
          1200:{
            items:4
          }
        }
      });
      $( ".owl-next").html('<i class="fa fa-chevron-right dhinatechnologies_metrokings_products_chevron dhinatechnologies_metrokings_products_right"></i>');
      $( ".owl-prev").html('<i class="fa fa-chevron-left dhinatechnologies_metrokings_products_chevron dhinatechnologies_metrokings_products_left"></i>');
    });
  </script>

  <style type="text/css">
    /*-- Products --*/
    /*.owl-item {
        float: left;
    }*/
    .gj_sec_pdts .dhinatechnologies_metrokings_products_bg1 {
      background-color: #f7f6f6;
      border-radius: 4px;
      margin: auto 10px;
      border: 1px solid #e6e6e6;
    }
    .gj_sec_pdts .prov_ln{
      line-height: 0.8;
    }
    .gj_sec_pdts .dhinatechnologies_metrokings_headings {
      font-size: 28px;
      color: #0688d4;
    }
    .gj_sec_pdts .pdt{
      margin-top: 20px;
    }
    .gj_sec_pdts .rd_none{
      display:none;
    }
    .gj_sec_pdts .rd_block{
      display:block;
    }
    .gj_sec_pdts .dhinatechnologies_metrokings_products_bg2 {
      background: -webkit-linear-gradient(right, rgba(247, 246, 246, 0.23), #0688d4);
      background: -o-linear-gradient(left, rgba(247, 246, 246, 0.23),  #0688d4);
      background: -moz-linear-gradient(left, rgba(247, 246, 246, 0.23),  #0688d4);
      background: linear-gradient(to left, rgba(247, 246, 246, 0.23), #0688d4);
      color: #fff;
      border-radius: 4px 0px;
      border-radius: 4px 0px;
    }
    .gj_sec_pdts .dhinatechnologies_metrokings_products_bundle {
      padding: 8px;
    }
    .gj_sec_pdts .dhinatechnologies_metrokings_products_img1 {
      padding: 0px 8px;
      width: 100%;
      height: 190px;
    }
    .gj_sec_pdts .dhinatechnologies_metrokings_products_hr {
      border-top: 2px solid #acacac;
      width: 90%;
    }
    .gj_sec_pdts .pad{
      padding-left:15px;
    }
    .gj_sec_pdts li.dhinatechnologies_metrokings_products_list1 {
      list-style: none;
      margin-bottom: 5px;
      font-family: roboto_regular;
    }
    .gj_sec_pdts li.dhinatechnologies_metrokings_products_list2 {
      list-style: none;
      font-size: 16px;
      padding-bottom: 5px;
    }
    .gj_sec_pdts span.dhinatechnologies_metrokings_products_mrp {
      color: #0688d4;
    }
    .gj_sec_pdts span.dhinatechnologies_metrokings_products_dp {
      margin-right: 15px;
      color: red
    }
    .gj_sec_pdts .dhinatechnologies_metrokings_products_chevron {
      position: absolute;
      width: 38px;
      height: 38px;
      background-color: rgba(118, 118, 118, 0.85);
      padding: 13px;
      color: #fff;
      border-radius: 4px;
    }
    .gj_sec_pdts .dhinatechnologies_metrokings_products_left {
      top: 273px;
      left: 10px;
    }
    .gj_sec_pdts .dhinatechnologies_metrokings_products_right {
      top: 273px;
      right: 10px;
    }
    .gj_sec_pdts p.dhinatechnologies_metrokings_products_bundle {
        height: 56px;
        overflow: hidden;
    }
    .gj_sec_pdts .gj_pdts_imgs {
        width: 100%;
        height: 241px;
        background-color: #f7f6f6;
        overflow: hidden;
    }
    .gj_sec_pdts p.gj_pdts_desc {
        font-family: roboto_regular;
        text-align: justify;
        padding: 0px 15px;
        height: 120px;
        overflow: hidden;
    }
    .gj_sec_pdts .dhinatechnologies_metrokings_pdts_but1 {
        width: 158px;
        height: 42px;
        background: #0087d0;
        color: #eef4fa;
        border-radius: 4px;
        outline: 0;
        border: 0;
        margin: 10px 10px 10px auto;
        padding: 10px !important;
    }
    .gj_sec_pdts .righ {
        margin-bottom: 20px;
    }

    /* Responsive */
    @media (min-width: 992px) and (max-width: 1199px){
      .gj_sec_pdts li.dhinatechnologies_metrokings_products_list2 {
          font-size: 14px;
      }
      .gj_sec_pdts .gj_pdts_imgs {
          height: 194px;
      }
    }
    @media (min-width: 768px) and (max-width: 991px){
      .gj_sec_pdts li.dhinatechnologies_metrokings_products_list2 {
          font-size: 14px;
      }
      .gj_sec_pdts .gj_pdts_imgs {
          height: 200px;
      }
    }
    @media (max-width: 767px){
      .gj_sec_pdts li.dhinatechnologies_metrokings_products_list2 {
          font-size: 12px;
      }
      .gj_sec_pdts .gj_pdts_imgs {
          height: 200px;
      }
    }
    @media (max-width: 460px){
      .gj_sec_pdts li.dhinatechnologies_metrokings_products_list2 {
          font-size: 14px;
      }
      .gj_sec_pdts .gj_pdts_imgs {
          height: 218px;
      }
      .gj_sec_pdts .dhinatechnologies_metrokings_pdts_but1 {
          width: 100px;
          height: 36px;
          padding: 6px !important;
      }
    }
    /* Responsive */
      </style>