<div class="dkbody">
  <div class="container-fluid">
    <div class="page-header">
      <h3>Send Message</h3>
    </div>
    <form method="post" action="<?php echo base_url('index.php/common/messages_crud/mobile_message_send'); ?>">
      <div class="row rr">
        <div class="col-lg-4 col-md-4 pull-left pr-0 pl-0">
          <div style="">
            <select class="type form-control" style="margin-left: 15px;margin-right: 25px; " name="type">
              <option value="0">All User</option>
              <option value="1">Individual</option>
            </select>
          </div>
        </div>

        <div class="col-lg-4 col-md-4 pull-left pr-0 pl-0 user">
          <div style="">
            <select class="user_id form-control" style="" name="user_id"></select>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-8">
          <textarea class="message form-control" name="message" rows="4"></textarea> 
        </div>
        <div class="col-md-2"></div>
      </div>
      
      <div class="row text-center" style="width:64%;overflow: hidden;">
        <input type="submit" value="SEND" class="country_button" style="float: none; margin: 0.5em auto 2em auto">
      </div>
    </form>
  </div>
</div>
<style type="text/css">
  select.form-control:not([size]):not([multiple]) {
    height: auto;
  }
  select.user_id.form-control,select.type.form-control {
      width: 80% !important;
  }
  .page-content section.content.bgcolor-1 form span.select2.select2-container.select2-container--default.select2-container--focus, span.select2.select2-container.select2-container--default.select2-container--below, span.select2.select2-container.select2-container--default {
      width: 300px !important;
      margin: 0px auto !important;
      text-align: left;
  }
</style>

<script type="text/javascript">
  $(document).ready(function(){
    $('.user').hide();
  })
  $('.type').change(function(){ 
    if($('.type').val() == 1){
      $('.user').show();  
      $('.user_id').select2({
        placeholder: 'Search With User Id',
        allowClear: true,
        ajax: {
          url: '<?php echo base_url("index.php/common_controller/get_all_user_id") ?>',
          dataType: 'json',
          delay: 1,
          data: function (params) {
                return { q: params.term 
                };
          },
          processResults: function (data) { return { results: data }; },
          cache: true
        },
      });
    }else{
      $('.user').hide();
    }
  })
</script>