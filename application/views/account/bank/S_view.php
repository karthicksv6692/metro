<?php if(isset($bank_obj)){

	if($bank_obj->getBankName() != null)	{
		$bank_name = $bank_obj->getBankName();
	}else{$bank_name = "";}

	if($bank_obj->getBankId() != null)	{
		$bank_id = $bank_obj->getBankId();
	}else{$bank_id = "";}

	if($bank_obj->getBankIfsc() != null)	{
		$ifsc = $bank_obj->getBankIfsc();
	}else{$ifsc = "";}

	if($bank_obj->getBankBranch() != null)	{
		$branch = $bank_obj->getBankBranch();
	}else{$branch = "";}

	if($bank_obj->getBankAddressId() != null)	{
		$full_address = $bank_obj->getBankAddressId()->get_Address_full_address();
	}else{$full_address = "";}

	if($bank_obj->getBankAddressId()->get_Address_pincode_id() != null)	{
		$pincode_no = $bank_obj->getBankAddressId()->get_Address_pincode_id()->getPincodeNo();
	}else{$pincode_no = "";}

	if($bank_obj->getBankAddressId()->get_Address_pincode_id() != null)	{
		$district = $bank_obj->getBankAddressId()->get_Address_pincode_id()->getPincodeDistrict();
	}else{$district = "";}

	if($bank_obj->getBankAddressId()->get_Address_pincode_id() != null)	{
		$state = $bank_obj->getBankAddressId()->get_Address_pincode_id()->getPincodeState();
	}else{$state = "";}

	if($bank_obj->getBankAddressId()->get_Address_pincode_id() != null)	{
		$country = $bank_obj->getBankAddressId()->get_Address_pincode_id()->getPincodeCountryId()->getCountryId();
	}else{$country = "";}

	if(isset($contact_obj)){

		if($contact_obj->getContactMobilePrimary() != null)	{
			$mobile = $contact_obj->getContactMobilePrimary();
		}else{$mobile = "";}

		if($contact_obj->getContactEmailId() != null)	{
			$email = $contact_obj->getContactEmailId();
		}else{$email = "";}

		if($contact_obj->getContactLandline() != null)	{
			$landline = $contact_obj->getContactLandline();
		}else{$landline = "";}


	}else{$mobile = "";$email = "";$landline = "";}

?>
	<div class="dkbody">

		<div class="container-fluid">
			<div class="page-header">
				<h3>Bank Details</h3>
			</div>
			<div class="page-content">
				<form method="post" action="<?php echo base_url('index.php/account_details/bank_crud/edit'); ?>">
					<section class="content bgcolor-1">
						<div class="col-md-6 pull-left table-responsive">
						<p class="bank_add_details"><span><i class="fa fa-institution" aria-hidden="true"></i></span> Bank Details</p>
							<table style="width: 100%" class="single_view_table ">
								<tr>
									<td class="single_table_view_head">IFSC Code</td>
									<td class="single_table_view_head">:</td>
									<td class="single_table_view_data"><?php echo $ifsc; ?></td>
								</tr>
								<tr>
									<td class="single_table_view_head">Bank Name</td>
									<td class="single_table_view_head">:</td>
									<td class="single_table_view_data"><?php echo $bank_name; ?></td>
								</tr>
								<tr>
									<td class="single_table_view_head">Bank Branch</td>
									<td class="single_table_view_head">:</td>
									<td class="single_table_view_data"><?php echo $branch; ?></td>
								</tr>
								
							</table>
						</div>
						<div class="col-md-6  pull-left">
						<p class="bank_add_details"><span><i class="fa fa-institution" aria-hidden="true"></i></span> Bank Communication Details</p>
							<div class="col-md-6 pull-left table-responsive">
								<table style="width: 100%" class="single_view_table ">
									<tr>
										<td class="single_table_view_head">Address</td>
										<td class="single_table_view_head">:</td>
										<td class="single_table_view_data"><?php echo $full_address; ?></td>
									</tr>
									<tr>
										<td class="single_table_view_head">Pincode</td>
										<td class="single_table_view_head">:</td>
										<td class="single_table_view_data"><?php echo $pincode_no; ?></td>
									</tr>
									<tr>
										<td class="single_table_view_head">District</td>
										<td class="single_table_view_head">:</td>
										<td class="single_table_view_data"><?php echo $district; ?></td>
									</tr>
									<tr>
										<td class="single_table_view_head">State</td>
										<td class="single_table_view_head">:</td>
										<td class="single_table_view_data"><?php echo $state; ?></td>
									</tr>
									
								</table>
							</div>
							<div class="col-md-6 pull-left table-responsive">
								<table style="width: 100%" class="single_view_table ">
									<tr>
										<td class="single_table_view_head">Country</td>
										<td class="single_table_view_head">:</td>
										<td class="single_table_view_data"><?php echo $country; ?></td>
									</tr>
									<tr>
										<td class="single_table_view_head">Phone No</td>
										<td class="single_table_view_head">:</td>
										<td class="single_table_view_data"><?php echo $landline; ?></td>
									</tr>
									<tr>
										<td class="single_table_view_head">Mobile No</td>
										<td class="single_table_view_head">:</td>
										<td class="single_table_view_data"><?php echo $mobile; ?></td>
									</tr>
									<tr>
										<td class="single_table_view_head">Email ID</td>
										<td class="single_table_view_head">:</td>
										<td class="single_table_view_data"><?php echo $email; ?></td>
									</tr>
									
								</table>
							</div>
							<input type="hidden" name="bank_id" value="<?php echo $bank_id; ?>">
						</div>
						<div class="country_width_100 col-12 mt-5">
								<div class="country_width_100">
									<div class="butt_sec_width mt-3 mb-3">
								      	<a href="<?php echo base_url('index.php/account_details/bank_crud'); ?>"><button type="submit" name="country_submit" class="country_button mr-2" >EDIT <i class="fa fa-pencil" aria-hidden="true"></i></button>		
									    <a href="<?php echo base_url('index.php/account_details/bank_crud'); ?>"><button type="button" name="country_reset" class="country_button">CANCEL <i class="fa fa-trash" aria-hidden="true"></i></button></a>
							      	
									</div>
						      	</div>
						      	
						        <div class="country_width_100 mt-3 mb-3">					      		  
								    <div class="country-right">
								      	<a href="<?php echo base_url('index.php/account_details/bank_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button></a>
								    </div>	
								</div>
							</div>
					</section>
			</div>
		</div>
	</div>
<?php }else{ ?>
	<div class="dkbody">
		<div class="container-fluid">
			<div class="page-header">
				<h3>Select</h3>
			</div>
			<?php echo $search; ?>
		</div>
	</div>
	<script type="text/javascript">
		$("#combobox").select2();
	</script>
<?php }  ?>