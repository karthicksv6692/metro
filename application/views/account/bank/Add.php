<?php if(isset($country_list) && count($country_list)>0){ ?>
	<div class="dkbody">
		<div class="container-fluid">
			<div class="page-header">
				<h3>Bank Details</h3>
			</div>
			<p class="bank_add_details"><span><i class="fa fa-institution" aria-hidden="true"></i></span> Bank Details</p>
			<form method="post" action="<?php echo base_url('index.php/account_details/bank_crud/add'); ?>" id="bank_add">
				<div class="page-content">
					<section class="content bgcolor-1">
						<form id='bankadd'>
							<div class="row rr">
								<div class="col-md-4 pull-left row form-group">
									<span class=" input input--nao">
										<input class="input__field input__field--nao ifsc" type="text" id="bank_add_ifcs_code"
										 name="ifsc" value="<?php echo set_value('ifsc'); ?>" 
										 data-validation="required custom length"
										 data-validation-length="min3"
										 data-validation-regexp="^[a-zA-Z0-9]*$"
										 data-validation-error-msg-custom="Please provide valid IFSC"
										 />
										<label class="input__label input__label--nao" for="bank_add_ifcs_code">
											<span class="input__label-content input__label-content--nao">IFSC Code</span>
										</label>
										<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
											<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></path>
										</svg>
										<img src="<?php echo base_url('assets/images/loader'); ?>/default.gif" class="ldr ifsc_ldr">
									</span>		
									<p class="ifsc_suc avail_er suc">Available</p>
									<p class="ifsc_err avail_er err">Already Exists</p>
									<?php echo form_error('ifsc','<span class="help-block form-error">', '</span>'); ?>	
								</div>
								<div class="col-md-4 pull-left row form-group">
									<span class="input input--nao">
										<input class="input__field input__field--nao" type="text" id="bank_add_name"
										name="bank_name" value="<?php echo set_value('bank_name'); ?>" 
										data-validation="required custom length"
										data-validation-length="2-100"
										data-validation-regexp="^[A-Za-z][A-Za-z0-9-_\ .&amp;]+$"
										data-validation-error-msg-custom="Please provide valid bank name"
										/>
										<label class="input__label input__label--nao" for="bank_add_name">
											<span class="input__label-content input__label-content--nao">Bank Name</span>
										</label>
										<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
											<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></path>
										</svg>
									</span>		
									<?php echo form_error('bank_name','<span class="help-block form-error">', '</span>'); ?>	
								</div>
								<div class="col-md-4 pull-left row form-group">
									<span class="input input--nao">
										<input class="input__field input__field--nao" type="text" id="bank_add_branch"
										 name="branch"  value="<?php echo set_value('branch'); ?>"
										 data-validation="required custom length"
										 data-validation-length="2-100"
										 data-validation-regexp="^[A-Za-z][A-Za-z0-9-_\ .&amp;]+$"
										 data-validation-error-msg-custom="Starts with alphabet,No special chars"
										 />
										<label class="input__label input__label--nao" for="bank_add_branch">
											<span class="input__label-content input__label-content--nao">Bank Branch</span>
										</label>
										<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
											<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></path>
										</svg>
									</span>		
									<?php echo form_error('branch','<span class="help-block form-error">', '</span>'); ?>	
								</div>
							</div>
							
							<p class="bank_add_details"><span><i class="fa fa-institution" aria-hidden="true"></i></span> Bank Communication Details</p>
							<div class="row rr">
								<div class="col-md-4 pull-left row form-group">
									<span class="input input--nao">
										<input class="input__field input__field--nao" type="text" id="bank_add_address"
										 name="full_address"  value="<?php echo set_value('full_address'); ?>" 
										data-validation="length"
										data-validation-length="min10"
										 data-validation-error-msg-custom="Please provide valid address"
										 />
										<label class="input__label input__label--nao" for="bank_add_address">
											<span class="input__label-content input__label-content--nao">Address</span>
										</label>
										<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
											<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></path>
										</svg>
									</span>		
									<?php echo form_error('full_address','<span class="help-block form-error">', '</span>'); ?>	
								</div>
								<div class="col-md-2 pull-left row form-group">
									<span class="input input--nao">
										<input class="input__field input__field--nao pincode_no" type="number" id="bank_add_pincode"
										 name="pincode_no" value="<?php echo set_value('pincode_no'); ?>" 
										 data-validation=" required number length" 
	                                     data-validation-length="4-6"
	                                     data-validation-error-msg-custom="Enter Your correct pincode"
	                                     />
										<label class="input__label input__label--nao" for="bank_add_pincode">
											<span class="input__label-content input__label-content--nao">Pincode</span>
										</label>
										<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
											<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></path>
										</svg>
									</span>		
									<?php echo form_error('pincode','<span class="help-block form-error">', '</span>'); ?>	
								</div>
								<div class="col-md-2 pull-left row form-group">
									<span class="input input--nao district_lbl">
										<input class="input__field input__field--nao district" type="text" id="bank_add_district"
										name="district"  value="<?php echo set_value('district'); ?>"
										data-validation="required custom length"
										data-validation-length="2-100"
										data-validation-regexp="^[A-Za-z][A-Za-z0-9-_\ .&amp;]+$"
										data-validation-error-msg-custom="Please provide valid district"
										/>
										<label class="input__label input__label--nao" for="bank_add_district">
											<span class="input__label-content input__label-content--nao">District</span>
										</label>
										<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
											<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></path>
										</svg>
									</span>		
									<?php echo form_error('district','<span class="help-block form-error">', '</span>'); ?>	
								</div>
								<div class="col-md-2 pull-left row form-group">
									<span class="input input--nao state_lbl">
										<input class="input__field input__field--nao state" type="text" id="bank_add_state"
										name="state" value="<?php echo set_value('state'); ?>" 
										data-validation="required custom length"
										data-validation-length="2-100"
										data-validation-regexp="^[A-Za-z][A-Za-z0-9-_\ .&amp;]+$"
										data-validation-error-msg-custom="Please provide valid state"
										/>
										<label class="input__label input__label--nao" for="bank_add_state">
											<span class="input__label-content input__label-content--nao">State</span>
										</label>
										<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
											<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></path>
										</svg>
									</span>	
									<?php echo form_error('state','<span class="help-block form-error">', '</span>'); ?>		
								</div>
								<div class="col-md-2 pull-left row form-group">
									<select class="js-example-basic-single select2_style country" name="country" id="combobox country">
										<?php foreach ($country_list as $list) {?>
											<option value="<?php echo $list->getCountryId();  ?>" <?php if(set_value('country_id') == $list->getCountryId()){echo "selected='selected'";} ?>><?php echo $list->getCountryName();  ?></option>
										<?php  } ?>
									</select>
									<?php echo form_error('country','<span class="help-block form-error">', '</span>'); ?>		
								</div>
								<div class="col-md-2 pull-left row form-group">
									<span class="input input--nao">
										<input class="input__field input__field--nao" type="number" id="bank_add_phonenumber"
										 name="landline" value="<?php echo set_value('landline'); ?>"/>
										<label class="input__label input__label--nao" for="bank_add_phonenumber">
											<span class="input__label-content input__label-content--nao">Phone Number</span>
										</label>
										<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
											<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></path>
										</svg>
									</span>	
									<?php echo form_error('landline','<span class="help-block form-error">', '</span>'); ?>		
								</div>
								<div class="col-md-2 pull-left row form-group">
									<span class="input input--nao">
										<input class="input__field input__field--nao" type="number" id="bank_add_mobilenumber"
										 name="mobile" value="<?php echo set_value('mobile'); ?>"  
										 data-validation=" required number length" 
		                                 data-validation-length="10-12" />
										<label class="input__label input__label--nao" for="bank_add_mobilenumber">
											<span class="input__label-content input__label-content--nao">Mobile Number</span>
										</label>
										<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
											<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></path>
										</svg>
									</span>	
									<?php echo form_error('mobile','<span class="help-block form-error">', '</span>'); ?>
								</div>
								<div class="col-md-2 pull-left row form-group">
									<span class="input input--nao">
										<input class="input__field input__field--nao" type="email" id="bank_add_emailid"
										 name="email" value="<?php echo set_value('email'); ?>"
										 data-validation="required email"
										 />
										<label class="input__label input__label--nao" for="bank_add_emailid">
											<span class="input__label-content input__label-content--nao">Email ID</span>
										</label>
										<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
											<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></path>
										</svg>
									</span>		
									<?php echo form_error('email','<span class="help-block form-error">', '</span>'); ?>
								</div>
							</div>
							<input type="hidden" name="bank" value="asdf">
							<div class="country_width_100 col-12 mt-5">
								<div class="country_width_100">
									<div class="butt_sec_width mt-3 mb-3">
								      	<button type="submit" name="country_submit" class="country_button mr-2" >SUBMIT <i class="fa fa-paper-plane" aria-hidden="true"></i></button>		
									    <button type="reset" name="country_reset" class="country_button">RESET <i class="fa fa-refresh" aria-hidden="true"></i></button>
							      	
									</div>
						      	</div>
						        <div class="country_width_100 mt-3 mb-3">					      		  
								    <div class="country-right">
								      	<a href="<?php echo base_url('index.php/account_details/bank_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button>
								    </div>	
								</div>
							</div>
						</form>
					</section>
				</div>
			</form>
		</div>
	</div>
	<?php 
		$pincode_array = array();
		foreach ($pincode_list as $list) {  
			array_push($pincode_array, $list->getPincodeNo());
		}
		function js_str($s) { return '"' . addcslashes($s, "\0..\37\"\\") . '"'; }
		function js_array($array) { 
			$temp = array_map('js_str', $array);
			return '[' . implode(',', $temp) . ']';
		}
	?>
<style type="text/css">
	.page-content section.content.bgcolor-1 form span.select2.select2-container.select2-container--default.select2-container--focus, span.select2.select2-container.select2-container--default.select2-container--below, span.select2.select2-container.select2-container--default {
	    width: 300px !important;
	    margin-top: 45px !important;
	    margin-bottom: 0px !important;
	    text-align: left;
	    margin-top: !IMPORTANT;
	}
</style>
<script type="text/javascript">
	$.validate({
	    form:'#bank_add',
	});  
	$(function(){
		$('.ifsc_suc,.ifsc_err,.ifsc_ldr').hide();
		$(".country").select2();
		//$('.district,.state,.country').attr('disabled', 'disabled');
	})
	$( function() { 
      	$( ".pincode_no" ).autocomplete({
        	source: <?php echo  js_array($pincode_array);?>
      	});
    });
	$('.ifsc').focus(function()  {
	  var a = $('.ifsc').val();
	  if(a.length>0){
	    cc_name = a;
	  }else{
	    cc_name="";
	  }
	})
	$('.ifsc').blur(function()  {
	    var ifsc = $('.ifsc').val();
	    if(ifsc.length>3 && cc_name != ifsc){
	      $('.ifsc_ldr').show();
	      $.ajax({
	        type: "POST",
	        url: "<?php echo base_url(); ?>index.php/Common_controller/ifsc_available",
	        data: {'field_value':ifsc},
	          success:  function (data) { 
	            $('.ifsc_ldr').hide();
	            if(data == 1){
	            	$('.ifsc_suc').show();
	            	$('.ifsc_err').hide();
	            }else{
	            	$('.ifsc_suc').hide();
	            	$('.ifsc_err').show();
	            }
	          }
	      })
	    }else{
	     $('.ifsc_suc,.ifsc_err').hide();
	    }
  	})
  	//Pincode Ajax Start
	    $('.pincode_no').blur(function()  {
	  		var pincode = $('.pincode_no').val();
	      	if(pincode.length > 0){
		      	$.ajax({
		        type: "POST",
		        url: "<?php echo base_url(); ?>index.php/Common_controller/ajax_pincode",
		        data: {'pincode':pincode},
	          	success:  function (data) {
	          		if(data != 0){
			            var option = JSON.parse(data);
			            console.log(option);
			            if(option[2]['pincode_district'] == false){
			              $('.district').val("");
			              //$('.district').attr('disabled', 'disabled');
			            }else{
			              $('.district_lbl').addClass('input--filled');
			              $('.district').val(option[2]['pincode_district']).removeAttr('disabled');
			              $('.district').attr('disabled', false);
			            }
			            if(option[3]['pincode_state'] == false){
			              $('.state').val("");
			              //$('.state').attr('disabled', 'disabled');
			            }else{
			              $('.state_lbl').addClass('input--filled');
			              $('.state').attr('disabled', 'false');
			              $('.state').val(option[3]['pincode_state']).removeAttr('disabled');
			            }
			            if(option[4]['pincode_country'] == false){
			              $('.country').val("");
			              //$('.country').attr('disabled', 'disabled');
			            }else{
			              	$('.country_lbl').addClass('input--filled');
			              	$('.country').val(option[6]['country_id']).trigger('change');  
			            }
			        }else{
				      	$('.district,.state,.country').val('');
			        	$('.state_lbl,.district_lbl,.country_lbl').removeClass('input--filled');
				      	// $('.district,.state,.country').attr('disabled', 'disabled');
			        }
	          	}
	      	});
	      	}else{
	      		$('.state_lbl,.district_lbl,.country_lbl').removeClass('input--filled');
	      	 	// $('.district,.state,.country').attr('disabled', 'disabled');
	      	 	$('.district,.state,.country').val('');
	      	}
	    });
  	//Pincode Ajax End
</script>

<?php }else{?>
	<a href="<?php echo base_url('index.php/common/country_crud/add') ?>"><button type="button" name="country_reset" class="country_button">ADD COUNTRY <i class="fa fa-refresh" aria-hidden="true"></i></button></a>
<?php } ?>
<script type="text/javascript">
	$("#combobox").select2();
	$(document).on("click", "button[type='reset']", function(){
	 $('.ifsc_suc,.ifsc_err,.ifsc_ldr').hide();
	});
</script>