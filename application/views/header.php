<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	$this->view('includes/header');
?>
<body>
	<div class="topper">
		<div class="logo ripple">
			<img src="<?php echo base_url('assets/')?>images/logo.png" class="img-fluid">
		</div>
		
		<div class="menu-btn ripple">
			<a class="menu-button" id="open-button">
				<hr class="mbar"/>
				<hr class="mbar"/>
				<hr class="mbar"/>
			</a>
		</div>

		<div class="right-menu float-right">
	        <div class="dropdown malarm ">
			  <div  class="dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			    <i class="fa fa-bell"></i><span class="badge">8</span>
			  </div>
			  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu2">
			    <a class="dropdown-item">
			    	<div>
	                    <i class="fa fa-comment fa-fw"></i> New Comment
	                    <span class="pull-right text-muted small">4 minutes ago</span>
	                </div>
			    </a>
			    <a class="dropdown-item">
			    	<div>
	                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
	                    <span class="pull-right text-muted small">12 minutes ago</span>
	                </div>
			    </a>
			    <a class="dropdown-item">
			    	 <a class="text-center" href="#" style="display: block;">
	                    <strong class="seeall">See All Alerts</strong>
	                    <i class="fa fa-angle-right"></i>
	                </a>
			    </a>
			  </div>
			</div>
			<div class="dropdown mumenu ">
			  <a  class="dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

			  	<?php 
		    		if(isset($_SESSION)){
		    			if(isset($_SESSION['profile_picture_url'])){ ?>
		    				<?php if(isset($_SESSION['profile_picture_url']) != null){ ?>
		    					<img src="<?php echo base_url()?><?php echo $_SESSION['profile_picture_url']; ?>" class="img-responsive">
		    				<?php }else{ ?>
			    				<img src="<?php echo base_url('assets/')?>images/user.jpg" class="img-responsive">
			    			<?php } ?>
		    			<?php }else{ ?>
		    				<img src="<?php echo base_url('assets/')?>images/user.jpg" class="img-responsive">
		    			<?php } ?>
		    		<?php }else{ ?>
		    			<img src="<?php echo base_url('assets/')?>images/user.jpg" class="img-responsive">
		    		<?php }
		    	?>

			    <span class="username">
			    	<?php 
			    		if(isset($_SESSION)){
			    			if(isset($_SESSION['username'])){
			    				echo $_SESSION['username'];
			    			}else{echo "No Name";}
			    		}else{echo "No Name";}
			    	?>

			    </span>
			  </a>
			  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu2">
			    <a class="dropdown-item" href="<?php echo base_url('index.php/login/login_user'); ?>">My Profile</a>
			    <a class="dropdown-item">Settings</a>
			    <li class="divider"></li>
			    <a href="<?php echo base_url('index.php/login/logout'); ?>" class="dropdown-item">Logout</a>
			  </div>
			</div>
		</div>
	</div>
	
	<?php $this->view('includes/menu'); ?>

	<div class="dk_body" id="dk_body">


  <?php if(isset($_SESSION['notification'])){ ?>
    <div class="noty">
      <marquee><p><?php echo $_SESSION['notification']; ?></p></marquee>
    </div>
  <?php } ?>



    <style type="text/css">
      .noty {
          width: 100%;
          overflow: hidden;
          height: 55px;
          margin-top: 10px;
          background: #f5105e;
          color: #fff;
          font-size: 35px;
          font-weight: bold;
      }
    </style>