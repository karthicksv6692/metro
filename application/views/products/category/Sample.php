<div style="width:520px;margin:0px auto;margin-top:30px;height:500px;">
  <h2>Select Box with Search Option Jquery Select2.js</h2>
  <select class="itemName form-control" style="width:500px" name="itemName"></select>
</div>


<script type="text/javascript">
      $('.itemName').select2({
        placeholder: 'Select an item',
        ajax: {
          url: '<?php echo base_url("index.php/common_controller/sample_ajax_select") ?>',
          dataType: 'json',
          delay: 1000,

          data: function (params) {console.log(params);
                return {
                    q: params.term // search term
                };
          },
          processResults: function (data) {
              // parse the results into the format expected by Select2.
              // since we are using custom formatting functions we do not need to
              // alter the remote JSON data
              return {
                  results: data
              };
          },
          cache: true
        },
        // minimumInputLength: 1
      });




     
</script>