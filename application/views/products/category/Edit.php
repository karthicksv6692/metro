<?php if(isset($cat_obj)){
        if($cat_obj != null){
          $cat_id   = $cat_obj->getCategoryId();
          $cat_name = $cat_obj->getCategoryName();
          $cat_desc = $cat_obj->getCategoryDesc();
          $parent_id = $cat_obj->getParentId();
        }else{
          $cat_id   = "";$cat_name  = "";$cat_desc = "";$parent_id = "";
        }
 ?>
  <div class="dkbody">
    <div class="container-fluid">
      <div class="page-header">
        <h3>Category - Edit</h3>
      </div>
      <form method="post" action="<?php echo base_url('index.php/products/category_crud/edit'); ?>" id="module_add">
        <div class="page-content">
          <section class="content bgcolor-1">
            <form id='bankadd'>
              <div class="row">
                <div class="col-md-5 row form-group">
                  <span class="input input--nao">
                    <input class="input__field input__field--nao category_name brdr_bt_clr" type="text" id="module_add_name"
                     name="category_name" value="<?php echo $cat_name; ?>" 
                      data-validation="required length alphanumeric"
                                data-validation-length="min3"
                                data-validation-ignore="-_ &,.%*!@()/" />
                    <label class="input__label input__label--nao" for="module_add_name">
                      <span class="input__label-content input__label-content--nao">Category Name</span>
                    </label>
                    <img src="<?php echo base_url('assets/images/loader/') ?>default.gif" class="ldr pin_ldr">
                  </span>   
                  <p class="pin_suc avail_er suc">Available</p>
                  <p class="pin_err avail_er err">Already Exists</p>
                  <?php echo form_error('category_name','<span class="help-block form-error">', '</span>'); ?>
                </div>
  
                <div class="col-md-5 row form-group">
                  <select class="itemName form-control" name="parent_id" style="float: left">
                    <?php if($parent_obj['parent_id'] != null){ ?>
                        <option value="<?php echo $parent_obj['parent_id']; ?>"><?php echo $parent_obj['parent_name']; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="row">
              <div class="col-md-10 row pull-left">
                <span class="input input--nao">
                  
                   <textarea class="input__field input__field--nao brdr_bt_clr" rows="3" id="module_add_description" name="category_desc" value="<?php echo $cat_desc; ?>"><?php echo $cat_desc; ?></textarea >
                  <label class="input__label input__label--nao" for="module_add_description">
                    <span class="input__label-content input__label-content--nao">Category Description</span>
                  </label>
                  
                </span> 
                <?php echo form_error('category_desc','<span class="help-block form-error">', '</span>'); ?>  
              </div>
              </div>
              <input type="hidden" name="edit_category_id" value="<?php echo $cat_id; ?>">
              
              <input type="hidden" name="cat" value="asdf">
              <div class="country_width_100 col-12 mt-5">
                <div class="country_width_100">
                  <div class="butt_sec_width mt-3 mb-3">
                        <button type="submit" name="moduleadd_submit" class="country_button mr-2" >SUBMIT <i class="fa fa-paper-plane" aria-hidden="true"></i></button>   
                      <button type="reset" name="moduleadd_reset" class="country_button">RESET <i class="fa fa-refresh" aria-hidden="true"></i></button>
                      
                  </div>
                    </div>
                    
                    <div class="country_width_100 mt-3 mb-3">                     
                    <div class="country-right">
                        <a href="<?php echo base_url('index.php/products/category_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button></as>
                    </div>  
                </div>
              </div>
            </form>
          </section>
        </div>
      </form>
    </div>
  </div>

  <script type="text/javascript">
    $.validate({
        form:'#module_add',
    });  
    $(function(){
      $('.pin_suc,.pin_err,.pin_ldr').hide();
    })
    $('.category_name').focus(function()  {
      var a = $('.category_name').val();
      if(a.length>0){
        cc_name = a;
      }else{
        cc_name="";
      }
    })
    $('.category_name').blur(function()  {
        var category_name = $('.category_name').val();
        if(category_name.length>4 && cc_name != category_name){
          $('.pin_ldr').show();
          $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>index.php/Common_controller/category_name_available_on_update",
            data: {'field_value':category_name,'pk':<?php echo $cat_id; ?>},
              success:  function (data) { 
                $('.pin_ldr').hide();
                if(data == 1){
                  $('.pin_suc').show();
                  $('.pin_err').hide();
                }else{
                  $('.pin_suc').hide();
                  $('.pin_err').show();
                }
              }
          })
        }else if(category_name.length == 0 && cc_name.length > 2){
         $('.pin_suc,.pin_err').hide();
        }
    })
    $(document).on("click", "button[type='reset']", function(){
    $('.pin_suc,.pin_err,.pin_ldr').hide();
    });

    $(document).ready(function () {
      var $select = $('.itemName');
      var id = <?php echo $parent_id; ?>;
      $select.val(id).trigger('change.select2');
    })

    $('.itemName').select2({
      placeholder: 'Select a Parent Category',
      ajax: {
        url: '<?php echo base_url("index.php/common_controller/get_category_names_on_edit") ?>',
        dataType: 'json',
        delay: 1,
        data: function (params) {
          return { q: params.term,id : <?php echo $cat_id; ?>
          };
        },
        
        processResults: function (data) { console.log(data); return { results: data }; },
        cache: true
      },
      allowClear: true,
      // minimumInputLength: 1
    });

  //  $('.itemName').select2('data', {id: 1, text: 'Beautiful.Home'});

    // $(".itemName").val('Beautiful.Home').trigger('change');
    // $(".itemName").val(<?php echo $parent_id; ?>).trigger('change');
    // <?php if($parent_id != null){ ?>
      
    // <?php }else{ ?>
    //     $(".itemName").val(1).trigger('change');
    //     $(".itemName").select2()
    // <?php } ?>
  </script>
  <style type="text/css">
    .page-content section.content.bgcolor-1 form span.select2.select2-container.select2-container--default.select2-container--focus, span.select2.select2-container.select2-container--default.select2-container--below, span.select2.select2-container.select2-container--default {
      width: 300px !important;
      margin: 39px 15px!important;
      text-align: left;
  }
  .brdr_bt_clr{
    border-bottom: 1px solid #9ca1a7 !important;
  }
  </style>

<?php }else{ ?>
  <div class="dkbody">
    <div class="container-fluid">
      <div class="page-header"> <h3>Select</h3> </div>
      <?php echo $search; ?>
    </div>
  </div>
  <script type="text/javascript"> $("#combobox").select2(); </script>
<?php } ?>