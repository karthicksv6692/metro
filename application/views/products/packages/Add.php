<div class="dkbody">
	<div class="container-fluid">
		<div class="page-header gj_new_pack">
			<h3>New Package</h3>
		</div>
		<p class="gj_package_dets"><span><i class="fa fa-dropbox" aria-hidden="true"></i></span> Package Details</p>
		<div class="page-content gj_pack_all">
			<section class="gj_pack_sec">
				<form method="post" acion="<?php echo base_url('index.php/products/packages_crud/add'); ?>" id="new_package_add" enctype="multipart/form-data">					
					<div class="row rr">
						<div class="col-lg-3 col-md-3 pull-left pr-0 pl-0 form-group">
							<span class="input input--nao">
								<input class="input__field input__field--nao" type="text" id="product_add_product_sku"
								value="<?php echo set_value('package_name');?>" name="package_name" 
								data-validation="required"
								/>
								<label class="input__label input__label--nao" for="product_add_product_sku">
									<span class="input__label-content input__label-content--nao">Package Name</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
								<img src="<?php echo base_url('assets/images/loader'); ?>/default.gif" class="ldr pin_ldr umail_ldr">
							</span>
							<p class="umail_suc avail_er suc">Available</p>
							<p class="umail_err avail_er err">Already Exists</p>
							<?php echo form_error('package_name','<span class="help-block form-error">', '</span>'); ?>		
						</div>

						<div class="col-lg-3 col-md-3 pull-left pr-0 form-group" style="display: none">
							<span class="input input--nao">
								<input class="input__field input__field--nao" type="number" id="product_add_product_name"
								 value="99" name="package_price" 
								 data-validation="required number length"
								 data-validation-length="min2"
								 data-validation-ignore="."
								 />
								<label class="input__label input__label--nao" for="product_add_product_name">
									<span class="input__label-content input__label-content--nao">Package Price</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
							</span>	
							<?php echo form_error('package_price','<span class="help-block form-error">', '</span>'); ?>
						</div>

            <div class="col-lg-3 col-md-3 pull-left pr-0 form-group">
              <span class="input input--nao">
                <input class="input__field input__field--nao" type="number" id="product_add_product_name"
                 value="<?php echo set_value('point_value');?>" name="point_value" 
                 data-validation="required number length"
                 data-validation-length="min2"
                 data-validation-ignore="."
                 />
                <label class="input__label input__label--nao" for="product_add_product_name">
                  <span class="input__label-content input__label-content--nao">Point Value</span>
                </label>
                <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                  <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                </svg>
              </span> 
              <?php echo form_error('point_value','<span class="help-block form-error">', '</span>'); ?>
            </div>
            <div class="col-lg-3 col-md-3 pull-left pr-0 form-group">
              <span class="input input--nao">
                <input class="input__field input__field--nao" type="number" id="product_add_level"
                 value="<?php echo set_value('level_income');?>" name="level_income" 
                 data-validation="required number length"
                 data-validation-length="min2"
                 data-validation-ignore="."
                 />
                <label class="input__label input__label--nao" for="product_add_level">
                  <span class="input__label-content input__label-content--nao">Level Income</span>
                </label>
                <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                  <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                </svg>
              </span> 
              <?php echo form_error('level_income','<span class="help-block form-error">', '</span>'); ?>
            </div>

						<div class="col-lg-3 col-md-3 pull-left pr-0 form-group">
							<span class="input input--nao">
								<input class="input__field input__field--nao" type="number" id="product_add_product_name"
								 value="<?php echo set_value('limited_pair_cutoff');?>" name="limited_pair_cutoff" 
								 data-validation="required number length"
								 data-validation-length="min1"
								 data-validation-ignore="."
								 />
								<label class="input__label input__label--nao" for="product_add_product_name">
									<span class="input__label-content input__label-content--nao">Limitted Per cutoff</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
							</span>	
							<?php echo form_error('limited_pair_cutoff','<span class="help-block form-error">', '</span>'); ?>	
						</div>
					</div>
					<div class="row rr">

						<div class="col-lg-4 col-md-4 pull-left pr-0 pl-0 form-group" style="display: none">
							<span class="input input--nao">
								<input class="input__field input__field--nao" type="number" id="product_add_product_quantity"
								value="99" name="pair_income" 
								data-validation="required number length"
								 data-validation-length="min2"
								 data-validation-ignore="."
								/>
								<label class="input__label input__label--nao" for="product_add_product_quantity">
									<span class="input__label-content input__label-content--nao">Point Value</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
							</span>
							<?php echo form_error('pair_income','<span class="help-block form-error">', '</span>'); ?>	
						</div>

						<div class="col-lg-4 col-md-4 pull-left pr-0 form-group" style="display: none"
							<span class="input input--nao">
								<input class="input__field input__field--nao" type="number" id="product_add_product_price" value="<?php echo set_value('package_ceiling_price');?>" name="package_ceiling_price"
								data-validation="required number length"
								 data-validation-length="min2"
								 data-validation-ignore="."
								 />
								<label class="input__label input__label--nao" for="product_add_product_price">
									<span class="input__label-content input__label-content--nao">Ceiling Price</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
							</span>		
							<?php echo form_error('package_ceiling_price','<span class="help-block form-error">', '</span>'); ?>
						</div>

						<div class="col-lg-4 col-md-4 pull-left pr-0 form-group" style="display: none">
							<span class="input input--nao">
								<input class="input__field input__field--nao" type="number" id="product_add_product_reorder_level" value="<?php echo set_value('package_ceiling');?>" name="package_ceiling"
								 data-validation="required number length"
								 data-validation-length="min2"
								 data-validation-ignore="."
								 />
								<label class="input__label input__label--nao" for="product_add_product_reorder_level">
									<span class="input__label-content input__label-content--nao">ceiling Per Month</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
							</span>	
							<?php echo form_error('package_ceiling','<span class="help-block form-error">', '</span>'); ?>	
						</div>
					</div>

					<div class="row rr">
						<div class="col-lg-2 col-md-3 pull-left pr-0 gj_mt form-group">
							<span class="access_add_select_placeholder pt-2">Package is Visible</span>
							<div class="bs-switch">
					            <input type="checkbox" class="bs-switch-checkbox" id="mySwitch" ="" <?php echo (set_value('is_active')!=null)? 'checked': '';?> name="is_active"
					            data-validation="required"
					            >
					            <label class="bs-switch-label" for="mySwitch">
					                <div class="bs-switch-inner"></div>
					                <div class="bs-switch-switch"></div>
					            </label>
					        </div>
					        <?php echo form_error('package_is_visible','<span class="help-block form-error">', '</span>'); ?>
						</div>

						<div class="col-lg-10 col-md-9 pull-left form-group">
							<span class="input input--nao gj_desc_wid">
								<textarea class="input__field input__field--nao" rows="6" id="product_add_product_package_short_description" name="package_short_desc"
								data-validation="length"
								data-validation-length="10-100"
								data-validaton-optional="true"
								><?php echo set_value('package_short_desc');?></textarea>
								<label class="input__label input__label--nao" for="product_add_product_package_short_description">
									<span class="input__label-content input__label-content--nao">Package Short Description</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
							</span>	
							<?php echo form_error('package_short_desc','<span class="help-block form-error">', '</span>'); ?>	
						</div>
					</div>

					<div class="row rr">
						<div class="col-md-12 row pull-left pr-0 form-group">
							<span class="input input--nao gj_pack_desc">
								 <textarea class="input__field input__field--nao" rows="6" id="module_add_description" name="package_desc"
								 data-validation="length"
								data-validation-length="min10"
								data-validaton-optional="true"
								 ><?php echo set_value('package_desc');?></textarea>
								<label class="input__label input__label--nao" for="module_add_description">
									<span class="input__label-content input__label-content--nao">Package Description</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
							</span>		
							<?php echo form_error('package_desc','<span class="help-block form-error">', '</span>'); ?>
						</div>
					</div>
					<div class="">
						<p class="gj_pack_title">Package Pictures</p>
						<div class=" pkfile gj_file">
					      	<div class=" toppkfile" style="float:left">
					      		<input type="file"  name="file[]" id="files" class="form-control img-responsive"  multiple="multiple">
					     		 <div class="width-60px width-6px">

					     		 </div>
					      	</div>
				        </div>
				    </div>

			        <div class="country_width_100 col-12 mt-5">
						<div class="country_width_100">
							<div class="butt_sec_width mt-3 mb-3">
						      	<button type="submit" name="moduleadd_submit" class="country_button mr-2" >SUBMIT <i class="fa fa-paper-plane" aria-hidden="true"></i></button>		
							    <button type="reset" name="moduleadd_reset" class="country_button">RESET <i class="fa fa-refresh" aria-hidden="true"></i></button>
							</div>
				      	</div>
				        <div class="country_width_100 mt-3 mb-3">					      		  
						    <div class="country-right">
						      	<a href="<?php echo base_url('index.php/products/packages_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button></a>
						    </div>	
						</div>
					</div>
				</form>
			</section>
		</div>
	</div>
</div>
<script type="text/javascript">
	$.validate({
		lang: 'en',
	    form:'#new_package_add',
	    //modules :'security'
	    modules :'logic,security,date',
	    onModulesLoaded : function() {
	      console.log('Modules are loaded successfully');
	  	}
	});
	$(document).ready(function() {
	  $(".js-example-basic-single").select2();
	});
	$(function(){
		$('.suc,.err,.pin_ldr').hide();
	})
	$('input[name^=package_name]').focus(function()  {
      var a = $('input[name^=package_name]').val();
      if(a.length>0){
        cc_name = a;
      }else{
        cc_name="";
      }
    })
	$('input[name^=package_name]').blur(function()  {
	    var package_name = $(this).val();
	    if(package_name.length>3 && cc_name != package_name){
	      $('.pin_ldr').show();
	      $.ajax({
	        type: "POST",
	        url: "<?php echo base_url(); ?>index.php/Common_controller/package_name_available",
	        data: {'field_value':package_name},
	          success:  function (data) { 
	            $('.pin_ldr').hide();
	            if(data == 1){
	            	$('.suc').show();
	            	$('.err').hide();
	            }else{
	            	$(' .suc').hide();
	            	$('.err').show();
	            }
	          }
	      })
	    }else if(pincode_no.length == 0 && cc_name.length > 2){
	     $('.succ,.err').hide();
	    }
  	});
</script>