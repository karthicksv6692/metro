<?php if(isset($obj)){ 
	// print_r($obj->getVendorAddressId());exit();
	if($obj != null){
		$vendor_id = $obj->getVendorId();
		$vendor_name = $obj->getVendorName();
		$vendor_tin_no = $obj->getVendorTinNo();
		$vendor_pan_no = $obj->getVendorPanNo();
		$vendor_gst_no = $obj->getVendorGstNo();


		$contact_obj = $obj->getVendorContactId();
		if($contact_obj != null){
			$vendor_contact_id = $contact_obj->getContactId();
			$vendor_contact_email = $contact_obj->getContactEmailId();
			$vendor_contact_mobile_primary = $contact_obj->getContactMobilePrimary();
			$vendor_contact_landline = $contact_obj->getContactLandline();
		}else{
			 $vendor_contact_id =''; $vendor_contact_email =''; $vendor_contact_mobile_primary =''; $vendor_contact_landline ='';
		}

		$address_obj = $obj->getVendorAddressId();
		if($address_obj != null){
			$vendor_address_id = $address_obj->get_Address_id();
			$full_address = $address_obj->get_Address_full_address();
			$pincode_code = $address_obj->get_Address_pincode_id()->getPincodeNo();
			$district = $address_obj->get_Address_pincode_id()->getPincodeDistrict();
			$state = $address_obj->get_Address_pincode_id()->getPincodeState();
			$country_id = $address_obj->get_Address_pincode_id()->getPincodeCountryId()->getCountryId();
			$country_name = $address_obj->get_Address_pincode_id()->getPincodeCountryId()->getCountryName();
		}else{
			$vendor_address_id =''; $full_address =''; $pincode_code =''; $district =''; $state =''; $country_id ='';$country_name ='';
		}
	}else{
		$vendor_name=''; $vendor_tin_no =''; $vendor_pan_no =''; $vendor_gst_no =''; $vendor_contact_id =''; $vendor_contact_email =''; $vendor_contact_mobile_primary =''; $vendor_contact_landline =''; $vendor_address_id =''; $full_address =''; $pincode_code =''; $district =''; $state =''; $country_id ='';$country_name ='';
	}


?>
<div class="container-fluid">
	<div class="page-header">
		<h3>Vendor - Delete</h3>
	</div>
	<div class="vendor_body">
		<div class="row">
			<div class="col-md-12 col-lg-4 col-sm-12">
				<p class="bank_add_details bar_head"><span><i class="fa fa-user" aria-hidden="true"></i></span> Personal Details</p>
				<!-- Personal Start -->
				<table class="s_table_view mt-3 mb-4">
				    <thead>
				    </thead>
				    <tbody>
				      <tr>
				        <td>Vendor Name</td>
				        <td><?php echo $vendor_name ; ?></td>
				      </tr>
				       <tr>
				        <td>TIN No </td>
				        <td><?php echo $vendor_tin_no ; ?></td>
				      </tr>
				      <tr>
				        <td>PAN Card No</td>
				        <td><?php echo $vendor_pan_no ; ?></td>
				      </tr>
				      <tr>
				        <td>GSt No</td>
				        <td><?php echo $vendor_gst_no ; ?></td>
				      </tr>
				    </tbody>
				</table>
				<!-- Personal End -->
			</div>
			<div class="col-md-12 col-lg-8 col-sm-12">
				<p class="bank_add_details bar_head"><span><i class="fa fa-user" aria-hidden="true"></i></span> Communication Details</p>
				<div class="col-md-6 p-0 pull-right">
					<table class="s_table_view mt-3 mb-4">
					    <thead>
					    </thead>
					    <tbody>
					      <tr>
					        <td>Address</td>
					        <td><?php echo $full_address ; ?></td>
					      </tr>
					       <tr>
					        <td>Pin code </td>
					        <td><?php echo $pincode_code ; ?></td>
					      </tr>
					      <tr>
					        <td>District</td>
					        <td><?php echo $district ; ?></td>
					      </tr>
					      <tr>
					        <td>State</td>
					        <td><?php echo $state ; ?></td>
					      </tr>
					    </tbody>
					</table>
				</div>
				<div class="col-md-6 p-0">
					<table class="s_table_view mt-3 mb-4">
					    <thead>
					    </thead>
					    <tbody>
					      <tr>
					        <td>Country</td>
					        <td><?php echo $country_name ; ?></td>
					      </tr>
					       <tr>
					        <td>Phone No</td>
					        <td><?php echo $vendor_contact_landline ; ?></td>
					      </tr>
					      <tr>
					        <td>Mobile No</td>
					        <td><?php echo $vendor_contact_mobile_primary ; ?></td>
					      </tr>
					      <tr>
					        <td>Email address</td>
					        <td><?php echo $vendor_contact_email ; ?></td>
					      </tr>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12">
				<p class="bank_add_details bar_head"><span><i class="fa fa-user" aria-hidden="true"></i></span> Account Details</p>
				<!-- Account Start -->
					<div class="container-fluid">
			      		<table class="table-responsive dynamic_list mb-3" id="propid_table">
			      		  	<thead>
				      		  	<tr>
					      		  	<th>Account Holder Name</th>
					      		  	<th>Account Number</th>
					      		  	<th>Account Type</th>
					      		  	<th>IFSC Code</th>
					      		  	<th>Bank Name</th>
					      		</tr>
			      		  	</thead>
			      		  	<tbody id="proappend">
			      		  		<?php if($account_obj != null){ //print_r($account_obj);exit();
					      			for($i=0;$i<count($account_obj);$i++){
			      		  		?>
				      		  	<tr>
					      		  	<td> 
					      		  		<?php echo $account_obj[$i]->getAccountHolderName() ; ?>
					      		  	</td>
					      		  	<td>
					      		  		<?php echo $account_obj[$i]->getAccountNo() ; ?>
					      		  	</td>
					      		  	<td>
					      		  		<?php if($account_obj[$i]->getAccountType() == 0){echo "Savings";}else{echo "Fixed";} ; ?>
				      		  		</td>
					      		  	<td>
					      		  		<?php echo $account_obj[$i]->getBankId()->getBankIfsc(); ?>
					      		  	</td>
					      		  	<td>
					      		  		<?php echo $account_obj[$i]->getBankId()->getBankName() ; ?>
					      		  	</td>
				      		  	</tr>
				      		  	<?php }}else{ ?>
					      		<tr>
						      		  	<td colspan="5">
						      		  		Account is Emplty
						      		  	</td>
						      	</tr>
					      		<?php } ?>
			      		  	</tbody>
			      		</table>
	      		  	</div>
				<!-- Account End -->
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12">
				<p class="bank_add_details bar_head"><span><i class="fa fa-user" aria-hidden="true"></i></span> Contact Details</p>
				<!-- Communication Start -->
					<div class="container-fluid">
			      		<table class="table-responsive dynamic_list mb-3" id="propid_table">
			      		  	<thead>
				      		  	<tr>
					      		  	<th>Name</th>
					      		  	<th>Designation</th>
					      		  	<th>Mobile</th>
					      		  	<th>Alter Mobile</th>
					      		  	<th>Email</th>
					      		</tr>
			      		  	</thead>
			      		  	<tbody id="contact_append">
			      		  		<?php if($alter_contact_obj != null){ 
					      			for($i=0;$i<count($alter_contact_obj);$i++){
			      		  		?>
					      		  	<tr>
						      		  	<td>
						      		  		<?php echo $alter_contact_obj[$i]->getContactName() ; ?>
						      		  	</td>
						      		  	<td>
						      		  		<?php echo $alter_contact_obj[$i]->getContactDesignation() ; ?>
						      		  	</td>
						      		  	<td>
						      		  		<?php echo $alter_contact_obj[$i]->getContactMobilePrimary() ; ?>
						      		  	<td>
							      		  	<?php echo $alter_contact_obj[$i]->getContactMobileSecondary() ; ?>
						      		  	</td>
						      		  	<td>
							      		  	<?php echo $alter_contact_obj[$i]->getContactEmailId() ; ?>
						      		  	</td>
					      		  	</tr>
					      		<?php }}else{ ?>
					      		<tr>
						      		  	<td colspan="5">
						      		  		Contact is Emplty
						      		  	</td>
						      	</tr>
					      		<?php } ?>
			      		  	</tbody>
			      		</table>
	      		  	</div>
				<!-- Communication End -->
			</div>
		</div>
		<div class="country_width_100 col-12 mt-5">
			<div class="country_width_100">
				<div class="butt_sec_width mt-3 mb-3">
					<form method="post" action="<?php echo base_url('index.php/products/vendor_crud/delete');?>">
						<input type="hidden" name="delete_vendor_id" value="<?php echo $vendor_id; ?>">
			      		<button type="submit" name="moduleadd_submit" class="country_button mr-2" >DELETE<i class="fa fa-paper-plane" aria-hidden="true"></i></button>	
			      	</form>	
			      	<form method="post" action="<?php echo base_url('index.php/products/vendor_crud/delete');?>">
			      		<input type="hidden" name="" value="">
				    	<button type="submit" name="moduleadd_reset" class="country_button">CANCEL <i class="fa fa-refresh" aria-hidden="true"></i></button>
				    <form>
				</div>
	      	</div>
	        <div class="country_width_100 mt-3 mb-3">					      		  
			    <div class="country-right">
			      	<a href="<?php echo base_url('index.php/products/Vendor_crud') ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button>
			    </div>	
			</div>
		</div>
	</div>

</div>

<?php }else{ ?>
	<div class="dkbody">
		<div class="container-fluid">
			<div class="page-header"> <h3>Select</h3> </div>
			<?php echo $search; ?>
		</div>
	</div>
	<script type="text/javascript"> $("#combobox").select2(); </script>
<?php }  ?>