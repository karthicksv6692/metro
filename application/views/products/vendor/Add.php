<div class="container-fluid">
		<div class="page-header">
			<h3>Vendor - Add</h3>
		</div>
		<div class="vendor_body">
			<form method="post" action="<?php echo base_url('index.php/products/vendor_crud/add'); ?>">
			<div class="row">
				<div class="col-md-12 col-lg-6 col-sm-12">
					<p class="bank_add_details bar_head"><span><i class="fa fa-user" aria-hidden="true"></i></span> Personal Details</p>
					<!-- Personal Start -->
						<div class="row">
							<div class="col-md-6">
								<span class="input input--nao">
									<input class="input__field input__field--nao vendor_name" type="text" id="vendor_name"
									  value="<?php echo set_value('vendor_name'); ?>" name="vendor_name"/>
									 <label class="input__label input__label--nao" for="vendor_name">
										<span class="input__label-content input__label-content--nao">Vendor Name</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
									<img src="<?php echo base_url('assets/images/loader'); ?>/default.gif" class="ldr v_name_ldr">
								</span>		
								<p class="v_name_suc avail_er suc">Available</p>
								<p class="v_name_err avail_er err">Already Exists</p>
								<?php echo form_error('vendor_name','<p class="error">', '</p>'); ?>	
							</div>
							<div class="col-md-6">
								<span class="input input--nao">
									<input class="input__field input__field--nao tin_no" type="text" id="tin_no"
									  value="<?php echo  set_value('vendor_tin_no'); ?>" name="vendor_tin_no"/>
									 <label class="input__label input__label--nao" for="tin_no">
										<span class="input__label-content input__label-content--nao">Tin Number</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
									<img src="<?php echo base_url('assets/images/loader'); ?>/default.gif" class="ldr tin_no_ldr">
								</span>		
								<p class="tin_no_suc avail_er suc">Available</p>
								<p class="tin_no_err avail_er err">Already Exists</p>
								<?php echo form_error('vendor_tin_no','<p class="error">', '</p>'); ?>	
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<span class="input input--nao">
									<input class="input__field input__field--nao pan_no" type="text" id="pan_no"
									  value="<?php echo  set_value('vendor_pan_no'); ?>" name="vendor_pan_no"/>
									 <label class="input__label input__label--nao" for="pan_no">
										<span class="input__label-content input__label-content--nao">Pan Number</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
									<img src="<?php echo base_url('assets/images/loader'); ?>/default.gif" class="ldr pan_no_ldr">
								</span>		
								<p class="pan_no_suc avail_er suc">Available</p>
								<p class="pan_no_err avail_er err">Already Exists</p>
								<?php echo form_error('vendor_pan_no','<p class="error">', '</p>'); ?>	
							</div>
							<div class="col-md-6">
								<span class="input input--nao">
									<input class="input__field input__field--nao gst_no" type="text" id="gst_no"
									  value="<?php echo  set_value('vendor_gst_no'); ?>" name="vendor_gst_no"/>
									 <label class="input__label input__label--nao" for="gst_no">
										<span class="input__label-content input__label-content--nao">GST Number</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
									<img src="<?php echo base_url('assets/images/loader'); ?>/default.gif" class="ldr gst_no_ldr">
								</span>		
								<p class="gst_no_suc avail_er suc">Available</p>
								<p class="gst_no_err avail_er err">Already Exists</p>
								<?php echo form_error('vendor_gst_no','<p class="error">', '</p>'); ?>	
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<span class="input input--nao">
									<input class="input__field input__field--nao email" type="text" id="email"
									  value="<?php echo  set_value('vendor_contact_email'); ?>" name="vendor_contact_email"/>
									 <label class="input__label input__label--nao" for="email">
										<span class="input__label-content input__label-content--nao">Email</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
								</span>		
								<?php echo form_error('vendor_contact_email','<p class="error">', '</p>'); ?>	
							</div>
							<div class="col-md-4">
								<span class="input input--nao">
									<input class="input__field input__field--nao mobile" type="text" id="mobile"
									  value="<?php echo  set_value('vendor_contact_mobile_primary'); ?>" name="vendor_contact_mobile_primary"/>
									 <label class="input__label input__label--nao" for="mobile">
										<span class="input__label-content input__label-content--nao">Mobile</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
								</span>		
								<?php echo form_error('vendor_contact_mobile_primary','<p class="error">', '</p>'); ?>	
							</div>
							<div class="col-md-4">
								<span class="input input--nao">
									<input class="input__field input__field--nao landline" type="text" id="landline"
									  value="<?php echo  set_value('vendor_contact_landline'); ?>" name="vendor_contact_landline"/>
									 <label class="input__label input__label--nao" for="landline">
										<span class="input__label-content input__label-content--nao">Landline</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
								</span>		
								<?php echo form_error('vendor_contact_landline','<p class="error">', '</p>'); ?>	
							</div>
						</div>
					<!-- Personal End -->
				</div>
				<div class="col-md-12 col-lg-6 col-sm-12">
					<p class="bank_add_details bar_head"><span><i class="fa fa-user" aria-hidden="true"></i></span> Communication Details</p>

					<div class="row">
						<div class="col-md-8">
							<span class="input input--nao outer_span_cus input--filled">
									
									 <textarea class="input__field input__field--nao vendor_cus_txt_ar" rows="3" id="module_add_description" name="full_address" value=""><?php echo  set_value('full_address'); ?></textarea >
									<label class="input__label input__label--nao vendor_cus_lbl" for="module_add_description">
										<span class="input__label-content input__label-content--nao">Address</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></path>
									</svg>
									

								</span>	
								<?php echo form_error('full_address','<p class="error">', '</p>'); ?>	
						</div>
						<div class="col-md-4">
							<span class="input input--nao">
								<input class="input__field input__field--nao pincode_no" type="text" id="pincode_no"
								  value="<?php echo  set_value('pincode_code'); ?>" name="pincode_code"/>
								 <label class="input__label input__label--nao" for="pincode_no">
									<span class="input__label-content input__label-content--nao">Pincode</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
								<img src="<?php echo base_url('assets/images/loader'); ?>/default.gif" class="ldr pin_no_ldr">
							</span>		
							<?php echo form_error('pincode_code','<p class="error">', '</p>'); ?>	
						</div>
					</div>

					<div class="row">
						<div class="col-md-4">
							<span class="input input--nao district_lbl">
								<input class="input__field input__field--nao district" type="text" id="district"
								  value="<?php echo  set_value('district'); ?>" name="district"/>
								 <label class="input__label input__label--nao" for="district">
									<span class="input__label-content input__label-content--nao">District</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
							</span>		
							<?php echo form_error('district','<p class="error">', '</p>'); ?>	
						</div>
						<div class="col-md-4">
							<span class="input input--nao state_lbl">
								<input class="input__field input__field--nao state" type="text" id="state"
								  value="<?php echo  set_value('state'); ?>" name="state"/>
								 <label class="input__label input__label--nao" for="state">
									<span class="input__label-content input__label-content--nao">State</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
							</span>		
							<?php echo form_error('state','<p class="error">', '</p>'); ?>	
						</div>
						<div class="col-md-4">
							<select class="js-example-basic-single select2_style ad_usr country" name="country" id="combobox">
								<?php foreach ($country_list as $list) {?>
									<option value="<?php echo $list->getCountryId();  ?>" <?php if(set_value('country_id') == $list->getCountryId()){echo "selected='selected'";} ?>><?php echo $list->getCountryName();  ?></option>
								<?php  } ?>
							</select>
							<?php echo form_error('country','<p class="error">', '</p>'); ?>	
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-lg-12 col-sm-12">
					<p class="bank_add_details bar_head"><span><i class="fa fa-user" aria-hidden="true"></i></span> Account Details</p>
					<!-- Account Start -->
						<div class="container-fluid">
				      		<table class="table-responsive" id="propid_table">
				      		  	<thead>
					      		  	<tr>
						      		  	<th>Account Holder Name</th>
						      		  	<th>Account Number</th>
						      		  	<th>Account Type</th>
						      		  	<th>IFSC Code</th>
						      		  	<th>Bank Name</th>
						      		  	<th>Delete</th>					      		  	
						      		</tr>
				      		  	</thead>
				      		  	<tbody id="proappend">
					      		 

					      		<?php if(set_value('account_holder_name') != null){ //print_r($account_obj);exit();
					      			for($i=0;$i<count(set_value('account_holder_name'));$i++){
			      		  		?>
			      		  			<tr>
						      		  	<td>
							      		  	<span class="input input--nao">
												<input class="input__field input__field--nao" type="text" id="account_holder_name"
												 name="account_holder_name[]" value="<?php echo set_value('account_holder_name')[$i]; ?>" />
												<label class="input__label input__label--nao" for="account_holder_name">
													<span class="input__label-content input__label-content--nao">Account Holder Name</span>
												</label>
												<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
													<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
												</svg>
											</span>	
											<?php echo form_error('account_holder_name['.$i.']','<p class="error">', '</p>'); ?>	
						      		  	</td>
						      		  	<td>
							      		  	<span class="input input--nao">
												<input class="input__field input__field--nao" type="text" id="account_number"
												 name="account_number[]" value="<?php echo set_value('account_number')[$i]; ?>" />
												<label class="input__label input__label--nao" for="account_number">
													<span class="input__label-content input__label-content--nao">Account Number</span>
												</label>
												<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
													<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
												</svg>
											</span>	
											<?php echo form_error('account_number['.$i.']','<p class="error">', '</p>'); ?>	
						      		  	</td>
						      		  	<td>
						      		  	<?php //print_r(set_value('account_number')); ?>
						      		  		<select class="js-example-basic-single_ac_type select2_style ad_usr" name="account_type[]" id="combobox">
												<option value="0" <?php if(set_value('account_type['.$i.']') == 0){echo "selected='selected'";} ?> >Savings Bank</option>
												<option value="1" <?php if(set_value('account_type['.$i.']') == 1){echo "selected='selected'";}?>>Fixed</option>
											</select>
											<script type="text/javascript">
												$('.js-example-basic-single_ac_type').val(<?php echo set_value('account_type['.$i.']'); ?>).trigger('change'); 
											</script>
											<?php echo form_error('account_type['.$i.']','<p class="error">', '</p>'); ?>
					      		  		</td>
						      		  	<td>
							      		  	<span class="input input--nao">
												<input class="input__field input__field--nao bank_ifsc" type="text" id="bank_ifsc"
												 name="bank_ifsc[]" value="<?php echo set_value('bank_ifsc')[$i]; ?>" />
												<label class="input__label input__label--nao" for="ifsc">
													<span class="input__label-content input__label-content--nao">Ifsc</span>
												</label>
												<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
													<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
												</svg>
											</span>	
											<?php echo form_error('bank_ifsc['.$i.']','<p class="error">', '</p>'); ?>	
						      		  	</td>
						      		  	<td>
							      		  	<span class="input input--nao">
												<input class="input__field input__field--nao" type="text" id="bank_name"
												 name="bank_name[]" value="<?php echo set_value('bank_name')[$i]; ?>" />
												<label class="input__label input__label--nao" for="bank_name">
													<span class="input__label-content input__label-content--nao">Bank Name</span>
												</label>
												<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
													<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
												</svg>
											</span>	
											<?php echo form_error('bank_name['.$i.']','<p class="error">', '</p>'); ?>	
						      		  	</td>
						      		  	<td class="text-center tk">
							      		  	<button type="button" name="delete_acc_holder[]" class="delete_icon"><span class="deleteicon"><i class="fa fa-trash"></i></span></button>
						      		  	</td>					      		  	
					      		  	</tr>
					      		  	
					      		  <?php }}else{?>
					      		  		 <tr>
					      		  	<td>
						      		  	<span class="input input--nao">
											<input class="input__field input__field--nao" type="text" id="account_holder_name"
											 name="account_holder_name[]" value="" />
											<label class="input__label input__label--nao" for="account_holder_name">
												<span class="input__label-content input__label-content--nao">Account Holder Name</span>
											</label>
											<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
												<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
											</svg>
										</span>	
										<?php echo form_error('account_holder_name','<p class="error">', '</p>'); ?>	
					      		  	</td>
					      		  	<td>
						      		  	<span class="input input--nao">
											<input class="input__field input__field--nao" type="text" id="account_number"
											 name="account_number[]" value="" />
											<label class="input__label input__label--nao" for="account_number">
												<span class="input__label-content input__label-content--nao">Account Number</span>
											</label>
											<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
												<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
											</svg>
										</span>	
										<?php echo form_error('account_number','<p class="error">', '</p>'); ?>	
					      		  	</td>
					      		  	<td>
					      		  		<select class="js-example-basic-single_ac_type select2_style ad_usr country" name="account_type[]" id="combobox">
											<option value="0">Savings Bank</option>
											<option value="1">Fixed</option>
										</select>
										<?php echo form_error('account_type','<p class="error">', '</p>'); ?>
				      		  		</td>
					      		  	<td>
						      		  	<span class="input input--nao">
											<input class="input__field input__field--nao bank_ifsc" type="text" id="bank_ifsc"
											 name="bank_ifsc[]" value="" />
											<label class="input__label input__label--nao" for="ifsc">
												<span class="input__label-content input__label-content--nao">Ifsc</span>
											</label>
											<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
												<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
											</svg>
										</span>	
										<?php echo form_error('bank_ifsc','<p class="error">', '</p>'); ?>	
					      		  	</td>
					      		  	<td>
						      		  	<span class="input input--nao">
											<input class="input__field input__field--nao" type="text" id="bank_name"
											 name="bank_name[]" value="" />
											<label class="input__label input__label--nao" for="bank_name">
												<span class="input__label-content input__label-content--nao">Bank Name</span>
											</label>
											<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
												<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
											</svg>
										</span>	
										<?php echo form_error('bank_name','<p class="error">', '</p>'); ?>	
					      		  	</td>
					      		  	<td class="text-center tk">
						      		  	<button type="button" name="delete_acc_holder[]" class="delete_icon"><span class="deleteicon"><i class="fa fa-trash"></i></span></button>
					      		  	</td>					      		  	
					      		  </tr>

					      		  <?php } ?>


				      		  	</tbody>
				      		</table>
		      		  	</div>
		      		  	<div class="col-12 ">
			      		  	<div class="rw">
				      		  	<div class="pull-right col-12 border-top text-right pb-2">
				      		  		<button type="button" name="add_new_account_holder_name" class="dynamic add_new_account_holder_name" id="add_new_account_holder_name">ADD NEW</button>
				      		  	</div>
		      		  	  	</div>
		      		  	</div>
					<!-- Account End -->
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-lg-12 col-sm-12">
					<p class="bank_add_details bar_head"><span><i class="fa fa-user" aria-hidden="true"></i></span> Contact Details</p>
					<!-- Communication Start -->
						<div class="container-fluid">
				      		<table class="table-responsive" id="propid_table">
				      		  	<thead>
					      		  	<tr>
						      		  	<th>Name</th>
						      		  	<th>Designation</th>
						      		  	<th>Mobile</th>
						      		  	<th>Alter Mobile</th>
						      		  	<th>Email</th>
						      		  	<th>Delete</th>					      		  	
						      		</tr>
				      		  	</thead>
				      		  	<tbody id="contact_append">

				      		  		<?php if(set_value('c_name') != null){ 
						      			for($i=0;$i<count(set_value('c_name'));$i++){
				      		  		?>
						      		  	<tr>
							      		  	<td>
								      		  	<span class="input input--nao">
													<input class="input__field input__field--nao" type="text" id="c_name"
													 name="c_name[]" value="<?php echo set_value('c_name')[$i]; ?>" />
													<label class="input__label input__label--nao" for="c_name">
														<span class="input__label-content input__label-content--nao">Contact Name</span>
													</label>
													<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
														<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
													</svg>
												</span>	
												<?php echo form_error('c_name','<p class="error">', '</p>'); ?>	
							      		  	</td>
							      		  	<td>
								      		  	<span class="input input--nao">
													<input class="input__field input__field--nao" type="text" id="desig"
													 name="desig[]" value="<?php echo  set_value('desig')[$i]; ?>" />
													<label class="input__label input__label--nao" for="desig">
														<span class="input__label-content input__label-content--nao">Designation</span>
													</label>
													<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
														<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
													</svg>
												</span>	
												<?php echo form_error('desig','<p class="error">', '</p>'); ?>
							      		  	</td>
							      		  	<td>
							      		  		<span class="input input--nao">
													<input class="input__field input__field--nao" type="text" id="c_mob"
													 name="c_mob[]" value="<?php echo  set_value('c_mob')[$i]; ?>" />
													<label class="input__label input__label--nao" for="c_mob">
														<span class="input__label-content input__label-content--nao">Mobile No</span>
													</label>
													<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
														<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
													</svg>
												</span>	
												<?php echo form_error('c_mob','<p class="error">', '</p>'); ?>	
						      		  		</td>
							      		  	<td>
								      		  	<span class="input input--nao">
													<input class="input__field input__field--nao" type="text" id="c_alter_mob"
													 name="c_alter_mob[]" value="<?php echo  set_value('c_alter_mob')[$i]; ?>" />
													<label class="input__label input__label--nao" for="c_alter_mob">
														<span class="input__label-content input__label-content--nao">Ex Mobile No</span>
													</label>
													<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
														<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
													</svg>
												</span>	
												<?php echo form_error('c_alter_mob','<p class="error">', '</p>'); ?>	
							      		  	</td>
							      		  	<td>
								      		  	<span class="input input--nao">
													<input class="input__field input__field--nao" type="text" id="c_email"
													 name="c_email[]" value="<?php echo  set_value('c_email')[$i]; ?>" />
													<label class="input__label input__label--nao" for="c_email">
														<span class="input__label-content input__label-content--nao">Email</span>
													</label>
													<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
														<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
													</svg>
												</span>	
												<?php echo form_error('c_email','<p class="error">', '</p>'); ?>	
							      		  	</td>
							      		  	<td class="text-center tk">
								      		  	<button type="button" name="delete_acc_holder[]" class="delete_icon"><span class="deleteicon"><i class="fa fa-trash"></i></span></button>
							      		  	</td>					      		  	
						      		  	</tr>
						      		<?php }}else{?>
						      			<tr>
						      		  	<td>
							      		  	<span class="input input--nao">
												<input class="input__field input__field--nao" type="text" id="c_name"
												 name="c_name[]" value="" />
												<label class="input__label input__label--nao" for="c_name">
													<span class="input__label-content input__label-content--nao">Contact Name</span>
												</label>
												<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
													<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
												</svg>
											</span>	
											<?php echo form_error('c_name','<p class="error">', '</p>'); ?>	
						      		  	</td>
						      		  	<td>
							      		  	<span class="input input--nao">
												<input class="input__field input__field--nao" type="text" id="desig"
												 name="desig[]" value="" />
												<label class="input__label input__label--nao" for="desig">
													<span class="input__label-content input__label-content--nao">Designation</span>
												</label>
												<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
													<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
												</svg>
											</span>	
						      		  	</td>
						      		  	<td>
						      		  		<span class="input input--nao">
												<input class="input__field input__field--nao" type="text" id="c_mob"
												 name="c_mob[]" value="" />
												<label class="input__label input__label--nao" for="c_mob">
													<span class="input__label-content input__label-content--nao">Mobile No</span>
												</label>
												<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
													<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
												</svg>
											</span>	
											<?php echo form_error('c_mob','<p class="error">', '</p>'); ?>	
					      		  		</td>
						      		  	<td>
							      		  	<span class="input input--nao">
												<input class="input__field input__field--nao" type="text" id="c_alter_mob"
												 name="c_alter_mob[]" value="" />
												<label class="input__label input__label--nao" for="c_alter_mob">
													<span class="input__label-content input__label-content--nao">Ex Mobile No</span>
												</label>
												<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
													<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
												</svg>
											</span>	
											<?php echo form_error('c_alter_mob','<p class="error">', '</p>'); ?>	
						      		  	</td>
						      		  	<td>
							      		  	<span class="input input--nao">
												<input class="input__field input__field--nao" type="text" id="c_email"
												 name="c_email[]" value="" />
												<label class="input__label input__label--nao" for="c_email">
													<span class="input__label-content input__label-content--nao">Email</span>
												</label>
												<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
													<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
												</svg>
											</span>	
											<?php echo form_error('c_email','<p class="error">', '</p>'); ?>	
						      		  	</td>
						      		  	<td class="text-center tk">
							      		  	<button type="button" name="delete_acc_holder[]" class="delete_icon"><span class="deleteicon"><i class="fa fa-trash"></i></span></button>
						      		  	</td>					      		  	
					      		  	</tr>
						      		<?php } ?>
				      		  	</tbody>
				      		</table>
		      		  	</div>
		      		  	<div class="col-12 ">
			      		  	<div class="rw">
				      		  	<div class="pull-right col-12 border-top text-right">
				      		  		<button type="button" name="add_new_account_holder_name" class="dynamic add_new_account_holder_name" id="new_contact_details">ADD NEW</button>
				      		  	</div>
		      		  	  	</div>
		      		  	</div>
					<!-- Communication End -->
				</div>
			</div>
			<input type="hidden" name="ven" value="asdf">
			<div class="country_width_100 col-12 mt-5">
				<div class="country_width_100">
					<div class="butt_sec_width mt-3 mb-3">
				      	<button type="submit" name="moduleadd_submit" class="country_button mr-2" >SUBMIT <i class="fa fa-paper-plane" aria-hidden="true"></i></button>		
					    <button type="reset" name="moduleadd_reset" class="country_button">RESET <i class="fa fa-refresh" aria-hidden="true"></i></button>
					</div>
		      	</div>
		        <div class="country_width_100 mt-3 mb-3">					      		  
				    <div class="country-right">
				      	<a href="<?php echo base_url('index.php/products/Vendor_crud') ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button>
				    </div>	
				</div>
			</div>
			</form>
		</div>
	</div>


	<?php 
		$pincode_array = array();
		$bank_ifsc_list = array();
		$bank_name_list = array();
		foreach ($pincode_list as $list) {  
			array_push($pincode_array, $list->getPincodeNo());
		}
		function js_str($s) { return '"' . addcslashes($s, "\0..\37\"\\") . '"'; }
		function js_array($array) { 
			$temp = array_map('js_str', $array);
			return '[' . implode(',', $temp) . ']';
		}
		for($i=0;$i<count($bank_list);$i++){
		    // array_push($bank_name_list, $bank_list[$i]->getBankName());
		    array_push($bank_ifsc_list, $bank_list[$i]->getBankIfsc());
		}
	?>

	<script type="text/javascript">

		$(document).ready(function() { 
			$(".js-example-basic-single").select2({ 
				placeholder: "Select a Country",
	  		});
	  		$(".js-example-basic-single_ac_type").select2({ 
				placeholder: "Select a Account Type",
	  		}); 
	  		$(".country").val('').trigger('change');
		});
		$( function() { 
	      	$( ".pincode_no" ).autocomplete({
	        	source: <?php echo  js_array($pincode_array);?>
	      	});
	    });
	    $( ".bank_ifsc" ).autocomplete({
	      source: <?php echo  js_array($bank_ifsc_list);?>
	    });
	    $(document).on('click','.delete_icon',function(){
	    	if($(this).closest('tbody').find('tr').length==1){
	    	}else{
	    		$(this).closest('tr').remove();    		    		
	    	}
	    })
	    $('.v_name_ldr,.v_name_suc,.v_name_err,.tin_no_ldr,.tin_no_suc,.tin_no_err,.pan_no_ldr,.pan_no_suc,.pan_no_err,.gst_no_ldr,.gst_no_suc,.gst_no_err,.pin_no_ldr').hide();

	    // ********************************************************************************************************
		$('.vendor_name').focus(function()  {
	      var a = $('.vendor_name').val();
	      if(a.length>0){
	        cc_name = a;
	      }else{
	        cc_name="";
	      }
	    })
	    $('.vendor_name').blur(function()  {
	      var vendor_name = $('.vendor_name').val();
	      if(vendor_name.length>2 && cc_name != vendor_name){
	        $('.v_name_ldr').show();
	        $.ajax({
	          type: "POST",
	          url: "<?php echo base_url(); ?>index.php/Common_controller/vendor_name_available",
	          data: {'field_value':vendor_name},
	            success:  function (data) { 
	              $('.v_name_ldr').hide();
	              if(data == 1){
	              	$('.v_name_suc').show();
	              	$('.v_name_err').hide();
	              }else{
	              	$('.v_name_suc').hide();
	              	$('.v_name_err').show();
	              }
	            }
	        })
	      }else if(vendor_name.length == 0 && cc_name.length > 2){
	       $('.v_name_suc,.v_name_err').hide();
	      }
	    })
	    // ********************************************************************************************************

	    // ********************************************************************************************************
	    $('.tin_no').focus(function()  {
	      var a = $('.tin_no').val();
	      if(a.length>0){
	        cc_tin_no = a;
	      }else{
	        cc_tin_no="";
	      }
	    })
	    $('.tin_no').blur(function()  {
	      var tin_no = $('.tin_no').val();
	      if(tin_no.length>2 && cc_tin_no != tin_no){
	        $('.tin_no_ldr').show();
	        $.ajax({
	          type: "POST",
	          url: "<?php echo base_url(); ?>index.php/Common_controller/vendor_tin_no_available",
	          data: {'field_value':tin_no},
	            success:  function (data) { 
	              $('.tin_no_ldr').hide();
	              if(data == 1){
	              	$('.tin_no_suc').show();
	              	$('.tin_no_err').hide();
	              }else{
	              	$('.tin_no_suc').hide();
	              	$('.tin_no_err').show();
	              }
	            }
	        })
	      }else if(tin_no.length == 0 && tin_no.length > 2){
	       $('.tin_no_suc,.tin_no_err').hide();
	      }
	    })
	    // ********************************************************************************************************

	    // ********************************************************************************************************
	    $('.pan_no').focus(function()  {
	      var a = $('.pan_no').val();
	      if(a.length>0){
	        cc_pan_no = a;
	      }else{
	        cc_pan_no="";
	      }
	    })
	    $('.pan_no').blur(function()  {
	      var pan_no = $('.pan_no').val();
	      if(pan_no.length>2 && cc_pan_no != pan_no){
	        $('.pan_no_ldr').show();
	        $.ajax({
	          type: "POST",
	          url: "<?php echo base_url(); ?>index.php/Common_controller/vendor_pan_no_available",
	          data: {'field_value':pan_no},
	            success:  function (data) { 
	              $('.pan_no_ldr').hide();
	              if(data == 1){
	              	$('.pan_no_suc').show();
	              	$('.pan_no_err').hide();
	              }else{
	              	$('.pan_no_suc').hide();
	              	$('.pan_no_err').show();
	              }
	            }
	        })
	      }else if(pan_no.length == 0 && pan_no.length > 2){
	       $('.pan_no_suc,.pan_no_err').hide();
	      }
	    })
	    // ********************************************************************************************************


	    // ********************************************************************************************************
	    $('.gst_no').focus(function()  {
	      var a = $('.gst_no').val();
	      if(a.length>0){
	        cc_gst_no = a;
	      }else{
	        cc_gst_no="";
	      }
	    })
	    $('.gst_no').blur(function()  {
	      var gst_no = $('.gst_no').val();
	      if(gst_no.length>2 && cc_gst_no != gst_no){
	        $('.gst_no_ldr').show();
	        $.ajax({
	          type: "POST",
	          url: "<?php echo base_url(); ?>index.php/Common_controller/vendor_gst_no_available",
	          data: {'field_value':gst_no},
	            success:  function (data) { 
	              $('.gst_no_ldr').hide();
	              if(data == 1){
	              	$('.gst_no_suc').show();
	              	$('.gst_no_err').hide();
	              }else{
	              	$('.gst_no_suc').hide();
	              	$('.gst_no_err').show();
	              }
	            }
	        })
	      }else if(gst_no.length == 0 && gst_no.length > 2){
	       $('.gst_no_suc,.gst_no_err').hide();
	      }
	    })
	    // ********************************************************************************************************




	    //Pincode Ajax Start
		    $('.pincode_no').blur(function()  {
		  		var pincode = $('.pincode_no').val();
		      	if(pincode.length > 0){
			      	$.ajax({
			        type: "POST",
			        url: "<?php echo base_url(); ?>index.php/Common_controller/ajax_pincode",
			        data: {'pincode':pincode},
		          	success:  function (data) {
		          		if(data != 0){
				            var option = JSON.parse(data);
				            console.log(option);
				            if(option[2]['pincode_district'] == false){
				              $('.district').val("");
				              $('.district').attr('disabled', 'disabled');
				            }else{
				              $('.district_lbl').addClass('input--filled');
				              $('.district').val(option[2]['pincode_district']).removeAttr('disabled');
				              $('.district').attr('disabled', false);
				            }
				            if(option[3]['pincode_state'] == false){
				              $('.state').val("");
				              $('.state').attr('disabled', 'disabled');
				               $('.state_lbl').removeClass('input--filled');
				            }else{
				              $('.state_lbl').addClass('input--filled');
				              $('.state').attr('disabled', 'false');
				              $('.state').val(option[3]['pincode_state']).removeAttr('disabled');
				            }
				            if(option[6]['pincode_country'] == false){
				              $('.country').val("");
				              $('.country').attr('disabled', 'disabled');
				            }else{
				             	$('.country_lbl').addClass('input--filled');
			              		$('.country').val(option[6]['country_id']).trigger('change');  
				            }
				        }else{
					      	 $('.district,.state,.country').val('');
				        	$('.state_lbl,.district_lbl,.country_label').removeClass('input--filled');
					      	 $('.district,.state,.country').attr('disabled', 'disabled');
				        }
		          	}
		      	});
		      	}else{
		      		$('.state_lbl,.district_lbl,.country_label').removeClass('input--filled');
		      	 	$('.district,.state,.country').attr('disabled', 'disabled');
		      	 	$('.district,.state,.country').val('');
		      	}
		    });
	  	//Pincode Ajax End
	  	$("#add_new_account_holder_name").click(function(){
		    $("#proappend").append('<tr><td><span class="input input--nao"><input class="input__field input__field--nao" type="text" id="account_holder_name"name="account_holder_name[]" value="" /><label class="input__label input__label--nao" for="account_holder_name"><span class="input__label-content input__label-content--nao">Account Holder Name</span></label><svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none"><path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></svg></span>	<?php echo form_error('account_holder_name','<p class="error">', '</p>'); ?>	</td><td><span class="input input--nao"><input class="input__field input__field--nao" type="text" id="account_number"name="account_number[]" value="" /><label class="input__label input__label--nao" for="account_number"><span class="input__label-content input__label-content--nao">Account Number</span></label><svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none"><path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></svg></span>	<?php echo form_error('account_number','<p class="error">', '</p>'); ?>	</td><td><select class="js-example-basic-single_ac_type select2_style ad_usr country" name="account_type[]" id="combobox"><option value="0">Savings Bank</option><option value="1">Fixed</option></select><?php echo form_error('account_type','<p class="error">', '</p>'); ?></td><td><span class="input input--nao"><input class="input__field input__field--nao bank_ifsc" type="text" id="bank_ifsc"name="bank_ifsc[]" value="" /><label class="input__label input__label--nao" for="ifsc"><span class="input__label-content input__label-content--nao">Ifsc</span></label><svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none"><path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></svg></span>	<?php echo form_error('bank_ifsc','<p class="error">', '</p>'); ?>	</td><td><span class="input input--nao"><input class="input__field input__field--nao" type="text" id="bank_name"name="bank_name[]" value="" /><label class="input__label input__label--nao" for="bank_name"><span class="input__label-content input__label-content--nao">Bank Name</span></label><svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none"><path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></svg></span>	<?php echo form_error('bank_name','<p class="error">', '</p>'); ?>	</td><td class="text-center tk"><button type="button" name="delete_acc_holder[]" class="delete_icon"><span class="deleteicon"><i class="fa fa-trash"></i></span></button></td></tr>');
		    	$( ".bank_ifsc" ).autocomplete({
			        source: <?php echo  js_array($bank_ifsc_list);?>
		      	});
				$( document ).ready(function( $ ) {
					$(".js-example-basic-single_ac_type").select2({ 
						placeholder: "Select a Account Type",
			  		});
					$.validate({
						form:'#vendor_add'
					});			
				});
				$(document.body).on('click', '#add_new_account_holder_name' ,function(){
					$(".js-example-basic-single_ac_type").select2({ 
						placeholder: "Select a Account Type",
			  		});
				})
			
		});
		$(document).on('click','#new_contact_details',function(){
			contact_r=$('#contact_append tr').first().clone();
			$('#contact_append').append(contact_r);
		});
	</script>

	<style type="text/css">
		.page-content section.content.bgcolor-1 form span.select2.select2-container.select2-container--default.select2-container--focus, span.select2.select2-container.select2-container--default.select2-container--below, span.select2.select2-container.select2-container--default {
		    width: 150px !important;
		    margin: 20px auto !important;
		    margin-top: 15px !important;
		    text-align: left;
		}
		.page-content section.content.bgcolor-1 form  .cntry span.select2.select2-container.select2-container--default.select2-container--focus, .page-content section.content.bgcolor-1 form  .cntry span.select2.select2-container.select2-container--default.select2-container--below, s.page-content section.content.bgcolor-1 form  .cntry pan.select2.select2-container.select2-container--default {
		    width: 170px !important;
		    margin-top: 45px !important;
		    text-align: left;
		}
		 .cntry span.select2.select2-container.select2-container--default{
		 	 margin-top: 45px !important;
		 }
		 .select2-container .select2-selection--single {
		    margin-top: 30px !important;
		}

		.select2-container--default .select2-selection--single .select2-selection__arrow {
		    height: 26px;
		    position: absolute;
		    top: 30px;
		    right: 0px;
		    width: 20px;
		}
	</style>