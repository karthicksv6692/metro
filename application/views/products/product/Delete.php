<?php if(isset($single_product)){ 

	if($single_product != null){ 
		$product_id 			= 	$single_product->getProductId();
		$product_name 			= 	$single_product->getProductName();
		$product_code 			= 	$single_product->getProductCode();
		$product_quantity 		= 	$single_product->getProductQuantity();
		$product_price 			= 	$single_product->getProductPrice();
		$product_vendor_id 		= 	$single_product->getProductVendorId()->getVendorName();
		$product_reorder_level 	= 	$single_product->getProductReorderLevel();

    $prd = explode('^', $single_product->getProductDesc());
    $ht = "<ul class='desc_cls'>";
    foreach ($prd as $pp) {
      $ht .= "<li>".$pp."</li>";
    }
    $ht .= "<ul>";
    $product_desc       =     $ht;

    $prdx = explode('^', $single_product->getProductShortDesc());
    $htx = "<ul class='desc_cls'>";
    foreach ($prdx as $ppx) {
      $htx .= "<li>".$ppx."</li>";
    }
    $htx .= "<ul>";
    $product_short_desc       =     $htx;

		$is_active 				= 	$single_product->getProductIsVisible();
	}else{
		$product_id = "";   $product_name  = "";   $product_code  = "";   $product_quantity  = "";   $product_price  = "";   $product_vendor_id  = "";   $product_reorder_level  = "";   $product_desc  = "";   $product_short_desc  = ""; $is_active  = "";   
	}

	$imgs = $single_product->getAttachmentRefererId();
	if(!empty($imgs)){
		$package_image=$imgs;
	}

?>
	<div class="dkbody">
		<div class="container-fluid">
			<div class="page-header gj_new_pack">
				<h3>Product Single View</h3>
			</div>
			<form method="post" action="<?php echo base_url('index.php/products/product_crud/delete'); ?>">
				<p class="gj_package_dets"><span><i class="fa fa-dropbox"></i></span> Product Details</p>
				<div class="row">
					<div class="col-md-12 col-lg-4 naveeno_single_car_border">
		    			<div id="jssor_2" style="position:relative;margin:0 auto;top:70px;left:0px;width:541px;height:477px;overflow:hidden;visibility:hidden;">
		        			<!-- Loading Screen -->
		      				<div data-u="loading" style="position:absolute;top:0px;left:0px;">
					            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
					            <div style="position:absolute;display:block;top:0px;left:0px;width:100%;height:100%;"></div>
		      				</div>
		      				<div data-u="slides" class="gj_jssor_slides">
		      					<?php if(isset($package_image)){ 
		      						for($i=0;$i<count($package_image);$i++){
								?>
			        				<div>
							          	<img data-u="image" src="<?php echo base_url($package_image[$i]->getAttachmentImageUrl()); ?>" class="img-responsive"/>
								        <div data-u="thumb">
								            <img class="i" src="<?php echo base_url($package_image[$i]->getAttachmentImageUrl()); ?>" />
								        </div>
							        </div>
							    <?php  }}else{ ?>

							    	<div>
							          	<img data-u="image" src="<?php echo base_url('assets/images'); ?>/no_image.jpg" class="img-responsive"/>
								        <div data-u="thumb">
								            <img class="i" src="<?php echo base_url('assets/images'); ?>/no_image.jpg" />
								        </div>
							        </div>

						     	<?php  } ?>
		      				</div>
		      				<!-- Thumbnail Navigator -->
		      				<div data-u="thumbnavigator" class="jssort11-130-64 gj_jssor_thums" data-autocenter="1">
		            			<!-- Thumbnail Item Skin Begin -->
		        				<div data-u="slides" style="cursor: default; width:200px;">
		            				<div data-u="prototype" class="p">
		                				<div data-u="thumbnailtemplate" class="tp"></div>
		            				</div>
		        				</div>
		        				<!-- Thumbnail Item Skin End -->
		  					</div>
					        <!-- Arrow Navigator -->
					      	<div data-u="arrowleft" class="jssora02l" style="top:0px;left:8px;width:55px;height:55px;" data-autocenter="2">
					      		<i class="fa fa-angle-left"></i>
					      	</div>
					        <div data-u="arrowright" class="jssora02r" style="top:0px;right:218px;width:55px;height:55px;" data-autocenter="2">
					        	<i class="fa fa-angle-right"></i>
				        	</div>
				      	</div>
		 				<script type="text/javascript">jssor_2_slider_init();</script>
					</div>
					
					<div class="col-md-12 col-lg-8">
						<div class="gj_psv_display">
							<p class="gj_pck"><?php echo $product_name; ?><span class="gj_psv_star"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span><span class="gj_psv_rating">4.5 Stars</span></p>
							<p class="gj_psv_packs">Home Appliances</p>
							<hr class="gj_psv_hr1">
							<p class="gj_psv_price">Price : <span class="gj_psv_money"> <i class="fa fa-inr"></i> <?php echo $product_price; ?></span></p>
							<hr class="gj_psv_hr1">
						</div>
						<div class="gj_psv_dis">
							<p class="gj_sht_desc">Short Description</p>
							<ul class="gj_psv_ul"><?php echo $product_short_desc; ?>
							</ul>
							<hr class="gj_psv_hr2">
						</div>
						<div class="gj_psv_disp">
							<table class="table">
								<tr>
									<th class="gj_thead">Vendor Id</th>
									<th>:</th>
									<td> <?php echo $product_vendor_id; ?></td>
									<th class="gj_thead">Product SKU</th>
									<th>:</th>
									<td> <?php echo $product_code; ?></td>
								</tr>
								<tr>
									<th class="gj_thead">Quantity</th>
									<th>:</th>
									<td> <?php echo $product_quantity; ?></td>
									<th class="gj_thead">Reorder Level</th>
									<th>:</th>
									<td> <?php echo $product_reorder_level; ?></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<hr class="gj_psv_last_hr">
				<div class="row">
					<div class="col-md-12">
						<div class="gj_psv_tab">
							<ul class="nav nav-tabs">
						    	<li class="active"><a data-toggle="tab" href="#description">Description</a></li>
						  	</ul>

						  	<div class="tab-content">
							    <div id="description" class="tab-pane fade in active"> <?php echo $product_desc; ?> </p>
							    </div>
						  	</div>
						</div>
					</div>
				</div>
				<input type="hidden" name="delete_product_id" value="<?php echo $product_id; ?>">
				<div class="country_width_100 col-12 mt-5">
					<div class="country_width_100">
						<div class="butt_sec_width mt-3 mb-3">
					      	<button type="submit" name="moduleadd_submit" class="country_button mr-2" >DELETE <i class="fa fa-pencil" aria-hidden="true"></i></button>		
						    <button type="reset" name="moduleadd_reset" class="country_button">CANCEL <i class="fa fa-times" aria-hidden="true"></i></button>
				      	
						</div>
			      	</div>
			      	
			        <div class="country_width_100 mt-3 mb-3">					      		  
					    <div class="country-right">
					      	<a href="<?php echo base_url('index.php/products/Product_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button></a>
					    </div>	
					</div>
				</div>
			</form>
		</div>	
	</div>

<?php }else{ ?>
	<div class="dkbody">
		<div class="container-fluid">
			<div class="page-header"> <h3>Select</h3> </div>
			<?php echo $search; ?>
		</div>
	</div>
	<script type="text/javascript"> $("#combobox").select2(); </script>
<?php }  ?>
<style type="text/css">
  ul.desc_cls li,ul.desc_cls li:hover {
      font-size: 13px;
      border: 0px;
  }
</style>