<div class="dkbody">
	<div class="container-fluid">
		<div class="page-header gj_new_pack">
			<h3>New Product</h3>
		</div>
		<p class="gj_package_dets"><span><i class="fa fa-dropbox" aria-hidden="true"></i></span> Product Details</p>
		<div class="page-content gj_pack_all">
			<section class="gj_pack_sec">
				<form method="post" acion="<?php echo base_url('index.php/products/product_crud/add'); ?>" enctype="multipart/form-data">
          <div class="row rr">
            <div class="col-lg-2 col-md-3 pull-left pr-0 gj_mt">
             <div class="checkboxFive">
                <input type="checkbox" id="user_choice" name="user_choice" value="2">  
                <label for="user_choice"></label>
                <span class="checkbox-material ml-4">
                  <span class="check">User Choice Product</span>
                </span>
              </div>
            </div>
          </div>
					<div class="row rr">
						<div class="col-lg-2 col-md-2 pull-left pr-0 pl-0">
							<span class="input input--nao">
								<input class="input__field input__field--nao product_code" type="text" id="product_add_product_sku"
								 name="product_code" value="<?php echo set_value('product_code'); ?>" />
								<label class="input__label input__label--nao" for="product_add_product_sku">
									<span class="input__label-content input__label-content--nao">Product SKU</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
								<img src="<?php echo base_url('assets/images/loader'); ?>/default.gif" class="ldr v_name_ldr">
							</span>		
							<p class="umail_suc avail_er suc">Available</p>
							<p class="umail_err avail_er err">Already Exists</p>
							<?php echo form_error('product_code','<p class="error">', '</p>'); ?>
						</div>

						<div class="col-lg-4 col-md-4 pull-left pr-0">
							<span class="input input--nao">
								<input class="input__field input__field--nao" type="text" id="product_add_product_name"
								 name="product_name" value="<?php echo set_value('product_name'); ?>"/>
								<label class="input__label input__label--nao" for="product_add_product_name">
									<span class="input__label-content input__label-content--nao">Product Name</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
							</span>	
							<?php echo form_error('product_name','<p class="error">', '</p>'); ?>	
						</div>

						<div class="col-lg-3 col-md-3 pull-left pr-0">
							<span class="input input--nao">
								<select class="input__field--nao gj_pro_select vendor_select" name="product_vendor_id">
									<?php foreach ($vendor as $list) {?>
										<option value="<?php echo $list->getVendorId();  ?>" <?php if(set_value('product_vendor_id') == $list->getVendorId()){echo "selected='selected'";} ?>><?php echo $list->getVendorName();  ?></option>
									<?php  } ?>
								</select>
							</span>
							<?php echo form_error('product_vendor_id','<p class="error">', '</p>'); ?>		
						</div>
            <div class="col-lg-3 col-md-3 pull-left pr-0">
              <span class="input input--nao">
                <select class="input__field--nao gj_pro_select cat_select" name="category_id">
                </select>
              </span>
              <?php echo form_error('category_id','<p class="error">', '</p>'); ?>    
            </div>
					</div>
					<div class="row rr">
            
            <div class="col-lg-3 col-md-3 pull-left pr-0">
              <span class="input input--nao" style="margin-left: 0;">
                <input class="input__field input__field--nao" type="number" id="product_add_product_price"
                  name="point_value" value="<?php echo set_value('point_value'); ?>" pattern="[0-9]+([\.,][0-9]+)?" step="0.01"/>
                <label class="input__label input__label--nao" for="product_add_product_price">
                  <span class="input__label-content input__label-content--nao">Point Value ( PV ) </span>
                </label>
                <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                  <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                </svg>
              </span>   
              <?php echo form_error('point_value','<p class="error">', '</p>'); ?>
            </div>
            <div class="col-lg-2 col-md-3 pull-left pr-0">
              <span class="input input--nao">
                <input class="input__field input__field--nao" type="number" id="product_add_product_price"
                  name="product_price" value="<?php echo set_value('product_price'); ?>" pattern="[0-9]+([\.,][0-9]+)?" step="0.01"/>
                <label class="input__label input__label--nao" for="product_add_product_price">
                  <span class="input__label-content input__label-content--nao">Product Price</span>
                </label>
                <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                  <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                </svg>
              </span>   
              <?php echo form_error('product_price','<p class="error">', '</p>'); ?>
            </div>
            <div class="col-lg-2 col-md-3 pull-left pr-0">
              <span class="input input--nao">
                <input class="input__field input__field--nao" type="number" id="product_add_product_price"
                  name="product_display_price" value="<?php echo set_value('product_display_price'); ?>" pattern="[0-9]+([\.,][0-9]+)?" step="0.01"/>
                <label class="input__label input__label--nao" for="product_add_product_price">
                  <span class="input__label-content input__label-content--nao">Product Display Price</span>
                </label>
                <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                  <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                </svg>
              </span>   
              <?php echo form_error('product_display_price','<p class="error">', '</p>'); ?>
            </div>


						<div class="col-lg-2 col-md-3 pull-left pr-0 pl-0">
							<span class="input input--nao">
								<input class="input__field input__field--nao" type="number" id="product_add_product_quantity"
								 name="product_quantity" value="<?php echo set_value('product_quantity'); ?>" />
								<label class="input__label input__label--nao" for="product_add_product_quantity">
									<span class="input__label-content input__label-content--nao">Product Quantity</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
							</span>	
							<?php echo form_error('product_quantity','<p class="error">', '</p>'); ?>
						</div>

						<div class="col-lg-2 col-md-3 pull-left pr-0">
							<span class="input input--nao">
								<input class="input__field input__field--nao" type="number" id="product_add_product_reorder_level"
								  name="product_reorder_level" value="<?php echo set_value('product_reorder_level'); ?>"/>
								<label class="input__label input__label--nao" for="product_add_product_reorder_level">
									<span class="input__label-content input__label-content--nao">Reorder Level</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
							</span>	
							<?php echo form_error('product_reorder_level','<p class="error">', '</p>'); ?>	
						</div>


					</div>

					<div class="row rr">
						<div class="col-lg-2 col-md-3 pull-left pr-0 gj_mt">
							<span class="access_add_select_placeholder pt-2">Package is Visible</span>
							<div class="bs-switch">
					            <input type="checkbox" name="is_active" class="bs-switch-checkbox" id="mySwitch" <?php if(set_value('is_active') == 1){echo "checked";} ?> value="1">
					            <label class="bs-switch-label" for="mySwitch">
					                <div class="bs-switch-inner"></div>
					                <div class="bs-switch-switch"></div>
					            </label>
					        </div>

						</div>

            

						<div class="col-lg-10 col-md-9 pull-left">
							<span class="input input--nao gj_desc_wid">
								<textarea class="input__field input__field--nao" rows="6" id="product_add_product_package_short_description" name="product_short_desc">
									<?php echo set_value('product_short_desc'); ?>
								</textarea>
								<label class="input__label input__label--nao" for="product_add_product_package_short_description">
									<span class="input__label-content input__label-content--nao">Package Short Description</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
							</span>		
							<?php echo form_error('product_short_desc','<p class="error">', '</p>'); ?>
						</div>
					</div>

					<div class="row rr">
						<div class="col-md-12 row pull-left pr-0">
							<span class="input input--nao gj_pack_desc">
								
								 <textarea class="input__field input__field--nao" rows="6" id="module_add_description" name="product_desc">
								 	  <?php echo set_value('product_desc'); ?>
								 </textarea>
								<label class="input__label input__label--nao" for="module_add_description">
									<span class="input__label-content input__label-content--nao">Package Description</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
							</span>		
							<?php echo form_error('product_desc','<p class="error">', '</p>'); ?>
						</div>
					</div>

					<p class="gj_pack_title">Package Pictures</p>
					<div class=" pkfile gj_file">
				      	<div class=" toppkfile" style="float:left">
				      		<input type="file"  name="files[]" id="files" class="form-control img-responsive"  multiple="multiple">
				     		 <div class="width-60px width-6px">

				     		 </div>
				      	</div>
			        </div>

			        <div class="country_width_100 col-12 mt-5">
						<div class="country_width_100">
							<div class="butt_sec_width mt-3 mb-3">
						      	<button type="submit" name="moduleadd_submit" class="country_button mr-2" >SUBMIT <i class="fa fa-paper-plane" aria-hidden="true"></i></button>		
							    <button type="reset" name="moduleadd_reset" class="country_button">RESET <i class="fa fa-refresh" aria-hidden="true"></i></button>
					      	
							</div>
				      	</div>
				      	
				        <div class="country_width_100 mt-3 mb-3">					      		  
						    <div class="country-right">
						      	<a href="<?php echo base_url('index.php/products/Product_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button></a>
						    </div>	
						</div>
					</div>
					<input type="hidden" name="prod" value="asdf">
				</form>
			</section>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
    $('textarea').each(function(){
            $(this).val($(this).val().trim());
        }
    );
	  $(".vendor_select").select2({
	  	placeholder: "Select Vendor",
	  });
	  $(".vendor_select").val('').trigger('change');

	  $('.v_name_ldr,.umail_suc,.umail_err').hide();
	});
	$('.product_code').focus(function()  {
      var a = $('.product_code').val();
      if(a.length>0){
        cc_name = a;
      }else{
        cc_name="";
      }
    })
    $('.product_code').blur(function()  {
      var product_code = $('.product_code').val();
      var letters = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9_]+)$/; 
      if(product_code.length>2 && cc_name != product_code){
      	if(product_code.match(letters)){
	        $('.v_name_ldr').show();
	        $.ajax({
	          type: "POST",
	          url: "<?php echo base_url(); ?>index.php/Common_controller/product_code_available",
	          data: {'field_value':product_code},
	            success:  function (data) { 
	              $('.v_name_ldr').hide();
	              if(data == 1){
	              	$('.umail_suc').show();
	              	$('.umail_err').hide();
	              }else{
	              	$('.umail_suc').hide();
	              	$('.umail_err').show();
	              }
	            }
	        })
	    }else{
	    	$('.umail_suc').hide();
          	$('.umail_err').show().text('Product Code Must be Alphanumeric');
	    }
      }else if(product_code.length == 0 && cc_name.length > 2){
       $('.umail_suc,.umail_err').hide();
      }
    })

  $('.cat_select').select2({
    placeholder: 'Select a Category',
    ajax: {
      url: '<?php echo base_url("index.php/common_controller/get_category_names") ?>',
      dataType: 'json',
      delay: 1,
      data: function (params) {console.log(params);
            return { q: params.term 
            };
      },
      processResults: function (data) { return { results: data }; },
      cache: true
    },
    allowClear: true,
    // minimumInputLength: 1
  });
</script>
<style type="text/css">
	.page-content section.content.bgcolor-1 form span.select2.select2-container.select2-container--default.select2-container--focus, span.select2.select2-container.select2-container--default.select2-container--below, span.select2.select2-container.select2-container--default {
	    width: 230px !important;
	    margin: 17px auto !important;
	    text-align: left;
	}
</style>
