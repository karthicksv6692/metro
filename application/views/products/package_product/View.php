<div class="dkbody">
	<div class="container-fluid">
		<div class="page-header">
			<h3>Package Product</h3>
		</div>
		<div class="page-content">                   
        <div class="container-fluid">
          <table id="bank_request" class="data_table_style">
              <thead>
                 <tr role="row">
                    <th>PACKAGE PRODUCT NAME</th>
                    <th>PACKAGE</th>
                    <th>PRICE</th>
                    <th>ACTIONS</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>
        <?php //if($add != null){ ?>
         <div class="per_rw btn-click pr-5">
              <a href="<?php echo base_url('index.php/products/Package_product_crud/add'); ?>"><button type="button" class="btn btn_row1  btn-7 btn-7a icon-truck mb-5" id="row1"> <span>ADD NEW</span></button></a>
          </div>
        <?php //} ?>
     	</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $('#bank_request').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": '<?php echo base_url(); ?>index.php/products/Package_product_crud/datatable_ajax',
        "order": [[ 0, "asc" ]],
        "pagingType": "full_numbers"
    });
});
</script>


<div id="dialog-confirm" title="DELETE">
     <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>Are you sure?</p>
</div>

<script>
	$('.dkbody').on('click','.btn_delete',function(){
	  var id = $(this).attr('data-value');
	  var tr = $(this).closest("tr");
	  var aa = $(this);
	   $( "#dialog-confirm" ).dialog({
	     resizable: true,
	     height: "auto",
	     width: 400,
	     modal: true,
	     buttons: {
	       "Delete": function() {
	          $.ajax({
	            type: "POST",
	            url: "<?php echo base_url(); ?>index.php/products/Package_product_crud/ajax_delete",
	            data: {'package_product_id': id },
	              success:  function (data) {
	                if(data == 1){
	                  tr.fadeOut(1000,function(){tr.remove()});
	                  toast({
	                    message: "Successfully Deleted",
	                    displayLength: 3000,
	                    className: 'success',
	                  });
	                  $('span.ui-button-icon.ui-icon.ui-icon-closethick').click();
	                }else{
	                  toast({
	                    message: "Deleted Failed",
	                    displayLength: 3000,
	                    className: 'error',
	                  });
	                  $('span.ui-button-icon.ui-icon.ui-icon-closethick').click();
	                }
	              }
	          })
	        },
	        Cancel: function() {
	          $(this).dialog( "close" );
	        }
	     }
	   });
	 });
</script>
<style>
#dialog-confirm{
 display:none;
}
</style>