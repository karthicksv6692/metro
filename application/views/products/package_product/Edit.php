<?php if(isset($obj)){ 

	
	if($obj != null){
		$package_product_id 				= $obj->getPackageProductId();
		$package_product_name 				= $obj->getPackageProductName();
		$package_product_quantity 			= $obj->getPackageProductQuantity();
		$package_product_display_price 		= $obj->getPackageProductDisplayPrice();
		$package_product_price 				= $obj->getPackageProductPrice();
		$package_product_desc 				= $obj->getPackageProductDesc();
		$package_product_short_desc 		= $obj->getPackageProductShortDesc();
		$is_active 							= $obj->getPackageProductIsVisible();
		$package_id 						= $obj->getPackageId();
	}else{
		$package_product_id  = '';$package_product_name  = '';$package_product_quantity  = '';$package_product_display_price  = '';$package_product_price = '';$package_product_desc= '';$package_product_short_desc = '';$is_active = '';$package_id  = '';
	}
?>

	<div class="dkbody">
		<div class="container-fluid">
			<div class="page-header gj_new_pack">
				<h3>Package Product - Edit</h3>
			</div>
			<p class="gj_package_dets"><span><i class="fa fa-dropbox" aria-hidden="true"></i></span> Package Product Details</p>
			<div class="page-content gj_pack_all">
				<section class="gj_pack_sec">
					<form method="post" action="<?php echo base_url('index.php/products/Package_product_crud/edit'); ?>" enctype="multipart/form-data">
						<div class="row rr">
							<div class="col-lg-3 col-md-3 pull-left pr-0 pl-0">
								<span class="input input--nao">
									<input class="input__field input__field--nao package_product_name" type="text" id="product_add_product_sku"
									 name="package_product_name" value="<?php echo $package_product_name; ?>" />
									<label class="input__label input__label--nao" for="product_add_product_sku">
										<span class="input__label-content input__label-content--nao">Package Product Name</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
								</span>		
								<p class="umail_suc avail_er suc">Available</p>
								<p class="umail_err avail_er err">Already Exists</p>
								<?php echo form_error('package_product_name','<p class="error">', '</p>'); ?>
							</div>

							<div class="col-lg-3 col-md-3 pull-left pr-0">
								<span class="input input--nao">
									<input class="input__field input__field--nao" type="number" id="product_add_product_name"
									  name="package_product_quantity" value="<?php echo $package_product_quantity; ?>" />
									<label class="input__label input__label--nao" for="product_add_product_name">
										<span class="input__label-content input__label-content--nao">Package Product Quantity</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
								</span>		
								<?php echo form_error('package_product_quantity','<p class="error">', '</p>'); ?>
							</div>

							<div class="col-lg-3 col-md-3 pull-left pr-0">
								<span class="input input--nao">
									<input class="input__field input__field--nao" type="number" id="product_add_product_name"
									 pattern="[0-9]+([\.,][0-9]+)?" step="0.01"  name="package_product_price" value="<?php echo $package_product_price; ?>" />
									<label class="input__label input__label--nao" for="product_add_product_name">
										<span class="input__label-content input__label-content--nao">Package Product Price</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
								</span>		
								<?php echo form_error('package_product_price','<p class="error">', '</p>'); ?>
							</div>

							<div class="col-lg-3 col-md-3 pull-left pr-0 pl-0">
								<span class="input input--nao">
									<input class="input__field input__field--nao" type="number" id="product_add_product_quantity"
									 pattern="[0-9]+([\.,][0-9]+)?" step="0.01"  name="package_product_display_price" value="<?php echo $package_product_display_price; ?>" />
									<label class="input__label input__label--nao" for="product_add_product_quantity">
										<span class="input__label-content input__label-content--nao">Package Product Display Price</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
								</span>	
								<?php echo form_error('package_product_display_price','<p class="error">', '</p>'); ?>
							</div>
						</div>

						<div class="row rr">
							<div class="col-lg-2 col-md-3 pull-left pr-0 gj_mt">
								<span class="access_add_select_placeholder pt-2">PackageProduct is Visible</span>
								<div class="bs-switch">
						            <input type="checkbox" name="is_active" class="bs-switch-checkbox" id="mySwitch" <?php if($is_active == 1){echo "checked";} ?> value="<?php echo $is_active; ?>">
						            <label class="bs-switch-label" for="mySwitch">
						                <div class="bs-switch-inner"></div>
						                <div class="bs-switch-switch"></div>
						            </label>
						        </div>

							</div>

							<div class="col-lg-10 col-md-9 pull-left">
								<span class="input input--nao gj_desc_wid">
									<textarea class="input__field input__field--nao" rows="6" id="product_add_product_package_short_description" name="package_product_short_desc"><?php echo $package_product_short_desc; ?></textarea>
									<label class="input__label input__label--nao" for="product_add_product_package_short_description">
										<span class="input__label-content input__label-content--nao">Package Short Description</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
								</span>	
								<?php echo form_error('package_product_short_desc','<p class="error">', '</p>'); ?>	
							</div>
						</div>

						<div class="row rr">
							<div class="col-md-12 row pull-left pr-0">
								<span class="input input--nao gj_pack_desc">
									 <textarea class="input__field input__field--nao" rows="6" id="module_add_description" name="package_product_desc"><?php echo $package_product_desc; ?></textarea>
									<label class="input__label input__label--nao" for="module_add_description">
										<span class="input__label-content input__label-content--nao">Package Description</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
								</span>	
								<?php echo form_error('package_product_desc','<p class="error">', '</p>'); ?>
							</div>
						</div>

						<div class="row rr">
							<div class="col-lg-3 col-md-4 row pull-left pr-0">
								<span class="input input--nao">
									<select id="gj_sel2" class="input__field--nao gj_pro_select gj_pp_sl package_id" name="package_id">
							       
											<option value="<?php echo $package_id->getPackageId();  ?>" selected><?php echo $package_id->getPackageName();  ?></option>
										
							        </select>
						        </span>
						        <?php echo form_error('package_id','<p class="error">', '</p>'); ?>
							</div>
						</div>



						<div class="gj_pp_append" id="jo">
							<?php if($package_product_item != null){ 
								for($i=0;$i<count($package_product_item);$i++){
							?>
								<div class="row gj_row rr svk_find_row">
									<div class="col-lg-3 col-md-4 row pull-left pr-0">
										<span class="input input--nao">
											<select id="prd_select cus_sel" class="input__field--nao gj_pro_select gj_pp_sl product_name prd_select" name="product_name[]" >
									            <option value="<?php echo $package_product_item[$i]->getPackageProductItemProductId()->getProductId(); ?>"><?php echo $package_product_item[$i]->getPackageProductItemProductId()->getProductName(); ?></option>
									        </select>
								        </span>
								        <?php echo form_error('package_id','<p class="error">', '</p>'); ?>
									</div>
									<div class="col-lg-3 col-md-4 row pull-left pr-0">
										<span class="input input--nao">
											<input class="input__field input__field--nao product_quantity" type="number" id="product_add_product_name"
											  name="product_quantity[]" value="<?php echo $package_product_item[$i]->getPackageProductQuantity(); ?>" />
											<label class="input__label input__label--nao" for="product_add_product_name">
												<span class="input__label-content input__label-content--nao">Count</span>
											</label>
											<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
												<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
											</svg>
										</span>	
										<?php echo form_error('product_quantity','<p class="error">', '</p>'); ?>
									</div>
									<div class="col-lg-6 col-md-4 row pull-left pr-0">
										<div class="gj_buttons">
											<button type="button" name="moduleadd_submit" class="country_button mr-2 pp_delete" >DELETE <i class="fa fa-trash-o"></i></button>	
										</div>
									</div>
								</div>
							<?php }}else{ ?>
								<div class="row gj_row rr svk_find_row">
									<div class="col-lg-3 col-md-4 row pull-left pr-0">
										<span class="input input--nao">
											<select id="prd_select" class="input__field--nao gj_pro_select gj_pp_sl product_name prd_select" name="product_name[]" >
									            <?php foreach ($product as $list) {?>
													<option value="<?php echo $list->getProductId();  ?>"><?php echo $list->getProductName();  ?></option>
												<?php  } ?>
									        </select>
								        </span>
								        <?php echo form_error('package_id','<p class="error">', '</p>'); ?>
									</div>
									<div class="col-lg-3 col-md-4 row pull-left pr-0">
										<span class="input input--nao">
											<input class="input__field input__field--nao product_quantity" type="number" id="product_add_product_name"
											  name="product_quantity[]" value="" />
											<label class="input__label input__label--nao" for="product_add_product_name">
												<span class="input__label-content input__label-content--nao">Count</span>
											</label>
											<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
												<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
											</svg>
										</span>	
										<?php echo form_error('product_quantity','<p class="error">', '</p>'); ?>
									</div>
									<div class="col-lg-6 col-md-4 row pull-left pr-0">
										<div class="gj_buttons">
											<button type="button" name="moduleadd_submit" class="country_button mr-2 pp_delete" >DELETE <i class="fa fa-trash-o"></i></button>	
										</div>
									</div>
								</div>
							<?php } ?>
						</div>

						<div class="row rr">
							<div class="col-lg-6 col-md-6 pull-left pr-0 offset-lg-6 offset-md-6">
								<div class="gj_apnd_but">
									<button type="button" name="moduleadd_submit" id="pp_add" class="country_button mr-2" >ADD <i class="fa fa-plus"></i></button>
								</div>
							</div>
						</div>

						<hr>
						<p class="gj_pack_title">Package Pictures</p>
						<div class=" pkfile gj_file">
							<?php 
								$ar_img = array();
								if(isset($attachment)){ 

										for($i=0;$i<count($attachment);$i++){
											array_push($ar_img, $attachment[$i]->getAttachmentImageUrl());
									?>

										<div class="p1-0 mr-2 mb-2 bdrstyle_hover" style="float:left">
											
											<img class="width-60px bdrstyle ad_imgs" src="<?php echo base_url($attachment[$i]->getAttachmentImageUrl()); ?>"><span>X</span>
										</div>
										<?php
									}
								}
							?>
							<input type="hidden" class="package_image" name="package_image" value=<?php echo implode(',', $ar_img); ?> >
								<input type="hidden" class="nw_ad_img" name="nw_ad_img" value="0">
					      	<div class=" toppkfile" style="float:left">
					      		<input type="file"  name="files[]" id="files" class="form-control img-responsive"  multiple="multiple">
					     		 <div class="width-60px width-6px">

					     		 </div>
					      	</div>
				        </div>
				        <input type="hidden" name="edit_package_product_id" value="<?php echo $package_product_id; ?>">
				        <div class="country_width_100 col-12 mt-5">
							<div class="country_width_100">
								<div class="butt_sec_width mt-3 mb-3">
							      	<button type="submit" name="moduleadd_submit" class="country_button mr-2" >SUBMIT <i class="fa fa-paper-plane" aria-hidden="true"></i></button>		
								    <button type="reset" name="moduleadd_reset" class="country_button">RESET <i class="fa fa-refresh" aria-hidden="true"></i></button>
						      	
								</div>
					      	</div>
					      	
					        <div class="country_width_100 mt-3 mb-3">					      		  
							    <div class="country-right">
							      	<a href="<?php echo base_url('index.php/products/Package_product_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button></a>
							    </div>	
							</div>
						</div>
						
					</form>
				</section>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
		  // $('#gj_sel2').select2({
    //     placeholder: 'Select Package',
    //     ajax: {
    //       url: '<?php echo base_url("index.php/common_controller/get_package_names_aj") ?>',
    //       dataType: 'json',
    //       delay: 1,
    //       data: function (params) {console.log(params);
    //             return { q: params.term // search term
    //             };
    //       },
    //       processResults: function (data) { return { results: data }; },
    //       cache: true
    //     },
    //   });
		  $(".prd_select").select2({
		  	placeholder: "Select Product",
		  });
		  	<?php 
			if($package_product_item != null){
				// for($i=0;$i<count($package_product_item);$i++){?>
					
				// 		$("#prd_select#cus_sel-<?php echo $i; ?>").val(<?php echo $package_product_item[$i]->getPackageProductItemProductId()->getProductId(); ?>).trigger('change');
					
				// 	<?php
				// }
			}else{ ?>
				//$(".prd_select").val('').trigger('change');
			<?php } ?>
		     
			<?php if($package_id != null){?>
		     	//$("#gj_sel2").val(<?php echo $package_id->getPackageId(); ?>).trigger('change');
			<?php }else{?>
		     	//$("#gj_sel2").val('').trigger('change');
	      	<?php } ?>

		});
	</script>

	<script>
		$('.v_name_ldr,.umail_suc,.umail_err').hide();
		$('.package_product_name').focus(function()  {
	      var a = $('.package_product_name').val();
	      if(a.length>0){
	        cc_name = a;
	      }else{
	        cc_name="";
	      }
	    })
	    $('.package_product_name').blur(function()  {
	      	var package_product_name = $('.package_product_name').val();
	      	if(package_product_name.length>2 && cc_name != package_product_name){
		        $('.v_name_ldr').show();
		        $.ajax({
		          type: "POST",
		          url: "<?php echo base_url(); ?>index.php/Common_controller/package_product_name_available_on_update",
		          data: {'field_value':package_product_name,'pk':<?php echo $package_product_id; ?>},
		            success:  function (data) { 
		              $('.v_name_ldr').hide();
		              if(data == 1){
		              	$('.umail_suc').show();
		              	$('.umail_err').hide();
		              }else{
		              	$('.umail_suc').hide();
		              	$('.umail_err').show();
		              }
		            }
		        })
		    
	      	}else if(package_product_name.length == 0 && cc_name.length > 2){
		       $('.umail_suc,.umail_err').hide();
	      	}
	    })
	</script>

	<?php 
		$id_ar = array();
		$name_ar = array();
		function js_str($s) { return '"' . addcslashes($s, "\0..\37\"\\") . '"'; }
		function js_array($array) { 
		$temp = array_map('js_str', $array);
		return '[' . implode(',', $temp) . ']';
		}
		foreach ($product as $list) {  
			array_push($id_ar, $list->getProductId());
			array_push($name_ar, $list->getProductName());
		}
	?>
	<script>

		$(document).ready(function(){
			$("#pp_add").click(function(){ //alert($('.svk_find_row').length);

				var a = <?php echo js_array($id_ar); ?>;
				var b = <?php echo js_array($name_ar); ?>;

				var name = $( ".svk_find_row:last" ).find('.product_name').val();
				var qty = $( ".svk_find_row:last" ).find('.product_quantity').val();
				
				if(name.length > 0 && qty.length > 0){
			   		$("#jo").append('<div class="row gj_row rr svk_find_row"><div class="col-lg-3 col-md-4 row pull-left pr-0">	<span class="input input--nao">		<select id="prd_select" class="input__field--nao gj_pro_select gj_pp_sl product_name prd_select" name="product_name[]"><option  class="xx"></option>  </select>       </span>       <?php echo form_error('package_id','<p class="error">', '</p>'); ?></div><div class="col-lg-3 col-md-4 row pull-left pr-0">	<span class="input input--nao">		<input class="input__field input__field--nao product_quantity" type="number" id="product_add_product_name"		  name="product_quantity[]" value="" />		<label class="input__label input__label--nao" for="product_add_product_name">			<span class="input__label-content input__label-content--nao">Count</span>		</label>		<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">			<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>		</svg>	</span>		<?php echo form_error('product_quantity','<p class="error">', '</p>'); ?></div><div class="col-lg-6 col-md-4 row pull-left pr-0">	<div class="gj_buttons">		<button type="button" name="moduleadd_submit" class="country_button mr-2 pp_delete" >DELETE <i class="fa fa-trash-o"></i></button>		</div></div></div>');
			   		     
			   	}
			   	for(i=0;i<a.length;i++){
			   		$('.xx').after('<option value="'+a[i]+'"> '+b[i]+'</option>');
		   			
		   		} 
			   	$(".prd_select").select2({
				  	placeholder: "Select Product",
				});
			});
		});
		$(document).on('click',".pp_delete",function(){
			$(this).closest('.gj_row').remove();
		});
		$(document).on('click','.bdrstyle_hover>span',function(){
	    	url = $(this).parent().find("img.ad_imgs").attr("src");
	    	var parts = url.split('/');
			var lastSegment = parts.pop() || parts.pop();  // handle potential trailing slash

			lastSegment = 'attachments/'+lastSegment;


			var pck_vals = $('.package_image').val();
			var ar_val = pck_vals.split(",");

			var index = ar_val.indexOf(lastSegment);    // <-- Not supported in <IE9

			var removedCars = ar_val.splice(index,1);
			$('.package_image').val(ar_val)
			
			console.log(ar_val);
	    	$(this).parent().remove();
	    	$('.nw_ad_img').val(1);
	  	}); 
	  	$('#files').change(function(){
	  		$('.nw_ad_img').val(1);
	  	})
	</script>
	<style type="text/css">
		.page-content section.content.bgcolor-1 form span.select2.select2-container.select2-container--default.select2-container--focus, span.select2.select2-container.select2-container--default.select2-container--below, span.select2.select2-container.select2-container--default {
		    width: 230px !important;
		    margin: 19px auto !important;
		    text-align: left;
		}
		.gj_buttons button {
		    margin-top: 32px;
		    margin-left: 10px;
		}
	</style>

<?php }else{ ?>
	<div class="dkbody">
		<div class="container-fluid">
			<div class="page-header"> <h3>Select</h3> </div>
			<?php echo $search; ?>
		</div>
	</div>
	<script type="text/javascript"> $("#combobox").select2(); </script>
<?php }  ?>


