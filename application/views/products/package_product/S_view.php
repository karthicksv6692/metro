<?php if(isset($obj)){ 

	
	if($obj != null){
		$package_product_id 				= $obj->getPackageProductId();
		$package_product_name 				= $obj->getPackageProductName();
		$package_product_quantity 			= $obj->getPackageProductQuantity();
		$package_product_display_price 		= $obj->getPackageProductDisplayPrice();
		$package_product_price 				= $obj->getPackageProductPrice();
		$package_product_desc 				= $obj->getPackageProductDesc();
		$package_product_short_desc 		= $obj->getPackageProductShortDesc();
		$is_active 							= $obj->getPackageProductIsVisible();
		$package_id 						= $obj->getPackageId();
	}else{
		$package_product_id  = '';$package_product_name  = '';$package_product_quantity  = '';$package_product_display_price  = '';$package_product_price = '';$package_product_desc = '';$package_product_short_desc = '';$is_active = '';$package_id  = '';
	}
	if(isset($attachment) && !empty($attachment)){
		$package_image=$attachment;
	}
?>

<div class="dkbody">
	<div class="container-fluid">
		<div class="page-header gj_new_pack">
			<h3>Package Product</h3>
		</div>
		<p class="gj_package_dets"><span><i class="fa fa-cubes"></i></span> Package Product Details</p>
		<div class="row">
			<div class="col-md-12 col-lg-4 naveeno_single_car_border">
    			<div id="jssor_2" style="position:relative;margin:0 auto;top:70px;left:0px;width:541px;height:477px;overflow:hidden;visibility:hidden;">
        			<!-- Loading Screen -->
      				<div data-u="loading" style="position:absolute;top:0px;left:0px;">
			            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
			            <div style="position:absolute;display:block;top:0px;left:0px;width:100%;height:100%;"></div>
      				</div>
      				<div data-u="slides" class="gj_jssor_slides">
        				<?php if(isset($package_image)){ 
      						for($i=0;$i<count($package_image);$i++){
						?>
	        				<div>
					          	<img data-u="image" src="<?php echo base_url($package_image[$i]->getAttachmentImageUrl()); ?>" class="img-responsive"/>
						        <div data-u="thumb">
						            <img class="i" src="<?php echo base_url($package_image[$i]->getAttachmentImageUrl()); ?>" />
						        </div>
					        </div>
					    <?php  }}else{ ?>

					    	<div>
					          	<img data-u="image" src="<?php echo base_url('assets/images'); ?>/no_image.jpg" class="img-responsive"/>
						        <div data-u="thumb">
						            <img class="i" src="<?php echo base_url('assets/images'); ?>/no_image.jpg" />
						        </div>
					        </div>

				     	<?php  } ?>
      				</div>
      				<!-- Thumbnail Navigator -->
      				<div data-u="thumbnavigator" class="jssort11-130-64 gj_jssor_thums" data-autocenter="1">
            			<!-- Thumbnail Item Skin Begin -->
        				<div data-u="slides" style="cursor: default; width:200px;">
            				<div data-u="prototype" class="p">
                				<div data-u="thumbnailtemplate" class="tp"></div>
            				</div>
        				</div>
        				<!-- Thumbnail Item Skin End -->
  					</div>
			        <!-- Arrow Navigator -->
			      	<div data-u="arrowleft" class="jssora02l" style="top:0px;left:8px;width:55px;height:55px;" data-autocenter="2">
			      		<i class="fa fa-angle-left"></i>
			      	</div>
			        <div data-u="arrowright" class="jssora02r" style="top:0px;right:218px;width:55px;height:55px;" data-autocenter="2">
			        	<i class="fa fa-angle-right"></i>
		        	</div>
		      	</div>
 				<script type="text/javascript">jssor_2_slider_init();</script>
			</div>
			
			<div class="col-md-12 col-lg-8">
				<div class="gj_psv_display">
					<p class="gj_pck"> <?php echo $package_product_name; ?> <span class="gj_psv_star"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span><span class="gj_psv_rating">4.5 Stars</span></p>
					<p class="gj_psv_packs">Home Appliances</p>
					<hr class="gj_psv_hr1">
					<p class="gj_psv_price">Price : <span class="gj_ppsv_price gj_line_through"> <i class="fa fa-inr"></i> <?php echo $package_product_price; ?></span></p>
					<p class="gj_psv_price">Display Price : <span class="gj_psv_money"> <i class="fa fa-inr"></i> <?php echo $package_product_display_price; ?></span></p>
					<p class="gj_psv_price">Quantity : <span class="gj_ppsv_price">  <?php echo $package_product_quantity; ?></span></p>
					<hr class="gj_psv_hr1">
				</div>
				<div class="gj_psv_dis">
					<p class="gj_sht_desc">Short Description</p>
					<ul class="gj_psv_ul">
						<li><?php echo $package_product_short_desc; ?></li>
						
					</ul>
					<hr class="gj_psv_hr2">
				</div>
			</div>
		</div>
		<hr class="gj_psv_last_hr">

		<div class="row">
			<div class="col-md-12">
				<div class="gj_ppsv_table">
					<div class="table-responsive">          
					 	<table class="table text-center">
					    	<thead>
					     		<tr>
							        <th class="text-center">Product</th>
							        <th class="text-center">Count</th>
					      		</tr>
					    	</thead>
					    	<tbody>
					    		<?php if($package_product_item != null){ 
									for($i=0;$i<count($package_product_item);$i++){
								?>
					      		<tr>
							        <td><?php echo $package_product_item[$i]->getPackageProductItemProductId()->getProductName(); ?></td>
							        <td><?php echo $package_product_item[$i]->getPackageProductQuantity(); ?></td>
					      		</tr>
					      		<?php }} ?>
					    	</tbody>
					  	</table>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="gj_psv_tab">
					<ul class="nav nav-tabs">
				    	<li class="active"><a data-toggle="tab" href="#description">Description</a></li>
				    	<li><a data-toggle="tab" href="#client">Client Reviews</a></li>
				  	</ul>

				  	<div class="tab-content">
					    <div id="description" class="tab-pane fade in active">
				      		<p><?php echo $package_product_desc; ?></p>
					    </div>
					    <div id="client" class="tab-pane fade">
					      	<p><?php echo $package_product_desc; ?></p>
					    </div>
				  	</div>
				</div>
			</div>
		</div>
		<div class="country_width_100 col-12 mt-5">
			<div class="country_width_100">
				<div class="butt_sec_width mt-3 mb-3">
					<form method="post" action="<?php echo base_url('index.php/products/Package_product_crud/edit') ?>">
					<input type="hidden" value="<?php echo $package_product_id; ?>" name="package_product_id">
			      	<button type="submit" name="moduleadd_submit" class="country_button mr-2" >EDIT <i class="fa fa-pencil" aria-hidden="true"></i></button>	
			      	</form>

				    <button type="reset" name="moduleadd_reset" class="country_button">DELETE <i class="fa fa-times" aria-hidden="true"></i></button>
		      	
				</div>
	      	</div>
	      	
	        <div class="country_width_100 mt-3 mb-3">					      		  
			    <div class="country-right">
			      	<a href="<?php echo base_url('index.php/products/Package_product_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button></a>
			    </div>	
			</div>
		</div>
	</div>	
</div>

<?php }else{ ?>
	<div class="dkbody">
		<div class="container-fluid">
			<div class="page-header"> <h3>Select</h3> </div>
			<?php echo $search; ?>
		</div>
	</div>
	<script type="text/javascript"> $("#combobox").select2(); </script>
<?php }  ?>
