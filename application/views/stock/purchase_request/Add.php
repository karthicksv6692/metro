<div class="dkbody">
	<div class="container-fluid">
		<div class="page-header gj_new_pack">
			<h3>New Purchase Request</h3>
		</div>
		<p class="gj_package_dets"><span><i class="fa fa-dropbox" aria-hidden="true"></i></span> Purchase Request Details</p>
		<div class="page-content gj_pack_all">
			<section class="gj_pack_sec">
				<form method="post" action="<?php echo base_url('index.php/stock/Purchase_request_crud/add'); ?>">
					<div class="row rr">
						<div class="col-lg-12 col-md-12 pull-left">
							<div class="gj_pur_bill">
								<p class="gj_purse_bill">Bill Number<span class="gj_bill_sep">:</span><span class="gj_bill_no"><?php if(set_value('gj_bill_no') != null){echo set_value('gj_bill_no');} ?></span></p>
					        </div>
						</div>
					</div>
					<div class="row rr">
						<div class="col-lg-4 col-md-6 row pull-left">
							<span class="input input--nao">
								<select id="gj_sel2" class="input__field--nao gj_pro_select gj_pp_sl vendor" name="vendor_id">
									<?php foreach ($vendor_list as $list) { 
										if($list->getVendorAddressId() != null){
											$address =  $list->getVendorAddressId()->get_Address_full_address();
											if($list->getVendorAddressId()->get_Address_pincode_id() != null){
												$pincode =  $list->getVendorAddressId()->get_Address_pincode_id()->getPincodeNo();
												$district =  $list->getVendorAddressId()->get_Address_pincode_id()->getPincodeDistrict();
												$state =  $list->getVendorAddressId()->get_Address_pincode_id()->getPincodeState();
												if($list->getVendorAddressId()->get_Address_pincode_id()->getPincodeCountryId() != null){
													$country = $list->getVendorAddressId()->get_Address_pincode_id()->getPincodeCountryId()->getCountryName();
												}else{$country = "";}
												
											}else{$pincode="";$district ="";$state ="";$country = "";}
										}else{$address="";$pincode="";$district ="";$state ="";$country = "";}
									?>

						            <option value="<?php echo $list->getVendorId(); ?>" data_address = "<?php echo $address; ?>" data_district = "<?php echo $district; ?>" data_pincode = "<?php echo $pincode; ?>" data_state = "<?php echo $state; ?>" data_country = "<?php echo $country; ?>" <?php if(set_value('vendor_id') != null){ if($list->getVendorId() == set_value('vendor_id') ){echo "selected='selected'";} }?>><?php echo $list->getVendorName(); ?></option>

									<?php } ?>

						        </select>
					        </span>
					        <?php echo form_error('vendor_id','<p class="error">', '</p>'); ?>
						</div>
						<div class="col-lg-4 col-md-6 row pull-left">
							<div class="col-lg-4 col-md-5 pull-left pr-0 pl-0">
								<div class="gj_pur">
									<p class="gj_pur_hd">Shop Name :</p>
								</div>
							</div>
							<div class="col-lg-8 col-md-7 pull-left pr-0 pl-0">
								<div class="gj_pur">
									<p class="gj_pur_data vendor_company_name"></p>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 row pull-left gj_add_mar">
							<div class="col-lg-3 col-md-4 pull-left pr-0 pl-0">
								<div class="gj_pur">
									<p class="gj_pur_hd">Address :</p>
								</div>
							</div>
							<div class="col-lg-9 col-md-8 pull-left pr-0 pl-0">
								<div class="gj_pur">
									<p class="gj_pur_data vendor_address"></p>
									<p class="gj_pur_data vendor_district"><span class="vendor_pin"></span></p>
									<p class="gj_pur_data vendor_state"></p>
									<p class="gj_pur_data vendor_country"></p>
								</div>
							</div>
						</div>
					</div>

					<hr>
					<div class="row rr">
						<div class="col-lg-12">
							<div class="gj_apnd_but pull-right">
								<button type="button" name="moduleadd_submit" id="pp_add" class="country_button mr-2 gj_pur_but" >ADD <i class="fa fa-plus"></i></button>
							</div>
						</div>
					</div>

				<div class="gj_purchase_table">
					<div class="gj_pur_bill_table">
						<div class="gj_bill_table table-responsive">         
  							<table class="table table-hover table-bordered table-striped">
							    <thead>
							      	<tr>
								        <th class="text-center">Product Name</th>
								        <th class="text-center" style=" width: 150px !important;">Quantity</th>
								        <th class="text-center" style=" width: 180px !important;">Price Per Quality</th>
								        <th class="text-center" style=" width: 210px !important;">Total Price</th>
								        <th class="text-center" style=" width: 100px !important;">Action</th>
							      	</tr>
							    </thead>
							      	<?php if(set_value('product_id') != null){ ?>
							      		<tbody id="jox">
									<?php for($i=0;$i<count(set_value('prd'));$i++){ ?>
										<tr class="<?php if(in_array(set_value('prd'), $product_list) != -1){echo "tr".$product_list[$i]->getProductId()."_".$product_list[$i]->getProductName();} ?>">
										<?php if(count(set_value('prd'))>1 && set_value('prd')[0] != null){ ?>
											<td>
												<span class="input input--nao product_sel_sel">
													<select id="" class="input__field--nao product_sel clx-<?php echo $i;?>" name="product_id[]">
														<?php foreach ($product_list as $list) {
															 if(set_value('prd')[$i] == $list->getProductId()){
														?>
															<option value="<?php echo $list->getProductId();  ?>"  class="<?php echo $list->getProductId(); ?>_<?php echo str_replace(' ', '', $list->getProductName()); ?>" data_price="<?php echo $list->getProductPrice(); ?>" selected="selected"> <?php echo $list->getProductName();?></option>
															<script type="text/javascript">
													
																$(".product_sel").select2({ placeholder: "Select Product"});
																$(this).closest('tr').find('.product_sel').val(<?php echo set_value('prd')[$i];  ?>).trigger('change');
															</script>
														<?php } } ?>
														<script type="text/javascript">
																$(".product_sel").select2({ placeholder: "Select Product"});
														</script>
													</select>
													<input type="hidden" name="prd[]" class="prd"  value="<?php echo set_value('prd')[$i] ?>" >
												</span>
												<?php echo form_error('product_id','<p class="error">', '</p>'); ?>
												<script type="text/javascript">
													$(this).closest('tr').find('.cd').addClass('checked');
												</script>
											</td>
										<?php }else{ ?>
												<td>
													<span class="input input--nao product_sel_sel">
														<select id="" class="input__field--nao product_sel clx-<?php echo $i;?>" name="product_id[]">
															<?php foreach ($product_list as $list) { ?>
																<option value="<?php echo $list->getProductId();  ?>"  class="<?php echo $list->getProductId(); ?>_<?php echo str_replace(' ', '', $list->getProductName()); ?>" data_price="<?php echo $list->getProductPrice(); ?>" selected="selected"> <?php echo $list->getProductName();?></option>
																<script type="text/javascript">
														
																	$(".product_sel").select2({ placeholder: "Select Product"});
																	
																</script>
															<?php  } ?>
															<script type="text/javascript">
																	$(".product_sel").select2({ placeholder: "Select Product"});
																	$('.product_sel').closest('tr').find('.product_sel').val("").trigger('change');
															</script>
														</select>
														<input type="hidden" name="prd[]" class="prd"  value="<?php echo set_value('prd')[$i] ?>" >
													</span>
													<?php echo form_error('product_id','<p class="error">', '</p>'); ?>
													<script type="text/javascript">
														$(this).closest('tr').find('.cd').addClass('checked');
													</script>
												</td>
										<?php } ?>

											<td>
												<span class="input input--nao">
													<input class="input__field input__field--nao prd_qty" type="number" name="quantity[]" value="<?php echo set_value('quantity')[$i] ?>" />
													<label class="input__label input__label--nao" for="product_add_product_quantity">
														<span class="input__label-content input__label-content--nao">Quantity</span>
													</label>
													<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none"><path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></svg>
												</span>
												<?php echo form_error('quantity','<p class="error">', '</p>'); ?>
											</td>
											<td>
												<span class="input input--nao">
													<input class="input__field input__field--nao prd_sel_qty" type="number" name="pp_qty[]"  value="<?php echo set_value('pp_qty')[$i] ?>"/>
													<label class="input__label input__label--nao" for="product_add_product_quantity">
													<span class="input__label-content input__label-content--nao">Price Per Quality</span>
													</label>
													<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
													<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></svg>
												</span>
											</td>
											<td>
												<span class="input input--nao">
													<input class="input__field input__field--nao prd_prc_rw" type="number" id="product_add_product_quantity" name="tot_price[]"  value="<?php echo set_value('tot_price')[$i] ?>"/>
													<label class="input__label input__label--nao" for="product_add_product_quantity">
													<span class="input__label-content input__label-content--nao">Total Price</span>
													</label>
													<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
													<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></svg>
												</span>
											</td>
											<td>
												<button type="button" name="moduleadd_submit" class="country_button mr-2 pp_delete" >DELETE <i class="fa fa-trash-o"></i></button>
											</td>
										</tr>
									<?php }?>
										</tbody>
									<?php }else{?>
										<tbody id="jox">
									 	</tbody>
									<?php } ?>
							</table>
						</div>
					</div>

					<div class="gj_total_bill_table">
						<div class="gj_total_bill table-responsive">         
  							<table class="table table-hover table-bordered table-striped">
							    <tbody>
							      	<tr>
								        <td class="gj_spe_td" ></td>
								        <td class="gj_spce_td">Total</td>

								        <?php if(set_value('tot_price') != null){ ?>
								        	<td class="tot_price"><?php echo set_value('tot_price'); ?></td>
								        <?php }else{ ?>
								        	<td class="tot_price">0.00</td>
								        <?php } ?>
								        <input type="hidden" class="tot_price" name="tot_price" value="<?php if(set_value('tot_price') != null){echo set_value('tot_price');}else{echo "";} ?>">
							      	</tr>
							      	<tr>
								        <td class="gj_spe_td"></td>
								        <td class="gj_spce_td">Tax</td>
								         <?php if(set_value('tot_price') != null){ ?>
								        	<td class="tax_price"><?php echo set_value('tot_price')*10/100; ?></td>
								        <?php }else{ ?>
								        	<td class="tax_price">0.00</td>
								        <?php } ?>
							      	</tr>
							      	<tr>
								        <td class="gj_spe_td"></td>
								        <td class="gj_spce_td">Grand Total</td>
								         <?php if(set_value('tot_price') != null){ ?>
								        	<td class="g_tot_price"><?php echo set_value('tot_price')*10/100 + set_value('tot_price'); ?></td>
							        	<?php }else{ ?>
								        	<td class="g_tot_price">0.00</td>
								        <?php } ?>
							      	</tr>
							    </tbody>
							</table>
						</div>
					</div>
				</div>

					<div class="row rr">
						<div class="col-lg-4 col-md-5 pull-left pr-0 row">
							<span class="input input--nao">
								<input class="input__field input__field--nao" type="text"  id="gj_datepicker"
								name="estimate_date" />
								<label class="input__label input__label--nao" for="gj_datepicker">
									<span class="input__label-content input__label-content--nao">Estimate Delivery Date</span>
								</label>
								<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
									<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
								</svg>
							</span>	
						</div>
					</div>

			        <div class="country_width_100 col-12 mt-5">
						<div class="country_width_100">
							<div class="butt_sec_width mt-3 mb-3 gj_pur_butt">
						      	<button type="submit" name="moduleadd_submit" class="country_button mr-2 gj_pur_create" >Create Request <i class="fa fa-paper-plane" aria-hidden="true"></i></button>		
							    <button type="reset" name="moduleadd_reset" class="country_button">RESET <i class="fa fa-refresh" aria-hidden="true"></i></button>
					      	
							</div>
				      	</div>
				      	
				        <div class="country_width_100 mt-3 mb-3">					      		  
						    <div class="country-right">
						      	<a href="<?php echo base_url('index.php/stock/Purchase_request_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button></a>
						    </div>	
						</div>
					</div>
					<input type="hidden" name="ad_pr" value="asdf">
					<input type="hidden" name="gj_bill_no" class="gj_bill_no" value="<?php if(set_value('gj_bill_no') != null){echo set_value('gj_bill_no');} ?>">
				</form>
			</section>
		</div>
	</div>
</div>
<?php 
	$id_ar = array();
	$name_ar = array();
	$price_ar = array();
	function js_str($s) { return '"' . addcslashes($s, "\0..\37\"\\") . '"'; }
	function js_array($array) { 
		$temp = array_map('js_str', $array);
		return '[' . implode(',', $temp) . ']';
	}
	foreach ($product_list as $list) {  
		array_push($id_ar, $list->getProductId());
		array_push($name_ar, $list->getProductName());
		array_push($price_ar, $list->getProductPrice());
	}
?>

<script type="text/javascript">
	var sel_ids = [];
	var tot = 0;
	var a = <?php echo js_array($id_ar); ?>;
	var b = <?php echo js_array($name_ar); ?>;
	var c = <?php echo js_array($price_ar); ?>;
	$(document).ready(function() {
	  	$("#gj_sel2").select2({
	  		placeholder: "Select Vendor Id",
	  	});
	  	
	  <?php if(set_value('product_id') == null){ ?>
	  	var idx = $('.xx').length-1;
	  	$("#jox").append('<tr><td>	<span class="input input--nao product_sel_sel">		<select id="" class="input__field--nao product_sel" name="product_id[]">			<option  class="xx"></option>		</select><input type="hidden" name="prd[]" class="prd">	</span>	<?php echo form_error('product_id','<p class="error">', '</p>'); ?></td><td>	<span class="input input--nao">		<input class="input__field input__field--nao prd_qty" type="number" name="quantity[]" />		<label class="input__label input__label--nao" for="product_add_product_quantity">			<span class="input__label-content input__label-content--nao">Quantity</span>		</label>		<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none"><path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></svg>	</span>	<?php echo form_error('quantity','<p class="error">', '</p>'); ?></td><td>	<span class="input input--nao">		<input class="input__field input__field--nao prd_sel_qty" type="number" name="pp_qty[]" />		<label class="input__label input__label--nao" for="product_add_product_quantity">		<span class="input__label-content input__label-content--nao">Price Per Quality</span>		</label>		<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">		<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></svg>	</span></td><td>	<span class="input input--nao">		<input class="input__field input__field--nao prd_prc_rw" type="number" id="product_add_product_quantity" name="tot_price[]" />		<label class="input__label input__label--nao" for="product_add_product_quantity">		<span class="input__label-content input__label-content--nao">Total Price</span>		</label>		<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">		<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></svg>	</span></td><td>	<button type="button" name="moduleadd_submit" class="country_button mr-2 pp_delete" >DELETE <i class="fa fa-trash-o"></i></button></td> </tr>');
	  	for(i=0;i<a.length;i++){
			if(sel_ids.indexOf(a[i]) == -1){
	   			$('.xx:eq('+idx+')').after('<option value="'+a[i]+'" class="'+a[i]+'_'+b[i].replace(/\s/g, '')+'" data_price="'+c[i]+'"> '+b[i]+'</option>');
	   		}
   			
   		}
   		$(".product_sel").select2({
	  		placeholder: "Select Product",
	  	});

	  	<?php } ?>

	  	<?php if(set_value('gj_bill_no') == null){?>
	        $.ajax({
	          type: "POST",
	          url: "<?php echo base_url(); ?>index.php/Common_controller/get_available_bill_no",
	            success:  function (data) { 
	             $('.gj_bill_no').text(data);
	             $('.gj_bill_no').val(data);
	            }
	        })
	    <?php } ?>





	});
	$("#gj_sel2").val('').trigger('change');
	// $(".product_sel").val('').trigger('change');

	$(document).on('blur','input',function(){
	  if($(this).val().length>=1){
	    $(this).parent().addClass('input--filled');
	  }
	});

	$(document).on('change','.vendor',function(){
		var index = $('.vendor').length-1;
		var company_name = $('.vendor:eq('+index+') option:selected').text();
		var address = $('.vendor:eq('+index+') option:selected').attr('data_address');
		var district = $('.vendor:eq('+index+') option:selected').attr('data_district');
		var pincode = $('.vendor:eq('+index+') option:selected').attr('data_pincode');
		var state = $('.vendor:eq('+index+') option:selected').attr('data_state');
		var country = $('.vendor:eq('+index+') option:selected').attr('data_country');

		$('.vendor_company_name').text(company_name);
		$('.vendor_address').text(address);
		$('.vendor_district').text(district+'- '+pincode);
		$('.vendor_state').text(state);
		$('.vendor_country').text(country);
	})
	$(document).on('change','.product_sel',function(){
		// $('#jox tr:nth-child(1)').removeClass('trundefined');
		var index = $('.product_sel').length-1;
		var cls = $('.product_sel:eq('+index+') option:selected').attr('class');
		var vv = $('.product_sel:eq('+index+') option:selected').val();
		// alert($(this).closest('.prd').attr('class'));

		$(this).closest('tr').find('.prd').val(vv)

		var old_cls = $(this).parent().parent().next().next().attr("class");
		var price = $('.product_sel:eq('+index+') option:selected').attr('data_price');

		var val = $('.product_sel:eq('+index+') option:selected').val();
		sel_ids[index] = val;

		if(old_cls != null){
			$(this).parent().parent().next().next().removeClass(old_cls);
			$(this).parent().parent().next().next().addClass(cls);

				var root_cls = $(this).parent().parent().parent().attr('class');
				if(root_cls != null){
					$(this).parent().parent().parent().removeClass(root_cls);
					$(this).parent().parent().parent().addClass('tr'+cls);
					$('.'+cls+' span.input.input--nao').addClass('input--filled');
					$('td.'+cls+' .prd_sel_qty').val(price)


					var qq = $('.tr'+cls+' td:nth-child(2)').find('.prd_qty').val();
					if(qq != null){
						$('.tr'+cls+' td:nth-child(4)').find('#product_add_product_quantity').val(parseInt(price)*parseInt(qq));
					}


				}else{
					$(this).parent().parent().parent().addClass('tr'+cls);
					$('.'+cls+' span.input.input--nao').addClass('input--filled');
					$('td.'+cls+' .prd_sel_qty').val($('.product_sel:eq('+index+') option:selected').attr('data_price'))

					var qq = $('.tr'+cls+' td:nth-child(2)').find('.prd_qty').val();
					if(qq != null){
						$('.tr'+cls+' td:nth-child(4)').find('#product_add_product_quantity').val(parseInt(price)*parseInt(qq));
					}
				}
		}else{
			$(this).parent().parent().next().next().addClass(cls);
			$(this).parent().parent().parent().addClass('tr'+cls);
			$('.'+cls+' span.input.input--nao').addClass('input--filled');
			$('td.'+cls+' .prd_sel_qty').val($('.product_sel:eq('+index+') option:selected').attr('data_price'))

			var qq = $('.tr'+cls+' td:nth-child(2)').find('.prd_qty').val();
			if(qq != null){
				$('.tr'+cls+' td:nth-child(4)').find('#product_add_product_quantity').val(parseInt(price)*parseInt(qq));
			}
		}
		tot=0;
   		
   		for(i=0;i<tot_len;i++){
   			if($('.prd_prc_rw:eq('+i+')').val() != ''){
   			tot += parseInt($('.prd_prc_rw:eq('+i+')').val());
   			}
   		}
			$('.tot_price').text(tot);
			$('.tot_price').val(tot);
			$('.tax_price').text(tot*10/100);
			$('.g_tot_price').text(tot+tot*10/100);
	})
	$(document).on('keyup','.prd_qty',function(){
		var qty = $(this).val();
		var rt_cls = $(this).parent().parent().parent().attr('class');
		var p_value = $(this).parent().parent().next().find('.prd_sel_qty').val();
		$('.'+rt_cls+' #product_add_product_quantity').parent().addClass('input--filled');
		$('.'+rt_cls+' #product_add_product_quantity').val(parseInt(p_value)*parseInt(qty));

		tot_len = $('.prd_prc_rw').length;
   		tot=0;
   		
   		for(i=0;i<tot_len;i++){
   			if($('.prd_prc_rw:eq('+i+')').val() != ''){
   			tot += parseInt($('.prd_prc_rw:eq('+i+')').val());
   			}
   		}
			$('.tot_price').text(tot);
			$('.tax_price').text(tot*10/100);
			$('.g_tot_price').text(tot+tot*10/100);
	})
</script>
<script>
	$("#pp_add").click(function(){
		ll = $('.product_sel').length-1;
		var aa = $('.product_sel:eq('+ll+')').val()
		var bb = $('.prd_qty:eq('+ll+')').val()
		var cc = $('.prd_sel_qty:eq('+ll+')').val()
		var dd = $('.prd_prc_rw:eq('+ll+')').val()
		if(aa != '' &&bb != '' &&cc != '' &&dd != ''){

			tot_len = $('.prd_prc_rw').length;
	   		tot=0;
	   		
	   		for(i=0;i<tot_len;i++){
	   			tot += parseInt($('.prd_prc_rw:eq('+i+')').val());
	   		}
   			$('.tot_price').text(tot);
   			$('.tax_price').text(tot*10/100);
   			$('.g_tot_price').text(tot+tot*10/100);

			$('.product_sel').attr('disabled',true);
		    $("#jox").append('<tr><td>	<span class="input input--nao product_sel_sel">		<select id="" class="input__field--nao product_sel" name="product_id[]">			<option  class="xx"></option>		</select>	</span>	<?php echo form_error('product_id','<p class="error">', '</p>'); ?><input type="hidden" name="prd[]" class="prd"></td><td>	<span class="input input--nao">		<input class="input__field input__field--nao prd_qty" type="number" name="quantity[]" />		<label class="input__label input__label--nao" for="product_add_product_quantity">			<span class="input__label-content input__label-content--nao">Quantity</span>		</label>		<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none"><path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></svg>	</span>	<?php echo form_error('quantity','<p class="error">', '</p>'); ?></td><td>	<span class="input input--nao">		<input class="input__field input__field--nao prd_sel_qty" type="number" name="pp_qty[]" />		<label class="input__label input__label--nao" for="product_add_product_quantity">		<span class="input__label-content input__label-content--nao">Price Per Quality</span>		</label>		<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">		<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></svg>	</span></td><td>	<span class="input input--nao">		<input class="input__field input__field--nao prd_prc_rw" type="number" id="product_add_product_quantity" name="tot_price[]" />		<label class="input__label input__label--nao" for="product_add_product_quantity">		<span class="input__label-content input__label-content--nao">Total Price</span>		</label>		<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">		<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></svg>	</span></td><td>	<button type="button" name="moduleadd_submit" class="country_button mr-2 pp_delete" >DELETE <i class="fa fa-trash-o"></i></button></td> </tr>');
		    $(".product_sel").select2({
		  		placeholder: "Select Product",
		  	});

			for(i=0;i<a.length;i++){
				if(sel_ids.indexOf(a[i]) == -1){
		   		$('.xx').after('<option value="'+a[i]+'" class="'+a[i]+'_'+b[i].replace(/\s/g, '')+'" data_price="'+c[i]+'"> '+b[i]+'</option>');
		   		}
	   		} 
   		} 
	});
	$(document).on('click',".pp_delete",function(){
		$(this).closest('tr').remove();
		var cls = $(this).closest('tr').attr('class');
		var res = cls.split("_");
		if(res != null){
			xx = res[0].slice(2);
			ind = sel_ids.indexOf(xx);
			if (ind > -1) {
			    sel_ids.splice(ind, 1);
			    console.log(sel_ids);
			}

				tot_len = $('.prd_prc_rw').length;
		   		tot=0;
		   		
		   		for(i=0;i<tot_len;i++){
		   			if($('.prd_prc_rw:eq('+i+')').val() != ''){
		   			tot += parseInt($('.prd_prc_rw:eq('+i+')').val());
		   			}
		   		}
	   			$('.tot_price').text(tot);
	   			$('.tax_price').text(tot*10/100);
	   			$('.g_tot_price').text(tot+tot*10/100);
		}
	});
</script>
<?php if(set_value('vendor_id') != null){ ?>
	<script type="text/javascript">
		$(".vendor").select2({ placeholder: "Select Vendor"});
		$('.vendor').val(<?php echo set_value('vendor_id'); ?>).trigger('change');
		$('.vendor').val(<?php echo set_value('vendor_id'); ?>).trigger('change');
	</script>
<?php } ?>
<script>
  	$( function() {
    	$( "#gj_datepicker" ).datepicker({
     		changeMonth: true,
      		changeYear: true,
      		dateFormat: 'dd-mm-yy',
    	});
  	} );
</script>
<style type="text/css">
	.page-content section.content.bgcolor-1 form span.select2.select2-container.select2-container--default.select2-container--focus, span.select2.select2-container.select2-container--default.select2-container--below, span.select2.select2-container.select2-container--default {
	    width: 90% !important;
	    margin: 0px auto !important;
	    text-align: left;
	}
	.page-content section.content.bgcolor-1 form .product_sel_sel span.select2.select2-container.select2-container--default.select2-container--focus, .product_sel_sel span.select2.select2-container.select2-container--default.select2-container--below, .product_sel_sel span.select2.select2-container.select2-container--default {
	    width: 100% !important;
	    margin: 0px auto !important;
	    text-align: left;
	}
	.product_sel_sel span.select2-selection.select2-selection--single{
		margin-top: 17px;
	}
	.product_sel_sel .select2-container--default .select2-selection--single .select2-selection__arrow b {
	    border-color: #888 transparent transparent transparent;
	    border-style: solid;
	    border-width: 5px 4px 0 4px;
	    height: 0;
	    left: 50%;
	    margin-left: -4px;
	    margin-top: 2px;
	    position: absolute;
	    top: 100%;
	    width: 0;
	}
</style>