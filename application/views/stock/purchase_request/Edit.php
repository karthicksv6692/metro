<?php if($purchase_req_obj != null){
	if($purchase_req_obj != null){
		$id = $purchase_req_obj->getPurchaseRequestId();
		$bill_no = $purchase_req_obj->getPurchaseRequestNo();
		$status = $purchase_req_obj->getPurchaseRequestStatus();

		$p_r_vendor_id = $purchase_req_obj->getPurchaseRequestVendorId()->getVendorId();
		$p_r_vendor_name = $purchase_req_obj->getPurchaseRequestVendorId()->getVendorName();
		$p_r_vendor_full_address = $purchase_req_obj->getPurchaseRequestVendorId()->getVendorAddressId()->get_Address_full_address();
		$p_r_vendor_district = $purchase_req_obj->getPurchaseRequestVendorId()->getVendorAddressId()->get_Address_pincode_id()->getPincodeDistrict();
		$p_r_vendor_pincode = $purchase_req_obj->getPurchaseRequestVendorId()->getVendorAddressId()->get_Address_pincode_id()->getPincodeNo();
		$p_r_vendor_state = $purchase_req_obj->getPurchaseRequestVendorId()->getVendorAddressId()->get_Address_pincode_id()->getPincodeState();
		$p_r_vendor_country = $purchase_req_obj->getPurchaseRequestVendorId()->getVendorAddressId()->get_Address_pincode_id()->getPincodeCountryId()->getCountryName();
	}else{
		$id = "";$bill_no = "";$p_r_vendor_id = "";$p_r_vendor_name = "";$p_r_vendor_full_address = "";$p_r_vendor_district = "";$p_r_vendor_pincode = "";$p_r_vendor_state = "";$p_r_vendor_country = "";
	}
?>
	<div class="dkbody">
		<div class="container-fluid">
			<div class="page-header gj_new_pack">
				<h3>New Purchase Request - Response</h3>
			</div>
			<p class="gj_package_dets"><span><i class="fa fa-dropbox" aria-hidden="true"></i></span> Purchase Request Details</p>
			<div class="page-content gj_pack_all">
				<section class="gj_pack_sec">
					<form method="post" action="<?php echo base_url('index.php/stock/Purchase_request_crud/edit'); ?>">
						<div class="row rr">
							<div class="col-lg-6 col-md-6 pull-left">
								<div class="gj_pur_bill">
									<p class="gj_purse_bill gj_pr_bill">Bill Number<span class="gj_bill_sep">:</span><span class="gj_bill_no"><?php echo $bill_no; ?></span></p>
						        </div>
							</div>
							<div class="col-lg-6 col-md-6 pull-left">
								<div class="col-lg-6 col-md-8 row pull-right pr-0">
									<span class="input input--nao">
										<select class="input__field--nao gj_pro_select gj_pp_sl" name="status">
								            <option value="0" <?php if($status == 0){echo"selected='selected'";} ?>>Requested</option>
								            <option value="1" <?php if($status == 1){echo"selected='selected'";} ?>>Approved</option>
								            <option value="2" <?php if($status == 2){echo"selected='selected'";} ?>>Rejected</option>
								        </select>
							        </span>
								</div>
							</div>
						</div>
						<div class="row rr">
							<div class="col-lg-4 col-md-6 pull-left">
								<div class="col-lg-4 col-md-6 pull-left pr-0 pl-0">
									<div class="gj_pur">
										<p class="gj_pur_hd ven_name_min_wd">Vendor Name :</p>
									</div>
								</div>
								<div class="col-lg-8 col-md-6 pull-left pr-0 pl-0">
									<div class="gj_pur">
										<p class="gj_pur_data pad_left_10px"><?php echo $p_r_vendor_name; ?></p>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-6 row pull-left">
								<div class="col-lg-4 col-md-5 pull-left pr-0 pl-0">
									<div class="gj_pur">
										<p class="gj_pur_hd">Shop Name :</p>
									</div>
								</div>
								<div class="col-lg-8 col-md-7 pull-left pr-0 pl-0">
									<div class="gj_pur">
										<p class="gj_pur_data"><?php echo $p_r_vendor_name; ?></p>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-6 row pull-left gj_add_mar">
								<div class="col-lg-3 col-md-4 pull-left pr-0 pl-0">
									<div class="gj_pur">
										<p class="gj_pur_hd">Address :</p>
									</div>
								</div>
								<div class="col-lg-9 col-md-8 pull-left pr-0 pl-0">
									<div class="gj_pur">
										<p class="gj_pur_data"><?php echo $p_r_vendor_full_address; ?></p>
										<p class="gj_pur_data"><?php echo $p_r_vendor_district; ?> - <?php echo $p_r_vendor_pincode; ?></p>
										<p class="gj_pur_data"><?php echo $p_r_vendor_state; ?></p>
										<p class="gj_pur_data"><?php echo $p_r_vendor_country; ?></p>
									</div>
								</div>
							</div>
						</div>

						<hr>
						<div class="row rr">
							<div class="col-lg-12">
								<div class="gj_apnd_but pull-right">
									<!-- <button type="button" name="moduleadd_submit" id="pp_add" class="country_button mr-2 gj_pur_but" >ADD <i class="fa fa-plus"></i></button> -->
								</div>
							</div>
						</div>

					<div class="gj_purchase_table">
						<div class="gj_pur_bill_table">
							<div class="gj_bill_table table-responsive">         
	  							<table class="table table-hover table-bordered table-striped">
								    <thead>
								      	<tr>
									        <th class="text-center">Product Name</th>
									        <th class="text-center">Quantity</th>
									        <th class="text-center">Price Per Quality</th>
									        <th class="text-center">Total Price</th>
									        <th class="text-center">Action</th>
								      	</tr>
								    </thead>
								    <tbody id="jo">
								      	
								      	<?php if($product_obj != null){ ?>
											<tbody id="jox">
											<?php foreach($product_obj as $obj){ ?>
												<tr class="<?php echo "tr".$obj->getPurchaseRequestProductProductId()->getProductId()."_".str_replace(' ', '', $obj->getPurchaseRequestProductProductId()->getProductName()); ?>">
												
													<td>
														<span class="input input--nao">
															<input class="input__field input__field--nao" type="text" name="product_id" value="<?php echo $obj->getPurchaseRequestProductProductId()->getProductName(); ?>" data_price="<?php echo $obj->getPurchaseRequestProductProductId()->getProductPrice(); ?>" disabled />
															<label class="input__label input__label--nao" for="product_add_product_quantity">
																<span class="input__label-content input__label-content--nao">Product Name</span>
															</label>
															<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none"><path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></svg>
															<input  type="hidden" name="prd[]" value="<?php echo $obj->getPurchaseRequestProductProductId()->getProductId(); ?>"/>
														</span>
														
													</td>
												

													<td>
														<span class="input input--nao">
															<input class="input__field input__field--nao prd_qty" type="number" name="quantity[]" value="<?php echo $obj->getPurchaseRequestProductProductQuantity(); ?>" />
															<label class="input__label input__label--nao" for="product_add_product_quantity">
																<span class="input__label-content input__label-content--nao">Quantity</span>
															</label>
															<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none"><path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></svg>
														</span>
														<?php echo form_error('quantity','<p class="error">', '</p>'); ?>
													</td>
													<td>
														<span class="input input--nao">
															<input class="input__field input__field--nao prd_sel_qty" type="number" name="pp_qty[]" value="<?php echo $obj->getPpqPrice(); ?>"/>
															<label class="input__label input__label--nao" for="product_add_product_quantity">
															<span class="input__label-content input__label-content--nao">Price Per Quality</span>
															</label>
															<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
															<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></svg>
														</span>
													</td>
													<td>
														<span class="input input--nao">
															<input class="input__field input__field--nao prd_prc_rw" type="number" id="product_add_product_quantity" name="tot_price[]"  value="<?php echo $obj->getPpqPrice()*$obj->getPurchaseRequestProductProductQuantity(); ?>"/>
															<label class="input__label input__label--nao" for="product_add_product_quantity">
															<span class="input__label-content input__label-content--nao">Total Price</span>
															</label>
															<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
															<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/></svg>
														</span>
													</td>
													<td>
														<button type="button" name="moduleadd_submit" class="country_button mr-2 pp_delete" >DELETE <i class="fa fa-trash-o"></i></button>
													</td>
												</tr>
											<?php }?>
											</tbody>
										<?php } ?>
								    </tbody>
								</table>
							</div>
						</div>

						<div class="gj_total_bill_table">
							<div class="gj_total_bill table-responsive">         
	  							<table class="table table-hover table-bordered table-striped">
								    <tbody>
								      	<tr>
									        <td class="gj_spe_td"></td>
									        <td class="gj_spce_td">Total</td>
									        <td class="tot_price"></td>
								      	</tr>
								      	<tr>
									        <td class="gj_spe_td"></td>
									        <td class="gj_spce_td">Tax</td>
									        <td class="tax_price"></td>
								      	</tr>
								      	<tr>
									        <td class="gj_spe_td"></td>
									        <td class="gj_spce_td">Grand Total</td>
									        <td class="g_tot_price"></td>
								      	</tr>
								    </tbody>
								</table>
							</div>
						</div>
					</div>

						<div class="row rr">
							<div class="col-lg-4 col-md-5 pull-left pr-0 row">
								<span class="input input--nao">
									<input class="input__field input__field--nao " name="received_date" type="text"  id="gj_datepicker"
									 />
									<label class="input__label input__label--nao" for="gj_datepicker">
										<span class="input__label-content input__label-content--nao">Received Date</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
								</span>	
								<?php echo form_error('received_date','<p class="error">', '</p>'); ?>
							</div>
						</div>
						<input type="hidden" name="edit_purchase_request_id" value="<?php echo $id;  ?>">
				        <div class="country_width_100 col-12 mt-5">
							<div class="country_width_100">
								<div class="butt_sec_width mt-3 mb-3 gj_pur_butt">
							      	<button type="submit" name="moduleadd_submit" class="country_button mr-2 gj_pur_create" >Submit Bill <i class="fa fa-paper-plane" aria-hidden="true"></i></button>		
								    <button type="reset" name="moduleadd_reset" class="country_button">RESET <i class="fa fa-refresh" aria-hidden="true"></i></button>
						      	
								</div>
					      	</div>
					      	
					        <div class="country_width_100 mt-3 mb-3">					      		  
							    <div class="country-right">
							      	<a href="<?php echo base_url('index.php/stock/Purchase_request_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button></a>
							    </div>	
							</div>
						</div>
						
					</form>
				</section>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
		  	$("#gj_sel2").select2({
		  		placeholder: "Select Vendor Id",
		  	});
		  	tot_len = $('.prd_prc_rw').length;
	   		tot=0;
	   		
	   		for(i=0;i<tot_len;i++){
	   			if($('.prd_prc_rw:eq('+i+')').val() != ''){
	   			tot += parseInt($('.prd_prc_rw:eq('+i+')').val());
	   			}
	   		}
			$('.tot_price').text(tot);
			$('.tax_price').text(tot*10/100);
			$('.g_tot_price').text(tot+tot*10/100);
		});
		$("#gj_sel2").val('').trigger('change');

		$(document).on('blur','input',function(){
		  if($(this).val().length>=1){
		    $(this).parent().addClass('input--filled');
		  }
		});
		$(document).on('keyup','.prd_qty',function(){
			var qty = $(this).val();
			var rt_cls = $(this).parent().parent().parent().attr('class');

			var p_value = $(this).closest('tr').find('.prd_sel_qty').val();

			$('.'+rt_cls+' #product_add_product_quantity').parent().addClass('input--filled');
			$('.'+rt_cls+' #product_add_product_quantity').val(parseInt(p_value)*parseInt(qty));

			tot_len = $('.prd_prc_rw').length;
	   		tot=0;
	   		
	   		for(i=0;i<tot_len;i++){
	   			if($('.prd_prc_rw:eq('+i+')').val() != ''){
	   			tot += parseInt($('.prd_prc_rw:eq('+i+')').val());
	   			}
	   		}
				$('.tot_price').text(tot);
				$('.tax_price').text(tot*10/100);
				$('.g_tot_price').text(tot+tot*10/100);
		})
		$(document).on('keyup','.prd_sel_qty',function(){
			var qty = $(this).closest('tr').find('.prd_qty').val();
			
			var rt_cls = $(this).parent().parent().parent().attr('class');
			var p_value = $(this).val();

			$('.'+rt_cls+' #product_add_product_quantity').parent().addClass('input--filled');
			$('.'+rt_cls+' #product_add_product_quantity').val(parseInt(p_value)*parseInt(qty));
			tot_len = $('.prd_prc_rw').length;
			$(this).closest('tr').find('.prd_prc_rw').val()
	   		tot=0;
	   		for(i=0;i<tot_len;i++){
	   			if($('.prd_prc_rw:eq('+i+')').val() != ''){
	   			tot += parseInt($('.prd_prc_rw:eq('+i+')').val());
	   			}
	   		}
			$('.tot_price').text(tot);
			$('.tax_price').text(tot*10/100);
			$('.g_tot_price').text(tot+tot*10/100);
		})
		$(document).on('click',".pp_delete",function(){
			p_value = $(this).closest('tr').find('.prd_sel_qty').val();
			qty = $(this).closest('tr').find('.prd_qty').val();
			dec_val = parseInt(p_value)*parseInt(qty);
			$(this).closest('tr').remove();
			tot = parseInt($('.tot_price').text());

			new_val = parseInt(tot)-parseInt(dec_val);
			$('.tot_price').text(new_val);
			$('.tax_price').text(new_val*10/100);
			$('.g_tot_price').text(new_val+new_val*10/100);
		});
	</script>
	<script>
	  	$( function() {
	    	$( "#gj_datepicker" ).datepicker({
	     		changeMonth: true,
	      		changeYear: true,
	      		dateFormat: 'dd-mm-yy',
	    	});
	  	} );
	</script>
<?php } ?>