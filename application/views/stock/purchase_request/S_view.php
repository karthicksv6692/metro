<?php if($purchase_req_obj != null){
	if($purchase_req_obj != null){
		$id = $purchase_req_obj->getPurchaseRequestId();
		$bill_no = $purchase_req_obj->getPurchaseRequestNo();
		$status = $purchase_req_obj->getPurchaseRequestStatus();
		$received_date = $purchase_req_obj->getPurchaseRequestEstimatedDeliveryDate();
		$type = $purchase_req_obj->getPurchaseType();

		$p_r_vendor_id = $purchase_req_obj->getPurchaseRequestVendorId()->getVendorId();
		$p_r_vendor_name = $purchase_req_obj->getPurchaseRequestVendorId()->getVendorName();
		$p_r_vendor_full_address = $purchase_req_obj->getPurchaseRequestVendorId()->getVendorAddressId()->get_Address_full_address();
		$p_r_vendor_district = $purchase_req_obj->getPurchaseRequestVendorId()->getVendorAddressId()->get_Address_pincode_id()->getPincodeDistrict();
		$p_r_vendor_pincode = $purchase_req_obj->getPurchaseRequestVendorId()->getVendorAddressId()->get_Address_pincode_id()->getPincodeNo();
		$p_r_vendor_state = $purchase_req_obj->getPurchaseRequestVendorId()->getVendorAddressId()->get_Address_pincode_id()->getPincodeState();
		$p_r_vendor_country = $purchase_req_obj->getPurchaseRequestVendorId()->getVendorAddressId()->get_Address_pincode_id()->getPincodeCountryId()->getCountryName();
	}else{
		$id = "";$bill_no = "";$p_r_vendor_id = "";$p_r_vendor_name = "";$p_r_vendor_full_address = "";$p_r_vendor_district = "";$p_r_vendor_pincode = "";$p_r_vendor_state = "";$p_r_vendor_country = "";
	}
?>
	<div class="dkbody">
		<div class="container-fluid">
			<div class="page-header gj_new_pack">
				<h3>Purchase Request View</h3>
			</div>
			<p class="gj_package_dets"><span><i class="fa fa-dropbox" aria-hidden="true"></i></span> Purchase Request View Details</p>
			<div class="page-content gj_pack_all">
				<section class="gj_pack_sec">
					<form method="post" action="<?php echo base_url('index.php/stock/Purchase_request_crud/edit'); ?>">
						<div class="row rr">
							<div class="col-lg-12 col-md-12 pull-left">
								<div class="gj_pur_bill">
									<p class="gj_purse_bill">Bill Number<span class="gj_bill_sep">:</span><span class="gj_bill_no"><?php echo $bill_no; ?></span></p>
						        </div>
							</div>
						</div>
						<div class="row rr gj_bot_sty">
							<div class="col-lg-4 col-md-6 pull-left">
								<div class="col-lg-4 col-md-5 pull-left pr-0 pl-0">
									<div class="gj_pur">
										<p class="gj_pur_hd">Vendor Id :</p>
									</div>
								</div>
								<div class="col-lg-8 col-md-7 pull-left pr-0 pl-0">
									<div class="gj_pur">
										<p class="gj_pur_data"><?php echo $p_r_vendor_name; ?></p>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-6 row pull-left">
								<div class="col-lg-4 col-md-5 pull-left pr-0 pl-0">
									<div class="gj_pur">
										<p class="gj_pur_hd">Shop Name :</p>
									</div>
								</div>
								<div class="col-lg-8 col-md-7 pull-left pr-0 pl-0">
									<div class="gj_pur">
										<p class="gj_pur_data"><?php echo $p_r_vendor_name; ?></p>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-6 row pull-left gj_add_mar">
								<div class="col-lg-3 col-md-4 pull-left pr-0 pl-0">
									<div class="gj_pur">
										<p class="gj_pur_hd">Address :</p>
									</div>
								</div>
								<div class="col-lg-9 col-md-8 pull-left pr-0 pl-0">
									<div class="gj_pur">
										<p class="gj_pur_data"><?php echo $p_r_vendor_full_address; ?></p>
										<p class="gj_pur_data"><?php echo $p_r_vendor_district; ?> - <?php echo $p_r_vendor_pincode; ?></p>
										<p class="gj_pur_data"><?php echo $p_r_vendor_state; ?></p>
										<p class="gj_pur_data"><?php echo $p_r_vendor_country; ?></p>
									</div>
								</div>
							</div>
						</div>

					<div class="gj_purchase_table">
						<div class="gj_pur_bill_table">
							<div class="gj_bill_table table-responsive">         
	  							<table class="table table-hover table-bordered table-striped">
								    <thead>
								      	<tr>
									        <th class="text-center">Product Name</th>
									        <th class="text-center">Quantity</th>
									        <th class="text-center">Price Per Quality</th>
									        <th class="text-center">Total Price</th>
								      	</tr>
								    </thead>
								    <?php if($product_obj != null){ ?>
									    <tbody id="jo">
									      	<?php foreach($product_obj as $obj){ ?>
												<tr>
												    <td><?php echo $obj->getPurchaseRequestProductProductId()->getProductName(); ?></td>
												    <td><?php echo $obj->getPurchaseRequestProductProductQuantity(); ?></td>
												    <td><?php echo $obj->getPpqPrice(); ?></td>
												    <td><?php echo $obj->getPpqPrice()*$obj->getPurchaseRequestProductProductQuantity(); ?></td>
												    <input type="hidden" class="prc" value="<?php echo $obj->getPpqPrice()*$obj->getPurchaseRequestProductProductQuantity(); ?>">
												</tr>
											<?php } ?>
									    </tbody>
								    <?php } ?>
								</table>
							</div>
						</div>

						<div class="gj_total_bill_table">
							<div class="gj_total_bill table-responsive">         
	  							<table class="table table-hover table-bordered table-striped">
								    <tbody>
								      	<tr>
									        <td class="gj_spe_td"></td>
									        <td class="gj_spce_td">Total</td>
									        <td class="tot_price"></td>
								      	</tr>
								      	<tr>
									        <td class="gj_spe_td"></td>
									        <td class="gj_spce_td">Tax</td>
									        <td class="tax_price"></td>
								      	</tr>
								      	<tr>
									        <td class="gj_spe_td"></td>
									        <td class="gj_spce_td">Grand Total</td>
									        <td class="g_tot_price"></td>
								      	</tr>
								    </tbody>
								</table>
							</div>
						</div>
					</div>

						<div class="row rr">
							<div class="col-lg-4 col-md-5 pull-left">
								<div class="col-lg-4 col-md-5 pull-left pr-0 pl-0">
									<div class="gj_pur">
										<p class="gj_pur_hd">Received Date :</p>
									</div>
								</div>
								<div class="col-lg-8 col-md-7 pull-left pr-0 pl-0">
									<div class="gj_pur">
										<p class="gj_pur_data"><?php echo print_r($received_date->format('Y-m-d')); ?></p>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-5 pull-left">
								<div class="col-lg-4 col-md-5 pull-left pr-0 pl-0">
									<div class="gj_pur">
										<p class="gj_pur_hd">Purchase Type :</p>
									</div>
								</div>
								<div class="col-lg-8 col-md-7 pull-left pr-0 pl-0">
									<div class="gj_pur">
										<p class="gj_pur_data"><?php if($type == 0){echo "Requested";}else{echo "Billed";} ?></p>
									</div>
								</div>
							</div>
						</div>
						<input type="hidden" value="<?php echo $id; ?>" name="purchase_request_id">
				        <div class="country_width_100 col-12 mt-5">
							<div class="country_width_100">
								<div class="butt_sec_width mt-3 mb-3">
							      	<button type="submit" name="moduleadd_submit" class="country_button mr-2" >EDIT <i class="fa fa-pencil" aria-hidden="true"></i></button>		
								    <button type="reset" name="moduleadd_reset" class="country_button">RESET <i class="fa fa-refresh" aria-hidden="true"></i></button>
						      	
								</div>
					      	</div>
					      	
					        <div class="country_width_100 mt-3 mb-3">					      		  
							    <div class="country-right">
							      <a href="<?php echo base_url('index.php/stock/Purchase_request_crud'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button></a>
							    </div>	
							</div>
						</div>
						
					</form>
				</section>
			</div>
		</div>
	</div>

	<style type="text/css">
		.gj_bot_sty {
		    margin-bottom: 15px;
		}
		.gj_bill_table .table-bordered td {
		    text-align: center;
		}
	</style>
	<script type="text/javascript">
		tot_len = $('.prc').length;
   		tot=0;
   		
   		for(i=0;i<tot_len;i++){
   			if($('.prc:eq('+i+')').val() != ''){
   			tot += parseInt($('.prc:eq('+i+')').val());
   			}
   		}
		$('.tot_price').text(tot);
		$('.tax_price').text(tot*10/100);
		$('.g_tot_price').text(tot+tot*10/100);
	</script>

<?php } ?>