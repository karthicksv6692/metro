<div class="dkbody">
  <div class="container-fluid">
    <div class="col-md-12">
      <?php if($_SESSION['role_id'] == RT::$default_stock_pointer_id){ ?>
        <a href="<?php echo base_url('index.php/shopping/shop_pin_requests'); ?>" target="new">
        <div class="col-md-3" style="margin-bottom: 2em">
          <div class="s_box1">
            <h3 class="shp">Shopping Pin Requests</h3>
          </div>
        </div>
      </a>
      <?php } ?>

      <a href="<?php echo base_url('index.php/pin/pin_store/used'); ?>" target="new">
        <div class="col-md-2">
          <div class="s_box">
            <h3 class="usd">Used Pins</h3>
          </div>
        </div>
      </a>

      <a href="<?php echo base_url('index.php/pin/pin_store/un_used'); ?>" target="new">
        <div class="col-md-2">
          <div class="s_box">
            <h3 class="unusd">Un Used Pins</h3>
          </div>
        </div>
      </a>

      <a href="<?php echo base_url('index.php/pin/pin_store/pin_transfer'); ?>" target="new">
        <div class="col-md-2">
          <div class="s_box">
            <h3 class="pntr">Pin Transfer</h3>
          </div>
        </div>
      </a>

      <a href="<?php echo base_url('index.php/pin/pin_store/pin_transfer_history'); ?>" target="new">
        <div class="col-md-3" style="margin-bottom: 2em">
          <div class="s_box1">
            <h3 class="pntrh">Pin Transfer History</h3>
          </div>
        </div>
      </a>
      <style type="text/css">
        .s_box {
            background: #2bb1e9;
            text-align: center;
            height: 45px;
        }
        .s_box1 {
            background: #2bb1e9;
            text-align: center;
            height: 45px;
        }
        .s_box h3{
            padding-top: 9%;
            font-size: 19px !important;
            border: 1px solid #2bb1e9;
            background: #fff;
            color: #2bb1e9;
            padding-bottom: 8%;
        }
        .s_box1 h3{
          padding-top: 6%;
          font-size: 19px !important;
          border: 1px solid #2bb1e9;
          background: #fff;
          color: #2bb1e9;
          padding-bottom: 4%;
        }
        .s_box1.active h3,.s_box.active h3,.s_box1 h3:hover,.s_box h3:hover{
          background: #2bb1e9;
          color: #fff;
        }
        h3.shp{
          background: #2bb1e9;
          color: #fff;
        }
      </style>
    </div>
    
    <div class="rr">
        <div class="col-md-3 pull-left form-group">
          <span class="input input--nao" id="sandbox-container">
            <input type="text" class="input__field input__field--nao form-control dtt1"  name="dob" value="<?php echo date('Y/m/d') ?>" 
             data-validation="date" />
             <!-- data-validation-format="yyyy-mm-dd" -->
            <label class="input__label input__label--nao" for="input-1">
              <span class="input__label-content input__label-content--nao">From</span>
            </label>
            <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
              <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
            </svg>
          </span> 
        </div>
        <div class="col-md-3 pull-left form-group">
          <span class="input input--nao" id="sandbox-container">
            <input type="text" class="input__field input__field--nao form-control dtt2"  name="dob" value="" 
             data-validation="date" />
             <!-- data-validation-format="yyyy-mm-dd" -->
            <label class="input__label input__label--nao" for="input-1">
              <span class="input__label-content input__label-content--nao">To</span>
            </label>
            <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
              <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
            </svg>
          </span> 
        </div>

        <button type="button" class="btn btn_row1  btn-7 btn-7a icon-truck mb-5" id="date-search"> <i class="fa fa-search" aria-hidden="true"></i></button>

        <div class="col-md-3 pull-right form-group">
          <button type="button" class="btn btn_row1  btn-7 btn-7a icon-truck mb-5" id="all-search"> All Requests</button>
        </div>
    </div>



    <div class="page-content">                   
        <div class="container-fluid">
          <table  id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="10" width="100%">
            <thead>
                
                <tr>
                  <th class="text-center">#</th>
                  <th class="text-center">User Id</th>
                  <th class="text-center">User Name</th>
                  <th class="text-center">Name</th>
                  <th class="text-center">Package</th>
                  <th class="text-center">Type</th>
                  <th class="text-center">Status</th>
                  <th class="text-center">Created</th>
                  <th class="text-center">Payment</th>
                  <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
        <div class="per_rw btn-click pr-5">
            <a href="<?php echo base_url('index.php/pin/pin_store/pin_store_viewx'); ?>"><button type="button" class="btn btn_row1  btn-7 btn-7a icon-truck mb-5" id="row1"> <span>BACK</span></button></a>
        </div>
         <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Request Details</h4>
                </div>
                <div class="modal-body">
                  
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default dismiss" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
          <button type="button" class="btn btn-info btn-lg mdl" data-toggle="modal" data-target="#myModal" style="visibility: hidden;"></button>
      </div>
  </div>
  
</div>
<style type="text/css">
  .btn_row1 {
      background-color: #0290cc;
      color: white;
      border-radius: 0;
      padding: 9px 17px 14px 18px;
      font-size: 1.9rem;
      margin: 2rem 0rem 0rem 0rem;
  }
  span.fai{
    color: red;
  }
  .modal-header {
      background-color: #28bfff;
      color: #fff;
      text-transform: uppercase;
  }
  input.cus_but {
    background: #0d93cc;
    color: #fff;
    border: 0;
    padding: 10px 20px;
    font-size: 16px;
  }
  span.war {
    color: #9f04a9;
  }
  @media (min-width: 768px){
    .modal-dialog {
        width: 600px;
        margin: 30px auto;
        min-width: 950px;
        min-height: 200px;
        margin-top: 9%;
    }
  }
</style>
<script type="text/javascript">
  $(document).ready(function() {
    var tab = "";
    tab =$('#gj_tree_struct_vw').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": '<?php echo base_url(); ?>index.php/shopping/datatable_pin_requests',
         "order": [[ 7, "desc" ]],
        "pagingType": "full_numbers"
    });
    document.querySelector("#date-search").addEventListener('click',function(){
      dt1 = $('.dtt1').val().replace('/','-');
      dt2 = $('.dtt2').val().replace('/','-');
      if(dt1.length>0 || dt2.length>0 ){
        tab.destroy();
        tab = $('#gj_tree_struct_vw').DataTable({
          "processing": true,
          "serverSide": true,
          "ajax": {
            "url": '<?php echo base_url(); ?>index.php/shopping/datatable_pin_requests',
             "data": {'dt1':dt1,'dt2':dt2},
          },
        });
      }
    })
    document.querySelector("#all-search").addEventListener('click',function(){
        tab.destroy();
        tab = $('#gj_tree_struct_vw').DataTable({
          "processing": true,
          "serverSide": true,
          "ajax": {
            "url": '<?php echo base_url(); ?>index.php/shopping/datatable_pin_requests',
             "data": {'all':1},
          },
        });
    })
});

function get_req_details(id){
  $('.modal-body').html('');
  $('.mdl').click();
  $.ajax({
    type : 'post',
    url  : "<?php echo base_url('index.php/common_controller/get_request_details'); ?>",
    data : {'request_id':id},
    success: function(data){
      var res = JSON.parse(data);
      $('.modal-body').html(res);
    }
  })
}
function up_sts(id){
  status = $('.sel_val').val();
  $.ajax({
    type : 'post',
    url  : "<?php echo base_url('index.php/common_controller/update_shop_status'); ?>",
    data : {'id':id,'status':status},
    success: function(data){
      if(data == 1){
        setTimeout(function(){ 
          $('.dismiss').click();
          toast({
            message: "Updated Successfully",
            displayLength: 2000,
            className: 'success',
          });
          location.reload();
        }, 1000);
      }else{
        toast({
          message: "Updated Failed",
          displayLength: 2000,
          className: 'error',
        });
      }
    }
  })
}
function gen_and_send(id){
  $.ajax({
    type : 'post',
    url  : "<?php echo base_url('index.php/common_controller/send_pin_to_user'); ?>",
    data : {'id':id},
    success: function(data){
      if(data == 1){
        setTimeout(function(){ 
          $('.dismiss').click();
          toast({
            message: "Pin Transfered Successfully",
            displayLength: 2000,
            className: 'success',
          });
          location.reload();
        }, 1000);
      }else{
        toast({
          message: "Pin Transfered Failed",
          displayLength: 2000,
          className: 'error',
        });
      }
    }
  })
}

</script>