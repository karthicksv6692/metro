<!-- products -->
  <section class="gj_s_p_sec"> 
    <div class="row gj_s_p_bg">
      <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 s_catagory_bg">
        <div class="s_catagory_menu">
          <div class="navbar-hear">
            <button type="button" class="navbar-toggle s_pro_catnmenu" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            
            <h3 class="s_catagory_head">CATAGORY</h3>
          </div>
          
          <!--<ul class=" s_catgory_ul">
            <li class="active"><a href="#">Catagory 1</a></li>
            <li><a href="#">Catagory 2</a></li>
            <li><a href="#">Catagory 3</a></li>
            <li><a href="#">Catagory 4</a></li>
            <li><a href="#">Catagory 5</a></li>
            <li><a href="#">Catagory 6</a></li>
            <li><a href="#">Catagory 7</a></li>
          </ul>-->
          <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
            <div class="accordion">
              <!-- <div class="accordion_head">Catagory1<span class="plusminus">+</span>
              </div>
              <div class="accordion_body" style="display: none;">
                <ul class="s_catgory_ul">
                  <li><a href="#">Catagory1.1</li>
                  <li><a href="#">Catagory1.2</a></li>
                  <li><a href="#">Catagory1.3</a></li>
                </ul>
              </div> -->

              <?php if($categories_list != null){ ?>
                    <a onclick="get_particular_products(-1)">
                      <div class="accordion_head">All
                        <!-- <span class="plusminus">+</span> -->
                      </div>
                    </a>
                <?php for($i=0;$i<count($categories_list);$i++){ ?>

                    <a onclick="get_particular_products(<?php echo $categories_list[$i]['cat_id']; ?>)">
                      <div class="accordion_head"><?php echo $categories_list[$i]['cat_name']; ?>
                        <!-- <span class="plusminus">+</span> -->
                      </div>
                    </a>
                <?php } ?>
                    
              <?php }else{echo"Category is Empty";} ?>
              
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 s_product_all">
        <!-- <div class="s-pro_detail"> -->
          <?php if(count($products) >0 ){ ?>
            <?php for($i=0;$i<count($products);$i++){ ?>
              <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 s_xs_width box wow bounceInDown center">
                <div class="s_product_item1">
                  <form method="post" action="<?php echo base_url('index.php/shopping/s_view'); ?>">
                          <input type="hidden" name="product_id" value="<?php echo $products[$i]['product_id']; ?>">
                    <a onclick="$(this).closest('form').submit();" title="View This Product">
                    <?php if(is_numeric($products[$i]['product_image_url'])){ ?>
                      <img src="<?php echo base_url('assets/images/no_image.jpg'); ?>" alt="product-image" class="img-responsive s_img_wid_height">
                    <?php }else{ ?>
                      <img src="<?php echo base_url('assets/images/'); ?><?php echo $products[$i]['product_image_url'];; ?>" alt="product-image" class="img-responsive s_img_wid_height">
                    <?php } ?>

                  <div class="s_pro_rate_bg">
                    <span class="s_product_rupee"><b>INR</b> <s style="color: red"><?php echo $products[$i]['product_price']; ?></s><b style="padding-left: 10px;"><?php echo $products[$i]['product_display_price']; ?></b></span>
                    <p class="s_product_offerrupee"><?php echo $products[$i]['point_value']; ?> PV</p>
                    <p class="s_product_name1"><?php echo $products[$i]['product_name']; ?></p>
                    <p class="s_product_shortdescription"><?php echo implode(',', $products[$i]['product_short_desc']); ?></p>
                  </div>
                  </a>
                </form>

                  <div>
                    <!-- <button type="button" class="s_productaddccart">Add to Cart</button> -->

                    <div class="gj_s_p_hd text-center">
                      <?php if(isset($_SESSION['cart_items'])){ ?>
                        <?php $ser = array_search($products[$i]['product_id'], array_column($_SESSION['cart_items'], 'product_id'));
                        if(!is_numeric($ser)){?>
                          <button class="gj_cart_but btn-success" id="bt_<?php echo $products[$i]['product_id']; ?>" type="button" onclick="common_add_remove(this,<?php echo $products[$i]['product_id']; ?>,1)"><i class="fa fa-check"></i> Add Cart</button>
                        <?php }else{ ?>
                          <button class="gj_cart_but btn-danger" id="bt_<?php echo $products[$i]['product_id']; ?>" type="button" onclick="common_add_remove(this,<?php echo $products[$i]['product_id']; ?>,1)"><i class="fa fa-close"></i> Remove</button>
                        <?php } ?>
                      <?php }else{ ?>
                        <button class="gj_cart_but btn-success" id="bt_<?php echo $products[$i]['product_id']; ?>" type="button" onclick="common_add_remove(this,<?php echo $products[$i]['product_id']; ?>,1)"><i class="fa fa-check"></i> Add Cart</button>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>
           
          <?php }else{ ?>
            <?php echo"Package is Empty"; ?>
          <?php } ?>
        <!-- </div> -->
      </div>
      <div class="row pull-right" style="width: 100%; text-align: right;">
          <a href="<?php echo base_url('index.php/shopping'); ?>" style="width: 100%;margin: 30px 0px;">
            <button type="button" name="back" class="country_button" style="float: ri"><i class="fa fa-arrow-left"></i> BACK</button>
          </a>
      </div>  
    </div>
  </section> 

  <style type="text/css">
    
.row.gj_s_p_bg {
    margin: 0px !important;
}
.s_catagory_bg {
    /* background-color: rgba(6, 136, 212, 0.93); */
  border-right: 1px solid rgba(128, 128, 128, 0.32);
  padding:0px !important;
  max-height: 500px;
    overflow-y: auto;
}
.s_product_item1 {
    padding: 8px;
    border: 1px solid rgb(238,238,238);
}
.s_pro_rate_bg {
    background-color: rgb(250,250,250);
}
.s_product_item1:hover {
    box-shadow: 0 0 20px 4px rgba(0,0,0,0.20);
}
.s_catgory_ul li {
    padding: 6px 6px 6px 16px;
  border-bottom: 1px solid rgba(128, 128, 128, 0.32);
}
.box:hover{
  box-shadow: 0px 2px 25px 0px #ddd !important;
}
.box:hover img{
  /*width: 230px !important;*/
  -webkit-animation-timing-function: ease-in;
}
ul.s_catgory_ul {
    padding: 0px;
    text-align: left;
  margin-bottom: 0px;
  
}
.s_pro_catnmenu .icon-bar {
    background-color: white;
}
.s_pro_catnmenu{  
    margin-right: 15px !important;
    background-color: #0688d4 !important;
}
.s_catgory_ul li a {
    text-decoration: none;
  /* color:white; */
}
.s_img_wid_height {
    max-width: 200px;
    max-height: 200px;
    margin: auto;
    margin-bottom: 20px;
}
.s_product_all {
    margin: 30px 0px;
}
.s_product_rupee {
    font-size: 20px;
}
.s_product_name1 {
    color: #067cce;
    font-size: 15px;
  max-height: 45px;
  overflow: hidden;
}
.s_product_shortdescription {
    font-size: 11px;
  max-height: 17px;
    overflow: hidden;
}
.s_productaddccart {
    width: 100%;
    background-color: #0688d4;
    border: none;
    color: white;
    margin-bottom: 10px;
    padding: 8px;
}
.s_catagory_head {
    text-align: center;
  width:100%;
}
/* menu catagory accordion */
.s_catagory_bg .accordion_container {
    width: 500px;
}
.s_catagory_bg .accordion_head {
    background-color:#0680d3;
    color: white;
    cursor: pointer;
    font-family: arial;
    font-size: 14px;
    margin: 0 0 1px 0;
    padding: 7px 11px;
    font-weight: bold;
}
.s_catagory_bg .accordion_body {
    background: rgb(250,250,250);
}
.s_catagory_bg .accordion_body p {
    padding: 18px 5px;
    margin: 0px;
}
.s_catagory_bg .plusminus {
    float:right;
}
@media (min-width:768px) and (max-width:991px){
  .s_product_item1 {
    margin-bottom: 20px;
  }
}

@media (max-width:767px){
  .s_catagory_menu {
    border-bottom: 1px solid rgba(128, 128, 128, 0.45);
  }
  .s_catagory_head {
    padding-left: 20px;
    text-align:left;
    width: 50%;
    float: left;
  }
  .s_catagory_bg div#bs-sidebar-navbar-collapse-1 {
    width: 100%;
  }
  .s_product_item1 {
    padding: 8px;
    border: 1px solid rgb(238,238,238);
    margin-bottom: 20px;
  }
}
@media (max-width:500px){
  .s_xs_width {
    width: 100% !important;
  }
}   
.btn-success,.btn-success:hover,.btn-success.focus, .btn-success:focus {
        color: #fff;
        background-color: #0680d3;
        border-color: #0680d3;
        width: 160px !important;
    }
  </style>
  <script>
    $("a[href='#top']").click(function() {
      $("html, body").animate({ scrollTop: 0 }, "slow");
      return false;
    });
  </script>
  <script>
    $(window).scroll(function() { 
      var scroll = $(window).scrollTop();
      if(scroll>=57){
        $(".mnu_cr").addClass("mnu_clr");
      }
      else{
        $(".mnu_cr").removeClass("mnu_clr");
      } 
    
    });
    
    // JavaScript Document
    $(document).ready(function () {
      //toggle the component with class accordion_body
      $(".accordion_head").click(function () {
        if ($('.accordion_body').is(':visible')) {
          $(".accordion_body").slideUp(300);
          $(".plusminus").text('+');
        }
        if ($(this).next(".accordion_body").is(':visible')) {
          $(this).next(".accordion_body").slideUp(300);
          $(this).children(".plusminus").text('+');
        } else {
          $(this).next(".accordion_body").slideDown(300);
          $(this).children(".plusminus").text('-');
        }
      });
    });

    function get_particular_products(id){
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>index.php/Common_controller/get_products_from_cat_id_user_choice",
        data: {'cat_id':id},
          success:  function (data) { 
            console.log(data);
            if(data != 0){
              var option = JSON.parse(data);
              $('.s_product_all').html('');
              $('.s_product_all').html(option);
            }else{
              toast({
                message: "Category is Empty",
                displayLength: 3000,
                className: 'error',
              });
            }
          }
      })
    }

  </script>
  
<style type="text/css">
  .s_product_item1{
    min-height: 315px !important;
  }
</style>