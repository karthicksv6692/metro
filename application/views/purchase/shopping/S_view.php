<!-- single view carousel Section Start -->
<?php //print_r($obj); ?>
	<section class="gj_sv_sec1">
		<div class="gj_sv_bg">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="gj_abo_hd_div">
							<p class="text-center dhinatechnologies_metrokings_headings pdt"> Our Products </p>
							<hr class="dhinatechnologies_metrokings_hr">
						</div>
					</div>
          
				</div>

				<div class="row">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="gj_sv_carousel">
							<div id="slider" class="flexslider">
					          	<ul class="slides">
                        <?php if(count($attach) > 0){ ?>
                          <?php foreach ($attach as $atc) { 
                            if(!is_numeric($atc->getAttachmentImageUrl())){
                          ?>
                              <li> <img src="<?php echo base_url($atc->getAttachmentImageUrl()); ?>" /></li>
                            <?php }else{ ?>
                              <li> <img src="<?php echo base_url('assets/images/no_image.jpg'); ?>" /></li>
                            <?php } ?>
                            
                          <?php } ?>
                        <?php }else{ ?>
					            	  <li> <img src="<?php echo base_url('assets/images/no_image.jpg'); ?>" /></li>
                        <?php } ?>
  					  	    		
					          	</ul>
					        </div>
							<div id="carousel" class="flexslider">
					          	<ul class="slides">
					            	<?php if(count($attach) > 0){ ?>
                          <?php foreach ($attach as $atc) { 
                            if(!is_numeric($atc->getAttachmentImageUrl())){
                          ?>
                              <li> <img src="<?php echo base_url($atc->getAttachmentImageUrl()); ?>" /></li>
                            <?php }else{ ?>
                              <li> <img src="<?php echo base_url('assets/images/no_image.jpg'); ?>" /></li>
                            <?php } ?>
                            
                          <?php } ?>
                        <?php }else{ ?>
                          <li> <img src="<?php echo base_url('assets/images/no_image.jpg'); ?>" /></li>
                        <?php } ?>
					  	    		
					          	</ul>
					        </div>
						</div>
					</div>
  
  <?php //echo"<pre>";print_r($obj);exit(); ?>

					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="gj_sv_description">
							<div class="gj_sv_pack1">
								<!-- <a href="#"><span class="fa fa-angle-left gj_s_l"></span></a>
								<a href="#"><span class="fa fa-angle-right gj_s_r"></span></a> -->
							</div>
							<hr class="gj_sv_hr1"/>
							<div class="gj_sv_pack2">
								<p class="gj_s_prod_hd"><?php echo $obj['product_obj']['product_name']; ?></p>
								<p class="gj_s_prod_cat"><?php echo $obj['package_obj']['package_name']; ?></p>
								<div class="gj_s_description">
									<p>
                    <ul>
                      <?php foreach ($obj['product_obj']['product_desc'] as $des){ ?>
                      <li><?php echo $des; ?></li>
                      <?php } ?>
                    </ul>
                  </p>
								</div>
								<div class="gj_s_price">
								 	<p class="gj_s_mrp">MRP</p>
								 	<p class="gj_s_mrp gj_s_mrp_line">Rs.<?php echo $obj['product_obj']['product_price']; ?></p>
								 	<p class="gj_s_dp">DP</p>
								 	<p class="gj_s_dp">Rs.<?php echo $obj['product_obj']['product_display_price']; ?></p>
								</div>
								<div class="gj_add_cart_div">
									<label for="quantity">Quantity</label>
									<input type="number" id="quantity" class="gj_quant" value="1" />
									<p class="gj_left_items"><?php echo $obj['product_obj']['product_quantity']; ?> items left</p>

									<!-- <button class="gj_cart_but btn-success" type="button"><i class="fa fa-check"></i> Add Cart</button> -->

									<?php $ser = array_search($obj['product_obj']['product_id'], array_column($_SESSION['cart_items'], 'product_id'));
										if(!is_numeric($ser)){
									 ?>
								  		<button class="gj_cart_but btn-success" id="bt_<?php echo $obj['product_obj']['product_id']; ?>" type="button" onclick="common_add_remove_from_s_view(this,<?php echo $obj['product_obj']['product_id']; ?>)"><i class="fa fa-check"></i> Add Cart</button>
									<?php }else{ ?>
								  		<button class="gj_cart_but btn-danger" id="bt_<?php echo $obj['product_obj']['product_id']; ?>" type="button" onclick="common_add_remove_from_s_view(this,<?php echo $obj['product_obj']['product_id']; ?>)"><i class="fa fa-close"></i> Remove</button>
								  	<?php } ?>

						  			<!-- <div class="gj_s_msg">Successfully Added</div> -->
								</div>
							</div>
						</div>
					</div>
          <form method="post" action="<?php echo base_url('index.php/shopping/get_product'); ?>">
            <input type="hidden" name="package_id" value="<?php echo $obj['package_obj']['package_id']; ?>">
              <button type="submit" name="back" class="country_button" style="float: ri" ><i class="fa fa-arrow-left"  ></i> BACK</button>
          </form>
					<hr class="gj_sv_hr1"/>
				</div>
				<!-- Description tab Section Start -->
				<div class="row">
					<div class="col-md-12">
						<div class="gj_sv_tabsec">
							<ul class="nav nav-tabs">
							  	<li class="active"><a data-toggle="tab" href="#description">DESCRIPTION</a></li>
							  	<li><a data-toggle="tab" href="#review">REVIEW (0)</a></li>
							</ul>
							<div class="tab-content">
							  	<div id="description" class="tab-pane fade in active">
                    <?php if(count($obj['product_obj']['product_desc']) > 0){ 
                      foreach ($obj['product_obj']['product_desc'] as $des) { ?>
							    	  <p class="gj_dsc_cnt"><?php echo $des; ?></p>
                    <?php } ?>
                    <?php }else{ ?>
                      <p class="gj_dsc_cnt">--</p>
                    <?php } ?>
							  	</div>
							  	<div id="review" class="tab-pane fade">
							  		<div class="gj_re_sec">
							  			<p class="gj_no_rev1">There are no reviews yet.</p>
							  			<p class="gj_no_rev2">BE THE FIRST TO REVIEW “BUTTERFLY MATCHLESS 4 JAR”</p>
							  			<form action="" class="gj_form_star_rev">
							  				<div class="row">
							  					<div class="col-md-3">
									  				<div class="gj_star_rating">
									  					<p class="gj_rate_hd">YOUR RATING</p>
									  					<p class="gj_sv_review"><i class="fa fa-star gj_star_rate"></i><i class="fa fa-star gj_star_rate"></i><i class="fa fa-star gj_star_rate"></i><i class="fa fa-star gj_star_rate"></i><i class="fa fa-star gj_star_rate"></i></p>
									  				</div>
								  				</div>
								  				<div class="col-md-6">
									  				<div class="gj_review_submit">
									  					<button type="submit" class="gj_rev_submit">SUBMIT</button>
									  				</div>
								  				</div>
							  				</div>
							  				<div class="row">
							  					<div class="col-md-12">
								  					<div class="gj_your_rev">
								  						<label for="your_review">YOUR REVIEW</label>
								  						<textarea name="your_review" id="your_review" cols="30" rows="10" class="gj_rev_tarea form-control"></textarea>
								  					</div>
							  					</div>
						  					</div>
						  					<div class="row">
							  					<div class="col-md-6">
								  					<div class="gj_your_rev">
								  						<label for="your_review">Name <span>*</span></label>
								  						<input type="text" class="form-control gj_inp_rev"/>
								  					</div>
							  					</div>
							  					<div class="col-md-6">
								  					<div class="gj_your_rev">
								  						<label for="your_review">EMAIL <span>*</span></label>
								  						<input type="email" class="form-control gj_inp_rev"/>
								  					</div>
								  				</div>
							  				</div>
							  			</form>
							  		</div>
							  	</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Description tab Section end -->
			</div>
		</div>
	</section>
	<!-- single view carousel Section End -->
