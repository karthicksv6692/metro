<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-number-input.js'); ?>"></script>
      <div class="row text-center">
      
      </div>
      <?php //echo"<pre>";print_r($_SESSION['cart_items']); echo"</pre>"; ?>
     <?php $tot_price = 0; $tot_pv = 0; if(isset($_SESSION)){ ?>  
      <!-- cart page Section Start -->
        <section> 
          <div class="gj_cart_bg">
            <div class="container">
              <div>
                <p class="text-center dhinatechnologies_metrokings_headings pdt"> PAYMENT </p>
                <hr class="dhinatechnologies_metrokings_hr">  
              </div>
              <div class="row">
                <div class="col-md-12 col-sm-12">
                  <div class="gj_cart_tot table-responsive">
                    <table id="gj_cart_table" class="table">
                        <thead>
                            <tr class="gj_c_my">
                              <!-- <th colspan="6" class="text-left"><span>My Cart</span></th> -->
                            </tr>
                            <tr>
                              <th>PREVIEW</th>
                              <th>PRODUCT</th>
                              <th>PV</th>
                              <th>PRICE</th>
                              <th>Quantity</th>
                              <th>TOTAL</th>
                            </tr>
                        </thead>
                       
                        <?php if(isset($_SESSION['cart_items'])){ ?>  
                          <?php if($_SESSION['cart_items'] != null){  //print_r($_SESSION);?> 
                             <tbody>
                              
                              <?php foreach($_SESSION['cart_items'] as $cart){ 
                                $tot_price += $cart['user_price'];
                                $tot_pv += $cart['point_value']*$cart['user_quantity'];
                              ?>
                                <tr class="crt_prd_<?php echo $cart['product_id'] ?>">
                                  <td>
                                      <?php if(!is_numeric($cart['product_image_url'])){ ?>
                                        <img src="<?php echo base_url($cart['product_image_url']); ?>" alt="cart image" class="img-responsive gj_c_p_img">
                                      <?php } ?>
                                    </td>
                                  <td><?php echo $cart['product_name'] ?></td>
                                  <td class=" gj_red_color"><?php echo $cart['point_value'] ?></td>
                                  <td class=" gj_red_color"><?php echo $cart['product_display_price'] ?></td>
                                  <td class="gj_spin_quan"><?php echo $cart['user_quantity'] ?></td>
                                  <td class="prt_prc_<?php echo $cart['product_id'] ?>"><?php echo $cart['user_price'] ?></td>
                                  
                                </tr>
                              <?php }?>
                                
                              </tbody>
                          <?php }else{?>
                              <tbody>
                                <tr>
                                  <td colspan="6">Cart is Empty</td>
                                </tr>
                              </tbody>
                          <?php } ?>
                        <?php } ?>
                      
                    </table>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-12">
                  <div class="gj_total_cart table-responsive">
                    <table id="gj_cart_total_table" class="table">
                        <thead>
                            <tr class="gj_c_my">
                              <th colspan="2" class="text-left"><span>CART DETAILS</span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                              <td>Total Poin Value </td>
                              <td class="k_prd_pv"></i><?php echo $tot_pv; ?> PV</td>
                            </tr>
                            <tr>
                              <td>Price </td>
                              <td class="k_prd_pg"><i class="fa fa-inr"></i><?php echo $tot_price; ?>.00</td>
                            </tr>
                            <tr>
                              <td>Delivery Charges</td>
                              <td><i class="fa fa-inr"></i> 0.00</td>
                            </tr>
                            <tr class="gj_amount">
                              <th>Amount Payable</th>
                              <th class="k_prd_pg"><i class="fa fa-inr"></i> <?php echo $tot_price; ?>.00</th>
                            </tr>
                            <!-- <tr class="gj_c_saving">
                              <th colspan="2" class="text-left">Your Total Savings on this order<span><i class="fa fa-inr"></i> 4752.00</span></th>
                            </tr> -->
                        </tbody>
                    </table>
                  </div>
                </div>
                
              </div>

              <div class="row" style="border: 1px solid #b1a6a6;padding: 0px 15px 23px 15px;width: 100%;margin: 0 auto;">
                <form method="post" action="<?php echo base_url('index.php/shopping/shop_pin_request'); ?>">
                  <div class="width21">
                      <h3 style="padding-right: 25px;">Pin Request For</h3>
                      <select name="request_for" class="request_for req_fr" style="margin-top: 15px;margin-bottom: 5px;">
                        <option value="0">Registration</option>
                        <option value="1">Re-Purchase</option>
                      </select>
                      <?php echo form_error('request_for','<span class="help-block form-error">', '</span>'); ?> 
                  </div>
                  <div class="width21 py_md">
                      <h3 style="padding-right: 25px;">Payment Mode</h3>
                      <select name="payment_mode" class="payment_mode req_fr" style="margin-top: 15px;margin-bottom: 5px;">
                        <option class="rep_opt" value="0">Cash</option>
                      </select>
                      <?php echo form_error('payment_mode','<span class="help-block form-error">', '</span>'); ?> 
                  </div>
                  <div class="width21 req_too">
                      <h3 style="padding-right: 25px;">Request To</h3>
                      <select name="request_to" class="request_to req_fr" style="margin-top: 15px;margin-bottom: 5px;">
                      </select>
                      <?php echo form_error('request_to','<span class="help-block form-error">', '</span>'); ?> 
                  </div>
                  <div class="col-md-2">
                    <button type="submit" class="gj_checkout_but" style="margin-top: 3em;">PAY NOW</button>
                  </div>
                </form>
              </div>


              <div class="row pull-right" style="width: 100%; text-align: right;">
          <a href="<?php echo base_url('index.php/shopping/cart'); ?>" style="width: 100%;margin: 30px 0px;">
            <button type="button" name="back" class="country_button" style="float: right"><i class="fa fa-arrow-left"></i> BACK</button>
          </a>
      </div> 
            </div>
          </div>

        </section>

        <!-- cart page Section End -->
    <?php } ?>  


<style type="text/css">
  .cart_price_bx {
      width: 300px;
      float: right;
      text-align: right;
      position: absolute;
      right: 5%;
  }
  .cart_price_whbx {
      position: relative;
          height: 150px;
          width: 100%;
  }
  form{width: 100%;}
  .gj_red_color {
      color: red !important;
  }
  .width21{
    width: 21% !important;
    float: left;
  }
  .page-content section.content.bgcolor-1 form span.select2.select2-container.select2-container--default.select2-container--focus, span.select2.select2-container.select2-container--default.select2-container--below, span.select2.select2-container.select2-container--default {
      width: 180px !important;
      margin: 8px 0px 0px 0px !important;
      text-align: left;
  }

</style>

<script type="text/javascript">
  $('.request_for').change(function(){
    fr = $(this).val();
    if(fr == 1){
      $('.rep_opt').after('<option class="rep_wal" value="4">Repurchase Wallet</option>')
    }else{
      $('.rep_wal').remove();
      $('.wll').remove();
    }
  })
  $('.payment_mode').change(function(){
    fr = $(this).val();
    if(fr == 4){
      $('.req_too').hide();
      $.ajax({
        url:"<?php echo base_url('index.php/login/get_rep_amt');?>",
        beforeSend: function(){
          $('.py_md').after('<div class="width21 wll"><h3 style="padding-right: 25px;">Wallet Balance</h3><h3 class="wal_bal gj_red_color"></h3></div>');
        },
        success: function(data){
          $('.wal_bal').text(data);
        }
      })
    }else{$('.req_too').show();$('.wll').remove();}
  })
  $('.request_to').select2({
    placeholder: 'Select Stock Pointer',
    ajax: {
      url: '<?php echo base_url("index.php/common_controller/get_stock_pointers") ?>',
      dataType: 'json',
      delay: 1,
      data: function (params) {console.log(params);
            return { q: params.term 
            };
      },
      processResults: function (data) { return { results: data }; },
      cache: true
    },
  });
</script>
