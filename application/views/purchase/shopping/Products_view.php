
<div class="container">
	<div class="row pull-right">
		<div class="car_details">
			<!-- <?php if(isset($_SESSION)){ ?>	
				<?php if(isset($_SESSION['cart_items'])){ ?>	
					<?php if($_SESSION['cart_items'] != null){ ?>	
						<table class="table">
							<thead>
								<th>Product Code</th>
								<th>Product Name</th>
								<th>Quantity</th>
								<th>Price</th>
							</thead>
							<?php //print_r($_SESSION['cart_items']); ?>
							<tbody class="cart_items_body">
								<?php foreach($_SESSION['cart_items'] as $cart){ ?>
									<tr class="crt_prd_<?php echo $cart['product_id'] ?>">
										<td><?php echo $cart['product_code'] ?></td>
										<td><?php echo $cart['product_name'] ?></td>
										<td><?php echo $cart['user_quantity'] ?></td>
										<td><?php echo $cart['user_price'] ?></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					<?php }else{?>
						<table class="table">
							<thead  class="empty_value_head">
								<th>Product Code</th>
								<th>Product Name</th>
								<th>Quantity</th>
								<th>Price</th>
							</thead>
							<tbody class="cart_items_body">
								<tr class="empty_value">
									<td>Cart is Empty</td>
								</tr>
							</tbody>
						</table>
					<?php } ?>
				<?php } ?>
			<?php } ?> -->
		</div>
	</div>
</div>
<?php //print_r($_SESSION['cart_items']); ?>
	<!-- products -->
	<section class="gj_s_p_sec"> 
		<div class="gj_s_p_bg">
			<div class="container">
				<div>
					<p class="text-center dhinatechnologies_metrokings_headings pdt"> Our Products </p>
					<hr class="dhinatechnologies_metrokings_hr">	
				</div>
				<div class="row">
				<?php if(count($products) >0 ){?>
					 <?php foreach ($products as $prd) { ?>
						<!-- <div class="col-md-3 col-sm-6 col-xs-12">
							<div class="wow zoomIn">
								<div class="snip1532" onclick="$(this).closest('form').submit();">
								  	<img src="<?php echo base_url('assets/images/dhinatechnologies_metrokings_Product_pic1.png'); ?>" alt="product"/>
								  	<div class="gj_prod_cap" >
								    	<h3 class="gj_cap_title"><?php echo $prd['product_name']; ?></h3>
								    	<h3 class="gj_cap_title"><?php echo $prd['product_price']; ?></h3>
								  	</div>
								  	<button onclick="add_to_cart(<?php echo $prd['product_id']; ?>)">Add To Cart</button>
								  	<button onclick="remove_from_cart(<?php echo $prd['product_id']; ?>)">Remove</button>
								</div>
							</div>
						</div> -->
						<div class="col-md-3 col-sm-6 col-xs-12">
							<div class="wow zoomIn gj_s_p_tot">
								<div class="gj_single_prod">
								  	<img src="<?php echo base_url('assets/images/dhinatechnologies_metrokings_Product_pic1.png'); ?>" alt="single product" />
								  	<div class="gj_s_caption">
								    	<div>
								    		<form method="post" action="<?php echo base_url('index.php/shopping/s_view'); ?>">
								    			<input type="hidden" name="product_id" value="<?php echo $prd['product_id']; ?>">
								    			<a onclick="$(this).closest('form').submit();" title="View This Product">
								    				<i class="fa fa-link"></i>
								    			</a>
								    		</form>
								    	</div>
								  	</div>
								</div>
								<div class="gj_s_p_hd text-center">
							  		<p class="gj_s_p_title"><?php echo $prd['product_name']; ?></p>
								  <?php $ser = array_search($prd['product_id'], array_column($_SESSION['cart_items'], 'product_id'));
									 if(!is_numeric($ser)){
								  ?>
							  		<button class="gj_cart_but btn-success" id="bt_<?php echo $prd['product_id']; ?>" type="button" onclick="common_add_remove(this,<?php echo $prd['product_id']; ?>,1)"><i class="fa fa-check"></i> Add Cart</button>
								  <?php }else{ ?>
							  		<button class="gj_cart_but btn-danger" id="bt_<?php echo $prd['product_id']; ?>" type="button" onclick="common_add_remove(this,<?php echo $prd['product_id']; ?>,1)"><i class="fa fa-close"></i> Remove</button>
							  	<?php } ?>
							  	</div>
							</div>
						</div>

					<?php } ?> 
				<?php }else{echo"Product is Empty";} ?>
				</div>
				<div class="row text-center">
					<a href="<?php echo base_url('index.php/shopping/cart'); ?>" class="crt_but"><input type="button" value="Cart" class="crt_but_but"></a>
				</div>
			</div>
		</div>
	</section> 
	
