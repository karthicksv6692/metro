<section class="gj_s_p_sec"> 
  <div class="gj_s_p_bg">
    <div class="container">
      <div>
        <p class="text-center dhinatechnologies_metrokings_headings pdt"> Our Packages </p>
        <hr class="dhinatechnologies_metrokings_hr">  
      </div>
      
      <div class="row">
        <a href="<?php echo base_url('index.php/shopping/get_packages'); ?>" target="new" style="width: 23%">
          <div class="xx">
            <div class="s_box">
              <h3>Packages</h3>
            </div>
          </div>
        </a>
        <a href="<?php echo base_url('index.php/shopping/get_user_choice_products'); ?>" target="new" style="width: 23%;margin-left: 2em;">
          <div class="xx">
            <div class="s_box">
              <h3>User Choice Products</h3>
            </div>
          </div>
        </a>
        
      </div>

      <?php if(isset($_SESSION['logged_in'])){ ?>
        <?php if($_SESSION['logged_in'] == 1){ ?>
          <div class="row pull-right" style="width: 100%; text-align: right;">
              <a href="<?php echo base_url('index.php/login/Dashboard'); ?>" style="width: 100%;margin: 30px 0px;">
                <button type="button" name="back" class="country_button" style="float: right;width: 230px;"><i class="fa fa-arrow-left"></i> Back To Dashboard</button>
              </a>
          </div> 
        <?php } ?>
      <?php } ?>
    </div>
  </div>
</section>
<style type="text/css">
  .s_box {
      background: #2bb1e9;
      text-align: center;
      height: 45px;
  }
  .s_box1 {
      background: #2bb1e9;
      text-align: center;
      height: 45px;
  }
  .s_box h3{
      padding-top: 9%;
      font-size: 19px !important;
      border: 1px solid #2bb1e9;
      background: #fff;
      color: #2bb1e9;
      padding-bottom: 8%;
  }
  .s_box1 h3{
    padding-top: 6%;
    font-size: 19px !important;
    border: 1px solid #2bb1e9;
    background: #fff;
    color: #2bb1e9;
    padding-bottom: 4%;
  }
  .s_box1.active h3,.s_box.active h3,.s_box1 h3:hover,.s_box h3:hover{
    background: #2bb1e9;
    color: #fff;
  }
</style>