<?php //print_r($obj);  ?>

<div class="dkbody">
	<div class="root_sub mar_3_em">
		<div class="left_node_count">
			<p>Left Count:  <?php echo $obj['root']['left_count'][0]; ?></p>
		</div>
		<div class="root">
			ROOT
			<p><?php echo $obj['root']['id'][0]; ?></p>
			<p><?php echo $obj['root']['name'][0]; ?></p>
		</div>
		<div class="right_node_count">
			<p>Right Count:  <?php echo $obj['root']['right_count'][0]; ?></p>
		</div>
	</div>
	<div class="row mar_3_em">
		<div class="first_sub">
			<div class="left">
				left
				<?php if($obj['left']['id'][0] != 0){ ?>
					<div class="left" onclick="get_node(<?php echo $obj['left']['p_id'][0]; ?>)">
						<p><?php echo $obj['left']['id'][0]; ?></p>
						<p><?php echo $obj['left']['name'][0]; ?></p>
					</div>
				<?php }else{ ?>
					<p class="left empty">Empty</p>
				<?php } ?>
			</div>

		</div>
		<div class="first_sub">
			<div class="right">
				right
				<?php if($obj['right']['id'][0] != 0){ ?>
					<div class="right" onclick="get_node(<?php echo $obj['right']['p_id'][0]; ?>)">
						<p><?php echo $obj['right']['id'][0]; ?></p>
						<p><?php echo $obj['right']['name'][0]; ?></p>
					</div>
				<?php }else{ ?>
					<p class="right empty">Empty</p>
				<?php } ?>
			</div>
		</div>
	</div>

	<div class="row mar_3_em">
		<div class="col-md-6">
			<div class="first_sub xx">	
				<div class="left_left">
					left_left
					<?php if($obj['left_left']['id'][0] != 0){ ?>
						<div class="left_left" onclick="get_node(<?php echo $obj['left_left']['p_id'][0]; ?>)">
							<p><?php echo $obj['left_left']['id'][0]; ?></p>
							<p><?php echo $obj['left_left']['name'][0]; ?></p>
						</div>
					<?php }else{ ?>
						<p class="left_left empty">Empty</p>
					<?php } ?>
				</div>
			</div>
			<div class="first_sub yy">
				<div class="left_right">
					left_right
					<?php if($obj['left_right']['id'][0] != 0){ ?>
						<div class="left_right" onclick="get_node(<?php echo $obj['left_right']['p_id'][0]; ?>)">
							<p><?php echo $obj['left_right']['id'][0]; ?></p>
							<p><?php echo $obj['left_right']['name'][0]; ?></p>
						</div>
					<?php }else{ ?>
						<p class="left_right empty">Empty</p>
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="first_sub">	
				
				<div class="right_right">
					right_right
					<?php if($obj['right_right']['id'][0] != 0){ ?>
						<div class="right_right" onclick="get_node(<?php echo $obj['right_right']['p_id'][0]; ?>)">
							<p><?php echo $obj['right_right']['id'][0]; ?></p>
							<p><?php echo $obj['right_right']['name'][0]; ?></p>
						</div>
					<?php }else{ ?>
						<p class="right_right empty">Empty</p>
					<?php } ?>
				</div>
			</div>
			<div class="first_sub">	
				<div class="right_left">
				right_left
				<?php if($obj['right_left']['id'][0] != 0){ ?>
					<div class="right_left" onclick="get_node(<?php echo $obj['right_left']['p_id'][0]; ?>)">
						<p><?php echo $obj['right_left']['id'][0]; ?></p>
						<p><?php echo $obj['right_left']['name'][0]; ?></p>
					</div>
				<?php }else{ ?>
					<p class="right_left empty">Empty</p>
				<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function get_node(id){
		if(id > 0){
			$.ajax({
	          type: "POST",
	          url: "<?php echo base_url(); ?>index.php/User_package_purchase/ajax_tree",
	          data: {'id':id},
	            success:  function (data) { 
	              	var option = JSON.parse(data);
	              	root_p_id = option['root']['p_id'][0];
	              	root_id = option['root']['id'][0];
	              	root_name = option['root']['name'][0];

	              	left_count = option['root']['left_count'][0];
	              	right_count = option['root']['right_count'][0];

	              	left_p_id = option['left']['p_id'][0];
	              	left_id = option['left']['id'][0];
	              	left_name = option['left']['name'][0];

	              	left_left_p_id = option['left_left']['p_id'][0];
	              	left_left_id = option['left_left']['id'][0];
	              	left_left_name = option['left_left']['name'][0];

	              	left_right_p_id = option['left_right']['p_id'][0];
	              	left_right_id = option['left_right']['id'][0];
	              	left_right_name = option['left_right']['name'][0];

	              	right_p_id = option['right']['p_id'][0];
	              	right_id = option['right']['id'][0];
	              	right_name = option['right']['name'][0];

	              	right_left_p_id = option['right_left']['p_id'][0];
	              	right_left_id = option['right_left']['id'][0];
	              	right_left_name = option['right_left']['name'][0];

	              	right_right_p_id = option['right_right']['p_id'][0];
	              	right_right_id = option['right_right']['id'][0];
	              	right_right_name = option['right_right']['name'][0];

	              	$( ".root p:eq(0)" ).text(root_id);
	              	$( ".root p:eq(1)" ).text(root_name);

	              	$( ".left_node_count p:eq(0)" ).text(left_count);
	              	$( ".right_node_count p:eq(0)" ).text(right_count);

	              	if(left_p_id > 0 ){
	              		$('.left').removeClass('empty');
	              		$( ".left p:eq(0)" ).text(left_id);
	              		$( ".left p:eq(1)" ).text(left_name);
	              		$('.left').attr('onclick',"get_node("+left_p_id+")");
	              	}else{
	              		$('.left').addClass('empty');
	              		$( ".left p:eq(0)" ).text("");
	              		$( ".left p:eq(1)" ).text("");
	              		$('.left').removeAttr('onclick');
	              	}

	              	if(left_left_p_id > 0 ){
	              		$('.left_left').removeClass('empty');
	              		$( ".left_left p:eq(0)" ).text(left_left_id);
	              		$( ".left_left p:eq(1)" ).text(left_left_name);
	              		$('.left_left').attr('onclick',"get_node("+left_left_p_id+")");
	              	}else{
	              		$('.left_left').addClass('empty');
	              		$( ".left_left p:eq(0)" ).text("");
	              		$( ".left_left p:eq(1)" ).text("");
	              		$('.left_left').removeAttr('onclick');
	              	}

	              	if(left_right_p_id > 0 ){
	              		$('.left_right').removeClass('empty');
	              		$( ".left_right p:eq(0)" ).text(left_right_id);
	              		$( ".left_right p:eq(1)" ).text(left_right_name);
	              		$('.left_right').attr('onclick',"get_node("+left_right_p_id+")");
	              	}else{
	              		$('.left_right').addClass('empty');
	              		$( ".left_right p:eq(0)" ).text("");
	              		$( ".left_right p:eq(1)" ).text("");
	              		$('.left_right').removeAttr('onclick');
	              	}


	              	if(right_p_id > 0 ){
	              		$('.right').removeClass('empty');
	              		$( ".right p:eq(0)" ).text(right_id);
	              		$( ".right p:eq(1)" ).text(right_name);
	              		$('.right').attr('onclick',"get_node("+right_p_id+")");
	              	}else{
	              		$('.right').addClass('empty');
	              		$( ".right p:eq(0)" ).text("");
	              		$( ".right p:eq(1)" ).text("");
	              		$('.right').removeAttr('onclick');
	              	}

	              	if(right_left_p_id > 0 ){
	              		$('.right_left').removeClass('empty');
	              		$( ".right_left p:eq(0)" ).text(right_left_id);
	              		$( ".right_left p:eq(1)" ).text(right_left_name);
	              		$('.right_left').attr('onclick',"get_node("+right_left_p_id+")");
	              	}else{
	              		$('.right_left').addClass('empty');
	              		$( ".right_left p:eq(0)" ).text("");
	              		$( ".right_left p:eq(1)" ).text("");
	              		$('.right_left').removeAttr('onclick');
	              	}

	              	if(right_right_p_id > 0 ){
	              		$('.right_right').removeClass('empty');
	              		$( ".right_right p:eq(0)" ).text(right_right_id);
	              		$( ".right_right p:eq(1)" ).text(right_right_name);
	              		$('.right_right').attr('onclick',"get_node("+right_right_p_id+")");
	              	}else{
	              		$('.right_right').addClass('empty');
	              		$( ".right_right p:eq(0)" ).text("");
	              		$( ".right_right p:eq(1)" ).text("");
	              		$('.right_right').removeAttr('onclick');
	              	}
	            }
	        })
		}
	}
</script>

<style type="text/css">
	.root_sub{
		width: 100%;
	}
	.root {
	    text-align: center;
	    width: 160px;
	    margin: 0 auto;
	    height: 100px;
	    background: yellow;
	}
	.first_sub {
	    width: 50%;
	    text-align: center;
	    float: left;
	}
	.left{
		width: 160px;
	    margin: 0 auto;
	    height: 100px;
	    background: green;
	}
	.right{
		width: 160px;
	    margin: 0 auto;
	    height: 100px;
	    background: #83838e;
	}
	.empty{
	    background: red !important;

	}
	.left_left{
		width: 160px;
	    margin: 0 auto;
	    height: 100px;
	    background: #807000;
	}
	.left_right{
		width: 160px;
	    margin: 0 auto;
	    height: 100px;
	    background: #00807a;
	}
	.right_left{
		width: 160px;
	    margin: 0 auto;
	    height: 100px;
	    background: rgba(79,0,128,0.5);
	}
	.right_right{
		width: 160px;
	    margin: 0 auto;
	    height: 100px;
	    background: rgba(128,0,112,0.6);
	}
	.mar_3_em{
		    margin: 3em 0em !important;
	}
	.left_node_count {
	    width: 200px;
	    float: left;
	    margin-top: 2em;
	    margin-left: 14em;
	}
	.right_node_count {
	    width: 200px;
	    float: right;
	    margin-top: -60px;
	    margin-right: 7em;
	}
</style>