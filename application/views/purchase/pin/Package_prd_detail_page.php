
<?php  //print_r($product_details);
	if (isset($obj)) {
		$avail_amount = $obj->getUserAvailableAmount();
		$pack_prd_price = $obj->getPackPrdPrice();
		$product_list = $obj->getProductListArray();
	}else{$avail_amount = "";$pack_prd_price="";$product_list = array();}
	//print_r($product_list);
?>
<div class="container-fluid">
		<div class="page-header">
			<h3>Package Products List</h3>
		</div>
		<div class="page-content">
			<section class="content bgcolor-1">
				<?php if($avail_amount>=$pack_prd_price){ ?>
					<form method="POST" action="<?php echo base_url('index.php/Purchase/package_purchase'); ?>" class="common_form">
				<?php }else{?>
				<form method="POST" action="<?php echo base_url('index.php/Purchase/package_purchase'); ?>" class="common_form">
					<!-- <form method="POST" action="<?php echo base_url('index.php/Purchase/add_money'); ?>" class="common_form"> -->
				<?php } ?>

					<div class="dkinput-txtflds simple_form svk_simple_form1">
						<table class="table table-bordered text-left">
							<thead>
								<tr>
									<th>Product_code</th>
									<th>Name</th>
									<th>Price</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($product_details) > 0){ ?>
									<?php for($i=0;$i<count($product_details['name']);$i++){ ?>
										<tr>
											<td><?php echo $product_details['code'][$i];  ?></td>
											<td><?php echo $product_details['name'][$i];  ?></td>
											<td><?php echo $product_details['price'][$i];  ?></td>
										</tr>
									<?php } ?>
								<?php } ?>
							</tbody>
						</table>		
					</div>
					<div class="row full_width">
						<div class="col-md-6">
							<div class="avail_bal">
								<h4>Available Balance</h4>
								<span><?php echo $avail_amount; ?></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="tot">
								<h4>Total</h4>
								<span><?php echo $pack_prd_price; ?></span>
							</div>
						</div>
					</div>

					<input type="hidden" name="pin_page" value="asdf">
					<?php if($avail_amount<$pack_prd_price){ ?>
						<div class="row bottom_buttons">
							<!-- <div class="next_buton text-center pull-left"><a  onclick="$(this).closest('form').submit()">Add Money<i class="fa fa-arrow-circle-o-right font_arrow"></i></a></div> -->
							<div class="next_buton text-center pull-left"><a  onclick="$(this).closest('form').submit()">Purchase <i class="fa fa-arrow-circle-o-right font_arrow"></i></a></div>
						</div>
					<?php }else{ ?>
						<div class="row bottom_buttons">
							<div class="next_buton text-center pull-left"><a  onclick="$(this).closest('form').submit()">Purchase <i class="fa fa-arrow-circle-o-right font_arrow"></i></a></div>
						</div>
					<?php } ?>

				</form>
			</section>
		</div>
	</div>