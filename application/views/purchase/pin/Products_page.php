<?php //print_r($products); ?>
<div class="dkbody">
	
	<div id="myDiv" class="container-fluid">
		<div class="page-header">
			<h3>Products List</h3>
		</div>
		<?php if(count($products['products']) > 0){ ?>
		<div class="loader">
			<img src="<?php echo base_url('assets/images/loader/ring-alt.gif'); ?>">
		</div>
		<div class="prd_bdy hide">
			<?php  if($products != null){ ?>
				<?php foreach($products['products'][0] as $prd){ ?>
					<div class="col-md-3">
						<div class="sing_prd">
							<div class="prg_img text-center">
								<img src="<?php echo base_url('assets/images/me.jpg'); ?>">
							</div>
							<p class="title">Name :  <?php echo $prd['product_name']; ?></p>
							<p class="price">Price :  <?php echo $prd['product_price']; ?></p>
							<p class="cart add_<?php echo $prd['product_id']; ?>" onclick="ad_cart(<?php echo $prd['product_id']; ?>)">Add to Cart</p>
						</div>
					</div>
				<?php } ?>

			<?php  }else{echo"Package Empty";} ?>
		</div>
	<div class="row" style="    width: 100%;">	
		<div class="full_prd_pagenation">
			<?php  //echo $products['pagination'][0]; ?>
			<!-- <ul class="full_prd_pag">
				<li class="full_prd_first"><a>First</a></li>
				<li class="full_prd_prev"><a>Prev</a></li>
				<li class="full_prd_ele active"><a>1</a></li>
				<li class="full_prd_ele"><a>2</a></li>
				<li>.</li>
				<li>.</li>
				<li class="full_prd_ele"><a>5</a></li>
				<li class="full_prd_next"><a>Next</a></li>
				<li class="full_prd_last"><a>Last</a></li>
			</ul> -->
		</div>
	</div>

	<div class="row" style="    width: 100%;">
		<div class="text-center arrow_sect">
			<form method="post" action="<?php echo base_url('index.php/purchase/products'); ?>">
				<input type="hidden" name="prd_ids" class="prd_ids" value="0">
				<div class="next_but"><a onclick="$(this).closest('form').submit();">Add To Cart<i class="fa fa-arrow-circle-o-right font_arrow"></i></a></div>
			</form>
		</div>
	</div>

		<?php }else{echo"Products Empty";} ?>
	</div>
	

</div>

<script type="text/javascript">
	var prd_values = [];
	setTimeout(function(){ 
		$('.prd_bdy').removeClass('hide');
		$('.prd_bdy').addClass('show');
		$('.loader').removeClass('show');
		$('.loader').addClass('hide');
	}, 500);
	function ad_cart(id){
		prd_values.push(id);
		$('.prd_ids').val(prd_values);
		$('.add_'+id).hide();
		$('.add_'+id).html('<span><i class="fa fa-times" aria-hidden="true"></i>"></span>');
	}	
</script>

<style type="text/css">
	.col-md-3{
		float: left;
	}
	.hide{
		display: none;
	}
	.show{
		display: block;
	}
	.loader img {
	    position: absolute;
	    height: 50px;
	    width: 50px;
	    left: 48%;
	    top: 44%;
	}
	.loader {
	    width: 100%;
	    height: 400px;
	    position: relative;
	}
	.sing_prd {
	    border: 1px solid #b1abab;
	    max-height: 265px;
	    min-height: 265px;
	}
	.sing_prd .prg_img img {
	    width: 60%;
	    margin-top: 10px;
	}
	.sing_prd p.title {
	    text-align: center;
	    padding: 20px 0px;
	    font-weight: 800;
	    font-size: 20px;
	}
	.sing_prd p.price {
	    font-weight: 200;
	    width: 48%;
	    margin-left: 5px;
	    float: left;
	}
	.sing_prd p.cart {
	    background: #0a87bb;
	    width: 44%;
	    float: right;
	    color: #fff;
	    text-align: center;
	    margin-right: 5px;
	    height: 28px;
	}
	ul.full_prd_pag li {
	    display: inherit;
	}
	ul.full_prd_pag {
	    list-style-type: none;
	    display: inline;
	}
	.full_prd_pagenation {
	    margin: 2em 0em;
	    text-align: center;
	}
	li.full_prd_first {
	    
	    
	    
	}
	li.full_prd_first,li.full_prd_prev,li.full_prd_ele,li.full_prd_next,li.full_prd_last {
		
		padding: 5px 10px;
		border: 1px solid #0a87bb;
	    background: #fff;
	    color: #0a87bb;
	    cursor: pointer;
	}
	li.full_prd_ele.active {
	   background: #0a87bb;
		color: #fff;
	}
	ul.full_prd_pag li:hover{
		background: #0a87bb;
		color: #fff;
	}
	ul.full_prd_pag li.disabled{
		background: #9db2ba !important;
	    color: #fff !important;
	    border: 1px solid #9db2ba !important;
	    cursor: none;
	}
	.prd_bdy{
		width: 100%;
	}
</style>