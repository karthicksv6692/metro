<?php 
	if ($this->session->userdata('pin_obj') != null) {
		$obj = unserialize($_SESSION['pin_obj']);
		$pin_number = $obj->getPinNo();
	}else{$pin_number = "";}
?>
<div class="container-fluid">
	<div class="page-header">
		<h3>Enter The Pin - Repurchase</h3>
	</div>
	<div class="page-content">
		<section class="content bgcolor-1">
			<form method="POST" action="<?php echo base_url('index.php/Purchase/enter_pin'); ?>" class="common_form">

				<div class="dkinput-txtflds simple_form svk_simple_form">
					<span class="input input--nao">
						<input type="text" class="input__field input__field--nao" name="pin_no" value="<?php if(set_value('pin_no') != null){echo set_value('pin_no');}else{echo $pin_number;} ?>">
						<label class="input__label input__label--nao" for="input-1">
							<span class="input__label-content input__label-content--nao">Enter The Pin Number</span>
						</label>
						<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
							<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
						</svg>
					</span>		
					<?php echo form_error('pin_no','<span class="help-block form-error">', '</span>'); ?>				
          
          <div class="row">
            <div class="col-md-12 pull-left mt-5 pad_zero">
              <div class="form_group radio_box text-left">
                <label style="margin-right: 39px; margin-left: 36px;">POSITION  :  </label> 
                <label class="chkbo" style="display:inline">Left
                  <input type="radio" id="radio" name="position" value="0" checked>
                  <label for="radio"></label>
                </label>
                <label class="chkbo" style="display:inline"  > Right
                  <input type="radio" id="radio2" name="position"  value="1">
                  <label for="radio2"></label>
                </label>
                
                <?php echo form_error('gender','<span class="help-block form-error">', '</span>'); ?>
              </div>
            </div>
          </div>


				</div>
        
				<input type="hidden" name="pin_page" value="asdf">
				<div class="row bottom_buttons">
					<div class="next_buton text-center pull-left"><a  onclick="$(this).closest('form').submit()">SUBMIT <i class="fa fa-arrow-circle-o-right font_arrow"></i></a></div>
				</div>
			</form>
		</section>
	</div>
</div>

<style type="text/css">
  .next_buton {
      width: 100%;
      text-align: center;
      margin: 4.5rem 0rem;
  }
</style>