<div class="container-fluid">
	<div class="user-header">
		<h3>Add Money</h3>
	</div>
	<div class="tab-contentt">
		<form method="POST" action="<?php echo base_url('index.php/Purchase/add_money'); ?>">
			<div class="dkinput-txtflds simple_form svk_simple_form">
				<span class="input input--nao">
					<input type="text" class="input__field input__field--nao" name="rupee" value="<?php if(set_value('rupee') != null){echo set_value('rupee');}else{echo "";} ?>">
					<label class="input__label input__label--nao" for="input-1">
						<span class="input__label-content input__label-content--nao">Enter The Amount</span>
					</label>
					<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
						<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
					</svg>
				</span>		
				<?php echo form_error('rupee','<span class="help-block form-error">', '</span>'); ?>	
				<input type="hidden" name="money_add_page" value="asdf">
				<div class="row bottom_buttons">
					<div class="next_buton text-center pull-left"><a  onclick="$(this).closest('form').submit()">Next <i class="fa fa-arrow-circle-o-right font_arrow"></i></a></div>
				</div>
			</div>
		</form>
	</div>
</div>