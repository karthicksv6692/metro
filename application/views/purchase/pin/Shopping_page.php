<?php //print_r(unserialize($_SESSION['pin_obj'])); ?>
<?php if(isset($obj)){

	
	if($obj != null){
		$package_product_id 				= $obj->getPackageProductId();
		$package_product_name 				= $obj->getPackageProductName();
		$package_product_display_price 		= $obj->getPackageProductDisplayPrice();
		$package_id 						= $obj->getPackageId();
	}else{
		$package_product_id  = '';$package_product_name  = '';$package_product_quantity  = '';$package_product_display_price  = '';$package_product_price = '';$package_product_desc = '';$package_product_short_desc = '';$is_active = '';$package_id  = '';
	}
	if(isset($attachment) && !empty($attachment)){
		$package_image=$attachment;
	}
}else{
	$package_product_id  = '';$package_product_name  = '';$package_product_quantity  = '';$package_product_display_price  = '';$package_product_price = '';$package_product_desc = '';$package_product_short_desc = '';$is_active = '';$package_id  = '';
}
?>
<div class="container-fluid">
	<div class="user-header">
		<h3>User Module</h3>
	</div>
	<ul class="nav nav-tabs user_module_tab">
		<li><a data-toggle="tab" href="s_refferal_detail.php"><i class="fa  fa-paper-plane-o font_icon"></i> Refferal Details</a></li>
		<li class="active"><a data-toggle="tab" href="s_product_detail.php"><i class="fa fa-angle-double-down font_icon"></i> Product Details</a></li>
		<li><a data-toggle="tab" href="#"><i class="fa fa-user font_icon"></i>Personal Details</a></li>
		<li><a data-toggle="tab" href="#"><i class="fa fa-building font_icon"></i> Bank Details</a></li>
		<li><a data-toggle="tab" href="#"><i class="fa  fa-paperclip font_icon"></i> Attachments</a></li>
		<li><a data-toggle="tab" href="#"><i class="fa   fa-lock font_icon"></i> Security</a></li>
	</ul>
	<div class="tab-contentt">
		<form method="POST" action="<?php echo base_url('index.php/Purchase/Product_details'); ?>">
			<div class="row">
					<div class="container-fluid">
				      		<table class="table-responsive" id="propid_table">
				      		  	<thead>
					      		  	<tr>
						      		  	<th>PackageProduct Name</th>
						      		  	<th>Price</th>
						      		  	<th>Products</th>
						      		  	<th>Images</th>
						      		</tr>
				      		  	</thead>
				      		  	<tbody id="contact_append">

		      		  				<tr>
						      		  	<td>
							      		  	<span class="input input--nao">
												<input class="input__field input__field--nao" type="text" id="c_name" name="c_name[]" value="<?php echo $package_product_name; ?>">
												<label class="input__label input__label--nao" for="c_name">
													<span class="input__label-content input__label-content--nao">PackageProduct Name</span>
												</label>
												<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
													<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
												</svg>
											</span>	
												
						      		  	</td>
						      		  	<td>
							      		  	<span class="input input--nao">
												<input class="input__field input__field--nao" type="text" id="desig" name="desig[]" value="<?php echo $package_product_display_price; ?>">
												<label class="input__label input__label--nao" for="desig">
													<span class="input__label-content input__label-content--nao">Price</span>
												</label>
												<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
													<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
												</svg>
											</span>	
						      		  	</td>
						      		  	<td>
						      		  		<?php 
						      		  			$ar = array();
						      		  			foreach ($package_product_item as $ps) {
						      		  				array_push($ar, $ps->getPackageProductItemProductId()->getProductName());
						      		  			}
						      		  		?>
						      		  		<span class="input input--nao">
												<input class="input__field input__field--nao" type="text" id="c_mob" name="c_mob[]" value="<?php echo implode(',', $ar);	 ?>">
												<label class="input__label input__label--nao" for="c_mob">
													<span class="input__label-content input__label-content--nao">Products</span>
												</label>
												<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
													<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
												</svg>
											</span>	
												
					      		  		</td>
						      		  	<td align="center">
							      		  	<?php if(isset($package_image)){ 
					      						if(count($package_image) > 0){
											?>
												<img data-u="image" class="table_img" src="<?php echo base_url($package_image[0]->getAttachmentImageUrl()); ?>" class="img-responsive"/>
											<?php  }}else{ ?>

										    	<div>
										          	<img data-u="image" src="<?php echo base_url('assets/images'); ?>/no_image.jpg" class="img-responsive"/>
											        <div data-u="thumb">
											            <img class="i" src="<?php echo base_url('assets/images'); ?>/no_image.jpg" />
											        </div>
										        </div>

									     	<?php  } ?>
												
						      		  	</td>
						      		  					      		  	
					      		  	</tr>
						      						      		  	</tbody>
				      		</table>
	      		  	</div>


					<!-- <div class="col-md-12 ">
						<div class="dkinput-txtflds">
							<span class="input input--nao input_spn">
								<?php if(isset($pack_prd_obj)){ ?>
									<?php if(count($pack_prd_obj) > 0){ ?>
									<select class="slt_type full_width pack_prd_id" type="text"  name="pack_prd_id">
										<option value="0">--Select--</option>
										<?php foreach ($pack_prd_obj as $pack) { ?>
											<option value="<?php echo $pack->getPackageProductId(); ?>"><?php echo $pack->getPackageProductName(); ?></option>
										<?php } ?>
									</select>
									<?php }else{echo'Package is Empty';}}else{echo"Package is Empty";} ?>
							</span>				
						</div>
					</div> -->


				<div class="row full_width shop_bdy">
					<!-- <div class="col-md-3 prd_brdr">
						<img src="<?php //echo base_url('attachments/') ?>toys.jpg" class="prd_img">
						<p >Name : <span class="prd_name">asdasdasda</span></p>
						<p class="">ShortDesc :  <span class="prd_shrt_desc">asdasdasda</span></p>
						<p class=""> Price <span class="prd_price">110</span></p>
					</div>
					<div class="rr full_width">
						<p class="avail_bal">Available Balance : <span class="avail_bal_text">1103.00</span></p>
						<p class="tot_bal">Total : <span class="tot_bal_text">1103.00</span></p>
					</div> -->
				</div>


				
				
			</div>
			<div id="#" class="tab-panee">
				<!-- <div class="shopin_cart" onclick="$(this).closest('form').submit();"><i class="fa fa-cart-arrow-down font_cart"></i>Shopping Now</div> -->
			</div>
			<div class="text-center arrow_sect">
				<div class="next_but"><a href="#"><i class="fa fa-arrow-circle-o-left font_arrow"></i>Prev </a></div>
				<div class="next_but"><a onclick="$(this).closest('form').submit();">Next <i class="fa fa-arrow-circle-o-right font_arrow"></i></a></div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$('.pack_prd_id').change(function(){
		var id = $(this).val();
		if(id > 0){
			$.ajax({
		        type: "POST",
		        url: "<?php echo base_url(); ?>registration/get_products_from_pack_prd_id",
		        data: {'id':id},
		          success:  function (data) {
		          	var tot=0;
		            var option = JSON.parse(data);
            		console.log(option);
            		for(i=0;i<option[0].length;i++){

            			tot = tot+parseInt(option[3][i]);
            			if(option[4][i] == ""){
            				var url = 'default_product.jpg';
            			}else{var url = option[4][i];}

					var xx = "<div class='col-md-3 prd_brdr'> <img src='<?php echo base_url('attachments/') ?>"+url+"' class='prd_img'> <p >Name : <span class='prd_name'>"+option[0][i]+"</span></p> <p class=''> Price <span class='prd_price'>"+option[3][i]+"</span></p> <p class=''>ShortDesc :  <span class='prd_shrt_desc'>"+option[1][i]+"</span></p> </div>";
					$('.shop_bdy').append(xx);



            			// console.log('Product_name : '+)
            			// console.log('Product_short_desc :  '+)
            			// console.log('Product_desc : '+option[2][i])
            			// console.log('Product_price : '+)
            		}
            		var yy = "<div class='rr full_width'><p class='avail_bal'>Price : <span class='avail_bal_text'>"+option[7]+" Rs</span></p><p class='tot_bal'>Available Balance : <span class='tot_bal_text'>"+option[6]+" </span></p></div>";
            		$('.shop_bdy').append(yy);
            		// console.log(tot);
		          }
	      	});
		}else{
			$('.shop_bdy').html("");
		}
	})
</script>