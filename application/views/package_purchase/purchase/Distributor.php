<div class="dkbody">
  <div class="container-fluid">
    <div class="page-header">
      <h3>Distributor List</h3>
    </div>
    
    <div class="rr">
      <div class="col-lg-3 col-md-3 pull-left pr-0 pl-0">
        <div style="">
          <select class="user_id form-control" style="" name="user_id"></select>
        </div>
      </div>

      <div class="col-lg-3 col-md-3 pull-left pr-0 pl-0">
        <div style="">
          <select class="user_name form-control" style="" name="user_name"></select>
        </div>
      </div>
      <div class="col-md-2 pull-left form-group" style="margin-top: -45px;">
        <span class="input input--nao" id="sandbox-container">
          <input type="text" class="input__field input__field--nao form-control dtt1"  name="dob" value="" 
           data-validation="date" />
           <!-- data-validation-format="yyyy-mm-dd" -->
          <label class="input__label input__label--nao" for="input-1">
            <span class="input__label-content input__label-content--nao">From</span>
          </label>
          <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
          </svg>
        </span> 
      </div>
      
      <div class="col-md-2 pull-left form-group" style="margin-top: -45px;">
        <span class="input input--nao" id="sandbox-container">
          <input type="text" class="input__field input__field--nao form-control dtt2"  name="dob" value="" 
           data-validation="date" />
           <!-- data-validation-format="yyyy-mm-dd" -->
          <label class="input__label input__label--nao" for="input-1">
            <span class="input__label-content input__label-content--nao">To</span>
          </label>
          <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
          </svg>
        </span> 
      </div>
        <button type="button" class="btn btn_row1  btn-7 btn-7a icon-truck mb-5" id="date-search" style="margin-top: -15px;"> <i class="fa fa-search" aria-hidden="true"></i></button>
    </div>

    <div class="page-content">                   
      <div class="container-fluid" style="margin-bottom: 2em">
        <table  id="gj_tree_struct_vw" class="table table-striped table-bordered table-responsive data_table_style" cellspacing="10"  style="width: 100%;">
          <thead>
            <tr>
              <th class="text-center">#</th>
              <th class="text-center" >Join Date</th>
              <th class="text-center" >Id</th>
              <th class="text-center" >User-Name</th>
              <th class="text-center" >City</th>
              <th class="text-center" >District</th>
              <th class="text-center" >Mobile</th>
              <th class="text-center" >Placement Id</th>
              <th class="text-center">Placement Name</th>
              <th class="text-center">Package</th>
              <th class="text-center">Option</th>
            </tr>
          </thead>
          <tbody style="text-align: center;" class="my_tab_bdy">
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<button type="button" class="btn btn-info btn-lg first_mdl" data-toggle="modal" data-target="#myModal" style="visibility: hidden;">Open Modal</button>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
        
      <div class="modal-body gj_whole_tree">
        <div class="ldr_tb_whole">
          <img src="<?php echo base_url('assets/images/loader/Rolling.gif'); ?>">
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<button type="button" class="btn btn-info btn-lg second_mdl" data-toggle="modal" data-target="#myModal2" style="visibility: hidden;">Open Modal</button>
<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
        
      <div class="modal-body user_edit">
        <div class="ldr_tb_whole">
          <img src="<?php echo base_url('assets/images/loader/Rolling.gif'); ?>">
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('.user_id').select2({
    placeholder: 'Search With User Id',
    allowClear: true,
    ajax: {
      url: '<?php echo base_url("index.php/common_controller/get_all_user_id") ?>',
      dataType: 'json',
      delay: 1,
      data: function (params) {
            return { q: params.term 
            };
      },
      processResults: function (data) { return { results: data }; },
      cache: true
    },
  });
  $('.user_name').select2({
    placeholder: 'Search With User Name',
    allowClear: true,
    ajax: {
      url: '<?php echo base_url("index.php/common_controller/get_all_first_name") ?>',
      dataType: 'json',
      delay: 1,
      data: function (params) {
            return { rs:1,q: params.term 
            };
      },
      processResults: function (data) { return { results: data }; },
      cache: true
    },
  });
  tab = $('#gj_tree_struct_vw').DataTable();
  document.querySelector("#date-search").addEventListener('click',function(){
    dt1 = $('.dtt1').val();
    dt2 = $('.dtt2').val();
    user_name = $('.user_name').val();
    user_id = $('.user_id').val();
    if(dt1.length>0 || dt2.length>0 ||user_name!=null || user_id != null ){
      tab.destroy();
      tab = $('#gj_tree_struct_vw').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
          "url": '<?php echo base_url(); ?>index.php/package_purchase/datatable_distributor_list',
           "data": {'dt1':dt1,'dt2':dt2,'user_name':user_name,'user_id':user_id},
        },
      });
    }
  })
  function get_tree(id){
    $('.first_mdl').click();
    $('.ldr_tb_whole').css({'display':'block'});
    $.ajax({
      type:'POST',
      url:"<?php echo base_url('index.php/package_purchase/get_next_node_ajax') ?>",
      data:{'id':id},
            success:  function (data) {
              var option = JSON.parse(data);
              $('.gj_whole_tree').html('');
              $('.gj_whole_tree').html(option);
              $('.ldr_tb_whole').css({'display':'none'});
            }
    })
  }
  function get_next_node(id){
    $('.ldr_tb_whole').css({'display':'block'});
    $.ajax({
      type:'POST',
      url:"<?php echo base_url('index.php/package_purchase/get_next_node_ajax') ?>",
      data:{'id':id},
            success:  function (data) {
              var option = JSON.parse(data);
              $('.gj_whole_tree').html('');
              $('.gj_whole_tree').html(option);
              $('.ldr_tb_whole').css({'display':'none'});
            }
    })
  }
  function view_profile(id){
    $('.page-content.gj_user_all').addClass('svk_cnt');
    $('.second_mdl').click();
    $('.ldr_tb_whole').css({'display':'block'});
    $.ajax({
      type:'POST',
      url:"<?php echo base_url('index.php/user/user_crud/user_ajax_edit_view') ?>",
      data:{'user_id':id},
            success:  function (data) {
              // var option = JSON.parse(data);
              $('.user_edit').html('');
              $('.user_edit').html(data);
              $('.ldr_tb_whole').css({'display':'none'});
            }
    })
  }
</script>
<style type="text/css">
  .btn_row1 {
    background-color: #0290cc;
    color: white;
    border-radius: 0;
    padding: 7px 18px 8px 18px;
    font-size: 1.9rem;
    margin: 3rem 0px 0px 0rem;
  }
  .page-content section.content.bgcolor-1 form span.select2.select2-container.select2-container--default.select2-container--focus, span.select2.select2-container.select2-container--default.select2-container--below, span.select2.select2-container.select2-container--default{
    width: 230px !important;
    margin: 0px !important;
  }
  .modal-dialog {
      min-width: 80%;
      max-width: 80%;
      width: 80%;
      margin-top: 12%;
  }
  .ldr_tb_whole {
      width: 97%;
      position: absolute;
      background: #fff;
      display: block;
      text-align: center;
      height: 100%;
      z-index: 2;
  }
  .ldr_tb_whole img {
      top: 40%;
      position: absolute;
  }
  .row.gj_row_bot {
      margin-top: 2em;
  }
  .modal-content {
      min-height: 500px;
  }
  
  .page-content.gj_user_all.svk_cnt {
      margin-top: 0%;
  }
  .modal-body.user_edit {
      margin-top: 25%;
  }
  .country_width_100.col-12.mt-5{
    display: none;
  }

</style>

<script type="text/javascript">
  $(document).ready(function() {
    $(".js-example-basic-single.select2_style.ad_usr.country").select2({
      placeholder: "Country",
    });
  });
  $("#gj_sel2_country").val('').trigger('change');

  $(document).ready(function() {
    $("#gj_sel2_pincode").select2({
      placeholder: "Pincode",
    });
  });
  $("#gj_sel2_pincode").val('').trigger('change');

  $(document).ready(function() {
      $("#gj_sel2_relation").select2({
        placeholder: "Relation",
      });
  });
  $("#gj_sel2_relation").val('').trigger('change');

  $(document).on('blur','input',function(){
    if($(this).val().length>=1){
      $(this).parent().addClass('input--filled');
    }
  });
</script>
<script>
  document.getElementById('files').addEventListener('change', handleFileSelect, false);
</script>

<script> /* Identity uploads */
  function handleFileSelect(evt) {
      var files = evt.target.files; // FileList object
      // Loop through the FileList and render image files as thumbnails.
      for (var i = 0, f; f = files[i]; i++) {
          // Only process image files.
          if (!f.type.match('image.*')) {
            continue;
          }
        var reader = new FileReader();
          // Closure to capture the file information.
          reader.onload = (function(theFile) {
        console.log(evt.target.id);
            return function(e) {
                // Render thumbnail.
                if(evt.target.id=="gj_file11"){
                  $('.ifile').prepend('<div class="gj_absolute p1-0 mr-2 mb-2 bdrstyle_hover" style="float:left"><img class="width-60px bdrstyle" src="'+e.target.result+'"><span>X</span></div>');

                    $('.bdrstyle_hover>span').on('click',function(){
                        $(this).parent().remove();
                  });
                }else  if(evt.target.id=="gj_file44"){
                  $('.yfile').prepend('<div class="gj_absolute p1-0 mr-2 mb-2 bdrstyle_hover" style="float:left"><img class="width-60px bdrstyle" src="'+e.target.result+'"><span>X</span></div>');

                    $('.bdrstyle_hover>span').on('click',function(){
                        $(this).parent().remove();
                  });
                }else  if(evt.target.id=="gj_file55"){
                  $('.xfile').prepend('<div class="gj_absolute p1-0 mr-2 mb-2 bdrstyle_hover" style="float:left"><img class="width-60px bdrstyle" src="'+e.target.result+'"><span>X</span></div>');

                    $('.bdrstyle_hover>span').on('click',function(){
                        $(this).parent().remove();
                  });
                }
            };
          })(f);
          // Read in the image file as a data URL.
          reader.readAsDataURL(f);
      }
    }

   $('.bdrstyle_hover>span').on('click',function(){
       $(this).parent().remove();
    });

    document.getElementById('gj_file11').addEventListener('change', handleFileSelect, false);
    document.getElementById('gj_file44').addEventListener('change', handleFileSelect, false);
    document.getElementById('gj_file55').addEventListener('change', handleFileSelect, false);

</script>
<script type="text/javascript">
  $( function() {
    $('.bank_ifsc').keyup();
  });
  $( function() {
    $('.pincode_no').keyup();
  });

  $('.bank_ifsc').keyup(function(){
    var id=$(this).val();
    $.post("<?php echo base_url('index.php/common_controller/get_ifsc'); ?>",
    {key:id},
    function(data,status){
      if(status=="success"){
        if(data==''){
          console.log(data)
        }else{
          var item=JSON.parse(data);//console.log(item)
          $(".bank_ifsc").autocomplete({
            source: item,
            minLength: 1,
          create: function () {
              $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
                return $('<li>')
                  .append('<a>'+ item.label +'</a>')
                  .appendTo(ul);
              };
          },
          select:function(event,ui){
                event.preventDefault();
                $(".bank_ifsc").val(ui.item.data_id);
                $('.bank_ifsc').val(ui.item.value).trigger('change');
                $('.bank_ifsc_hid').val(ui.item.data_id);
             }
            });
        }
        }
    });
  });
  $(".bank_ifsc").blur(function(){
    id = $('.bank_ifsc_hid').val()
    if(id != 0 || id != null){
      $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>index.php/Common_controller/get_bank_details_from_bank",
          data: {'id':id},
            success:  function (data) {
              if(data != null){
                var option = JSON.parse(data);
                //console.log(option);
                $('.bank_name').val(option['bank_name']);
                $('.bank_branch').val(option['bank_branch']);
                $('.bank_branch_lbl').addClass('input--filled');
              }
            }
        })
    }
  })

  $('.pincode_no').keyup(function(){
    var id=$(this).val();
    $.post("<?php echo base_url('index.php/common_controller/get_pincode'); ?>",
    {key:id},
    function(data,status){
      if(status=="success"){
        if(data==''){
          console.log(data)
        }else{
          var item=JSON.parse(data);//console.log(item)
          $(".pincode_no").autocomplete({
            source: item,
            minLength: 1,
          create: function () {
              $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
                return $('<li>')
                  .append('<a>'+ item.label +'</a>')
                  .appendTo(ul);
              };
          },
          select:function(event,ui){
                event.preventDefault();
                $(".pincode_no").val(ui.item.data_id);
                $('.pincode_no').val(ui.item.value).trigger('change');
                $('.pincode_id_hid').val(ui.item.data_id);
             }
            });
        }
        }
    });
  });
  $(".pincode_no").blur(function(){
    id = $(this).val()
    if(id > 0){
      $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>index.php/Common_controller/get_pincode_details",
          data: {'id':id},
            success:  function (data) {
              if(data != 0){
                if(data != null){
                  var option = JSON.parse(data);
                  //console.log(option);
                  $('.district').val(option['district']);
                  $('.state').val(option['state']);
                }else{
                  $('.pincode_id_hid').val(0);
                  $('.district').val('');
                  $('.state').val('');
                }
              }else{
                $('.pincode_id_hid').val(0);
                $('.district').val('');
                $('.state').val('');
              }
              
            }
        })
    }
  })

</script>

<script type="text/javascript">
  $.validate({
      form:'.gj_form',
    });

  $('.pesonal_but').click(function(){
    gender = $('input[name=gender]:checked').val();
    first_name = $('.first_name').val();
    last_name = $('.last_name').val();
    user_email = $('.user_email').val();
    dob = $('.dob').val();
    primary_mobile = $('.primary_mobile').val();
    secondary_mobile = $('.secondary_mobile').val();
    id = $('.edit_user_id').val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url('index.php/common_controller/update_user_from_pers_but'); ?>",
      data: {'first_name':first_name,'last_name':last_name,'user_email':user_email,'gender':gender,'dob':dob,'primary_mobile':primary_mobile,'secondary_mobile':secondary_mobile,'id':id},
      success: function(data) {
        if(data != 0){
          res = JSON.parse(data)
          if(res['status'] == 1){
            toast({
              message: "Personal Details Success",
              displayLength: 3000,
              className: 'success',
            });
          }else if(res['status'] == -1){
            toast({
              message: "User Fields Already Exists",
              displayLength: 3000,
              className: 'error',
            });
          }else{
            console.log(res);
            for (var key in res['array']) {
              $('.error.'+key).text(res['array'][key]);
              // console.log("key " + key + " has value " + res['array'][key]);
            }
          }
        }else{
          toast({
            message: "Invalid User",
            displayLength: 3000,
            className: 'error',
          });
        }
      }
    })
  })
  $('.bank_but').click(function(){
    account_no = $('.account_no').val();
    bank_ifsc = $('.bank_ifsc').val();
    bank_name = $('.bank_name').val();
    bank_branch = $('.bank_branch').val();
    id = $('.edit_user_id').val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url('index.php/common_controller/update_bank_details_from_bk_but'); ?>",
      data: {'account_no':account_no,'bank_ifsc':bank_ifsc,'bank_name':bank_name,'bank_branch':bank_branch,'id':id},
      success: function(data) {
        if(data != 0){
          res = JSON.parse(data)
          if(res['status'] == 1){
            toast({
              message: "Successfully Updated",
              displayLength: 3000,
              className: 'success',
            });
          }else if(res['status'] == -1){
            toast({
              message: "Update Failed",
              displayLength: 3000,
              className: 'error',
            });
          }else{
            console.log(res);
            for (var key in res['array']) {
              $('.error.'+key).text(res['array'][key]);
              // console.log("key " + key + " has value " + res['array'][key]);
            }
          }
        }else{
          toast({
            message: "Invalid Bank",
            displayLength: 3000,
            className: 'error',
          });
        }
      }
    })
  })
  $('.communication_but').click(function(){
    full_address = $('.full_address').val();
    pincode_no = $('.pincode_no').val();
    district = $('.district').val();
    state = $('.state').val();
    country = $('.country').val();
    id = $('.edit_user_id').val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url('index.php/common_controller/update_communication_details_from_cm_but'); ?>",
      data: {'full_address':full_address,'pincode_no':pincode_no,'district':district,'state':state,'country':country,'id':id},
      success: function(data) {
        if(data != 0){
          res = JSON.parse(data)
          if(res['status'] == 1){
            toast({
              message: "Successfully Updated",
              displayLength: 3000,
              className: 'success',
            });
          }else if(res['status'] == -1){
            toast({
              message: "Update Failed",
              displayLength: 3000,
              className: 'error',
            });
          }else{
            console.log(res);
            for (var key in res['array']) {
              $('.error.'+key).text(res['array'][key]);
              // console.log("key " + key + " has value " + res['array'][key]);
            }
          }
        }else{
          toast({
            message: "Invalid Bank",
            displayLength: 3000,
            className: 'error',
          });
        }
      }
    })
  })
  $('.nominee_but').click(function(){
    nominee_name = $('.nominee_name').val();
    n_mobile = $('.n_mobile').val();
    relation = $('.relation').val();
    n_dob = $('.n_dob').val();
    n_gender = $('input[name=n_gender]:checked').val();
    id = $('.edit_user_id').val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url('index.php/common_controller/update_nominee_details_from_no_but'); ?>",
      data: {'nominee_name':nominee_name,'n_mobile':n_mobile,'relation':relation,'n_dob':n_dob,'n_gender':n_gender,'id':id},
      success: function(data) {
        if(data != 0){
          res = JSON.parse(data)
          if(res['status'] == 1){
            toast({
              message: "Successfully Updated",
              displayLength: 3000,
              className: 'success',
            });
          }else if(res['status'] == -1){
            toast({
              message: "Update Failed",
              displayLength: 3000,
              className: 'error',
            });
          }else{
            console.log(res);
            for (var key in res['array']) {
              $('.error.'+key).text(res['array'][key]);
              // console.log("key " + key + " has value " + res['array'][key]);
            }
          }
        }else{
          toast({
            message: "Invalid Bank",
            displayLength: 3000,
            className: 'error',
          });
        }
      }
    })
  })
  $('.pass_but').click(function(){
    id = $('.edit_user_id').val();
    pwd = $('.pwd').val();
    pin_pwd = $('.pin_pwd').val();
    if(pwd.length>4 && pin_pwd.length>4){
      $.ajax({
        type: "POST",
        url: "<?php echo base_url('index.php/common_controller/update_pass_details_from_pas_but'); ?>",
        data: {'pwd':pwd,'pin_pwd':pin_pwd,'id':id},
        success: function(data) {
          if(data != 0){
            res = JSON.parse(data)
            if(res['status'] == 1){
              toast({
                message: "Successfully Updated",
                displayLength: 3000,
                className: 'success',
              });
            }else if(res['status'] == -1){
              toast({
                message: "Update Failed",
                displayLength: 3000,
                className: 'error',
              });
            }else{
              console.log(res);
              for (var key in res['array']) {
                $('.error.'+key).text(res['array'][key]);
                // console.log("key " + key + " has value " + res['array'][key]);
              }
            }
          }else{
            toast({
              message: "Invalid Bank",
              displayLength: 3000,
              className: 'error',
            });
          }
        }
      })
    }else{
      toast({
        message: "Min Length 4",
        displayLength: 2000,
        className: 'error',
      });
    }
  })

  $(".pesonal_but_x").on("click", function() {
        $(".gj_form").submit();
  });
  $(".identity_but_x").on("click", function() {
        $(".gj_identity").submit();
  });

  $('.gj_form').on('submit',(function(e) {
    e.preventDefault()
     var formData = new FormData(this);
    $.ajax({
        type:'POST',
        url: "<?php echo base_url('index.php/common_controller/update_user_aj_with_prof'); ?>",
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        success:function(data){
            if(data != 0){
            res = JSON.parse(data)
            if(res['status'] == 1){
              toast({
                message: "Successfully Updated",
                displayLength: 3000,
                className: 'success',
              });
            }else if(res['status'] == -1){
              toast({
                message: "Update Failed",
                displayLength: 3000,
                className: 'error',
              });
            }else{
              console.log(res);
              for (var key in res['array']) {
                $('.error.'+key).text(res['array'][key]);
                // console.log("key " + key + " has value " + res['array'][key]);
              }
            }
          }else{
            toast({
              message: "Invalid Bank",
              displayLength: 3000,
              className: 'error',
            });
          }
        },
       
    });
}));

  $('.gj_identity').on('submit',(function(e) {
    e.preventDefault()
     var formData = new FormData(this);
    $.ajax({
        type:'POST',
        url: "<?php echo base_url('index.php/common_controller/update_attach_aj_with'); ?>",
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        success:function(data){
            if(data != 0){
            res = JSON.parse(data)
            if(res['status'] == 1){
              toast({
                message: "Successfully Updated",
                displayLength: 3000,
                className: 'success',
              });
            }else if(res['status'] == -1){
              toast({
                message: "Update Failed",
                displayLength: 3000,
                className: 'error',
              });
            }else{
              console.log(res);
              for (var key in res['array']) {
                $('.error.'+key).text(res['array'][key]);
                // console.log("key " + key + " has value " + res['array'][key]);
              }
            }
          }else{
            toast({
              message: "Invalid Bank",
              displayLength: 3000,
              className: 'error',
            });
          }
        },
       
    });
}));
</script>