<div class="dkbody">
  <div class="container-fluid">
    <div class="page-header gj_new_pack">
      <h3>Distributor Details View</h3>
    </div>
    <p class="gj_package_dets"><span><i class="fa fa-cubes"></i></span> Distributor Details</p>
    <div class="row">
      <div class="col-md-12 col-lg-12 gj_tree_struct">
          <div class="gj_tree_div text-center">
            <form method="post" action="<?php echo base_url('index.php/package_purchase/genology_search'); ?>">
              <input type="hidden" value="hid_gene" name="hid_gene">
              <div class="rowx rrx" style="margin-bottom: 1em">
                <div class="col-lg-4 col-md-4 pull-left pr-0 pl-0">
                  <div style="">
                    <select class="user_id form-control" style="" name="user_id"></select>
                  </div>
                </div>

                <div class="col-lg-4 col-md-4 pull-left pr-0 pl-0">
                  <div style="">
                    <select class="user_name form-control" style="" name="user_name"></select>
                  </div>
                </div>

                <div class="col-lg-3 col-md-3 pull-left pr-0 text-left">
                  <button type="submit" id="gj_dist_search" class="gj_dist_view" style="margin-top: 25px"><i class="fa fa-search gj_icon_srh"></i> SEARCH</button>    
                </div>
              </div>
            </form> 
          </div>
      </div>
    </div>
  
    <div class="gj_whole_tree">
      <div class="ldr_tb_whole">
        <img src="<?php echo base_url('assets/images/loader/Rolling.gif'); ?>">
      </div>

      <div class="row rr" style="margin-top: 2em;">
        <div class="col-lg-6 col-md-6 pull-left">
          <div class="gj_tree_l_count">
            <span class="gj_tree_left_cnt">Total Left Side Count : <span class="gj_l_cnt_no"><?php echo $left_count; ?></span></span>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 pull-left">
          <div class="gj_tree_r_count">
            <span class="gj_tree_right_cnt">Total Right Side Count : <span class="gj_r_cnt_no"><?php echo $right_count; ?></span></span>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="gj_first_tree gj_tree_vws" onmouseenter="get_tbl(<?php echo $obj->getPurchaseId(); ?>)">
            <?php if($_SESSION['role_id'] == RT::$deafault_admin_role_id){ ?>
              <p><?php if($obj->getUserId() != null){echo $obj->getUserId();} ?></p>
            <?php }else{ ?>
              <p></p>
            <?php } ?>
            <?php if($obj->getImage() != 0){ ?>
              <img src="<?php echo base_url($obj->getImage()); ?>" alt="tree">
            <?php }else{ ?>
              <img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree">
            <?php } ?>

            <p><?php if($obj->getUserName() != null){echo $obj->getUserName();} ?></p>
            <p><?php if($obj->getFirstName() != null){echo $obj->getFirstName();} ?></p>
            <p class="fa fa-clock-o"><?php if($obj->getDate() != null){echo $obj->getDate()->format('Y-m-d H:i:s');} ?></p>
            <input type="hidden" name="iid" class="iid" value="<?php echo $obj->getPurchaseId(); ?>">

              <!-- <div class="gj_tree_table_vw">
                <div class="gj_cutoff_table gj_tree_resp table-responsive">
                  <div class="ldr_tb">
                    <img src="<?php echo base_url('assets/images/loader/Rolling.gif'); ?>">
                  </div>
                  <table id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="0" width="100%">
                      <thead>
                          <tr>
                            <th class="text-center gj_tree_downline" colspan="9">Downline BV Details For 00001</th>
                          </tr>
                          <tr>
                            <th class="text-center" rowspan="2">Package Name</th>
                            <th class="text-center" colspan="2">Total</th>
                            <th class="text-center" colspan="2">Personal</th>
                            <th class="text-center" colspan="2">Current</th>
                            <th class="text-center" colspan="2">Carry Forward</th>
                          </tr>
                          <tr>
                            <th class="text-center">Left</th>
                            <th class="text-center">Right</th>
                            <th class="text-center">Left</th>
                            <th class="text-center">Right</th>
                            <th class="text-center">Left</th>
                            <th class="text-center">Right</th>
                            <th class="text-center">Left</th>
                            <th class="text-center">Right</th>
                          </tr>
                      </thead>
                      <tbody class="pck_bdy_<?php echo $obj->getPurchaseId(); ?>">
                          
                      </tbody>
                  </table>
                </div>
              </div> -->
            
          </div>
          
          <!-- <hr class="gj_tree_hr"/> -->
        </div>
      </div>


    <?php //echo"<pre>";print_r($obj->getRightObj());echo"</pre>"; exit();?>

      <div class="row gj_row_bot">
        <div class="col-md-6 col-lg-6 gj_tree_vw1_struct">
          <?php if($obj->getLeftObj() != null){ ?>
            <div class="gj_first_tree gj_tree_vws" onmouseenter="get_tbl(<?php echo $obj->getLeftObj()->getPurchaseId(); ?>)" onclick="get_next_node(<?php echo $obj->getLeftObj()->getPurchaseId(); ?>)">

              <?php if($_SESSION['role_id'] == RT::$deafault_admin_role_id){ ?>
              <p><?php echo $obj->getLeftObj()->getUserId(); ?></p>
              <?php }else{echo"<p></p>";} ?>
              <img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree">
              <p><?php echo $obj->getLeftObj()->getUserName(); ?></p>
              <p><?php echo $obj->getLeftObj()->getFirstName(); ?></p>
              <p class="fa fa-clock-o"><?php echo $obj->getLeftObj()->getDate()->format('Y-m-d H:i:s'); ?></p>
                <!-- <div class="gj_tree_table_vw">
                  <div class="gj_cutoff_table gj_tree_resp table-responsive">
                    <div class="ldr_tb">
                      <img src="<?php echo base_url('assets/images/loader/Rolling.gif'); ?>">
                    </div>
                    <table id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="0" width="100%">
                      <thead>
                          <tr>
                            <th class="text-center gj_tree_downline" colspan="9">Downline BV Details For 00001</th>
                          </tr>
                          <tr>
                            <th class="text-center" rowspan="2">Package Name</th>
                            <th class="text-center" colspan="2">Total</th>
                            <th class="text-center" colspan="2">Personal</th>
                            <th class="text-center" colspan="2">Current</th>
                            <th class="text-center" colspan="2">Carry Forward</th>
                          </tr>
                          <tr>
                            <th class="text-center">Left</th>
                            <th class="text-center">Right</th>
                            <th class="text-center">Left</th>
                            <th class="text-center">Right</th>
                            <th class="text-center">Left</th>
                            <th class="text-center">Right</th>
                            <th class="text-center">Left</th>
                            <th class="text-center">Right</th>
                          </tr>
                      </thead>
                      <tbody class="pck_bdy_<?php echo $obj->getLeftObj()->getPurchaseId(); ?>">
                          
                      </tbody>
                    </table>
                  </div>
                </div> -->
              <input type="hidden" name="iid" class="iid" value="<?php echo $obj->getLeftObj()->getPurchaseId(); ?>">
            </div>
            <!-- <hr class="gj_tree_hr"/> -->
          <?php }else{ ?> 
            <div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
              <div class="gj_tree_id_div gj_tree_id">-----</div>
              <img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
              <?php if($_SESSION['role_id'] == RT::$deafault_admin_role_id){ ?>
              <p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
              <?php }else{echo"<p></p>";} ?>
              <?php if($obj->getPurchaseId() != null){?>
                <form method="post" action="<?php echo base_url('index.php/registration/cus_select_enter_pin'); ?>" class="subrt_cls">
                     <a onclick="$(this).closest('form').submit()">Add New User</a>
                    <input type="hidden" name="top_id" value="<?php echo $obj->getUserId(); ?>">
                     <input type="hidden" name="pos" value="0">
                </form>
              <?php } ?>
            </div>
          <?php } ?>
        </div>
        <div class="col-md-6 col-lg-6 gj_tree_vw1_struct">
          <?php if($obj->getRightObj() != null){ ?>
            <div class="gj_first_tree gj_tree_vws" onmouseenter="get_tbl(<?php echo $obj->getRightObj()->getPurchaseId(); ?>)" onclick="get_next_node(<?php echo $obj->getRightObj()->getPurchaseId(); ?>)">
              <?php if($_SESSION['role_id'] == RT::$deafault_admin_role_id){ ?>
              <p><?php echo $obj->getRightObj()->getUserId(); ?></p>
              <?php }else{echo"<p></p>";} ?>
              <img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree">
              <p><?php echo $obj->getRightObj()->getUserName(); ?></p>
              <p><?php echo $obj->getRightObj()->getFirstName(); ?></p>
              <p class="fa fa-clock-o"><?php echo $obj->getRightObj()->getDate()->format('Y-m-d H:i:s'); ?></p>
              <!-- <div class="gj_tree_table_vw" style="top: auto; left: auto; z-index: 99; bottom: 60px; right: 0;">
                <div class="gj_cutoff_table gj_tree_resp table-responsive">
                    <div class="ldr_tb">
                      <img src="<?php echo base_url('assets/images/loader/Rolling.gif'); ?>">
                    </div>
                    <table id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                          <th class="text-center gj_tree_downline" colspan="9">Downline BV Details For 00001</th>
                        </tr>
                        <tr>
                          <th class="text-center" rowspan="2">Package Name</th>
                          <th class="text-center" colspan="2">Total</th>
                          <th class="text-center" colspan="2">Personal</th>
                          <th class="text-center" colspan="2">Current</th>
                          <th class="text-center" colspan="2">Carry Forward</th>
                        </tr>
                        <tr>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                        </tr>
                    </thead>
                    <tbody class="pck_bdy_<?php echo $obj->getRightObj()->getPurchaseId(); ?>">
                       
                    </tbody>
                  </table>
                </div>
              </div> -->
              <input type="hidden" name="iid" class="iid" value="<?php echo $obj->getRightObj()->getPurchaseId(); ?>">
            </div>
            <!-- <hr class="gj_tree_hr"/> -->
          <?php }else{ ?>  

          <div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
              <div class="gj_tree_id_div gj_tree_id">-----</div>
              <img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
              <p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
              <?php if($obj->getPurchaseId() != null){?>
                <form method="post" action="<?php echo base_url('index.php/registration/cus_select_enter_pin'); ?>" class="subrt_cls">
                     <a onclick="$(this).closest('form').submit()">Add New User</a>
                    <input type="hidden" name="top_id" value="<?php echo $obj->getUserId(); ?>">
                     <input type="hidden" name="pos" value="1">
                </form>
              <?php } ?>
            </div>

          <?php } ?>
        </div>
      </div>

      <div class="row gj_row_bot">
        <div class="col-md-3 col-lg-3 gj_tree_vw1_struct">
          <?php if($obj->getLeftObj() != null){ ?>
            <?php if($obj->getLeftObj()->getLeftObj() != null){ ?>
              <div class="gj_first_tree gj_tree_vws" onclick="get_next_node(<?php echo $obj->getLeftObj()->getLeftObj()->getPurchaseId(); ?>)" onmouseenter="get_tbl(<?php echo $obj->getLeftObj()->getLeftObj()->getPurchaseId(); ?>)">
                <?php if($_SESSION['role_id'] == RT::$deafault_admin_role_id){ ?>
                <p><?php echo $obj->getLeftObj()->getLeftObj()->getUserId(); ?></p>
                <?php }else{echo"<p></p>";} ?>
                <img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree">
                <p><?php echo $obj->getLeftObj()->getLeftObj()->getUserName(); ?></p>
                <p><?php echo $obj->getLeftObj()->getLeftObj()->getFirstName(); ?></p>
                <p class="fa fa-clock-o"><?php echo $obj->getLeftObj()->getLeftObj()->getDate()->format('Y-m-d H:i:s'); ?></p>
                <!-- <div class="gj_tree_table_vw">
                  <div class="gj_cutoff_table gj_tree_resp table-responsive">
                    <div class="ldr_tb">
                      <img src="<?php echo base_url('assets/images/loader/Rolling.gif'); ?>">
                    </div>
                    <table id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                          <th class="text-center gj_tree_downline" colspan="9">Downline BV Details For 00001</th>
                        </tr>
                        <tr>
                          <th class="text-center" rowspan="2">Package Name</th>
                          <th class="text-center" colspan="2">Total</th>
                          <th class="text-center" colspan="2">Personal</th>
                          <th class="text-center" colspan="2">Current</th>
                          <th class="text-center" colspan="2">Carry Forward</th>
                        </tr>
                        <tr>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                        </tr>
                    </thead>
                    <tbody class="pck_bdy_<?php echo $obj->getLeftObj()->getLeftObj()->getPurchaseId(); ?>">
                       
                    </tbody>
                  </table>
                  </div>
                </div> -->
                <input type="hidden" name="iid" class="iid" value="<?php echo $obj->getLeftObj()->getLeftObj()->getPurchaseId(); ?>">
              </div>
              <!-- <hr class="gj_tree_hr"/> -->
            <?php }else{ ?> 
              <div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
                <div class="gj_tree_id_div gj_tree_id">-----</div>
                <img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
                <p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
                <?php if($obj->getLeftObj() != null){ ?>  
                    <form method="post" action="<?php echo base_url('index.php/registration/cus_select_enter_pin'); ?>" class="subrt_cls">
                         <a onclick="$(this).closest('form').submit()">Add New User</a>
                        <input type="hidden" name="top_id" value="<?php echo $obj->getLeftObj()->getUserId(); ?>">
                        <input type="hidden" name="pos" value="0">
                    </form>
                <?php } ?>
              </div>
            <?php } ?>
          <?php }else{ ?> 
            <div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
              <div class="gj_tree_id_div gj_tree_id">-----</div>
              <img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
              <p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
              <?php if($obj->getLeftObj() != null){ ?>  
                  <form method="post" action="<?php echo base_url('index.php/registration/cus_select_enter_pin'); ?>" class="subrt_cls">
                       <a onclick="$(this).closest('form').submit()">Add New User</a>
                      <input type="hidden" name="top_id" value="<?php echo $obj->getLeftObj()->getUserId(); ?>">
                      <input type="hidden" name="pos" value="0">
                  </form>
              <?php } ?>
            </div>
          <?php } ?>
        </div>
        <div class="col-md-3 col-lg-3 gj_tree_vw1_struct">
          <?php if($obj->getLeftObj() != null){ ?>
            <?php if($obj->getLeftObj()->getRightObj() != null){ ?>
              <div class="gj_first_tree gj_tree_vws" onclick="get_next_node(<?php echo $obj->getLeftObj()->getRightObj()->getPurchaseId(); ?>)" onmouseenter="get_tbl(<?php echo $obj->getLeftObj()->getRightObj()->getPurchaseId(); ?>)">
                <?php if($_SESSION['role_id'] == RT::$deafault_admin_role_id){ ?>
                <p><?php echo $obj->getLeftObj()->getRightObj()->getUserId(); ?></p>
                <?php }else{echo"<p></p>";} ?>
                <img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree">
                <p><?php echo $obj->getLeftObj()->getRightObj()->getUserName(); ?></p>
                <p><?php echo $obj->getLeftObj()->getRightObj()->getFirstName(); ?></p>
                <p class="fa fa-clock-o"><?php echo $obj->getLeftObj()->getRightObj()->getDate()->format('Y-m-d H:i:s'); ?></p>
                <!-- <div class="gj_tree_table_vw">
                  <div class="gj_cutoff_table gj_tree_resp table-responsive">
                      <div class="ldr_tb">
                      <img src="<?php echo base_url('assets/images/loader/Rolling.gif'); ?>">
                    </div>
                    <table id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                          <th class="text-center gj_tree_downline" colspan="9">Downline BV Details For 00001</th>
                        </tr>
                        <tr>
                          <th class="text-center" rowspan="2">Package Name</th>
                          <th class="text-center" colspan="2">Total</th>
                          <th class="text-center" colspan="2">Personal</th>
                          <th class="text-center" colspan="2">Current</th>
                          <th class="text-center" colspan="2">Carry Forward</th>
                        </tr>
                        <tr>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                        </tr>
                    </thead>
                    <tbody class="pck_bdy_<?php echo $obj->getLeftObj()->getRightObj()->getPurchaseId(); ?>">
                      
                    </tbody>
                  </table>
                  </div>
                </div> -->
                <input type="hidden" name="iid" class="iid" value="<?php echo $obj->getLeftObj()->getRightObj()->getPurchaseId(); ?>">
              </div>
              <hr class="gj_tree_hr"/>
            <?php }else{ ?> 
              <div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
                <div class="gj_tree_id_div gj_tree_id">-----</div>
                <img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
                <p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
                <?php if($obj->getLeftObj() != null){ ?>  
                    <form method="post" action="<?php echo base_url('index.php/registration/cus_select_enter_pin'); ?>" class="subrt_cls">
                         <a onclick="$(this).closest('form').submit()">Add New User</a>
                        <input type="hidden" name="top_id" value="<?php echo $obj->getLeftObj()->getUserId(); ?>">
                        <input type="hidden" name="pos" value="1">
                    </form>
                <?php } ?>
              </div>
            <?php } ?>
          <?php }else{ ?> 
            <div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
              <div class="gj_tree_id_div gj_tree_id">-----</div>
              <img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
              <p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
            </div>
          <?php } ?>
        </div>
        <div class="col-md-3 col-lg-3 gj_tree_vw1_struct">
          <?php if($obj->getRightObj() != null){ ?>
            <?php if($obj->getRightObj()->getLeftObj() != null){ ?>
              <div class="gj_first_tree gj_tree_vws" onclick="get_next_node(<?php echo $obj->getRightObj()->getLeftObj()->getPurchaseId(); ?>)" onmouseenter="get_tbl(<?php echo $obj->getRightObj()->getLeftObj()->getPurchaseId(); ?>)">
                <?php if($_SESSION['role_id'] == RT::$deafault_admin_role_id){ ?>
                <p><?php echo $obj->getRightObj()->getLeftObj()->getUserId(); ?></p>
                <?php }else{echo"<p></p>";} ?>
                <img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree">
                <p><?php echo $obj->getRightObj()->getLeftObj()->getUserName(); ?></p>
                <p><?php echo $obj->getRightObj()->getLeftObj()->getFirstName(); ?></p>
                <p class="fa fa-clock-o"><?php echo $obj->getRightObj()->getLeftObj()->getDate()->format('Y-m-d H:i:s'); ?></p>
                  <!-- <div class="gj_tree_table_vw" style="top: auto; left: auto; z-index: 99; bottom: 60px; right: 0;">
                    <div class="gj_cutoff_table gj_tree_resp table-responsive">
                        <div class="ldr_tb">
                          <img src="<?php echo base_url('assets/images/loader/Rolling.gif'); ?>">
                        </div>
                      <table id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th class="text-center gj_tree_downline" colspan="9">Downline BV Details For 00001</th>
                            </tr>
                            <tr>
                              <th class="text-center" rowspan="2">Package Name</th>
                              <th class="text-center" colspan="2">Total</th>
                              <th class="text-center" colspan="2">Personal</th>
                              <th class="text-center" colspan="2">Current</th>
                              <th class="text-center" colspan="2">Carry Forward</th>
                            </tr>
                            <tr>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                            </tr>
                        </thead>
                        <tbody class="pck_bdy_<?php echo $obj->getRightObj()->getLeftObj()->getPurchaseId(); ?>">
                          
                        </tbody>
                      </table>
                    </div>
                  </div> -->
                  <input type="hidden" name="iid" class="iid" value="<?php echo $obj->getRightObj()->getLeftObj()->getPurchaseId(); ?>">
              </div>
              <!-- <hr class="gj_tree_hr"/> -->
            <?php }else{ ?> 
              <div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
                <div class="gj_tree_id_div gj_tree_id">-----</div>
                <img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
                <p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
                <?php if($obj->getRightObj() != null){ ?>  
                    <form method="post" action="<?php echo base_url('index.php/registration/cus_select_enter_pin'); ?>" class="subrt_cls">
                         <a onclick="$(this).closest('form').submit()">Add New User</a>
                        <input type="hidden" name="top_id" value="<?php echo $obj->getRightObj()->getUserId(); ?>">
                        <input type="hidden" name="pos" value="0">
                    </form>
                <?php } ?>
              </div>
            <?php } ?>
          <?php }else{ ?> 
            <div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
              <div class="gj_tree_id_div gj_tree_id">-----</div>
              <img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
              <p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
            </div>
          <?php } ?>
        </div>
        <div class="col-md-3 col-lg-3 gj_tree_vw1_struct">
          <?php if($obj->getRightObj() != null){ ?>
            <?php if($obj->getRightObj()->getRightObj() != null){ ?>
              <div class="gj_first_tree gj_tree_vws" onclick="get_next_node(<?php echo $obj->getRightObj()->getRightObj()->getPurchaseId(); ?>)" onmouseenter="get_tbl(<?php echo $obj->getRightObj()->getRightObj()->getPurchaseId(); ?>)">
                <?php if($_SESSION['role_id'] == RT::$deafault_admin_role_id){ ?>
                <p><?php echo $obj->getRightObj()->getRightObj()->getUserId(); ?></p>
                <?php }else{echo"<p></p>";} ?>
                <img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree">
                <p><?php echo $obj->getRightObj()->getRightObj()->getUserName(); ?></p>
                <p><?php echo $obj->getRightObj()->getRightObj()->getFirstName(); ?></p>
                <p class="fa fa-clock-o"><?php echo $obj->getRightObj()->getRightObj()->getDate()->format('Y-m-d H:i:s'); ?></p>
                <!-- <div class="gj_tree_table_vw" style="top: auto; left: auto; z-index: 99; bottom: 60px; right: 0;">
                  <div class="gj_cutoff_table gj_tree_resp table-responsive">
                    <div class="ldr_tb">
                      <img src="<?php echo base_url('assets/images/loader/Rolling.gif'); ?>">
                    </div>
                      <table id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th class="text-center gj_tree_downline" colspan="9">Downline BV Details For 00001</th>
                            </tr>
                            <tr>
                              <th class="text-center" rowspan="2">Package Name</th>
                              <th class="text-center" colspan="2">Total</th>
                              <th class="text-center" colspan="2">Personal</th>
                              <th class="text-center" colspan="2">Current</th>
                              <th class="text-center" colspan="2">Carry Forward</th>
                            </tr>
                            <tr>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                            </tr>
                        </thead>
                        <tbody class="pck_bdy_<?php echo $obj->getRightObj()->getRightObj()->getPurchaseId(); ?>">
                          
                        </tbody>
                      </table>
                  </div>
                </div> -->
                <input type="hidden" name="iid" class="iid" value="<?php echo $obj->getRightObj()->getRightObj()->getPurchaseId(); ?>">
              </div>
              <!-- <hr class="gj_tree_hr"/> -->
            <?php }else{ ?> 
              <div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
                <div class="gj_tree_id_div gj_tree_id">-----</div>
                <img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
                <p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
                 <?php if($obj->getRightObj() != null){ ?>  
                    <form method="post" action="<?php echo base_url('index.php/registration/cus_select_enter_pin'); ?>" class="subrt_cls">
                         <a onclick="$(this).closest('form').submit()">Add New User</a>
                        <input type="hidden" name="top_id" value="<?php echo $obj->getRightObj()->getUserId(); ?>">
                        <input type="hidden" name="pos" value="1">
                    </form>
                <?php } ?>
              </div>
            <?php } ?>
          <?php }else{ ?> 
            <div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
              <div class="gj_tree_id_div gj_tree_id">-----</div>
              <img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
              <p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>

    <div class="country_width_100 col-12 mt-5">         
          <div class="country_width_100 mt-3 mb-3">                     
          <div class="country-right">
              <a href="<?php echo base_url('index.php/login/dashboard'); ?>"><button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button></a>
          </div>  
      </div>
    </div>
  </div>  
</div>

<style type="text/css">
  p.fa.fa-clock-o:before {
      padding-right: 5px;
  }
  .row{
    margin:25px 0px;
  }
  .table-striped>tbody>tr:nth-of-type(odd) {
      background-color: none !important;
  }
</style>

<?php if($_SESSION['role_id'] == RT::$deafault_admin_role_id) { ?>
  <script type="text/javascript">
     $('.user_id').select2({
    placeholder: 'Search With User Id',
    ajax: {
      url: '<?php echo base_url("index.php/common_controller/get_all_user_id") ?>',
      dataType: 'json',
      delay: 1,
      data: function (params) {console.log(params);
            return { q: params.term 
            };
      },
      processResults: function (data) { return { results: data }; },
      cache: true
    },
  });

  $('.user_name').select2({
    placeholder: 'Search With User Name',
    ajax: {
      url: '<?php echo base_url("index.php/common_controller/get_all_user_name") ?>',
      dataType: 'json',
      delay: 1,
      data: function (params) {console.log(params);
            return { rs:1,q: params.term 
            };
      },
      processResults: function (data) { return { results: data }; },
      cache: true
    },
  });
  </script>
  
<?php }else{ ?>
  <script type="text/javascript">
     $('.user_id').select2({
    placeholder: 'Search With User Id',
    ajax: {
      url: '<?php echo base_url("index.php/common_controller/get_sub_user_id") ?>',
      dataType: 'json',
      delay: 1,
      data: function (params) {console.log(params);
            return { q: params.term 
            };
      },
      processResults: function (data) { return { results: data }; },
      cache: true
    },
  });

  $('.user_name').select2({
    placeholder: 'Search With User Name',
    ajax: {
      url: '<?php echo base_url("index.php/common_controller/get_sub_user_id") ?>',
      dataType: 'json',
      delay: 1,
      data: function (params) {console.log(params);
            return { rs:1,q: params.term 
            };
      },
      processResults: function (data) { return { results: data }; },
      cache: true
    },
  });
  </script>
<?php } ?>


<script type="text/javascript">

  function get_next_node(id){
    $('.ldr_tb_whole').css({'display':'block'});
    $.ajax({
      type:'POST',
      url:"<?php echo base_url('index.php/package_purchase/get_next_node_ajax') ?>",
      data:{'id':id},
            success:  function (data) {
              var option = JSON.parse(data);
              $('.gj_whole_tree').html('');
              $('.gj_whole_tree').html(option);
              $('.ldr_tb_whole').css({'display':'none'});
            }
    })
  }

 


$(function(){
  $('.ldr_tb').css({'display':'block'});
  $('.ldr_tb_whole').css({'display':'none'});
})
function get_tbl(id){
  console.log('asd');
}
  function get_tblx(id){
    $('.ldr_tb').css({'display':'block'});
    $('.pck_bdy_'+id).html('');
    $.ajax({
      type:'POST',
      url:"<?php echo base_url('index.php/package_purchase/get_tree_table_ajax') ?>",
      data:{'up_id':id},
            success:  function (data) {
              var option = JSON.parse(data);
              console.log(option);
              $('.pck_bdy_'+id).append(option);
              $('.ldr_tb').css({'display':'none'});
            }
    })
  }
</script>



<style type="text/css">
  .gj_tree_table_vw .gj_tree_resp {
    border: 2px solid #2bb0e9;
    width: 500px;
    background-color: #e4e5f0;
}
.gj_tree_table_vw table thead, .gj_tree_table_vw table tbody tr {
    display: table;
    width: 100%;
    table-layout: fixed;
}
.gj_tree_table_vw tbody {
    overflow-y: scroll;
    height: 111px;
    display: block;
}
.gj_tree_table_vw table th, .gj_tree_table_vw table td {
    color: #fff;
    font-size: 10px;
    padding: 8px 0px !important;
}
.gj_tree_table_vw {
    display: none;
    position: absolute;
    bottom: 50px;
    right: -20px;
    z-index: 99;
}
tbody.pck_bdy tr td:first-child{
  width: 54px;
}
.gj_tree_table_vw table{
  width: auto !important;
}
.gj_tree_table_vw tbody::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;
}
.gj_tree_table_vw tbody::-webkit-scrollbar-thumb {
    background-color: rgba(43, 176, 233, 0.36);
    border: 2px solid rgba(1, 134, 190, 0.21);
    border-radius: 50px;
}
.gj_tree_table_vw tbody::-webkit-scrollbar {
    width: 7px;
    height: 7px;
    background-color: #F5F5F5;
}
.ldr_tb {
    width: 479px;
    display: block;
    z-index: 2;
    background: #fff;
    position: absolute;
    height: 210px;
    text-align: center
}
.ldr_tb img {
    position: absolute;
    top: 35%;
    
}
.ldr_tb_whole {
    width: 96%;
    position: absolute;
    background: #fff;
    display: block;
    text-align: center;
    height: 60%;
    z-index: 2;
}
.ldr_tb_whole img {
    top: 40%;
    position: absolute;
}
@media (max-width: 767px) {
   .gj_whole_tree {
       width: 650px;
   }
   .gj_whole_tree .col-lg-6.col-md-6.pull-left {
       width: 50%;
       float: left !important;
   }
   .gj_whole_tree .col-md-6.col-lg-6.gj_tree_vw1_struct {
       width: 50% !important;
       float: left !important;
   }
   .gj_whole_tree .col-md-3.col-lg-3.gj_tree_vw1_struct {
       width: 25% !important;
       float: left !important;
   }
   .gj_tree_vws {
        width: 150px;
          min-height: 93px;
   }
   p.fa.fa-clock-o:before {
       vertical-align: baseline;
   }
   .gj_tree_vws img {
       width: 40px;
       height: 40px;
       padding: 4px;
       margin-top: 6px;
   }
   .gj_tree_vws p:first-child {
       font-size: 14px;
   }
   .gj_tree_vws img+p, .gj_tree_vws img+p+p {
       font-size: 10px;
   }
   .gj_tree_vacant {
       font-size: 14px !important;
       padding: 5px;
   }
   .subrt_cls {
       font-size: 12px;
       left: 35%;
   }
   .right-menu {
      display: none;
  }
  .gj_tree_vws p.fa {
   font-size: 10px !important;
  
  }
}
</style>