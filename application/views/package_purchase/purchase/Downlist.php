<div class="dkbody">
  <div class="container-fluid">
    <div class="page-header">
      <h3>User Downlist</h3>
    </div>

    <div class="page-content">                   
      <div class="container-fluid pad_zero table_lr_c" style="margin-bottom: 2em">
        <div class="rr text-center">
          <h4 style="color: #FF4081;"><span style="color: #16a9e7;">Profile Of</span> Mr.<?php echo $obj[2]['user_name'].'  ('.$obj[2]['user_id'].')'; ?></h4>
        </div>
        <div class="tables_lr pad_zero">
          <table  id="left_table" class="table table-striped table-bordered data_table_style" cellspacing="10"  style="width: 100%;">
            <thead>
              <tr>
                  <th colspan="1"></th>
                  <th colspan="1"></th>
                  <th colspan="2">Distributor</th>
                  <th colspan="2">Placement</th>
                  <th>City</th>
              </tr>
              <tr>
                <th>#</th>
                <th>Join Date</th>
                <th>Id</th>
                <th>Name</th>
                <th>Id</th>
                <th>Name</th>
                <th>City</th>
              </tr>

            </thead>
            <tbody style="text-align: center;" class="my_tab_bdy">
              <?php if(count($obj[0]) > 0){ ?>
                <?php foreach ($obj[0] as $lobj) { ?>
                  <tr>
                    <td><?php //echo $lobj['user_id']; ?></td>
                    <td><?php echo $lobj['created_at']; ?></td>
                    <td><?php echo $lobj['user_id']; ?></td>
                    <td><?php echo $lobj['user_name']; ?></td>
                    <td><?php echo $lobj['actual_id']; ?></td>
                    <td><?php echo $lobj['actual_name']; ?></td>
                    <td><?php echo $lobj['place']; ?></td>
                  </tr>
                <?php } ?>
              <?php }else{ ?>
                <tr><td colspan="7">No Records</td></tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
        <div class="tables_lr pad_zero" style="margin-left:15px !important">
          <table  id="right_table" class="table table-striped table-bordered data_table_style" cellspacing="10"  style="width: 100%;">
            <thead>
              <tr>
                  <th colspan="1"></th>
                  <th colspan="1"></th>
                  <th colspan="2">Distributor</th>
                  <th colspan="2">Placement</th>
                  <th>City</th>
              </tr>
              <tr>
                <th>#</th>
                <th style="min-width: 80px !important;">Join Date</th>
                <th>Id</th>
                <th>Name</th>
                <th>Id</th>
                <th>Name</th>
                <th>City</th>
              </tr>

            </thead>
            <tbody style="text-align: center;" class="my_tab_bdy">
              <?php if(count($obj[1]) > 0){ ?>
                <?php foreach ($obj[1] as $lobj) { ?>
                  <tr>
                    <td><?php //echo $lobj['user_id']; ?></td>
                    <td><?php echo $lobj['created_at']; ?></td>
                    <td><?php echo $lobj['user_id']; ?></td>
                    <td><?php echo $lobj['user_name']; ?></td>
                    <td><?php echo $lobj['actual_id']; ?></td>
                    <td><?php echo $lobj['actual_name']; ?></td>
                    <td><?php echo $lobj['place']; ?></td>
                  </tr>
                <?php } ?>
              <?php }else{ ?>
                <tr><td colspan="7">No Records</td></tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    var t = $('#left_table').DataTable();
    var r = $('#right_table').DataTable();
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
    r.on( 'order.dt search.dt', function () {
        r.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
  })
</script>
<style type="text/css">
  .pad_zero{
    padding: 0px !important;
  }
  .tables_lr{
    width: 650px !important;
    float: left !important;
    margin-left: 10px!important;
  }
  .table_lr_c{
    width: 1330px !important;
  }
</style>