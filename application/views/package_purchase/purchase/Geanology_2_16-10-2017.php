<div class="dkbody">
	<div class="container-fluid">
		<div class="page-header gj_new_pack">
			<h3>Distributor Details View</h3>
		</div>
		<p class="gj_package_dets"><span><i class="fa fa-cubes"></i></span> Distributor Details</p>
		<div class="row">
			<div class="col-md-12 col-lg-12 gj_tree_struct">
				<div class="gj_tree_div text-center">
					<form>
						<!-- <div class="row rr">
							<div class="col-lg-3 col-md-3 offset-lg-2 offset-md-2 pull-left pr-0 pl-0">
								<span class="input input--nao">
									<input class="input__field input__field--nao" type="text" id="distributor_id"
									 />
									<label class="input__label input__label--nao" for="distributor_id">
										<span class="input__label-content input__label-content--nao">Distributor ID</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
								</span>		
							</div>

							<div class="col-lg-3 col-md-3 pull-left pr-0">
								<span class="input input--nao">
									<input class="input__field input__field--nao" type="text" id="distributor_name"
									 />
									<label class="input__label input__label--nao" for="distributor_name">
										<span class="input__label-content input__label-content--nao">Name</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
								</span>		
							</div>

							<div class="col-lg-2 col-md-3 pull-left pr-0">
								<button type="button" id="gj_dist_search" class="gj_dist_view"><i class="fa fa-search gj_icon_srh"></i> View</button>		
							</div>
						</div> -->
						
					</form>	
				</div>
			</div>
		</div>
	
		<div class="gj_whole_tree">
			<div class="row rr" style="margin-top: 2em;">
				<div class="col-lg-6 col-md-6 pull-left">
					<div class="gj_tree_l_count">
						<span class="gj_tree_left_cnt">Total Left Side Count : <span class="gj_l_cnt_no"><?php echo $left_count; ?></span></span>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 pull-left">
					<div class="gj_tree_r_count">
						<span class="gj_tree_right_cnt">Total Right Side Count : <span class="gj_r_cnt_no"><?php echo $right_count; ?></span></span>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="gj_first_tree gj_tree_vws">
						<p><?php if($obj->getUserId() != null){echo $obj->getUserId();} ?></p>
            <?php if($obj->getImage() != 0){ ?>
              <img src="<?php echo base_url($obj->getImage()); ?>" alt="tree">
            <?php }else{ ?>
						  <img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree">
            <?php } ?>

						<p><?php if($obj->getUserName() != null){echo $obj->getUserName();} ?></p>
						<p><?php if($obj->getFirstName() != null){echo $obj->getFirstName();} ?></p>
						<p class="fa fa-clock-o"><?php if($obj->getDate() != null){echo $obj->getDate()->format('Y-m-d h:m:s');} ?></p>
							<div class="gj_tree_table_vw">
							<div class="gj_cutoff_table gj_tree_resp table-responsive">
                <input type="hidden" name="iid" class="iid" value="<?php echo $obj->getPurchaseId(); ?>">
								<table id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="0" width="100%">
								    <thead>
								      	<tr>
									        <th class="text-center gj_tree_downline" colspan="9">Downline BV Details For 00001</th>
								      	</tr>
								      	<tr>
									        <th class="text-center" rowspan="2">Package Name</th>
									        <th class="text-center" colspan="2">Total</th>
									        <th class="text-center" colspan="2">Personal</th>
									        <th class="text-center" colspan="2">Current</th>
									        <th class="text-center" colspan="2">Carry Forward</th>
								      	</tr>
								      	<tr>
									        <th class="text-center">Left</th>
									        <th class="text-center">Right</th>
									        <th class="text-center">Left</th>
									        <th class="text-center">Right</th>
									        <th class="text-center">Left</th>
									        <th class="text-center">Right</th>
									        <th class="text-center">Left</th>
									        <th class="text-center">Right</th>
								      	</tr>
								    </thead>
								    <tbody class="pck_bdy">
								      	
								    </tbody>
								</table>
							</div>
						</div>
						
					</div>
					
					<!-- <hr class="gj_tree_hr"/> -->
				</div>
			</div>

			<div class="row gj_row_bot">
				<div class="col-md-6 col-lg-6 gj_tree_vw1_struct">
					<?php if($obj->getLeftObj() != null){ ?>
						<div class="gj_first_tree gj_tree_vws" onclick="get_next_node(<?php echo $obj->getLeftObj()->getPurchaseId(); ?>)">
							<p><?php echo $obj->getLeftObj()->getUserId(); ?></p>
							<img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree">
							<p><?php echo $obj->getLeftObj()->getUserName(); ?></p>
							<p><?php echo $obj->getLeftObj()->getFirstName(); ?></p>
							<p class="fa fa-clock-o"><?php echo $obj->getLeftObj()->getDate()->format('Y-m-d h:m:s'); ?></p>
              <div class="gj_tree_table_vw">
                <div class="gj_cutoff_table gj_tree_resp table-responsive">
                  <table id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                          <th class="text-center gj_tree_downline" colspan="9">Downline BV Details For 00001</th>
                        </tr>
                        <tr>
                          <th class="text-center" rowspan="2">Package Name</th>
                          <th class="text-center" colspan="2">Total</th>
                          <th class="text-center" colspan="2">Personal</th>
                          <th class="text-center" colspan="2">Current</th>
                          <th class="text-center" colspan="2">Carry Forward</th>
                        </tr>
                        <tr>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                        </tr>
                    </thead>
                    <tbody class="pck_bdy">
                    </tbody>
                  </table>
                </div>
              </div>
              <input type="hidden" name="iid" class="iid" value="<?php echo $obj->getLeftObj()->getPurchaseId(); ?>">
						</div>
						<!-- <hr class="gj_tree_hr"/> -->
					<?php }else{ ?> 
						<div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
							<div class="gj_tree_id_div gj_tree_id">-----</div>
							<img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
              <p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
              <?php if($obj->getPurchaseId() != null){?>
                <form method="post" action="<?php echo base_url('index.php/registration/cus_select_enter_pin'); ?>" class="subrt_cls">
							       <a onclick="$(this).closest('form').submit()">Add New User</a>
                    <input type="hidden" name="top_id" value="<?php echo $obj->getUserId(); ?>">
                     <input type="hidden" name="pos" value="0">
                </form>
              <?php } ?>
						</div>
					<?php } ?>
				</div>
				<div class="col-md-6 col-lg-6 gj_tree_vw1_struct">
					<?php if($obj->getRightObj() != null){ ?>
						<div class="gj_first_tree gj_tree_vws" onclick="get_next_node(<?php echo $obj->getRightObj()->getPurchaseId(); ?>)">
							<p><?php echo $obj->getRightObj()->getUserId(); ?></p>
							<img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree">
							<p><?php echo $obj->getRightObj()->getUserName(); ?></p>
							<p><?php echo $obj->getRightObj()->getFirstName(); ?></p>
							<p class="fa fa-clock-o"><?php echo $obj->getRightObj()->getDate()->format('Y-m-d h:m:s'); ?></p>
							<div class="gj_tree_table_vw" style="top: auto; left: auto; z-index: 99; bottom: 60px; right: 0;">
                <div class="gj_cutoff_table gj_tree_resp table-responsive">
                    <table id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                          <th class="text-center gj_tree_downline" colspan="9">Downline BV Details For 00001</th>
                        </tr>
                        <tr>
                          <th class="text-center" rowspan="2">Package Name</th>
                          <th class="text-center" colspan="2">Total</th>
                          <th class="text-center" colspan="2">Personal</th>
                          <th class="text-center" colspan="2">Current</th>
                          <th class="text-center" colspan="2">Carry Forward</th>
                        </tr>
                        <tr>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                        </tr>
                    </thead>
                    <tbody class="pck_bdy">
                    </tbody>
                  </table>
                </div>
              </div>
              <input type="hidden" name="iid" class="iid" value="<?php echo $obj->getRightObj()->getPurchaseId(); ?>">
						</div>
						<!-- <hr class="gj_tree_hr"/> -->
					<?php }else{ ?>  

          <div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
              <div class="gj_tree_id_div gj_tree_id">-----</div>
              <img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
              <p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
              <?php if($obj->getPurchaseId() != null){?>
                <form method="post" action="<?php echo base_url('index.php/registration/cus_select_enter_pin'); ?>" class="subrt_cls">
                     <a onclick="$(this).closest('form').submit()">Add New User</a>
                    <input type="hidden" name="top_id" value="<?php echo $obj->getUserId(); ?>">
                     <input type="hidden" name="pos" value="1">
                </form>
              <?php } ?>
            </div>

					<?php } ?>
				</div>
			</div>

			<div class="row gj_row_bot">
				<div class="col-md-3 col-lg-3 gj_tree_vw1_struct">
					<?php if($obj->getLeftObj() != null){ ?>
						<?php if($obj->getLeftObj()->getLeftObj() != null){ ?>
							<div class="gj_first_tree gj_tree_vws" onclick="get_next_node(<?php echo $obj->getLeftObj()->getLeftObj()->getPurchaseId(); ?>)">
								<p><?php echo $obj->getLeftObj()->getLeftObj()->getUserId(); ?></p>
								<img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree">
								<p><?php echo $obj->getLeftObj()->getLeftObj()->getUserName(); ?></p>
								<p><?php echo $obj->getLeftObj()->getLeftObj()->getFirstName(); ?></p>
								<p class="fa fa-clock-o"><?php echo $obj->getLeftObj()->getLeftObj()->getDate()->format('Y-m-d h:m:s'); ?></p>
								<div class="gj_tree_table_vw">
                  <div class="gj_cutoff_table gj_tree_resp table-responsive">
                    <table id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                          <th class="text-center gj_tree_downline" colspan="9">Downline BV Details For 00001</th>
                        </tr>
                        <tr>
                          <th class="text-center" rowspan="2">Package Name</th>
                          <th class="text-center" colspan="2">Total</th>
                          <th class="text-center" colspan="2">Personal</th>
                          <th class="text-center" colspan="2">Current</th>
                          <th class="text-center" colspan="2">Carry Forward</th>
                        </tr>
                        <tr>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                        </tr>
                    </thead>
                    <tbody class="pck_bdy">
                    </tbody>
                  </table>
                  </div>
                </div>
                <input type="hidden" name="iid" class="iid" value="<?php echo $obj->getLeftObj()->getLeftObj()->getPurchaseId(); ?>">
							</div>
							<!-- <hr class="gj_tree_hr"/> -->
						<?php }else{ ?> 
							<div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
                <div class="gj_tree_id_div gj_tree_id">-----</div>
                <img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
                <p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
                <?php if($obj->getLeftObj() != null){ ?>  
                    <form method="post" action="<?php echo base_url('index.php/registration/cus_select_enter_pin'); ?>" class="subrt_cls">
                         <a onclick="$(this).closest('form').submit()">Add New User</a>
                        <input type="hidden" name="top_id" value="<?php echo $obj->getLeftObj()->getUserId(); ?>">
                        <input type="hidden" name="pos" value="0">
                    </form>
                <?php } ?>
              </div>
						<?php } ?>
					<?php }else{ ?> 
						<div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
							<div class="gj_tree_id_div gj_tree_id">-----</div>
							<img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
							<p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
              <?php if($obj->getLeftObj() != null){ ?>  
                  <form method="post" action="<?php echo base_url('index.php/registration/cus_select_enter_pin'); ?>" class="subrt_cls">
                       <a onclick="$(this).closest('form').submit()">Add New User</a>
                      <input type="hidden" name="top_id" value="<?php echo $obj->getLeftObj()->getUserId(); ?>">
                      <input type="hidden" name="pos" value="0">
                  </form>
              <?php } ?>
						</div>
					<?php } ?>
				</div>
				<div class="col-md-3 col-lg-3 gj_tree_vw1_struct">
					<?php if($obj->getLeftObj() != null){ ?>
						<?php if($obj->getLeftObj()->getRightObj() != null){ ?>
							<div class="gj_first_tree gj_tree_vws" onclick="get_next_node(<?php echo $obj->getLeftObj()->getRightObj()->getPurchaseId(); ?>)">
								<p><?php echo $obj->getLeftObj()->getRightObj()->getUserId(); ?></p>
								<img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree">
								<p><?php echo $obj->getLeftObj()->getRightObj()->getUserName(); ?></p>
								<p><?php echo $obj->getLeftObj()->getRightObj()->getFirstName(); ?></p>
								<p class="fa fa-clock-o"><?php echo $obj->getLeftObj()->getRightObj()->getDate()->format('Y-m-d h:m:s'); ?></p>
								<div class="gj_tree_table_vw">
                  <div class="gj_cutoff_table gj_tree_resp table-responsive">
                    <table id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                          <th class="text-center gj_tree_downline" colspan="9">Downline BV Details For 00001</th>
                        </tr>
                        <tr>
                          <th class="text-center" rowspan="2">Package Name</th>
                          <th class="text-center" colspan="2">Total</th>
                          <th class="text-center" colspan="2">Personal</th>
                          <th class="text-center" colspan="2">Current</th>
                          <th class="text-center" colspan="2">Carry Forward</th>
                        </tr>
                        <tr>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                          <th class="text-center">Left</th>
                          <th class="text-center">Right</th>
                        </tr>
                    </thead>
                    <tbody class="pck_bdy">
                    </tbody>
                  </table>
                  </div>
                </div>
                <input type="hidden" name="iid" class="iid" value="<?php echo $obj->getLeftObj()->getRightObj()->getPurchaseId(); ?>">
							</div>
							<hr class="gj_tree_hr"/>
						<?php }else{ ?> 
							<div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
								<div class="gj_tree_id_div gj_tree_id">-----</div>
								<img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
								<p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
                <?php if($obj->getLeftObj() != null){ ?>  
                    <form method="post" action="<?php echo base_url('index.php/registration/cus_select_enter_pin'); ?>" class="subrt_cls">
                         <a onclick="$(this).closest('form').submit()">Add New User</a>
                        <input type="hidden" name="top_id" value="<?php echo $obj->getLeftObj()->getUserId(); ?>">
                        <input type="hidden" name="pos" value="1">
                    </form>
                <?php } ?>
							</div>
						<?php } ?>
					<?php }else{ ?> 
						<div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
							<div class="gj_tree_id_div gj_tree_id">-----</div>
							<img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
							<p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
						</div>
					<?php } ?>
				</div>
				<div class="col-md-3 col-lg-3 gj_tree_vw1_struct">
					<?php if($obj->getRightObj() != null){ ?>
						<?php if($obj->getRightObj()->getLeftObj() != null){ ?>
							<div class="gj_first_tree gj_tree_vws" onclick="get_next_node(<?php echo $obj->getRightObj()->getLeftObj()->getPurchaseId(); ?>)">
								<p><?php echo $obj->getRightObj()->getLeftObj()->getUserId(); ?></p>
								<img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree">
								<p><?php echo $obj->getRightObj()->getLeftObj()->getUserName(); ?></p>
								<p><?php echo $obj->getRightObj()->getLeftObj()->getFirstName(); ?></p>
								<p class="fa fa-clock-o"><?php echo $obj->getRightObj()->getLeftObj()->getDate()->format('Y-m-d h:m:s'); ?></p>
								  <div class="gj_tree_table_vw" style="top: auto; left: auto; z-index: 99; bottom: 60px; right: 0;">
                    <div class="gj_cutoff_table gj_tree_resp table-responsive">
                      <table id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th class="text-center gj_tree_downline" colspan="9">Downline BV Details For 00001</th>
                            </tr>
                            <tr>
                              <th class="text-center" rowspan="2">Package Name</th>
                              <th class="text-center" colspan="2">Total</th>
                              <th class="text-center" colspan="2">Personal</th>
                              <th class="text-center" colspan="2">Current</th>
                              <th class="text-center" colspan="2">Carry Forward</th>
                            </tr>
                            <tr>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                            </tr>
                        </thead>
                        <tbody class="pck_bdy">
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <input type="hidden" name="iid" class="iid" value="<?php echo $obj->getRightObj()->getLeftObj()->getPurchaseId(); ?>">
							</div>
							<!-- <hr class="gj_tree_hr"/> -->
						<?php }else{ ?> 
							<div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
								<div class="gj_tree_id_div gj_tree_id">-----</div>
								<img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
								<p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
                <?php if($obj->getRightObj() != null){ ?>  
                    <form method="post" action="<?php echo base_url('index.php/registration/cus_select_enter_pin'); ?>" class="subrt_cls">
                         <a onclick="$(this).closest('form').submit()">Add New User</a>
                        <input type="hidden" name="top_id" value="<?php echo $obj->getRightObj()->getUserId(); ?>">
                        <input type="hidden" name="pos" value="0">
                    </form>
                <?php } ?>
							</div>
						<?php } ?>
					<?php }else{ ?> 
						<div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
							<div class="gj_tree_id_div gj_tree_id">-----</div>
							<img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
							<p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
						</div>
					<?php } ?>
				</div>
				<div class="col-md-3 col-lg-3 gj_tree_vw1_struct">
					<?php if($obj->getRightObj() != null){ ?>
						<?php if($obj->getRightObj()->getRightObj() != null){ ?>
							<div class="gj_first_tree gj_tree_vws" onclick="get_next_node(<?php echo $obj->getRightObj()->getRightObj()->getPurchaseId(); ?>)">
								<p><?php echo $obj->getRightObj()->getRightObj()->getUserId(); ?></p>
								<img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree">
								<p><?php echo $obj->getRightObj()->getRightObj()->getUserName(); ?></p>
								<p><?php echo $obj->getRightObj()->getRightObj()->getFirstName(); ?></p>
								<p class="fa fa-clock-o"><?php echo $obj->getRightObj()->getRightObj()->getDate()->format('Y-m-d h:m:s'); ?></p>
								<div class="gj_tree_table_vw" style="top: auto; left: auto; z-index: 99; bottom: 60px; right: 0;">
                  <div class="gj_cutoff_table gj_tree_resp table-responsive">
                      <table id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th class="text-center gj_tree_downline" colspan="9">Downline BV Details For 00001</th>
                            </tr>
                            <tr>
                              <th class="text-center" rowspan="2">Package Name</th>
                              <th class="text-center" colspan="2">Total</th>
                              <th class="text-center" colspan="2">Personal</th>
                              <th class="text-center" colspan="2">Current</th>
                              <th class="text-center" colspan="2">Carry Forward</th>
                            </tr>
                            <tr>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                              <th class="text-center">Left</th>
                              <th class="text-center">Right</th>
                            </tr>
                        </thead>
                        <tbody class="pck_bdy">
                        </tbody>
                      </table>
                  </div>
                </div>
                <input type="hidden" name="iid" class="iid" value="<?php echo $obj->getRightObj()->getRightObj()->getPurchaseId(); ?>">
							</div>
							<!-- <hr class="gj_tree_hr"/> -->
						<?php }else{ ?> 
							<div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
								<div class="gj_tree_id_div gj_tree_id">-----</div>
								<img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
								<p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
                 <?php if($obj->getRightObj() != null){ ?>  
                    <form method="post" action="<?php echo base_url('index.php/registration/cus_select_enter_pin'); ?>" class="subrt_cls">
                         <a onclick="$(this).closest('form').submit()">Add New User</a>
                        <input type="hidden" name="top_id" value="<?php echo $obj->getRightObj()->getUserId(); ?>">
                        <input type="hidden" name="pos" value="1">
                    </form>
                <?php } ?>
							</div>
						<?php } ?>
					<?php }else{ ?> 
						<div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
							<div class="gj_tree_id_div gj_tree_id">-----</div>
							<img src="<?php echo base_url('assets/images/default_user.png'); ?>" alt="tree" class="img-responsive gj_tree_img_div gj_tree_img">
							<p class="gj_user_det gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>

		<div class="country_width_100 col-12 mt-5">	      	
	        <div class="country_width_100 mt-3 mb-3">					      		  
			    <div class="country-right">
			      	<button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button>
			    </div>	
			</div>
		</div>
	</div>	
</div>

<style type="text/css">
	p.fa.fa-clock-o:before {
	    padding-right: 5px;
	}
  .row{
    margin:25px 0px;
  }
  .table-striped>tbody>tr:nth-of-type(odd) {
      background-color: none !important;
  }
</style>


<script type="text/javascript">

	function get_next_node(id){
		$.ajax({
			type:'POST',
			url:"<?php echo base_url('index.php/package_purchase/get_next_node_ajax') ?>",
			data:{'id':id},
          	success:  function (data) {
          		var option = JSON.parse(data);
          		$('.gj_whole_tree').html('');
          		$('.gj_whole_tree').html(option);
          	}
		})
	}

  // $(document).on('mouseover','.gj_first_tree.gj_tree_vws',function(){
  // // $('.gj_first_tree.gj_tree_vws').hover(function(){
  //   id = $(this).find('.iid').val();
  //   // alert(id)
  //   var aa = $(this);
  //   // if(!id=""){
  //     $.ajax({
  //       method  :   'post',
  //       url     :   '<?php echo base_url("index.php/package_purchase/ajax_single_package_count"); ?>',
  //       data    :   {'id':id,'pack_id':12},
  //       success :   function(data){
  //         var option  = JSON.parse(data);
  //         console.log(option);
  //         aa.find('.gj_cutoff_table.gj_tree_resp.table-responsive .pck_bdy').html("");
  //         aa.find('.gj_cutoff_table.gj_tree_resp.table-responsive .pck_bdy').append(option);
  //       }
  //     })
  //   // }
  // })
</script>