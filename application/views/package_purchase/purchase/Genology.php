<?php //print_r($obj); ?>
<div class="dkbody">
	<div class="container-fluid">
		<!-- <div class="page-header gj_new_pack">
			<h3>Distributor Details View</h3>
		</div> -->
		<!-- <p class="gj_package_dets"><span><i class="fa fa-cubes"></i></span> Distributor Details</p> -->
		<div class="row">
			<div class="col-md-12 col-lg-12 gj_tree_struct">
				<div class="gj_tree_div text-center">
					<form>
						<div class="row rr">
							<!-- <div class="col-lg-3 col-md-3 offset-lg-2 offset-md-2 pull-left pr-0 pl-0">
								<span class="input input--nao">
									<input class="input__field input__field--nao" type="text" id="distributor_id"
									 />
									<label class="input__label input__label--nao" for="distributor_id">
										<span class="input__label-content input__label-content--nao">Distributor ID</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
								</span>		
							</div>

							<div class="col-lg-3 col-md-3 pull-left pr-0">
								<span class="input input--nao">
									<input class="input__field input__field--nao" type="text" id="distributor_name"
									 />
									<label class="input__label input__label--nao" for="distributor_name">
										<span class="input__label-content input__label-content--nao">Name</span>
									</label>
									<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
										<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
									</svg>
								</span>		
							</div> -->

							<div class="col-lg-2 col-md-3 pull-left pr-0">
								<!-- <button type="button" id="gj_dist_search" class="gj_dist_view"><i class="fa fa-search gj_icon_srh"></i> View</button>		 -->
							</div>
						</div>
						
					</form>	
				</div>
			</div>
		</div>
		<div class="dappa" style="margin-top: 3em">
			<div class="row rr">
				<div class="col-lg-6 col-md-6 pull-left">
					<div class="gj_tree_l_count">
						<span class="gj_tree_left_cnt">Total Left Side Count : <span class="gj_l_cnt_no"><?php echo $left_count; ?></span></span>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 pull-left">
					<div class="gj_tree_r_count">
						<span class="gj_tree_right_cnt">Total Right Side Count : <span class="gj_r_cnt_no"><?php echo $right_count; ?></span></span>
					</div>
				</div>
			</div>
			<div class="row gj_row_bot">
				<div class="col-md-12 col-lg-12 gj_tree_vw_struct">
					<div class="gj_tree_vw_tab">
						<div class="row rr">
							<div class="col-lg-12">
								<div class="gj_first_tree gj_tree_vws">
									<div class="gj_tree_id_div">
										<p class="gj_tree_id"><?php if($obj->getUserId() != null){echo $obj->getUserId();} ?></p>
									</div>
									<div class="gj_tree_other_det">
										<div class="gj_tree_img_div">
											<div class="gj_tree_img">
												<?php //echo $obj->getImage(); ?>
												<?php if($obj->getImage() != 0){ ?>
													<img src="<?php echo base_url(); ?><?php echo $obj->getImage(); ?>" alt="tree" class="img-responsive gj_tree_vw_img">
												<?php }else{?>
													<img src="<?php echo base_url('assets/images/user_1.png'); ?>" alt="tree" class="img-responsive gj_tree_vw_img">
												<?php } ?>
											</div>
										</div>
										<div class="gj_user_det_div">
											<div class="gj_user_det">
												<p class="gj_tree_inf gj_tree_name"><?php if($obj->getUserName() != null){echo $obj->getUserName();} ?></p>
												<p class="gj_tree_inf">JOIN DATE : <?php if($obj->getDate() != null){echo $obj->getDate()->format('Y-m-d');} ?></p>
												<p class="gj_tree_inf"><span class="fa fa-phone gj_tree_ph"></span><?php if($obj->getMobile() != null){echo $obj->getMobile();} ?></p>
											</div>
										</div>
									</div>
									<div class="gj_tree_table_vw">
										<div class="gj_cutoff_table gj_tree_resp table-responsive">
											<table id="gj_tree_struct_vw" class="table table-striped table-bordered" cellspacing="0" width="100%">
											    <thead>
											      	<tr>
												        <th class="text-center gj_tree_downline" colspan="9">Downline BV Details For 00001</th>
											      	</tr>
											      	<tr>
												        <th class="text-center" rowspan="2">Package Name</th>
												        <th class="text-center" colspan="2">Total</th>
												        <th class="text-center" colspan="2">Personal</th>
												        <th class="text-center" colspan="2">Current</th>
												        <th class="text-center" colspan="2">Carry Forward</th>
											      	</tr>
											      	<tr>
												        <th class="text-center">Left</th>
												        <th class="text-center">Right</th>
												        <th class="text-center">Left</th>
												        <th class="text-center">Right</th>
												        <th class="text-center">Left</th>
												        <th class="text-center">Right</th>
												        <th class="text-center">Left</th>
												        <th class="text-center">Right</th>
											      	</tr>
											    </thead>
											    <tbody>
											      	<tr>
												        <td>PACKAGE 100BV</td>
												        <td>2719</td>
												        <td>1084</td>
												        <td>0</td>
												        <td>3</td>
												        <td>0</td>
												        <td>0</td>
												        <td>1639</td>
												        <td>0</td>
											      	</tr>
											      	<tr>
												        <td>PACKAGE 200BV</td>
												        <td>5</td>
												        <td>3</td>
												        <td>0</td>
												        <td>0</td>
												        <td>0</td>
												        <td>0</td>
												        <td>2</td>
												        <td>0</td>
											      	</tr>
											      	<tr>
												        <td>BUNDLE OFFER</td>
												        <td>408</td>
												        <td>247</td>
												        <td>0</td>
												        <td>0</td>
												        <td>0</td>
												        <td>0</td>
												        <td>159</td>
												        <td>0</td>
											      	</tr>
											    </tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr class="gj_tree_hr"/>
				</div>
			</div>

			<div class="row gj_row_bot">
				<div class="col-md-6 col-lg-6 gj_tree_vw1_struct">
					<div class="gj_tree_vw_tab">
						<div class="row rr">
							<div class="col-lg-12">
								<div class="gj_second_tree gj_tree_vws">

									<div class="gj_tree_id_div">
										<form method="post" action="<?php echo base_url('index.php/package_purchase/get_next_node_post') ?>">
											<input type="hidden" name="id" value="<?php if($obj->getLeftObj() != null){echo $obj->getLeftObj()->getPurchaseId();}else{echo"0";} ?>">
										<p class="gj_tree_id" onclick="$(this).closest('form').submit()"><?php if($obj->getLeftObj() != null){echo $obj->getLeftObj()->getUserId();}else{echo"-------";} ?></p>
										</form>
									</div>
									<div class="gj_tree_other_det">
										<div class="gj_tree_img_div">
											<div class="gj_tree_img">
												<?php if($obj->getLeftObj() != null){ ?>
													<?php if($obj->getLeftObj()->getImage() != 0){ ?>
														<img src="<?php echo base_url(); ?><?php echo $obj->getLeftObj()->getImage(); ?>" alt="tree" class="img-responsive gj_tree_vw_img">
													<?php }else{?>
														<img src="<?php echo base_url('assets/images/user_1.png'); ?>" alt="tree" class="img-responsive gj_tree_vw_img">
													<?php } ?>
												<?php }else{ ?>
													<img src="<?php echo base_url('assets/images/user_3.png'); ?>" alt="tree" class="img-responsive gj_tree_vw_img">
												<?php } ?>
											</div>
										</div>
										<div class="gj_user_det_div">
											<?php if($obj->getLeftObj() != null){ ?>
												<div class="gj_user_det">
													<p class="gj_tree_inf gj_tree_name"><?php if($obj->getLeftObj() != null){echo $obj->getLeftObj()->getUserName();}else{echo"Empty Node";} ?></p>
													<p class="gj_tree_inf">JOIN DATE : <?php if($obj->getLeftObj() != null){echo $obj->getLeftObj()->getDate()->format('Y-m-d');}else{echo"Empty Node";} ?></p>
													<p class="gj_tree_inf"><span class="fa fa-phone gj_tree_ph"></span><?php if($obj->getLeftObj() != null){echo $obj->getLeftObj()->getMobile();}else{echo"Empty Node";} ?></p>
												</div>
											<?php }else{ ?>
												<div class="gj_user_det">
													<p class="gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
												</div>
											<?php } ?>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
					<hr class="gj_tree_hr"/>
				</div>
				<div class="col-md-6 col-lg-6 gj_tree_vw1_struct">
					<div class="gj_tree_vw_tab">
						<div class="row rr">
							<div class="col-lg-12">
								<div class="gj_second_tree gj_second_tree_nxt gj_tree_vws">
									<div class="gj_tree_id_div"">
										<form method="post" action="<?php echo base_url('index.php/package_purchase/get_next_node_post') ?>">
											<input type="hidden" name="id" value="<?php if($obj->getRightObj() != null){echo $obj->getRightObj()->getPurchaseId();}else{echo"0";} ?>">

											<p class="gj_tree_id" onclick="$(this).closest('form').submit()"><?php if($obj->getRightObj() != null){echo $obj->getRightObj()->getUserId();}else{echo"-------";} ?></p>
										</form>
									</div>
									<div class="gj_tree_other_det">
										<div class="gj_tree_img_div">
											<div class="gj_tree_img">
												<?php if($obj->getRightObj() != null){ ?>
													<?php if($obj->getRightObj()->getImage() != 0){ ?>
														<img src="<?php echo base_url(); ?><?php echo $obj->getRightObj()->getImage(); ?>" alt="tree" class="img-responsive gj_tree_vw_img">
													<?php }else{?>
														<img src="<?php echo base_url('assets/images/user_1.png'); ?>" alt="tree" class="img-responsive gj_tree_vw_img">
													<?php } ?>
												<?php }else{ ?>
													<img src="<?php echo base_url('assets/images/user_3.png'); ?>" alt="tree" class="img-responsive gj_tree_vw_img">
												<?php } ?>
											</div>
										</div>
										<div class="gj_user_det_div">
											<?php if($obj->getRightObj() != null){ ?>
												<div class="gj_user_det">
													<p class="gj_tree_inf gj_tree_name"><?php if($obj->getRightObj() != null){echo $obj->getRightObj()->getUserName();}else{echo"Empty Node";} ?></p>
													<p class="gj_tree_inf">JOIN DATE : <?php if($obj->getRightObj() != null){echo $obj->getRightObj()->getDate()->format('Y-m-d');}else{echo"Empty Node";} ?></p>
													<p class="gj_tree_inf"><span class="fa fa-phone gj_tree_ph"></span><?php if($obj->getRightObj() != null){echo $obj->getRightObj()->getMobile();}else{echo"Empty Node";} ?></p>
												</div>
											<?php }else{ ?>
												<div class="gj_user_det">
													<p class="gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
												</div>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr class="gj_tree_hr"/>
				</div>
			</div>

			<div class="row gj_row_bot">
				<div class="col-md-3 col-lg-3 gj_tree_vw1_struct">
					<div class="gj_tree_vw_tab">
						<div class="row rr">
							<div class="col-lg-12">
								<div class="gj_third_tree gj_tree_vws">
									<div class="gj_tree_id_div">
										<form method="post" action="<?php echo base_url('index.php/package_purchase/get_next_node_post') ?>">
											<input type="hidden" name="id" value="<?php if($obj->getLeftObj() != null){ if($obj->getLeftObj()->getLeftObj() != null){echo $obj->getLeftObj()->getLeftObj()->getPurchaseId();}else{echo"0";}}else{echo"0";} ?>">
										<p class="gj_tree_id"  onclick="$(this).closest('form').submit()"><?php if($obj->getLeftObj() != null){if($obj->getLeftObj()->getLeftObj() != null){echo $obj->getLeftObj()->getLeftObj()->getUserId();}else{echo"-------";}}else{echo"-------";} ?></p>
										</form>
									</div>
									<div class="gj_tree_other_det">
										<div class="gj_tree_img_div">
											<div class="gj_tree_img">
												<?php if($obj->getLeftObj() != null){ if($obj->getLeftObj()->getLeftObj() != null){ ?>
													<?php if($obj->getLeftObj()->getLeftObj()->getImage() != 0){ ?>
														<img src="<?php echo base_url(); ?><?php echo $obj->getLeftObj()->getLeftObj()->getImage(); ?>" alt="tree" class="img-responsive gj_tree_vw_img">
													<?php }else{?>
														<img src="<?php echo base_url('assets/images/user_1.png'); ?>" alt="tree" class="img-responsive gj_tree_vw_img">
													<?php } ?>
												<?php }else{ ?>
													<img src="<?php echo base_url('assets/images/user_3.png'); ?>" alt="tree" class="img-responsive gj_tree_vw_img">
												<?php } ?><?php }else{ ?>
													<img src="<?php echo base_url('assets/images/user_3.png'); ?>" alt="tree" class="img-responsive gj_tree_vw_img">
												<?php } ?>
											</div>
										</div>

										<div class="gj_user_det_div">
											<?php if($obj->getLeftObj() != null){ if($obj->getLeftObj()->getLeftObj() != null){ ?>
												<div class="gj_user_det">
													<p class="gj_tree_inf gj_tree_name"><?php if($obj->getLeftObj()->getLeftObj() != null){echo $obj->getLeftObj()->getLeftObj()->getUserName();}else{echo"Empty Node";} ?></p>
													<p class="gj_tree_inf">JOIN DATE : <?php if($obj->getLeftObj()->getLeftObj() != null){echo $obj->getLeftObj()->getLeftObj()->getDate()->format('Y-m-d');}else{echo"Empty Node";} ?></p>
													<p class="gj_tree_inf"><span class="fa fa-phone gj_tree_ph"></span><?php if($obj->getLeftObj()->getLeftObj() != null){echo $obj->getLeftObj()->getLeftObj()->getMobile();}else{echo"Empty Node";} ?></p>
												</div>
											<?php }else{ ?>
												<div class="gj_user_det">
													<p class="gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
												</div>
											<?php } ?><?php }else{ ?>
												<div class="gj_user_det">
													<p class="gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
												</div>
											<?php } ?>
										</div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-lg-3 gj_tree_vw1_struct">
					<div class="gj_tree_vw_tab">
						<div class="row rr">
							<div class="col-lg-12">
								<div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
									<div class="gj_tree_id_div" >
										<form method="post" action="<?php echo base_url('index.php/package_purchase/get_next_node_post') ?>">
											<input type="hidden" name="id" value="<?php if($obj->getLeftObj() != null){ if($obj->getLeftObj()->getRightObj() != null){echo $obj->getLeftObj()->getRightObj()->getPurchaseId();}else{echo"0";} ?>">
										<p class="gj_tree_id"  onclick="$(this).closest('form').submit()"><?php if($obj->getLeftObj()->getRightObj() != null){echo $obj->getLeftObj()->getRightObj()->getUserId();}else{echo"-------";}}else{echo"-------";} ?></p>
										</form>
									</div>
									<div class="gj_tree_other_det">
										<div class="gj_tree_img_div">
											<div class="gj_tree_img">
												<?php if($obj->getLeftObj() != null){  if($obj->getLeftObj()->getRightObj() != null){ ?>
													<?php if($obj->getLeftObj()->getRightObj()->getImage() != 0){ ?>
														<img src="<?php echo base_url(); ?><?php echo $obj->getLeftObj()->getRightObj()->getImage(); ?>" alt="tree" class="img-responsive gj_tree_vw_img">
													<?php }else{?>
														<img src="<?php echo base_url('assets/images/user_1.png'); ?>" alt="tree" class="img-responsive gj_tree_vw_img">
													<?php } ?>
												<?php }else{ ?>
													<img src="<?php echo base_url('assets/images/user_3.png'); ?>" alt="tree" class="img-responsive gj_tree_vw_img">
												<?php } ?><?php }else{ ?>
													<img src="<?php echo base_url('assets/images/user_3.png'); ?>" alt="tree" class="img-responsive gj_tree_vw_img">
												<?php } ?>
											</div>
										</div>
										<div class="gj_user_det_div">
											<div class="gj_user_det">
												<?php if($obj->getLeftObj()!= null){ if($obj->getLeftObj()->getRightObj() != null){ ?>
													<div class="gj_user_det">
														<p class="gj_tree_inf gj_tree_name"><?php if($obj->getLeftObj()->getRightObj() != null){echo $obj->getLeftObj()->getRightObj()->getUserName();}else{echo"Empty Node";} ?></p>
														<p class="gj_tree_inf">JOIN DATE : <?php if($obj->getLeftObj()->getRightObj() != null){echo $obj->getLeftObj()->getRightObj()->getDate()->format('Y-m-d');}else{echo"Empty Node";} ?></p>
														<p class="gj_tree_inf"><span class="fa fa-phone gj_tree_ph"></span><?php if($obj->getLeftObj()->getRightObj() != null){echo $obj->getLeftObj()->getRightObj()->getMobile();}else{echo"Empty Node";} ?></p>
													</div>
												<?php }else{ ?>
													<div class="gj_user_det">
														<p class="gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
													</div>
												<?php } ?><?php }else{ ?>
													<div class="gj_user_det">
														<p class="gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
													</div>
												<?php } ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-lg-3 gj_tree_vw1_struct">
					<div class="gj_tree_vw_tab">
						<div class="row rr">
							<div class="col-lg-12">
								<div class="gj_third_tree gj_tree_vws">
									<div class="gj_tree_id_div">
										<form method="post" action="<?php echo base_url('index.php/package_purchase/get_next_node_post') ?>">
											<input type="hidden" name="id" value="<?php if($obj->getRightObj() != null){ if($obj->getRightObj()->getLeftObj() != null){echo $obj->getLeftObj()->getLeftObj()->getPurchaseId();}else{echo"0";}}else{echo"0";} ?>">
										<p class="gj_tree_id"  onclick="$(this).closest('form').submit()"><?php if($obj->getRightObj() != null){ if($obj->getRightObj()->getLeftObj() != null){echo $obj->getLeftObj()->getLeftObj()->getUserId();}else{echo"-------";}}else{echo"-------";} ?></p>
										</form>
									</div>
									<div class="gj_tree_other_det">
										<div class="gj_tree_img_div">
											<div class="gj_tree_img">
												<?php if($obj->getRightObj() != null){ if($obj->getRightObj()->getLeftObj() != null){ ?>
													<?php if($obj->getRightObj()->getLeftObj()->getImage() != 0){ ?>
														<img src="<?php echo base_url(); ?><?php echo $obj->getRightObj()->getLeftObj()->getImage(); ?>" alt="tree" class="img-responsive gj_tree_vw_img">
													<?php }else{?>
														<img src="<?php echo base_url('assets/images/user_1.png'); ?>" alt="tree" class="img-responsive gj_tree_vw_img">
													<?php } ?>
												<?php }else{ ?>
													<img src="<?php echo base_url('assets/images/user_3.png'); ?>" alt="tree" class="img-responsive gj_tree_vw_img">
												<?php } ?><?php }else{ ?>
													<img src="<?php echo base_url('assets/images/user_3.png'); ?>" alt="tree" class="img-responsive gj_tree_vw_img">
												<?php } ?>
											</div>
										</div>
										<div class="gj_user_det_div">
											<div class="gj_user_det">
												<?php if($obj->getRightObj() != null){ if($obj->getRightObj()->getLeftObj() != null){ ?>
													<div class="gj_user_det">
														<p class="gj_tree_inf gj_tree_name"><?php if($obj->getRightObj()->getLeftObj() != null){echo $obj->getRightObj()->getLeftObj()->getUserName();}else{echo"Empty Node";} ?></p>
														<p class="gj_tree_inf">JOIN DATE : <?php if($obj->getRightObj()->getLeftObj() != null){echo $obj->getRightObj()->getLeftObj()->getDate()->format('Y-m-d');}else{echo"Empty Node";} ?></p>
														<p class="gj_tree_inf"><span class="fa fa-phone gj_tree_ph"></span><?php if($obj->getRightObj()->getLeftObj() != null){echo $obj->getRightObj()->getLeftObj()->getMobile();}else{echo"Empty Node";} ?></p>
													</div>
												<?php }else{ ?>
													<div class="gj_user_det">
														<p class="gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
													</div>
												<?php } ?>
												<?php }else{ ?>
													<div class="gj_user_det">
														<p class="gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
													</div>
												<?php } ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-lg-3 gj_tree_vw1_struct">
					<div class="gj_tree_vw_tab">
						<div class="row rr">
							<div class="col-lg-12">
								<div class="gj_third_tree gj_third_tree_nxt gj_tree_vws">
									<div class="gj_tree_id_div" onclick="myfunc('')">
										<form method="post" action="<?php echo base_url('index.php/package_purchase/get_next_node_post') ?>">
											<input type="hidden" name="id" value="<?php if($obj->getRightObj() != null){ if($obj->getRightObj()->getRightObj() != null){echo $obj->getRightObj()->getRightObj()->getPurchaseId();}else{echo"0";}}else{echo"0";} ?>">
										<p class="gj_tree_id"  onclick="$(this).closest('form').submit()"><?php if($obj->getRightObj() != null){ if($obj->getRightObj()->getRightObj() != null){echo $obj->getRightObj()->getRightObj()->getUserId();}else{echo"-------";}}else{echo"-------";} ?></p>
										</form>
									</div>
									<div class="gj_tree_other_det">
										<div class="gj_tree_img_div">
											<div class="gj_tree_img">
												<?php if($obj->getRightObj() != null){ if($obj->getRightObj()->getRightObj() != null){ ?>
													<?php if($obj->getRightObj()->getRightObj()->getImage() != 0){ ?>
														<img src="<?php echo base_url(); ?><?php echo $obj->getRightObj()->getRightObj()->getImage(); ?>" alt="tree" class="img-responsive gj_tree_vw_img">
													<?php }else{?>
														<img src="<?php echo base_url('assets/images/user_1.png'); ?>" alt="tree" class="img-responsive gj_tree_vw_img">
													<?php } ?>
												<?php }else{ ?>
													<img src="<?php echo base_url('assets/images/user_3.png'); ?>" alt="tree" class="img-responsive gj_tree_vw_img">
												<?php } ?>
												<?php }else{ ?>
													<img src="<?php echo base_url('assets/images/user_3.png'); ?>" alt="tree" class="img-responsive gj_tree_vw_img">
												<?php } ?>
											</div>
										</div>
										<div class="gj_user_det_div">
											<div class="gj_user_det">
												<?php if($obj->getRightObj() != null){ if($obj->getRightObj()->getRightObj() != null){ ?>
													<div class="gj_user_det">
														<p class="gj_tree_inf gj_tree_name"><?php if($obj->getRightObj()->getRightObj() != null){echo $obj->getRightObj()->getRightObj()->getUserName();}else{echo"Empty Node";} ?></p>
														<p class="gj_tree_inf">JOIN DATE : <?php if($obj->getRightObj()->getRightObj() != null){echo $obj->getRightObj()->getRightObj()->getDate()->format('Y-m-d');}else{echo"Empty Node";} ?></p>
														<p class="gj_tree_inf"><span class="fa fa-phone gj_tree_ph"></span><?php if($obj->getRightObj()->getRightObj() != null){echo $obj->getRightObj()->getRightObj()->getMobile();}else{echo"Empty Node";} ?></p>
													</div>
												<?php }else{ ?>
													<div class="gj_user_det">
														<p class="gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
													</div>
												<?php } ?><?php }else{ ?>
													<div class="gj_user_det">
														<p class="gj_tree_inf gj_tree_name gj_tree_vacant">Vacant</p>
													</div>
												<?php } ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="country_width_100 col-12 mt-5">	      	
		        <div class="country_width_100 mt-3 mb-3">					      		  
				    <div class="country-right">
				      	<!-- <button type="button" name="back" class="country_button"><i class="fa fa-arrow-left"></i> BACK</button> -->
				    </div>	
				</div>
			</div>
		</div>


	</div>	
</div>
<script type="text/javascript">
	function myfunc(id){
		if(id != 0){
			$.ajax({
	          type: "POST",
	          url: "<?php echo base_url(); ?>index.php/Package_purchase/get_next_node",
	          data: {'id':parseInt(id)},
	            success:  function (data) { 
	             	if(data != 0){
	             		var option = JSON.parse(data);
	             		$('.dappa').html('');
	             		$('.dappa').html(option);
	             		// console.log(option);
	             	}
	            }
	        })
	    }
	}
</script>
<style type="text/css">
	p.gj_package_dets {
	    margin-top: 75px;
	}
</style>