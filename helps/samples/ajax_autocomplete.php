<!-- 

Author : S.V.Karthick

 -->




<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<span class="input input--nao">
		<input class="input__field input__field--nao package" type="text" value="">
		<label class="input__label input__label--nao" for="input-1">
			<span class="input__label-content input__label-content--nao">Package</span>
		</label>
		<svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
			<path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"></path>
		</svg>
	</span>	
</body>
</html>


<script type="text/javascript">
	$(function(){
		$('.package').autocomplete();
	})

	$(document).on('keyup','.package',function(){
	// $('.package').keyup(function(){
		var id=$(this).val();
		$.post("<?php echo base_url('index.php/common_controller/get_all_packages_ajax')?>",
		{
	   		key:id,
	 
	 	},
	 	function(data,status){
	  		if(status=="success"){
			    if(data==''){
			   		console.log(data)
			    }else{
	   				var item=JSON.parse(data);//console.log(item)
	    			$(".package").autocomplete({
					    source: item,
					    minLength: 0,
					 	create: function () {
					  		$(this).data('ui-autocomplete')._renderItem = function (ul, item) {
						   		return $('<li>')
						      	.append('<a>'+ item.label +'</a>')
						      	.appendTo(ul);
						  	};
					 	},
					 	select:function(event,ui){
				      		event.preventDefault();
				      		$(".package").val(ui.item.data_id);
				      		$('.package').val(ui.item.value).trigger('change');
				      		$('.package_hid_id').val(ui.item.data_id);
				      		$('.package_pack_price').val(ui.item.pck_price);
					     }
					    });
	    		}
	      	}
	    });
 	});
</script>



<?php 

	// Controller FUnction Start
		public function get_all_packages_ajax(){
			$utill = new Common_utill();
			$val = 0;
			if($this->input->post('val') != null){
				$val = $this->input->post('val');
			}
			$pack = $utill->get_packages($val);
			
			echo json_encode($pack);
		}
	// Controller FUnction End


	// Model function  Start
			public function get_packages($key){
				$em=$this->doctrine->em;
				
				if($key != 0){
					$pack = array();
					$query = $em->createQuery("SELECT b.package_id,b.package_name,b.package_price FROM Entity\Packages as b WHERE b.package_name LIKE '%$key%' AND b.package_is_visible =1" )->setMaxResults(4);
					$tot_pack = $query->getResult();
					if(count($tot_pack) > 0){
						for ($i=0; $i < count($tot_pack); $i++) { 
							$ar = array('data_id'=>$tot_pack[$i]['package_id'],'value'=>$tot_pack[$i]['package_name']);
							array_push($pack, $ar);
						}
						return $pack;
					}else{
						$pack_sub = array();
						$query_sub = $em->createQuery("SELECT b.package_id,b.package_name,b.package_price FROM Entity\Packages as b WHERE b.package_is_visible =1" )->setMaxResults(4);
						$tot_pack_sub = $query_sub->getResult();
						for ($i=0; $i < count($tot_pack_sub); $i++) { 
							$ar_sub = array('data_id'=>$tot_pack_sub[$i]['package_id'],'value'=>$tot_pack_sub[$i]['package_name']);
							array_push($pack_sub, $ar_sub);
						}
						return $pack_sub;
					}
				}else{
					$pack_sub_sub = array();
					$query_sub_sub = $em->createQuery("SELECT b.package_id,b.package_name,b.package_price FROM Entity\Packages as b WHERE b.package_is_visible =1" )->setMaxResults(4);
					$tot_pack_sub_sub = $query_sub_sub->getResult();
					for ($i=0; $i < count($tot_pack_sub_sub); $i++) { 
						$ar_sub_sub = array('data_id'=>$tot_pack_sub_sub[$i]['package_id'],'value'=>$tot_pack_sub_sub[$i]['package_name']);
						array_push($pack_sub_sub, $ar_sub_sub);
					}
					return $pack_sub_sub;
				}
			}
	// Model function  End


?>