
/********************************************************/
/********************************************************/
/********************************************************/

REFERENCE URL  :  

     http://itsolutionstuff.com/post/jquery-select2-ajax-autocomplete-example-with-demo-in-phpexample.html
     https://southcoastweb.co.uk/jquery-select2-v4-ajaxphp-tutorial/


/********************************************************/
/********************************************************/
/********************************************************/



<!-- HTML Code Start -->
  <div style="width:520px;margin:0px auto;margin-top:30px;height:500px;">
    <select class="itemName form-control" style="width:500px" name="itemName"></select>
  </div>
<!-- HTML Code End -->



<!-- AJAX Code Start -->
  <script type="text/javascript">
      $('.itemName').select2({
        placeholder: 'Select an item',
        ajax: {
          url: '<?php echo base_url("index.php/common_controller/sample_ajax_select") ?>',
          dataType: 'json',
          delay: 1,
          data: function (params) {console.log(params);
                return { q: params.term // search term
                };
          },
          processResults: function (data) { return { results: data }; },
          cache: true
        },
        minimumInputLength: 2
      });


      

///////////////////////////////////////PASS THIRD PARAMETER START/////////////////////////////
      $('.itemName').on("select2:selecting", function(e) { 
         console.log(e['params']['args']['data']['price']);
      });
///////////////////////////////////////PASS THIRD PARAMETER END/////////////////////////////




///////////////////////////////////////RESET SELECT2 START/////////////////////////////
$('.pck_prd').empty()
///////////////////////////////////////RESET SELECT2 END/////////////////////////////

</script>
<!-- AJAX Code End -->

<?php

<!-- Contoller Code Start -->
  public function sample_ajax_select(){
    $key = $this->input->get('q');
    $em=$this->doctrine->em;
    $bnk_ar = array();

    $query = $em->createQuery("SELECT b.bank_id,b.bank_ifsc FROM Entity\Bank_details as b WHERE b.bank_ifsc LIKE '%$key%'" )->setMaxResults(3);

    $single_bank = $query->getResult();
    $json = [];
    for ($i=0; $i < count($single_bank); $i++) { 
      $json[] = ['id'=>$single_bank[$i]['bank_id'], 'text'=>$single_bank[$i]['bank_ifsc']];
    }
   
    echo json_encode($json);
  }

<!-- Contoller Code End -->

?>



<script type="text/javascript">
  $('.itemName').on("select2:selecting", function(e) { 
    // console.log(e);
     var price = e['params']['args']['data']['price'];
     $('.prd_prc').text(price+'.00');
     $('.package_pack_price').val(price);
     $('.package_hid_id').val(e['params']['args']['data']['id']);
     
  }).on("select2:close", function(e) {
    var pck_id = $('.package_hid_id').val();
    if(pck_id){
      $('.pck_prd').select2({
        placeholder: 'Select an item',
        ajax: {
        url: '<?php echo base_url("index.php/common_controller/get_package_prd_names_aj") ?>',
        dataType: 'json',
        delay: 1,
        data: function (params) {console.log(params);
              return { id:pck_id,q: params.term // search term
              };
        },
        processResults: function (data) {return { results: data }; },
        cache: true
      },
      })
    }
  }).on("select2:opening", function(e) {
    $('.pck_prd').empty()
  })
</script>