<div class="col-md-3 pull-left">
  <span class="input input--nao">
    <input class="input__field input__field--nao user_name" type="text" id="input-1" name="user_name" value="<?php if(set_value('user_name') != null){echo set_value('user_name');}else{echo $user_name;} ?>"/>
    <label class="input__label input__label--nao" for="input-1">
      <span class="input__label-content input__label-content--nao">User Name </span>
    </label>
    <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
      <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
    </svg>
    <img src="<?php echo base_url('assets/images/loader/'); ?>default.gif" class="ldr uname_ldr">
  </span> 
  <p class="uname_suc avail_er suc">Available</p>
  <p class="uname_err avail_er err">Already Exists</p>
  <?php echo form_error('user_name','<p class="error">', '</p>'); ?>            
</div>






<script type="text/javascript">
	$('.user_name').focus(function()  {
      var a = $('.user_name').val();
      if(a.length>0){
        cc_name = a;
      }else{
        cc_name="";
      }
  })
  $('.user_name').blur(function()  {
    var user_name = $('.user_name').val();
    if(user_name.length>2 && cc_name != user_name){
      $('.uname_ldr').show();
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>Common_controller/user_name_available",
        data: {'field_value':user_name},
          success:  function (data) { 
            $('.uname_ldr').hide();
            if(data == 1){
            	$('.uname_suc').show();
            	$('.uname_err').hide();
            }else{
            	$('.uname_suc').hide();
            	$('.uname_err').show();
            }
          }
      })
    }else if(user_name.length == 0 && cc_name.length > 2){
     $('.uname_suc,.uname_err').hide();
    }
  })


    function validateEmail(email) {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    }

</script>