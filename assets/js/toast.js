/*
    Materialize's toast redone for standalone jQuery
    
    Author: André Luiz Rabêllo
    Version: 1.0.0
*/

function toast(message, duration, className, completeCallback) {
    // Settings
    var settings = $.extend({
        message:            message             || '',
        displayLength:      duration            || 5000,
        className:          className           || '',
        completeCallback:   completeCallback    || $.noop
    }, $.isPlainObject(message) ? message : {});

    // Get container
    var $container = $('#toast-container');

    // Create toast container if it does not exist
    if (!$container.length)
        $container = $('<div id="toast-container">').appendTo('body');

    // If no message, no toast
    if (!settings.message || $.isPlainObject(settings.message))
        return false;

    // Append toast
    var $toast = createToast(settings.message).appendTo($container);

    // Animate toast in
    $toast.animate({ "top": "0px", opacity: 1 }, {
        duration: 300,
        specialEasing: 'easeOutCubic',
        queue: false
    });

    // Allows timer to be pause while being panned
    var counterInterval = setInterval(function () {

        if (!$toast.parent().length)
            clearInterval(counterInterval);

        // If toast is not being dragged, decrease its time remaining
        if (!$toast.is('.panning'))
            settings.displayLength -= 20;

        if (settings.displayLength <= 0) {
            // Animate toast out
            $toast.animate({ "opacity": 0, marginTop: '-40px' }, {
                duration: 375,
                specialEasing: 'easeOutExpo',
                queue: false,
                complete: function () {
                    // Call the optional callback
                    if ($.isFunction(settings.completeCallback))
                        settings.completeCallback();
                    // Remove toast after it times out
                    $toast.remove();
                }
            });
            clearInterval(counterInterval);
        }
    }, 20);

    function createToast(html) {

        // Create toast
        var $toast = $('<div class="toast ' + settings.className + '" style="top: 35px; opacity: 0">').append(html);

       
        return $toast;
    }
};
