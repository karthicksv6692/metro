
jssor_2_slider_init = function() {

  var jssor_2_options = {
    $AutoPlay: 0,
    $ArrowNavigatorOptions: {
      $Class: $JssorArrowNavigator$
    },
    $ThumbnailNavigatorOptions: {
      $Class: $JssorThumbnailNavigator$,
      $Cols: 3,
      $SpacingX: 10,
      $SpacingY: 10,
      $Align: 0
    }
  };

  var jssor_2_slider = new $JssorSlider$("jssor_2", jssor_2_options);

  /*responsive code begin*/
  /*remove responsive code if you don't want the slider scales while window resizing*/
  function ScaleSlider() {
      var refSize = jssor_2_slider.$Elmt.parentNode.clientWidth;
      if (refSize) {
          refSize = Math.min(refSize, 810);
          jssor_2_slider.$ScaleWidth(refSize);
      }
      else {
          window.setTimeout(ScaleSlider, 30);
      }
  }
  ScaleSlider();
  $Jssor$.$AddEvent(window, "load", ScaleSlider);
  $Jssor$.$AddEvent(window, "resize", ScaleSlider);
  $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
  /*responsive code end*/
};

 $(window).on('popstate', function() {
  // location.reload();
  var pathname = window.location.pathname; 
  var url      = window.location.href; 

      ar = pathname.split('/'); 
      ar.splice(0, 3);
      
      if(ar.length == 2){
        $.ajax({
          type:"POST",
          url:"../"+ar.join('/')+"x",
          success:function (data) { 
           
            $('.dk_body').html(data);
          }

        });  
      }
  });
