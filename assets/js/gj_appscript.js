$(document).ready(function(){
	//For multiple fileupload--------------package_add.php,
	function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object
    console.log(files[0])
    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

      // Only process image files.
      if (!f.type.match('image.*')) {
        continue;
      }

      var reader = new FileReader();


      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
         $('.pkfile').prepend('<div class="p1-0 mr-2 mb-2 bdrstyle_hover" style="float:left"><img class="width-60px bdrstyle" src="'+e.target.result+'"><span>X</span></div>');
         $('.bdrstyle_hover>span').on('click',function(){
					$(this).parent().remove();
				});	
        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }
  }

  if($('#files').length>=1){
  	document.getElementById('files').addEventListener('change', handleFileSelect, false);
  }
  //End multiple fileupload script
  $.formUtils.addValidator({
    name : 're_pass',
    validatorFunction : function(value, $el, config, language, $form) {
      pass_el1=$($el).attr('data-confirm-with');
      pass1=$($el).val();
      pass2=$('#'+pass_el1).val();
      if(pass1==pass2){
        return true;        
      }
      else{
        return false;       
      }
    },
    errorMessage : 'Passwords are not same',
    errorMessageKey: 'Passwords are not same'
  });
});
/* for place the text area label to the top when that have a value*/
$(document).on('blur','textarea',function(){
  if($(this).val().length>=1){
    $(this).parent().addClass('input--filled');
  }
})
 $(document).on('click','.bdrstyle_hover>span',function(){
    $(this).parent().remove();
  }); 